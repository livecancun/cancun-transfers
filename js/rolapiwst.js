/*
Wstravel v1.0.0 (2018-Sep)
Copyright (c) 2017-2019 Wstravel.
License: http://webservicestravel.com/license/
*/
$(document).ready(function () {
	var apimain = function($) {
		// variables
		var selTipo  = '#wst-tipo-traslado';
		var selHotel = '#wst-hotel'
		var fllegada;
		// colecciones
		var hotelesCollection;
		var tiposCollection;
		// views
		var hotelesView;
		var tiposView;
		// render
		var hotelesRender;
		var tiposRender;
		var contRequest = 0;

		var ROL_TOKEN;
		var ROL_BASE_URL = 'http://api.webservicestravel.com';
		var ROL_CLIENT_ID = 'd1888de1-9655-4650-8e65-9d07da2ebc61';
		var ROL_URL_SCRIPT = '../apiwst/index.php';

		var initializeObject = function() {
		 	var authSyncFunction = Backbone.sync;

	 		Backbone.sync = function(method, model, options) {
	 			var that = this;
	 			var RolObj = {};
	 			ROL_TOKEN = null;

	 			if (localStorage.getItem('RolWst') !== null) {
	 				RolObj = JSON.parse(localStorage.getItem('RolWst'));
	 				if (RolObj) {
		 				if (RolObj.token !== undefined && RolObj.token != '' && RolObj.token != null) {
		 					ROL_TOKEN = RolObj.token;
		 				}
		 			}
	 			}

		        var url = _.isFunction(model.url) ? model.url() : model.url;

			    if (url) {
			        options = options || {};
			        options.url =  ROL_BASE_URL + url;
			    }

			    var args = options;
		        var beforeSend = options.beforeSend;
		        var error = options.error;

		        _.extend(options, {
		            beforeSend: function(xhr) {
		            	var lang = $('html').attr('lang');
		                xhr.setRequestHeader('Authorization', "Bearer " + ROL_TOKEN);
		                if (lang !== undefined && lang !='') {
		                	xhr.setRequestHeader('Accept-Language', lang);
		                }
		                if (beforeSend) return beforeSend.apply(this, arguments);
		            },

		            error: function(xhr, textStatus, errorThrown) {
		            	
		                if (error) error.call(options.context, xhr, textStatus, errorThrown);

		                if (xhr.status === 401 && contRequest <=3) {
		                	$.ajax({
		                		url :ROL_URL_SCRIPT,
		                		data : {
		                			ROL_CLIENT_ID : ROL_CLIENT_ID 
		                		},
		                		type : 'POST',
		                		dataType : 'json'
		                	}).done(function(data) {
		                		if (data.rol_secret_id !== undefined) {
		                			ROL_SECRET_ID = data.rol_secret_id
				                	
									$.ajax({
										url: ROL_BASE_URL+'/oauth',
										type : 'POST',
										data : JSON.stringify({
											client_id: ROL_CLIENT_ID,
											client_secret: ROL_SECRET_ID,
											grant_type : "client_credentials"
				                		}),
										contentType : 'application/json',
										dataType : 'json'
									}).done(function (data) {
										contRequest = contRequest + 1;
										var rol = JSON.stringify({
											token: data.access_token
										});

										localStorage.setItem('RolWst',rol);							
										args.selfcall();
									});
		                		}
		                	})
		                } else {
		                	contRequest = 0;
		                	return;
		                }
		            }
		        });

		        return authSyncFunction(method, model, options);
		    };
		}

		var createObjects = function() {
			// inicializamos los objetos
			$(selTipo).selectpicker();
			$(selHotel).selectpicker();
			// hoteles
			hotelesCollection = Backbone.Collection.extend({
				url:'/hotel/hoteles',
				parse: function(response) {
					return response._embedded.hoteles
				}
			});

			hotelesView = Backbone.View.extend({
			    initialize: function () {
			        this.render();
			    },
				render : function() {
					var that = this;
					var hoteles = new hotelesCollection();
					hoteles.fetch({
						success: function(hoteles) {
							selectMap({
								items: hoteles,
								el : selHotel
							})

							$(selHotel).selectpicker('refresh');
						},
						selfcall : function() {
							that.render();
						}
					})
				}
			});
			// tipos
			tiposCollection = Backbone.Collection.extend({
				url:'/transfer/traslados',
				parse: function(response) {
					return response._embedded.traslados
				}
			});

			tiposView = Backbone.View.extend({
			    initialize: function () {
			        this.render();
			    },
				render : function() {
					var that = this;
					var tipos = new tiposCollection();
					tipos.fetch({
						success: function(tipos) {
							selectMap({
								items: tipos,
								el : selTipo
							});
							$(selTipo).selectpicker('refresh');
							// hoteles(2)
							hotelesRender = new hotelesView();
						},
						selfcall : function() {
							that.render();
						}
					})
				}
			});

			// tipos(1)
			tiposRender = new tiposView();
		}

		var addEventListeners = function() {
		}

		var selectMap = function (args) {
			args.items.map(function(item) {
				option = $('<option/>');
		     	option.attr('value',item.get('uuid')).text(item.get('nombre'));
		     	$(args.el).append(option);
			})
		}

		return {
			init : function() {
				initializeObject();
				createObjects();
				addEventListeners();
			}
		}
	}(jQuery);

	apimain.init();
})