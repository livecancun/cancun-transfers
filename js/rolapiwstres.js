/*
Wstravel v1.0.0 (2018-Sep)
Copyright (c) 2017-2019 Wstravel.
License: http://webservicestravel.com/license/
*/
$(document).ready(function () {
	var apireserva = function($) {
		// variables
		var servicio_uuid     = '';
		var servicio_nombre   = '#wst-servicio-nombre';
		var selTipo           = '#wst-tipo-traslado';
		var selPais           = '#wst-pais';
		var selHotel          = '#wst-hotel-destino';
		var selPasajeros  	  = '#wst-pasajeros';
		var aerolinea         = '#wst-aerolinea';
		var numVuelo          = '#wst-num-vuelo';
		var fechaLlegada	  = '#wst-fecha-llegada';
		var horaLlegada       = '#wst-hora-llegada';
		var fechaSalida       = '#wst-fecha-salida';
		var horaPickup		  = '#wst-pickup-salida';
		var horaVueloSalida   = '#wst-hora-vuelo-salida';
		var nombreCompleto    = '#wst-nombre-completo';
		var email			  = '#wst-email';
		var telefono          = '#wst-telefono';
		var rowLLegada		  = '#wst-row-llegada';
		var rowSalida		  = '#wst-row-salida';
		var tarifa_precio     = '#wst-tarifa';
		var btnSubmitReserva  = '#wst-submit-reserva';
		var comentarios       = '#wst-comentarios';
		var pagoOpcion        = '#wst-pago-opcion';
		var pagoForma         = '#wst-pago-forma';
		var alerta      	  = '#wst-alerta';
		var alertaDetalle     = '#wst-alerta-detalle';
		var formTransfer	  = '#wst-form-transfer';
		var divSelHotel       = '#wst-hotel-list';
		var divInpHotel       = '#wst-hotel-input';
		var nomHotel		  = '#wst-hotel-destino-nombre';
		var hotelNoFound      = '#wst-hotel_nofound';
		var traslado_data     = [];		
		// colecciones
		var paisesCollection;
		var hotelesCollection;
		var serviciosCollection;
		var tiposCollection;
		var tarifaCollection;
		var reservaCollection;
		// views
		var paisesView;
		var hotelesView;
		var serviciosView;
		var tiposView;
		// render
		var paisesRender;
		var hotelesRender;
		var serviciosRender;
		var tiposRender;
		var contRequest = 0;

		var ROL_TOKEN;
		var ROL_BASE_URL = 'http://api.webservicestravel.com';
		var ROL_CLIENT_ID  = 'd1888de1-9655-4650-8e65-9d07da2ebc61';
		var ROL_URL_SCRIPT = '../apiwst/index.php';
		var ROL_URL_EMAIL  = '../apiwst/rolwstemail.php';
		var ROL_URL_PAY    = '_proc/rolwstpay.php';

		var initializeObject = function() {
		 	var authSyncFunction = Backbone.sync;

	 		Backbone.sync = function(method, model, options) {
	 			var that = this;
	 			var RolObj = {};
	 			ROL_TOKEN = null;

	 			if (localStorage.getItem('RolWst') !== null) {
	 				RolObj = JSON.parse(localStorage.getItem('RolWst'));
	 				if (RolObj) {
		 				if (RolObj.token !== undefined && RolObj.token != '' && RolObj.token != null) {
		 					ROL_TOKEN = RolObj.token;
		 				}
		 			}
	 			}

		        var url = _.isFunction(model.url) ? model.url() : model.url;
			    if (url) {
			        options = options || {};
			        options.url =  ROL_BASE_URL + url;
			    }

			    var args = options;
		        var beforeSend = options.beforeSend;
		        var error = options.error;

		        _.extend(options, {
		            beforeSend: function(xhr) {
		            	var lang = $('html').attr('lang');
		                xhr.setRequestHeader('Authorization', "Bearer " + ROL_TOKEN);
		                if (lang !== undefined && lang !='') {
		                	xhr.setRequestHeader('Accept-Language', lang);
		                }
		                if (beforeSend) return beforeSend.apply(this, arguments);
		            },

		            error: function(xhr, textStatus, errorThrown) {
		            	
		                if (error) error.call(options.context, xhr, textStatus, errorThrown);

		                if (xhr.status === 401 && contRequest <=3) {
		                	$.ajax({
		                		url :ROL_URL_SCRIPT,
		                		data : {
		                			ROL_CLIENT_ID : ROL_CLIENT_ID 
		                		},
		                		type : 'POST',
		                		dataType : 'json'
		                	}).done(function(data) {
		                		if (data.rol_secret_id !== undefined) {
		                			ROL_SECRET_ID = data.rol_secret_id
				                	
									$.ajax({
										url: ROL_BASE_URL+'/oauth',
										type : 'POST',
										data : JSON.stringify({
											client_id: ROL_CLIENT_ID,
											client_secret: ROL_SECRET_ID,
											grant_type : "client_credentials"
				                		}),
										contentType : 'application/json',
										dataType : 'json'
									}).done(function (data) {
										contRequest = contRequest + 1;
										var rol = JSON.stringify({
											token: data.access_token
										});

										localStorage.setItem('RolWst',rol);							
										args.selfcall();
									});
		                		}
		                	})
		                } else {
		                	contRequest = 0;
		                	return;
		                }
		            }
		        });

		        return authSyncFunction(method, model, options);
		    };
		}

		var createObjects = function(args) {
			// inicializamos los objetos
			$(rowLLegada).hide();
			$(rowSalida).hide();
			$(selPasajeros).selectpicker();
			$(selTipo).selectpicker();
			$(selHotel).selectpicker();
			$(selPais).selectpicker();
			$(alerta).hide();
			$(divInpHotel).hide();
			$(alerta).removeClass('hide');
			$(divInpHotel).removeClass('hide');
			$('body').loading({
				message : 'Procesando ...',
				start : false
			});
			// reservas
			reservaCollection = Backbone.Collection.extend({
				url:'/transfer/reservas',
				parse : function(response) {
					return response._embedded.reservas
				}
			});

			// tarifas
			tarifaCollection = Backbone.Collection.extend({
				url:'/transfer/tarifas',
				parse : function(response) {
					return response._embedded.tarifas
				}
			});

			// hoteles
			hotelesCollection = Backbone.Collection.extend({
				url:'/hotel/hoteles',
				parse: function(response) {
					return response._embedded.hoteles
				}
			});

			hotelesView = Backbone.View.extend({
			    initialize: function () {
			        this.render();
			    },
				render : function() {
					var that = this;
					var hoteles = new hotelesCollection();
					hoteles.fetch({
						success: function(hoteles) {
							selectMap({
								items: hoteles,
								el : selHotel
							});
							$(selHotel).selectpicker('refresh');
							setPreReserva(args);
						},
						selfcall : function() {
							that.render();
						}
					})
				}
			});

			// paises
			paisesCollection = Backbone.Collection.extend({
				url:'/region/paises',
				parse: function(response) {
					return response._embedded.paises
				}
			});

			paisesView = Backbone.View.extend({
			    initialize: function () {
			        this.render();
			    },
				render : function() {
					var that = this;
					var paises = new paisesCollection();
					paises.fetch({
						success: function(paises) {
							selectMap({
								items: paises,
								el : selPais
							})

							$(selPais).selectpicker('refresh');
							// hoteles(4)
							hotelesRender = new hotelesView();
						},
						selfcall : function() {
							that.render();
						}
					})
				}
			});

			// tipos
			tiposCollection = Backbone.Collection.extend({
				url:'/transfer/traslados',
				parse: function(response) {
					return response._embedded.traslados
				}
			});

			tiposView = Backbone.View.extend({
			    initialize: function () {
			        this.render();
			    },
				render : function() {
					var that = this;
					var tipos = new tiposCollection();
					tipos.fetch({
						success: function(tipos) {
							traslado_data = tipos;
							selectMap({
								items: tipos,
								el : selTipo
							});
							$(selTipo).selectpicker('refresh');
							// paises(3)
							paisesRender = new paisesView();
						},
						selfcall : function() {
							that.render();
						}
					})
				}
			});			

			// servicios
			serviciosCollection = Backbone.Collection.extend({
				url:'/transfer/servicios',
				parse: function(response) {
					return response._embedded.servicios
				}
			});

			serviciosView = Backbone.View.extend({
			    initialize: function () {
			        this.render();
			    },
				render : function() {
					var that = this;
					var servicios = new serviciosCollection();
					servicios.fetch({
						success: function(servicios) {
							var item = getItem({
								items : servicios,
								member : 'clave',
								value  : 'PRI',
								return : [
									{ name : 'uuid'},
									{ name : 'nombre'}
								]
							});

							if (item !== false) {
								$(servicio_nombre).text(item.nombre);
								servicio_uuid = item.uuid;
							}
							// tipos (2)
							tiposRender = new tiposView();
						},
						selfcall : function() {
							that.render();
						}
					})
				}
			});

			// servicios(1)
			serviciosRender = new serviciosView();
		}

		var addEventListeners = function() {
			$(selTipo).on('changed.bs.select',function(e) {
				var uuid = $(this).val();
				var item = getItem({
								items : traslado_data,
								member : 'uuid',
								value  : uuid,
								return : [
									{ name : 'clave'}
								]
							});
				if (item !== false) {
					switch (item.clave) {
						case 'aero-hotel': // llegada
							$(rowLLegada).show();
							$(rowSalida).hide();
						break;

						case 'hotel-aero': // salida
							$(rowSalida).show();
							$(rowLLegada).hide();
						break;

						case 'aero-hotel-aero': // redondo
							$(rowLLegada).show();
							$(rowSalida).show();
						break;
					}
				}
			});
			
			$(selTipo + ',' + selHotel + ',' + selPasajeros).on('changed.bs.select',function(e) {
				getTarifa();
			});
			
			$(fechaLlegada + ',' + fechaSalida).on('change',function() {
				getTarifa();
			});

			$(btnSubmitReserva).on('click',function() {
				var data = {
					servicio_tipo    : servicio_uuid,
					traslado_tipo    : $(selTipo).val(),
					num_adultos      : parseInt($(selPasajeros).val()),
					num_menores      : 0,
					num_pasajeros    : parseInt($(selPasajeros).val()),
					nombre_completo  : $(nombreCompleto).val(),
					email            : $(email).val(),
					telefono         : $(telefono).val(),
					pais             : $(selPais).val(),
					observaciones    : $(comentarios).val(),
					movimientos : {}
				};

				if ($(hotelNoFound).is(':checked')) {
					data['hotel'] = -1;
					data['hotel_nombre'] = $(nomHotel).val();
				} else {
					data['hotel'] = $(selHotel).val();
				}
				
				var item = getItem({
					items : traslado_data,
					member : 'uuid',
					value  : $(selTipo).val(),
					return : [
							{ name : 'clave'}
					]
				});

				if (item !== false) {
					switch (item.clave) {
						case 'aero-hotel':
							data.movimientos['llegada'] = getDatosLlegada();
						break;

						case 'aero-hotel-aero':
							data.movimientos['llegada'] = getDatosLlegada();
							data.movimientos['salida'] = getDatosSalida();
						break

						case 'hotel-aero':
							data.movimientos['salida'] = getDatosSalida();
						break;
						// TODO: Pendiente implementar	
						//case 'hotel-hotel':
						//	data.hotel_inter_id = $(selHotelInter).val();
						//	data.movimientos['inter'] = getDatosInter();
						//break;
					}

					var $btn = $(this).button('loading');
					var wstReserva = new reservaCollection();
					var options = {
							beforeSend : function() {
								$('body').loading('start');
								$(alerta).hide();
								$(alertaDetalle).empty();
							},
							success : function(res, textStatus, jqXHR) {
								if (jqXHR.xhr.status == 201) {
									$btn.button('reset');
									var resJSON = jqXHR.xhr.responseJSON;
									delete resJSON._links;
									resJSON['webpay'] = ($(pagoOpcion).is(':checked')) ? true : false;
									$(formTransfer)[0].reset();
									sendMail(resJSON);
								}
							},
							selfcall : function() {
								wstReserva.create(data,options);
							},
							error : function(res, handler) {
								$(alertaDetalle).empty();
								$btn.button('reset');
								if (handler.responseJSON.hasOwnProperty('trace')) {
									let l = handler.responseJSON.trace.length;
									if (l > 0) {
										var traces = handler.responseJSON.trace;
										for (let i = 0; i < l; i++) {
											if (traces[i].priorityName == 'CRIT') continue;
											let mensaje = $("<p></p>");
											$(mensaje).append(traces[i].message);
											$(alertaDetalle).append(mensaje);
										}
										$(alerta).show();
									}
								} else {
									let mensaje = $("<p></p>");	
									$(mensaje).append(handler.responseJSON.detail);
									$(alertaDetalle).append(mensaje);
									$(alerta).show();
								}

								$('body').loading('stop');
							}
						};

					wstReserva.create(data,options);
				}
			});

			$(pagoOpcion).on('change',function() {
    			if( $(this).is(':checked') ) {
        			$(pagoForma).html('<a href="#" class="btn btn-success btn-lg disabled" role="button" aria-disabled="true">Pago en linea seleccionado</a><img src="images/arrow.png" /> - <a href="#" class="btn btn-outline-secondary btn-lg disabled" role="button" aria-disabled="true">Deseo pagar en aeropuerto</a>');
    			} else {
        			$(pagoForma).html('<a href="#" class="btn btn-primary btn-lg disabled" role="button" aria-disabled="true">Pago en aeropuerto seleccionado</a><img src="images/arrow.png" /> - <a href="#" class="btn btn-outline-secondary btn-lg disabled" role="button" aria-disabled="true">Deseo pagar en linea</a>');
    			}
    		});

    		$(hotelNoFound).on('change',function() {
    			if( $(this).is(':checked') ) {
    				$(divInpHotel).show();
    				$(divSelHotel).hide();
    				$(selHotel).selectpicker('val','');
    			} else  {
    				$(divInpHotel).hide();
    				$(divSelHotel).show();    				
    			}
    		});
		}

		var getDatosLlegada = function() {
			return {
				fecha :$(fechaLlegada).val(),
				hora : $(horaLlegada).val(),
				num_vuelo : $(numVuelo).val(),
				aerolinea : $(aerolinea).val()
			}
		}

		var getDatosSalida = function() {
			return {
				fecha : $(fechaSalida).val(),
				hora : $(horaPickup).val(),
				hora_vuelo : $(horaVueloSalida).val()
			}			
		}

		var setPreReserva = function(args) {
			var item = getItem({
						items : traslado_data,
						member : 'uuid',
						value  : args.wst_tipo_traslado,
						return : [
								{ name : 'clave'}
							]
					});
			if (item !== false) {
				if (item.clave == 'aero-hotel' || item.clave == 'aero-hotel-aero') {
					$(fechaLlegada).val(args.wst_fecha);
				}

				if (item.clave == 'hotel-aero') {
					$(fechaSalida).val(args.wst_fecha);
				}

				$(selHotel).selectpicker('val',args.wst_hotel);
				$(selTipo).selectpicker('val',args.wst_tipo_traslado);
			}
		}

		var sendMail = function(args) {
			args['ROL_CLIENT_ID'] = ROL_CLIENT_ID;
			$.ajax({
				url :ROL_URL_EMAIL,
				data : args,
				type : 'POST',
				dataType : 'json',
				success: function(res) {
					var params = {
						ROL_CLIENT_ID : args.ROL_CLIENT_ID,
						costo : args.tarifa,
						servicio_nombre : args.servicio_nombre,
						traslado_nombre : args.traslado_nombre,
						folio: args.folio,
						webpay : args.webpay
					};
										
					$('body').loading('stop');
					resumenReserva(params);
				},
				error: function() {
				}
			});
		}

		var resumenReserva = function(params) {
    		var form = document.createElement("form");
    		form.setAttribute("method", 'POST');
    		form.setAttribute("action", ROL_URL_PAY);
    		for(var key in params) {
        		if(params.hasOwnProperty(key)) {
            		var hiddenField = document.createElement("input");
            		hiddenField.setAttribute("type", "hidden");
            		hiddenField.setAttribute("name", key);
            		hiddenField.setAttribute("value", params[key]);
            		form.appendChild(hiddenField);
         		}
    		}

    		document.body.appendChild(form);
    		form.submit();
		}

		var selectMap = function (args) {
			args.items.map(function(item) {
				option = $('<option/>');
		     	option.attr('value',item.get('uuid')).text(item.get('nombre'));
		     	$(args.el).append(option);
			})
		}

		var getTarifa = function() {
			var servicio = servicio_uuid;
			var tipo = $(selTipo).val();
			var hotel = $(selHotel).val();

			var item = getItem({
				items : traslado_data,
				member : 'uuid',
				value  : tipo,
				return : [
					{ name : 'clave'}
				]
			});

			if (item !== false) {
				if (item.clave == 'aero-hotel' || item.clave == 'aero-hotel-aero') {
					fecha = $(fechaLlegada).val();
				}

				if (item.clave == 'hotel-aero') {
					fecha = $(fechaSalida).val();
				}
			}

			if ((servicio !== undefined && servicio != '') && 
				(tipo !== undefined && tipo != '') &&
				(hotel !== undefined && hotel != '') && 
				(fecha !== undefined && fecha != '')) {

				var data = {
					servicio : servicio,
					tipo : tipo,
					hotel : hotel,
					fecha : fecha,
					pasajeros : parseInt($(selPasajeros).val())
				}

				var tarifa = new tarifaCollection();
				var options = {
						data : data,
						success : function(data) {
							var costo = 0;
							if (data.models[0] !== undefined) {
								costo = data.models[0].get('costo');
							}

							$(tarifa_precio).text(costo);
						},
						selfcall : function() {
							tarifa.fetch(options);
						},
						beforeSend: function() {
							$(tarifa_precio).text('.....');
						},
					};

				tarifa.fetch(options);
			} else {
				$(tarifa_precio).text('0');
			}
		}

		var getItem = function(args) {
			var l = args.items.models.length;
			if (l == 0) return false;
			var res = false;

			for (let i = 0; i < l ; i++) {
				var item = args.items.models[i];				
				if (item.get(args.member) == args.value) {
					var  res = {};
					for( let j = 0 ; j < args.return.length ; j++) {
						res[args.return[j].name] = item.get(args.return[j].name);
					}

					break;
				}
			}

			return res;
		}

		return {
			init : function(args) {
				initializeObject();
				createObjects(args);
				addEventListeners();
			}
		}
	}(jQuery);

	apireserva.init(wstGlobal);
})