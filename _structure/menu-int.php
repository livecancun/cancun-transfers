<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="http://www.cancun-shuttle.com"><img src="../images/cancun-shuttle-logo.png" class="img-responsive" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="http://www.cancun-shuttle.com">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.cancun-shuttle.com/cancun-transfers-rates.php?&lng=en">Rates</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.cancun-shuttle.com/arrivals.php">Arrivals</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.cancun-shuttle.com/contact-us.php">Contact US</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.cancun-shuttle.com/es/">Español</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>