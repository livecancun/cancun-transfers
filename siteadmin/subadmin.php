<?php
#====================================================================================================
# File Name : subadmin.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
#define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

include_once("../includes/common.php");
include_once($physical_path['DB_Access']. 'Admin.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : 'ViewAll');

# Initialize object
$admin = new Admin();

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Add admin
#-----------------------------------------------------------------------------------------------------------------------------
if($Action == 'Add' && $_POST['Submit'] == 'Save')
{
	if($admin->IsUsernameExists($_POST['user_login_id']))
	{
		$Error_Message = "Username already exists. Please, choose another one.";
	}
	else
	{
		$retVal = $admin->Insert(	$_POST['user_login_id'],		$_POST['user_password'],
									$_POST['admin_firstname'],		$_POST['admin_lastname'],
									$_POST['admin_phone_areacode']. '-'. $_POST['admin_phone_citycode']. '-'. $_POST['admin_phone_no'],
									$_POST['admin_email'],			$_POST['admin_privileges'],
									'admin_privileges_');

		header('location: subadmin.php?add=true');
		exit();
	}
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Update admin info
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Edit' && $_POST['Submit'] == 'Save')
{
	$retVal = $admin->Update(	$_POST['user_auth_id'],			$_POST['user_password'],
								$_POST['admin_firstname'],		$_POST['admin_lastname'],
								$_POST['admin_phone_areacode']. '-'. $_POST['admin_phone_citycode']. '-'. $_POST['admin_phone_no'],
								$_POST['admin_email'],			$_POST['admin_privileges'],
								'admin_privileges_');

	header('location: subadmin.php?save=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Login as selected subadmin
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == "Login")
{
	$rst = $admin->getAdmin($_POST['user_auth_id']);
	$user->doLogin($db->f('user_auth_id'), $db->f('user_perm'));
	header('location: ../'. strtolower($db->f('user_perm')). '/index.php');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Delete admin type
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == "Delete")
{
	$ret = $admin->Delete($_POST['user_auth_id']);
	header('location: subadmin.php?delete=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Delete admin type
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == "DeleteSelected")
{
	$ret = $admin->Delete($_POST['admin_list']);
	header('location: subadmin.php?seldelete=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
elseif($_POST['Submit'] == "Cancel")
{
	header('location: subadmin.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Show all admin
#-----------------------------------------------------------------------------------------------------------------------------
if(!in_array($Action, array('Add', 'Edit')))
{
	# Define required message
	if($_GET['add']==true)
		$succMessage = "SubAdmin information has been added successfully!!";
	elseif($_GET['save']==true)
		$succMessage = "SubAdmin information has been saved successfully!!";
	elseif($_GET['delete']==true)
		$succMessage = "SubAdmin information has been deleted successfully!!";
	elseif($_GET['seldelete']==true)
		$succMessage = "Selected SubAdmin information has been deleted successfully!!";
		
	$tpl->assign(array("T_Body"			=>	'subadmin_manage'. $config['tplEx'],
						"JavaScript"	=>  array("subadmin.js"),
						"A_Action"		=>	"subadmin.php",
						"succMessage"	=>	$succMessage,
						));

	# Show all admin
	$tpl->assign(array("SubAdminList"	=>	$admin->ViewAll(),
						));
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Add/Edit admin info
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Add' || $Action == 'Edit')
{
	$tpl->assign(array("T_Body"					=>	'subadmin_addedit'. $config['tplEx'],
						"JavaScript"			=>  array("subadmin.js"),
						"A_Action"				=>	"subadmin.php",
						"user_auth_id"			=>	$db->f('user_auth_id'),
						"Error_Message"			=>	$Error_Message,
						"Action"				=>	$Action,
						"AdminPrivilegesList"	=>	$lang['AdminPrivileges'],
						));

	# If edit, show existing info
	if($Action == 'Edit')
	{
		$rst = $admin->getAdmin($_POST['user_auth_id']);

		$admin_phone	=	ereg_replace('[-()\ ]', '', $db->f('admin_phone'));

		$tpl->assign(array("user_auth_id"			=>	$db->f('user_auth_id'),
							"user_login_id"			=>	$db->f('user_login_id'),
							"admin_firstname"		=>	$db->f('admin_firstname'),
							"admin_lastname"		=>	$db->f('admin_lastname'),
							"admin_email"			=>	$db->f('admin_email'),
							"admin_phone_areacode"	=>	substr($admin_phone, 0,3),
							"admin_phone_citycode"	=>	substr($admin_phone, 3,3),
							"admin_phone_no"		=>	substr($admin_phone, 6,4),
							"admin_privileges"		=>	$admin->DecryptPrivilege($db->f('admin_privileges')),
							));
	 }
	 else
	 {
		$tpl->assign(array("user_auth_id"			=>	$_POST['user_auth_id'],
							"user_login_id"			=>	$_POST['user_login_id'],
							"admin_firstname"		=>	$_POST['admin_firstname'],
							"admin_lastname"		=>	$_POST['admin_lastname'],
							"admin_email"			=>	$_POST['admin_email'],
							"admin_phone_areacode"	=>	$_POST['admin_phone_areacode'],
							"admin_phone_citycode"	=>	$_POST['admin_phone_citycode'],
							"admin_phone_no"		=>	$_POST['admin_phone_no'],
							));
	 }

}
$tpl->display('default_layout'. $config['tplEx']);
?>