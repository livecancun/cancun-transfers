<?php
#====================================================================================================
# File Name : page.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
#define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

if($_GET['Action'] == 'View')
	define('POPUP_WIN', 	true);

include_once("../includes/common.php");
include($physical_path['DB_Access']. 'Page.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : 'ShowAll');

//echo $Action,  $_POST['SubAction'];

# Initialize object
$page = new Page();

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Add page
#-----------------------------------------------------------------------------------------------------------------------------
if($Action == 'Add' && $_POST['Submit'] == 'Save')
{
	$ret = $page->Insert($_POST['page_type'],			$_POST['page_url'],
						$_POST['page_title'],			$_POST['page_browser_title'],
						$_POST['page_metatitle'],		$_POST['page_metakeyword'],
						$_POST['page_metadesc'],		$_POST['page_content'],
						$_POST['page_parent_id']	
						);

	header('location: page.php?add=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Edit' && $_POST['Submit'] == 'Save')
{
	/*print_r($_POST['page_content']);
	print $_POST['page_content'][0]."<br>";
	print $_POST['page_content'][1];
	die;*/
	//print_r($_POST); die;
	$ret = $page->Update($_POST['page_id'], 			$_POST['page_title'],
						$_POST['page_browser_title'],	$_POST['page_url'],
						$_POST['page_metatitle'],		$_POST['page_metakeyword'],
						$_POST['page_metadesc'],		$_POST['page_content'],
						$_POST['page_parent_id']
						);

	header('location: page.php?edit=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Delete')
{
	$ret = $page->Delete($_POST['pid']);

	header('location: page.php?delete=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Sort' && $_POST['Submit'] == 'Save')
{
	$ret = $page->Sort($_POST['page_order']);
	header('location: page.php?sort=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Toggle Display
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'ChangeStatus')
{
	$ret = $page->ToggleStatus($_POST['pid'], $_POST['status']);

	header('location: page.php?status='. $_POST['status']);
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
elseif($_POST['Submit'] == "Cancel")
{
	header('location: page.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Show page list
#-----------------------------------------------------------------------------------------------------------------------------
if(!in_array($Action, array('Add', 'Edit', 'Sort', 'View')))
{
	if($_GET['add']==true)
		$succMessage = "Page content has been added successfully!!";
	elseif($_GET['save']==true)
		$succMessage = "Page content has been updated successfully!!";
	elseif($_GET['delete']==true)
		$succMessage = "Page has been deleted successfully!!";
	elseif($_GET['sort']==true)
		$succMessage = "Page order has been set successfully!!";
	elseif($_GET['status']==true)
		$succMessage = "Page status has been changed successfully!!";
	
/*	print "<pre>";
	print_r($page->getSubPage());
	print "</pre>";
	die;*/
	$tpl->assign(array("T_Body"			=>	'page_manage'. $config['tplEx'],
						"JavaScript"	=>  array("page.js"),
						"succMessage"	=>	$succMessage,
						"Action"		=>	$Action,
						//"PageInfo"		=>	$page->ViewAll(),
						'R_RecordSet'	=>	$page->getSubPage(),
						));
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Add/Edit Page
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Add' || $Action == 'Edit')
{
	$tpl->assign(array("T_Body"			=>	'page_addedit'. $config['tplEx'],
						"JavaScript"	=>  array("page.js"),
						"Action"		=>	$Action,
						"Language"		=>	$LangInfo,
						"page_list"		=>	$page->getKeyValueArray(),
						));

	if($Action == 'Edit')
	{
		$rsPage = $page->getPage($_GET['pid']);
	
		$tpl->assign(array("page_id"				=>	$rsPage['page_id'],
							//"page_title"			=>	$rsPage['page_title'],
							//"page_browser_title"	=>	$rsPage['page_browser_title'],
							//"page_type"				=>	$rsPage['page_type'],
							//"page_url"				=>	$rsPage['page_url'],
							//"page_metatitle"		=>	$rsPage['page_metatitle'][0],
							//"page_metakeyword"		=>	$rsPage['page_metakeyword'][0],
							//"page_metadesc"			=>	$rsPage['page_metadesc'][0],
							//"page_content"			=>	$rsPage['page_content'][0],
							"rsPage"				=>	$rsPage,
							//"page_highlight"		=>	$rsPage->page_highlight,
							));
	 }
	 else
	 {
		$tpl->assign(array(	"page_type"				=>	$utility->fillArrayCombo($arrPageType),
							"page_browser_title"	=>	$config[WC_SITE_TITLE],
							"page_metatitle"		=>	$config[WC_META_TITLE],
							"page_metakeyword"		=>	$config[WC_META_KEYWORD],
							"page_metadesc"			=>	$config[WC_META_DESCRIPTION],
						));
	 }
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Add/Edit Page
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Sort')
{
	$tpl->assign(array("T_Body"			=>	'page_sort'. $config['tplEx'],
						"JavaScript"	=>  array("page.js"),
						"succMessage"	=>	$succMessage,
						"Action"		=>	$Action,
						));

	$tpl->assign(array("PageInfo"		=>	$page->ViewAll(PAGE_VISIBLE),
						));
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Show Page
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'View')
{
	$tpl->assign(array("T_Body"			=>	'page_view'. $config['tplEx'],
						"PageInfo"		=>  $page->getPage($_GET['pid']),
						));
}

$tpl->display('default_layout'. $config['tplEx']);
?>