<?php
#====================================================================================================
# File Name : changepassword.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
#define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

include_once("../includes/common.php");
include_once($physical_path['DB_Access']. 'Admin.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : 'View');

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Config information
#-----------------------------------------------------------------------------------------------------------------------------
if($_POST['Submit'] == "Update")
{
	if($user->ChangePassword($user->User_Id, $_POST['user_old_password'], $_POST['user_new_password']))
	{
		header('location: changepassword.php?update=true');
		exit();
	}
	else
		$Error_Message = "Old password confirmation does not match!!!";
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
else if($_POST['Submit'] == "Cancel")
{
	header('location: index.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------

if($_GET['update']==true)
	$succMessage = "Password  has been changed successfully!!";

$rst = $user->getLogin($user->User_Id);

$tpl->assign(array("T_Body"			=>	'subadmin_changepassword'. $config['tplEx'],
					"JavaScript"	=>  array("login.js"),
					"user_login_id"	=>	$db->f('user_login_id'),
					"Error_Message"	=>	$Error_Message,
					"succMessage"	=>	$succMessage));

$tpl->display('default_layout'. $config['tplEx']);
?>