<?php
#====================================================================================================
# File Name : emailtemplate.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_ADMIN', 	true);
if($_GET['Action'] == 'View')
	define('POPUP_WIN', 	true);

# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'EmailTemplate.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : 'ShowAll');

# Initialize object with required module
$emailTmpl = new EmailTemplate();

$scriptName = "emailtemplate.php";

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Add email template
#-----------------------------------------------------------------------------------------------------------------------------
if($Action == 'Add' && $_POST['Submit'] == 'Save')
{
	$ret = $emailTmpl->Insert($_POST);
	header("location: $scriptName?add=true");
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Save email template
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Edit' && $_POST['Submit'] == 'Save')
{
	$ret = $emailTmpl->Update($_POST['pk'], $_POST);					
	header("location: $scriptName?save=true");
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Delete email template
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == "Delete")
{
	$ret = $emailTmpl->Delete($_POST['pk']);
	header("location: $scriptName?delete=true");
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Delete email template
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == "DeleteSelected")
{
	$ret = $emailTmpl->Delete($_POST['pk_list']);
	header("location: $scriptName?delete=true");
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
else if($_POST['Submit'] == 'Cancel')
{
	header("location: $scriptName");
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
if(!in_array($Action, $emailTmpl->Data['C_CommandList']))
{
	if($_GET['add']==true)
		$succMessage = $emailTmpl->Data['L_Module']. " has been added successfully!!";
	elseif($_GET['save']==true)
		$succMessage = $emailTmpl->Data['L_Module']. " has been saved successfully!!";
	elseif($_GET['delete']==true)
		$succMessage = $emailTmpl->Data['L_Module']. " been deleted successfully!!";

	$tpl->assign(array('T_Body'			=>	'custom_manage'. $config['tplEx'],
						'A_Action'		=>	$scriptName,
						'alphaSort'		=>	false,
						'Action'		=>	$Action,
						'L_Module'		=>	$emailTmpl->Data['L_Module'],
						'H_HelpText'	=>	$emailTmpl->Data['H_Manage'],
						'F_PrimaryKey'	=>	$emailTmpl->Data['F_PrimaryKey'],
						'F_HeaderItem'	=>	$emailTmpl->Data['F_HeaderItem'],
						'C_CommandList'	=>	$emailTmpl->Data['C_CommandList'],
						'PageSize'		=>	$asset['OL_PageSize'],
						'R_RecordSet'	=>	$emailTmpl->ViewAll(),
						'total_record'	=>	$emailTmpl->total_record,
						'succMessage'	=>	$succMessage,
						));
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Add/Edit email template information
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == A_ADD || $Action == A_EDIT)
{
	if($Action == A_EDIT)
	{
		$ret = $emailTmpl->getEditFieldInfo($emailTmpl->getInfoById($_GET['pk']), $emailTmpl->Data['F_FieldInfo']);
	}

	$tpl->assign(array('T_Body'			=>	'custom_addedit'. $config['tplEx'],
						'A_Action'		=>	$scriptName,
						'Action'		=>	$Action,
						'L_Module'		=>	$emailTmpl->Data['L_Module'],
						'H_HelpText'	=>	$emailTmpl->Data['H_AddEdit'],
						'F_FieldInfo'	=>	$emailTmpl->Data['F_FieldInfo'],
						'PK'			=>	$_GET['pk'],
						));
}
#-----------------------------------------------------------------------------------------------------------------------------
#	View email template information
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == A_VIEW)
{
	$ret = $emailTmpl->getEditFieldInfo($emailTmpl->getInfoById($_GET['pk']), $emailTmpl->Data['F_FieldInfo']);
	
	$tpl->assign(array('T_Body'			=>	'custom_view'. $config['tplEx'],
						'A_Action'		=>	$scriptName,
						'L_Module'		=>	$emailTmpl->Data['L_Module'],
						'F_FieldInfo'	=>	$emailTmpl->Data['F_FieldInfo'],
						'PopupSize'		=>	$emailTmpl->Data['C_PopupSize'],
						));
}
$tpl->display('default_layout'. $config['tplEx']);
?>