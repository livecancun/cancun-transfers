<?php
#====================================================================================================
# File Name : index.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------

define('IN_ADMIN', 	true);
include_once("../includes/common.php");

$tpl->assign(array("T_Body"				=>	'home'. $config['tplEx'],
					"JavaScript"		=>  array("home.js"),
					"A_Action"			=>	"index.php",
					));

$tpl->display('default_layout'. $config['tplEx']);
?>