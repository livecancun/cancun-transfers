<?php
#====================================================================================================
# File Name : resevation.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_ADMIN', 	true);

if($_GET['Action'] == 'View')
	define('POPUP_WIN', 	true);

# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'Cart.php');
include_once($physical_path['DB_Access']. 'Reservation.php');
include_once($physical_path['DB_Access']. 'Destination.php');
include_once($physical_path['DB_Access']. 'UserMaster.php');

# Initialize object with required module
$objCart	= new Cart();
$objRes		= new Reservation();
$objDest 	= new Destination();
$objUser 	= new UserMaster();

$scriptName = "reservation.php?&start=$start_record";

//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 10;
$num_records = '';


//==================================================================================================
// Show All
//==================================================================================================
if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL ||  $_POST['Submit']==CANCEL || $_POST['Submit']=="Back To Search" ||  $_POST['Submit']==BACK)
{
		$tpl->assign(array( 'T_Body'		=>	'user_showall'. $config['tplEx'],
							'JavaScript'	=>	array('user.js'),
							'A_Action'		=>	$scriptName,
							));
	
		if($add == true)
		{
			$succ_message = $lang['Content_Added'];
		}
		if($update == true)
		{
			$succ_message = $lang['Content_Updated'];
		}
		if($delete == true)
		{
			$succ_message = $lang['Content_Deleted'];
		}
		if($_POST['Action']== "ShowFrame")
		{
			$tpl->assign(array ("File_Name"     => $_POST['file_name'],
								"Height"		=> 700));
		}
		
		$tpl->assign(array(	"Heading"				=>	$lang['Heading_Maintain_Reservation'],
							"Message"				=>  $succ_message,
							"ErrorMessage"			=>  $err_message,
							"L_Reservation"			=>	$lang['L_Reservation'],
							"L_Manage_Reservation"	=>	$lang['L_Manage_Reservation'],
							"Confirm_Delete"		=>	$lang['Confirm_Delete'],
							"Msg"					=>	$lang['select'],
							"MsgLink"				=>	$lang['ConfirmLink'],
							"L_User_Details"		=>  $lang['L_User_Details'],
							"L_Tour"				=>  $lang['L_TourReservation'],
							"L_Transfer"			=>  $lang['L_TransferReservation']));
	
		if($delete == true)
		{
			$succ_message = $lang['Msg_User_Deleted'];
		}
		if($nodelete == true)
		{
			$succ_message = $lang['Msg_User_NoDeleted'];
		}
	
		$tpl->assign(array(	"ACTION"			=>  SHOW_ALL,
							"start_record"		=>	$start_record,
							"Message"			=>  $succ_message,
							"ErrorMessage"		=>  $err_message,
							"Confirm_Delete"	=>	$lang['msgConfirmDeleteAll'],
							"L_User_Details" 	=> 	$lang['L_Reservation_Details'],
							"L_ConfirmationNo"  => 	$lang['L_ConfirmationNo']
							));
							
		$tpl->assign(array( "SrNo"				=> $lang['SrNo'],
							"L_Name"			=> $lang['name'],
							"L_User_Name"		=> $lang['L_User_Name'],
							"L_Email"			=> $lang['email'],
							"Action"			=> $lang['Action'],
							"View"					=>  $lang['View'],
							"L_User_Details"        =>  $lang['L_User_Details'], 
							"L_Reservation_Details" =>  $lang['L_Reservation_Details'],
							"Delete"   				=>  $lang['Delete'],							
							'UserInfo'				=> 	$objUser->ViewUsers($start_record,$Page_Size),
							));
	
		$num_records	= $objUser->UsersRecords();

		if($num_records >= $Page_Size)
			$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));
	
	}		

	elseif($_POST['Action']==SHOW || $_GET['Action']==SHOW)
	{

		$tpl->assign(array( 'T_Body'		=>	'user_detail'. $config['tplEx'],
							'JavaScript'	=>	array('user.js'),		
							'A_Action'		=>	$scriptName,
							));

		$tpl->assign(array( "A_User"			=>  "user.php?&start=$start_record",
							"ACTION"			=>  SHOW,
							"start_record"		=>	$start_record,
							"Message"			=>  $succ_message,
							"user_id"			=>  $_POST['user_id'],
							"Back"				=>  $lang['Back'],
							"L_User_Details"    =>  $lang['L_User_Details']
							));
		
		$tpl->assign(array( "L_User_Name"		 => $lang['L_User_Name'],
							"L_Name"			 => $lang['name'],
							"L_Address"			 => $lang['address'],
							"L_Country"			 => $lang['country'],
							"L_State"			 => $lang['state'],
							"L_City"			 => $lang['city'],
							"L_Zip"				 => $lang['zip'],
							"L_Phone_Number"	 => $lang['phoneno'],
							"L_Email"			 => $lang['email']
						));	
	

		$sql = $objUser->ShowUser($_POST['user_id']);
				
		if($db->num_rows() <= 0)
				$tpl->assign("Notfound_Message",$lang['Msg_User_Norecord']);
		else
		{
				$db->next_record();
				$tpl->assign(array( "userName"			=>	$db->f('email'),
									"Firstname"  	 	=>	ucfirst(stripslashes($db->f('firstname'))),
									"Lastname"			=>  ucfirst(stripslashes($db->f('lastname'))),
									"V_Address1"        =>  stripslashes($db->f("address1")),
									"V_Address2"        =>  stripslashes($db->f("address2")),
									//"CountryName"		=>	stripslashes($db->f('CountryName')),
									"CountryName"		=>	stripslashes($db->f('country')),
									"V_City"			=>	stripslashes($db->f('city')),
									"V_State"			=>	stripslashes($db->f("state")),
									"V_Zip"		        =>	$db->f("zip"),
									"V_Phone_Number"	=>	$db->f("phoneno"),
									"V_Email"			=>	stripslashes($db->f("email"))
									));
		 }
	
		
	}

	elseif($_POST['Action']==DELETE || $_GET['Action']==DELETE)
	{
			$status = $objCart -> Delete_Reservation_Detail($_POST['user_id'],$_POST['confirmationNo']);
			header("location: reservation.php?&delete=true");
	}
	
	$tpl->display('default_layout'. $config['tplEx']);
	
?>