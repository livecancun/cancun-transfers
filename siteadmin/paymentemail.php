<?php
#====================================================================================================
# File Name : paymentemail.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
  define("IN_ADMIN", true);
 
 if($_POST['Action']=="Print" || $_GET['Action']=="Print")
		define("POPUP_WINDOW", true);
		
	include_once("../includes/common.php");

	include($physical_path['DB_Access']."Cart.php");
	include_once($physical_path['DB_Access']. 'User.php');
	include_once($physical_path['DB_Access']. 'UserMaster.php');	

	# Initialize object with required module
	$objCart 	= new Cart();
	$objAuthUser= new User();		
	$objUser 	= new UserMaster();
	
//==================================================================================================
if($_POST['Submit'] == SEND  || $_POST['Submit'] == "Enviar")
{
	$email = $_POST['toEmail'];
	$from  = $_POST['fromEmail'];
	$subject = $_POST['subject'];
	$message = stripslashes(nl2br($_POST['message']));
	// print $message;exit;
	// stripslashes(nl2br($message));
	
	$headers = "From:" . $from . "\nReply-To: " .$from."\nContent-Type:text/html;charset=iso-8859-1;";
	@mail("$email","$subject","$message","$headers");
	
	$Status = $objCart->chageRequestStatus($_POST['confirmationNo'],'AR'); 

	header("location: reservationdetail.php?&confirmationNo=".$_POST['confirmationNo']."&update=true");
	exit;
}

if($_POST['Submit'] == CANCEL)
{
	 header("location: reservationdetail.php?&confirmationNo=".$_POST['confirmationNo']);
	exit;
}

//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 10;
$num_records = '';

#====================================================================================================
if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL || $_POST['Action']==BACK || $_POST['Action'] == SHOWCART)
{
	$tpl->assign(array( 'T_Body'		=>	'custom_page'. $config['tplEx'],
						'JavaScript'	=>	array('paymentemail.js'),
						'A_Action'		=>	$scriptName,
						'L_Module'		=>	$objRate->Data['L_Module'],
						'H_HelpText'	=>	$objRate->Data['H_Manage'],
						'includeFile'	=>	'paymentemail'. $config['tplEx'],
						));

	if($update == true)
	{
		$succ_message = $lang['Msg_CartDetail_Updated'];
	}
	if($requestsent == true)
	{
		$succ_message = $lang['Msg_Request_Sent'];
	}
	if($delete == true)
	{
		$succ_message = $lang['Msg_CartDetail_Deleted'];
	}
	
	$confirmationNo = isset($_POST['confirmationNo'])?$_POST['confirmationNo']:$_GET['confirmationNo'];
	$db->free();
	$tpl->assign(array( "A_Reservation"			=> "reservationdetail.php?&start=$start_record",
						"ACTION"			    =>  SHOW_ALL,
						"Send"					=> $lang['Send'],	
						"Cancel"    	  		=> $lang['cancel'],
						"L_To"					=> $lang['L_To'],
						"L_From"				=> $lang['L_From'],
						"L_subject"				=> $lang['L_Subject'],
						"L_message"				=> $lang['L_Message'],
						"L_Reservation_Details"	=> $lang['L_Reservation_Details'],
						"L_approveRequest"		=> $lang['Note_AR'],
						"L_Note"				=> $lang['L_Note'],
						"adminMessage"			=> $lang['adminMessage'],			
						"L_ConfirmationNo"  	=> $lang['L_ConfirmationNo'],
						"V_ConfirmationNo"		=> $confirmationNo,
						"Message"				=>  $succ_message
						));
	 	
		$sql = $objUser->ShowConfrmAuthUser($confirmationNo);
		$db->next_record();

		$firstname = ucfirst(stripslashes($db->f('firstname')));
		$lastname =	ucfirst(stripslashes($db->f('lastname')));
		
		$tpl->assign(array( "Firstname"  	 	 =>	ucfirst(stripslashes($db->f('firstname'))),
						    "Lastname"	    	 =>	ucfirst(stripslashes($db->f('lastname'))),
							"Email"				 =>	stripslashes($db->f("email"))
							));
										
		//========================== Support Email ===============================//
		$support_email = $config[WC_SUPPORT_EMAIL];
				
		$tpl->assign(array("adminEmail"     => $support_email,
						   "V_subject"		=> $lang['L_Payment_Instruction']
							));
				
		$url="http://#";
		$message = $lang['L_PaymentMail_Detail_1'].$firstname." ".$lastname.",";
		$message .= $lang['L_PaymentMail_Detail_2'];
		$message .= $confirmationNo;
		$message .= $lang['L_PaymentMail_Detail_3'];
		$message .= "\n\r<a href='http://www.Cancun-Transfers.com/paymentProcess.php?&lng=".$_SESSION['lng']."&confirmationNo=".md5($confirmationNo)."' target='_blank'>Payment Process Link</a>";
		$message .= $lang['L_PaymentMail_Detail_4'];
		
		$tpl->assign("V_message",$message);

		if($num_records >= $Page_Size)
			$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));
}


	$tpl->display('default_layout'. $config['tplEx']);


?>