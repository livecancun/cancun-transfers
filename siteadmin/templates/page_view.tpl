<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	{foreach from=$PageInfo item=Page}
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif"></td>
					<td class="stdSection" width="99%">{$Page->page_title}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						{$Page->page_content}
					</td>
				</tr>
				<tr><td colspan="2" height="10"></td></tr>
				<tr><td colspan="2" align="center"><input type="button" class="stdButton" value="Close Button" onClick="window.close();"></td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
			</table>
		</td>
	</tr>
	{/foreach}
</table>