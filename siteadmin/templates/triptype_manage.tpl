<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmTripType" action="{$A_Action}" method="post"> 
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">{$L_TripType_Manager}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						{$L_Manage_TripType}
					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td colspan="4" align="right">
						<a href="{$smarty.server.PHP_SELF}?Action=Add"><img src="{$Templates_Image}a_add.gif" class="imgAction" title="Add Page"></a>&nbsp;&nbsp;
						<!--a href="{$smarty.server.PHP_SELF}?Action=Sort"><img src="{$Templates_Image}a_sort.gif" class="imgAction" title="Sort Pages"></a-->&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td width="2%" class="listHeader">&nbsp;</td>
					<td class="listHeader" width="71%">{$L_TripType}</td>
					<td class="listHeader" width="15%">{$L_Visible}</td>
					<td class="listHeader" width="15%">{$Action}</td>
				</tr>
				{foreach name=TripTypeInfo from=$TripTypeInfo item=TripType}
				<tr class="{cycle values='list_A, list_B'}">
					<td><input type="checkbox" name="triptype_id1[]" class="stdCheckBox" value="{$TripType->triptype_id}"></td>
					<td>{$TripType.triptype_title }</td>
					<td align="center">
						{if $TripType.triptype_status}
							<b>Yes</b>
							(<a href="JavaScript: ToggleStatus_Click('{$TripType->triptype_id}', '0');" class="actionLink">No</a>)
						{else}
							<b>No</b>
							(<a href="JavaScript: ToggleStatus_Click('{$TripType->triptype_id}', '1');" class="actionLink">Yes</a>)
						{/if}
					</td>					
					<td align="center">
						<!--img src="{$Templates_Image}a_view.gif" class="imgAction" title="View" onClick="JavaScript: View_Click('{$TripType->triptype_id}');"-->&nbsp;
						<img src="{$Templates_Image}a_edit.gif" class="imgAction" title="Edit" onClick="JavaScript: Edit_Click('{$TripType.triptype_id}');">&nbsp;
						<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: Delete_Click('{$TripType.triptype_id}');">&nbsp;
						<!--img src="{$Templates_Image}a_email.gif" class="imgAction" title="Send Mail" onClick="JavaScript: SendMail_Click('{$TripType->triptype_id}');"-->
					</td>
				</tr>
				{foreachelse}
				<tr>
					<td colspan="5">No Trip Type available.</td>
				</tr>
				{/foreach}
			</table>
			{if $smarty.foreach.TripTypeInfo.total > 1}
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td>
						<img src="{$Templates_Image}arrow_ltr.png"> 
						<a href="JavaScript: CheckUncheck_Click(document.all['triptype_id1[]'], true);" onMouseMove="window.status='Check All';" onMouseOut="window.status='';" class="actionLink">Check All</a> / 
						<a href="JavaScript: CheckUncheck_Click(document.all['triptype_id1[]'], false);" onMouseMove="window.status='Uncheck All';" onMouseOut="window.status='';" class="actionLink">Uncheck All</a>  &nbsp;&nbsp;
						With selected
						<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: DeleteChecked_Click();">
					</td>
				</tr>
			</table>
			{/if}
				<!-- {include file='tree_node.tpl' node=$R_RecordSet level=0} -->
			</table>
			<br><br>
			<input type="hidden" name="Action">
			<input type="hidden" name="triptype_id">
			<input type="hidden" name="status">
		</td>
	</tr>
	</form>
</table>