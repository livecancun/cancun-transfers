<form name="frmRate" method="post" action="{$smarty.server.PHP_SELF}">
<table border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr><td align="right" colspan="3" class="mandatory">{$L_No_Gaps}</td></tr>
	<tr>
		<td class="listHeader" width="25%">{$L_Destinations}</td>
<!--		
		<td class="listHeader" width="10%" align="center">
			Passenger Range<br>
			<input name="passenger_starting_range[]" value="{$Record.passenger_starting_range}" size="5" readonly="true"/> -	
			<input name="passenger_ending_range[]" value="{$Record.passenger_ending_range}" size="5" onkeypress="isNumericKey()"  onchange="setStartRange(document.frmRate,'{$smarty.foreach.DestinationInfo.iteration}')"/>
		</td>
-->		
		{foreach name=PassengerRange from=$PassengerRange item=Passenger}
		<td class="listHeader" width="15%" align="center">{$L_Passenger_Range}<br>
			<input name="passenger_starting_range[]" value="{$Passenger.passenger_starting_range}" size="5" readonly="true"/> -	
			<input name="passenger_ending_range[]" value="{$Passenger.passenger_ending_range}" size="5" onkeypress="isNumericKey()"  onchange="setStartRange(document.frmRate,'{$smarty.foreach.PassengerRange.iteration}')"/>
		</td>
		{if $smarty.foreach.PassengerRange.last}{assign var=RangeStart value=$Passenger.passenger_ending_range+1}{/if}
		{/foreach}
		
		<td class="listHeader" width="15%" align="center">
			{$L_Add_New}<br>
			<input name="passenger_start" id="passenger_start" size="5" value="{$RangeStart|default:'1'}" readonly="true"/>	-	<input name="passenger_end" id="passenger_end" size="5" onkeypress="isNumericKey()"/>
		</td>
	</tr>
	
	{foreach name=DestinationInfo from=$DestinationInfo item=Destination}
	
	<tr class="{cycle values='list_A, list_B'}">

	  	<td>{$Destination.dest_title}&nbsp;&nbsp;</td>

	{foreach name=R_RecordSet from=$R_RecordSet item=Record}
	{assign var=PriceData value='price_data'|array_value:$Record}
		<td valign="top" class="Text" width="15%">
			<table border="0" cellspacing="3" cellpadding="0" align="center">
				<tr>
				 {foreach name=TripTypeInfo from=$TripTypeInfo item=TripType}
					<td nowrap="nowrap" align="center">
						{$TripType.triptype_title}&nbsp;&nbsp;<br>
						<input name="price_{$smarty.foreach.R_RecordSet.iteration}_{$Destination.dest_id}_{$TripType.triptype_id}" id="price_{$Record.passenger_id}_{$Destination.dest_id}_{$TripType.triptype_id}"  size="5" value="{$Record.price_data[$Destination.dest_id][$TripType.triptype_id]}" onkeypress="isNumericDotKey()"/>&nbsp;&nbsp;&nbsp;						
					</td>
				  {/foreach}
		   				  
				</tr>
		   </table>
 		</td>
	{/foreach}	

		<td valign="top" align="center">
			<table cellpadding="0" cellspacing="0">
				<tr class="list_C">
					<td valign="top" class="Text">
						<table border="0" cellspacing="3" cellpadding="0" align="center">
							<tr>
							 {foreach name=TripTypeInfo from=$TripTypeInfo item=TripType}
								<td nowrap="nowrap" align="center">
									{$TripType.triptype_title}&nbsp;&nbsp;<br>
									<input name="price_{$Destination.dest_id}_{$TripType.triptype_id}" id="price_{$Destination.dest_id}_{$TripType.triptype_id}"  size="5" onkeypress="isNumericDotKey()"/>&nbsp;&nbsp;
								</td>
							  {/foreach}
									  
							</tr>
					   </table>
					</td>
				</tr>
			</table>
		</td>
	 </tr>
	

	{/foreach}
	
	<tr bgcolor="#B5D7EF">

		<td align="center" width="10%">
			<!--{if $Passenger.passenger_id}		
				<b>Action</b>
			{/if}-->
		</td>
		
		{foreach name=PassengerRange from=$PassengerRange item=Passenger}
		<td valign="middle" align="center">
			<input name="Delete" type="submit" value="Delete" onclick="JavaScript: CDelete_Click(document.frmRate, '{$Passenger.passenger_id}');" class="greyButton"/>
<!--		<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: CDelete_Click(document.frmRate, '{$Passenger.passenger_id}');">-->
		</td>				
		{/foreach}
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="divider"></td>
		<td class="divider" colspan="{$smarty.foreach.PassengerRange.iteration}"></td>
		<td class="divider"></td>		
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td>&nbsp;</td>
		{if $Passenger.passenger_id}
		<td align="right" colspan="{$smarty.foreach.PassengerRange.iteration}">
			<input name="Update" type="submit" value="Update" onclick="return CUpdate_Click(document.frmRate)" class="stdButton" />			
			<input name="Reset" type="reset" value="Reset" class="stdButton"/>
		</td>
		{/if}	
		<td align="right">
			<input type="submit" name="Submit" value="Add" onclick="return CAdd_Click(document.frmRate)" class="stdButton">
		</td>
		<td valign="top">
			<input type="hidden" name="dest_cnt" value="{$smarty.foreach.DestinationInfo.total}"/>
			<input type="hidden" name="triptype_cnt" value="{$smarty.foreach.TripTypeInfo.total}"/>
			<input type="hidden" name="Action" value="{$Action}">
			<input type="hidden" name="pk">
		</td>		
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>

</table>
</form>