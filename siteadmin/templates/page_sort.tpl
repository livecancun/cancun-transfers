<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmPage" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="49%">Page Manager [{$Action}]</td>
					<td class="stdSection" width="50%" align="right"><img src="{$Templates_Image}icon_help.gif" title="Help">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						Change the order of the pages
					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td align="left" colspan="2">
						<input type="submit" name="Submit" value="Save" class="stdButton" onClick="SortPage_Click();">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
				</tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						Page Information
					</td>
				</tr>
				<tr><td colspan="2" class="fieldInputStyle" >&nbsp;</td></tr>
				<tr>
					<td class="fieldInputStyle" align="right" width="50%">
						<select name="page_list" size="15">
							{foreach name=pageInfo from=$PageInfo item=Page}
							<option value="{$Page->page_id}">{$Page->page_title}</option>
							{/foreach}
						</select>
					</td>
					<td class="fieldInputStyle">
						<img src="{$Templates_Image}btn_up.gif" class="imgAction" onClick="ChangeOrder_Click(-1);"><br><br>
						<img src="{$Templates_Image}btn_down.gif" class="imgAction" onClick="ChangeOrder_Click(1);">
					</td>
				</tr>
				<tr><td colspan="2" class="fieldInputStyle" >&nbsp;</td></tr>
				<tr><td class="divider" colspan="2"></td></tr>
				<tr>
					<td align="left" colspan="2">
						<input type="submit" name="Submit" value="Save" class="stdButton" onClick="SortPage_Click();">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
						<input type="hidden" name="Action" value="{$Action}">
						<input type="hidden" name="page_order">
					</td>
				</tr>
			</table>
			<br><br>
		</td>
	</tr>
	</form>
</table>