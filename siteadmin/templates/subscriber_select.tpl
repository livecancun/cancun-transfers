<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmSubscriberSelect" action="{$A_Action}" method="post" onSubmit="">
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				
				<tr><td height="2"></td></tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
				<tr><td height="2"></td></tr>
			</table>
				
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td class="listHeader" width="10%">&nbsp;</td>
					<td class="listHeader" width="60%">Email</td>
				</tr>
				{foreach from=$SubscriberInfo item=Subscriber}
				<tr class="{cycle values='list_A, list_B'}">
					<td><input type="checkbox" name="subscriber_list[]" id="subscriber_list" class="stdCheckBox" value="{$Subscriber->subscriber_email}" onClick="JavaScript: Item_Click(this, '{$smarty.get.fld}');"></td>
					<td width="15%">{$Subscriber->subscriber_email}</td>
				</tr>
				{foreachelse}
				<tr class="list_B">
					<td colspan="5" align="center">No Record Available</td>
				</tr>
				{/foreach}
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td>
						<img src="{$Templates_Image}arrow_ltr.png"> 
						<a href="JavaScript: Item_CheckUncheck(document.all['subscriber_list[]'], true,'{$smarty.get.fld}');" onMouseMove="window.status='Check All';" onMouseOut="window.status='';" class="actionLink">Check All</a> / 
						<a href="JavaScript: Item_CheckUncheck(document.all['subscriber_list[]'], false,'{$smarty.get.fld}');" onMouseMove="window.status='Uncheck All';" onMouseOut="window.status='';" class="actionLink">Uncheck All</a>  &nbsp;&nbsp;
					</td>
				</tr>
			</table>
			{if $total_record > $smarty.session.page_size}
			<br>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td valign="top" width="33%">
						{html_pager num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record image_path=$Templates_Image add_prevnext_text=true}
					</td>
					<td align="center">
						{html_pager_text num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record}
					</td>
					<td align="right"  width="33%">
						Goto Page: <input type="text" name="goto_page" size="5">
						<input type="submit" name="go" value="Go" class="stdFilterButton">
					</td>
				</tr>
			</table>
			{/if}			
			<table border="0" cellpadding="2" cellspacing="2" width="95%">
				<tr>
					<td align="center">
						<input type="button" name="btnClose" value="Close" class="stdFilterButton" onClick="window.close()">
					</td>
					<td align="right">
						{$Page_Link}
					</td>
				</tr>
			</table>
			<br><br>
		</td>
	</tr>
		<input type="hidden" name="Action" value="{$Action}">
		<input type="hidden" name="emp_id" value="{$emp_id}">
		<input type="hidden" name="SubAction" value="{$SubAction}">
		<input type="hidden" name="from" value="{$From}">
	</form>
</table>
<script language="javascript">CheckedSelectedItem(document.all['subscriber_list[]'], '{$smarty.get.fld}');</script>