<script>
	var Confirm_Delete = '{$Confirm_Delete}';
</script>


<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">{$L_Reservation}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						{$L_Manage_Reservation}
					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
<table border="0" cellpadding="0" cellspacing="0" width="750" align="center">
<form name="frmUser" action="{$A_Action}" method="post">
	<tr><td align="center" class="successMsg">{$Message}</td></tr>
	<tr><td align="center" class="errorMsg">{$ErrorMessage}</td></tr>
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr><td colspan="7" height="5"></td></tr>
                <tr>
					<td class="listHeader" width="8%" height="20" align="center">{$SrNo}</td>
					<td class="listHeader" width="20%" align="center">{$L_Email}</td>
                   	<td class="listHeader" width="15%" align="center">{$L_Name}</td>
					<td class="listHeader" width="20%" align="center">{$L_ConfirmationNo}</td>
                   	<td class="listHeader" width="37%" colspan="3" align="center">{$Action}</td>
                </tr>
                <tr><td colspan="7" height="5" class="successMsg" align="center">{$Notfound_Message}</td></tr>
					
					{foreach name=UserInfo from=$UserInfo item=Users}
					 {assign var="Sr_No" value=$smarty.foreach.UserInfo.iteration} 
					 
					 {if $Users.user_id}
					<tr class="{cycle values='list_A, list_B'}">
						<td class="List_B" align="center" height="20" width="8%">{$Sr_No}</td>
						<td class="List_B" align="center" width="20%">{$Users.email}</td>
						<td class="List_B" align="center" width="15%">{$Users.firstname} {$Users.lastname}</td>
						<td class="List_B" align="center" width="20%" >{$Users.confirmationNo}</td>
						<td class="List_B" align="center" width="12%">
							<a href="JavaScript: Show_Click('{$Users.index_id}');" class="actionLink">{$L_User_Details}</a>
						</td>
						<td class="List_B" align="center" width="18%">
							<a href="JavaScript: Reserve_Click('{$Users.confirmationNo}');" class="actionLink">{$L_Reservation_Details}</a>
						</td>
						<td class="List_B" align="center" width="12%">
							<a href="JavaScript: Delete_Click('{$Users.index_id}','{$Users.confirmationNo}');" class="actionLink">{$Delete}</a>
						</td>
					</tr>
					{/if}
					{/foreach}
					
                <tr><td colspan="7">&nbsp;</td></tr>
			</table>
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr height="15">
					<td class="paggingLine" width="10%">
						&nbsp;&nbsp;{$Page_Link}
					</td>
				</tr>
			</table>
			<br>
			<input type="hidden" name="user_id">
			<input type="hidden" name="confirmationNo">
			<input type="hidden" name="Action" value="{$ACTION}">
		</td>
	</tr>
	<tr><td><br></td></tr>
	</form>
</table>
		</td>
	</tr>

</table>


