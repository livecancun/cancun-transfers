<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmPage" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">Page Manager [{$Action}]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						Design your content. When you finish click <b>Save</b> to save the changes.
						Click <b>Cancel</b> to discard the changes.
					</td>
				</tr>
				<tr><td height="5"></td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td align="left">
						<input type="submit" name="Submit" value="Save" class="stdButton" onclick="JavaScript: return Validate_Form(document.frmPage);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
					<td align="right" class="mandatory">Fields marked with (<font class="mandatoryMark">*</font>) are mandatory.</td>
				</tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						Page Information
					</td>
				</tr>
				<tr>
					<td width="25%" class="fieldLabelLeft" valign="top">Page Type:</td>
					<td width="75%" class="fieldInputStyle">
						{if $Action== 'Edit'}
							{$rsPage.page_type}
						{else}
							<select name="page_type" onChange="PageType_Change();">
								{$page_type}
							</select>
						{/if}
					</td>
				</tr>
				<tr>
					<td width="25%" class="fieldLabelLeft" valign="top">Page Parent:</td>
					<td width="75%" class="fieldInputStyle">
							<select name="page_parent_id">
								<option value="0">ROOT</option>
								{html_options options=$page_list selected=$rsPage.page_parent_id}
							</select>
					</td>
				</tr>				
				<tr id='pageUrl' style="display:{if $rsPage.page_type=='iFrame' || $rsPage.page_type=='OutsideLink' || $rsPage.page_type=='InternalLink'}block{else}none{/if};">
				    <td width="25%" class="fieldLabelLeft" valign="top">Page URL: </td>
					<td width="75%" class="fieldInputStyle">
						<div id='eginternal' class="smalllink" style="">{$Site_Root}</div>
						<input type="text" value="{$rsPage.page_url}" name="page_url" size="40" maxlength="120"><br>
						<div id='eglink' class="smalllink">e.g. http://www.google.com</div>
					</td>
				</tr>
			</table>
			{foreach name=LanguageInfo from=$Language item=Lan}
			 {assign var="sr" value=$smarty.foreach.LanguageInfo.iteration-1} 
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr id='pageTitle_{$smarty.foreach.LanguageInfo.iteration}' style="display:{if $rsPage.page_type=='iFrame' || $rsPage.page_type=='OutsideLink'}none{else}else{/if};">
					<td width="25%" class="fieldLabelLeft" valign="top">Page Title: (<i>{$Lan.name}</i>)<font class="mandatoryMark">*</font></td>
					<td width="75%" class="fieldInputStyle">
						<input type="text" value="{$rsPage.page_title[$sr]}" name="page_title[]" size="40" maxlength="120">
					</td>
				</tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2"  width="95%" id="pageMeta_{$smarty.foreach.LanguageInfo.iteration}" style="display:{if $rsPage.page_type=='OutsideLink' || $rsPage.page_type=='InternalLink'}none{else}block{/if};">
				<tr >
					<td colspan="2" class="stdSubSection" align="center">
						({$smarty.foreach.LanguageInfo.iteration}) Meta Information for language {$Lan.name} 
					</td>
				</tr>
				<tr>
					<td width="25%" class="fieldLabelLeft" valign="top">Browser Title:</td>
					<td width="75%" class="fieldInputStyle">
						<input type="text" value="{if $Action=='Edit'}{$rsPage.page_browser_title[$sr]}{else}{$page_browser_title}{/if}" name="page_browser_title[]" size="60" maxlength="95">
					</td>
				</tr>
				<tr>
					<td width="25%" class="fieldLabelLeft" valign="top">Meta Title</td>
					<td width="75%" class="fieldInputStyle">
						<textarea name="page_metatitle[]" rows="2" cols="60">{if $Action=='Edit'}{$rsPage.page_metatitle[$sr]}{else}{$page_metatitle}{/if}</textarea><br>
						<font class="validationText">Maximum 100 characters </font><br>
						<b>Site Title</b> - Probably the most important attribute considered by search engines. 
						In many cases, you will want to use the term describing your services as well as your geographic location.
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Meta Description</td>
					<td class="fieldInputStyle">
						<textarea name="page_metadesc[]" rows="3" cols="60">{if $Action=='Edit'}{$rsPage.page_metadesc[$sr]}{else}{$page_metadesc}{/if}</textarea><br>
						<font class="validationText">Maximum 400 characters </font><br>
						<b>Meta Description</b> - This text appears in the listing for your site in search engine results 
						(e.g., a brief description of the services and products you offer, as well as your location). 
						Begin this description with your service title, and then include other keywords and/or phrases 
						describing your services. 						
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Meta Keyword</td>
					<td class="fieldInputStyle">
						<textarea name="page_metakeyword[]" rows="3" cols="60">{if $Action=='Edit'}{$rsPage.page_metakeyword[$sr]}{else}{$page_metakeyword}{/if}</textarea><br>
						<font class="validationText">Maximum 1000 characters </font><br>
						<b>Meta Keywords</b> - A list of terms and phrases search engines use to find and rank your site. 
						Your keywords should accurately describe your services, features, products, and location. 						
					</td>
				</tr>
			</table>			
			<table border="0" cellpadding="1" cellspacing="2" width="95%" id="pageContent_{$smarty.foreach.LanguageInfo.iteration}" style="display:{if $rsPage.page_type=='iFrame' || $rsPage.page_type=='OutsideLink' || $rsPage.page_type=='InternalLink'}none{else}block{/if};">
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						Page Content
					</td>
				</tr>
				<tr>
					<td class="fieldInputStyle" colspan="2" align="center">
						{html_richtext toolbar='full' width='95%' height='500' RichTextName='page_content[]' RichTextValue=$rsPage.page_content[$sr] lang=$LangInfo.code}
					</td>
				</tr>
			</table>
			
			{/foreach}
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td class="divider" colspan="4"></td></tr>
				<tr>
				  <td align="left" colspan="2">
						<input type="submit" name="Submit" value="Save" class="stdButton" onclick="JavaScript: return Validate_Form(document.frmPage);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
						<input type="hidden" name="page_id" value="{$page_id}">
						<input type="hidden" name="Action" value="{$Action}">
						<input type="hidden" name="lang_cnt" value="{$smarty.foreach.LanguageInfo.total}" />
					</td>
				</tr>
			</table>	
			<br>
		</td>
	</tr>
	</form>
</table>