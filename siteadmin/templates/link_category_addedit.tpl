<script language="javascript">
	var msg_CatName		= '{$msg_CatName}';
</script>

<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
<form name="frmCategory"  action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">{$L_Link_Manager} [ {$L_Action} ]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						{$L_Manage_Links}
					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
				  <td bgcolor="#FFFFFF">
				  
						<table border="0" align="center" cellpadding="0" cellspacing="0" width="740" >
							<tr>
								<td valign="top" align="center">
									<table border="0" cellpadding="0" cellspacing="1" width="750">
										<tr>
											<td valign="top" align="center">
												<table border="0" cellpadding="1" cellspacing="3" width="97%">
													<tr >
														<td width="35%">&nbsp;</td>
														<td width="65%">&nbsp;</td>
													</tr>
													<tr height="20">
														<td colspan="2" align="center" class="successMsg">&nbsp;{$Message}</td>
													</tr>
													<tr><td colspan="2">&nbsp;</td></tr>
													<tr>
														<td class="fieldLabelRight" >{$L_New_Category_Name} :</td>
														<td class="fieldLabel" ><input type="text" name="cat_name" value="{$cat_name}"  maxlength="255" ></td>
													</tr>
													<tr>
														<td class="fieldLabelRight">{$L_Category_Status} :</td>
														<td class="fieldLabel">
														<select name="cat_status">
														<option value="">{$L_Select_Status}</option>
														{$Status_List}
														</select>
														</td>
													</tr>
													<tr><td colspan="2">&nbsp;</td></tr>
													<tr><td colspan="2">&nbsp;</td></tr>
													<tr>
														<td colspan="8" align="center" >
															<input type="submit" name="Submit" value="{$Save}{$Update}" class="nrlButton" onClick="javascript: return Form_Submit(document.frmCategory);">
															<input type="submit" name="Submit" value="{$Cancel}" class="nrlButton">
														</td>
													</tr>
													<tr>
														<td colspan="2">
															<input type="hidden" name="Action" value="{$ACTION}">
															<input type="hidden" name="start" value="{$Start}">
															<input type="hidden" name="cat_id" value="{$cat_id}">
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>				  
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>