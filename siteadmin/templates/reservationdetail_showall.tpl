<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">Reservation</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td class="successMsg" align="center"> {$succMessage}</td></tr>
			</table>
<table border="0" cellspacing="0" cellpadding="0" width="750">
  <tr> 
    <td>
	<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" >
		<tr> 
		  <td colspan="5" height="1" bgcolor="#000066"></td>
		</tr>
	    <tr> 
			<td class="blueHeader" width="100%" colspan="5">{$L_Reservation_Details} -> {$L_ConfirmationNo} : {$V_ConfirmationNo}</td>
	    </tr>
		<tr> 
		  <td colspan="5" height="1" bgcolor="#000066"></td>
		</tr>	
		<tr><td align="center" class="successMsg" width="100%" colspan="5">{$Message}</td></tr>
		<tr><td align="center" class="errorMsg" width="100%" colspan="5">{$ErrorMessage}</td></tr>
			<FORM name="frmReservation" action="{$A_Reservation}" method="POST">
			   <tr>
			     <td class="listHeader" align="center" width="30%"  >{$L_cartItem}</td>
  				 <!--<td class="blueHeader" align="center" width="25%" >{$L_ItemType}</td>-->
  				 <td class="listHeader" align="center" width="15%">{$L_Total}</td>
				 <td class="listHeader" align="center" width="30%" colspan="3" >{$Action}</td>
			   </tr>
			    <tr><td class="successMsg" colspan="5"  align="center">{$Notfound_Message}</td></tr>

			{foreach name=CartInfo from=$CartInfo item=Cart}
			{assign var='TotalPrice' value=`$TotalPrice+$Cart.totalCharge`}
			<tr>
				<td class="List_B" align="center" height="25" >{$Cart.dest_title} </td>
				<td class="List_B" align="right">$ {$Cart.totalCharge}  </td>
				<td class="List_B" align="center"><a href="JavaScript: Show_Click('{$Cart.cart_id}');" class="lightgreyLink">{$View}</a></td>
				<td class="List_B" align="center"><a href="JavaScript: Delete_Click('{$Cart.cart_id}');" class="lightgreyLink">{$Delete}</a></td>
 			</tr>	
			{/foreach}
			
		 	<tr>
				<td align="right"><b>{$L_Total} ($):</b></td>
			   	<td align="right"><b>{$TotalPrice|number_format:2:'.'}</b>  </td>
			   	<td colspan="4">&nbsp;</td>
			</tr>
			
            {if $is_airline_empl == 1}
		 	<tr>
				<td align="right"><b>{$L_Discount} {$discount_rate}% :</b>{assign var='discount' value=`$discount_rate`}</td>
			   	<td align="right"><b>- $ {math equation="(x * y)/100" x=$TotalPrice y=$discount format="%.2f"}</b>  </td>
			   	<td colspan="4">&nbsp;</td>
			</tr>

		 	<tr>
				<td align="right"><b>{$L_Final} {$L_Total}:</b></td>
			   	<td align="right"><b>$ {math equation="(x-((x * y)/100))" x=$TotalPrice y=$discount format="%.2f"}</b>  </td>
			   	<td colspan="4">&nbsp;</td>
			</tr>
			{/if}
			
			<tr><td colspan="5"> <b>{$L_status} : {$Cart.cartStatus}</b></td></tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr><td colspan="5" align="center">

				{if $Cart.cartStatus == "NA" || $Cart.cartStatus == "PR"}
					<input type="submit" name="Submit" value="{$AR}" class="nrlButton">
				{/if}
				
				{if $Cart.cartStatus == "AR" || $Cart.cartStatus == "PR"}				
					<input type="submit" name="Submit" value="{$NA}" class="nrlButton">
				{/if}
				
				{if $Cart.cartStatus == "AR" || $Cart.cartStatus == "NA"}
				    <input type="submit" name="Submit" value="{$PR}" class="nrlButton">
				{/if}

				{if $Cart.cartStatus == "AR"}				
					 <input type="submit" name="Submit" value="{$PD}" class="nrlButton">
				{/if}
			
					<input type="button" name="Back" value="{$Back}" class="nrlButton" onclick="javascript:location.href('reservation.php')">
					<input type="hidden" name="user_id" value="{$user_id}"> 
					<input type="hidden" name="cartId" value="{$cartId}"> 
					<input type="hidden" name="packageCartId" value="{$packageCartId}"> 
					<input type="hidden" name="itemType"> 
				    <input type="hidden" name="confirmationNo" value="{$V_ConfirmationNo}"> 
			 	    <input type="hidden" name="Action" value="{$ACTION}">
				</td>
			</tr>
			
			</form>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr><td colspan="5"><b>{$Note} : </b></td></tr>
			<tr><td colspan="5">{$empl_comments}</td></tr>
			<tr> 
			  <td colspan="5" height="1" bgcolor="#000066"></td>
			</tr>
			<tr>
			  	<td colspan="5">
					<table width="100%">
					<tr>
						<td colspan="2" align="left"><b>NR</b> - {$NoRequest}</td>
						<td colspan="2" align="left" ><b>PR</b> - {$PendingRequest}</td>
						<td colspan="2" ><b>AR</b> - {$ApproveRequest}</td>
					</tr>
					<tr>
					<td colspan="2"><b>NA</b> - {$NotApprove}</td>
					<td colspan="2"><b>RP</b> - {$RequestPayment}</td>
					<td colspan="2"><b>PD</b> - {$PaymentDone}</td>
				   </tr>
					</table>
				</td>	
			</tr>
			<tr> 
					  <td colspan="5" height="1" bgcolor="#000066"></td>
			</tr>
      </table>
    </td>
 </tr>
 <tr>
	   <td colspan="5">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr height="15">
						<td class="paggingLine" width="10%">
							  {$Page_Link}
						</td>
					</tr>
				</table>
			</td>
		</tr>
</table>
</td>
	</tr>

</table>






