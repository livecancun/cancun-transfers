				{foreach name=recordInfo from=$node item=Record}
				<tr class="{cycle values='list_A, list_B'}">
					<td align="{$Field.Align|default:'left'}">
						&nbsp;&nbsp;
						{if $level > 0}
							&nbsp;&nbsp;&nbsp;&nbsp;{'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'|str_repeat:$level-1}&brvbar;---
						{/if}
						{$Record.page_title}
					</td>
					<td align="center" nowrap>
						<a href="../index.php?pid={$Record.page_id}" target="_blank"><img src="{$Templates_Image}a_view.gif" class="imgAction" title="View" ></a>
						<a href="?Action=Edit&pid={$Record.page_id}"><img src="{$Templates_Image}a_edit.gif" class="imgAction" title="Edit" ></a>
						{if $Record.page_default == 'No'}
							<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: Delete_Click('{$Record.page_id}');">
						{else}
							&nbsp;&nbsp;&nbsp;&nbsp;
						{/if}
					</td>
				</tr>
				{if $Record.child != ''}
					{include file='tree_node.tpl' node='child'|array_value:$Record level=`$level+1`}
				{/if}
				{foreachelse}
				<tr class="list_A">
					<td colspan="6" align="center">No {$L_Module} information available.</td>
				</tr>
				{/foreach}