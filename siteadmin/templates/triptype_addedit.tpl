<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmTripType" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">{$L_TripType_Manager} [{$Action}]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						{$L_Design_Content}
					</td>
				</tr>
				<tr><td height="5"></td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td align="left">
						<input type="submit" name="Submit" value="Save" class="stdButton" onclick="JavaScript: return Validate_Form(document.frmTripType);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
					<td align="right" class="mandatory">{$L_Mandatory_Fields}</td>
				</tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						{$L_TripType_Info}
					</td>
				</tr>
			</table>
			{foreach name=LanguageInfo from=$Language item=Lan}
			 {assign var="sr" value=$smarty.foreach.LanguageInfo.iteration-1} 
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr id='pageTitle_{$smarty.foreach.LanguageInfo.iteration}' style="display:{if $rsPage.page_type=='iFrame' || $rsPage.page_type=='OutsideLink'}none{else}else{/if};">
					<td width="25%" class="fieldLabelLeft" valign="top">{$L_TripType}: (<i>{$Lan.name}</i>)<font class="mandatoryMark">*</font></td>
					<td width="75%" class="fieldInputStyle">
						<input type="text" value="{$rsTripType.triptype_title[$sr]}" name="triptype_title[]" size="40" maxlength="120">
					</td>
				</tr>
			</table>
					
			{/foreach}
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td class="divider" colspan="4"></td></tr>
				<tr>
				  <td align="left" colspan="2">
						<input type="submit" name="Submit" value="Save" class="stdButton" onclick="JavaScript: return Validate_Form(document.frmTripType);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
						<input type="hidden" name="triptype_id" value="{$triptype_id}">
						<input type="hidden" name="Action" value="{$Action}">
						<input type="hidden" name="lang_cnt" value="{$smarty.foreach.LanguageInfo.total}" />
					</td>
				</tr>
			</table>	
			<br>
		</td>
	</tr>
	</form>
</table>