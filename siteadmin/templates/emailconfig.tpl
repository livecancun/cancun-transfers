<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmSiteConfig" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_settings.gif"></td>
					<td class="stdSection" width="49%">Email Config</td>
					<td align="right" width="50%">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						Change your email configuration and click <b>Update</b> to save the changes.
						Click <b>Cancel</b> to discard the changes.
					</td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
				<tr><td height="5"></td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td align="left">
						<input type="submit" name="Submit" value="Update" class="stdButton" onClick="javascript: return Form_Submit(document.frmSiteConfig);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
					<td align="right" class="mandatory">Fields marked with (<font class="mandatoryMark">*</font>) are mandatory.</td>
				</tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						Default Email Configuration
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top" width="20%">Email from:</td>
					<td class="fieldInputStyle">
						<input type="text" name="email_fromname" size="50" maxlength="120" value="{$email_fromname}"><br>
						Set the email from name.
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top" width="30%">Default Email [email_from]: <font class="mandatoryMark">*</font></td>
					<td width="70%" class="fieldInputStyle">
						<input type="text" name="support_email" size="50" maxlength="120" value="{$support_email}"><br>
						Set the Default email address for the <b>[email_from]</b> header.
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top" width="20%">Default Email [reply_to]:</td>
					<td class="fieldInputStyle">
						<input type="text" name="reply_to" size="50" maxlength="120" value="{$reply_to}"><br>
						Set the Default email address for the <b>[reply_to]</b> header.
					</td>
				</tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						Other configuration
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Default Email Signature:</td>
					<td class="fieldInputStyle">
						<textarea name="default_signature" rows="5" cols="60">{$default_signature}</textarea><br>
						This will be the default signature you can append to ALL emails.
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Default Email Footer:</td>
					<td class="fieldInputStyle">
						<textarea name="email_footer" rows="5" cols="70">{$email_footer}</textarea><br>
						This will be the default footer you can appended to ALL emails.
					</td>
				</tr>
				<tr><td class="divider" colspan="2"></td></tr>
				<tr>
					<td align="left" colspan="2">
						<input type="submit" name="Submit" value="Update" class="stdButton" onClick="javascript: return Form_Submit(document.frmSiteConfig);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
				</tr>
			</table>	
			<br>
		</td>
	</tr>
	</form>
</table>