<table border="0" cellpadding="1" cellspacing="2" width="95%">
	<tr>
		<td valign="top" align="center" colspan="4">
			<table border="0" cellpadding="5" cellspacing="2" width="100%">
			{foreach name=PrivilegeInfo from=$PrivilegesList key=Key item=Menu}
				{if $smarty.foreach.PrivilegeInfo.iteration|mod:2==1}
				<tr>
				{/if}
					<td align="left" valign="top" width="33%">
						<table border="0" cellpadding="1" cellspacing="2" width="200" class="stdTableBorder">
							<tr>
								<td class="stdSectionHeader">
									<table border="0" cellpadding="0" cellspacing="1" width="100%">
										<tr>
											<td width="20">
												<input type="checkbox" name="{$FieldName}_[]" value="{$Key}"  
													onClick="CheckUncheck_Click(document.forms[0].all('{$FieldName}_{$Key}[]'), this.checked);"
													{if $Key|array_key_exists:$selected}checked{/if} >
											</td>
											<td width="16"><img src="{$Templates_Image}icon_{$Key|lower}.gif"></td>
											<td width="98%" class="stdSection">{$Menu.Title}</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<table border="0" cellpadding="1" cellspacing="2" width="100%">
										<tr><td colspan="4" height="5"></td></tr>
										{foreach key=subKey item=SubMenu from=$Menu.SubOption}
										<tr>
											<td width="10">&nbsp;</td>
											<td width="10"><input type="checkbox" name="{$FieldName}_{$Key}[]" value="{$subKey}" class="stdCheckbox" {if $subKey|in_array:$selected[$Key]}checked{/if}></td>
											<td width="5"><img src="{$Templates_Image}b_arrow.gif"></td>
											<td width="98%">{$SubMenu.Title}</td>
										</tr>
										{/foreach}
										<tr><td colspan="4" height="5"></td></tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				{if $smarty.foreach.PrivilegeInfo.iteration|mod:2==0}
				</tr>
				{/if}
			{/foreach}
			</table>
		</td>
	</tr>
</table>