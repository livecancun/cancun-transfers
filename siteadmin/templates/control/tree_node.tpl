				{foreach name=recordInfo from=$node item=Record}
				<tr class="{cycle values='list_A, list_B'}">
					{if $smarty.const.A_DELETE|in_array:$C_CommandList}
					<td><input type="checkbox" name="pk_list[]" class="stdCheckBox" value="{$F_PrimaryKey|array_value:$Record}"></td>
					{/if}
					{foreach name=hdInfo from=$F_HeaderItem key=fieldName item=Field}
					<td align="{$Field.Align|default:'left'}">
						{if $smarty.foreach.hdInfo.index == 0 && $level > 0}
							&nbsp;&nbsp;&nbsp;&nbsp;{'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'|str_repeat:$level-1}&brvbar;---
						{/if}
						{if $Field.IsPicture }
							{assign var="fileName" value=$fieldName|array_value:$Record}
							{assign var="fileName" value=small_`$fileName`}
							{if !$fileName|file_exists:$P_Upload_Root}
								{assign var="fileName" value=small_default.jpg}
							{/if}
							<img src="{$V_Upload_Root}{$fileName}" border="0">
						{elseif $Field.IsDate }
							{$fieldName|array_value:$Record|time_to_date:'d-M-Y'}
						{else}
							{if $F_Link && $fieldName|in_array:$F_Link}
								<a href="?{'qName'|array_value:$F_Link}={'idField'|array_value:$F_Link|array_value:$Record}" class="stdLink">{$fieldName|array_value:$Record}</a>
							{else}
								{$fieldName|array_value:$Record}
							{/if}
						{/if}
					</td>
					{/foreach}
					<td align="center" nowrap>
						{if $smarty.const.A_VIEW|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_view.gif" class="imgAction" title="View" onClick="JavaScript: CView_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}');">
						{/if}
						{if $smarty.const.A_LOGIN|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_login.gif" class="imgAction" title="Login" onClick="JavaScript: CLogin_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}');">
						{/if}
						{if $smarty.const.A_EDIT|in_array:$C_CommandList}
							<a href="?Action=Edit&pk={$F_PrimaryKey|array_value:$Record}"><img src="{$Templates_Image}a_edit.gif" class="imgAction" title="Edit" ></a>
						{/if}
						{if $smarty.const.A_EMAIL|in_array:$C_CommandList}
							<a href="?Action=Email&pk={$F_PrimaryKey|array_value:$Record}"><img src="{$Templates_Image}a_email.gif" class="imgAction" title="Email" ></a>
						{/if}
						{if $smarty.const.A_DELETE|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: CDelete_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}');">
						{/if}
					</td>
				</tr>
				{if $Record.child != ''}
					{include file='control/tree_node.tpl' node='child'|array_value:$Record level=`$level+1`}
				{/if}
				{foreachelse}
				<tr class="list_A">
					<td colspan="6" align="center">No {$L_Module} information available.</td>
				</tr>
				{/foreach}
