		<script language="javascript">
			function Country_Change(eCaller, fldChange, defValue)
			{ldelim}
				var listFld 	= document.getElementById(fldChange);
				var arrData 	= eval('data_'+fldChange);
				
				var countryCode	= eCaller.value;
			
				listFld.options.length=0;

				for (var i in arrData[countryCode])
				{ldelim}
					listFld.options.length++;
					listFld.options[listFld.options.length-1]=new Option(arrData[countryCode][i], i);
					if(defValue == i)
						listFld.selectedIndex = listFld.options.length-1;
				{rdelim}
			{rdelim}

			function Validate_Form(frm)
			{ldelim}
				with(frm)
				{ldelim}
					{foreach name=fieldInfo from=$F_FieldInfo key=fieldName item=Field}

						{* Is Validation required *}
						{if $Field.Validate }
						
							{* Check  for empty field *}
							{if $Field.ValidationType&$smarty.const.V_EMPTY}
								if(!IsEmpty({$fieldName}, 'Please, enter {$Field.Title}.'))
								{ldelim}			
									return false;
								{rdelim}
							{/if}

							{if $Field.ValidationType&$smarty.const.V_EMPTYFILE}
								if(prev_{$fieldName}.value == '')
								{ldelim}			
									if(!IsEmpty({$fieldName}, 'Please, enter {$Field.Title}.'))
									{ldelim}			
										return false;
									{rdelim}
								{rdelim}
							{/if}
							
							{* Check  for integer value *}
							{if $Field.ValidationType&$smarty.const.V_INT}
								if(!IsInt({$fieldName}, 'Oppsss!!! Invalid input. \nPlease, re-check {$Field.Title}.'))
								{ldelim}			
									return false;
								{rdelim}
							{/if}

							{* Check  for float value *}
							{if $Field.ValidationType&$smarty.const.V_FLOAT}
								if(!IsFloat({$fieldName}, 'Oppsss!!! Invalid input. \nPlease, re-check {$Field.Title}.'))
								{ldelim}			
									return false;
								{rdelim}
							{/if}

							{* Check  for maximum length *}
							{if $Field.ValidationType&$smarty.const.V_LEN && $Field.ValidationType&$smarty.const.V_MAX}
								if(!IsLen({$fieldName}, 0, {$Field.ValidationMaxLength}, '{$Field.Title} : {$Field.ValidationMesg}'))
								{ldelim}			
									return false;
								{rdelim}
							{* Check  for maximum value *}
							{elseif $Field.ValidationType&$smarty.const.V_MAX}
								if({$fieldName}.value < {$Field.ValidationMaxLength})
								{ldelim}
									alert('{$Field.ValidationMesg}');
									{$fieldName}.focus();
									return false;
								{rdelim}
							{/if}

							{* Check  for minimum length *}
							{if $Field.ValidationType&$smarty.const.V_LEN && $Field.ValidationType&$smarty.const.V_MIN}
								if(!IsLen({$fieldName}, {$Field.ValidationMinLength}, 1000000, '{$Field.ValidationMesg}'))
								{ldelim}			
									return false;
								{rdelim}
							{* Check  for minimum value *}
							{elseif $Field.ValidationType&$smarty.const.V_MIN}
								if({$fieldName}.value < {$Field.ValidationMinLength})
								{ldelim}
									alert('{$Field.ValidationMesg}');
									{$fieldName}.focus();
									return false;
								{rdelim}
							{/if}

							{* Check for valid file type *}
							{if $Field.ValidationType&$smarty.const.V_EXTENTION}
								if({$fieldName}.value != '' && !IsValidFormat({$fieldName}, '{$Field.ValidationExtention}', 'Oppsss!!! Invalid file format. \nValid format : {$Field.ValidationExtention}.'))
								{ldelim}			
									return false;
								{rdelim}
							{/if}

							{* Check for valid size *}
							{if $Field.ValidationType&$smarty.const.V_SIZE}
								if(!IsValidSize({$fieldName}, 'Oppsss!!! Invalid size specified. \nValid format: width X height.'))
								{ldelim}			
									return false;
								{rdelim}
							{/if}

							{* Check for valid email *}
							{if $Field.ValidationType&$smarty.const.V_EMAIL }
								if(IsEmpty({$fieldName},''))
								{ldelim}
									if(!IsEmail({$fieldName}, 'Oppsss!!! Invalid email address specified!'))
									{ldelim}
										return false;
									{rdelim}
								{rdelim}
							{/if}
						{/if}
					{/foreach}
					return true;
				{rdelim}
			{rdelim}
			</script>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
			<form name="frmStdForm" action="{$A_Action}" enctype="multipart/form-data" method="post">
				<tr>
					<td align="left" width="30%">
						<input type="submit" name="Submit" value="Save" class="stdButton" onClick="JavaScript: return Validate_Form(document.frmStdForm);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
					<td align="right" class="mandatory">Fields marked with (<font class="mandatoryMark">*</font>) are mandatory.</td>
				</tr>
				{foreach name=fieldInfo from=$F_FieldInfo key=fieldName item=Field}
					{if $Field.GroupTitle != ''}
						<tr>
							<td colspan="2" class="stdSubSection" align="center">
								{$Field.GroupTitle}
							</td>
						</tr>
					{else}
						{if $Field.ControlType == $smarty.const.C_HIDDEN}
							<tr><td colspan="2"><input type="hidden" id="{$fieldName}" name="{$fieldName}" value="{$Field.SelValue|default:$Field.DefaultValue}"></td></tr>
						{else}
							<tr>
								<td class="fieldLabelLeft" width="25%" valign="top">{$Field.Title} {if $Field.Validate && ($Field.ValidationType&$smarty.const.V_EMPTY || $Field.ValidationType&$smarty.const.V_EMPTYFILE)} <font class="mandatoryMark">*</font> {/if}</td>
								<td class="fieldInputStyle">
									{if $Field.ControlType == $smarty.const.C_TEXT}
										<input type="text" id="{$fieldName}" name="{$fieldName}" size="{$Field.ControlSize}" maxlength="{$Field.ControlMaxLen}" value="{$Field.SelValue|default:$Field.DefaultValue}" style="text-align:{$Field.ControlAlign|default:'left'};" {if $Field.ControlReadOnly}readonly="true"{/if}>
									{elseif $Field.ControlType == $smarty.const.C_PASSWORD}
										<input type="password" id="{$fieldName}" name="{$fieldName}" size="{$Field.ControlSize}" maxlength="{$Field.ControlMaxLen}">
									{elseif $Field.ControlType == $smarty.const.C_RICHTEXT}
										{if $Field.ControlToolbar == 'full' || $Field.ControlToolbar == ''}
											&nbsp;
										</td>
										<tr>
										<td class="fieldInputStyle" colspan="2">
											{html_richtext toolbar='full' width=$Field.ControlWidth|default:100% height=$Field.ControlHeight|default:500 RichTextName=$fieldName RichTextValue=$Field.SelValue}
										{else}
											{html_richtext toolbar=$Field.ControlToolbar|default:'full' width=$Field.ControlWidth|default:100% height=$Field.ControlHeight|default:500 RichTextName=$fieldName RichTextValue=$Field.SelValue}
										{/if}
									{elseif $Field.ControlType == $smarty.const.C_TEXTAREA}
										<textarea id="{$fieldName}" name="{$fieldName}" rows="4" cols="55" wrap="hard">{$Field.SelValue}</textarea>
									{elseif $Field.ControlType == $smarty.const.C_CHECKBOX}
										{if $Field.Option != ''}
											{html_checkboxes name=$fieldName options=$Field.Option separator='<br>' selected=$Field.SelValue|default:$Field.DefaultValue}
										{else}
										<input type="checkbox" id="{$fieldName}" name="{$fieldName}" value="{$Field.KeyValue}" {if $Field.KeyValue==$Field.SelValue}checked{/if}>
										{/if}
									{elseif $Field.ControlType == $smarty.const.C_RADIO}
										{if $Field.Option != ''}
											{html_radios name=$fieldName options=$Field.Option separator=$Field.Separator|default:'&nbsp;' checked=$Field.SelValue|default:$Field.DefaultValue}
										{else}
										<input type="radio" id="{$fieldName}" name="{$fieldName}" value="{$Field.KeyValue}" {if $Field.KeyValue==$Field.SelValue}checked{/if}>
										{/if}
									{elseif $Field.ControlType == $smarty.const.C_DATE}
										{html_select_date name=$fieldName start_year="2005" end_year="2007" display_days=true display_months=true selected=$Field.SelValue|default:$Field.DefaultValue}
									{elseif $Field.ControlType == $smarty.const.C_PICFILE}
										<input type="file" id="{$fieldName}" name="{$fieldName}" value="{$fieldName}" size="{$Field.ControlSize}" ><br>
										{assign var="fileName" value=small_`$Field.SelValue`}
										{if !$fileName|file_exists:$P_Upload_Root}
											{assign var="fileName" value=small_default.jpg}
										{/if}
										<img src="{$V_Upload_Root}{$fileName}" border="0">
										<input type="hidden" id="prev_{$fieldName}" name="prev_{$fieldName}" value="{$Field.SelValue}">
										{if $fileName != 'small_default.jpg'}
										<br><input type="checkbox" id="del_{$fieldName}" name="del_{$fieldName}" value="1" class="stdCheckbox"> Delete {$Field.Title}
										{/if}
									{elseif $Field.ControlType == $smarty.const.C_AUDIOFILE}
										<input type="file" id="{$fieldName}" name="{$fieldName}" size="{$Field.ControlSize}">
										<input type="hidden" id="prev_{$fieldName}" name="prev_{$fieldName}" value="{$Field.SelValue}">
										{if $Field.SelValue|file_exists:$P_Upload_Root}
											<object height="50%" width="50%" classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
											<param name="AutoStart" value="0" />
											<param name="FileName" value="{$V_Upload_Root}{$Field.SelValue}" />
											</object>						
											<br><input type="checkbox"  id="del_{$fieldName}" name="del_{$fieldName}" value="1" class="stdCheckbox"> Delete {$Field.Title}
										{/if}
									{elseif $Field.ControlType == $smarty.const.C_COMBOBOX || $Field.ControlType == $smarty.const.C_DBCOMBOBOX}
										<select id="{$fieldName}" name="{$fieldName}">
											{if $Field.DefaultOption}
												<option value="{'value'|array_value:$Field.DefaultOption}">{'text'|array_value:$Field.DefaultOption}</option>
											{/if}
											{html_options options=$Field.Option selected=$Field.SelValue|default:$Field.DefaultValue}
										</select>
									{elseif $Field.ControlType == $smarty.const.C_STATE_COMBOBOX}
										<script language="javascript">
											var data_{$fieldName} = new Object();
											{foreach name=countryInfo from=$Field.Option key=country_code item=stateList}
												data_{$fieldName}['{$country_code}'] = new Object();
												{foreach name=stateInfo from=$stateList key=state_id item=state_name}
													data_{$fieldName}['{$country_code}']['{$state_id}'] = "{$state_name}";
												{/foreach}
											{/foreach}
										</script>
										<select  id="{$fieldName}" name="{$fieldName}">
										</select>
										<script language="javascript">
											Country_Change(document.getElementById('{$Field.ControlDependency}'), '{$fieldName}', '{$Field.SelValue|default:$Field.DefaultValue}');
										</script>
									{elseif $Field.ControlType == $smarty.const.C_COUNTRY_COMBOBOX}
										<select id="{$fieldName}" name="{$fieldName}" onchange="JavaScript: Country_Change(this, '{$Field.ControlDependency}')">
											{html_options options=$Field.Option selected=$Field.SelValue|default:$Field.DefaultValue}
										</select>
									{elseif $Field.ControlType == $smarty.const.C_PRIVILEGE}
										{include file='control/privilege.tpl' FieldName=$fieldName PrivilegesList=$Field.Option selected=$Field.SelValue}
									{/if}
									<br>
									{$Field.Desc}
								</td>
							</tr>
						{/if}
					{/if}
				{/foreach}
				<tr><td class="divider" colspan="2"></td></tr>
				<tr>
					<td align="left" colspan="2">
						<input type="submit" name="Submit" value="Save" class="stdButton" onClick="JavaScript: return Validate_Form(document.frmStdForm);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
						<input type="hidden" name="Action" 	value="{$Action}">
						<input type="hidden" name="pk" 		value="{$PK}">
					</td>
				</tr>
			</form>
			</table>