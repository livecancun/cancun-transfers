<script>
//====================================================================================================
//	Function Name	:	Validate_Form()
//----------------------------------------------------------------------------------------------------				
function Validate_Form(frm)
{ldelim}
	with(frm)
	{ldelim}
		{foreach name=fieldInfo from=$F_HeaderItem key=fieldName item=Field}
		
			{* Is Validation required *}
			{if $Field.Validate }
				var fld = document.all['{$fieldName}[]'];
				{* Check  for empty field *}
				{if $Field.ValidationType&$smarty.const.V_EMPTY}
					for(i=0; i < fld.length; i++)
					{ldelim}
							if(!IsEmpty(fld[i], 'Please, enter {$Field.Title}.'))
								return false;
					{rdelim}
				{/if}
				
				{* Check  for integer value *}
				{if $Field.ValidationType&$smarty.const.V_INT}
					for(i=0; i < fld.length; i++)
					{ldelim}							
							if(!IsInt(fld[i], 'Oppsss!!! Invalid input. \nPlease, re-check {$Field.Title}.'))
								return false;
					{rdelim}
				{/if}

				{* Check  for float value *}
				{if $Field.ValidationType&$smarty.const.V_FLOAT}
					for(i=0; i < fld.length; i++)
					{ldelim}							
						if(!IsFloat(fld[i], 'Oppsss!!! Invalid input. \nPlease, re-check {$Field.Title}.'))
							return false;
					{rdelim}
				{/if}

				{* Check  for maximum length *}
				{if $Field.ValidationType&$smarty.const.V_LEN && $Field.ValidationType&$smarty.const.V_MAX}
					for(i=0; i < fld.length; i++)
					{ldelim}							
						if(!IsLen(fld[i], 0, {$Field.ValidationMaxLength}, '{$Field.Title} : {$Field.ValidationMesg}'))
						{ldelim}			
							return false;
						{rdelim}
					{rdelim}	
				{* Check  for maximum value *}
				{elseif $Field.ValidationType&$smarty.const.V_MAX}
					for(i=0; i < fld.length; i++)
					{ldelim}							
						if(fld[i].value < {$Field.ValidationMaxLength})
						{ldelim}
							alert('{$Field.ValidationMesg}');
							fld[i].focus();
							return false;
						{rdelim}
					{rdelim}
				{/if}

				{* Check  for minimum length *}
				{if $Field.ValidationType&$smarty.const.V_LEN && $Field.ValidationType&$smarty.const.V_MIN}
					for(i=0; i < fld.length; i++)
					{ldelim}	
						if(!IsLen(fld[i], {$Field.ValidationMinLength}, 1000000, '{$Field.ValidationMesg}'))
						{ldelim}			
							return false;
						{rdelim}
					{rdelim}
				{* Check  for minimum value *}
				{elseif $Field.ValidationType&$smarty.const.V_MIN}
					for(i=0; i < fld.length; i++)
					{ldelim}	
						if(fld[i].value < {$Field.ValidationMinLength})
						{ldelim}
							alert('{$Field.ValidationMesg}');
							fld[i].focus();
							return false;
						{rdelim}
					{rdelim}
				{/if}

				{* Check for valid file type *}
				{if $Field.ValidationType&$smarty.const.V_EXTENTION}
					for(i=0; i < fld.length; i++)
					{ldelim}	
						if(fld[i].value != '' && !IsValidFormat(fld[i], '{$Field.ValidationExtention}', 'Oppsss!!! Invalid file format. \nValid format : {$Field.ValidationExtention}.'))
							return false;
					{rdelim}
				{/if}

				{* Check for valid size *}
				{if $Field.ValidationType&$smarty.const.V_SIZE}
					for(i=0; i < fld.length; i++)
					{ldelim}	
						if(!IsValidSize(fld[i], 'Oppsss!!! Invalid size specified. \nValid format: width X height.'))
							return false;
					{rdelim}
				{/if}

				{* Check for valid email *}
				{if $Field.ValidationType&$smarty.const.V_EMAIL }
					for(i=0; i < fld.length; i++)
					{ldelim}	
						if(IsEmpty(fld[i],''))
						{ldelim}
							if(!IsEmail(fld[i], 'Oppsss!!! Invalid email address specified!'))
								return false;
						{rdelim}
					{rdelim}
				{/if}
			{/if}
			
		{/foreach}
		return true;
	{rdelim}
{rdelim}
	
</script>

<form name="frmStdForm" action="" method="post">
<table border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		<td nowrap>
			{$LinkPath}
			{if $AlphaSort}
				{html_alpha_pager name=test linkClassName="listHeader"}
			{/if}
		</td>
		<td align="right">
			{if $Pagination}
			Page Size:
			<select name="page_size" onChange="document.frmStdForm.submit();">
				{html_options options=$PageSize selected=$smarty.session.page_size}
			</select>
			{/if}
		</td>
	</tr>
</table>
<table border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		{if $FieldSort}
			{html_sort_link dataItem=$F_HeaderItem className="listHeader" selClassName="selListHeader" templateImage=$Templates_Image}
		{else}
			{foreach name=HeaderInfo from=$F_HeaderItem item=Header}
				<td class="listHeader" width="{$Header.Width}">{$Header.Title}</td>
			{/foreach}
		{/if}
	</tr>
	{foreach name=recordInfo from=$R_RecordSet item=Record}
	<tr class="{cycle values='list_A, list_B'}">
		<input type="hidden" name="{$F_PrimaryKey}[]" value="{$F_PrimaryKey|array_value:$Record}">
		{foreach name=hdInfo from=$F_HeaderItem key=fieldName item=Field}
		<td align="{$Field.Align|default:'left'}">
			{if $Field.IsControl}
				{if $Field.ControlType=='Text'}
					<input name="{$fieldName}[]" type="text" size="{$Field.ControlSize}" value="{$fieldName|array_value:$Record}" maxlength="{$Field.ControlMaxLen}" style="text-align:{$Field.ControlAlign|default:'left'};" />
				{elseif $Field.ControlType == $smarty.const.C_COMBOBOX || $Field.ControlType == $smarty.const.C_DBCOMBOBOX}
					<select name="{$fieldName}[]">
						{if $Field.DefaultOption}
							<option value="{'value'|array_value:$Field.DefaultOption}">{'text'|array_value:$Field.DefaultOption}</option>
						{/if}
						{html_options options=$Field.Option selected=$Field.SelValue|default:$Field.DefaultValue}
					</select>					
				{/if}
			{elseif $Field.IsPicture}
				{assign var="fileName" value=$fieldName|array_value:$Record}
				{assign var="fileName" value=small_`$fileName`}
				{if !$fileName|file_exists:$P_Upload_Root}
					{assign var="fileName" value=small_default.jpg}
				{/if}
				<img src="{$V_Upload_Root}{$fileName}" border="0">
			{elseif $Field.IsDate }
				{$fieldName|array_value:$Record|time_to_date:'d-M-Y'}
			{elseif $Field.IsAmount }
				{$fieldName|array_value:$Record|number_format}
			{elseif $Field.IsId }
				{$fieldName|array_value:$Record|array_value:$Field.Option}
			{else}
				{if $F_Link && $fieldName|in_array:$F_Link}
					<a href="?{'qName'|array_value:$F_Link}={'idField'|array_value:$F_Link|array_value:$Record}" class="stdLink">{$fieldName|array_value:$Record}</a>
				{else}
					{$fieldName|array_value:$Record}
				{/if}
			{/if}
		</td>
		{/foreach}
	</tr>
	{foreachelse}
	<tr class="list_A">
		<td colspan="6" align="center">No {$L_Module} information available.</td>
	</tr>
	{/foreach}
</table>
<table border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		<td align="right">
			<input type="submit" name="Submit" value="Update" class="stdFilterButton" onclick="return Validate_Form(document.frmStdForm);"/>
		</td>
	</tr>				
</table>


{if $total_record > $smarty.session.page_size && $Pagination}
<br>
<table border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr>
		<td valign="top" width="33%">
			{html_pager num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record image_path=$Templates_Image add_prevnext_text=true}
		</td>
		<td align="center">
			{html_pager_text num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record}
		</td>
		<td align="right"  width="33%">
			Goto Page: <input type="text" name="goto_page" size="5">
			<input type="submit" name="go" value="Go" class="stdFilterButton" >
		</td>
	</tr>
</table>
{/if}
<input type="hidden" name="Action">
</form>