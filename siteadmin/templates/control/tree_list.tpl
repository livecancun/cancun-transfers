			<table border="0" cellpadding="1" cellspacing="1" width="95%">
			<form name="frmStdForm" action="" method="post">
				<tr>
					<td align="right" colspan="2">
						{if $smarty.const.A_SORT|in_array:$C_CommandList}
							<a href="?Action=Sort"><img src="{$Templates_Image}a_sort.gif" class="imgAction" title="Sort {$L_Module}"></a>&nbsp;&nbsp;
						{/if}								
						{if $smarty.const.A_ADD|in_array:$C_CommandList}
							<a href="?Action=Add"><img src="{$Templates_Image}a_add.gif" class="imgAction" title="Add {$L_Module}"></a>&nbsp;&nbsp;
						{/if}
					</td>
				</tr>
				<tr>
					<td nowrap>
						{$LinkPath}
						{if $AlphaSort}
							{html_alpha_pager name=test linkClassName="listHeader"}
						{/if}
					</td>
					<td align="right">
						Page Size:
						<select name="page_size" onChange="document.frmStdForm.submit();">
							{html_options options=$PageSize selected=$smarty.session.page_size}
						</select>
					</td>
				</tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					{if $smarty.const.A_DELETE|in_array:$C_CommandList}
					<td class="listHeader" width="20">&nbsp;</td>
					{/if}
					{if $FieldSort}
						{html_sort_link dataItem=$F_HeaderItem className="listHeader" selClassName="selListHeader" templateImage=$Templates_Image}
					{else}
						{foreach name=HeaderInfo from=$F_HeaderItem item=Header}
							<td class="listHeader" width="{$Header.Width}">{$Header.Title}</td>
						{/foreach}
					{/if}
					<td class="listHeader" nowrap>Action</td>
				</tr>
				{include file='control/tree_node.tpl' node=$R_RecordSet level=0}
			</table>
			{if $smarty.foreach.recordInfo.total > 1 && $smarty.const.A_DELETE|in_array:$C_CommandList}
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td>
						<img src="{$Templates_Image}arrow_ltr.png"> 
						<a href="JavaScript: CheckUncheck_Click(document.all['pk_list[]'], true);" onMouseMove="window.status='Check All';" onMouseOut="window.status='';" class="actionLink">Check All</a> / 
						<a href="JavaScript: CheckUncheck_Click(document.all['pk_list[]'], false);" onMouseMove="window.status='Uncheck All';" onMouseOut="window.status='';" class="actionLink">Uncheck All</a>  &nbsp;&nbsp;
						With selected
						<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: CDeleteChecked_Click(document.frmStdForm, document.all['pk_list[]']);">&nbsp;
					</td>
				</tr>
			</table>
			{/if}
			{if $total_record > $smarty.session.page_size}
			<br>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td valign="top" width="33%">
						{html_pager num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record image_path=$Templates_Image add_prevnext_text=true}
					</td>
					<td align="center">
						{html_pager_text num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record}
					</td>
					<td align="right"  width="33%">
						Goto Page: <input type="text" name="goto_page" size="5">
						<input type="submit" name="go" value="Go" class="stdFilterButton">
					</td>
				</tr>
			</table>
			{/if}
			<input type="hidden" name="pk">
			<input type="hidden" name="Action">
			</form>
			<script>
				//====================================================================================================
				//	Function Name	:	View_Click()
				//----------------------------------------------------------------------------------------------------
				function CView_Click(frm, PK)
				{ldelim}
					popupWindowURL(frm.action + '?Action=View&pk='+PK, 'list', 600, 500, false, false, true);
				{rdelim}

				//====================================================================================================
				//	Function Name	:	Delete_Click()
				//----------------------------------------------------------------------------------------------------
				function CLogin_Click(frm, PK)
				{ldelim}
					with(frm)
					{ldelim}
						pk.value = PK;
						Action.value	= 'Login';
						frm.submit();
					{rdelim}
				{rdelim}
				
				//====================================================================================================
				//	Function Name	:	Delete_Click()
				//----------------------------------------------------------------------------------------------------
				function CDelete_Click(frm, PK)
				{ldelim}
					with(frm)
					{ldelim}
						if(confirm('Are you sure you wants to delete this information?'))
						{ldelim}
							pk.value = PK;
							Action.value	= 'Delete';
							frm.submit();
						{rdelim}
					{rdelim}
				{rdelim}
				
				//====================================================================================================
				//	Function Name	:	Delete_Click()
				//----------------------------------------------------------------------------------------------------
				function CDeleteChecked_Click(frm, PK)
				{ldelim}
					with(frm)
					{ldelim}
						var flg=false;
			
						for(i=0; i < PK.length; i++)
						{ldelim}
							if(PK[i].checked)
								flg = true;
						{rdelim}

						if(!flg)
						{ldelim}
							alert('Please, select the information you wants to delete.');
							return false;
						{rdelim}

						if(confirm('Are you sure you wants to delete selected information?'))
						{ldelim}
							Action.value 	= 'DeleteSelected';
							submit();
						{rdelim}
					{rdelim}
				{rdelim}
			</script>