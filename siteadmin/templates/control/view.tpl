<table border="0" cellpadding="1" cellspacing="2" width="95%">
	
	{foreach name=fieldInfo from=$F_FieldInfo key=fieldName item=Field}
		{if $Field.GroupTitle != ''}
			<tr>
				<td colspan="2" class="stdSubSection" align="center">
					{$Field.GroupTitle}
				</td>
			</tr>
		{else}
			<tr>
				<td class="fieldLabelLeft" width="25%" valign="top">{$Field.Title}</td>
				<td class="fieldInputStyle">
					{if $Field.ControlType == $smarty.const.C_PICFILE}
						{assign var="fileName" value=medium_`$Field.SelValue`}
						{if !$fileName|file_exists:$P_Upload_Root}
							{assign var="fileName" value=small_default.jpg}
						{/if}
						<img src="{$V_Upload_Root}{$fileName}" border="0">
					{elseif $Field.ControlType == $smarty.const.C_AUDIOFILE}
						{if $Field.SelValue|file_exists:$P_Upload_Root}
							<object height="50%" width="50%" type="application/x-mplayer2" standby="Loading Microsoft windows media player components..." codebase="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701"  classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95">
							<param name="AutoStart" value="0" />
							<param name="FileName" value="{$V_Upload_Root}{$Field.SelValue}" />
							<embed src="{$V_Upload_Root}{$Field.SelValue}" type="application/x-mplayer2" autostart="false" hidden="true" loop=0></embed>
							</object>				
						{/if}
					{elseif $Field.ControlType == $smarty.const.C_RICHTEXT}
						{if $Field.ControlToolbar == 'full'}
							&nbsp;
						</td>
						<tr>
						<td class="fieldInputStyle" colspan="2">
						{/if}
						{$Field.SelValue}
					{elseif $Field.ControlType == $smarty.const.C_DBCOMBOBOX}
						{$Field.SelValue|array_value:$Field.Option|default:'[None]'}
					{elseif $Field.ControlType == $smarty.const.C_DATE}
						{$Field.SelValue}
					{elseif $Field.ControlType == $smarty.const.C_HIDDEN}
					{elseif $Field.ControlType == $smarty.const.C_PASSWORD}
						xxxxxxxxxxxxxxx
					{else}
						{$Field.SelValue}&nbsp;
					{/if}
				</td>
			</tr>
		{/if}
	{/foreach}
	<tr><td class="divider" colspan="2"></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
		<td colspan="2" align="center">
			<input type="button" name="button" value="Close" class="stdButton" onClick="window.close();">
		</td>
	</tr>
</table>