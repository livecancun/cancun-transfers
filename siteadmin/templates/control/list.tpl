			<table border="0" cellpadding="1" cellspacing="1" width="95%">
			<form name="frmStdForm" action="" method="post">
				<tr>
					<td align="right" colspan="2">
						{if $smarty.const.A_SORT|in_array:$C_CommandList}
							<!--a href="?Action=Sort"><img src="{$Templates_Image}a_sort.gif" class="imgAction" title="Sort {$L_Module}"></a>&nbsp;&nbsp;-->
						{/if}
						{if $smarty.const.A_ADD|in_array:$C_CommandList}
							<a href="{'Action=Add'|querystring_pad:$A_Action}"><img src="{$Templates_Image}a_add.gif" class="imgAction" title="Add {$L_Module}"></a>&nbsp;&nbsp;
						{/if}
						{if $smarty.const.A_PRINT|in_array:$C_CommandList}
							<a href="?Action=Print" target="_blank" class="imgAction"><img src="{$Templates_Image}a_print.gif" class="imgAction" title="Print"></a>&nbsp;&nbsp;
						{/if}
					</td>
				</tr>
				<tr>
					<td nowrap>
						{$LinkPath}
						{if $AlphaSort}
							{html_alpha_pager name=test linkClassName="listHeader"}
						{/if}
					</td>
					<td align="right">
						{if $Pagination}
						Page Size:
						<select name="page_size" onChange="document.frmStdForm.submit();">
							{html_options options=$PageSize selected=$smarty.session.page_size}
						</select>
						{/if}
					</td>
				</tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					{if $smarty.const.A_DELETE|in_array:$C_CommandList}
					<td class="listHeader" width="20">&nbsp;</td>
					{/if}
					{if $FieldSort}
						{html_sort_link dataItem=$F_HeaderItem className="listHeader" selClassName="selListHeader" templateImage=$Templates_Image}
					{else}
						{foreach name=HeaderInfo from=$F_HeaderItem item=Header}
							<td class="listHeader" width="{$Header.Width}">{$Header.Title}</td>
						{/foreach}
					{/if}
					{if $smarty.const.A_SORT|in_array:$C_CommandList}
					<td class="listHeader" nowrap>Display Order</td>
					{/if}
					<td class="listHeader" nowrap>Action</td>
				</tr>
				{foreach name=recordInfo from=$R_RecordSet item=Record}
				<tr class="{cycle values='list_A, list_B'}">
					{if $smarty.const.A_DELETE|in_array:$C_CommandList}
					<td><input type="checkbox" name="pk_list[]" class="stdCheckBox" value="{$F_PrimaryKey|array_value:$Record}"></td>
					{/if}
					{foreach name=hdInfo from=$F_HeaderItem key=fieldName item=Field}
					<td align="{$Field.Align|default:'left'}">
						{if $Field.IsPicture }
							{assign var="fileName" value=$fieldName|array_value:$Record}
							{assign var="fileName" value=small_`$fileName`}
							{if !$fileName|file_exists:$P_Upload_Root}
								{assign var="fileName" value=small_default.jpg}
							{/if}
							<img src="{$V_Upload_Root}{$fileName}" border="0">
						{elseif $Field.IsDate }
							{$fieldName|array_value:$Record|time_to_date:'d-M-Y'}
						{elseif $Field.IsAmount }
							{$fieldName|array_value:$Record|number_format}
						{elseif $Field.IsId }
							{$fieldName|array_value:$Record|array_value:$Field.Option}
						{else}
							{if $F_Link && $fieldName|in_array:$F_Link}
								<a href="?{'qName'|array_value:$F_Link}={'idField'|array_value:$F_Link|array_value:$Record}" class="stdLink">{$fieldName|array_value:$Record}</a>
							{else}
								{$fieldName|array_value:$Record}
							{/if}
						{/if}
					</td>
					{/foreach}
					{if $smarty.const.A_SORT|in_array:$C_CommandList}
					<td align="center" nowrap>
						{if !$smarty.foreach.recordInfo.first}
						<img src="{$Templates_Image}a_up.gif" class="imgAction" title="Move Up" onClick="JavaScript: CSortOrder_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}', 'Up');">
						{else}
						&nbsp;&nbsp;&nbsp;
						{/if}
						{if !$smarty.foreach.recordInfo.last}
						<img src="{$Templates_Image}a_down.gif" class="imgAction" title="Move Down" onClick="JavaScript: CSortOrder_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}', 'Down');">
						{else}
						&nbsp;&nbsp;&nbsp;
						{/if}
					</td>
					{/if}
					<td align="center" nowrap>
						{if $smarty.const.A_VIEW|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_view.gif" class="imgAction" title="View" onClick="JavaScript: CView_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}');">
						{/if}
						{if $smarty.const.A_VISIBILITY|in_array:$C_CommandList}
							{if $F_Visibility|array_value:$Record == 'No'}
								<img src="{$Templates_Image}a_active_no.gif" class="imgAction" title="Visible" onClick="JavaScript: CVisibleHide_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}', 'Yes');">
							{else}
								<img src="{$Templates_Image}a_active_yes.gif" class="imgAction" title="Hide" onClick="JavaScript: CVisibleHide_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}', 'No');">
							{/if}
						{/if}
						{if $smarty.const.A_APPROVE|in_array:$C_CommandList}
							{if $F_Visibility|array_value:$Record == 'No'}
								<img src="{$Templates_Image}a_active_no.gif" class="imgAction" title="Approve" onClick="JavaScript: CVisibleHide_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}', 'Yes');">
							{else}
								<img src="{$Templates_Image}a_active_yes.gif" class="imgAction" title="Disapprove" onClick="JavaScript: CVisibleHide_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}', 'No');">
							{/if}
						{/if}						
						{if $smarty.const.A_LOGIN|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_login.gif" class="imgAction" title="Login" onClick="JavaScript: CLogin_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}');">
						{/if}						
						{if $smarty.const.A_SUBZONE|in_array:$C_CommandList}
							<a href="{'Action=Add Subzone'|querystring_pad:$A_Action}&pk={$F_PrimaryKey|array_value:$Record}"><img src="{$Templates_Image}a_invoice.gif" class="imgAction" title="Add Subzone" ></a>
						{/if}
						{if $smarty.const.A_UPDATE|in_array:$C_CommandList}
							<a href="?Action=Update&pk={$F_PrimaryKey|array_value:$Record}"><img src="{$Templates_Image}a_update.gif" class="imgAction" title="Update" ></a>
						{/if}
						{if $smarty.const.A_INVOICE|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_invoice.gif" class="imgAction" title="Invoice" onClick="JavaScript: CInvoice_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}');">
						{/if}
						{if $smarty.const.A_EDIT|in_array:$C_CommandList}
							<a href="{'Action=Edit'|querystring_pad:$A_Action}&pk={$F_PrimaryKey|array_value:$Record}"><img src="{$Templates_Image}a_edit.gif" class="imgAction" title="Edit" ></a>
						{/if}
						{if $smarty.const.A_EMAIL|in_array:$C_CommandList}
							<a href="{'Action=Email'|querystring_pad:$A_Action}&pk={$F_PrimaryKey|array_value:$Record}"><img src="{$Templates_Image}a_email.gif" class="imgAction" title="Email" ></a>
						{/if}						
						{if $smarty.const.A_DELETE|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: CDelete_Click(document.frmStdForm, '{$F_PrimaryKey|array_value:$Record}');">
						{/if}
					</td>
				</tr>
				{foreachelse}
				<tr class="list_A">
					<td colspan="6" align="center">No {$L_Module} information available.</td>
				</tr>
				{/foreach}
			</table>
			{if $smarty.foreach.recordInfo.total > 1 && ($smarty.const.A_DELETE|in_array:$C_CommandList || $smarty.const.A_VISIBILITY|in_array:$C_CommandList)}
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td>
						<img src="{$Templates_Image}arrow_ltr.png"> 
						<a href="JavaScript: CheckUncheck_Click(document.all['pk_list[]'], true);" onMouseMove="window.status='Check All';" onMouseOut="window.status='';" class="actionLink">Check All</a> / 
						<a href="JavaScript: CheckUncheck_Click(document.all['pk_list[]'], false);" onMouseMove="window.status='Uncheck All';" onMouseOut="window.status='';" class="actionLink">Uncheck All</a>  &nbsp;&nbsp;
						With selected
						{if $smarty.const.A_DELETE|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: CDeleteChecked_Click(document.frmStdForm, document.all['pk_list[]']);">&nbsp;
						{/if}
						{if $smarty.const.A_VISIBILITY|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_active_yes.gif" class="imgAction" title="Visible" onClick="JavaScript: CVisibleHideChecked_Click(document.frmStdForm, document.all['pk_list[]'], 'Yes');">&nbsp;
							<img src="{$Templates_Image}a_active_no.gif" class="imgAction" title="Hide" onClick="JavaScript: CVisibleHideChecked_Click(document.frmStdForm, document.all['pk_list[]'], 'No');">&nbsp;
						{/if}
						{if $smarty.const.A_APPROVE|in_array:$C_CommandList}
							<img src="{$Templates_Image}a_active_yes.gif" class="imgAction" title="Approve" onClick="JavaScript: CVisibleHideChecked_Click(document.frmStdForm, document.all['pk_list[]'], 'Yes');">&nbsp;
							<img src="{$Templates_Image}a_active_no.gif" class="imgAction" title="Disapprove" onClick="JavaScript: CVisibleHideChecked_Click(document.frmStdForm, document.all['pk_list[]'], 'No');">&nbsp;
						{/if}
					</td>
				</tr>
			</table>
			{/if}
			
			{if $total_record > $smarty.session.page_size && $Pagination}
			<br>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td valign="top" width="33%">
						{html_pager num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record image_path=$Templates_Image add_prevnext_text=true}
					</td>
					<td align="center">
						{html_pager_text num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record}
					</td>
					<td align="right"  width="33%">
						Goto Page: <input type="text" name="goto_page" size="5">
						<input type="submit" name="go" value="Go" class="stdFilterButton">
					</td>
				</tr>
			</table>
			{/if}
			<input type="hidden" name="pk">
			<input type="hidden" name="visible">
			<input type="hidden" name="move">
			<input type="hidden" name="Action">
			</form>
			<script>
				//====================================================================================================
				//	Function Name	:	View_Click()
				//----------------------------------------------------------------------------------------------------
				function CView_Click(frm, PK)
				{ldelim}
					popupWindowURL(frm.action + '?Action=View&pk='+PK, 'View{$L_Module|strip:''}', {$PopupSize|default:'500, 400, false, false, true'});
				{rdelim}

				//====================================================================================================
				//	Function Name	:	View_Click()
				//----------------------------------------------------------------------------------------------------
				function CInvoice_Click(frm, PK)
				{ldelim}
					popupWindowURL(frm.action + '?Action=Invoice&pk='+PK, 'Invoice{$L_Module|strip:''}', {$PopupSize|default:'500, 400, false, false, true'});
				{rdelim}


				//====================================================================================================
				//	Function Name	:	CPrintAll_Click()
				//----------------------------------------------------------------------------------------------------
				function CPrintAll_Click()
				{ldelim}
					popupWindowURL('{$A_Action}?Action=Print', 'Print{$L_Module|strip:''}', {$PopupSize|default:'500, 400, false, false, true'});
				{rdelim}

				//====================================================================================================
				//	Function Name	:	CLogin_Click()
				//----------------------------------------------------------------------------------------------------
				function CLogin_Click(frm, PK)
				{ldelim}
					with(frm)
					{ldelim}
						pk.value 		= PK;
						Action.value	= 'Login';
						frm.submit();
					{rdelim}
				{rdelim}

				//====================================================================================================
				//	Function Name	:	CSortOrder_Click()
				//----------------------------------------------------------------------------------------------------
				function CSortOrder_Click(frm, PK, moveDir)
				{ldelim}
					with(frm)
					{ldelim}
						pk.value 		= PK;
						Action.value	= 'Sort';
						move.value		= moveDir;
						frm.submit();
					{rdelim}
				{rdelim}
				
				//====================================================================================================
				//	Function Name	:	Delete_Click()
				//----------------------------------------------------------------------------------------------------
				function CDelete_Click(frm, PK)
				{ldelim}
					with(frm)
					{ldelim}
						if(confirm('Are you sure you wants to delete this information?'))
						{ldelim}
							pk.value 		= PK;
							Action.value	= 'Delete';
							frm.submit();
						{rdelim}
					{rdelim}
				{rdelim}
				
				//====================================================================================================
				//	Function Name	:	CDeleteChecked_Click()
				//----------------------------------------------------------------------------------------------------
				function CDeleteChecked_Click(frm, PK)
				{ldelim}
					with(frm)
					{ldelim}
						var flg=false;
			
						for(i=0; i < PK.length; i++)
						{ldelim}
							if(PK[i].checked)
								flg = true;
						{rdelim}

						if(!flg)
						{ldelim}
							alert('Please, select the information you wants to delete.');
							return false;
						{rdelim}

						if(confirm('Are you sure you wants to delete selected information?'))
						{ldelim}
							Action.value 	= 'DeleteSelected';
							submit();
						{rdelim}
					{rdelim}
				{rdelim}
				
				//====================================================================================================
				//	Function Name	:	Delete_Click()
				//----------------------------------------------------------------------------------------------------
				function CVisibleHide_Click(frm, PK, Visibility)
				{ldelim}
					with(frm)
					{ldelim}
						pk.value 		= PK;
						Action.value	= 'VisibleHide';
						visible.value	= Visibility;
						frm.submit();
					{rdelim}
				{rdelim}
				
				//====================================================================================================
				//	Function Name	:	CActiveChecked_Click()
				//----------------------------------------------------------------------------------------------------
				function CVisibleHideChecked_Click(frm, PK, Visibility)
				{ldelim}
					with(frm)
					{ldelim}
						var flg=false;
			
						for(i=0; i < PK.length; i++)
						{ldelim}
							if(PK[i].checked)
								flg = true;
						{rdelim}

						if(!flg)
						{ldelim}
							alert('Please, select the information you wants to visible/hide.');
							return false;
						{rdelim}

						visible.value	= Visibility;
						Action.value 	= 'VisibleHideSelected';
						submit();
					{rdelim}
				{rdelim}
			</script>