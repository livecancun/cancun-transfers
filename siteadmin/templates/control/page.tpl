<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif"></td>
					<td class="stdSection" width="99%">{$ModuleTitle} {if $Action != ''} [{$Action}] {/if}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td height="5"></td></tr>
				<tr>
					<td>
						{$HelpText}
					</td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr><td class="errorMsg" align="center">{$errorMessage}</td></tr>
				<tr><td class="successMsg" align="center">{$succMessage}</td></tr>
			</table>
			{if $searchForm}
				{include file=$searchForm}
				<br>
			{/if}
			{include file=$includeFile}
			{if $backUrl}
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td class="successMsg" align="left">
						<img src="{$Templates_Image}t_back.gif"><a  href="{$backUrl}" class="stdNormalLink"> Back</a>
					</td>
				</tr>
			</table>				
			{/if}
			<br>
		</td>
	</tr>
</table>