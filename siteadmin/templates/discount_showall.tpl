<script language="javascript">
	var Confirm_Delete_Msg	= '{$Confirm_Delete_Msg}';
</script>


<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
<form name="frmTravelDiscount" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">{$L_Discount_Manager}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						{$L_Manage_Discount}
					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
				  <td bgcolor="#FFFFFF">
				  <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
				<tr>
					 <td colspan="5" class="successMsg" align="center">{$Message}</td>
				</tr>
				<tr>
					<td colspan="5" align="right">
						<a href="{$smarty.server.PHP_SELF}?Action=Add"><img src="{$Templates_Image}a_add.gif" class="imgAction" title="Add Page"></a>&nbsp;&nbsp;
						<!--a href="{$smarty.server.PHP_SELF}?Action=Sort"><img src="{$Templates_Image}a_sort.gif" class="imgAction" title="Sort Pages"></a-->&nbsp;&nbsp;
					</td>
				</tr>
				
<!--			<tr>
					<td colspan="5" class="stdSection" align="right">
						<a href="javascript: Add_Click();" class="actionLink">{$L_Add_Travel_Discount}</a>
					</td>
				</tr>
-->				
				<tr>
				 	<td class="listHeader" width="8%" align="center">{$SrNo}</td>
					<td class="listHeader" width="40%" align="center">{$L_Discount_For}</td>
					<td class="listHeader" width="30%" align="center">{$L_Discount_Rate}</td>
					<td class="listHeader" colspan="2" align="center">{$Action}</td>
				</tr>
				<tr><td colspan="5" align="center" class="errorMsg">{$No_Record_Found}</td></tr>
				{foreach name=DiscInfo from=$DiscInfo item=Disc}
				 {assign var="Sr_No" value=$smarty.foreach.DiscInfo.iteration} 
				<tr class="{cycle values='list_A, list_B'}">
					<td align="center">{$Sr_No}</td>
					<td align="center">{$Disc.discount_title}</td>
					<td align="center">{$Disc.discount_rate} %</td>
					<td align="center">
						<img src="{$Templates_Image}a_edit.gif" class="imgAction" title="Edit" onClick="javascript: Edit_Click('{$Disc.discount_id}');">&nbsp;
					</td>
					<td align="center">
						<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="javascript: Delete_Click('{$Disc.discount_id}');">&nbsp;					
					</td>
				</tr>
				{/foreach}
				<tr><td colspan="5">&nbsp;</td></tr>
				<tr>
					<td colspan="5" class="pageLink" align="right">{$Page_Link}</td>
				</tr>
			</table>
		</td>
	</tr>

			</table>
			<br>
			<input type="hidden" name="discount_id" value="">
			<input type="hidden" name="Action">
			<input type="hidden" name="start" value="{$Start}">
		</td>
	</tr>
	</form>
</table>