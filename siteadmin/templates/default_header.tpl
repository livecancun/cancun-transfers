<table width="100%" border="0" align="center" height="17" cellpadding="0" cellspacing="0" background="{$Templates_Image}headerspacer.gif">
	<tr>
		{if $smarty.session.User_Id}
		<td align="left">&nbsp;&nbsp;&nbsp;
			{if $smarty.session.lng == "en"}
				<a href="{$smarty.server.PHP_SELF}?&lng=sp" class="MenuLink" title="Espanol">Espanol</a>&nbsp;
			{else}
				<a href="{$smarty.server.PHP_SELF}?&lng=en" class="MenuLink" title="Enlish">English</a>&nbsp;
			{/if}
		</td>
		<td align="right">
			<a href="logout.php" class="MenuLink" title="Exit from Administrative Zone"><img src="{$Templates_Image}a_logout.gif" class="imgAction" width="16" height="16"></a>&nbsp;
		</td>
		{/if}		
	</tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="{$Templates_Image}hd_spacer.gif" style="background-repeat:repeat-x">
	<tr>
		<td width="50%" align="left" valign="bottom">
        	&nbsp;&nbsp;<font size="5">Administrative Zone</font><br>
        	&nbsp;&nbsp;<font size="2">[{$Company_Title}]</font><br><br>
        </td>
		<td width="50%" align="right"><img src="{$Templates_Image}headerimage.gif"></td>
	</tr>
	<tr><td colspan="2" height="2"></td></tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" background="{$Templates_Image}menuspacer.gif" >
  <tr>
  	<td class="boldText">
    	&nbsp;&nbsp;
        {if $smarty.session.User_Id}
	        Welcome {$User_Name}
        {/if}
    </td>
    <td height="22" align="right"><b>Date:</b> <script>document.write(getDate(new Date()));</script> <b>Time:</b><label id="timeId"><script>SetTime()</script></label>&nbsp;&nbsp;&nbsp;</td>
  </tr>
</table>