<script language="javascript">
	var msg_CatDel		= '{$msg_CatDel}';
</script>

<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
<form name="frmCategory" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">{$L_Link_Manager}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						{$L_Manage_Links}
					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
				  <td bgcolor="#FFFFFF">
				  
						<table border="0" align="center" cellpadding="0" cellspacing="0" width="740" >
							<tr>
								<td valign="top" align="center">
									<table border="0" cellpadding="0" cellspacing="1" width="95%">
										<tr><td colspan="4" class="stdSection" align="right">
											<a href="javascript: Add_Click();"><img src="{$Templates_Image}a_add.gif" class="imgAction" title="Add Page"></a>
										<tr><td colspan="4" class="successMsg" align="center">{$Message}</td></tr>
										<tr>
											<td class="listHeader">{$L_Category_Name}</td>
											<td class="listHeader" colspan="3">{$L_Action}</td>
										</tr>
										{if $No_Record_Found}
										<tr><td colspan="4">&nbsp;</td></tr>
										<tr><td colspan="4" align="center" class="errorMsg">{$No_Record_Found}</td></tr>
										<tr><td colspan="4">&nbsp;</td></tr>										
										{/if}
										
										{foreach name=LinkInfo from=$LinkInfo item=Link}
										<tr class="list_B">
											<td class="list_C">{$Link.cat_name|capitalize}</td>
											<td width="15%" class="list_C" align="center">
												<img src="{$Templates_Image}a_edit.gif" class="imgAction" title="Edit" onClick="javascript: Edit_Click('{$Link.cat_id}');">
											</td>
											<td width="15%" class="list_C" align="center">
												<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="javascript: Delete_Click('{$Link.cat_id}');">
											</td>
											<td width="15%" align="center" class="list_C"><a href="javascript: Add_Link_Click('{$Link.cat_id}');" class="actionLink">{$L_Link_Add_Link}</a></td>
										</tr>
										
											{foreach name=LinkDtlInfo from=$LinkDtlInfo item=LinkDtl}
											{if $LinkDtl.link_category_id == $Link.cat_id}
											<tr>
												<td colspan="4" align="center">
													<table border="0" cellpadding="0" cellspacing="0" width="90%">
														<tr class="list_C">
															<td>{$LinkDtl.link_title|capitalize}</td>
															<td width="15%" align="center">
																<img src="{$Templates_Image}a_edit.gif" class="imgAction" title="Edit" onClick="javascript: Edit_Link_Click('{$LinkDtl.link_id}');">
															</td>
															<td width="15%" align="center" colspan="2">
																<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="javascript: Delete_Link_Click('{$LinkDtl.link_id}');">
															</td>
														</tr>
													</table>
												</td>
											</tr>
											{/if}
											{/foreach}
										
										{/foreach}
										
										<tr><td colspan="4">&nbsp;</td></tr>
										<tr>
											<td colspan="4" class="pageLink" align="right">{$Page_Link}</td>
										</tr>
										<input type="hidden" name="cat_id" value="{$cat_id}">
										<input type="hidden" name="link_id" value="{$link_id}">
										<input type="hidden" name="Action">
										<input type="hidden" name="start" value="{$Start}">
									</table>
								</td>
							</tr>
						</table>				  
				  
					</td>
				</tr>

			</table>
		</td>
	</tr>
	</form>
</table>





