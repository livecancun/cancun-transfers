{foreach name=PrivilegeInfo from=$AssignedPrivileges key=Key item=Type}
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_{$Key|lower}.gif"></td>
					<td class="stdSection" width="99%">{$Privileges[$Key].Title}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="5" cellspacing="1" width="90%">
				<tr>
					<td valign="top" align="center">
						<table border="0" cellpadding="1" cellspacing="2">
							{foreach key=key item=Option from=$Privileges[$Key].SubOption}
							<tr>
								<td align="center" width="1%"><img src="{$Templates_Image}b_arrow.gif"></td>
								<td width="99%"><a href="{$Option.Link}" class="MenuLink" title="{$Option.Desc}">{$Option.Title}</a></td>
							</tr>
							{/foreach}
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
{/foreach}