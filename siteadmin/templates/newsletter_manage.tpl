<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmNewsletter" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">Newsletter Manager</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						Manage newsletter details. You can add/modify/delete newsletter details. 
					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td colspan="3" align="right">
					Page Size:
					<select name="page_size" onChange="document.frmNewsletter.submit();">
						{html_options options=$PageSize selected=$smarty.session.page_size}
					</select>&nbsp;&nbsp;&nbsp;
					<a href="{$smarty.server.PHP_SELF}?Action=Add"><img src="{$Templates_Image}a_add.gif" class="imgAction" title="Add Page"></a>
					</td>
				</tr>
				<tr>
					<td class="listHeader" width="5%">&nbsp;</td>
					<td class="listHeader" width="70%">Newsletter Title</td>
					<td class="listHeader" width="25%">Action</td>
				</tr>
				{foreach name=NewsletterInfo from=$NewsletterInfo item=Newsletter}
				<tr class="{cycle values='list_A, list_B'}">
					<td><input type="checkbox" name="newsletter_id1[]" class="stdCheckBox" value="{$Newsletter->newsletter_id}"></td>
					<td>{$Newsletter->newsletter_title}</td>
					<td align="center">
						<img src="{$Templates_Image}a_view.gif" class="imgAction" title="View" onClick="JavaScript: View_Click('{$Newsletter->newsletter_id}');">&nbsp;
						<img src="{$Templates_Image}a_edit.gif" class="imgAction" title="Edit" onClick="JavaScript: Edit_Click('{$Newsletter->newsletter_id}');">&nbsp;
						<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: Delete_Click('{$Newsletter->newsletter_id}');">&nbsp;
						<img src="{$Templates_Image}a_email.gif" class="imgAction" title="Send Mail" onClick="JavaScript: SendMail_Click('{$Newsletter->newsletter_id}');">
					</td>
				</tr>
				{foreachelse}
				<tr>
					<td colspan="5">No static page available.</td>
				</tr>
				{/foreach}
			</table>
			{if $smarty.foreach.NewsletterInfo.total > 1}
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td>
						<img src="{$Templates_Image}arrow_ltr.png"> 
						<a href="JavaScript: CheckUncheck_Click(document.all['newsletter_id1[]'], true);" onMouseMove="window.status='Check All';" onMouseOut="window.status='';" class="actionLink">Check All</a> / 
						<a href="JavaScript: CheckUncheck_Click(document.all['newsletter_id1[]'], false);" onMouseMove="window.status='Uncheck All';" onMouseOut="window.status='';" class="actionLink">Uncheck All</a>  &nbsp;&nbsp;
						With selected
						<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: DeleteChecked_Click();">
					</td>
				</tr>
			</table>
			{/if}
			{if $total_record > $smarty.session.page_size}
			<br>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td valign="top" width="33%">
						{html_pager num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record image_path=$Templates_Image add_prevnext_text=true}
					</td>
					<td align="center">
						{html_pager_text num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record}
					</td>
					<td align="right"  width="33%">
						Goto Page: <input type="text" name="goto_page" size="5">
						<input type="submit" name="go" value="Go" class="stdFilterButton">
					</td>
				</tr>
			</table>
			{/if}
			<input type="hidden" name="Action">
			<input type="hidden" name="newsletter_id">
			<input type="hidden" name="status">
		</td>
	</tr>
	</form>
</table>