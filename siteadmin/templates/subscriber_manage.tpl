<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmSubscriber" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="49%">Subscriber Management</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						Manage your user contents.
					</td>
				</tr>
				<tr><td height="2"></td></tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td colspan="6" align="right">
					Page Size:
					<select name="page_size" onChange="document.frmSubscriber.submit();">
						{html_options options=$PageSize selected=$smarty.session.page_size}
					</select>&nbsp;&nbsp;&nbsp;
						<a href="?Action=Add"><img src="{$Templates_Image}a_add.gif" class="imgAction" title="Add Subscriber"></a>&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td class="listHeader" width="3%">&nbsp;</td>
					<td class="listHeader" width="45%">Email</td>
					<td class="listHeader" width="25%">Subscribed</td>
					<td class="listHeader" width="25%">Action</td>
				</tr>
				{foreach name=SubscriberInfo from=$SubscriberInfo item=Subscriber}
				<tr class="{cycle values='list_A, list_B'}">
					<td><input type="checkbox" name="subscriber_list[]" class="stdCheckBox" value="{$Subscriber->subscriber_id}"></td>
					<td align="left">{$Subscriber->subscriber_email}</td>
					<td align="center">
						{if $Subscriber->subscriber_status}
							<b>Yes</b>
							(<a href="JavaScript: ToggleStatus_Click('{$Subscriber->subscriber_id}', '0');" class="actionLink">No</a>)
						{else}
							<b>No</b>
							(<a href="JavaScript: ToggleStatus_Click('{$Subscriber->subscriber_id}', '1');" class="actionLink">Yes</a>)
						{/if}
					</td>
					<td align="center">
						<!--img src="{$Templates_Image}a_view.gif" class="imgAction" title="View" onClick="JavaScript: View_Click('{$Subscriber->subscriber_id}');"-->&nbsp;
						<img src="{$Templates_Image}a_edit.gif" class="imgAction" title="Edit" onClick="JavaScript: Edit_Click('{$Subscriber->subscriber_id}');">&nbsp;
						<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: Delete_Click('{$Subscriber->subscriber_id}');">
					</td>
				</tr>
				{foreachelse}
				<tr>	
					<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td colspan="5">No subscriber information yet inserted.</td>
				</tr>
				{/foreach}
			</table>
			{if $smarty.foreach.SubscriberInfo.total > 0}
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td>
						<img src="{$Templates_Image}arrow_ltr.png"> 
						<a href="JavaScript: CheckUncheck_Click(document.all['subscriber_list[]'], true);" onMouseMove="window.status='Check All';" onMouseOut="window.status='';" class="actionLink">Check All</a> / 
						<a href="JavaScript: CheckUncheck_Click(document.all['subscriber_list[]'], false);" onMouseMove="window.status='Uncheck All';" onMouseOut="window.status='';" class="actionLink">Uncheck All</a>  &nbsp;&nbsp;
						With selected
						<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: DeleteChecked_Click();">
						<input type="hidden" name="subscriber_id">
						<input type="hidden" name="subscriber_status">
						<input type="hidden" name="Action" value="{$ACTION}">
					</td>
				</tr>
			</table>
			{/if}
			{if $total_record > $smarty.session.page_size}
			<br>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td valign="top" width="33%">
						{html_pager num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record image_path=$Templates_Image add_prevnext_text=true}
					</td>
					<td align="center">
						{html_pager_text num_items=$total_record per_page=$smarty.session.page_size start_item=$smarty.session.start_record}
					</td>
					<td align="right"  width="33%">
						Goto Page: <input type="text" name="goto_page" size="5">
						<input type="submit" name="go" value="Go" class="stdFilterButton">
					</td>
				</tr>
			</table>
			{/if}
			<br><br>
		</td>
	</tr>
	</form>
</table>