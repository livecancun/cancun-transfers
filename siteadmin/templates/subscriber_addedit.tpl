<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmSubscriber" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif"></td>
					<td class="stdSection" width="49%">Subscriber Management [{$Action}]</td>
					<td align="right" width="50%">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						Change subscriber details and click <b>Save</b> to save the changes.
						Click <b>Cancel</b> to discard the changes.
					</td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
				<tr><td height="5"></td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td align="left">
						<input type="submit" name="Submit" value="Save" class="stdButton" onClick="return Validate_Form(document.frmSubscriber);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">					</td>
					<td width="70%" align="right" class="mandatory">Fields marked with (<font class="mandatoryMark">*</font>) are mandatory.</td>
				</tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						Subscriber Info </td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top" width="30%">Email: <font class="mandatoryMark">*</font></td>
					<td class="fieldInputStyle">
						<input type="text" name="subscriber_email" size="50" maxlength="120" value="{$Subscriber->subscriber_email}"><br>
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top" width="30%">Subscribed:</td>
					<td class="fieldInputStyle">
						<select name="subscriber_status">
						{html_options options=$YesNo_List selected=$Subscriber->subscriber_status}
						</select>
					</td>
				</tr>
				<tr><td class="divider" colspan="2"></td></tr>
				<tr>
					<td align="left" colspan="2">
						<input type="submit" name="Submit" value="Save" class="stdButton" onClick="return Validate_Form(document.frmSubscriber);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
						<input type="hidden" name="Action" value="{$Action}">
					</td>
				</tr>
			</table>	
			<br>
			<input type="hidden" name="subscriber_id" value="{$Subscriber->subscriber_id}">
		</td>
	</tr>
	</form>
</table>