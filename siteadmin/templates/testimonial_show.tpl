<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="100%"> 
	<form name="frmTestimonial" action="{$A_Action}" method="post" enctype="multipart/form-data"> 
	<tr> 
		<td class="stdSectionHeader"> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%"> 
				<tr> 
					<td class="stdSection">{$Testimonial_Header}</td> 
				</tr> 
			</table>
		</td> 
	</tr> 
	<tr> 
		<td valign="top" align="center"> 
			<table border="0" cellpadding="2" cellspacing="2" width="97%"> 
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr> 
					<td class="fieldLabelRight" valign="top" width="15%">{$Name} :</td> 
					<td class="fieldInputStyle">{$Person_Name}</td> 
				</tr> 
<!--				<tr> 
					<td class="fieldLabelRight" valign="top">{$Company} :</td> 
					<td class="fieldInputStyle">{$Person_Company}</td> 
				</tr> 
-->				
				{if $Person_Address}
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Address} :</td> 
					<td class="fieldInputStyle">{$Person_Address}</td> 
				</tr> 
				{/if}
				
				{if $Person_City}				
				<tr> 
					<td class="fieldLabelRight" valign="top">{$City} :</td> 
					<td class="fieldInputStyle">{$Person_City}</td> 
				</tr> 
				{/if}
				
				{if $Person_State}				
				<tr> 
					<td class="fieldLabelRight" valign="top">{$State} :</td> 
					<td class="fieldInputStyle">{$Person_State}</td> 
				</tr> 
				{/if}
				
				{if $Person_Country}				
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Country} :</td> 
					<td class="fieldInputStyle">{$Person_Country}</td> 
				</tr> 
				{/if}
				
				{if $Person_Zip}				
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Zip} :</td> 
					<td class="fieldInputStyle">{$Person_Zip}</td> 
				</tr> 
				{/if}
				
				{if $Person_Phone}				
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Phone} :</td> 
					<td class="fieldInputStyle">{$Person_Phone}</td> 
				</tr> 
				{/if}
				
				{if $Person_Fax}				
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Fax} :</td> 
					<td class="fieldInputStyle">{$Person_Fax}</td> 
				</tr> 
				{/if}
				
				{if $Person_Email}				
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Email} :</td> 
					<td class="fieldInputStyle">{$Person_Email}</td> 
				</tr> 
				{/if}
				
				{if $Person_Website}				
				<tr> 
					<td class="fieldLabelRight" valign="top" width="22%">{$Website} :</td> 
					<td class="fieldInputStyle">{$Person_Website}</td> 
				</tr> 
				{/if}
				
				{if $Person_Pic != ''}
				<tr>
					<td class="fieldLabelRight" valign="top">{$Photo} :</td> 
					<td class="fieldInputStyle" align="left">
						<img src="{$Picture_Filename}" id="pic" border="0">
						<input type="hidden" name="person_pic_del" value="{$Person_Pic}">
					</td>
				</tr>
				{/if}

				
				{if $Person_Comment}				
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Comment} :</td> 
					<td class="fieldInputStyle">{$Person_Comment}</td> 
				</tr> 
				{/if}
				
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Display_On_Website} :</td> 
					<td class="fieldInputStyle">{$Person_Status}</td> 
				</tr> 
				<tr> 
					<td colspan="2" height="5" class="divider"></td> 
				</tr> 
				<tr> 
					<td></td> 
					<td>
						{if $ACTION == $A_View}
						<input type="button" name="Submit" value="{$Back}" class="stdButton" onclick="javascript: Cancel_Click('{$Cancel_Action}')"> 
						{else}
						<input type="submit" name="Submit" value="{$Delete}" class="stdButton"> 
						<input type="button" name="Submit" value="{$Cancel}" class="stdButton" onclick="javascript: Cancel_Click('{$Cancel_Action}')"> 
						{/if}
						<input type="hidden" name="Action" value="{$ACTION}">
						<input type="hidden" name="person_id" value="{$Person_Id}">
					</td> 
				</tr> 
			</table>
		</td> 
	</tr> 
	<tr> 
		<td>&nbsp;</td> 
	</tr> 
	</form> 
</table>
