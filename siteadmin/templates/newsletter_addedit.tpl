<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmNewaddedit" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="49%">Page Manager [{$Action}]</td>
					<!--td class="stdSection" width="50%" align="right"><img src="{$Templates_Image}icon_help.gif" title="Help">&nbsp;</td-->
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr><td height="5"></td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td align="left">
						<input type="submit" name="Submit" value="Save" class="stdButton" onclick="JavaScript: return Validate_Form(document.frmNewaddedit);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
					<td align="right" class="mandatory">Fields marked with (<font class="mandatoryMark">*</font>) are mandatory.</td>
				</tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%" id="pageMeta" style="display:{if $page_type=='OutsideLink'}none{else}block{/if};">
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						Newsletter Information 
					</td>
				</tr>
				<tr>
					<td width="25%" class="fieldLabelLeft" valign="top">NewsLetter Title:<font class="mandatoryMark">*</font></td>
					<td width="75%" class="fieldInputStyle">
						<input type="text" value="{$Newsletter->newsletter_title}" name="newsletter_title" size="40" maxlength="120">
					</td>
				</tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%" id="pageContent" style="display:{if $page_type=='iFrame' || $page_type=='OutsideLink'}none{else}block{/if};">
				<tr>
					<td colspan="2" align="center"><br>
						Enter the newsletter description over here. This is a sample newsletter design. Just edit it using editing tools provided below. 
					</td>
				</tr>
				<tr>
					<td class="fieldInputStyle" colspan="2" align="center">
						{html_richtext toolbar='full' width='99%' height='500' RichTextName='newsletter_desc' RichTextValue=$Newsletter->newsletter_desc lang=$LangInfo.code}
					</td>
				</tr>
			</table>
			<input type="hidden" name="Action" value="{$Action}">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td class="divider" colspan="4"></td></tr>
				<tr>
				  <td align="left" colspan="2">
						<input type="submit" name="Submit" value="Save" class="stdButton" onclick="JavaScript: return Validate_Form(document.frmNewaddedit);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
						<input type="hidden" name="newsletter_id" value="{$Newsletter->newsletter_id}">
					</td>
				</tr>
			</table>	
			<br>
		</td>
	</tr>
	</form>
</table>