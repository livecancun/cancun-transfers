<script>
	var Empty_Name		= '{$Empty_Name}';
	var Empty_Company  	= '{$Empty_Company}';
	var Valid_Zip 		= '{$Valid_Zip}';
	var Empty_Phone		= '{$Empty_Phone}';
	var Valid_Phone  	= '{$Valid_Phone}';
	var Empty_Fax 		= '{$Empty_Fax}';
	var Valid_Fax 		= '{$Valid_Fax}';
	var Empty_TollFree 	= '{$Empty_TollFree}';
	var Valid_TollFree 	= '{$Valid_TollFree}';
	var Empty_Email 	= '{$Empty_Email}';
	var Valid_Email 	= '{$Valid_Email}';
	var Valid_URL 		= '{$Valid_URL}';
	var Valid_Photo 	= '{$Valid_Photo}';			
	var Empty_Description	= '{$Empty_Description}';
	var Empty_Comment 	= '{$Empty_Comment}';	

	var Delete_Testimonial 		= '{$Delete_Testimonial}';	
	var Delete_AllTestimonials 	= '{$Delete_AllTestimonials}';	
</script>

<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="100%"> 
	<form name="frmTestimonial" action="{$A_Action}" method="post" enctype="multipart/form-data"> 
	<tr> 
		<td class="stdSectionHeader"> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%"> 
				<tr> 
					<td class="stdSection">{$Testimonial_Header}</td> 
					<td class="stdSection" align="right"><a href="testimonial.php" class="otherLink">{$Back}</a></td>
				</tr> 
			</table>
		</td> 
	</tr> 
	<tr> 
		<td valign="top" align="center"> 
			<table border="0" cellpadding="2" cellspacing="2" width="97%"> 
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr> 
					<td class="fieldLabelRight" valign="top" width="15%">{$Name} :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_name" value="{$Person_Name}" size="35" maxlength="50"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Address} :</td> 
					<td class="fieldInputStyle" colspan="3"><textarea name="person_address" rows="5" cols="50">{$Person_Address}</textarea></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$City} :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_city" value="{$Person_City}" size="25" maxlength="25"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$State} :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_state" value="{$Person_State}" size="10" maxlength="5"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Country} :</td> 
					<td class="fieldInputStyle" colspan="3"><SELECT name="person_country" class="texto_content"><option value="0">-----------{$L_Select_Country}--------</option>{$CountryList}</SELECT></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Zip} :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_zip" value="{$Person_Zip}" size="10" maxlength="5" onKeyPress="javascript: isNumericKey(this.value)"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Phone} :</td> 
					<td class="fieldInputStyle" colspan="3">
						<input type="text" name="person_phone_areacode" value="{$Person_Phone_Areacode}" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
						<input type="text" name="person_phone_citycode" value="{$Person_Phone_Citycode}" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
						<input type="text" name="person_phone_no" value="{$Person_Phone_No}" size="4" maxlength="4" onKeyPress="javascript: isNumericKey(this.value)">
					</td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Fax} :</td> 
					<td class="fieldInputStyle" colspan="3">
						<input type="text" name="person_fax_areacode" value="{$Person_Fax_Areacode}" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
						<input type="text" name="person_fax_citycode" value="{$Person_Fax_Citycode}" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
						<input type="text" name="person_fax_no" value="{$Person_Fax_No}" size="4" maxlength="4" onKeyPress="javascript: isNumericKey(this.value)">
					</td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Email} :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_email" value="{$Person_Email}" size="35" maxlength="255"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top" width="22%">{$Website} :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_website" value="{$Person_Website}" size="50" maxlength="255"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Photo} :</td> 
					<td class="fieldInputStyle" colspan="3">
						<input type="file" name="person_pic" size="25" maxlength="255" {if $Picture_Filename} onchange="javascript: UploadImage_Change(this, pic, '{$Picture_Filename}','');" {/if}>
						<input type="hidden" name="person_pic_edit" value="{$Person_Pic}"><br>
						<img src="{$Picture_Filename}" id="pic" border="0"><br>
						{if $Person_Pic != ''}
						<input type="checkbox" name="delete_picture" value="1" class="stdCheckBox"> {$Delete_Picture}
						{/if}
					</td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Comment} :</td> 
					<td class="fieldInputStyle" colspan="3"><textarea name="person_comment" rows="5" cols="50">{$Person_Comment}</textarea></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top">{$Display_On_Website} :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="checkbox" name="person_status" value="1" {$Is_Status_Check}></td> 
				</tr> 
				<tr> 
					<td></td> 
					<td>
						<input type="submit" name="Submit" value="{$Save}" class="stdButton" onClick="javascript: return Form_Submit(document.frmTestimonial);"> 
						&nbsp;&nbsp;<input type="button" name="Submit" value="{$Cancel}" class="stdButton" onClick="javascript: Cancel_Click('{$Cancel_Action}');">&nbsp;&nbsp;
						<input type="hidden" name="Action" value="{$ACTION}">
						<input type="hidden" name="person_id" value="{$Person_Id}">
					</td> 
				</tr> 
			</table>
		</td> 
	</tr> 
	<tr> 
		<td>&nbsp;</td> 
	</tr> 
	</form> 
</table>
