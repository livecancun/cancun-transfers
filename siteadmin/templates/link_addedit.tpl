<script language="javascript">
	var msg_Name		= '{$msg_Name}';
	var msg_Address		= '{$msg_Address}';
	var msg_City		= '{$msg_City}';
	var msg_State		= '{$msg_State}';
	var msg_Zip			= '{$msg_Zip}';
	var msg_Country  	= '{$msg_Country}';
	var msg_Telephone	= '{$msg_Telephone}';
	var msg_Title		= '{$msg_Title}';
	var msg_Description	= '{$msg_Description}';
	var msg_Url			= '{$msg_Url}';
	var msg_Our_Url		= '{$msg_Our_Url}';
	var msg_Email		= '{$msg_Email}';
</script>

<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
<form name="frmLinkDetail"  action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">{$L_Link_Manager} [ {$L_Action} ]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						{$L_Manage_Links}
					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
				  <td bgcolor="#FFFFFF">
				  
						<table border="0" align="center" cellpadding="0" cellspacing="0" width="740" >
							<tr>
								<td valign="top" align="center">
									<table border="0" cellpadding="0" cellspacing="1" width="750">
											<tr>
												<td valign="top" align="center">
													<table border="0" cellpadding="1" cellspacing="3" width="97%">
														<tr height="20">
															<td colspan="2" align="center" class="successMsg">&nbsp;{$Message}</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
														<tr>
															<td class="fieldLabelRight" >{$L_Link_Name} :</td>
															<td class="fieldInputStyle" ><input name="url_name" type="text" id="url_name" value="{$url_name}"  maxlength="255" ></td>
														</tr>
														<tr>
															<td class="fieldLabelRight" >{$L_Link_Address} :</td>
															<td class="fieldInputStyle" ><textarea name="url_address" cols="50" rows="3" id="url_address">{$url_address}</textarea></td>
														</tr>	
														<tr>
															<td class="fieldLabelRight" >{$L_Link_City} :</td>
															<td class="fieldInputStyle" ><input name="url_city" type="text" id="url_city" value="{$url_city}"  maxlength="255" ></td>
														</tr>	
														<tr>
															<td class="fieldLabelRight" >{$L_Link_State} :</td>
															<td class="fieldInputStyle" ><input name="url_state" type="text" id="url_state" value="{$url_state}"  maxlength="255" ></td>
														</tr>	
														<tr>
															<td class="fieldLabelRight" >{$L_Link_Zip} :</td>
															<td class="fieldInputStyle" ><input name="url_zip" type="text" id="url_zip" value="{$url_zip}"  maxlength="5"  onKeyPress="javascript: isNumericKey(this.value)"></td>
														</tr>	
														<tr>
															<td class="fieldLabelRight" >{$L_Link_Country} :</td>
															<td class="fieldInputStyle" ><input name="url_country" type="text" id="url_country" value="{$url_country}"  maxlength="255" ></td>
														</tr>
														<tr>
															<td class="fieldLabelRight" >{$L_Link_Telephone} :</td>
														  	<td class="fieldInputStyle">
																<input type="text" name="url_telno_areacode" size="3" maxlength="3" value="{$url_telno_areacode}" onKeyPress="javascript: isNumericKey(this.value)"> -
																<input type="text" name="url_telno_citycode" size="3" maxlength="3" value="{$url_telno_citycode}" onKeyPress="javascript: isNumericKey(this.value)"> -
																<input type="text" name="url_telno_no" size="4" maxlength="4" value="{$url_telno_no}" onKeyPress="javascript: isNumericKey(this.value)">
															</td>	
														</tr>	
														<tr>
															<td class="fieldLabelRight" >{$L_Link_Title} :</td>
															<td class="fieldInputStyle" ><input name="url_title" type="text" value="{$url_title}" size="50"  maxlength="255" ></td>
														</tr>
														<tr>
															<td class="fieldLabelRight" valign="top" width="35%" >{$L_Link_Category} :</td>
															<td width="75%" class="fieldInputStyle">{$Link_Category}</td>
														</tr>	
														<tr>
															<td width="35%" class="fieldLabelRight" valign="top">{$L_Link_Description} :</td>
															<td class="fieldInputStyle">
															<textarea name="url_desc" rows="3" cols="50">{$url_desc}</textarea>
															</td>
														</tr>
														<tr>
															<td class="fieldLabelRight" >{$L_Link_Url} :</td>
															<td class="fieldInputStyle" >
															{if $url_url}
																<input name="url_url" type="text" value="{$url_url}" size="50"  maxlength="255" >
															{else}
																<input name="url_url" type="text" value="http://" size="50"  maxlength="255" >
															{/if}
															</td>
														</tr>
														<tr>
															<td class="fieldLabelRight" >{$L_Link_Our_Url} :</td>
															<td class="fieldInputStyle" >
															
															{if $url_oururl}
																<input name="url_oururl" type="text" id="url_oururl" value="{$url_oururl}" size="50"  maxlength="255" >
															{else}
																<input name="url_oururl" type="text" value="http://" size="50"  maxlength="255">
															{/if}
															
															
															
															</td>
														</tr>				
														<tr>
															<td class="fieldLabelRight">{$L_Link_Email} :</td>
															<td class="fieldInputStyle" ><input type="text" name="url_email" value="{$url_email}"  maxlength="255" size="50" ></td>
														</tr>
														<tr>
															<td class="fieldLabelRight">{$L_Link_Status} :</td>
															<td width="75%" class="fieldInputStyle"> 
															<select name = "cat_status">
															{$Status_List}
															</select>
															</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
														<tr>
															<td colspan="8" align="center" >
																<input type="submit" name="Submit" value="{$Save}{$Update}" class="nrlButton" onClick="javascript: return Form_LinkSubmit(document.frmLinkDetail);">
																<input type="submit" name="Submit" value="{$Cancel}" class="nrlButton">
															</td>
														</tr>
														<tr>
															<td colspan="2">
																<input type="hidden" name="Action" value="{$ACTION}">
																<input type="hidden" name="start" value="{$Start}">
																<input type="hidden" name="link_id" value="{$link_id}">
																<input type="hidden" name="cat_id" value="{$cat_id}">
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
								</td>
							</tr>
						</table>				  
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>