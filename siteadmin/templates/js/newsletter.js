//====================================================================================================
//	Function Name	:	Validate_Form()
//----------------------------------------------------------------------------------------------------
function Validate_Form(frm)
{
	with(frm)
    {
			if(!IsEmpty(newsletter_title, 'Please, enter newsletter Title.'))
			{
				return false;
			}
			return true;
	}
}
//====================================================================================================
//	Function Name	:	Form_Submit_Send()
//----------------------------------------------------------------------------------------------------
function Form_Submit_Send(frm)
{
	with(frm)
    {
		if(!IsEmpty(email_to, 'Please, Enter Email to send.'))
        {
			return false;
        }
		if(!IsEmpty(email_subject, 'Please, Enter Subject.'))
        {
			return false;
        }
        return true;
    }

}
//====================================================================================================
//	Function Name	:	View_Click()
//----------------------------------------------------------------------------------------------------
function View_Click(newsletter_id)
{
	//with(document.frmUserSelectletter)
	//{
			//alert(newsletter_id);

		popupWindowURL('newsletter.php?Action=View&newsletter_id='+newsletter_id, 'list', 600, 550, false, true, true);
	//}
}

//====================================================================================================
//	Function Name	:	Select_Contact()
//----------------------------------------------------------------------------------------------------
function Select_Contact(fld)
{
	popupWindowURL('subscriber.php?Action=List&fld='+fld, 'list', 600, 300, false, false, true);
}

//====================================================================================================
//	Function Name	:	Edit_Click()
//----------------------------------------------------------------------------------------------------
function Edit_Click(newsletterId)
{
	with(document.frmNewsletter)
	{
		newsletter_id.value		= newsletterId;
		Action.value	= 'Edit';
		submit();
	}
}
//====================================================================================================
//	Function Name	:	SendMail_Click()
//----------------------------------------------------------------------------------------------------
function SendMail_Click(newsletterId)
{
	with(document.frmNewsletter)
	{
		newsletter_id.value		= newsletterId;
		Action.value	= 'SendMail';
		//alert(Action.value);
		submit();
	}
}
//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function Delete_Click(newsletterId)
{
	with(document.frmNewsletter)
	{
		if(confirm('Are you sure you wants to delete newsletter?'))
		{
			newsletter_id.value = newsletterId;
			Action.value		= 'Delete';
			submit();
		}
	}
}
//====================================================================================================
//	Function Name	:	DeleteChecked_Click()
//----------------------------------------------------------------------------------------------------
function DeleteChecked_Click()
{
	with(document.frmNewsletter)
	{
		var flg=false;

		if(document.all['newsletter_id1[]'].length)
		{
			for(i=0; i < document.all['newsletter_id1[]'].length; i++)
			{
				if(document.all['newsletter_id1[]'][i].checked)
					flg = true;
			}
		}
		else
		{
			if(document.all['newsletter_id1[]'].checked)
				flg = true;
		}

		if(!flg)
		{
			alert('Please select Newsletter(s) you want to delete.');
			return false;
		}
			
		if(confirm('Are you sure you want to delete selected Newsletter(s)?'))
		{
			Action.value 	= 'DeleteSelected';
			submit();
		}
	}
}
//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function ChangeOrder_Click(direction)
{
	var list = document.frmPage.page_list;
	var tmpVal;
	var tmpText;

	if(list.selectedIndex==-1)
	{
		alert("Select the page you want to move.");
		list.focus();
		return;
	}


	if(!list.length) return;
	
	if(direction==1)
	{
		if(list.selectedIndex == list.length-1) return;
	}
	else
		if(list.selectedIndex == 0) return;

	tmpVal 	= list.options[list.selectedIndex+direction].value;
	tmpText	= list.options[list.selectedIndex+direction].text;

	list.options[list.selectedIndex+direction].value 	= list.options[list.selectedIndex].value;
	list.options[list.selectedIndex+direction].text 	= list.options[list.selectedIndex].text;

	list.options[list.selectedIndex].value 	= tmpVal;
	list.options[list.selectedIndex].text 	= tmpText;

	list.selectedIndex += direction;
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function SortPage_Click()
{
	with(document.frmPage)
	{
		for(i=0; i<page_list.length; i++)
			page_order.value += page_list[i].value + ':';
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function ToggleStatus_Click(pageId, state)
{
	with(document.frmPage)
	{
		pid.value 		= pageId;
		status.value 	= state;
		Action.value	= 'ChangeStatus';
		submit();
	}
}
//====================================================================================================
//	Function Name	:	OK_Click()
//----------------------------------------------------------------------------------------------------
function OK_Click()
{
	var templist='',j=0;
	//alert(document.all["user_list[]"].length);
	if(!document.all["user_list[]"].length)
		templist += document.all["user_list[]"].value;
		
	for(i=0; i < document.all["user_list[]"].length; i++)
	{
		if(document.all["user_list[]"][i].checked == true)
		{
			templist += document.all["user_list[]"][i].value;
			templist += ',';
		}
	}
	//alert(templist);
	/*
	str = opener.document.frmNews.email_selected.value;
	newstr = str.replace(re,"");
	temp = newstr.slice(0,-2)
	*/
	
	opener.document.frmNews.email_to.value = templist;
	window.close();
	opener.window.focus();
}
//====================================================================================================
//	Function Name	:	Close_Click()
//----------------------------------------------------------------------------------------------------
function Close_Click()
{
	window.close();
	opener.window.focus();

}