//====================================================================================================
//	Function Name	:	Form_Submit
//----------------------------------------------------------------------------------------------------
function Validate_Form(frm)
{
	with(frm)
    {		
		if(!IsEmpty(subscriber_email, 'Please, enter your email address.'))
		{
			return false;    
		}
		
		if(!IsEmail(subscriber_email, 'Please, enter valid email address.'))
		{
			return false;
		}
	}
	return true;    
}

//====================================================================================================
//	Function Name	:	View_Click()
//----------------------------------------------------------------------------------------------------
function View_Click(SubscriberId)
{
	popupWindowURL('subscriber.php?Action=View&subscriber_id='+SubscriberId, 'list', 400, 400, false, false, true);
}

//====================================================================================================
//	Function Name	:	Edit_Click()
//----------------------------------------------------------------------------------------------------
function Edit_Click(SubscriberId)
{
	with(document.frmSubscriber)
	{	
		subscriber_id.value	= SubscriberId;
		Action.value			= 'Edit';
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function Delete_Click(SubscriberId)
{
	with(document.frmSubscriber)
	{
		if(confirm('Are you sure you want to delete user?'))
		{
			subscriber_id.value	= SubscriberId;
			Action.value			= 'Delete';
			submit();
		}
	}
}

//====================================================================================================
//	Function Name	:	DeleteChecked_Click()
//----------------------------------------------------------------------------------------------------
function DeleteChecked_Click()
{
	with(document.frmSubscriber)
	{
		var flg=false;

		if(document.all['subscriber_list[]'].length)
		{
			for(i=0; i < document.all['subscriber_list[]'].length; i++)
			{
				if(document.all['subscriber_list[]'][i].checked)
					flg = true;
			}
		}
		else
		{
			if(document.all['subscriber_list[]'].checked)
				flg = true;
		}

		if(!flg)
		{
			alert('Please, select the user(s) you want to delete.');
			return false;
		}
			
		if(confirm('Are you sure you want to delete selected user(s)?'))
		{
			Action.value 	= 'DeleteSelected';
			submit();
		}
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function ToggleStatus_Click(SubscriberId, state)
{
	with(document.frmSubscriber)
	{
		subscriber_id.value	= SubscriberId;
		subscriber_status.value	= state;
		Action.value			= 'ChangeStatus';
		submit();
	}
}