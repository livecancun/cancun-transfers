
//====================================================================================================
//	File Name		:	siteconfig.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Client side validation in JavaScript.
//	Author			:	Dinesh Sailor
//	Creation Date	:	05-May-2003
//	Copyright		:	Copyrights � 2003 Dot Infosys
//	Email			:	dinesh@dotinfosys.com
//	History			:
//						Date				Author					Remark
//						05-May-2003			Dinesh Sailor			Initial Release
//
//====================================================================================================

function Form_Submit(frm)
{
	with(frm)
    {
			if(!IsEmpty(company_title, 'Please, enter your company name.'))
			{
				return false;    
			}
			if(!IsEmpty(site_title, 'Please, enter your site title.'))
			{
				return false;
			}
			if(!IsEmpty(copyright_text, 'Please, enter your copyright text.'))
			{
				return false;
			}
			if(!IsEmpty(support_email, 'Please, enter your email address.'))
			{
				return false;    
			}
			if(!IsEmail(support_email, 'Please, enter valid email address.'))
			{
				return false;
			}
			if(!IsEmpty(paypal_email, 'Please, enter paypal email address.'))
			{
				return false;    
			}
			if(!IsEmail(paypal_email, 'Please, enter valid paypal email address.'))
			{
				return false;
			}

		return true;
    }
}

