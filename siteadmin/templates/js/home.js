//====================================================================================================
//	Function Name	:	Validate_Form
//----------------------------------------------------------------------------------------------------
function Validate_Form(frm)
{
	with(frm)
    {		
		if(!IsEmpty(home_address, 'Please, enter Address.'))
		{
			return false;
		}

		if(!IsEmpty(home_city, 'Please, enter City.'))
		{
			return false;
		}
		
		if(!IsEmpty(home_state, 'Please, select State.'))
		{
			return false;
		}
		
		if(!IsEmpty(home_zip, 'Please, enter Zip code.'))
		{
			return false;
		}
		
		if(IsEmpty(home_zip,''))
		{
			if(!IsZip(home_zip,'Please, enter valid Zip code.'))
			{
				return false;
			}
		}	

		if(!IsEmpty(home_price, 'Please, enter List Price.'))
		{
			return false;
		}
		
		if(IsEmpty(home_price,''))
		{
			if(!IsCurrency(home_price,'Please, enter valid List Price.'))
			{
				return false;
			}
		}	
/*	
		if(!IsEmpty(home_fsbo_id, 'Please, select Fsbo.'))
		{
			return false;
		}
*/	}
	return true;    
}

//====================================================================================================
//	Function Name	:	View_Click()
//----------------------------------------------------------------------------------------------------
function View_Click(HomeId)
{
	//popupWindowURL('home.php?Action=View&home_id='+HomeId, 'list', 400, 400, true, true, true);
	with(document.frmHome)
	{
		home_id.value	= HomeId;
		Action.value	= 'View';
		submit();
	}	
}
//====================================================================================================
//	Function Name	:	Picture_Click()
//----------------------------------------------------------------------------------------------------
function Picture_Click(HomeId)
{
	with(document.frmHome)
	{
		home_id.value	= HomeId;
		Action.value	= 'Picture';
		submit();
	}
}
//====================================================================================================
//	Function Name	:	Edit_Click()
//----------------------------------------------------------------------------------------------------
function Edit_Click(HomeId)
{
	with(document.frmHome)
	{
		home_id.value	= HomeId;
		Action.value	= 'Edit';
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function Delete_Click(HomeId)
{
	with(document.frmHome)
	{
		if(confirm('Are you sure you wants to delete home information?'))
		{
			home_id.value 	= HomeId;
			Action.value	= 'Delete';
			submit();
		}
	}
}


//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function DeleteChecked_Click()
{
	with(document.frmHome)
	{
		var flg=false;

		if(document.all['home_list[]'].length)
		{
			for(i=0; i < document.all['home_list[]'].length; i++)
			{
				if(document.all['home_list[]'][i].checked)
					flg = true;
			}
		}
		else
		{
			if(document.all['home_list[]'].checked)
				flg = true;
		}

		if(!flg)
		{
			alert('Please, select the record you wants to delete.');
			return false;
		}
			
		if(confirm('Are you sure you wants to delete selected home information?'))
		{
			Action.value 	= 'DeleteSelected';
			submit();
		}
	}
}

//====================================================================================================
//	Function Name	:	Picture_Gallery_Click()
//----------------------------------------------------------------------------------------------------
function Picture_Gallery_Click(HomeId,PicId,totalPic)
{
	/*with(document.frmHome)
	{		
		home_id.value	= HomeId;
		pic_id.value	= PicId;
		Action.value	= 'PictureGallery';
		submit();	
	}*/
	popupWindowURL('home.php?Action=PictureGallery&home_id='+HomeId+'&pic_id='+PicId+'&total_pic='+totalPic, 'list', 600, 600, false, false, true);
}

//====================================================================================================
//	Function Name	:	Next_Click()
//----------------------------------------------------------------------------------------------------
function Next_Click(HomeId,PicId,totalPic)
{
	/*with(document.frmHome)
	{
		home_id.value	= HomeId;
		pic_id.value	= PicId;
		Action.value	= 'PictureGallery';
		Action2.value	= 'Next';
		submit();		
	}*/
	popupWindowURL('home.php?Action=PictureGallery&Action2=Next&home_id='+HomeId+'&pic_id='+PicId+'&total_pic='+totalPic, 'list', 600, 600, false, false, true);
}

//====================================================================================================
//	Function Name	:	Prev_Click()
//----------------------------------------------------------------------------------------------------
function Prev_Click(HomeId,PicId,totalPic)
{
	/*with(document.frmHome)
	{
		home_id.value	= HomeId;
		pic_id.value	= PicId;
		Action.value	= 'PictureGallery';
		Action2.value	= 'Previous';		
		submit();		
	}*/
	popupWindowURL('home.php?Action=PictureGallery&Action2=Previous&home_id='+HomeId+'&pic_id='+PicId+'&total_pic='+totalPic, 'list', 600, 600, false, false, true);
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function ToggleStatus_Click(HomeId, state)
{
	with(document.frmHome)
	{
		home_id.value 		= HomeId;
		status.value 		= state;
		Action.value		= 'ChangeStatus';
		submit();
	}
}
//====================================================================================================
//	Function Name	:	ToggleFeatures_Click()
//----------------------------------------------------------------------------------------------------
function ToggleFeatures_Click(HomeId, state)
{
	with(document.frmHome)
	{
		home_id.value 		= HomeId;
		feature.value 		= state;
		Action.value		= 'ChangeFeatured';
		submit();
	}
}
