// JavaScript Document
//=======================================================================================
// Function: CAdd_Click
//---------------------------------------------------------------------------------------
function CAdd_Click(frm)
{
	with(frm)
	{
		if(!IsEmpty(passenger_end, "Please enter range end"))
		{
			return false;
		}
		else
		{
			if(!IsInt(passenger_end, "Please enter integer value for range end"))
				return false;
			
			if( +passenger_end.value <= +passenger_start.value)
			{
				alert("Range end must be greater than range start")
				passenger_end.focus();
				return false;
			}	
		}
			
		Action.value='Add';
		return true;
	}
}

//=======================================================================================
// Function: CUpdate_Click
//---------------------------------------------------------------------------------------
function CUpdate_Click(frm)
{
	with(frm)
	{
		if (document.all['passenger_starting_range[]'].length)
		{
			for(i=0; i < document.all['passenger_starting_range[]'].length; i++)
			{
				/*if(!IsEmpty(document.all['passenger_starting_range[]'][i], "Please enter range start"))
				{
					return false;
				}
				else
				{
					if(!IsInt(document.all['passenger_starting_range[]'][i], "Please enter integer value for range start"))
						return false;
				}*/
		
				if(!IsEmpty(document.all['passenger_ending_range[]'][i], "Please enter range end"))
				{
					return false;
				}
				else
				{
					if(!IsInt(document.all['passenger_ending_range[]'][i], "Please enter integer value for range end"))
						return false;

					if( +document.all['passenger_ending_range[]'][i].value <= +document.all['passenger_starting_range[]'][i].value)
					{
						alert("Range end must be greater than range start");
						document.all['passenger_ending_range[]'][i].focus();
						return false;
					}						
				}
			}
		}
		else
		{
			if(!IsEmpty(document.all['passenger_starting_range[]'], "Please enter range start"))
			{
				return false;
			}
			else
			{
				if(!IsInt(document.all['passenger_starting_range[]'], "Please enter integer value for range start"))
					return false;
			}
	
			if(!IsEmpty(document.all['passenger_ending_range[]'], "Please enter range end"))
			{
				return false;
			}
			else
			{
				if(!IsInt(document.all['passenger_ending_range[]'], "Please enter integer value for range end"))
					return false;
			}			
		}
			
		Action.value='Edit';
		return true;
	}
}

//=======================================================================================
// Function: CDelete_Click
//---------------------------------------------------------------------------------------
function CDelete_Click(frm, priceId)
{
	with(frm)
	{
		if(!confirm("Are you sure?"))
			return;

		Action.value = 'Delete';
		pk.value = priceId;
		frm.submit();
	}
}

//=======================================================================================
// Function: setStartRange
//---------------------------------------------------------------------------------------
function setStartRange(frm, num)
{
	with(frm)
	{
		//alert(num);	

		if(document.all['passenger_starting_range[]'][num])
		{
			document.all['passenger_starting_range[]'][num].value = +document.all['passenger_ending_range[]'][num-1].value + 1;
		}
	}
}