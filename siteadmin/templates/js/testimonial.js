
//====================================================================================================
//	File Name		:	edit_profile.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Client side validation in JavaScript.
//	Author			:	Jignasa Naik
//	Creation Date	:	22-Oct-2004
//	Copyright		:	Copyrights � 2004 DotInfosys
//	Email			:	jignasa@dotinfosys.com
//	History			:
//						Date				Author					Remark
//						22-Oct-2003			Jignasa Naik			Initial Release
//
//====================================================================================================

//====================================================================================================
//	Function Name	:	Validate_Subadmin_Form()
//	Purpose			:	This function will executed when admin edit profile and submits a form. It checks validity of 
//						Firstname, Lastname and Email in the form.
//	Parameters		:	frm  - form name
//	Return			:	true or false
//	Author			:	Jignasa Naik
//	Creation Date	:	22-Oct-2004
//----------------------------------------------------------------------------------------------------
function Form_Submit(frm)
{
	with(frm)
    {
		if(!IsEmpty(person_name, Empty_Name))
        {			
			return false;
        }		
    	if(IsEmpty(person_zip, ''))
		{
	    	if(!IsZip(person_zip, Valid_Zip))
    	    {
				return false;
	        }
		}
		
    	if(IsEmpty(person_phone_citycode, ''))
		{
	    	if(!IsEmpty(person_phone_areacode, Empty_Phone))
    	    {
				return false;
	        }
		}
    	if(IsEmpty(person_phone_no, ''))
		{
	    	if(!IsEmpty(person_phone_areacode, Empty_Phone))
    	    {
				return false;
	        }
		}
		
		if(IsEmpty(person_phone_areacode,''))
		{
	    	if(!IsEmpty(person_phone_citycode, Empty_Phone))
    	    {
				return false;
	        }
			
	    	if(!IsEmpty(person_phone_no, Empty_Phone))
    	    {
				return false;
	        }
			if(!IsPhone(person_phone_areacode,person_phone_citycode,person_phone_no, Valid_Phone))
			{
				return false;
			}
		}
		
		
    	if(IsEmpty(person_fax_citycode, ''))
		{
	    	if(!IsEmpty(person_fax_areacode, Empty_Fax))
    	    {
				return false;
	        }
		}
    	if(IsEmpty(person_fax_no, ''))
		{
	    	if(!IsEmpty(person_fax_areacode, Empty_Fax))
    	    {
				return false;
	        }
		}
		if(IsEmpty(person_fax_areacode,''))
		{
	    	if(!IsEmpty(person_fax_citycode, Empty_Fax))
    	    {
				return false;
	        }
	    	if(!IsEmpty(person_fax_no, Empty_Fax))
    	    {
				return false;
	        }
			if(!IsPhone(person_fax_areacode,person_fax_citycode,person_fax_no, Valid_Fax))
			{
				return false;
			}
		}

    	if(IsEmpty(person_email, ''))
		{
	    	if(!IsEmail(person_email, Valid_Email))
    	    {
				return false;
	        }
		}
		
		if(IsEmpty(person_website, ''))
        {			
			if(!IsUrl(person_website, Valid_URL))
			{			
				return false;
			}
        }
		
		if(person_pic.value != '')
		{
			if(!checkImageType(person_pic, Valid_Photo))
			{
				return false;					
			}
		}

		if(!IsEmpty(person_comment, Empty_Comment))
        {			
			return false;
        }
		return true;		
    }
}

//====================================================================================================
//	Function Name	:	Add_Click()
//	Purpose			:	This function will set the Report id and action and then submit the form.
//	Parameters		:	StoryId  - Report Id whose detail will to be Add
//	Return			:	none
//	Author			:	Dinesh Sailor
//	Creation Date	:	19-Aug-2004
//----------------------------------------------------------------------------------------------------
function Add_Click(actType)
{
	with(document.frmTestimonial)
	{
		Action.value = actType;		
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Edit_Click()
//	Purpose			:	This function will set the Member id and action and then submit the form.
//	Parameters		:	StoryId  - Report Id whose detail will to be edit
//	Return			:	none
//	Author			:	Dinesh Sailor
//	Creation Date	:	19-Aug-2004
//----------------------------------------------------------------------------------------------------
function Edit_Click(actType, personId)
{
	with(document.frmTestimonial)
	{
		person_id.value = personId;
		Action.value 	= actType;		
		submit();
	}
}

function View_Click(actType, personId)
{
	with(document.frmTestimonial)
	{
		person_id.value = personId;
		Action.value 	= actType;		
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//	Purpose			:	This function will set the Member id and action and then submit the form.
//	Parameters		:	StoryId  - Report Id whose detail will to be delete
//	Return			:	none
//	Author			:	Dinesh Sailor
//	Creation Date	:	19-Aug-2004
//----------------------------------------------------------------------------------------------------
function Delete_Click(actType, personId)
{
	with(document.frmTestimonial)
	{
		person_id.value = personId;
		Action.value 	= actType;
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//	Purpose			:	This function will set the Member id and action and then submit the form.
//	Parameters		:	StoryId  - Report Id whose detail will to be delete
//	Return			:	none
//	Author			:	Dinesh Sailor
//	Creation Date	:	19-Aug-2004
//----------------------------------------------------------------------------------------------------
function DeleteChecked_Click(actType)
{
	with(document.frmTestimonial)
	{
		var flg=false;

		if(document.all['person_list[]'].length)
		{
			for(i=0; i < document.all['person_list[]'].length; i++)
			{
				if(document.all['person_list[]'][i].checked)
					flg = true;
			}
		}
		else
		{
			if(document.all['person_list[]'].checked)
				flg = true;
		}
		if(!flg)
		{
			alert(Delete_Testimonial);
			return false;
		}

		if(confirm(Delete_AllTestimonials))
		{
			Action.value 	= actType;
			submit();
		}
	}
}

//====================================================================================================
//	Function Name	:	Cancel_Click()
//	Purpose			:	redirect to cancelActionLink
//	Parameters		:	cancelActionLink
//	Return			:	redirect to cancelActionLink
//	Author			:	
//	Creation Date	:	27-Oct-2004
//----------------------------------------------------------------------------------------------------
function Cancel_Click(cancelActionLink)
{
	document.location.href=cancelActionLink;
}