// JavaScript Document
//=======================================================================================
// Function: Show_Click
//---------------------------------------------------------------------------------------
function Show_Click(userId)
{
	with(document.frmUser)
	{
		user_id.value  = userId;
		Action.value = "Show";
		submit();
	}
}

//=======================================================================================
// Function: Delete_Click
//---------------------------------------------------------------------------------------
function Delete_Click(userId,ConfirmationNo)
{
	with(document.frmUser)
	{
		if(confirm(Confirm_Delete))
		{
			user_id.value = userId;
			confirmationNo.value  = ConfirmationNo;
			Action.value = "Delete";
			submit();
		}
	}
}

//=======================================================================================
// Function: Reserve_Click
//---------------------------------------------------------------------------------------
function Reserve_Click(ConfirmationNo)
{
  with(document.frmUser)
	{
		confirmationNo.value  = ConfirmationNo;
		Action.value = "Show_All";
		action = "reservationdetail.php";
		submit();
	}
}

