//====================================================================================================
//	Function Name	:	CheckedSelectedItem()
//----------------------------------------------------------------------------------------------------
function CheckedSelectedItem(fld, cntId)
{
	var cntFld = window.opener.document.getElementById(cntId);
	
	if(!cntFld) return;
	
	if(fld.length)
	{
		for(i=0; i<fld.length; i++)
		{
			if(cntFld.value.search(fld[i].value) != -1 && fld[i].value != '')
				fld[i].checked = true;
		}
	}
	else
	{
		if(cntFld.value.search(fld.value) != -1 && fld.value != '')
			fld.checked = true;
	}
}

//====================================================================================================
//	Function Name	:	Item_Select()
//----------------------------------------------------------------------------------------------------
function Item_Click(fld, cntId)
{
	var cntFld = window.opener.document.getElementById(cntId);
	if(!cntFld) return;

	if(fld.checked)
	{
		if(fld.value != '')
		{
			cntFld.value += fld.value + ',';
		}
	}
	else
	{
		cntFld.value = cntFld.value.replace(' ', '');
		cntFld.value = cntFld.value.replace(fld.value + ',', '');
	}
}

//====================================================================================================
//	Function Name	:	Item_CheckUncheck()
//----------------------------------------------------------------------------------------------------
function Item_CheckUncheck(fld, status, cntId)
{
	if(fld)
	{
		for(i=0; i < fld.length; i++)
		{
			fld[i].checked = status;
			Item_Click(fld[i], cntId);
		}
	}
}