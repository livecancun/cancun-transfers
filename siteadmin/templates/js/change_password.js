//====================================================================================================
//	File Name		:	cms.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Client side validation in JavaScript.
//	Author			:	Dinesh Sailor
//	Creation Date	:	26-Oct-2005
//	Copyright		:	Copyrights � 2003 Dot Infosys
//	Email			:	dinesh@dotinfosys.com
//	History			:
//						Date				Author					Remark
//						26-Oct-2005			Dinesh Sailor			Initial Release
//
//====================================================================================================

//====================================================================================================
//	Function Name	:	Form_Submit()
//----------------------------------------------------------------------------------------------------
function Form_Submit(frm)
{
	with(frm)
    {
    	if(!IsEmpty(old_password, 'Enter your old password.'))
        {
			return false;
        }
    	if(!IsEmpty(new_password, 'Enter your new password.'))
        {
			return false;
        }
		else if(new_password.value != retype_password.value)
		{
			alert('Password confirmation does not match!!');	
			new_password.focus();
			return false;
		}
        return true;
    }
}
