//====================================================================================================
//	File Name		:	emailconfig.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Client side validation in JavaScript.
//	Author			:	Dinesh Sailor
//	Creation Date	:	26-Oct-2005
//	Copyright		:	Copyrights � 2003 Dot Infosys
//	Email			:	dinesh@dotinfosys.com
//	History			:
//						Date				Author					Remark
//						26-Oct-2005			Dinesh Sailor			Initial Release
//
//====================================================================================================

function Form_Submit(frm)
{
	with(frm)
    {
			if(!IsEmpty(email_fromname, 'Please enter email from name.'))
			{
				return false;
			}
			if(!IsEmpty(support_email, 'Please, enter your email address.'))
			{
				return false;    
			}
			if(!IsEmail(support_email, 'Please, enter valid email address.'))
			{
				return false;
			}
			
        return true;
    }
}

