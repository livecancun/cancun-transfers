//====================================================================================================
//	Function Name	:	Validate_Form()
//----------------------------------------------------------------------------------------------------
function Validate_Form(frm)
{
	with(frm)
    {
		for(i=0; i < document.all['page_title[]'].length; i++)
		{
			if(!IsEmpty(document.all['page_title[]'][i], 'Please, enter page name.'))
			{
				return false;
			}
		}
			return true;
	}
}
//====================================================================================================
//	Function Name	:	View_Click()
//----------------------------------------------------------------------------------------------------
function View_Click(page_id)
{
	with(document.frmPage)
	{
		popupWindowURL('page.php?Action=View&pid='+page_id, 'list', 600, 550, false, true, true);
	}
}

function PageType_Change()
{
	with(document.frmPage)
	{
		if(page_type.value == 'SimplePage' || page_type.value == 'Popup')
		{
			for (var i=1; i<=lang_cnt.value; i++)
			{
				// Show 
				document.getElementById('pageTitle_' +i).style.visibility = 'visible';
				document.getElementById('pageTitle_' +i).style.display = 'block';
				
				document.getElementById('pageMeta_' +i).style.visibility = 'visible';
				document.getElementById('pageMeta_' +i).style.display = 'block';
				document.getElementById('pageContent_' +i).style.visibility = 'visible';
				document.getElementById('pageContent_' +i).style.display = 'block';
			}
				
			// Hide 
			document.getElementById('pageUrl').style.visibility = 'hidden';
			document.getElementById('pageUrl').style.display = 'none';
		}
		else if(page_type.value == 'iFrame' || page_type.value == 'InternalLink')
		{
			document.getElementById('pageUrl').style.visibility = 'visible';
			document.getElementById('pageUrl').style.display = 'block';
			
			for (var i=1; i<=lang_cnt.value; i++)
			{
				// Show
				document.getElementById('pageContent_' +i).style.visibility = 'visible';
				document.getElementById('pageContent_' +i).style.display = 'block';
	
				// Hide
				document.getElementById('pageMeta_' +i).style.visibility = 'hidden';
				document.getElementById('pageMeta_' +i).style.display = 'none';				
				document.getElementById('pageContent_' +i).style.visibility = 'hidden';
				document.getElementById('pageContent_' +i).style.display = 'none';
			}
			
			//
			if (page_type.value == 'InternalLink')
			{
				document.getElementById('eglink').style.display = 'none';
				document.getElementById('eginternal').style.display = 'block';
				for (var i=1; i<=lang_cnt.value; i++)
				{
					// Show
					document.getElementById('pageTitle_' +i).style.visibility = 'visible';
					document.getElementById('pageTitle_' +i).style.display = 'block';
				}
				
			}
			else
			{
				document.getElementById('eglink').style.display = 'block';
				document.getElementById('eginternal').style.display = 'none';				
				for (var i=1; i<=lang_cnt.value; i++)
				{
					// Hide
					document.getElementById('pageTitle_' +i).style.visibility = 'hidden';
					document.getElementById('pageTitle_' +i).style.display = 'none';
				}				
			}
		}
		else if(page_type.value == 'OutsideLink')
		{
			// Show
			document.getElementById('pageUrl').style.visibility = 'visible';
			document.getElementById('pageUrl').style.display = 'block';
			
			
			// Hide
			for (var i=1; i<=lang_cnt.value; i++)
			{
				document.getElementById('pageTitle_' +i).style.visibility = 'hidden';
				document.getElementById('pageTitle_' +i).style.display = 'none';

				document.getElementById('pageMeta_' +i).style.visibility = 'hidden';
				document.getElementById('pageMeta_' +i).style.display = 'none';
				document.getElementById('pageContent_' +i).style.visibility = 'hidden';
				document.getElementById('pageContent_' +i).style.display = 'none';
			}
			
			//
			document.getElementById('eglink').style.display = 'block';
			document.getElementById('eginternal').style.display = 'none';							
		}
	}
}

//====================================================================================================
//	Function Name	:	Edit_Click()
//----------------------------------------------------------------------------------------------------
function Edit_Click(pageId)
{
	with(document.frmPage)
	{
		pid.value		= pageId;
		Action.value	= 'Edit';
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function Delete_Click(pageId)
{
	with(document.frmPage)
	{
		if(confirm('Are you sure you wants to delete page?'))
		{
			pid.value 		= pageId;
			Action.value	= 'Delete';
			submit();
		}
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function ChangeOrder_Click(direction)
{
	var list = document.frmPage.page_list;
	var tmpVal;
	var tmpText;

	if(list.selectedIndex==-1)
	{
		alert("Select the page you want to move.");
		list.focus();
		return;
	}

	if(!list.length) return;
	
	if(direction==1)
	{
		if(list.selectedIndex == list.length-1) return;
	}
	else
		if(list.selectedIndex == 0) return;

	tmpVal 	= list.options[list.selectedIndex+direction].value;
	tmpText	= list.options[list.selectedIndex+direction].text;

	list.options[list.selectedIndex+direction].value 	= list.options[list.selectedIndex].value;
	list.options[list.selectedIndex+direction].text 	= list.options[list.selectedIndex].text;

	list.options[list.selectedIndex].value 	= tmpVal;
	list.options[list.selectedIndex].text 	= tmpText;

	list.selectedIndex += direction;
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function SortPage_Click()
{
	with(document.frmPage)
	{
		for(i=0; i<page_list.length; i++)
			page_order.value += page_list[i].value + ':';
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function ToggleStatus_Click(pageId, state)
{
	with(document.frmPage)
	{
		pid.value 		= pageId;
		status.value 	= state;
		Action.value	= 'ChangeStatus';
		submit();
	}
}
