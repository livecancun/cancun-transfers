// JavaScript Document
//=======================================================================================
// Function: Frame_Click
//---------------------------------------------------------------------------------------
function Frame_Click(File)
{
	with(frmReservation)
    {
		   	Action.value = "ShowFrame";
			file_name.value = File;
			submit();
	}
}

function View_Cart()
{
	with(document.frmCart)
	{
		Action.value = "Show_Cart";
		submit();
	}
}

function View_Request()
{
	with(document.frmCart)
	{
		Action.value = "Show_Request";
		submit();
	}
}

function Edit_Click(CartId)
{
	with(document.frmReservation)
	{
		cartId.value = CartId;
		Action.value = "Modify";

		action = "transfercartedit.php";

		submit();
	}
}

function Show_Click(CartId)
{
	with(document.frmReservation)
	{
	   
		cartId.value = CartId;
		Action.value = "Show";
		action = "transfercart.php";
		
		submit();
	}
}

function Delete_Click(CartId)
{
	with(document.frmReservation)
	{
		cartId.value = CartId;
		Action.value = "Delete";
		submit();
	}
}