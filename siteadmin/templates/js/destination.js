//====================================================================================================
//	Function Name	:	Validate_Form()
//----------------------------------------------------------------------------------------------------
function Validate_Form(frm)
{
	with(frm)
    {
		for(i=0; i < document.all['dest_title[]'].length; i++)
		{
			if(!IsEmpty(document.all['dest_title[]'][i], 'Please, enter Destination name.'))
			{
				return false;
			}
		}
			return true;
	}
}
//====================================================================================================
//	Function Name	:	View_Click()
//----------------------------------------------------------------------------------------------------
function View_Click(page_id)
{
	with(document.frmDestination)
	{
		popupWindowURL('page.php?Action=View&dest_id='+page_id, 'list', 600, 550, false, true, true);
	}
}

//====================================================================================================
//	Function Name	:	Edit_Click()
//----------------------------------------------------------------------------------------------------
function Edit_Click(destId)
{
	with(document.frmDestination)
	{
		dest_id.value		= destId;
		Action.value	= 'Edit';
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function Delete_Click(destId)
{
	with(document.frmDestination)
	{
		if(confirm('Are you sure you wants to delete Destination?'))
		{
			dest_id.value 		= destId;
			Action.value	= 'Delete';
			submit();
		}
	}
}
//====================================================================================================
//	Function Name	:	DeleteChecked_Click()
//----------------------------------------------------------------------------------------------------
function DeleteChecked_Click()
{
	with(document.frmDestination)
	{
		var flg=false;

		if(document.all['dest_id1[]'].length)
		{
			for(i=0; i < document.all['dest_id1[]'].length; i++)
			{
				if(document.all['dest_id1[]'][i].checked)
					flg = true;
			}
		}
		else
		{
			if(document.all['dest_id1[]'].checked)
				flg = true;
		}

		if(!flg)
		{
			alert('Please select Destination(s) you want to delete.');
			return false;
		}
			
		if(confirm('Are you sure you want to delete selected Destination(s)?'))
		{
			Action.value 	= 'DeleteSelected';
			submit();
		}
	}
}

//====================================================================================================
//	Function Name	:	ToggleStatus_Click()
//----------------------------------------------------------------------------------------------------
function ToggleStatus_Click(destId, state)
{
	with(document.frmDestination)
	{
		dest_id.value 	= destId;
		status.value 	= state;
		Action.value	= 'ChangeStatus';
		submit();
	}
}

/*
//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function ChangeOrder_Click(direction)
{
	var list = document.frmDestination.page_list;
	var tmpVal;
	var tmpText;

	if(list.selectedIndex==-1)
	{
		alert("Select the page you want to move.");
		list.focus();
		return;
	}

	if(!list.length) return;
	
	if(direction==1)
	{
		if(list.selectedIndex == list.length-1) return;
	}
	else
		if(list.selectedIndex == 0) return;

	tmpVal 	= list.options[list.selectedIndex+direction].value;
	tmpText	= list.options[list.selectedIndex+direction].text;

	list.options[list.selectedIndex+direction].value 	= list.options[list.selectedIndex].value;
	list.options[list.selectedIndex+direction].text 	= list.options[list.selectedIndex].text;

	list.options[list.selectedIndex].value 	= tmpVal;
	list.options[list.selectedIndex].text 	= tmpText;

	list.selectedIndex += direction;
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function SortPage_Click()
{
	with(document.frmDestination)
	{
		for(i=0; i<page_list.length; i++)
			page_order.value += page_list[i].value + ':';
		submit();
	}
}
*/

