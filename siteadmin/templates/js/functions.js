
//====================================================================================================
//	File Name		:	functions.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Javascript Utility functions
//	Author			:	Nirmal Patel
//	Creation Date	:	05-May-2003
//	Copyright		:	Copyrights � 2003 Dot Infosys
//	Email			:	dinesh@dotinfosys.com
//	History			:
//						Date						Author						Remark
//						05-May-2003					Nirmal Patel				Initial Release
//
//====================================================================================================

//====================================================================================================
//	Function Name	:	popupWindowURL
//	Purpose			:	Whenever you wanna open a link into a new window just call this function
//								you need to pass some arguemnts as described below.
//	Parameters		:
//								url  = url to be open in the new window
//								winname = winname is the window name for the reference of that window
//								w is the width
//								h is the height
//								menu is the parameter, if you want menubar to be enabled on the window
//								resize if you wanna resize the window
//								scroll i fyou needed
//	Return			:	true or false
//	Author			:	Nirmal Patel
//	Creation Date	:	05-May-2003
//----------------------------------------------------------------------------------------------------

function popupWindowURL(url, winname,  w, h, menu, resize, scroll) {

    var x = (screen.width-w)/2;
    var y = (screen.height-h)/3;

	if (winname == null) winname = "newWindow";
	if (w == null) w = 800;
	if (h == null) h = 600;
	if (resize == null) resize = 1;

	menutype   = "nomenubar";
	resizetype = "noresizable";
	scrolltype = "noscrollbars";
	if (menu) menutype = "menubar";
	if (resize) resizetype = "resizable";
	if (scroll) scrolltype = "scrollbars";
	//alert(url+","+x+","+winname);
    cwin=window.open(url,winname,"top=" + y + ",left=" + x + ",screenX=" + x + ",screenY=" + y + "," + "status," + menutype + "," + scrolltype + "," + resizetype + ",width=" + w + ",height=" + h);

	if (!cwin.opener) cwin.opener=self;
	cwin.focus();

	return true;
}

function CheckUncheck_Click(fld, status)
{
	if(fld)
	{
		if(fld.length)
			for(i=0; i < fld.length; i++)
				fld[i].checked = status;
		else
			fld.checked = status;
	}
}

function Menu_ShowHide(menu, img, imgUp, imgDown)
{
	if(menu)
	{
		if(menu.style.display == 'none')
		{
			menu.style.visibility	= 'visible';
			menu.style.display		= 'block';
			img.src = imgUp;
			SetCookie(menu.id, 'open');
		}
		else
		{
			menu.style.visibility 	= 'hidden';
			menu.style.display 		= 'none';
			img.src = imgDown;
			SetCookie(menu.id, 'close');
		}
	}
}

function UploadImage_Change(obj, imgTag, defaultVal, defaultWidth)
{
	imgTag.width=120;

	if(obj.value == '')
		imgTag.src = defaultVal;
	else
	{
		imgTag.src = obj.value;
		if(defaultWidth != '')
			imgTag.width=defaultWidth;
	}
}

function SetTime()
{
	if(!document.getElementById('timeId'))	return;

	var Hours;
	var Mins;
	var Time;

	Stamp = new Date();

	Hours = Stamp.getHours();
	
	if (Hours >= 12)
		Time = " PM";
	else
		Time = " AM";
	
	if (Hours > 12)
		Hours -= 12;
	
	if (Hours == 0)
		Hours = 12;
	
	Mins = Stamp.getMinutes();

	if (Mins < 10)
		Mins = "0" + Mins;

	Sec = Stamp.getSeconds();
	if (Sec < 10)
		Sec = "0" + Sec;

	document.getElementById('timeId').innerHTML = ("&nbsp;" + Hours + ":" + Mins + ":" + Sec + Time);
}

setInterval('SetTime()',1000);

function getDate(parmDate)
{
	var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September","October", "November", "December");

	var curr_date = parmDate.getDate();

	var sup = "";

	if (curr_date == 1 || curr_date == 21 || curr_date ==31)
	{
	   sup = "st";
	}
	else if (curr_date == 2 || curr_date == 22)
	{
	   sup = "nd";
	}
	else if (curr_date == 3 || curr_date == 23)
	{
	   sup = "rd";
	}
	else
	{
	   sup = "th";
	}

	var curr_month 	= parmDate.getMonth();
	var curr_year 	= parmDate.getFullYear();

	return (curr_date + "<SUP>" + sup + "</SUP> " + m_names[curr_month] + " " + curr_year);
}

function GetCookie (name) 
{
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	var i = 0;
	while (i < clen) 
	{
		var j = i + alen;
		if (document.cookie.substring(i, j) == arg)
			return getCookieVal (j);

		i = document.cookie.indexOf(" ", i) + 1;
		if (i == 0) break; 
	}
	return null;
}

function SetCookie (name, value) 
{
	var argv = SetCookie.arguments;
	var argc = SetCookie.arguments.length;
	
	var expires = (argc > 2) ? argv[2] : null;
	var path = (argc > 3) ? argv[3] : null;
	var domain = (argc > 4) ? argv[4] : null;
	var secure = (argc > 5) ? argv[5] : false;
	
	document.cookie = name + "=" + escape (value) +
	((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
	((path == null) ? "" : ("; path=" + path)) +
	((domain == null) ? "" : ("; domain=" + domain)) +
	((secure == true) ? "; secure" : "");
}
