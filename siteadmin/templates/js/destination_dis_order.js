//====================================================================================================
//	File Name		:	sport_display_order.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Client side validation in JavaScript.
//	Author			:	Nilesh Patel
//	Creation Date	:	18-Oct-2004
//	Copyright		:	Copyrights � 2003 Dot Infosys
//	Email			:	info@dotinfosys.com
//	History			:
//						Date				Author					Remark
//						18-Oct-2004			Nilesh Patel			Initial Release
//
//====================================================================================================

//====================================================================================================
//	Function Name	:	Form_Submit()
//	Purpose			:	This function will executed when user submits a form. It checks validity of
//						every field in the form.
//
//	Parameters		:	frm  - form name
//	Return			:	true or false
//	Author			:	Nilesh Patel
//	Creation Date	:	18-Oct-2004
//----------------------------------------------------------------------------------------------------
//====================================================================================================
//	Function Name	:	MoveChecked_Click()
//----------------------------------------------------------------------------------------------------
function MoveChecked_Click()
{
	with(document.frmDestination)
	{
	
		var flg=false;
		if(document.all['Category_List[]'].length)
		{
			for(i=0; i < document.all['Category_List[]'].length; i++)
			{
				if(document.all['Category_List[]'][i].checked)
				
				flg = true;
			}
		}
		else
		{
			if(document.all['Category_List[]'].checked)
				flg = true;
		}

		if(!flg)
		{
			alert('Please, select the destination you want to move.');
			return false;
		}
			
			Action.value 	= 'Move';
			submit();
	}
}

//====================================================================================================
//	Function Name	:	Button_Click()
//----------------------------------------------------------------------------------------------------
function Button_Click(btnName)
{
	with(document.frmDestination)
	{
		SubAction.value = btnName.value;
		if(btnName.type == 'button')
		{
			if(Validate_Form(document.frmDestination))
				submit();
		}
	}
}
//====================================================================================================
//	Function Name	:	Sort_Form_Submit()
//----------------------------------------------------------------------------------------------------
function Sort_Form_Submit(frm,ActionType)
{ 
	var i;
	with(frm)
    {
		Action.value=ActionType;
		
		for(i=0; i<sort_data.length; i++)
		{
			sorted_list.value += sort_data[i].value + ':';
		}
		return true;
	}
}
function SortUpDown_Click(direction)
{
	var list = document.frmDestination.sort_data;

	var tmpVal;
	var tmpText;

	if(list.selectedIndex==-1)
	{
		alert("Select the destination you want to move.");
		return;
	}

	if(!list.length) return;
	
	if(direction==1)
	{
		if(list.selectedIndex == list.length-1) return;
	}
	else
		if(list.selectedIndex == 0) return;

	tmpVal 	= list.options[list.selectedIndex+direction].value;
	tmpText	= list.options[list.selectedIndex+direction].text;

	list.options[list.selectedIndex+direction].value 	= list.options[list.selectedIndex].value;
	list.options[list.selectedIndex+direction].text 	= list.options[list.selectedIndex].text;

	list.options[list.selectedIndex].value 	= tmpVal;
	list.options[list.selectedIndex].text 	= tmpText;

	list.selectedIndex += direction;
}
//====================================================================================================
//	Function Name	:	Cancel_Click()
//	Purpose			:	redirect to cancelActionLink
//	Parameters		:	cancelActionLink
//	Return			:	redirect to cancelActionLink
//	Author			:	
//	Creation Date	:	27-Oct-2004
//----------------------------------------------------------------------------------------------------
function Cancel_Click(cancelActionLink)
{
	document.location.href=cancelActionLink;
}