//====================================================================================================
//	Function Name	:	Validate_Form()
//----------------------------------------------------------------------------------------------------
function Validate_Form(frm)
{
	with(frm)
    {
		for(i=0; i < document.all['triptype_title[]'].length; i++)
		{
			if(!IsEmpty(document.all['triptype_title[]'][i], 'Please, enter Trip Type.'))
			{
				return false;
			}
		}
			return true;
	}
}
//====================================================================================================
//	Function Name	:	View_Click()
//----------------------------------------------------------------------------------------------------
function View_Click(page_id)
{
	with(document.frmTripType)
	{
		popupWindowURL('page.php?Action=View&triptype_id='+page_id, 'list', 600, 550, false, true, true);
	}
}

//====================================================================================================
//	Function Name	:	Edit_Click()
//----------------------------------------------------------------------------------------------------
function Edit_Click(TripTypeId)
{
	with(document.frmTripType)
	{
		triptype_id.value		= TripTypeId;
		Action.value	= 'Edit';
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function Delete_Click(TripTypeId)
{
	with(document.frmTripType)
	{
		if(confirm('Are you sure you wants to delete TripType?'))
		{
			triptype_id.value 		= TripTypeId;
			Action.value	= 'Delete';
			submit();
		}
	}
}
//====================================================================================================
//	Function Name	:	DeleteChecked_Click()
//----------------------------------------------------------------------------------------------------
function DeleteChecked_Click()
{
	with(document.frmTripType)
	{
		var flg=false;

		if(document.all['triptype_id1[]'].length)
		{
			for(i=0; i < document.all['triptype_id1[]'].length; i++)
			{
				if(document.all['triptype_id1[]'][i].checked)
					flg = true;
			}
		}
		else
		{
			if(document.all['triptype_id1[]'].checked)
				flg = true;
		}

		if(!flg)
		{
			alert('Please select Trip Type(s) you want to delete.');
			return false;
		}
			
		if(confirm('Are you sure you want to delete selected Trip Type(s)?'))
		{
			Action.value 	= 'DeleteSelected';
			submit();
		}
	}
}

//====================================================================================================
//	Function Name	:	ToggleStatus_Click()
//----------------------------------------------------------------------------------------------------
function ToggleStatus_Click(TripTypeId, state)
{
	with(document.frmTripType)
	{
		triptype_id.value 	= TripTypeId;
		status.value 	= state;
		Action.value	= 'ChangeStatus';
		submit();
	}
}

/*
//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function ChangeOrder_Click(direction)
{
	var list = document.frmTripType.page_list;
	var tmpVal;
	var tmpText;

	if(list.selectedIndex==-1)
	{
		alert("Select the page you want to move.");
		list.focus();
		return;
	}

	if(!list.length) return;
	
	if(direction==1)
	{
		if(list.selectedIndex == list.length-1) return;
	}
	else
		if(list.selectedIndex == 0) return;

	tmpVal 	= list.options[list.selectedIndex+direction].value;
	tmpText	= list.options[list.selectedIndex+direction].text;

	list.options[list.selectedIndex+direction].value 	= list.options[list.selectedIndex].value;
	list.options[list.selectedIndex+direction].text 	= list.options[list.selectedIndex].text;

	list.options[list.selectedIndex].value 	= tmpVal;
	list.options[list.selectedIndex].text 	= tmpText;

	list.selectedIndex += direction;
}

//====================================================================================================
//	Function Name	:	Delete_Click()
//----------------------------------------------------------------------------------------------------
function SortPage_Click()
{
	with(document.frmTripType)
	{
		for(i=0; i<page_list.length; i++)
			page_order.value += page_list[i].value + ':';
		submit();
	}
}
*/

