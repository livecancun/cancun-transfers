//====================================================================================================
//	File Name		:	discount.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Client side validation in JavaScript.
//	Author			:	Urvashi Solanki
//	Creation Date	:	01-Feb-2006
//	Copyright		:	Copyrights � 2006 Dot Infosys
//	Email			:	dinesh@dotinfosys.com
//	History			:
//						Date				Author					Remark
//						01-Feb-2006			Urvashi Solanki			Initial Release
//
//====================================================================================================

//====================================================================================================
//	Function Name	:	Form_Submit()
//----------------------------------------------------------------------------------------------------
function Form_Submit(frm)
{
	with(frm)
	{
		if(!IsEmpty(discount_for, Empty_Discount_For))
		{
				return false;
		}

		if(!IsEmpty(discount_for_sp, Empty_Discount_For))
		{
				return false;
		}

		if(!IsEmpty(discount_rate,Empty_Discount_Rate))
		{
			return false;
		}
		else if(!allDigits(discount_rate,Valid_Discount_Rate))
		{
			return false;
		}
		
		return true;
	}
}

function Add_Click()
{
	with(document.frmTravelDiscount)
	{
		Action.value = "Add";
		submit();
	}
}

function Edit_Click(Discount_Id)
{
	with(document.frmTravelDiscount)
	{
		discount_id.value = Discount_Id;
		Action.value = "Modify";
		submit();
	}
}

function Delete_Click(Discount_Id)
{
	with(document.frmTravelDiscount)
	{
		if(confirm(Confirm_Delete_Msg))
		{	
			discount_id.value = Discount_Id;
			Action.value = "Delete";
			submit();
		}
	}
}

