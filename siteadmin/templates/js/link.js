//====================================================================================================
//	File Name		:	link.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Client side validation in JavaScript.
//	Author			:	Urvashi Solanki
//	Creation Date	:	01-Feb-2006
//	Copyright		:	Copyrights � 2006 Dot Infosys
//	Email			:	dinesh@dotinfosys.com
//	History			:
//						Date				Author					Remark
//						01-Feb-2006			Urvashi Solanki			Initial Release
//
//====================================================================================================

//====================================================================================================
//	Function Name	:	Form_Submit(frm)
//----------------------------------------------------------------------------------------------------
function Form_Submit(frm)
{
	with(frm)
    {
		if(!IsEmpty(cat_name, msg_CatName))
		{
			return false;
		}
        return true;
    }
}

//====================================================================================================
//	Function Name	:	Add_Click()
//----------------------------------------------------------------------------------------------------
	function Add_Click()
	{
		with(document.frmCategory)
		{
			Action.value = "Add";
			submit();
		}
	}

//====================================================================================================
//	Function Name	:	Edit_Click(Cat_Id)
//----------------------------------------------------------------------------------------------------
	function Edit_Click(Cat_Id)
	{
		with(document.frmCategory)
		{
			Action.value = "Modify";
			cat_id.value = Cat_Id;
			submit();
		}
	}

//====================================================================================================
//	Function Name	:	Delete_Click(Cat_Id)
//----------------------------------------------------------------------------------------------------
	function Delete_Click(Cat_Id)
	{
		with(document.frmCategory)
		{
			if(confirm(msg_CatDel))
			{
				Action.value = "Delete";
				cat_id.value = Cat_Id;
				submit();
			}
		}
	}

//====================================================================================================
//	Function Name	:	Add_Link_Click(Cat_Id)
//----------------------------------------------------------------------------------------------------
	function Add_Link_Click(Cat_Id)
	{
		with(document.frmCategory)
		{
			Action.value = "AddLink";
			cat_id.value = Cat_Id;
			submit();
		}
	}	

//====================================================================================================
//	Function Name	:	Edit_Link_Click(Link_Id)
//----------------------------------------------------------------------------------------------------
	function Edit_Link_Click(Link_Id)
	{
		with(document.frmCategory)
		{
			Action.value = "ModifyLink";
			link_id.value = Link_Id;
			submit();
		}
	}

//====================================================================================================
//	Function Name	:	Delete_Link_Click(Link_Id)
//----------------------------------------------------------------------------------------------------
	function Delete_Link_Click(Link_Id)
	{
		with(document.frmCategory)
		{
			if(confirm(msg_CatDel))
			{
				Action.value = "DeleteLink";
				link_id.value = Link_Id;
				submit();
			}
		}
	}
	
	
//====================================================================================================
//	Function Name	:	Form_LinkSubmit(frm)
//----------------------------------------------------------------------------------------------------
	function Form_LinkSubmit(frm)
	{
			with(frm)
			{
				if(!IsEmpty(url_name, msg_Name))
				{
					return false;
				}
				if(!IsEmpty(url_address, msg_Address))
				{
					return false;
				}
				if(!IsEmpty(url_city, msg_City))
				{
					return false;
				}
				if(!IsEmpty(url_state, msg_State))
				{
					return false;
				}
				if(!IsZip(url_zip, msg_Zip))
				{
					return false;
				}
				if(!IsEmpty(url_country, msg_Country))
				{
					return false;
				}
				if(!IsEmpty(url_telno_areacode, msg_Telephone))
				{
						return false;
				}
	
				if(!IsEmpty(url_telno_citycode, msg_Telephone))
				{
						return false;
				}
				if(!IsEmpty(url_telno_no, msg_Telephone))
				{
						return false;
				}
				if(!IsEmpty(url_title, msg_Title))
				{
					return false;
				}
				if(!IsEmpty(url_desc, msg_Description))
				{
					return false;
				}
				if(!IsUrl(url_url, msg_Url))
				{
					return false;
				}
				if(!IsUrl(url_oururl, msg_Our_Url))
				{
					return false;
				}
				if(!IsEmpty(url_email, msg_Email))
				{
					return false;
				}
				if(!IsEmail(url_email, msg_Email))
				{
					return false;
				}
	
				return true;
			}
	}
	