//====================================================================================================
//	Function Name	:	Form_Submit1()
//----------------------------------------------------------------------------------------------------
function Form_Submit1(frm)
{
	with(frm)
    {
		if(!IsEmpty(restName, '{Empty_Restaurant_Name}'))
        {
			return false;
        }
		if(!IsEmpty(restLocation, '{Empty_Restaurant_Location}'))
        {
			return false;
        }
		if(!IsEmpty(restAddress, '{Empty_Restaurant_Address}'))
        {
			return false;
        }
		if(!IsPhone(rest_phone_areacode,rest_phone_citycode,rest_phone_no, '{Valid_Restaurant_PhoneNo}'))
        {
			return false;
        }
		if(!IsEmpty(rest_phone_areacode, '{Empty_Area_Code}'))
        {
			return false;
        }
		if(!IsEmpty(rest_phone_citycode, '{Empty_City_Code}'))
        {
			return false;
        }
		if(!IsEmpty(rest_phone_no, '{Empty_Phone_No}'))
        {
			return false;
        }
		if(!IsEmpty(restRating, '{Empty_Restaurant_Rating}'))
        {
			return false;
        }
		if(isNaN(restRating.value))
		{
		    alert("{Valid_Rest_Rating_Digit}");
			frm.restRating.focus();
			return false;
		}
		if(restRating.value > 5 || restRating.value < 1)
		{
		    alert("{Valid_Rest_Rating}");
			frm.restRating.focus();
			return false;
		}
		if(!IsEmpty(restImage, '{Empty_File_Msg}'))
        {
			return false;
        }
		if(!checkImageType(restImage, '{Valid_File_Msg}'))
		{
			return false;
        }
	 	if(!IsEmpty(restDesc, '{Empty_Restaurant_TitleDesc}'))
        {
			return false;
        }
		if(!IsEmpty(restMainDesc, '{Empty_Restaurant_MainDesc}'))
        {
			return false;
        }
		if(!IsEmpty(restCuisineName, '{Empty_Cuisine_Name}'))
        {
			return false;
        }
		if(!IsEmpty(restCuisineImage, '{Empty_File_Msg}'))
        {
			return false;
        }
		if(!checkImageType(restCuisineImage, '{Valid_File_Msg}'))
		{
			return false;
		}
		if(!IsEmpty(restCuisineDesc, '{Empty_Cuisine_Desc}'))
        {
			return false;
        }


        return true;
    }
}

//====================================================================================================
//	Function Name	:	Form_Submit2()
//----------------------------------------------------------------------------------------------------

function Form_Submit2(frm)
{
	with(frm)
    {
		if(!IsEmpty(restName, '{Empty_Restaurant_Name}'))
        {
			return false;
        }
		if(!IsEmpty(restLocation, '{Empty_Restaurant_Location}'))
        {
			return false;
        }
		if(!IsEmpty(restAddress, '{Empty_Restaurant_Address}'))
        {
			return false;
        }
		if(!IsPhone(rest_phone_areacode,rest_phone_citycode,rest_phone_no, '{Valid_Restaurant_PhoneNo}'))
        {
			return false;
        }
		if(!IsEmpty(rest_phone_areacode, '{Empty_Area_Code}'))
        {
			return false;
        }
		if(!IsEmpty(rest_phone_citycode, '{Empty_City_Code}'))
        {
			return false;
        }
		if(!IsEmpty(rest_phone_no, '{Empty_Phone_No}'))
        {
			return false;
        }
		if(!IsEmpty(restRating, '{Empty_Restaurant_Rating}'))
        {
			return false;
        }
		if(isNaN(restRating.value))
		{
		    alert("{Valid_Rest_Rating_Digit}");
			frm.restRating.focus();
			return false;
		}
		if(restRating.value > 5 || restRating.value < 1)
		{
		    alert("{Valid_Rest_Rating}");
			frm.restRating.focus();
			return false;
		}
		if(frm.restImage.value != '')
        {
			if(!checkImageType(restImage, '{Valid_File_Msg}'))
			 {
				return false;
			 }
		}
		if(!IsEmpty(restDesc, '{Empty_Restaurant_TitleDesc}'))
        {
			return false;
        }
		if(!IsEmpty(restMainDesc, '{Empty_Restaurant_MainDesc}'))
        {
			return false;
        }	 
		if(!IsEmpty(restCuisineName, '{Empty_Cuisine_Name}'))
        {
			return false;
        }
		if(frm.restCuisineImage.value != '')
        {
			if(!checkImageType(restCuisineImage, '{Valid_File_Msg}'))
			{
				return false;
			}
		}	
		if(!IsEmpty(restCuisineDesc, '{Empty_Cuisine_Desc}'))
        {
			return false;
        }
        return true;
    }
}
