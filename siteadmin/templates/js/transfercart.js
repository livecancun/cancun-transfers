//====================================================================================================
//	Function Name	:	Show_Click()
//====================================================================================================
function Show_Click(CartId)
{
	//alert (TourCartId);
	with(document.frmTransferCart)
	{
		cartId.value  = CartId;
	//	alert (tourCartId.value);
		Action.value = "Show";
		submit();
	}
}


//====================================================================================================
//	Function Name	:	Delete_Click()
//====================================================================================================
function Delete_Click(CartId,TransferCartId)
{
	with(document.frmTransferCart)
	{
		if (confirm('{Confirm_Delete}'))
		{
			cartId.value = CartId;
			carttranId.value = TransferCartId;
			Action.value = "Delete";
			submit();
		}
	}
}