
//====================================================================================================
//	File Name		:	login.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Client side validation in JavaScript.
//	Author			:	Dinesh Sailor
//	Creation Date	:	05-May-2003
//	Copyright		:	Copyrights � 2003 Dot Infosys
//	Email			:	dinesh@dotinfosys.com
//	History			:
//						Date				Author					Remark
//						05-May-2003			Dinesh Sailor			Initial Release
//
//====================================================================================================

//====================================================================================================
//	Function Name	:	Form_Submit()
//	Purpose			:	This function will executed when user submits a form. It checks validity of 
//						every field in the form.
//	Parameters		:	frm  - form name
//	Return			:	true or false
//	Author			:	Dinesh Sailor
//	Creation Date	:	05-May-2003
//----------------------------------------------------------------------------------------------------
function Form_Submit(frm)
{
	with(frm)
    {
    	if(!IsEmpty(username, 'Please, enter Username.'))
        {
			return false;
        }
    	if(!IsEmpty(password, 'Please, enter Password.'))
        {
			return false;
        }
        return true;
    }
}


function ChangePassword_Form(frm)
{
	with(frm)
    {
		if(!IsEmpty(user_old_password, 'Please, enter old password.'))
        {
			return false;
        }
		if(!IsEmpty(user_new_password, 'Please, enter new password.'))
        {
			return false;
        }
		else if(!IsLen(user_new_password, 6, 15, 'Password must be at least six (6) characters long, \nand should not exceed more than 15 characters.'))
		{
			return false;
		}
		
		if(!IsEmpty(retype_password, 'Please, retype new password'))
        {
			return false;
        }
		else if(user_new_password.value != retype_password.value)
        {
			alert('Password confirmation does not match');
			return false;
        }
        return true;
    }
}

