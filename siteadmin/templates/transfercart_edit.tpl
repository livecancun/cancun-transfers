<table width="750" border="0" cellspacing="0" cellpadding="0">
		<tr> 
		  <td  width="95%" height="1" bgcolor="#000066"></td>
		</tr>
		<tr>
                <td colspan="2" class="blueHeader">{$Edit} -> {$L_Transfer_Service}</td>
	   </tr>
		<tr> 
		  <td  width="95%" height="1" bgcolor="#000066"></td>
		</tr>
	   <tr> 
          <td width="100%"  valign="top" bgcolor="#FFFFFF" align="center"><br>
			  <table width="80%" border="0" cellspacing="1" cellpadding="4">
				   <tr>
					 <td class="blueHeader" align="center" width="50%">{$L_Destinations}</td>
					 <td class="blueHeader" align="center" width="25%"><img src="{$imagepath}/XOneWay.gif" border="0"></td>
					 <td class="blueHeader" align="center" width="25%"><img src="{$imagepath}/XRoundTrip.gif" border="0"></td>
				  </tr>
				  <tr>
					<td height="1" align="center" colspan="5">{$nodata}</td>
				 </tr>	 
				  <!-- START BLOCK : MainBlock -->
				   <tr>
						<td class="List_B" align="center">{$V_Destination}</a></td>
						<td class="List_B" align="center">$ {$transferSingleTripRate}</td>
						<td class="List_B" align="center">$ {$transferRoundTripRate}</td>
				  </tr>
				  <!-- END BLOCK : MainBlock --> 
				 </table>
	  </td>
   </tr>
   <tr>
   <td  width="100%"  valign="top" bgcolor="#FFFFFF" align="center">
<FORM name="frmTransfer" action="{$A_TransferCart}" method="post">
<TABLE cellSpacing=1 cellPadding=1 width=80% bgColor=#EEEEEE  >
<tr>
<td colspan="2">
<TABLE cellSpacing=0 cellPadding=4 width=98% bgColor=#EEEEEE  border=0>
      <TR>
          <TD width="100%" >{$L_Book_Transfer}</TD>
	 </TR>
      <TR>
          <TD align="center">
                <TABLE cellSpacing=0 cellPadding=2 width=63%  bgColor=#EEEEEE border=0>
					<TR>
					  		<TD align=right><STRONG>{$L_Destinations}:</STRONG></TD>
							<TD align=left colSpan=5>
							<SELECT name="transferId"> 
								<!-- START BLOCK : Transfers -->
							 	  	<OPTION value="{$V_transferId}" {$transferselected}>{$V_transferOrgin} - {$V_transferDestination}</OPTION>
								<!-- END BLOCK : Transfers -->
							</SELECT> 
						   </TD>
					 </TR>
					  <TR>
						<TD align=right><STRONG>{$L_Adults}:</STRONG></TD>
						<TD><INPUT type="text" size=3 name="tres_no_adult" value="{$tres_no_adult}"></TD>
						<TD align=right nowrap><STRONG>{$L_Childs}:</STRONG></TD>
						<TD align=left><INPUT type="text" size=3 name="tres_no_kid" value="{$tres_no_kid}"></TD>
					 </TR>
					  <TR>
						<TD align=right><STRONG>{$L_Hotel}:</STRONG></TD>
						<TD align=left  colSpan=3><INPUT type="text" size=20 name="hotelName"  value="{$hotelName}"></TD>
					 </TR>
			  </TABLE>
	</td>
  </tr>
</table>
</td>
</tr>
<tr>
<td align="center" valign="top">
<TABLE cellSpacing=0 cellPadding=1 width=245 bgColor=#5270A1  border=0>
      <TR>
          <TD align="center">
		       <TABLE cellSpacing=0 cellPadding=2 width=245 bgColor=#EEEEEE border=0>
                    <TR>
                            <TD align=middle bgColor=#5270A1  colSpan=4>
							 <INPUT type="checkbox" value="1" name="singleTrip" {$CHECKED}>&nbsp;<STRONG><font color="#FFFFFF">{$L_Airport_To_Hotel}</font></STRONG>
							 </TD>
				   </TR>
                    <TR>
                           <TD align=left colSpan=4>{$L_Airport_To_Hotel_Msg}</TD>
					</TR>
                    <TR>
                            <TD align=right><STRONG>{$L_Date}</STRONG>:</TD>
                            <TD colSpan=3>
							<input type="text" name="arrivalDate" value="{$arrivalDate}" id="arrivalDate" size="17" >
								  <script language="JavaScript" type="text/javascript">
											fooCalendar = new dynCalendar('fooCalendar', 'calendarCallback', '../templates/images/');
								 </script>
							</TD>
					</TR>
                     <TR>
                           <TD align=right><STRONG>{$L_Time}</STRONG>:</TD>
                            <TD colSpan=3>
							<SELECT name="arrival_hour"	>
							  <!-- START BLOCK : Arrival_Hour -->
								<OPTION value="{$V_Arrival_Hour}"  {$hourselected}>{$V_Arrival_Hour}</OPTION>
							  <!-- END BLOCK : Arrival_Hour -->		
							</SELECT>: 
							<SELECT class=combo name="arrival_minute">
							  <!-- START BLOCK : Arrival_Minute -->
								<OPTION value="{$V_Arrival_Minute}"  {$minuteselected}>{$V_Arrival_Minute}</OPTION>
							  <!-- END BLOCK : Arrival_Minute -->	
							</SELECT>
							<SELECT class=combo name="arrival_DayPart">
									  <!-- START BLOCK : Arrival_DayPart -->
										<OPTION value="{$V_arrivalDayPart}"  {$arrivaldaypartselected}>{$V_arrivalDayPart}</OPTION>
									  <!-- END BLOCK : Arrival_DayPart -->	
							</SELECT> 
							</TD>
					</TR>
                     <TR>
                            <TD align=right><STRONG>{$L_Airline}:</STRONG></TD>
                            <TD><INPUT type="text" size=10 name="arrivalAirline" value="{$arrivalAirline}"></TD>
					</TR>
					<tr>
                    	   <TD noWrap align=right><STRONG>{$L_Flight} #:</STRONG></TD>
                           <TD><INPUT type="text" size=10 name="arrivalFlight" value="{$arrivalFlight}"></TD>
					</tr>
				</table>
		  </td>
   </tr>
</table>
</td>
<td align="center" valign="top">
<TABLE cellSpacing=0 cellPadding=1 width=245 bgColor=#5270A1  border=0>
      <TR>
          <TD align="center">
		       <TABLE cellSpacing=0 cellPadding=1 width=245 bgColor=#EEEEEE border=0>
					<TR>
                            <TD align=middle bgColor=#5270A1 colSpan=4>
							<INPUT type="checkbox" value="2" name="roundTrip" {$CHECKED1}>&nbsp;<STRONG><font color="#FFFFFF">{$L_Hotel_To_Airport}</font></STRONG>
							 </TD>
					 </TR>
                     <TR>
                            <TD align=left colSpan=4>{$L_Hotel_To_Airport_Msg}</TD>
					 </TR>
                      <TR>
                            <TD align=right><STRONG>{$L_Date}</STRONG>:</TD>
                            <TD colSpan=3>
							 <input type="text" name="departureDate" value="{$departureDate}" id="departureDate" size="17" >
								  <script language="JavaScript" type="text/javascript">
											fooCalendar1 = new dynCalendar('fooCalendar1', 'calendarCallback1', '../templates/images/');
								 </script>
							 </TD>
					</TR>
                     <TR>
                            <TD align=right><STRONG>{$L_Time}</STRONG>:</TD>
                            <TD colSpan=3>
							<SELECT name="departure_hour">
							  <!-- START BLOCK : Departure_Hour -->
								<OPTION value="{$V_Departure_Hour}"  {$hourselected}>{$V_Departure_Hour}</OPTION>
							  <!-- END BLOCK : Departure_Hour -->		
							</SELECT>: 
							<SELECT class=combo name="departure_minute">
							  <!-- START BLOCK : Departure_Minute -->
								<OPTION value="{$V_Departure_Minute}"  {$minuteselected}>{$V_Departure_Minute}</OPTION>
							  <!-- END BLOCK : Departure_Minute -->	
							</SELECT>
							<SELECT class=combo name="departure_DayPart">
									  <!-- START BLOCK : Departure_DayPart -->
										<OPTION value="{$V_departureDayPart}"  {$departuredaypartselected}>{$V_departureDayPart}</OPTION>
									  <!-- END BLOCK : Departure_DayPart -->	
							</SELECT> 
							</TD>
				    </TR>
                     <TR>
                            <TD align=right><STRONG>{$L_Airline}:</STRONG></TD>
                            <TD><INPUT type="text" size=10 name="departureAirline" value="{$departureAirline}"></TD>
					</TR>
					<tr>
							<TD align="right" nowrap><STRONG>{$L_Flight}#:</STRONG></TD>
	                         <TD><INPUT type="text"  size=10 name="departureFlight" value="{$departureFlight}"></TD>
					</tr>
				</table>
		  </td>
   </tr>
</table>

</td>
</tr>
 <TR>
	<TD colSpan=2 align="center">
	
	<!--input type="hidden" name="tres_no_kid" value="{$tres_no_kid}"-->
	<input type="hidden" name="carttranId" value="{$carttranId}">
	<input type="submit" name="Submit" value="{$L_Update}" class="nrlButton" onClick="javascript: return Form_Submit(frmTransfer);">&nbsp;&nbsp;
	<input type="submit" name="Submit" value="{$Cancel}" class="nrlButton">
	<input type="hidden" name="Action" value="{$ACTION}">
	<input type="hidden" name="status">
	<input type="hidden" name="confirmationNo" value="{$V_ConfirmationNo}">
	<!--input type="hidden" name="transferId" value="{$transferId}"--> 
	<input type="hidden" name="cartId" value="{$cartId}"> 
	
<!--INPUT type=image alt=Search src="{$imagepath}/Book_ING.gif" border=0-->

 
	<!--input type="button" value="Book" Name="Submit" class="lfmButton1" onClick="javascript: return Form_Submit(frmTransfer);"-->
	</TD>
</TR>
</table>
</form>

			 </td>
        </tr>
      </table>
    </td>
 </tr>
 <tr>
	    <td colspan="8">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr height="15">
						<td class="paggingLine" width="10%">
							&nbsp;&nbsp;{$Page_Link}
						</td>
					</tr>
				</table>
			</td>
		</tr>
</table>
