<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
	<tr>
		<td align="center">
			<table border="0" cellpadding="0" cellspacing="1" width="550" align="center">
				<tr>
					<td align="center">
						Welcome user from <b>{$smarty.server.REMOTE_ADDR}</b>.
						Enter your username and password to login.
					</td>
				</tr>
			</table>
			<br>
			<table border="0" cellpadding="0" cellspacing="1" width="350" class="stdTableBorder" align="center">
				<tr>
					<td class="stdSectionHeader">
						<table border="0" cellpadding="0" cellspacing="1" width="100%">
							<tr>
								<td class="stdSection">Login Here</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" align="center">
						<form name="frmLogin" action="{$A_Login}"  method="post" onSubmit="JavaScript: return Form_Submit(this);">
						<table width="350" border="0" cellspacing="1" cellpadding="0">
							<tr><td colspan="2" height="10"></td></tr>
							<tr><td colspan="2" class="errorMsg" align="center">&nbsp;{$Error_Message}</td></tr>
							<tr><td colspan="2" height="10"></td></tr>
							<tr>
								<td align="right" width="40%">Username &nbsp;</td>
								<td><input type="text"  name="username"  value="{$smarty.post.username}" size="18" maxlength="25" tabindex="1"></td>
							</tr>
							<tr>
								<td align="right">Password &nbsp;</td>
								<td><input  type="password" name="password"  maxlength="15" size="18" tabindex="2"></td>
							</tr>
							<tr><td colspan="2" height="5"></td></tr>
							<tr>
								<td>&nbsp;</td>
								<td>
									<input type="submit" name="Submit" value="Login" class="stdButton" tabindex="3">&nbsp;
									<input type="reset" name="Submit" value="Reset" class="stdButton" tabindex="4">
								</td>
							</tr>
							<tr><td colspan="2" height="10"></td></tr>
						</table>
						</form>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>