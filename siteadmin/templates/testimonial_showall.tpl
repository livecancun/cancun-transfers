<script>
	var Delete_Testimonial 		= '{$Delete_Testimonial}';	
	var Delete_AllTestimonials 	= '{$Delete_AllTestimonials}';	
</script>

<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="100%">
<form name="frmTestimonial" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection">{$L_Manage_Testimonial} [ <a href="javascript: Add_Click('{$Add_Action}');" class="actionLink">{$Add}</a> ]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>{$L_Testimonial_Desc}</td>
				</tr>
				<tr>
					<td align="center" class="successMsg">{$SuccMessage}</td>
				</tr>
				<tr>
					<td valign="top" align="center">
						<table border="0" cellpadding="2" cellspacing="2" width="97%">
							<tr>
								<td class="listHeader" width="3%"></td>
								<td class="listHeader">{$Photo}</td>
								<td class="listHeader">{$Comment}</td>	
								<td class="listHeader" width="15%">{$Display_On_Website}</td>																								
								<td class="listHeader" width="14%">{$L_Action}</td>
							</tr>
							
							{foreach from=$PersonData item=Person}    
							<tr class="{cycle values='list_A, list_B'}">
								<td valign="top"><input type="checkbox" name="person_list[]" class="stdCheckBox" value="{$Person->person_id}"></td>
								<td width="130" valign="top"><img src="{if $Person->person_pic} {$Testimonial_Path|cat:'thumb_'|cat:$Person->person_pic|regex_replace:'/[\\\\]/':''} {else} {$Testimonial_Path|cat:$Default_Picture} {/if}" border="0"></td> 
								<td valign="top">
									<table border="0" cellpadding="0" cellspacing="0" width="98%">
										<tr> 
											<td><p align="justify">
												{if $Person->person_comment|count_characters == 200}
													{$Person->person_comment|regex_replace:"/[\\\\]/":""}
												{else}
													{$Person->person_comment|regex_replace:"/[\\\\]/":""|truncate:200:"..."}
												{/if}</p>
											</td> 
										</tr> 
										<tr>
											<td class="boldText">
												{$Person->person_name|regex_replace:"/[\\\\]/":""}
											</td>
										</tr>
									</table>
								</td> 
								<td align="center">
										{if $Person->person_status == 1}
											YES
										{else}
											NO
										{/if}
								</td>
								<td align="center">
									<img src="{$Templates_Image}a_view.gif" 	class="imgAction" title="View" onClick="javascript: View_Click('{$A_View}','{$Person->person_id}');">&nbsp;&nbsp;
									<img src="{$Templates_Image}a_edit.gif" 	class="imgAction" title="Edit" onClick="JavaScript: Edit_Click('{$A_Edit}','{$Person->person_id}');">&nbsp;&nbsp;
									<img src="{$Templates_Image}a_delete.gif" 	class="imgAction" title="Delete" onClick="JavaScript: Delete_Click('{$A_Delete}','{$Person->person_id}');">&nbsp;&nbsp;
								</td> 
							</tr>
							{/foreach}
							<tr><td colspan="5">&nbsp;</td></tr>
							<tr>
								<td colspan="5">
									<table border="0" cellpadding="1" cellspacing="1" width="95%">
										<tr>
											<td>
												{if $Person->person_id!=''}
												<img src="{$Templates_Image}arrow_ltr.png"> 
												<a href="JavaScript: CheckUncheck_Click(document.all['person_list[]'], true);" onMouseMove="window.status='Check All';" onMouseOut="window.status='';" class="actionLink">Check All</a> / 
												<a href="JavaScript: CheckUncheck_Click(document.all['person_list[]'], false);" onMouseMove="window.status='Uncheck All';" onMouseOut="window.status='';" class="actionLink">Uncheck All</a>  &nbsp;&nbsp;
												With selected
												<img src="{$Templates_Image}a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: DeleteChecked_Click('{$A_DelSelected}');">
											   {/if}
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="5" class="pageLink" align="right">{$Page_Link}<!-- Page : 1 2 3 Next --></td>
							</tr>
							<input type="hidden" name="person_id">
							<input type="hidden" name="Action" value="{$ACTION}">
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>	
</table>