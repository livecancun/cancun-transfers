<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmPage" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">Page Manager</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						Manage your website static contents.
					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;{$succMessage}</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td colspan="3" align="right">
						<a href="{$smarty.server.PHP_SELF}?Action=Add"><img src="{$Templates_Image}a_add.gif" class="imgAction" title="Add Page"></a>&nbsp;&nbsp;
						<a href="{$smarty.server.PHP_SELF}?Action=Sort"><img src="{$Templates_Image}a_sort.gif" class="imgAction" title="Sort Pages"></a>&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td class="listHeader" width="70%">Page Title</td>
					<td class="listHeader" width="15%">Action</td>
				</tr>
				{include file='tree_node.tpl' node=$R_RecordSet level=0}
			</table>
			<br><br>
			<input type="hidden" name="Action">
			<input type="hidden" name="pid">
			<input type="hidden" name="status">
		</td>
	</tr>
	</form>
</table>