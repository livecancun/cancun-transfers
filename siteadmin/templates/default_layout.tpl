<html>
<head>
	<title>{$Site_Title}</title>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
	<meta name="title" 			content="{$Meta_Title}">
	<meta name="description" 	content="{$Meta_Description}">
	<meta name="keywords" 		content="{$Meta_Keyword}">
	<link href="{$Templates_CSS}style.css" rel="stylesheet" type="text/css">
	<script language="javascript" src="{$Templates_JS}validate.js"></script>
	<script language="javascript" src="{$Templates_JS}functions.js"></script>
    {section name=FileName loop=$JavaScript}
    <script language="javascript" src="{$Templates_JS}{$JavaScript[FileName]}"></script>
	{/section}
</head>
<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" bgcolor="#FFFFFF">
{if $smarty.const.POPUP_WIN != 'true' }
	{include file="$T_Header"}
{/if}
<table border="0" cellpadding="0" cellspacing="10" width="100%" align="center" height="63%">
	<tr>
    	{if $smarty.session.User_Id && ($smarty.const.POPUP_WIN != 'true')}
		<td width="175" align="center" valign="top">
			{include file="$T_Menu"}
		</td>
        {/if}
		<td valign="top" align="center">
			{include file="$T_Body"}
		</td>
	</tr>
</table>
{if $smarty.const.POPUP_WIN != 'true' }
	{include file="$T_Footer"}
{/if}
</body>
</html>