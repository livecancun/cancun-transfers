<script>
	var Confirm_Delete = '{$Confirm_Delete}';
</script>


<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">Reservation</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td class="successMsg" align="center"> {$succMessage}</td></tr>
			</table>
<table border="0" cellpadding="0" cellspacing="0" width="750">
<form name="frmTransferCart" action="{$A_TransferCart}" method="post">
 <tr>
	<td>
      <table border="0" cellpadding="0" cellspacing="1" width="100%">
		<tr><td colspan="2" height="5"></td></tr>
		   <tr>
			  <td align="center" valign="top">
				<table border="0" cellpadding="2" cellspacing="2" width="98%">
					<tr> 
					  <td colspan="2" class="blueHeader"> {$View} -> {$L_Trasferdetail}</td>
					</tr>
					<tr>
						 <td colspan="2">&nbsp;</td>
					</tr>


				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								{if $trip_status == 0 || $trip_status == 2}		
								<td width="50%">
									<table width="100%" cellpadding="2" cellspacing="2" >
										<tr> 
											<td valign="top" class="fieldLabelRight" width="40%">{$L_Destinations} :</td>
											<td width="60%">{$destination}</td>
										</tr>
										<tr> 
											<td valign="top" class="fieldLabelRight">{$L_Hotelname} :</td>
											<td>{$hotelName}</td>
										</tr>
										<tr> 
											<td valign="top" class="fieldLabelRight">{$L_No_Person} :</td>
											<td>{$tres_no_person}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_arrivalDate} :</td>
										  <td>{$arrivalDate}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_arrivalTime} :</td>
										  <td  >{$arrivalTime}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_arrivalAirline} :</td>
										  <td  >{$arrivalAirline}</td>
										</tr>
										
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_arrivalFlight} :</td>
										  <td  >{$arrivalFlight}</td>
										</tr>
									</table>
								</td>
								{/if}
								<td width="3%"></td>
								
								{if $trip_status == 1 || $trip_status == 2}	
								<td width="50%">
									<table width="100%" cellpadding="2" cellspacing="2" >
										<tr> 
										  <td valign="top" class="fieldLabelRight" width="40%">{$L_Origin} :</td>
										  <td width="60%">{$departureDestination}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_Hotel} :</td>
										  <td>{$departureHotelName}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_No_Person} :</td>
										  <td>{$departureTotalPerson}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_departureDate} :</td>
										  <td>{$departureDate}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_departureTime} :</td>
										  <td>{$departureTime}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_departureAirline} :</td>
										  <td>{$departureAirline}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_Pickup_Time} :</td>
										  <td  >{$pickupTime}</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight">{$L_departureFlight} :</td>
										  <td>{$departureFlight}</td>
										</tr>
									</table>
								</td>
								{/if}								
							</tr>				
						</table>
					</td>
				</tr>

				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="2" cellspacing="2">
							{if $is_airline_empl == 1}
							<tr> 
							  <td valign="top" class="fieldLabelRight" width="40%">{$L_Applicable_Discount} :</td>
							  <td width="60%">{$discount_title}</td>
							</tr>
							<tr> 
							  <td valign="top" class="fieldLabelRight">{$L_Company_Name} :</td>
							  <td>{$empl_airline}</td>
							</tr>
							<tr> 
							  <td valign="top" class="fieldLabelRight">{$L_Company_ID} :</td>
							  <td>{$empl_id}</td>
							</tr>
							{/if}
						</table>
					</td>
				</tr>

				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="2" cellspacing="2">
							{if $is_airline_empl == 1}
							<tr> 
							  <td valign="top" class="fieldLabelRight" width="40%">{$L_Total} :</td>
							  <td width="60%"> $ {$transferCharge}</td>
							</tr>

							<tr> 
							  <td valign="top" class="fieldLabelRight" width="40%">{$L_Discount} [{$discount_rate}%] :</td>
							  <td width="60%">- $ {math equation="(x * y)/100" x=$transferCharge y=$discount_rate format="%.2f"}</td>
							</tr>
							{/if}

							<tr> 
								<td valign="top" class="fieldLabelRight" width="40%">{$L_Total} :</td>
								<td width="60%">$ 
									{if $is_airline_empl == 1}
										{math equation="x - ((x * y)/100)" x=$transferCharge y=$discount_rate format="%.2f"}
									{else}
										{$transferCharge}
									{/if}
								</td>
							</tr>
							<tr> 
							  <td valign="top" class="fieldLabelRight" width="40%">{$L_Transferstatus} :</td>
							  <td width="60%"><b>{$cartStatus}</b></td>
							</tr>
						</table>
					</td>
				</tr>

                <tr>
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" height="10"></td>
                </tr>
				<tr>
					<td colspan="2" align="center">
						<input type="button" name="Submit" value="{$Back}" class="greyButton" onclick="javascript:location.href('reservation.php')">
					</td>
				</tr>
                <tr> 
                  <td align="center" colspan="2"> 
                    <input type="hidden" name="Action" value="{$ACTION}"> 
					<input type="hidden" name="user_id" value="{$user_id}"> 
					<input type="hidden" name="carttranId" value="{$carttranId}"> 
					<input type="hidden" name="confirmationNo" value="{$V_ConfirmationNo}">
					<input type="hidden" name="cartId" value="{$cartId}"> 
					<input type="hidden" name="Start" value="{$Start}"> </td>
                </tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr><td colspan="2"><b>{$Note} : </b></td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr> 
				  <td colspan="2" height="1" bgcolor="#000066"></td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><b>NR</b> - {$NoRequest}</td>
								<td><b>PR</b> - {$PendingRequest}</td>
								<td><b>AR</b> - {$ApproveRequest}</td>
							</tr>
							<tr>
								<td><b>NA</b> - {$NotApprove}</td>
								<td><b>RP</b> - {$RequestPayment}</td>
								<td><b>PD</b> - {$PaymentDone}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr> 
				  <td colspan="2" height="1" bgcolor="#000066"></td>
				</tr>
				</table>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
        </td>
	</tr>
	</form>
	</table>		
		</td>
	</tr>
</table>




