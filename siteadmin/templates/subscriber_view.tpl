<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmUser" action="{$A_Action}" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="{$Templates_Image}icon_master.gif"></td>
					<td class="stdSection" width="49%" nowrap>User Management [{$User->client_firstname} {$User->client_lastname}]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="5" cellspacing="1" width="95%">
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						Personal Information
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">First Name:</td>
					<td class="fieldInputStyle">{$User->client_firstname}</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Last Name:</td>
					<td class="fieldInputStyle">{$User->client_lastname}</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Email:</td>
					<td class="fieldInputStyle">
						<a href="mailto:{$User->client_email}" class="navigateLink">{$User->client_email}</a>
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Subscribe:</td>
					<td class="fieldInputStyle">{$User->client_subscribe}</td>
				</tr>
				<tr><td colspan="2" height="10"></td></tr>
				<tr><td colspan="2" align="center"><input type="button" class="stdButton" value="Close Button" onClick="window.close();"></td></tr>
				<tr><td colspan="2" height="10"></td></tr>
			</table>	
		</td>
	</tr>
	</form>
</table>