<?php
#====================================================================================================
# File Name : links.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#====================================================================================================
#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_ADMIN', 	true);

# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'Links.php');

# Initialize object with required module
$objLinks	= new Links();

$scriptName = "links.php?&start=$start_record";

#====================================================================================================
#	Add Service into database
#----------------------------------------------------------------------------------------------------
if($_POST['Action']==ADD && $_POST['Submit']==SAVE)
{	
	$no = $objLinks->Add_Link_Category($_POST['cat_name'],'0',$_POST['cat_status']);

	$startpt = $_GET['start'];
	header("location: links.php?&start=$startpt&add=true");
	exit;
	
}
#====================================================================================================
#	Update Service into database
#----------------------------------------------------------------------------------------------------
elseif($_POST['Submit']==UPDATE && $_POST['Action']==MODIFY)
{	
	$no = $objLinks->Update_Category($_POST['cat_id'],$_POST['cat_name'],$_POST['cat_status']);
	$startpt = $_GET['start'];
	header("location: links.php?&start=$startpt&update=true");
	exit;
}
#====================================================================================================
#	Delete Service
#----------------------------------------------------------------------------------------------------
elseif($_GET['Action']==DELETE || $_POST['Action']==DELETE)
{
	$no = $objLinks->Delete_Category($_POST['cat_id']);
	$startpt = $_GET['start'];
	header("location: links.php?&start=$startpt&delete=true");
	exit;
}

#====================================================================================================
#	Add LInk
#----------------------------------------------------------------------------------------------------
elseif($_POST['Action']==ADDLINK  && $_POST['Submit']==SAVE)
{
	$url_telno=$url_telno_areacode."-".$url_telno_citycode."-".$url_telno_no;

	$no = $objLinks->Add_LinkDetail($_POST['cat_id'],		$_POST['url_name'],		$_POST['url_address'],
								   $_POST['url_city'],		$_POST['url_state'],	$_POST['url_zip'],
								   $_POST['url_country'],	$url_telno,				$_POST['url_email'],
								   $_POST['url_title'],		$_POST['url_desc'],		$_POST['url_url'],
								   $_POST['url_oururl'],	date('Y-m-d'),			$_POST['url_email'],
								   $_POST['cat_status']);
	$startpt = $_GET['start'];
	header("location: links.php?&start=$startpt&linkadd=true");
	exit;
}

#====================================================================================================
#	Modify LInk
#----------------------------------------------------------------------------------------------------
elseif($_POST['Action']==MODIFYLINK  && $_POST['Submit']==UPDATE)

{
	$url_telno=$_POST['url_telno_areacode']."-".$_POST['url_telno_citycode']."-".$_POST['url_telno_no'];
	
	$no = $objLinks->Update_LinkDetail($_POST['link_id'],	$_POST['url_name'],		$_POST['url_address'],
									$_POST['url_city'],		$_POST['url_state'],	$_POST['url_zip'],
									$_POST['url_country'],	$url_telno,				$_POST['url_title'],
									$_POST['url_desc'],		$_POST['url_url'],		$_POST['url_oururl'],
									$_POST['url_email'],	$_POST['cat_status']);
	$startpt = $_GET['start'];
	header("location: links.php?&start=$startpt&linkupdate=true");
	exit;
}

#====================================================================================================
#	Delete Link
#----------------------------------------------------------------------------------------------------
elseif($_POST['Action']==DELETELINK)
{
	$no = $objLinks->Delete_LinkDetail($_POST['link_id']);
	
	$startpt = $_GET['start'];
	header("location: links.php?&start=$startpt&linkdelete=true");
	exit;
}

#==================================================================================================
# Set the Starting Page, Page Size
#==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 100;
$num_records = '';

#====================================================================================================
#	Listing
#----------------------------------------------------------------------------------------------------
//IF CANCEL WAS CLICKED ON PREVIOUS PAGE OR LISTING FOR THE FIRST TIME
if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL || $_POST['Submit']==CANCEL)
{
		$tpl->assign(array( 'T_Body'		=>	'link_category_showall'. $config['tplEx'],
							'JavaScript'	=>	array('link.js'),
							'A_Action'		=>	$scriptName,
							));
		
		if($_GET['add'] == true)
			$succ_message = "<br>". $lang['L_LinkCat_Added'];
	
		if($_GET['update'] == true)
			$succ_message = "<br>". $lang['L_LinkCat_Updated'];
	
		if($_GET['delete'] == true)
			$succ_message = "<br>". $lang['L_LinkCat_Deleted'];

		if($_GET['linkadd'] == true)
			$succ_message = "<br>". $lang['L_Link_Added'];
	
		if($_GET['linkupdate'] == true)
			$succ_message = "<br>". $lang['L_Link_Updated'] ;
	
		if($_GET['linkdelete'] == true)
			$succ_message = "<br>". $lang['L_Link_Deleted'];
		
				
		$tpl->assign(array ("imagepath"		=>	$physical_path['Image_Root'],
						  "jsroot"         	=>  $physical_path['Js_Root'],
						  "cssSiteroot"   	=>	$physical_path['Css_Root']));
						   
		$tpl->assign(array(	"A_Action"   		=>  "links.php?&start=$start_record",
							"Message"			=>  $succ_message,
							"L_Link_Manager"	=>  $lang['L_Link_Manager'],			
							"L_Manage_Links"	=>  $lang['L_Manage_Links'],			
							"L_Add_Category"	=>  $lang['L_Add_Category'],										
							"L_Title"			=>  $lang['L_Link_Mgt'],
							"L_Link_Add_Link"	=>  $lang['L_Link_Add_Link'],	
							"msg_CatDel"		=>  $lang['msg_CatDel'],								
							"Modify"			=>  $lang['Modify'],	
							"Delete"			=>  $lang['Delete'],															
							"L_Category_Name"	=>  $lang['L_Category'],
							"L_Action"			=>	$lang['Action'],
							));
	
		$tpl->assign(array("LinkInfo" 	=>  $objLinks->Show_Link_Category3($cat_id='',$start_record, $Page_Size, &$num_records),
							));
	
		$tpl->assign(array("LinkDtlInfo"=>  $objLinks->Show_Category()
						));

		if($num_records > $Page_Size)
			$tpl->assign("Page_Link", "Go To" . ' ' . generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));
}

#====================================================================================================
#	Add new Banner or update Banner
#----------------------------------------------------------------------------------------------------
elseif($_GET['Action']==ADD || $_POST['Action']==ADD || $_GET['Action']==MODIFY || $_POST['Action']==MODIFY)
{
	$tpl->assign(array( 'T_Body'		=>	'link_category_addedit'. $config['tplEx'],
						'JavaScript'	=>	array('link.js'),
						'A_Action'		=>	$scriptName,
						));
	
	$tpl->assign(array ("imagepath"		=>	$physical_path['Image_Root'],
					  "jsroot"         	=>  $physical_path['Js_Root'],
					  "cssSiteroot"   	=>	$physical_path['Css_Root'],
					));

	$tpl->assign(array(	"A_Action"				=>  "links.php?&start=$start_record",
					   	"Start"					=>	$start_record,
						"L_Link_Manager"		=>  $lang['L_Link_Manager'],
						"L_Manage_Links"		=>  $lang['L_Manage_Links'],			
					   	"L_Select_Status"		=>  $lang['L_Select_Status'],
					   	"L_Category_Detail"		=>  $lang['L_Category_Detail'],
					   	"L_New_Category_Name"	=>  $lang['L_New_Category_Name'],
					   	"L_Category_Status"		=>  $lang['L_Category_Status'],						
					));


	$tpl->assign(array(	"msg_CatName"		=>  $lang['msg_CatName'],
						"L_Manage_Links"	=>  $lang['L_Manage_Links'],			
					));


	if($_GET['Action'] == ADD || $_POST['Action'] == ADD)
	{
		$tpl->assign(array( "ACTION"		=> 	ADD,
							"L_Action"		=>	ADD,
							"Status_List"	=>  fillArrayCombo($arrLinkStatus),
							"Save"  		=>   $lang['save'],
							"Cancel"    	=>   $lang['cancel'],
							"Add"			=>   $lang['add'],
					));
	
	}
	elseif($_GET['Action'] == MODIFY || $_POST['Action'] == MODIFY)
	{
		$tpl->assign(array( "ACTION"		=> 	MODIFY,
							"L_Action"		=>	MODIFY,
							"Update"  		=>  $lang['update'],
							"Cancel"    	=>  $lang['cancel'],
							"Edit"			=>  $lang['edit'],
							));
				
	
		$record  = $objLinks->Show_Link_Category2($_POST['cat_id'], $start_record, $Page_Size, &$num_records);
		$db->next_record();
		$tpl->assign(array("cat_id"   		=> $db->f('cat_id'),
						   "cat_name" 		=> stripslashes($db->f('cat_name')),
						   "cat_desc" 		=> stripslashes($db->f('cat_desc')),
						   "Status_List"	=> fillArrayCombo($arrLinkStatus,stripslashes($db->f('cat_status'))),
					));
	}
}	

elseif($_GET['Action']==ADDLINK || $_POST['Action']==ADDLINK || $_GET['Action']==MODIFYLINK || $_POST['Action']==MODIFYLINK)
{

	$tpl->assign(array( 'T_Body'		=>	'link_addedit'. $config['tplEx'],
						'JavaScript'	=>	array('link.js'),
						'A_Action'		=>	$scriptName,
						));

	$tpl->assign(array ("imagepath"		=>	$physical_path['Image_Root'],
						"jsroot"		=>  $physical_path['Js_Root'],
						"cssSiteroot"	=>	$physical_path['Css_Root'],
						));

	$tpl->assign(array("A_Action"			=>  "links.php?&start=$start_record",
					   "Start"				=>	$start_record,
						"L_Link_Manager"	=>  $lang['L_Link_Manager'],			
						"L_Manage_Links"	=>  $lang['L_Manage_Links'],			
						"L_Add_Category"	=>  $lang['L_Add_Category'],										
						"L_Title"			=>  $lang['L_Link_Mgt'],
						"L_Link_Add_Link"	=>  $lang['L_Link_Add_Link'],	
						"L_Link_Status"		=>  $lang['L_status'],							
					));

	$tpl->assign(array( "msg_Name"		=>  $lang['msg_Name'],			
						"msg_Address"	=>  $lang['msg_Address'],			
						"msg_City"		=>  $lang['msg_City'],										
						"msg_State"		=>  $lang['msg_State'],
						"msg_Zip"		=>  $lang['msg_Zip'],	
						"msg_Country"	=>  $lang['msg_Country'],
						"msg_Telephone"	=>  $lang['msg_Telephone'],	
						"msg_Title"		=>  $lang['msg_Title'],
						"msg_Description"=>  $lang['msg_Description'],	
						"msg_Url"		=>  $lang['msg_Url'],
						"msg_Our_Url"	=>  $lang['msg_Our_Url'],
						"msg_Email"		=>  $lang['msg_Email'],	
					));

	if($_GET['Action'] == ADDLINK || $_POST['Action'] == ADDLINK)
	{
		$tpl->assign(array( "ACTION"			=>	ADDLINK,
							"L_Action"			=>	ADDLINK,
							"Save"  			=>	$lang['save'],
							"Cancel"    		=>	$lang['cancel'],
							"Status_List"		=>	"<option value=\"0\">Disable</option><option value=\"1\" selected >Enable</option>",
							"cat_id"			=>	$_POST['cat_id'],
							"L_Link_Name"		=>	$lang['L_Link_Name'],
							"L_Link_Address"	=>	$lang['L_Link_Address'],
							"L_Link_City"		=>	$lang['L_Link_City'],
							"L_Link_State"		=>	$lang['L_Link_State'],
							"L_Link_Zip"		=>	$lang['L_Link_Zip'],
							"L_Link_Country"	=>	$lang['L_Link_Country'],
							"L_Link_Telephone"	=>	$lang['L_Link_Telephone'],
							"L_Link_Title"		=>	$lang['L_Link_Title'],
							"L_Link_Category"	=>	$lang['L_Link_Category'],
							"L_Link_Description"=>	$lang['L_Link_Description'],
							"L_Link_Url"		=>	$lang['L_Link_Url'],
							"L_Link_Our_Url"	=>	$lang['L_Link_Our_Url'],
							"L_Link_Email"		=>	$lang['L_Link_Email'],
							
					));
		$tpl->assign(array(	"msg_Name"			=>	$lang['msg_Name'],
							"msg_Address"		=>  $lang['msg_Address'],
							"msg_City"			=>	$lang['msg_City'],
							"msg_State"			=>	$lang['msg_State'],
							"msg_Zip"			=>	$lang['msg_Zip'],
							"msg_Country"		=>	$lang['msg_Country'],
							"msg_Telephone"		=>	$lang['msg_Telephone'],
							"msg_Title"			=>	$lang['msg_Title'],
							"msg_Category"		=>	$lang['msg_Category'],
							"msg_Description"	=>	$lang['msg_Description'],
							"msg_Url"			=> 	$lang['msg_Url'],
							"msg_Our_Url"		=>	$lang['msg_Our_Url'],
							"msg_Email"			=>	$lang['msg_Email'],
							));

		$record  = $objLinks->Show_Link_Category2($_POST['cat_id'], $start_record, $Page_Size, &$num_records);

		$db->next_record();

		$tpl->assign("Link_Category",	$db->f('cat_name'));	
	}

	elseif($_GET['Action'] == MODIFYLINK || $_POST['Action'] == MODIFYLINK)
	{
		$tpl->assign(array( "ACTION"		=> 	MODIFYLINK,
							"L_Action"		=>	MODIFYLINK,
							"Update"  		=>   $lang['update'],
							"Cancel"    	=>   $lang['cancel'],
							"Edit"			=>   $lang['edit'],
							"L_Link_Name"		=>	$lang['L_Link_Name'],
							"L_Link_Address"	=>	$lang['L_Link_Address'],
							"L_Link_City"		=>	$lang['L_Link_City'],
							"L_Link_State"		=>	$lang['L_Link_State'],
							"L_Link_Zip"		=>	$lang['L_Link_Zip'],
							"L_Link_Country"	=>	$lang['L_Link_Country'],
							"L_Link_Telephone"	=>	$lang['L_Link_Telephone'],
							"L_Link_Title"		=>	$lang['L_Link_Title'],
							"L_Link_Category"	=>	$lang['L_Link_Category'],
							"L_Link_Description"=>	$lang['L_Link_Description'],
							"L_Link_Url"		=>	$lang['L_Link_Url'],
							"L_Link_Our_Url"	=>	$lang['L_Link_Our_Url'],
							"L_Link_Email"		=>	$lang['L_Link_Email'],
							));

		$tpl->assign(array(	"msg_Name"			=>	$lang['msg_Name'],
							"msg_Address"		=>  $lang['msg_Address'],
							"msg_City"			=>	$lang['msg_City'],
							"msg_State"			=>	$lang['msg_State'],
							"msg_Zip"			=>	$lang['msg_Zip'],
							"msg_Country"		=>	$lang['msg_Country'],
							"msg_Telephone"		=>	$lang['msg_Telephone'],
							"msg_Title"			=>	$lang['msg_Title'],
							"msg_Category"		=>	$lang['msg_Category'],
							"msg_Description"	=>	$lang['msg_Description'],
							"msg_Url"			=> 	$lang['msg_Url'],
							"msg_Our_Url"		=>	$lang['msg_Our_Url'],
							"msg_Email"			=>	$lang['msg_Email'],
							));

		$record  = $objLinks->Show_LinkDetail($_POST['link_id'], $start_record, $Page_Size, &$num_records);
		$db->next_record();
		
		$link_cat_id = $db->f('link_category_id');
		$telno	= ereg_replace('[-()\ ]', '', $db->f('link_telno'));

		$tpl->assign(array(
							"url_name"   				=> $db->f('link_name'),
							"url_address"   			=> $db->f('link_address'),
							"url_city"   				=> $db->f('link_city'),
							"url_state"   				=> $db->f('link_state'),
							"url_zip"   				=> $db->f('link_zip'),
							"url_country"   			=> $db->f('link_country'),
							"url_telno_areacode"   		=> substr($telno, 0, 3),
							"url_telno_citycode"   		=> substr($telno, 3, 3),
							"url_telno_no"   			=> substr($telno, 6,4),
							"url_title"   				=> $db->f('link_title'),
						    "url_desc" 					=> stripslashes($db->f('link_desc')),
   						    "url_url" 					=> stripslashes($db->f('link_url')),
							"url_oururl" 				=> stripslashes($db->f('link_oururl')),
						    "url_email" 				=> stripslashes($db->f('link_add_by')),								   
						    "Status_List"				=> fillArrayCombo($arrLinkStatus,stripslashes($db->f('link_status'))),
						    "link_id"					=> $db->f('link_id'),
					));
		
		$record  = $objLinks->Show_Link_Category2($link_cat_id, $start_record, $Page_Size, &$num_records);
		
		$db->next_record();
		
		$tpl->assign( "Link_Category" ,  $db->f('cat_name'));
		
	}

}

	$tpl->display('default_layout'. $config['tplEx']);
	
?>