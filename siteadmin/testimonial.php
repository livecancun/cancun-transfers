<?php
#====================================================================================================
# File Name : testimonial.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contains all application configuration details
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
//define('IN_SITE', 	true);
define("IN_ADMIN", true);

# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'Testimonial.php');

define("DEBUG", false);

if(DEBUG)
{
	print "<br>Post Action = ". $_POST['Action'];
	print "<br>Get Action = ". $_GET['Action'];
	print "<br>Post Submit = ". $_POST['Submit'];
	print "<br>Get Submit = ". $_GET['Submit'];
}

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : A_VIEW_ALL);

if($_GET['start'])
	$startrecord = $_GET['start'];
else
	$startrecord = 0;

# Initialize office object
$testimonial = new Testimonial();

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
$redirectUrl = 'testimonial.php?start='. $_GET['start']. '&';

#====================================================================================================
#	Add report details into database
#----------------------------------------------------------------------------------------------------
if($Action == A_ADD && $_POST['Submit'] == $lang['save'])
{
	if(!empty($_FILES['person_pic']['tmp_name']))
	{
		
		$uploadedFileName = fileUpload2($_FILES['person_pic'], 
										IMG_TESTIMONIALS, 
										array(array($config['picSizeAuto'])),'');
	}

	$phoneno = "";
	if($_POST['person_phone_areacode'])
		$phoneno = "(". $_POST['person_phone_areacode'] .")".$_POST['person_phone_citycode']."-".$_POST['person_phone_no'];
	$faxno = "";
	if($_POST['person_fax_areacode'])
		$faxno = "(". $_POST['person_fax_areacode'] .")".$_POST['person_fax_citycode']."-".$_POST['person_fax_no'];
	$tollfreeno = "";
	if($_POST['person_tollfree_areacode'])
		$tollfreeno = "(". $_POST['person_tollfree_areacode'] .")".$_POST['person_tollfree_citycode']."-".$_POST['person_tollfree_no'];

	# Save information
	$retVal = $testimonial->Insert(	$_POST['person_name'],	$_POST['person_company'],	$_POST['person_address'],	$_POST['person_city'],
									$_POST['person_state'],	$_POST['person_country'],	$_POST['person_zip'],		$phoneno,					$faxno,
									$tollfreeno,				$_POST['person_email'],		$_POST['person_website'],	$uploadedFileName,
									$_POST['person_desc'],	$_POST['person_comment'],	$_POST['person_status']);

	header('location: '. $redirectUrl. A_ADD. '='. $retVal);
//header('location: index.php');

	exit();
}

#====================================================================================================
#	Update report details into database
#----------------------------------------------------------------------------------------------------
elseif($Action == A_EDIT && $_POST['Submit'] == $lang['save'])
{
	# Upload picture if exists
	if(!empty($_FILES['person_pic']['tmp_name']))
	{
		$uploadedFileName = fileUpload2($_FILES['person_pic'], 
										IMG_TESTIMONIALS, 
										array(array($config['picSizeAuto'])),
										$_POST['person_pic_edit']);
	}
	else
	{
		if($_POST['delete_picture'])
		{
			@unlink($physical_path[IMG_TESTIMONIALS]. $_POST['person_pic_edit']);
			@unlink($physical_path[IMG_TESTIMONIALS]. 'thumb_'. $_POST['person_pic_edit']);
			$uploadedFileName = '';
		}
		else
			$uploadedFileName = $_POST['person_pic_edit'];
	}

	$phoneno = "";
	if($_POST['person_phone_areacode'])
		$phoneno = "(". $_POST['person_phone_areacode'] .")".$_POST['person_phone_citycode']."-".$_POST['person_phone_no'];
	$faxno = "";
	if($_POST['person_fax_areacode'])
		$faxno = "(". $_POST['person_fax_areacode'] .")".$_POST['person_fax_citycode']."-".$_POST['person_fax_no'];
	$tollfreeno = "";
	if($_POST['person_tollfree_areacode'])
		$tollfreeno = "(". $_POST['person_tollfree_areacode'] .")".$_POST['person_tollfree_citycode']."-".$_POST['person_tollfree_no'];

	# Save Information
	$retVal = $testimonial->Update(	$_POST['person_id'],	$_POST['person_name'], 	$_POST['person_company'], 	$_POST['person_address'],
									$_POST['person_city'],	$_POST['person_state'], $_POST['person_country'],	$_POST['person_zip'], 		$phoneno,
									$faxno,					$tollfreeno,	 		$_POST['person_email'], 	$_POST['person_website'],
									$uploadedFileName, 		$_POST['person_desc'],	$_POST['person_comment'],	$_POST['person_status']);

	header('location: '. $redirectUrl. A_EDIT. '='. $retVal);
	exit();
}

#====================================================================================================
#	Delete report details into database
#----------------------------------------------------------------------------------------------------
elseif($Action == A_DELETE && $_POST['Submit'] == $lang['Delete'])
 {
	@unlink($physical_path[IMG_TESTIMONIALS]. $_POST['person_pic_del']);
	@unlink($physical_path[IMG_TESTIMONIALS]. 'thumb_'. $_POST['person_pic_del']);

	$testimonial->person_id = $_POST['person_id'];

	$retVal = $testimonial->Delete();
	
	header('location: '. $redirectUrl. A_DELETE. '='. $retVal);
	exit();
 }
elseif($Action == A_DELETE_SELECTED)
{
	# Delete selected
	$personlist = implode(',',$_POST['person_list']);
	$testimonial->person_id = $personlist;

	$testimonial->ViewByID();

	while($db->next_record())
	{
		@unlink($physical_path[IMG_TESTIMONIALS]. $db->f('person_pic'));
		@unlink($physical_path[IMG_TESTIMONIALS]. 'thumb_'. $db->f('person_pic'));
	}
	
	$retVal = $testimonial->Delete();

	header('location: '. $redirectUrl. A_DELETE. '='. $retVal);
	exit();
}

//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0; 
else
	$start_record = $_GET['start']; 

#====================================================================================================
#	List all reports
#----------------------------------------------------------------------------------------------------
if($Action == A_VIEW_ALL)
{
	# Display required message
	if($_GET[A_ADD])
		$SuccMessage	=	$lang['Msg_Testimonial_Added'];
	elseif($_GET[A_EDIT])
		$SuccMessage	=	$lang['Msg_Testimonial_Updated'];
	elseif($_GET[A_DELETE])
		$SuccMessage	=	$lang['Msg_Testimonial_Deleted'];

	$tpl->assign(array("T_Body"			=>	'testimonial_showall'. $config['tplEx'],
						"JavaScript"	=>  array("testimonial.js"),
						"A_Action"		=>  "testimonial.php?&start=".$startrecord,
						"SuccMessage"	=>	$SuccMessage,
					));
	
	$marketing_tool->page_size		=	15;
	$marketing_tool->start_record	=	$startrecord;

	$tpl->assign(array(	"ACTION"				=>	A_VIEW_ALL,
						"Add_Action"			=>	A_ADD,
						"A_Edit"				=>	A_EDIT,
						"A_Delete"				=>	A_DELETE,
						"A_View"				=>	A_VIEW,
						"A_DelSelected"			=>	A_DELETE_SELECTED,
						"PersonData"			=>	$testimonial->ViewAll(),
						"Testimonial_Path"		=>	$virtual_path[IMG_TESTIMONIALS],
						"Page_Link"				=>	($person->total_record > $person->page_size)? "Page : ". generate_pagination($person->total_record, $person->page_size, $person->start_record, $add_prevnext_text = TRUE) : '',
					));

	if($lng=='en')					
		$tpl->assign(array(	"Default_Picture"		=>	'img_optional_en.gif',
						));	
	else
		$tpl->assign(array(	"Default_Picture"		=>	'img_optional_sp.gif',
						));	

	$tpl->assign(array(	"Name"				=>  $lang['Name'],
						"Photo"				=>  $lang['Photo'],			
						"Comment"			=>  $lang['Comment'],			
						"L_Action"			=>  $lang['L_Action'],		
						"Add"				=>  $lang['add'],								
					));

	$tpl->assign(array(	"L_Testimonial_Manager"	=>  $lang['L_Testimonial_Manager'],
						"L_Manage_Testimonial"	=>  $lang['L_Manage_Testimonial'],			
						"L_Testimonial_Desc"	=>  $lang['L_Testimonial_Desc'],	
						"Display_On_Website"	=>  $lang['Display_On_Website'],	
						"Delete_Testimonial"	=>  $lang['Delete_Testimonial'],							
						"Delete_AllTestimonials"=>  $lang['Delete_AllTestimonials'],							
					));


}
else if($Action == A_VIEW)
{
		$tpl->assign(array(	"T_Body"			=>	'testimonial_show'.$config['tplEx'],
							"JavaScript"		=>  array("testimonial.js"),
							"A_Action"  	   	=>  "testimonial.php?&start=$start_record",
							"ACTION"			=>	$Action,
							"A_View"			=>	A_VIEW,
							"Cancel_Action"		=>	$redirectUrl,
							));
	
		$tpl->assign(array(	"Testimonial_Header"	=>	$lang['View_Testimonial'],
							));


		$tpl->assign(array(	"Name"				=>  $lang['Name'],
							"Company"			=>  $lang['Company'],			
							"Address"			=>  $lang['address'],			
							"City"				=>  $lang['city'],			
							"State"				=>  $lang['state'],			
							"Country"			=>  $lang['country'],
							"L_Select_Country"	=>  $lang['L_Select_Country'],										
							"Zip"				=>  $lang['zip'],			
							"Phone"				=>  $lang['phoneno'],			
							"Fax"				=>  $lang['fax'],			
							"Toll_Free"			=>  $lang['Toll_Free'],			
							"Email"				=>  $lang['email'],			
							"Website"			=>  $lang['Website'],			
							"Photo"				=>  $lang['Photo'],			
							"Delete_Picture"	=>  $lang['Delete_Picture'],			
							"Description"		=>  $lang['Description'],			
							"Comment"			=>  $lang['Comment'],			
							"Display_On_Website"=>  $lang['Display_On_Website'],		
							"Back"				=>  $lang['Back'],							
							"Delete"			=>  $lang['Delete'],														
							"Cancel"			=>  $lang['cancel'],							
						));
							
		$record = $testimonial->ViewAll($_POST['person_id']);

		$tpl->assign(array(	"Person_Id"			=>	$db->f('person_id'),
							"Person_Name"		=>	stripslashes($db->f('person_name')),
							"Person_Company"	=>	stripslashes($db->f('person_company')),
							"Person_Address"	=>	stripslashes($db->f('person_address')),
							"Person_City"		=>	stripslashes($db->f('person_city')),
							"Person_State"		=>	stripslashes($db->f('person_state')),							
							"Person_Country"	=>	stripslashes($db->f('person_country')),														
							"Person_Zip"		=>	stripslashes($db->f('person_zip')),
							"Person_Phone"		=>	$db->f('person_phone'),
							"Person_Fax"		=>	$db->f('person_fax'),
							"Person_TollFree"	=>	$db->f('person_tollfree'),
							"Person_Email"		=>	stripslashes($db->f('person_email')),
							"Person_Website"	=>	stripslashes($db->f('person_website')),
							"Person_Desc"		=>	stripslashes($db->f('person_desc')),
							"Person_Comment"	=>	stripslashes($db->f('person_comment')),
							"Person_Pic"		=>	stripslashes($db->f('person_pic')),
							"Picture_Filename"	=>	$virtual_path[IMG_TESTIMONIALS].'thumb_'.stripslashes($db->f('person_pic')),
							"Person_Status"		=>	($db->f('person_status') == 1) ? YES : NO,
							));
}
#====================================================================================================
#	Add new report or update report
#----------------------------------------------------------------------------------------------------
elseif($Action == A_ADD || $Action == A_EDIT)
{
	if(DEBUG)
    {
		print "Action = ". $_POST['Action'];
		print "Action = ". $_POST['Action'];
	}

	$tpl->assign(array(	"T_Body"			=>	'testimonial_addedit'.$config['tplEx'],
						"JavaScript"		=>  array("testimonial.js"),
						"A_Action"  	   	=>  "testimonial.php?&start=$start_record",
						"ACTION"			=>	$Action,
						"Cancel_Action"		=>	$redirectUrl,
						));

	$tpl->assign(array(	"Name"				=>  $lang['Name'],
						"Company"			=>  $lang['Company'],			
						"Address"			=>  $lang['address'],			
						"City"				=>  $lang['city'],			
						"State"				=>  $lang['state'],			
						"Country"			=>  $lang['country'],
						"L_Select_Country"	=>  $lang['L_Select_Country'],										
						"Zip"				=>  $lang['zip'],			
						"Phone"				=>  $lang['phoneno'],			
						"Fax"				=>  $lang['fax'],			
						"Toll_Free"			=>  $lang['Toll_Free'],			
						"Email"				=>  $lang['email'],			
						"Website"			=>  $lang['Website'],			
						"Photo"				=>  $lang['Photo'],			
						"Delete_Picture"	=>  $lang['Delete_Picture'],			
						"Description"		=>  $lang['Description'],			
						"Comment"			=>  $lang['Comment'],			
						"Display_On_Website"=>  $lang['Display_On_Website'],			
						"Save"				=>  $lang['save'],			
						"Cancel"			=>  $lang['cancel'],			
					));

	$tpl->assign(array(	"Empty_Name"		=>  $lang['msg_Name'],
						"Empty_Company"		=>  $lang['msg_Company'],			
						"Empty_Address"		=>  $lang['msg_Address'],			
						"Empty_City"		=>  $lang['msg_City'],			
						"Empty_State"		=>  $lang['msg_State'],	
						"Empty_Country"		=>  $lang['msg_Country'],									
						"Valid_Zip"			=>  $lang['msg_validZip'],			
						"Empty_Phone"		=>  $lang['msg_compTelephone'],			
						"Valid_Phone"		=>  $lang['msg_validTelephone'],									
						"Empty_Fax"			=>  $lang['msg_Fax'],			
						"Valid_Fax"			=>  $lang['msg_validFax'],			
						"Empty_TollFree"	=>  $lang['msg_ToolFree'],			
						"Valid_TollFree"	=>  $lang['msg_validToolFree'],									
						"Empty_Email"		=>  $lang['msg_Email'],		
						"Valid_Email"		=>  $lang['msg_validEmail'],								
						"Empty_Website"		=>  $lang['Website'],			
						"Valid_URL"			=>  $lang['msg_validURL'],			
						"Valid_Photo"		=>  $lang['msg_validPhoto'],			
						"Empty_Description"	=>  $lang['msg_Description'],			
						"Empty_Comment"		=>  $lang['msg_Comment'],
						"Delete_Testimonial"=>  $lang['Delete_Testimonial'],
						"Delete_AllTestimonials"=>  $lang['Delete_AllTestimonials'],															
					));

	$record  		= Country_List();
	$countryList	= fillDbCombo($record, "country_name", "country_name", $country_id);

	$tpl->assign(array(	'CountryList'			=>	$countryList,
						));

	#====================================================================================================
	#	Add new report
	#----------------------------------------------------------------------------------------------------
	if($Action == A_ADD)
	{
		$tpl->assign(array(	"Testimonial_Header"	=>	$lang['Add_Testimonial'],
							"Yes_Checked"			=>	"checked",
							"State_List"			=>	fillArrayCombo($lang['State_List']),
							));


		if($lng=='en')					
	    	$tpl->assign(array(	"Picture_Filename"		=>	$virtual_path[IMG_TESTIMONIALS].'img_optional_en.gif',
							));	
		else
			$tpl->assign(array(	"Picture_Filename"		=>	$virtual_path[IMG_TESTIMONIALS].'img_optional_sp.gif',
							));	
							
	}
	#====================================================================================================
	#	Modify report details
	#----------------------------------------------------------------------------------------------------
	else
	{
		$tpl->assign(array(	"Testimonial_Header"	=>	$lang['Edit_Testimonial'],
							));

		$record = $testimonial->ViewAll($_POST['person_id']);

		$phoneareacode 		= split(")",stripslashes($db->f('person_phone')));
		$phoneno 			= split("-",$phoneareacode[1]);
		$phoneareacode1 	= substr($phoneareacode[0],1,strlen($phoneareacode[0]));

		$faxareacode 		= split(")",stripslashes($db->f('person_fax')));
		$faxno 				= split("-",$faxareacode[1]);
		$faxareacode1 		= substr($faxareacode[0],1,strlen($faxareacode[0]));

		$tollfreeareacode 	= split(")",stripslashes($db->f('person_tollfree')));
		$tollfreeno 		= split("-",$tollfreeareacode[1]);
		$tollfreeareacode1 	= substr($tollfreeareacode[0],1,strlen($tollfreeareacode[0]));


		if($lng=='en')					
	    {
			$PictureFilename = 'img_optional_en.gif';
		}
		else
	    {	
			$PictureFilename = 'img_optional_sp.gif';
		}
			

		$tpl->assign(array(	"Person_Id"					=>	$db->f('person_id'),
							"Person_Name"				=>	stripslashes($db->f('person_name')),
							"Person_Company"			=>	stripslashes($db->f('person_company')),
							"Person_Address"			=>	stripslashes($db->f('person_address')),
							"Person_City"				=>	stripslashes($db->f('person_city')),
							"Person_State"				=>	stripslashes($db->f('person_state')),							
							"Person_Zip"				=>	stripslashes($db->f('person_zip')),
							"Person_Phone_Areacode"		=>	$phoneareacode1,
							"Person_Phone_Citycode"		=>	$phoneno[0],
							"Person_Phone_No"			=>	$phoneno[1],
							"Person_Fax_Areacode"		=>	$faxareacode1,
							"Person_Fax_Citycode"		=>	$faxno[0],
							"Person_Fax_No"				=>	$faxno[1],
							"Person_TollFree_Areacode"	=>	$tollfreeareacode1,
							"Person_TollFree_Citycode"	=>	$tollfreeno[0],
							"Person_TollFree_No"		=>	$tollfreeno[1],
							"Person_Email"				=>	stripslashes($db->f('person_email')),
							"Person_Website"			=>	stripslashes($db->f('person_website')),
							"Person_Desc"				=>	stripslashes($db->f('person_desc')),
							"Person_Comment"			=>	stripslashes($db->f('person_comment')),
							"Person_Pic"				=>	stripslashes($db->f('person_pic')),
							"Picture_Filename"			=>	($db->f('person_pic') != '') ? $virtual_path[IMG_TESTIMONIALS].'thumb_'.stripslashes($db->f('person_pic')) : $virtual_path[IMG_TESTIMONIALS].$PictureFilename,
							"Is_Status_Check"			=>	($db->f('person_status') == 1) ? "checked" : "",
							));

		$record  		= Country_List();
		$countryList	= fillDbCombo($record, "country_name", "country_name", $db->f('person_country'));

		$tpl->assign(array(	'CountryList'			=>	$countryList,
							));
	
	}
}
else if($Action == A_DELETE)
{
		$tpl->assign(array(	"T_Body"			=>	'testimonial_show'.$config['tplEx'],
							"JavaScript"		=>  array("testimonial.js"),
							"A_Action"  	   	=>  "testimonial.php?&start=$start_record",
							"ACTION"			=>	$Action,
							"A_View"			=>	A_VIEW,
							"Cancel_Action"		=>	$redirectUrl,
							));
	
		$tpl->assign(array(	"Testimonial_Header"	=>	$lang['View_Testimonial'],
							));

		$record = $testimonial->ViewAll($_POST['person_id']);

		$tpl->assign(array(	"Name"				=>  $lang['Name'],
							"Company"			=>  $lang['Company'],			
							"Address"			=>  $lang['address'],			
							"City"				=>  $lang['city'],			
							"State"				=>  $lang['state'],			
							"Country"			=>  $lang['country'],
							"L_Select_Country"	=>  $lang['L_Select_Country'],										
							"Zip"				=>  $lang['zip'],			
							"Phone"				=>  $lang['phoneno'],			
							"Fax"				=>  $lang['fax'],			
							"Toll_Free"			=>  $lang['Toll_Free'],			
							"Email"				=>  $lang['email'],			
							"Website"			=>  $lang['Website'],			
							"Photo"				=>  $lang['Photo'],			
							"Delete_Picture"	=>  $lang['Delete_Picture'],			
							"Description"		=>  $lang['Description'],			
							"Comment"			=>  $lang['Comment'],			
							"Display_On_Website"=>  $lang['Display_On_Website'],
							"Back"				=>  $lang['back'],							
							"Delete"			=>  $lang['Delete'],														
							"Cancel"			=>  $lang['cancel'],							
						));

		$tpl->assign(array(	"Person_Id"			=>	$db->f('person_id'),
							"Person_Name"		=>	stripslashes($db->f('person_name')),
							"Person_Company"	=>	stripslashes($db->f('person_company')),
							"Person_Address"	=>	stripslashes($db->f('person_address')),
							"Person_City"		=>	stripslashes($db->f('person_city')),
							"Person_State"		=>	stripslashes($db->f('person_state')),							
							"Person_Country"	=>	stripslashes($db->f('person_country')),														
							"Person_Zip"		=>	stripslashes($db->f('person_zip')),
							"Person_Phone"		=>	$db->f('person_phone'),
							"Person_Fax"		=>	$db->f('person_fax'),
							"Person_TollFree"	=>	$db->f('person_tollfree'),
							"Person_Email"		=>	stripslashes($db->f('person_email')),
							"Person_Website"	=>	stripslashes($db->f('person_website')),
							"Person_Desc"		=>	stripslashes($db->f('person_desc')),
							"Person_Comment"	=>	stripslashes($db->f('person_comment')),
							"Person_Pic"		=>	stripslashes($db->f('person_pic')),
							"Picture_Filename"	=>	$virtual_path[IMG_TESTIMONIALS].'thumb_'.stripslashes($db->f('person_pic')),
							"Person_Status"		=>	($db->f('person_status') == 1) ? YES : NO,
							));
}

$tpl->display('default_layout'. $config['tplEx']);
?>