<?php
#====================================================================================================
# File Name : siteconfig.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
#define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

include_once("../includes/common.php");
include($physical_path['DB_Access']. 'Page.php');

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Config information
#-----------------------------------------------------------------------------------------------------------------------------
if($_POST['Submit'] == $lang['update'])
{
	$webConf->Set(WC_COMPANY_TITLE, 	$_POST[WC_COMPANY_TITLE]);
	$webConf->Set(WC_SITE_TITLE, 		$_POST[WC_SITE_TITLE]);
	$webConf->Set(WC_META_TITLE, 		$_POST[WC_META_TITLE]);
	$webConf->Set(WC_META_KEYWORD, 		$_POST[WC_META_KEYWORD]);
	$webConf->Set(WC_META_DESCRIPTION, 	$_POST[WC_META_DESCRIPTION]);
	$webConf->Set(WC_COPYRIGHT_TEXT, 	$_POST[WC_COPYRIGHT_TEXT]);
	$webConf->Set(WC_SUPPORT_EMAIL, 	$_POST[WC_SUPPORT_EMAIL]);
	$webConf->Set(WC_PAYPAL_EMAIL, 		$_POST[WC_PAYPAL_EMAIL]);	
	//$webConf->Set(WC_PAGESIZE, 			$_POST[WC_PAGESIZE]);

	if($_POST['updatemeta']=='yes')
	{
		# Initialize object
		$page = new Page();
		
		$page->updateMeta($_POST[WC_META_TITLE], $_POST[WC_META_TITLE], $_POST[WC_META_KEYWORD], $_POST[WC_META_DESCRIPTION]);
	}
	
	header('location: siteconfig.php?update=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
else if($_POST['Submit'] == $lang['cancel'])
{
	header('location: index.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------

if($_GET['update']==true)
{
	$succMessage = $lang['Site_Config_Updated'];
}

	$tpl->assign(array( "T_Body"		=>	'siteconfig'. $config['tplEx'],
						"JavaScript"	=>  array("siteconfig.js"),
						"A_Action"		=>	"siteconfig.php",
						"succMessage"	=>	$succMessage)
						);
	
	$tpl->assign(array( "SrNo"			=> $lang['SrNo'],
						"L_Name"		=> $lang['name'],
						"L_User_Name"	=> $lang['L_User_Name'],
						"L_Email"		=> $lang['email'],
						"Action"		=> $lang['Action'],
						));


	$tpl->assign(array( "Update"				=> $lang['update'],
						"Cancel"				=> $lang['cancel'],
						"L_Site_Config"			=> $lang['L_Site_Config'],
						"L_Site_ConfigDesc"		=> $lang['L_Site_ConfigDesc'],
						"L_Mandatory_Fields"	=> $lang['L_Mandatory_Fields'],
						"L_Comp_Info"			=> $lang['L_Comp_Info'],
						"L_Company_Name"		=> $lang['L_Company_Name'],
						"L_Max100_Chars"		=> $lang['L_Max100_Chars'],
						"L_Max400_Chars"		=> $lang['L_Max400_Chars'],
						"L_Max1000_Chars"		=> $lang['L_Max1000_Chars'],												
						"L_Site_Title"			=> $lang['L_Site_Title'],
						"L_Copyright_Text"		=> $lang['L_Copyright_Text'],
						"L_Support_Email"		=> $lang['L_Support_Email'],
						"L_Support_EmailDesc"	=> $lang['L_Support_EmailDesc'],
						"L_Paypal_Email"		=> $lang['L_Paypal_Email'],
						"L_Paypal_EmailDesc"	=> $lang['L_Paypal_EmailDesc'],
						"L_Meta_Info"			=> $lang['L_Meta_Info'],
						"L_Meta_Title"			=> $lang['L_Meta_Title'],
						"L_Meta_TitleDesc"		=> $lang['L_Meta_TitleDesc'],
						"L_Meta_Description"	=> $lang['L_Meta_Description'],
						"L_Meta_DescriptionDesc"=> $lang['L_Meta_DescriptionDesc'],
						"L_Meta_Keyword"		=> $lang['L_Meta_Keyword'],
						"L_Meta_KeywordDesc"	=> $lang['L_Meta_KeywordDesc'],
						));

	
	$tpl->assign(array( WC_COMPANY_TITLE		=>	$config[WC_COMPANY_TITLE],
						WC_SITE_TITLE			=>	$config[WC_SITE_TITLE],
						WC_COPYRIGHT_TEXT		=>	$config[WC_COPYRIGHT_TEXT],
						WC_META_TITLE			=>	$config[WC_META_TITLE],
						WC_META_KEYWORD			=>	$config[WC_META_KEYWORD],
						WC_META_DESCRIPTION		=>	$config[WC_META_DESCRIPTION],
						WC_SUPPORT_EMAIL		=>	$config[WC_SUPPORT_EMAIL],
						WC_PAYPAL_EMAIL			=>	$config[WC_PAYPAL_EMAIL],					
						//WC_PAGESIZE				=>	$utility->fillArrayCombo($lang['PageSize_List'], $config[WC_PAGESIZE]),
						));
	
	$tpl->display('default_layout'. $config['tplEx']);

?>