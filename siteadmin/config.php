<?php
#====================================================================================================
# File Name : config.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
#define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

include_once("../includes/common.php");

#---------------------------------------------------------------------------------------------------
#	Check for proper configuration key
#---------------------------------------------------------------------------------------------------
$configKey = $_GET['config']?$_GET['config']:$_POST['config'];

if(!array_key_exists($configKey, $webConf->Data))
{
	header('location: index.php');
	exit;
}

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Config information
#-----------------------------------------------------------------------------------------------------------------------------
if($_POST['Submit'] == "Save")
{
	$webConf->Save($configKey, $_POST);
	header("location: config.php?config=$configKey&update=true");
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
else if($_POST['Submit'] == "Cancel")
{
	header('location: index.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------

if($_GET['update']==true)
	$succMessage = $webConf->Data['SM_'.$configKey];

$tpl->assign(array("T_Body"			=>	'custom_addedit'. $config['tplEx'],
					"A_Action"		=>	"config.php?config=".$configKey,
					'P_Upload_Root'	=>	$webConf->Data['P_Upload'],
					'V_Upload_Root'	=>	$webConf->Data['V_Upload'],
					'L_Module'		=>	$webConf->Data['L_'.$configKey],
					'H_HelpText'	=>	$webConf->Data['H_'.$configKey],
					'F_FieldInfo'	=>	$webConf->Data[$configKey],
					"succMessage"	=>	$succMessage,
					));

$tpl->display('default_layout'. $config['tplEx']);
?>