<?php
#====================================================================================================
# File Name : login.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_ADMIN', 	true);

include_once("../includes/common.php");

if($_POST['Submit'] == 'Login')
{
	if($user->IsValidLogin($_POST['username'], $_POST['password'], ADMIN))
	{
		$user->doLogin($_POST['username'], ADMIN);
		header("location: index.php");
		exit(0);
	}
	$db->free();
	$Error_Message = "Invalid Username / Password!!!";
}

if($user->User_Id != '')	header("location: ./index.php");

$tpl->assign(array("T_Body"				=>	'login'. $config['tplEx'],
					"JavaScript"		=>  array("login.js"),
					"A_Login"			=>	"login.php",
					"Error_Message"		=>	$Error_Message));

$tpl->display('default_layout'. $config['tplEx']);
?>