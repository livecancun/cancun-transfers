<?php
#====================================================================================================
# File Name : profile.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
#define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

include_once("../includes/common.php");

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : 'View');

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Config information
#-----------------------------------------------------------------------------------------------------------------------------
if($_POST['Submit'] == "Update")
{
	$retVal = $usrInfo->UpdateProfile($user->User_Id,
										$_POST['admin_firstname'],
										$_POST['admin_lastname'],
										$_POST['admin_phone_areacode']. '-'. $_POST['admin_phone_citycode']. '-'. $_POST['admin_phone_no'],
										$_POST['admin_email']);

	header('location: profile.php?update=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
else if($_POST['Submit'] == "Cancel")
{
	header('location: index.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------

if($_GET['update']==true)
	$succMessage = "Profile has been updated successfully!!";

$tpl->assign(array("T_Body"			=>	'subadmin_profile'. $config['tplEx'],
					"JavaScript"	=>  array("subadmin.js"),
					"A_Action"		=>	"profile.php",
					"succMessage"	=>	$succMessage));
				
$rst = $usrInfo->getAdmin($user->User_Id);

$admin_phone	=	ereg_replace('[-()\ ]', '', $db->f('admin_phone'));

$tpl->assign(array("admin_firstname"		=>	$db->f('admin_firstname'),
					"admin_lastname"		=>	$db->f('admin_lastname'),
					"admin_email"			=>	$db->f('admin_email'),
					"admin_phone_areacode"	=>	substr($admin_phone, 0,3),
					"admin_phone_citycode"	=>	substr($admin_phone, 3,3),
					"admin_phone_no"		=>	substr($admin_phone, 6,4),
					));

$tpl->display('default_layout'. $config['tplEx']);
?>