<?php
#====================================================================================================
# File Name : destination.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------

define('IN_ADMIN', 	true);

include_once("../includes/common.php");
include($physical_path['DB_Access']. 'Destination.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : 'ShowAll');

//echo $Action,  $_POST['SubAction'];

# Initialize object
$dest = new Destination();

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Add destination
#-----------------------------------------------------------------------------------------------------------------------------
if($Action == 'Add' && $_POST['Submit'] == 'Save')
{
	$ret = $dest->Insert($_POST['dest_title']);

	header('location: destination.php?add=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Edit' && $_POST['Submit'] == 'Save')
{
	//print_r($_POST); die;
	$ret = $dest->Update($_POST['dest_id'], $_POST['dest_title']);

	header('location: destination.php?edit=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Delete')
{
	$ret = $dest->Delete($_POST['dest_id']);

	header('location: destination.php?delete=true');
	exit();
}
#====================================================================================================
#	Deleteselected Contact
#----------------------------------------------------------------------------------------------------
if ($Action=='DeleteSelected')
{
	$ret=$dest->Delete(implode(',',$_POST['dest_id1']))?'true':'false';
	header("location: destination.php?Action=Manage&deleteSelected=true");		
}

#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Sort' && $_POST['Submit'] == 'Save')
{
	$ret = $destination->Sort($_POST['destination_order']);
	header('location: destination.php?sort=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Toggle Display
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'ChangeStatus')
{
	$ret = $dest->ToggleStatus($_POST['dest_id'], $_POST['status']);
	header('location: destination.php?status='. $_POST['status']);
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
elseif($_POST['Submit'] == "Cancel")
{
	header('location: destination.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Show destination list
#-----------------------------------------------------------------------------------------------------------------------------
if(!in_array($Action, array('Add', 'Edit', 'Sort', 'View')))
{
	if($_GET['add']==true)
		$succMessage = "Destination content has been added successfully!!";
	elseif($_GET['save']==true)
		$succMessage = "Destination content has been updated successfully!!";
	elseif($_GET['delete']==true)
		$succMessage = "Destination has been deleted successfully!!";
	elseif($_GET['deleteSelected']==true)
		$succMessage = "Selected Destination has been deleted successfully!!";
	elseif($_GET['sort']==true)
		$succMessage = "Destination order has been set successfully!!";
	elseif($_GET['status']==true)
		$succMessage = "Destination status has been changed successfully!!";
	
	$tpl->assign(array("T_Body"				=>	'destination_manage'. $config['tplEx'],
						"JavaScript"		=>  array("destination.js"),
						"succMessage"		=>	$succMessage,
						"Action"			=>	$Action,
						"DestinationInfo"	=>	$dest->ViewAll(),
						));
						
	$tpl->assign(array(	"L_Destination_Manager"	 =>	$lang['L_Destination_Manager'],
						"L_Manage_Destinations"	 => $lang['L_Manage_Destinations'],
						"L_Destination_Title"  	 => $lang['L_Destination_Title'],
						"L_Action"  	 	 	 => $lang['L_Action'],
						"L_Visible"		 		 => $lang['L_Visible'],
						));
						
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Add/Edit Destination
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Add' || $Action == 'Edit')
{
	$tpl->assign(array("T_Body"			=>	'destination_addedit'. $config['tplEx'],
						"JavaScript"	=>  array("destination.js"),
						"Action"		=>	$Action,
						"Language"		=>	$LangInfo,
						));

	$tpl->assign(array(	"L_Destination_Manager"	=>	$lang['L_Destination_Manager'],
						"L_Design_Content"		=>	$lang['L_Design_Content'],
						"L_Destination_Title"  	 => $lang['L_Destination_Title'],						
						"L_Mandatory_Fields"	=>	$lang['L_Mandatory_Fields'],
						"L_Destination_Info"	=>	$lang['L_Destination_Info'],						
						));

	if($Action == 'Edit')
	{

		$rsDestination = $dest->getDestination($_POST['dest_id']);
	
		$tpl->assign(array("dest_id"				=>	$rsDestination['dest_id'],
							"rsDestination"				=>	$rsDestination,
							));
							
	 }
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Add/Edit Destination
#-----------------------------------------------------------------------------------------------------------------------------
/*elseif($Action == 'Sort')
{
	$tpl->assign(array("T_Body"			=>	'destination_sort'. $config['tplEx'],
						"JavaScript"	=>  array("destination.js"),
						"succMessage"	=>	$succMessage,
						"Action"		=>	$Action,
						));

	$tpl->assign(array("DestinationInfo"		=>	$destination->ViewAll(PAGE_VISIBLE),
						));
}*/
$tpl->display('default_layout'. $config['tplEx']);
?>