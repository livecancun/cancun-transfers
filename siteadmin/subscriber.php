<?php
#====================================================================================================
# File Name : subscriber.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
#define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

if($_GET['Action'] == 'View' || $_GET['Action'] == 'List')
	define('POPUP_WIN', true);

include_once("../includes/common.php");
include($physical_path['DB_Access']. 'Subscriber.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : 'ViewAll');

# Initialize object
$subscriber = new Subscriber();

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Add page
#-----------------------------------------------------------------------------------------------------------------------------
if($Action == 'Add' && $_POST['Submit'] == 'Save')
{
	if (!$subscriber->IsExists($_POST['subscriber_email']))
	{
		$ret = $subscriber->Insert(	$_POST['subscriber_email'],	$_POST['subscriber_status']);
		header('location: subscriber.php?add=true');
	}
	else
		header('location: subscriber.php?add=false');
	
	exit();
}

#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Edit' && $_POST['Submit'] == 'Save')
{
	$ret = $subscriber->Update(		$_POST['subscriber_id'],	$_POST['subscriber_email'],
									$_POST['subscriber_status']);

	header('location: subscriber.php?save=true');
	exit();
}

#-----------------------------------------------------------------------------------------------------------------------------
#	Delete subscriber
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Delete')
{
	$ret = $subscriber->Delete($_POST['subscriber_id']);

	header('location: subscriber.php?delete=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Delete subscriber
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == "DeleteSelected")
{
	$ret = $subscriber->Delete($_POST['subscriber_list']);
	
	header('location: subscriber.php?deletesel=true');
	exit();
}

#-----------------------------------------------------------------------------------------------------------------------------
#	Toggle Display
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'ChangeStatus')
{
	$ret = $subscriber->ToggleStatus($_POST['subscriber_id'], $_POST['subscriber_status']);
	header('location: subscriber.php?status=true');
	exit();
}

#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
elseif($_POST['Submit'] == "Cancel")
{
	header('location: subscriber.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Show page list
#-----------------------------------------------------------------------------------------------------------------------------
if(!in_array($Action, array('Add', 'Edit', 'View', 'List')))
{
	if($_GET['add']== 'true')
		$succMessage = "Subscriber has been added successfully!!";
	if($_GET['add']== 'false')
		$succMessage = "Subscriber email already exists!!";		
	elseif($_GET['save']== 'true')
		$succMessage = "Subscriber content has been updated successfully!!";
	elseif($_GET['delete']== 'true')
		$succMessage = "Subscriber has been deleted successfully!!";
	elseif($_GET['deletesel']== 'true')
		$succMessage = "Selected subscribers have been deleted successfully!!";
	elseif($_GET['status']== 'true')
		$succMessage = "Subscriber status has been changed successfully!!";
		
	$tpl->assign(array( "T_Body"		=>	'subscriber_manage'. $config['tplEx'],
						"JavaScript"	=>  array("subscriber.js"),
						"succMessage"	=>	$succMessage,
						"Action"		=>	$Action,
						"SubscriberInfo"=>	$subscriber->ViewAll(),
						"PageSize"		=>	$lang['PageSize_List'],
						"total_record"	=>	$subscriber->total_record,
						));
}

#-----------------------------------------------------------------------------------------------------------------------------
#	Add/Edit Page
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Add' || $Action == 'Edit')
{
	$tpl->assign(array( "T_Body"		=>	'subscriber_addedit'. $config['tplEx'],
						"JavaScript"	=>  array("subscriber.js"),
						"Action"		=>	$Action,
						"YesNo_List"	=>	array("1" => "Yes", "0" => "No"),
						));
						
	if($Action == 'Edit')
	{
		$rsSubscriber = $subscriber->getInfoById($_POST['subscriber_id']);
		
		$tpl->assign(array( "Subscriber"	=>	$rsSubscriber,
							));
							
	}
}

#-----------------------------------------------------------------------------------------------------------------------------
#	Show Page
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'View')
{
	$tpl->assign(array( "T_Body"		=>	'subscriber_view'. $config['tplEx'],
						"JavaScript"	=>  array("subscriber.js"),
						"User"			=>  $subscriber->getInfoById($_GET['subscriber_id']),
						));
}
#-----------------------------------------------------------------------------------------------------------------------------
#	List 
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action = 'List')
{
	$tpl->assign(array( "T_Body"		=>	'subscriber_select'. $config['tplEx'],
						"JavaScript"	=>  array("custom.js"),
						"SubscriberInfo"=>	$subscriber->ViewAll("AND subscriber_status=1"),
						"PageSize"		=>	$lang['PageSize_List'],
						"total_record"	=>	$subscriber->total_record,
						));
}

$tpl->display('default_layout'. $config['tplEx']);
?>