<?php
#====================================================================================================
# File Name : triptype.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
#define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

include_once("../includes/common.php");
include($physical_path['DB_Access']. 'TripType.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : $lang['L_ShowAll']);

//echo $Action,  $_POST['SubAction'];

# Initialize object
$triptype = new TripType();

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Add triptype
#-----------------------------------------------------------------------------------------------------------------------------
if($Action == 'Add' && $_POST['Submit'] == 'Save')
{
	$ret = $triptype->Insert($_POST['triptype_title']);

	header('location: triptype.php?add=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Edit' && $_POST['Submit'] == 'Save')
{
	//print_r($_POST); die;
	$ret = $triptype->Update($_POST['triptype_id'], $_POST['triptype_title']);

	header('location: triptype.php?edit=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Delete')
{
	$ret = $triptype->Delete($_POST['triptype_id']);

	header('location: triptype.php?delete=true');
	exit();
}
#====================================================================================================
#	Deleteselected Contact
#----------------------------------------------------------------------------------------------------
if ($Action=='DeleteSelected')
{
	$ret=$triptype->Delete(implode(',',$_POST['triptype_id1']))?'true':'false';
	header("location: triptype.php?Action=Manage&deleteSelected=true");		
}

#-----------------------------------------------------------------------------------------------------------------------------
#	Update Content
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Sort' && $_POST['Submit'] == 'Save')
{
	$ret = $triptype->Sort($_POST['triptype_order']);
	header('location: triptype.php?sort=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Toggle Display
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'ChangeStatus')
{
	$ret = $triptype->ToggleStatus($_POST['triptype_id'], $_POST['status']);
	header('location: triptype.php?status='. $_POST['status']);
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
elseif($_POST['Submit'] == "Cancel")
{
	header('location: triptype.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Show triptype list
#-----------------------------------------------------------------------------------------------------------------------------
if(!in_array($Action, array('Add', 'Edit', 'Sort', 'View')))
{
	if($_GET['add']==true)
		$succMessage = "Trip Type content has been added successfully!!";
	elseif($_GET['save']==true)
		$succMessage = "Trip Type content has been updated successfully!!";
	elseif($_GET['delete']==true)
		$succMessage = "Trip Type has been deleted successfully!!";
	elseif($_GET['deleteSelected']==true)
		$succMessage = "Selected Trip Type has been deleted successfully!!";
	elseif($_GET['sort']==true)
		$succMessage = "Trip Type order has been set successfully!!";
	elseif($_GET['status']==true)
		$succMessage = "Trip Type status has been changed successfully!!";
	
	$tpl->assign(array("T_Body"				=>	'triptype_manage'. $config['tplEx'],
						"JavaScript"		=>  array("triptype.js"),
						"succMessage"		=>	$succMessage,
						"Action"			=>	$Action,
						"TripTypeInfo"		=>	$triptype->ViewAll(),
						));

	$tpl->assign(array(	"L_TripType_Manager"	=>	$lang['L_TripType_Manager'],
						"L_Manage_TripType"	 	=> $lang['L_Manage_TripType'],
						"L_TripType"  	 		=> $lang['L_TripType'],
						"L_Action"  	 	 	=> $lang['L_Action'],
						"L_Visible"		 		=> $lang['L_Visible'],
						));
						

}
#-----------------------------------------------------------------------------------------------------------------------------
#	Add/Edit TripType
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Add' || $Action == 'Edit')
{
	$tpl->assign(array("T_Body"			=>	'triptype_addedit'. $config['tplEx'],
						"JavaScript"	=>  array("triptype.js"),
						"Action"		=>	$Action,
						"Language"		=>	$LangInfo,
						));

	$tpl->assign(array(	"L_TripType_Manager"	=>	$lang['L_TripType_Manager'],
						"L_Manage_TripType"	 	=>  $lang['L_Manage_TripType'],
						"L_Design_Content"		=>	$lang['L_Design_Content'],
						"L_TripType"  	 		=>  $lang['L_TripType'],
						"L_Mandatory_Fields"	=>	$lang['L_Mandatory_Fields'],	
						"L_TripType_Info"		=>	$lang['L_TripType_Info'],							
						));

	if($Action == 'Edit')
	{

		$rsTripType = $triptype->getTripType($_POST['triptype_id']);
	
		$tpl->assign(array("triptype_id"	=>	$rsTripType['triptype_id'],
							"rsTripType"	=>	$rsTripType,
							));
							
	 }
}

	$tpl->display('default_layout'. $config['tplEx']);
	
?>