<?php
#====================================================================================================
# File Name : home.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
# define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

if($_GET['Action'] == 'PictureGallery')
	define('POPUP_WIN', 	true);

include_once("../includes/common.php");
include_once($physical_path['DB_Access']. 'Fsbo.php');
include_once($physical_path['DB_Access']. 'Property.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : 'ViewAll');
$Action2 = isset($_GET['Action2']) ? $_GET['Action2'] : (isset($_POST['Action2']) ? $_POST['Action2'] : '');
$home_id = isset($_GET['home_id']) ? $_GET['home_id'] : $_POST['home_id']; 

# Initialize object
$fsbo = new Fsbo();
$Home = new Property();

# redirect Page
$redirectUrl = "home.php?";

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Add fsbo
#-----------------------------------------------------------------------------------------------------------------------------
if($Action == 'Add' && $_POST['Submit'] == 'Save')
{	
	$home_user_id = $user->User_Id;
		
	if (is_array($_POST['home_feature']))
		$home_feature = implode(",",$_POST['home_feature']);	
		
	$home_int_sqft =  $_POST['home_int_sqft'] . ":" . $_POST['home_sqft_type'];
	$home_submit_date = $utility->formateTime(date("Y-m-d"));	
	
	if($_POST['exp_date'] != "")
	{		
		list($month,$date,$year) = explode("-",$_POST['exp_date']);
		$exp_date = $year . "-". $month ."-". $date;
	}
	else
	{
		$exp_date = date("Y-m-d");
	}	
	$home_expire_date = $utility->formateTime($exp_date);	
	
	$retVal = $Home->InsertHomeInfo(	$home_user_id,				$_POST['home_address'],
										$_POST['home_city'],		$_POST['home_zip'],
										$_POST['home_state'],		$_POST['home_mls'],
										$_POST['home_type'],		$_POST['home_style'],
										$_POST['home_bedroom'],		$_POST['home_bathroom'],		
										$_POST['home_parking'],		$home_int_sqft,
										$_POST['home_lotsize'],		$_POST['home_build_year'],		
										$_POST['home_price'],		$home_feature,		
										$_POST['home_fsbo_id'],		$home_submit_date,
										$home_expire_date,			$_POST['home_desc'],
										$_POST['home_status'],		$_POST['home_virtualtour']);

		header('location: ' . $redirectUrl . 'add=true');
		exit();
	
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Home info
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Edit' && $_POST['Submit'] == 'Save')
{
	$home_feature = implode(",",$_POST['home_feature']);
	$home_int_sqft =  $_POST['home_int_sqft'] . ":" . $_POST['home_sqft_type'];
	
	if($_POST['exp_date'] != "")
	{		
		list($month,$date,$year) = explode("-",$_POST['exp_date']);
		$exp_date = $year . "-". $month ."-". $date;
	}
	else
	{
		$exp_date = date("Y-m-d");
	}		
	
	$home_expire_date = $utility->formateTime($exp_date);	
	
	$retVal = $Home->UpdateHomeInfo(	$home_id,					$_POST['home_address'],
										$_POST['home_city'],		$_POST['home_zip'],
										$_POST['home_state'],		$_POST['home_mls'],
										$_POST['home_type'],		$_POST['home_style'],
										$_POST['home_bedroom'],		$_POST['home_bathroom'],		
										$_POST['home_parking'],		$home_int_sqft,
										$_POST['home_lotsize'],		$_POST['home_build_year'],		
										$_POST['home_price'],		$home_feature,		
										$_POST['home_fsbo_id'],		
										$home_expire_date,			$_POST['home_desc'],
										$_POST['home_status'],		$_POST['home_virtualtour']);

	header('location: ' . $redirectUrl . 'save=true');	
	exit();
}

#-----------------------------------------------------------------------------------------------------------------------------
#	Delete Home info
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == "Delete")
{	
	$ret = $Home->DeleteHomeInfo($home_id,$physical_path['Picture']);
	header('location: ' . $redirectUrl . 'delete=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Delete fsbo type
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == "DeleteSelected")
{		
	$ret = $Home->DeleteHomeInfo(implode(',', $_POST['home_list']),$physical_path['Picture']);
	header('location: ' . $redirectUrl . 'seldelete=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Toggle Authorization
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'ChangeStatus')
{
	$ret = $Home->ToggleAuthorization($home_id, $_POST['status']);
	header('location: ' . $redirectUrl . 'status='. $_POST['status']);
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Toggle Featured
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'ChangeFeatured')
{
	/*echo "feature:".$_POST['feature'];
	die;*/
	$ret = $Home->ToggleFeatured($home_id, $_POST['feature']);
	header('location: ' . $redirectUrl . 'status='. $_POST['feature']);
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
elseif($_POST['Submit'] == "Cancel")
{
	header('location: home.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Show all Home Info
#-----------------------------------------------------------------------------------------------------------------------------
if(!in_array($Action, array('Add', 'Edit', 'View', 'Picture', 'PictureGallery')))
{
	# Define required message
	if($_GET['add']==true)
		$succMessage = "Home information has been added successfully!!";
	elseif($_GET['save']==true)
		$succMessage = "Home information has been saved successfully!!";
	elseif($_GET['delete']==true)
		$succMessage = "Home information has been deleted successfully!!";
	elseif($_GET['seldelete']==true)
		$succMessage = "Selected Home information has been deleted successfully!!";
	elseif($_GET['status']==YES)
		$succMessage = "Property has been marked visible!!";
	elseif($_GET['status']==NO)
		$succMessage = "Property has been marked invisible!!";

	$tpl->assign(array("T_Body"			=>	'home_manage'. $config['tplEx'],
						"JavaScript"	=>  array("home.js"),
						"succMessage"	=>	$succMessage,
						"Picture_path"	=>	$virtual_path['Picture'],
						"Default_Pic"	=>	$config['picDefault'],
					));

	# Show all fsbo
	$tpl->assign(array("HomeList"		=>	$Home->ViewAllHomeInfo(),						
					));
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Add/Edit home info
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Add' || $Action == 'Edit')
{
	$tpl->assign(array("T_Body"					=>	'home_addedit'. $config['tplEx'],
						"JavaScript"			=>  array("home.js"),
						"A_Action"				=>	"home.php",						
						"Error_Message"			=>	$Error_Message,
						"Action"				=>	$Action,
						));

		# Get Home Type		
		$rsHomeType=$Home->ViewAllHomeType();		

		# Get Home Style		
		$rsHomeStyle=$Home->ViewAllHomeStyle();
		
		# Get Bedroom Type
		$rsBedroom=$Home->ViewAllBedroomType();		
		
		# Get Bathroom Type
		$rsBathroom=$Home->ViewAllBathroomType();		
		
		# Get Status Type
		$rsHomeStatus=$Home->ViewAllHomeStatus();		
		
		# Get Bathroom Type
		$rsState=$utility->ViewAllState();		
		
		# Get Parking Type
		$rsParking=$Home->ViewAllParkingType();		
				
		# Get Fsbo
		$rsFsbo=$fsbo->ViewAllFsbo();		
				
		$Home_Feature = array();

		# Get all features
		foreach($FeatureType as $key => $val)
		{
			$Home_Feature[$val] = $Home->ViewAllHomeFeature($key);
		}
	
	if($Action == 'Edit')
	{
		$rs = $Home->getHomeInfo($home_id);		
		$features = explode(",",$db->f("home_features"));		
		
		list($intsqft,$sqfttype) = explode(":",$db->f("home_int_sqft"));
		
		if ($sqfttype == "SqFeet")
			$SelectSqfeet = "selected";
		else
			$SelectAcres = "selected";
			
		$tpl->assign(array( "home_id"			=>	$home_id,
							"home_address"		=>	$db->f("home_address") . $sqfttype,
							"home_city"			=>	$db->f("home_city"),
							"State_List"		=>	$utility->fillComboFromRS($rsState,"state_alias","state_name",$db->f("home_state")),
							"home_zip"			=>	$db->f("home_zip"),
							
							"home_mls"			=>	$db->f("home_mls"),							
							"Home_Type"			=>	$utility->fillComboFromRS($rsHomeType,"type_id","type_title",$db->f("home_type")),
							"Home_Style"		=>	$utility->fillComboFromRS($rsHomeStyle,"style_id","style_title",$db->f("home_style")),
							"Home_Bedroom"		=>	$utility->fillComboFromRS($rsBedroom,"bedroom_id","bedroom_title",$db->f("home_bedroom")),
							"Home_Bathroom"		=>	$utility->fillComboFromRS($rsBathroom,"bathroom_id","bathroom_title",$db->f("home_bathroom")),
							"home_int_sqft"		=>	$intsqft,
							"home_lotsize"		=>	$db->f("home_lotsize"),
							"Home_Parking"		=>	$utility->fillComboFromRS($rsParking,"parking_id","parking_title",$db->f("home_parking")),
							"home_build_year"	=>	$db->f("home_build_year"),
							"home_price"		=>	$db->f("home_price"),
							
							"Home_Feature"		=>	$Home_Feature,
							"SelecteHome_Feature"	=>	$features,
							
							"Home_Fsbo"			=>	$utility->fillComboFromRS($rsFsbo,"fsbo_auth_id","fsbo_name",$db->f("home_fsbo_id")),
							"Home_Submit_Date"	=>	$utility->formatDate1($db->f("home_submit_date"), $formate='d M, Y'),							
							"exp_date"			=>	$utility->formatDate1($db->f("home_expire_date"), $formate='m-d-Y'),														
							"Home_Status"		=>	$utility->fillComboFromRS($rsHomeStatus,"status_id","status_title",$db->f("home_status")),
							"Home_Desc"			=>	$db->f("home_desc"),
							"Home_Virtualtour"	=>	$db->f("home_virtualtour"),					
							//"SelectSqfeet"		=>	$SelectSqfeet,
							//"SelectAcres"		=>	$SelectAcres,
						));
						
		
	 }
	 else
	 {		
		$tpl->assign(array( "Home_Type"			=>	$utility->fillComboFromRS($rsHomeType,"type_id","type_title",''),
							"Home_Style"		=>	$utility->fillComboFromRS($rsHomeStyle,"style_id","style_title",''),
							"Home_Bedroom"		=>	$utility->fillComboFromRS($rsBedroom,"bedroom_id","bedroom_title",''),
							"Home_Bathroom"		=>	$utility->fillComboFromRS($rsBathroom,"bathroom_id","bathroom_title",''),
							"Home_Status"		=>	$utility->fillComboFromRS($rsHomeStatus,"status_id","status_title",''),
							"State_List"		=>	$utility->fillComboFromRS($rsState,"state_alias","state_name",''),
							"Home_Feature"		=>	$Home_Feature,
							"Home_Parking"		=>	$utility->fillComboFromRS($rsParking,"parking_id","parking_title",''),
							"Home_Fsbo"			=>	$utility->fillComboFromRS($rsFsbo,"fsbo_auth_id","fsbo_name",''),
							"Home_Submit_Date"	=>	$utility->formatDate(date("Y-m-d"), $formate='d M, Y'),							
							//"SelecteHome_Feature"	=>	array(16,12,13),
						));			
	 }	
}
#-----------------------------------------------------------------------------------------------------------------------------
#	View Home Details
#-----------------------------------------------------------------------------------------------------------------------------
else if($Action == 'View')
{	
	$tpl->assign(array("T_Body"					=>	'home_view'. $config['tplEx'],
						"JavaScript"			=>  array("home.js"),
						"A_Action"				=>	"home.php",						
						"Error_Message"			=>	$Error_Message,
						"Action"				=>	$Action,
						));
										
	$Home_Feature = array();

	$rsHome = $Home->ViewHomeInfo($home_id);

	# Get all features
	foreach($FeatureType as $key => $val)
	{
		$Home_Feature[$val] = $Home->ViewAllHomeFeature($key, $rsHome->home_features);
	}

	$tpl->assign(array("home_id"			=>	$home_id,
						"Home_Feature"		=>	$Home_Feature,
						"rsHome"			=>	$rsHome,
						"PictureList"		=>	$Home->ViewAllPicture($home_id),
						"Picture_path"		=>	$virtual_path['Picture'],
					));
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Manage Picture
#-----------------------------------------------------------------------------------------------------------------------------
else if($Action == 'Picture')
{	
	header('location: homepicture.php?home_id=' . $home_id);
}
#-----------------------------------------------------------------------------------------------------------------------------
#	View Home Picture Gallery
#-----------------------------------------------------------------------------------------------------------------------------
else if($Action == 'PictureGallery')
{	
	if($Action2 == 'Next')
		$pic_id = $_GET['pic_id'] + 1;
	else if($Action2 == 'Previous')
		$pic_id = $_GET['pic_id'] - 1;
	else if($_POST['pic_id'])
		$pic_id = $_POST['pic_id'];
	else if($_GET['pic_id'])
		$pic_id = $_GET['pic_id'];
	else
		$pic_id = 1;		
	
	# Set Total Pictures	
	$total_pic = $_GET['total_pic'];
	
	if ($pic_id < 1 )
		$pic_id = $total_pic;
	if ($pic_id > $total_pic )
		$pic_id = 1;
	
	$tpl->assign(array("T_Body"					=>	'home_picturegallery'. $config['tplEx'],
						"JavaScript"			=>  array("home.js"),
						"A_Action"				=>	"home.php",						
						"Error_Message"			=>	$Error_Message,
						"Action"				=>	$Action,
						"Picture_path"			=>	$virtual_path['Picture'],
						"Default_Pic"			=>	$config['picDefault'],
						"Back_Link"				=>	"?Action=View&home_id=" . $home_id,
				));
				
				
	# Get Picture 
	$rsPicture = $Home->getPictureGallery($home_id,$pic_id);	
	
	# If end
	if($rsPicture === false)
	{
		header('location: home.php?Action=View&home_id='.$home_id);	
		exit;
		//header('location: home.php?Action=PictureGallery&Action2=Next&home_id='.$home_id . '&pic_id='. $pic_id);	
	}

	$tpl->assign(array(	"Picture"				=>	$rsPicture,
						"pic_id"				=>	$pic_id,
						"home_id"				=>	$home_id,				
						"total_pic"				=>	$total_pic,
					));					

}

$tpl->display('default_layout'. $config['tplEx']);										
?>
