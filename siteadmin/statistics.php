<?php
#====================================================================================================
# File Name : statistics.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#=========================================
#	Copy the website statistics
#-----------------------------------------
echo "\n". str_repeat("=", 65); 
echo "\nExecution Date : ". date ("l dS of F Y h:i:s A"). "\n"; 

$username			= 'myindy';												// client adminpanel username
$site_source_path	= "/home/". $username. "/tmp/webalizer/"; 				// Source File Path
$site_dest_path     = "/home/". $username. "/public_html/webalizer/"; 		// Copy Destnation path

$ret = copyDirectory($site_source_path, $site_dest_path);

system("chmod 655 -R ".$site_dest_path);
system("chown ".$username.":".$username." -R ".$site_dest_path);

echo "\n". str_repeat("-", 65). "\n"; 

#=========================================
#	Function Name	:	copyDirectory
#-----------------------------------------
function copyDirectory($source,	$dest)
{
	global $config;
	
	if(!file_exists($source))
		return 2;

	if ($dir = opendir($source)) 
	{
		@mkdir($dest);
		// $config['Folder_Perms']
		@chmod($dest, 0777);
		while ($file = readdir($dir)) 
		{
			if (($file != ".")&&($file != "..")) 
			{
				$tempSource = $source."/".$file;
				$tempDest 	= $dest."/".$file;
				if (is_dir($tempSource)) 
				{
					$ret = copyDirectory($tempSource, $tempDest);
				}
				else 
				{
					copy($tempSource, $tempDest);
					chmod($tempDest, 0765);
				} 
			} 
		} 
	} 
	else
	{
		return 0;
	}
	
	return 1;
}

?>