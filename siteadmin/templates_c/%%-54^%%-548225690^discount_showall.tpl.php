<?php /* Smarty version 2.6.0, created on 2009-12-29 23:20:46
         compiled from discount_showall.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'discount_showall.tpl', 57, false),array('function', 'cycle', 'discount_showall.tpl', 58, false),)), $this); ?>
<script language="javascript">
	var Confirm_Delete_Msg	= '<?php echo $this->_tpl_vars['Confirm_Delete_Msg']; ?>
';
</script>


<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
<form name="frmTravelDiscount" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%"><?php echo $this->_tpl_vars['L_Discount_Manager']; ?>
</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<?php echo $this->_tpl_vars['L_Manage_Discount']; ?>

					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;<?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
				  <td bgcolor="#FFFFFF">
				  <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1">
				<tr>
					 <td colspan="5" class="successMsg" align="center"><?php echo $this->_tpl_vars['Message']; ?>
</td>
				</tr>
				<tr>
					<td colspan="5" align="right">
						<a href="<?php echo $_SERVER['PHP_SELF']; ?>
?Action=Add"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_add.gif" class="imgAction" title="Add Page"></a>&nbsp;&nbsp;
						<!--a href="<?php echo $_SERVER['PHP_SELF']; ?>
?Action=Sort"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_sort.gif" class="imgAction" title="Sort Pages"></a-->&nbsp;&nbsp;
					</td>
				</tr>
				
<!--			<tr>
					<td colspan="5" class="stdSection" align="right">
						<a href="javascript: Add_Click();" class="actionLink"><?php echo $this->_tpl_vars['L_Add_Travel_Discount']; ?>
</a>
					</td>
				</tr>
-->				
				<tr>
				 	<td class="listHeader" width="8%" align="center"><?php echo $this->_tpl_vars['SrNo']; ?>
</td>
					<td class="listHeader" width="40%" align="center"><?php echo $this->_tpl_vars['L_Discount_For']; ?>
</td>
					<td class="listHeader" width="30%" align="center"><?php echo $this->_tpl_vars['L_Discount_Rate']; ?>
</td>
					<td class="listHeader" colspan="2" align="center"><?php echo $this->_tpl_vars['Action']; ?>
</td>
				</tr>
				<tr><td colspan="5" align="center" class="errorMsg"><?php echo $this->_tpl_vars['No_Record_Found']; ?>
</td></tr>
				<?php if (isset($this->_foreach['DiscInfo'])) unset($this->_foreach['DiscInfo']);
$this->_foreach['DiscInfo']['name'] = 'DiscInfo';
$this->_foreach['DiscInfo']['total'] = count($_from = (array)$this->_tpl_vars['DiscInfo']);
$this->_foreach['DiscInfo']['show'] = $this->_foreach['DiscInfo']['total'] > 0;
if ($this->_foreach['DiscInfo']['show']):
$this->_foreach['DiscInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Disc']):
        $this->_foreach['DiscInfo']['iteration']++;
        $this->_foreach['DiscInfo']['first'] = ($this->_foreach['DiscInfo']['iteration'] == 1);
        $this->_foreach['DiscInfo']['last']  = ($this->_foreach['DiscInfo']['iteration'] == $this->_foreach['DiscInfo']['total']);
?>
				 <?php echo smarty_function_assign(array('var' => 'Sr_No','value' => $this->_foreach['DiscInfo']['iteration']), $this);?>
 
				<tr class="<?php echo smarty_function_cycle(array('values' => 'list_A, list_B'), $this);?>
">
					<td align="center"><?php echo $this->_tpl_vars['Sr_No']; ?>
</td>
					<td align="center"><?php echo $this->_tpl_vars['Disc']['discount_title']; ?>
</td>
					<td align="center"><?php echo $this->_tpl_vars['Disc']['discount_rate']; ?>
 %</td>
					<td align="center">
						<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_edit.gif" class="imgAction" title="Edit" onClick="javascript: Edit_Click('<?php echo $this->_tpl_vars['Disc']['discount_id']; ?>
');">&nbsp;
					</td>
					<td align="center">
						<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_delete.gif" class="imgAction" title="Delete" onClick="javascript: Delete_Click('<?php echo $this->_tpl_vars['Disc']['discount_id']; ?>
');">&nbsp;					
					</td>
				</tr>
				<?php endforeach; unset($_from); endif; ?>
				<tr><td colspan="5">&nbsp;</td></tr>
				<tr>
					<td colspan="5" class="pageLink" align="right"><?php echo $this->_tpl_vars['Page_Link']; ?>
</td>
				</tr>
			</table>
		</td>
	</tr>

			</table>
			<br>
			<input type="hidden" name="discount_id" value="">
			<input type="hidden" name="Action">
			<input type="hidden" name="start" value="<?php echo $this->_tpl_vars['Start']; ?>
">
		</td>
	</tr>
	</form>
</table>