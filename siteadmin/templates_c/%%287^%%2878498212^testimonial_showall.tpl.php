<?php /* Smarty version 2.6.0, created on 2016-10-27 06:19:57
         compiled from testimonial_showall.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'testimonial_showall.tpl', 39, false),array('modifier', 'cat', 'testimonial_showall.tpl', 41, false),array('modifier', 'regex_replace', 'testimonial_showall.tpl', 41, false),array('modifier', 'count_characters', 'testimonial_showall.tpl', 46, false),array('modifier', 'truncate', 'testimonial_showall.tpl', 49, false),)), $this); ?>
<script>
	var Delete_Testimonial 		= '<?php echo $this->_tpl_vars['Delete_Testimonial']; ?>
';	
	var Delete_AllTestimonials 	= '<?php echo $this->_tpl_vars['Delete_AllTestimonials']; ?>
';	
</script>

<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="100%">
<form name="frmTestimonial" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection"><?php echo $this->_tpl_vars['L_Manage_Testimonial']; ?>
 [ <a href="javascript: Add_Click('<?php echo $this->_tpl_vars['Add_Action']; ?>
');" class="actionLink"><?php echo $this->_tpl_vars['Add']; ?>
</a> ]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td><?php echo $this->_tpl_vars['L_Testimonial_Desc']; ?>
</td>
				</tr>
				<tr>
					<td align="center" class="successMsg"><?php echo $this->_tpl_vars['SuccMessage']; ?>
</td>
				</tr>
				<tr>
					<td valign="top" align="center">
						<table border="0" cellpadding="2" cellspacing="2" width="97%">
							<tr>
								<td class="listHeader" width="3%"></td>
								<td class="listHeader"><?php echo $this->_tpl_vars['Photo']; ?>
</td>
								<td class="listHeader"><?php echo $this->_tpl_vars['Comment']; ?>
</td>	
								<td class="listHeader" width="15%"><?php echo $this->_tpl_vars['Display_On_Website']; ?>
</td>																								
								<td class="listHeader" width="14%"><?php echo $this->_tpl_vars['L_Action']; ?>
</td>
							</tr>
							
							<?php if (count($_from = (array)$this->_tpl_vars['PersonData'])):
    foreach ($_from as $this->_tpl_vars['Person']):
?>    
							<tr class="<?php echo smarty_function_cycle(array('values' => 'list_A, list_B'), $this);?>
">
								<td valign="top"><input type="checkbox" name="person_list[]" class="stdCheckBox" value="<?php echo $this->_tpl_vars['Person']->person_id; ?>
"></td>
								<td width="130" valign="top"><img src="<?php if ($this->_tpl_vars['Person']->person_pic): ?> <?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['Testimonial_Path'])) ? $this->_run_mod_handler('cat', true, $_tmp, 'thumb_') : smarty_modifier_cat($_tmp, 'thumb_')))) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['Person']->person_pic) : smarty_modifier_cat($_tmp, $this->_tpl_vars['Person']->person_pic)))) ? $this->_run_mod_handler('regex_replace', true, $_tmp, '/[\\\\]/', '') : smarty_modifier_regex_replace($_tmp, '/[\\\\]/', '')); ?>
 <?php else: ?> <?php echo ((is_array($_tmp=$this->_tpl_vars['Testimonial_Path'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['Default_Picture']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['Default_Picture'])); ?>
 <?php endif; ?>" border="0"></td> 
								<td valign="top">
									<table border="0" cellpadding="0" cellspacing="0" width="98%">
										<tr> 
											<td><p align="justify">
												<?php if (((is_array($_tmp=$this->_tpl_vars['Person']->person_comment)) ? $this->_run_mod_handler('count_characters', true, $_tmp) : smarty_modifier_count_characters($_tmp)) == 200): ?>
													<?php echo ((is_array($_tmp=$this->_tpl_vars['Person']->person_comment)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")); ?>

												<?php else: ?>
													<?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['Person']->person_comment)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")))) ? $this->_run_mod_handler('truncate', true, $_tmp, 200, "...") : smarty_modifier_truncate($_tmp, 200, "...")); ?>

												<?php endif; ?></p>
											</td> 
										</tr> 
										<tr>
											<td class="boldText">
												<?php echo ((is_array($_tmp=$this->_tpl_vars['Person']->person_name)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")); ?>

											</td>
										</tr>
									</table>
								</td> 
								<td align="center">
										<?php if ($this->_tpl_vars['Person']->person_status == 1): ?>
											YES
										<?php else: ?>
											NO
										<?php endif; ?>
								</td>
								<td align="center">
									<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_view.gif" 	class="imgAction" title="View" onClick="javascript: View_Click('<?php echo $this->_tpl_vars['A_View']; ?>
','<?php echo $this->_tpl_vars['Person']->person_id; ?>
');">&nbsp;&nbsp;
									<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_edit.gif" 	class="imgAction" title="Edit" onClick="JavaScript: Edit_Click('<?php echo $this->_tpl_vars['A_Edit']; ?>
','<?php echo $this->_tpl_vars['Person']->person_id; ?>
');">&nbsp;&nbsp;
									<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_delete.gif" 	class="imgAction" title="Delete" onClick="JavaScript: Delete_Click('<?php echo $this->_tpl_vars['A_Delete']; ?>
','<?php echo $this->_tpl_vars['Person']->person_id; ?>
');">&nbsp;&nbsp;
								</td> 
							</tr>
							<?php endforeach; unset($_from); endif; ?>
							<tr><td colspan="5">&nbsp;</td></tr>
							<tr>
								<td colspan="5">
									<table border="0" cellpadding="1" cellspacing="1" width="95%">
										<tr>
											<td>
												<?php if ($this->_tpl_vars['Person']->person_id != ''): ?>
												<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
arrow_ltr.png"> 
												<a href="JavaScript: CheckUncheck_Click(document.all['person_list[]'], true);" onMouseMove="window.status='Check All';" onMouseOut="window.status='';" class="actionLink">Check All</a> / 
												<a href="JavaScript: CheckUncheck_Click(document.all['person_list[]'], false);" onMouseMove="window.status='Uncheck All';" onMouseOut="window.status='';" class="actionLink">Uncheck All</a>  &nbsp;&nbsp;
												With selected
												<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: DeleteChecked_Click('<?php echo $this->_tpl_vars['A_DelSelected']; ?>
');">
											   <?php endif; ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="5" class="pageLink" align="right"><?php echo $this->_tpl_vars['Page_Link']; ?>
<!-- Page : 1 2 3 Next --></td>
							</tr>
							<input type="hidden" name="person_id">
							<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>	
</table>