<?php /* Smarty version 2.6.0, created on 2018-07-23 11:11:26
         compiled from default_menu.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'lower', 'default_menu.tpl', 7, false),)), $this); ?>
<?php if (isset($this->_foreach['PrivilegeInfo'])) unset($this->_foreach['PrivilegeInfo']);
$this->_foreach['PrivilegeInfo']['name'] = 'PrivilegeInfo';
$this->_foreach['PrivilegeInfo']['total'] = count($_from = (array)$this->_tpl_vars['AssignedPrivileges']);
$this->_foreach['PrivilegeInfo']['show'] = $this->_foreach['PrivilegeInfo']['total'] > 0;
if ($this->_foreach['PrivilegeInfo']['show']):
$this->_foreach['PrivilegeInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Key'] => $this->_tpl_vars['Type']):
        $this->_foreach['PrivilegeInfo']['iteration']++;
        $this->_foreach['PrivilegeInfo']['first'] = ($this->_foreach['PrivilegeInfo']['iteration'] == 1);
        $this->_foreach['PrivilegeInfo']['last']  = ($this->_foreach['PrivilegeInfo']['iteration'] == $this->_foreach['PrivilegeInfo']['total']);
?>
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_<?php echo ((is_array($_tmp=$this->_tpl_vars['Key'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp)); ?>
.gif"></td>
					<td class="stdSection" width="99%"><?php echo $this->_tpl_vars['Privileges'][$this->_tpl_vars['Key']]['Title']; ?>
</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="5" cellspacing="1" width="90%">
				<tr>
					<td valign="top" align="center">
						<table border="0" cellpadding="1" cellspacing="2">
							<?php if (count($_from = (array)$this->_tpl_vars['Privileges'][$this->_tpl_vars['Key']]['SubOption'])):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['Option']):
?>
							<tr>
								<td align="center" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
b_arrow.gif"></td>
								<td width="99%"><a href="<?php echo $this->_tpl_vars['Option']['Link']; ?>
" class="MenuLink" title="<?php echo $this->_tpl_vars['Option']['Desc']; ?>
"><?php echo $this->_tpl_vars['Option']['Title']; ?>
</a></td>
							</tr>
							<?php endforeach; unset($_from); endif; ?>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<?php endforeach; unset($_from); endif; ?>