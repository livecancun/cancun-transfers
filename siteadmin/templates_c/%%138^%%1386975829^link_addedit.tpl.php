<?php /* Smarty version 2.6.0, created on 2009-03-05 08:22:25
         compiled from link_addedit.tpl */ ?>
<script language="javascript">
	var msg_Name		= '<?php echo $this->_tpl_vars['msg_Name']; ?>
';
	var msg_Address		= '<?php echo $this->_tpl_vars['msg_Address']; ?>
';
	var msg_City		= '<?php echo $this->_tpl_vars['msg_City']; ?>
';
	var msg_State		= '<?php echo $this->_tpl_vars['msg_State']; ?>
';
	var msg_Zip			= '<?php echo $this->_tpl_vars['msg_Zip']; ?>
';
	var msg_Country  	= '<?php echo $this->_tpl_vars['msg_Country']; ?>
';
	var msg_Telephone	= '<?php echo $this->_tpl_vars['msg_Telephone']; ?>
';
	var msg_Title		= '<?php echo $this->_tpl_vars['msg_Title']; ?>
';
	var msg_Description	= '<?php echo $this->_tpl_vars['msg_Description']; ?>
';
	var msg_Url			= '<?php echo $this->_tpl_vars['msg_Url']; ?>
';
	var msg_Our_Url		= '<?php echo $this->_tpl_vars['msg_Our_Url']; ?>
';
	var msg_Email		= '<?php echo $this->_tpl_vars['msg_Email']; ?>
';
</script>

<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
<form name="frmLinkDetail"  action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%"><?php echo $this->_tpl_vars['L_Link_Manager']; ?>
 [ <?php echo $this->_tpl_vars['L_Action']; ?>
 ]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<?php echo $this->_tpl_vars['L_Manage_Links']; ?>

					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;<?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
				  <td bgcolor="#FFFFFF">
				  
						<table border="0" align="center" cellpadding="0" cellspacing="0" width="740" >
							<tr>
								<td valign="top" align="center">
									<table border="0" cellpadding="0" cellspacing="1" width="750">
											<tr>
												<td valign="top" align="center">
													<table border="0" cellpadding="1" cellspacing="3" width="97%">
														<tr height="20">
															<td colspan="2" align="center" class="successMsg">&nbsp;<?php echo $this->_tpl_vars['Message']; ?>
</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_Name']; ?>
 :</td>
															<td class="fieldInputStyle" ><input name="url_name" type="text" id="url_name" value="<?php echo $this->_tpl_vars['url_name']; ?>
"  maxlength="255" ></td>
														</tr>
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_Address']; ?>
 :</td>
															<td class="fieldInputStyle" ><textarea name="url_address" cols="50" rows="3" id="url_address"><?php echo $this->_tpl_vars['url_address']; ?>
</textarea></td>
														</tr>	
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_City']; ?>
 :</td>
															<td class="fieldInputStyle" ><input name="url_city" type="text" id="url_city" value="<?php echo $this->_tpl_vars['url_city']; ?>
"  maxlength="255" ></td>
														</tr>	
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_State']; ?>
 :</td>
															<td class="fieldInputStyle" ><input name="url_state" type="text" id="url_state" value="<?php echo $this->_tpl_vars['url_state']; ?>
"  maxlength="255" ></td>
														</tr>	
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_Zip']; ?>
 :</td>
															<td class="fieldInputStyle" ><input name="url_zip" type="text" id="url_zip" value="<?php echo $this->_tpl_vars['url_zip']; ?>
"  maxlength="5"  onKeyPress="javascript: isNumericKey(this.value)"></td>
														</tr>	
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_Country']; ?>
 :</td>
															<td class="fieldInputStyle" ><input name="url_country" type="text" id="url_country" value="<?php echo $this->_tpl_vars['url_country']; ?>
"  maxlength="255" ></td>
														</tr>
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_Telephone']; ?>
 :</td>
														  	<td class="fieldInputStyle">
																<input type="text" name="url_telno_areacode" size="3" maxlength="3" value="<?php echo $this->_tpl_vars['url_telno_areacode']; ?>
" onKeyPress="javascript: isNumericKey(this.value)"> -
																<input type="text" name="url_telno_citycode" size="3" maxlength="3" value="<?php echo $this->_tpl_vars['url_telno_citycode']; ?>
" onKeyPress="javascript: isNumericKey(this.value)"> -
																<input type="text" name="url_telno_no" size="4" maxlength="4" value="<?php echo $this->_tpl_vars['url_telno_no']; ?>
" onKeyPress="javascript: isNumericKey(this.value)">
															</td>	
														</tr>	
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_Title']; ?>
 :</td>
															<td class="fieldInputStyle" ><input name="url_title" type="text" value="<?php echo $this->_tpl_vars['url_title']; ?>
" size="50"  maxlength="255" ></td>
														</tr>
														<tr>
															<td class="fieldLabelRight" valign="top" width="35%" ><?php echo $this->_tpl_vars['L_Link_Category']; ?>
 :</td>
															<td width="75%" class="fieldInputStyle"><?php echo $this->_tpl_vars['Link_Category']; ?>
</td>
														</tr>	
														<tr>
															<td width="35%" class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['L_Link_Description']; ?>
 :</td>
															<td class="fieldInputStyle">
															<textarea name="url_desc" rows="3" cols="50"><?php echo $this->_tpl_vars['url_desc']; ?>
</textarea>
															</td>
														</tr>
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_Url']; ?>
 :</td>
															<td class="fieldInputStyle" >
															<?php if ($this->_tpl_vars['url_url']): ?>
																<input name="url_url" type="text" value="<?php echo $this->_tpl_vars['url_url']; ?>
" size="50"  maxlength="255" >
															<?php else: ?>
																<input name="url_url" type="text" value="http://" size="50"  maxlength="255" >
															<?php endif; ?>
															</td>
														</tr>
														<tr>
															<td class="fieldLabelRight" ><?php echo $this->_tpl_vars['L_Link_Our_Url']; ?>
 :</td>
															<td class="fieldInputStyle" >
															
															<?php if ($this->_tpl_vars['url_oururl']): ?>
																<input name="url_oururl" type="text" id="url_oururl" value="<?php echo $this->_tpl_vars['url_oururl']; ?>
" size="50"  maxlength="255" >
															<?php else: ?>
																<input name="url_oururl" type="text" value="http://" size="50"  maxlength="255">
															<?php endif; ?>
															
															
															
															</td>
														</tr>				
														<tr>
															<td class="fieldLabelRight"><?php echo $this->_tpl_vars['L_Link_Email']; ?>
 :</td>
															<td class="fieldInputStyle" ><input type="text" name="url_email" value="<?php echo $this->_tpl_vars['url_email']; ?>
"  maxlength="255" size="50" ></td>
														</tr>
														<tr>
															<td class="fieldLabelRight"><?php echo $this->_tpl_vars['L_Link_Status']; ?>
 :</td>
															<td width="75%" class="fieldInputStyle"> 
															<select name = "cat_status">
															<?php echo $this->_tpl_vars['Status_List']; ?>

															</select>
															</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
														<tr>
															<td colspan="8" align="center" >
																<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Save'];  echo $this->_tpl_vars['Update']; ?>
" class="nrlButton" onClick="javascript: return Form_LinkSubmit(document.frmLinkDetail);">
																<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Cancel']; ?>
" class="nrlButton">
															</td>
														</tr>
														<tr>
															<td colspan="2">
																<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
																<input type="hidden" name="start" value="<?php echo $this->_tpl_vars['Start']; ?>
">
																<input type="hidden" name="link_id" value="<?php echo $this->_tpl_vars['link_id']; ?>
">
																<input type="hidden" name="cat_id" value="<?php echo $this->_tpl_vars['cat_id']; ?>
">
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
								</td>
							</tr>
						</table>				  
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>