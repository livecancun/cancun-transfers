<?php /* Smarty version 2.6.0, created on 2011-06-02 22:02:34
         compiled from reservationdetail_showall.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'reservationdetail_showall.tpl', 42, false),array('function', 'math', 'reservationdetail_showall.tpl', 60, false),array('modifier', 'number_format', 'reservationdetail_showall.tpl', 53, false),)), $this); ?>
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">Reservation</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td class="successMsg" align="center"> <?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
			</table>
<table border="0" cellspacing="0" cellpadding="0" width="750">
  <tr> 
    <td>
	<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" >
		<tr> 
		  <td colspan="5" height="1" bgcolor="#000066"></td>
		</tr>
	    <tr> 
			<td class="blueHeader" width="100%" colspan="5"><?php echo $this->_tpl_vars['L_Reservation_Details']; ?>
 -> <?php echo $this->_tpl_vars['L_ConfirmationNo']; ?>
 : <?php echo $this->_tpl_vars['V_ConfirmationNo']; ?>
</td>
	    </tr>
		<tr> 
		  <td colspan="5" height="1" bgcolor="#000066"></td>
		</tr>	
		<tr><td align="center" class="successMsg" width="100%" colspan="5"><?php echo $this->_tpl_vars['Message']; ?>
</td></tr>
		<tr><td align="center" class="errorMsg" width="100%" colspan="5"><?php echo $this->_tpl_vars['ErrorMessage']; ?>
</td></tr>
			<FORM name="frmReservation" action="<?php echo $this->_tpl_vars['A_Reservation']; ?>
" method="POST">
			   <tr>
			     <td class="listHeader" align="center" width="30%"  ><?php echo $this->_tpl_vars['L_cartItem']; ?>
</td>
  				 <!--<td class="blueHeader" align="center" width="25%" ><?php echo $this->_tpl_vars['L_ItemType']; ?>
</td>-->
  				 <td class="listHeader" align="center" width="15%"><?php echo $this->_tpl_vars['L_Total']; ?>
</td>
				 <td class="listHeader" align="center" width="30%" colspan="3" ><?php echo $this->_tpl_vars['Action']; ?>
</td>
			   </tr>
			    <tr><td class="successMsg" colspan="5"  align="center"><?php echo $this->_tpl_vars['Notfound_Message']; ?>
</td></tr>

			<?php if (isset($this->_foreach['CartInfo'])) unset($this->_foreach['CartInfo']);
$this->_foreach['CartInfo']['name'] = 'CartInfo';
$this->_foreach['CartInfo']['total'] = count($_from = (array)$this->_tpl_vars['CartInfo']);
$this->_foreach['CartInfo']['show'] = $this->_foreach['CartInfo']['total'] > 0;
if ($this->_foreach['CartInfo']['show']):
$this->_foreach['CartInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Cart']):
        $this->_foreach['CartInfo']['iteration']++;
        $this->_foreach['CartInfo']['first'] = ($this->_foreach['CartInfo']['iteration'] == 1);
        $this->_foreach['CartInfo']['last']  = ($this->_foreach['CartInfo']['iteration'] == $this->_foreach['CartInfo']['total']);
?>
			<?php echo smarty_function_assign(array('var' => 'TotalPrice','value' => ($this->_tpl_vars['TotalPrice']+$this->_tpl_vars['Cart']['totalCharge'])), $this);?>

			<tr>
				<td class="List_B" align="center" height="25" ><?php echo $this->_tpl_vars['Cart']['dest_title']; ?>
 </td>
				<td class="List_B" align="right">$ <?php echo $this->_tpl_vars['Cart']['totalCharge']; ?>
  </td>
				<td class="List_B" align="center"><a href="JavaScript: Show_Click('<?php echo $this->_tpl_vars['Cart']['cart_id']; ?>
');" class="lightgreyLink"><?php echo $this->_tpl_vars['View']; ?>
</a></td>
				<td class="List_B" align="center"><a href="JavaScript: Delete_Click('<?php echo $this->_tpl_vars['Cart']['cart_id']; ?>
');" class="lightgreyLink"><?php echo $this->_tpl_vars['Delete']; ?>
</a></td>
 			</tr>	
			<?php endforeach; unset($_from); endif; ?>
			
		 	<tr>
				<td align="right"><b><?php echo $this->_tpl_vars['L_Total']; ?>
 ($):</b></td>
			   	<td align="right"><b><?php echo ((is_array($_tmp=$this->_tpl_vars['TotalPrice'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, '.') : smarty_modifier_number_format($_tmp, 2, '.')); ?>
</b>  </td>
			   	<td colspan="4">&nbsp;</td>
			</tr>
			
            <?php if ($this->_tpl_vars['is_airline_empl'] == 1): ?>
		 	<tr>
				<td align="right"><b><?php echo $this->_tpl_vars['L_Discount']; ?>
 <?php echo $this->_tpl_vars['discount_rate']; ?>
% :</b><?php echo smarty_function_assign(array('var' => 'discount','value' => ($this->_tpl_vars['discount_rate'])), $this);?>
</td>
			   	<td align="right"><b>- $ <?php echo smarty_function_math(array('equation' => "(x * y)/100",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</b>  </td>
			   	<td colspan="4">&nbsp;</td>
			</tr>

		 	<tr>
				<td align="right"><b><?php echo $this->_tpl_vars['L_Final']; ?>
 <?php echo $this->_tpl_vars['L_Total']; ?>
:</b></td>
			   	<td align="right"><b>$ <?php echo smarty_function_math(array('equation' => "(x-((x * y)/100))",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</b>  </td>
			   	<td colspan="4">&nbsp;</td>
			</tr>
			<?php endif; ?>
			
			<tr><td colspan="5"> <b><?php echo $this->_tpl_vars['L_status']; ?>
 : <?php echo $this->_tpl_vars['Cart']['cartStatus']; ?>
</b></td></tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr><td colspan="5" align="center">

				<?php if ($this->_tpl_vars['Cart']['cartStatus'] == 'NA' || $this->_tpl_vars['Cart']['cartStatus'] == 'PR'): ?>
					<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['AR']; ?>
" class="nrlButton">
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Cart']['cartStatus'] == 'AR' || $this->_tpl_vars['Cart']['cartStatus'] == 'PR'): ?>				
					<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['NA']; ?>
" class="nrlButton">
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Cart']['cartStatus'] == 'AR' || $this->_tpl_vars['Cart']['cartStatus'] == 'NA'): ?>
				    <input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['PR']; ?>
" class="nrlButton">
				<?php endif; ?>

				<?php if ($this->_tpl_vars['Cart']['cartStatus'] == 'AR'): ?>				
					 <input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['PD']; ?>
" class="nrlButton">
				<?php endif; ?>
			
					<input type="button" name="Back" value="<?php echo $this->_tpl_vars['Back']; ?>
" class="nrlButton" onclick="javascript:location.href('reservation.php')">
					<input type="hidden" name="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
"> 
					<input type="hidden" name="cartId" value="<?php echo $this->_tpl_vars['cartId']; ?>
"> 
					<input type="hidden" name="packageCartId" value="<?php echo $this->_tpl_vars['packageCartId']; ?>
"> 
					<input type="hidden" name="itemType"> 
				    <input type="hidden" name="confirmationNo" value="<?php echo $this->_tpl_vars['V_ConfirmationNo']; ?>
"> 
			 	    <input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
				</td>
			</tr>
			
			</form>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr><td colspan="5"><b><?php echo $this->_tpl_vars['Note']; ?>
 : </b></td></tr>
			<tr><td colspan="5"><?php echo $this->_tpl_vars['empl_comments']; ?>
</td></tr>
			<tr> 
			  <td colspan="5" height="1" bgcolor="#000066"></td>
			</tr>
			<tr>
			  	<td colspan="5">
					<table width="100%">
					<tr>
						<td colspan="2" align="left"><b>NR</b> - <?php echo $this->_tpl_vars['NoRequest']; ?>
</td>
						<td colspan="2" align="left" ><b>PR</b> - <?php echo $this->_tpl_vars['PendingRequest']; ?>
</td>
						<td colspan="2" ><b>AR</b> - <?php echo $this->_tpl_vars['ApproveRequest']; ?>
</td>
					</tr>
					<tr>
					<td colspan="2"><b>NA</b> - <?php echo $this->_tpl_vars['NotApprove']; ?>
</td>
					<td colspan="2"><b>RP</b> - <?php echo $this->_tpl_vars['RequestPayment']; ?>
</td>
					<td colspan="2"><b>PD</b> - <?php echo $this->_tpl_vars['PaymentDone']; ?>
</td>
				   </tr>
					</table>
				</td>	
			</tr>
			<tr> 
					  <td colspan="5" height="1" bgcolor="#000066"></td>
			</tr>
      </table>
    </td>
 </tr>
 <tr>
	   <td colspan="5">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr height="15">
						<td class="paggingLine" width="10%">
							  <?php echo $this->_tpl_vars['Page_Link']; ?>

						</td>
					</tr>
				</table>
			</td>
		</tr>
</table>
</td>
	</tr>

</table>





