<?php /* Smarty version 2.6.0, created on 2015-12-27 22:52:27
         compiled from siteconfig.tpl */ ?>
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmSiteConfig" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_settings.gif"></td>
					<td class="stdSection" width="49%"><?php echo $this->_tpl_vars['L_Site_Config']; ?>
&nbsp;</td>
					<td align="right" width="50%">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<?php echo $this->_tpl_vars['L_Site_ConfigDesc']; ?>

					</td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr><td class="successMsg" align="center">&nbsp;<?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
				<tr><td height="5"></td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td align="left">
						<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Update']; ?>
" class="stdButton" onClick="javascript: return Form_Submit(document.frmSiteConfig);">
						<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Cancel']; ?>
" class="stdButton">
					</td>
					<td align="right" class="mandatory"><?php echo $this->_tpl_vars['L_Mandatory_Fields']; ?>
</td>
				</tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						<?php echo $this->_tpl_vars['L_Comp_Info']; ?>

					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top" width="30%"><?php echo $this->_tpl_vars['L_Company_Name']; ?>
 <font class="mandatoryMark">*</font></td>
					<td width="70%" class="fieldInputStyle">
						<textarea name="company_title" rows="2" cols="60"><?php echo $this->_tpl_vars['company_title']; ?>
</textarea><br>
						<font class="validationText"><?php echo $this->_tpl_vars['L_Max100_Chars']; ?>
 </font><br>
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top" width="20%"><?php echo $this->_tpl_vars['L_Site_Title']; ?>
 <font class="mandatoryMark">*</font></td>
					<td class="fieldInputStyle">
						<textarea name="site_title" rows="2" cols="60"><?php echo $this->_tpl_vars['site_title']; ?>
</textarea><br>
						<font class="validationText"><?php echo $this->_tpl_vars['L_Max100_Chars']; ?>
 </font><br>
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top"><?php echo $this->_tpl_vars['L_Copyright_Text']; ?>
 <font class="mandatoryMark">*</font></td>
					<td class="fieldInputStyle">
						<textarea name="copyright_text" rows="3" cols="60"><?php echo $this->_tpl_vars['copyright_text']; ?>
</textarea>
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top" width="30%"><?php echo $this->_tpl_vars['L_Support_Email']; ?>
: <font class="mandatoryMark">*</font></td>
					<td width="70%" class="fieldInputStyle">
						<input type="text" name="support_email" size="50" maxlength="120" value="<?php echo $this->_tpl_vars['support_email']; ?>
"><br>
						<?php echo $this->_tpl_vars['L_Support_EmailDesc']; ?>

					</td>
				</tr>									
				<tr>
					<td class="fieldLabelLeft" valign="top" width="30%"><?php echo $this->_tpl_vars['L_Paypal_Email']; ?>
: <font class="mandatoryMark">*</font></td>
					<td width="70%" class="fieldInputStyle">
						<input type="text" name="paypal_email" size="50" maxlength="120" value="<?php echo $this->_tpl_vars['paypal_email']; ?>
"><br>
						<?php echo $this->_tpl_vars['L_Paypal_EmailDesc']; ?>

					</td>
				</tr>									
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						<?php echo $this->_tpl_vars['L_Meta_Info']; ?>

					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top"><?php echo $this->_tpl_vars['L_Meta_Title']; ?>
</td>
					<td class="fieldInputStyle">
						<textarea name="meta_title" rows="2" cols="60"><?php echo $this->_tpl_vars['meta_title']; ?>
</textarea><br>
						<font class="validationText"><?php echo $this->_tpl_vars['L_Max100_Chars']; ?>
 </font><br>
						<?php echo $this->_tpl_vars['L_Meta_TitleDesc']; ?>

					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top"><?php echo $this->_tpl_vars['L_Meta_Description']; ?>
</td>
					<td class="fieldInputStyle">
						<textarea name="meta_description" rows="3" cols="60"><?php echo $this->_tpl_vars['meta_description']; ?>
</textarea><br>
						<font class="validationText"><?php echo $this->_tpl_vars['L_Max400_Chars']; ?>
 </font><br>
						<?php echo $this->_tpl_vars['L_Meta_DescriptionDesc']; ?>
 						
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top"><?php echo $this->_tpl_vars['L_Meta_Keyword']; ?>
</td>
					<td class="fieldInputStyle">
						<textarea name="meta_keyword" rows="3" cols="60"><?php echo $this->_tpl_vars['meta_keyword']; ?>
</textarea><br>
						<font class="validationText"><?php echo $this->_tpl_vars['L_Max1000_Chars']; ?>
 </font><br>
						<?php echo $this->_tpl_vars['L_Meta_KeywordDesc']; ?>
 						
					</td>
				</tr>
				<!--tr>
					<td class="fieldLabelLeft" valign="top">Update Meta Info to all pages</td>
					<td class="fieldInputStyle">
						<input type="checkbox" name="updatemeta" value="yes"/>
					</td>
				</tr-->				
				<!--tr>
					<td colspan="2" class="stdSubSection" align="center">
						Other Settings
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Default Page Size</td>
					<td class="fieldInputStyle">
						<select name="page_size">
							<?php echo $this->_tpl_vars['page_size']; ?>

						</select><br>
						Default is <b>30</b>. This will determine how many results, per page, should be displayed. 
						The higher the number, the longer it will take the page to display.
					</td>
				</tr-->
				<tr><td class="divider" colspan="2"></td></tr>
				<tr>
					<td align="left" colspan="2">
						<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Update']; ?>
" class="stdButton" onClick="javascript: return Form_Submit(document.frmSiteConfig);">
						<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Cancel']; ?>
" class="stdButton">
					</td>
				</tr>
			</table>	
			<br>
		</td>
	</tr>
	</form>
</table>