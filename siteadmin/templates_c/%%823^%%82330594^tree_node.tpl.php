<?php /* Smarty version 2.6.0, created on 2016-04-19 17:56:50
         compiled from tree_node.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'tree_node.tpl', 2, false),array('modifier', 'default', 'tree_node.tpl', 3, false),array('modifier', 'str_repeat', 'tree_node.tpl', 6, false),array('modifier', 'array_value', 'tree_node.tpl', 21, false),)), $this); ?>
				<?php if (isset($this->_foreach['recordInfo'])) unset($this->_foreach['recordInfo']);
$this->_foreach['recordInfo']['name'] = 'recordInfo';
$this->_foreach['recordInfo']['total'] = count($_from = (array)$this->_tpl_vars['node']);
$this->_foreach['recordInfo']['show'] = $this->_foreach['recordInfo']['total'] > 0;
if ($this->_foreach['recordInfo']['show']):
$this->_foreach['recordInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Record']):
        $this->_foreach['recordInfo']['iteration']++;
        $this->_foreach['recordInfo']['first'] = ($this->_foreach['recordInfo']['iteration'] == 1);
        $this->_foreach['recordInfo']['last']  = ($this->_foreach['recordInfo']['iteration'] == $this->_foreach['recordInfo']['total']);
?>
				<tr class="<?php echo smarty_function_cycle(array('values' => 'list_A, list_B'), $this);?>
">
					<td align="<?php echo ((is_array($_tmp=@$this->_tpl_vars['Field']['Align'])) ? $this->_run_mod_handler('default', true, $_tmp, 'left') : smarty_modifier_default($_tmp, 'left')); ?>
">
						&nbsp;&nbsp;
						<?php if ($this->_tpl_vars['level'] > 0): ?>
							&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ((is_array($_tmp='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')) ? $this->_run_mod_handler('str_repeat', true, $_tmp, $this->_tpl_vars['level']-1) : str_repeat($_tmp, $this->_tpl_vars['level']-1)); ?>
&brvbar;---
						<?php endif; ?>
						<?php echo $this->_tpl_vars['Record']['page_title']; ?>

					</td>
					<td align="center" nowrap>
						<a href="../index.php?pid=<?php echo $this->_tpl_vars['Record']['page_id']; ?>
" target="_blank"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_view.gif" class="imgAction" title="View" ></a>
						<a href="?Action=Edit&pid=<?php echo $this->_tpl_vars['Record']['page_id']; ?>
"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_edit.gif" class="imgAction" title="Edit" ></a>
						<?php if ($this->_tpl_vars['Record']['page_default'] == 'No'): ?>
							<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: Delete_Click('<?php echo $this->_tpl_vars['Record']['page_id']; ?>
');">
						<?php else: ?>
							&nbsp;&nbsp;&nbsp;&nbsp;
						<?php endif; ?>
					</td>
				</tr>
				<?php if ($this->_tpl_vars['Record']['child'] != ''): ?>
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'tree_node.tpl', 'smarty_include_vars' => array('node' => ((is_array($_tmp='child')) ? $this->_run_mod_handler('array_value', true, $_tmp, $this->_tpl_vars['Record']) : smarty_modifier_array_value($_tmp, $this->_tpl_vars['Record'])),'level' => ($this->_tpl_vars['level']+1))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				<?php endif; ?>
				<?php endforeach; unset($_from); else: ?>
				<tr class="list_A">
					<td colspan="6" align="center">No <?php echo $this->_tpl_vars['L_Module']; ?>
 information available.</td>
				</tr>
				<?php endif; ?>