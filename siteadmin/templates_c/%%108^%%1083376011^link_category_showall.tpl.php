<?php /* Smarty version 2.6.0, created on 2015-12-27 22:52:26
         compiled from link_category_showall.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'capitalize', 'link_category_showall.tpl', 51, false),)), $this); ?>
<script language="javascript">
	var msg_CatDel		= '<?php echo $this->_tpl_vars['msg_CatDel']; ?>
';
</script>

<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
<form name="frmCategory" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%"><?php echo $this->_tpl_vars['L_Link_Manager']; ?>
</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<?php echo $this->_tpl_vars['L_Manage_Links']; ?>

					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;<?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
				  <td bgcolor="#FFFFFF">
				  
						<table border="0" align="center" cellpadding="0" cellspacing="0" width="740" >
							<tr>
								<td valign="top" align="center">
									<table border="0" cellpadding="0" cellspacing="1" width="95%">
										<tr><td colspan="4" class="stdSection" align="right">
											<a href="javascript: Add_Click();"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_add.gif" class="imgAction" title="Add Page"></a>
										<tr><td colspan="4" class="successMsg" align="center"><?php echo $this->_tpl_vars['Message']; ?>
</td></tr>
										<tr>
											<td class="listHeader"><?php echo $this->_tpl_vars['L_Category_Name']; ?>
</td>
											<td class="listHeader" colspan="3"><?php echo $this->_tpl_vars['L_Action']; ?>
</td>
										</tr>
										<?php if ($this->_tpl_vars['No_Record_Found']): ?>
										<tr><td colspan="4">&nbsp;</td></tr>
										<tr><td colspan="4" align="center" class="errorMsg"><?php echo $this->_tpl_vars['No_Record_Found']; ?>
</td></tr>
										<tr><td colspan="4">&nbsp;</td></tr>										
										<?php endif; ?>
										
										<?php if (isset($this->_foreach['LinkInfo'])) unset($this->_foreach['LinkInfo']);
$this->_foreach['LinkInfo']['name'] = 'LinkInfo';
$this->_foreach['LinkInfo']['total'] = count($_from = (array)$this->_tpl_vars['LinkInfo']);
$this->_foreach['LinkInfo']['show'] = $this->_foreach['LinkInfo']['total'] > 0;
if ($this->_foreach['LinkInfo']['show']):
$this->_foreach['LinkInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Link']):
        $this->_foreach['LinkInfo']['iteration']++;
        $this->_foreach['LinkInfo']['first'] = ($this->_foreach['LinkInfo']['iteration'] == 1);
        $this->_foreach['LinkInfo']['last']  = ($this->_foreach['LinkInfo']['iteration'] == $this->_foreach['LinkInfo']['total']);
?>
										<tr class="list_B">
											<td class="list_C"><?php echo ((is_array($_tmp=$this->_tpl_vars['Link']['cat_name'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</td>
											<td width="15%" class="list_C" align="center">
												<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_edit.gif" class="imgAction" title="Edit" onClick="javascript: Edit_Click('<?php echo $this->_tpl_vars['Link']['cat_id']; ?>
');">
											</td>
											<td width="15%" class="list_C" align="center">
												<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_delete.gif" class="imgAction" title="Delete" onClick="javascript: Delete_Click('<?php echo $this->_tpl_vars['Link']['cat_id']; ?>
');">
											</td>
											<td width="15%" align="center" class="list_C"><a href="javascript: Add_Link_Click('<?php echo $this->_tpl_vars['Link']['cat_id']; ?>
');" class="actionLink"><?php echo $this->_tpl_vars['L_Link_Add_Link']; ?>
</a></td>
										</tr>
										
											<?php if (isset($this->_foreach['LinkDtlInfo'])) unset($this->_foreach['LinkDtlInfo']);
$this->_foreach['LinkDtlInfo']['name'] = 'LinkDtlInfo';
$this->_foreach['LinkDtlInfo']['total'] = count($_from = (array)$this->_tpl_vars['LinkDtlInfo']);
$this->_foreach['LinkDtlInfo']['show'] = $this->_foreach['LinkDtlInfo']['total'] > 0;
if ($this->_foreach['LinkDtlInfo']['show']):
$this->_foreach['LinkDtlInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['LinkDtl']):
        $this->_foreach['LinkDtlInfo']['iteration']++;
        $this->_foreach['LinkDtlInfo']['first'] = ($this->_foreach['LinkDtlInfo']['iteration'] == 1);
        $this->_foreach['LinkDtlInfo']['last']  = ($this->_foreach['LinkDtlInfo']['iteration'] == $this->_foreach['LinkDtlInfo']['total']);
?>
											<?php if ($this->_tpl_vars['LinkDtl']['link_category_id'] == $this->_tpl_vars['Link']['cat_id']): ?>
											<tr>
												<td colspan="4" align="center">
													<table border="0" cellpadding="0" cellspacing="0" width="90%">
														<tr class="list_C">
															<td><?php echo ((is_array($_tmp=$this->_tpl_vars['LinkDtl']['link_title'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</td>
															<td width="15%" align="center">
																<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_edit.gif" class="imgAction" title="Edit" onClick="javascript: Edit_Link_Click('<?php echo $this->_tpl_vars['LinkDtl']['link_id']; ?>
');">
															</td>
															<td width="15%" align="center" colspan="2">
																<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_delete.gif" class="imgAction" title="Delete" onClick="javascript: Delete_Link_Click('<?php echo $this->_tpl_vars['LinkDtl']['link_id']; ?>
');">
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<?php endif; ?>
											<?php endforeach; unset($_from); endif; ?>
										
										<?php endforeach; unset($_from); endif; ?>
										
										<tr><td colspan="4">&nbsp;</td></tr>
										<tr>
											<td colspan="4" class="pageLink" align="right"><?php echo $this->_tpl_vars['Page_Link']; ?>
</td>
										</tr>
										<input type="hidden" name="cat_id" value="<?php echo $this->_tpl_vars['cat_id']; ?>
">
										<input type="hidden" name="link_id" value="<?php echo $this->_tpl_vars['link_id']; ?>
">
										<input type="hidden" name="Action">
										<input type="hidden" name="start" value="<?php echo $this->_tpl_vars['Start']; ?>
">
									</table>
								</td>
							</tr>
						</table>				  
				  
					</td>
				</tr>

			</table>
		</td>
	</tr>
	</form>
</table>




