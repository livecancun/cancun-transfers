<?php /* Smarty version 2.6.0, created on 2010-07-12 20:13:31
         compiled from destination_addedit.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'destination_addedit.tpl', 39, false),)), $this); ?>
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmDestination" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%"><?php echo $this->_tpl_vars['L_Destination_Manager']; ?>
 [<?php echo $this->_tpl_vars['Action']; ?>
]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<?php echo $this->_tpl_vars['L_Design_Content']; ?>

					</td>
				</tr>
				<tr><td height="5"></td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td align="left">
						<input type="submit" name="Submit" value="Save" class="stdButton" onclick="JavaScript: return Validate_Form(document.frmDestination);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
					<td align="right" class="mandatory"><?php echo $this->_tpl_vars['L_Mandatory_Fields']; ?>
</td>
				</tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						<?php echo $this->_tpl_vars['L_Destination_Info']; ?>

					</td>
				</tr>
			</table>
			<?php if (isset($this->_foreach['LanguageInfo'])) unset($this->_foreach['LanguageInfo']);
$this->_foreach['LanguageInfo']['name'] = 'LanguageInfo';
$this->_foreach['LanguageInfo']['total'] = count($_from = (array)$this->_tpl_vars['Language']);
$this->_foreach['LanguageInfo']['show'] = $this->_foreach['LanguageInfo']['total'] > 0;
if ($this->_foreach['LanguageInfo']['show']):
$this->_foreach['LanguageInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Lan']):
        $this->_foreach['LanguageInfo']['iteration']++;
        $this->_foreach['LanguageInfo']['first'] = ($this->_foreach['LanguageInfo']['iteration'] == 1);
        $this->_foreach['LanguageInfo']['last']  = ($this->_foreach['LanguageInfo']['iteration'] == $this->_foreach['LanguageInfo']['total']);
?>
			 <?php echo smarty_function_assign(array('var' => 'sr','value' => $this->_foreach['LanguageInfo']['iteration']-1), $this);?>
 
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr id='pageTitle_<?php echo $this->_foreach['LanguageInfo']['iteration']; ?>
' style="display:<?php if ($this->_tpl_vars['rsPage']['page_type'] == 'iFrame' || $this->_tpl_vars['rsPage']['page_type'] == 'OutsideLink'): ?>none<?php else: ?>else<?php endif; ?>;">
					<td width="25%" class="fieldLabelLeft" valign="top"><?php echo $this->_tpl_vars['L_Destination_Title']; ?>
 : (<i><?php echo $this->_tpl_vars['Lan']['name']; ?>
</i>)<font class="mandatoryMark">*</font></td>
					<td width="75%" class="fieldInputStyle">
						<input type="text" value="<?php echo $this->_tpl_vars['rsDestination']['dest_title'][$this->_tpl_vars['sr']]; ?>
" name="dest_title[]" size="40" maxlength="120">
					</td>
				</tr>
			</table>
					
			<?php endforeach; unset($_from); endif; ?>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td class="divider" colspan="4"></td></tr>
				<tr>
				  <td align="left" colspan="2">
						<input type="submit" name="Submit" value="Save" class="stdButton" onclick="JavaScript: return Validate_Form(document.frmDestination);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
						<input type="hidden" name="dest_id" value="<?php echo $this->_tpl_vars['dest_id']; ?>
">
						<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['Action']; ?>
">
						<input type="hidden" name="lang_cnt" value="<?php echo $this->_foreach['LanguageInfo']['total']; ?>
" />
					</td>
				</tr>
			</table>	
			<br>
		</td>
	</tr>
	</form>
</table>