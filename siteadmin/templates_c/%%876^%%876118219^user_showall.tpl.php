<?php /* Smarty version 2.6.0, created on 2018-07-23 11:11:21
         compiled from user_showall.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'user_showall.tpl', 46, false),array('function', 'cycle', 'user_showall.tpl', 49, false),)), $this); ?>
<script>
	var Confirm_Delete = '<?php echo $this->_tpl_vars['Confirm_Delete']; ?>
';
</script>


<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%"><?php echo $this->_tpl_vars['L_Reservation']; ?>
</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<?php echo $this->_tpl_vars['L_Manage_Reservation']; ?>

					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;<?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
			</table>
<table border="0" cellpadding="0" cellspacing="0" width="750" align="center">
<form name="frmUser" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr><td align="center" class="successMsg"><?php echo $this->_tpl_vars['Message']; ?>
</td></tr>
	<tr><td align="center" class="errorMsg"><?php echo $this->_tpl_vars['ErrorMessage']; ?>
</td></tr>
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr><td colspan="7" height="5"></td></tr>
                <tr>
					<td class="listHeader" width="8%" height="20" align="center"><?php echo $this->_tpl_vars['SrNo']; ?>
</td>
					<td class="listHeader" width="20%" align="center"><?php echo $this->_tpl_vars['L_Email']; ?>
</td>
                   	<td class="listHeader" width="15%" align="center"><?php echo $this->_tpl_vars['L_Name']; ?>
</td>
					<td class="listHeader" width="20%" align="center"><?php echo $this->_tpl_vars['L_ConfirmationNo']; ?>
</td>
                   	<td class="listHeader" width="37%" colspan="3" align="center"><?php echo $this->_tpl_vars['Action']; ?>
</td>
                </tr>
                <tr><td colspan="7" height="5" class="successMsg" align="center"><?php echo $this->_tpl_vars['Notfound_Message']; ?>
</td></tr>
					
					<?php if (isset($this->_foreach['UserInfo'])) unset($this->_foreach['UserInfo']);
$this->_foreach['UserInfo']['name'] = 'UserInfo';
$this->_foreach['UserInfo']['total'] = count($_from = (array)$this->_tpl_vars['UserInfo']);
$this->_foreach['UserInfo']['show'] = $this->_foreach['UserInfo']['total'] > 0;
if ($this->_foreach['UserInfo']['show']):
$this->_foreach['UserInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Users']):
        $this->_foreach['UserInfo']['iteration']++;
        $this->_foreach['UserInfo']['first'] = ($this->_foreach['UserInfo']['iteration'] == 1);
        $this->_foreach['UserInfo']['last']  = ($this->_foreach['UserInfo']['iteration'] == $this->_foreach['UserInfo']['total']);
?>
					 <?php echo smarty_function_assign(array('var' => 'Sr_No','value' => $this->_foreach['UserInfo']['iteration']), $this);?>
 
					 
					 <?php if ($this->_tpl_vars['Users']['user_id']): ?>
					<tr class="<?php echo smarty_function_cycle(array('values' => 'list_A, list_B'), $this);?>
">
						<td class="List_B" align="center" height="20" width="8%"><?php echo $this->_tpl_vars['Sr_No']; ?>
</td>
						<td class="List_B" align="center" width="20%"><?php echo $this->_tpl_vars['Users']['email']; ?>
</td>
						<td class="List_B" align="center" width="15%"><?php echo $this->_tpl_vars['Users']['firstname']; ?>
 <?php echo $this->_tpl_vars['Users']['lastname']; ?>
</td>
						<td class="List_B" align="center" width="20%" ><?php echo $this->_tpl_vars['Users']['confirmationNo']; ?>
</td>
						<td class="List_B" align="center" width="12%">
							<a href="JavaScript: Show_Click('<?php echo $this->_tpl_vars['Users']['index_id']; ?>
');" class="actionLink"><?php echo $this->_tpl_vars['L_User_Details']; ?>
</a>
						</td>
						<td class="List_B" align="center" width="18%">
							<a href="JavaScript: Reserve_Click('<?php echo $this->_tpl_vars['Users']['confirmationNo']; ?>
');" class="actionLink"><?php echo $this->_tpl_vars['L_Reservation_Details']; ?>
</a>
						</td>
						<td class="List_B" align="center" width="12%">
							<a href="JavaScript: Delete_Click('<?php echo $this->_tpl_vars['Users']['index_id']; ?>
','<?php echo $this->_tpl_vars['Users']['confirmationNo']; ?>
');" class="actionLink"><?php echo $this->_tpl_vars['Delete']; ?>
</a>
						</td>
					</tr>
					<?php endif; ?>
					<?php endforeach; unset($_from); endif; ?>
					
                <tr><td colspan="7">&nbsp;</td></tr>
			</table>
			<br>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr height="15">
					<td class="paggingLine" width="10%">
						&nbsp;&nbsp;<?php echo $this->_tpl_vars['Page_Link']; ?>

					</td>
				</tr>
			</table>
			<br>
			<input type="hidden" name="user_id">
			<input type="hidden" name="confirmationNo">
			<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
		</td>
	</tr>
	<tr><td><br></td></tr>
	</form>
</table>
		</td>
	</tr>

</table>

