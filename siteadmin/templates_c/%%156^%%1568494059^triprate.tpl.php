<?php /* Smarty version 2.6.0, created on 2016-04-19 17:57:11
         compiled from triprate.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'triprate.tpl', 18, false),array('function', 'cycle', 'triprate.tpl', 29, false),array('modifier', 'default', 'triprate.tpl', 23, false),array('modifier', 'array_value', 'triprate.tpl', 34, false),)), $this); ?>
<form name="frmRate" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
<table border="0" cellpadding="1" cellspacing="1" width="95%">
	<tr><td align="right" colspan="3" class="mandatory"><?php echo $this->_tpl_vars['L_No_Gaps']; ?>
</td></tr>
	<tr>
		<td class="listHeader" width="25%"><?php echo $this->_tpl_vars['L_Destinations']; ?>
</td>
<!--		
		<td class="listHeader" width="10%" align="center">
			Passenger Range<br>
			<input name="passenger_starting_range[]" value="<?php echo $this->_tpl_vars['Record']['passenger_starting_range']; ?>
" size="5" readonly="true"/> -	
			<input name="passenger_ending_range[]" value="<?php echo $this->_tpl_vars['Record']['passenger_ending_range']; ?>
" size="5" onkeypress="isNumericKey()"  onchange="setStartRange(document.frmRate,'<?php echo $this->_foreach['DestinationInfo']['iteration']; ?>
')"/>
		</td>
-->		
		<?php if (isset($this->_foreach['PassengerRange'])) unset($this->_foreach['PassengerRange']);
$this->_foreach['PassengerRange']['name'] = 'PassengerRange';
$this->_foreach['PassengerRange']['total'] = count($_from = (array)$this->_tpl_vars['PassengerRange']);
$this->_foreach['PassengerRange']['show'] = $this->_foreach['PassengerRange']['total'] > 0;
if ($this->_foreach['PassengerRange']['show']):
$this->_foreach['PassengerRange']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Passenger']):
        $this->_foreach['PassengerRange']['iteration']++;
        $this->_foreach['PassengerRange']['first'] = ($this->_foreach['PassengerRange']['iteration'] == 1);
        $this->_foreach['PassengerRange']['last']  = ($this->_foreach['PassengerRange']['iteration'] == $this->_foreach['PassengerRange']['total']);
?>
		<td class="listHeader" width="15%" align="center"><?php echo $this->_tpl_vars['L_Passenger_Range']; ?>
<br>
			<input name="passenger_starting_range[]" value="<?php echo $this->_tpl_vars['Passenger']['passenger_starting_range']; ?>
" size="5" readonly="true"/> -	
			<input name="passenger_ending_range[]" value="<?php echo $this->_tpl_vars['Passenger']['passenger_ending_range']; ?>
" size="5" onkeypress="isNumericKey()"  onchange="setStartRange(document.frmRate,'<?php echo $this->_foreach['PassengerRange']['iteration']; ?>
')"/>
		</td>
		<?php if ($this->_foreach['PassengerRange']['last']):  echo smarty_function_assign(array('var' => 'RangeStart','value' => $this->_tpl_vars['Passenger']['passenger_ending_range']+1), $this); endif; ?>
		<?php endforeach; unset($_from); endif; ?>
		
		<td class="listHeader" width="15%" align="center">
			<?php echo $this->_tpl_vars['L_Add_New']; ?>
<br>
			<input name="passenger_start" id="passenger_start" size="5" value="<?php echo ((is_array($_tmp=@$this->_tpl_vars['RangeStart'])) ? $this->_run_mod_handler('default', true, $_tmp, '1') : smarty_modifier_default($_tmp, '1')); ?>
" readonly="true"/>	-	<input name="passenger_end" id="passenger_end" size="5" onkeypress="isNumericKey()"/>
		</td>
	</tr>
	
	<?php if (isset($this->_foreach['DestinationInfo'])) unset($this->_foreach['DestinationInfo']);
$this->_foreach['DestinationInfo']['name'] = 'DestinationInfo';
$this->_foreach['DestinationInfo']['total'] = count($_from = (array)$this->_tpl_vars['DestinationInfo']);
$this->_foreach['DestinationInfo']['show'] = $this->_foreach['DestinationInfo']['total'] > 0;
if ($this->_foreach['DestinationInfo']['show']):
$this->_foreach['DestinationInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Destination']):
        $this->_foreach['DestinationInfo']['iteration']++;
        $this->_foreach['DestinationInfo']['first'] = ($this->_foreach['DestinationInfo']['iteration'] == 1);
        $this->_foreach['DestinationInfo']['last']  = ($this->_foreach['DestinationInfo']['iteration'] == $this->_foreach['DestinationInfo']['total']);
?>
	
	<tr class="<?php echo smarty_function_cycle(array('values' => 'list_A, list_B'), $this);?>
">

	  	<td><?php echo $this->_tpl_vars['Destination']['dest_title']; ?>
&nbsp;&nbsp;</td>

	<?php if (isset($this->_foreach['R_RecordSet'])) unset($this->_foreach['R_RecordSet']);
$this->_foreach['R_RecordSet']['name'] = 'R_RecordSet';
$this->_foreach['R_RecordSet']['total'] = count($_from = (array)$this->_tpl_vars['R_RecordSet']);
$this->_foreach['R_RecordSet']['show'] = $this->_foreach['R_RecordSet']['total'] > 0;
if ($this->_foreach['R_RecordSet']['show']):
$this->_foreach['R_RecordSet']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Record']):
        $this->_foreach['R_RecordSet']['iteration']++;
        $this->_foreach['R_RecordSet']['first'] = ($this->_foreach['R_RecordSet']['iteration'] == 1);
        $this->_foreach['R_RecordSet']['last']  = ($this->_foreach['R_RecordSet']['iteration'] == $this->_foreach['R_RecordSet']['total']);
?>
	<?php echo smarty_function_assign(array('var' => 'PriceData','value' => ((is_array($_tmp='price_data')) ? $this->_run_mod_handler('array_value', true, $_tmp, $this->_tpl_vars['Record']) : smarty_modifier_array_value($_tmp, $this->_tpl_vars['Record']))), $this);?>

		<td valign="top" class="Text" width="15%">
			<table border="0" cellspacing="3" cellpadding="0" align="center">
				<tr>
				 <?php if (isset($this->_foreach['TripTypeInfo'])) unset($this->_foreach['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['name'] = 'TripTypeInfo';
$this->_foreach['TripTypeInfo']['total'] = count($_from = (array)$this->_tpl_vars['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['show'] = $this->_foreach['TripTypeInfo']['total'] > 0;
if ($this->_foreach['TripTypeInfo']['show']):
$this->_foreach['TripTypeInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['TripType']):
        $this->_foreach['TripTypeInfo']['iteration']++;
        $this->_foreach['TripTypeInfo']['first'] = ($this->_foreach['TripTypeInfo']['iteration'] == 1);
        $this->_foreach['TripTypeInfo']['last']  = ($this->_foreach['TripTypeInfo']['iteration'] == $this->_foreach['TripTypeInfo']['total']);
?>
					<td nowrap="nowrap" align="center">
						<?php echo $this->_tpl_vars['TripType']['triptype_title']; ?>
&nbsp;&nbsp;<br>
						<input name="price_<?php echo $this->_foreach['R_RecordSet']['iteration']; ?>
_<?php echo $this->_tpl_vars['Destination']['dest_id']; ?>
_<?php echo $this->_tpl_vars['TripType']['triptype_id']; ?>
" id="price_<?php echo $this->_tpl_vars['Record']['passenger_id']; ?>
_<?php echo $this->_tpl_vars['Destination']['dest_id']; ?>
_<?php echo $this->_tpl_vars['TripType']['triptype_id']; ?>
"  size="5" value="<?php echo $this->_tpl_vars['Record']['price_data'][$this->_tpl_vars['Destination']['dest_id']][$this->_tpl_vars['TripType']['triptype_id']]; ?>
" onkeypress="isNumericDotKey()"/>&nbsp;&nbsp;&nbsp;						
					</td>
				  <?php endforeach; unset($_from); endif; ?>
		   				  
				</tr>
		   </table>
 		</td>
	<?php endforeach; unset($_from); endif; ?>	

		<td valign="top" align="center">
			<table cellpadding="0" cellspacing="0">
				<tr class="list_C">
					<td valign="top" class="Text">
						<table border="0" cellspacing="3" cellpadding="0" align="center">
							<tr>
							 <?php if (isset($this->_foreach['TripTypeInfo'])) unset($this->_foreach['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['name'] = 'TripTypeInfo';
$this->_foreach['TripTypeInfo']['total'] = count($_from = (array)$this->_tpl_vars['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['show'] = $this->_foreach['TripTypeInfo']['total'] > 0;
if ($this->_foreach['TripTypeInfo']['show']):
$this->_foreach['TripTypeInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['TripType']):
        $this->_foreach['TripTypeInfo']['iteration']++;
        $this->_foreach['TripTypeInfo']['first'] = ($this->_foreach['TripTypeInfo']['iteration'] == 1);
        $this->_foreach['TripTypeInfo']['last']  = ($this->_foreach['TripTypeInfo']['iteration'] == $this->_foreach['TripTypeInfo']['total']);
?>
								<td nowrap="nowrap" align="center">
									<?php echo $this->_tpl_vars['TripType']['triptype_title']; ?>
&nbsp;&nbsp;<br>
									<input name="price_<?php echo $this->_tpl_vars['Destination']['dest_id']; ?>
_<?php echo $this->_tpl_vars['TripType']['triptype_id']; ?>
" id="price_<?php echo $this->_tpl_vars['Destination']['dest_id']; ?>
_<?php echo $this->_tpl_vars['TripType']['triptype_id']; ?>
"  size="5" onkeypress="isNumericDotKey()"/>&nbsp;&nbsp;
								</td>
							  <?php endforeach; unset($_from); endif; ?>
									  
							</tr>
					   </table>
					</td>
				</tr>
			</table>
		</td>
	 </tr>
	

	<?php endforeach; unset($_from); endif; ?>
	
	<tr bgcolor="#B5D7EF">

		<td align="center" width="10%">
			<!--<?php if ($this->_tpl_vars['Passenger']['passenger_id']): ?>		
				<b>Action</b>
			<?php endif; ?>-->
		</td>
		
		<?php if (isset($this->_foreach['PassengerRange'])) unset($this->_foreach['PassengerRange']);
$this->_foreach['PassengerRange']['name'] = 'PassengerRange';
$this->_foreach['PassengerRange']['total'] = count($_from = (array)$this->_tpl_vars['PassengerRange']);
$this->_foreach['PassengerRange']['show'] = $this->_foreach['PassengerRange']['total'] > 0;
if ($this->_foreach['PassengerRange']['show']):
$this->_foreach['PassengerRange']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Passenger']):
        $this->_foreach['PassengerRange']['iteration']++;
        $this->_foreach['PassengerRange']['first'] = ($this->_foreach['PassengerRange']['iteration'] == 1);
        $this->_foreach['PassengerRange']['last']  = ($this->_foreach['PassengerRange']['iteration'] == $this->_foreach['PassengerRange']['total']);
?>
		<td valign="middle" align="center">
			<input name="Delete" type="submit" value="Delete" onclick="JavaScript: CDelete_Click(document.frmRate, '<?php echo $this->_tpl_vars['Passenger']['passenger_id']; ?>
');" class="greyButton"/>
<!--		<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: CDelete_Click(document.frmRate, '<?php echo $this->_tpl_vars['Passenger']['passenger_id']; ?>
');">-->
		</td>				
		<?php endforeach; unset($_from); endif; ?>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="divider"></td>
		<td class="divider" colspan="<?php echo $this->_foreach['PassengerRange']['iteration']; ?>
"></td>
		<td class="divider"></td>		
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td>&nbsp;</td>
		<?php if ($this->_tpl_vars['Passenger']['passenger_id']): ?>
		<td align="right" colspan="<?php echo $this->_foreach['PassengerRange']['iteration']; ?>
">
			<input name="Update" type="submit" value="Update" onclick="return CUpdate_Click(document.frmRate)" class="stdButton" />			
			<input name="Reset" type="reset" value="Reset" class="stdButton"/>
		</td>
		<?php endif; ?>	
		<td align="right">
			<input type="submit" name="Submit" value="Add" onclick="return CAdd_Click(document.frmRate)" class="stdButton">
		</td>
		<td valign="top">
			<input type="hidden" name="dest_cnt" value="<?php echo $this->_foreach['DestinationInfo']['total']; ?>
"/>
			<input type="hidden" name="triptype_cnt" value="<?php echo $this->_foreach['TripTypeInfo']['total']; ?>
"/>
			<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['Action']; ?>
">
			<input type="hidden" name="pk">
		</td>		
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>

</table>
</form>