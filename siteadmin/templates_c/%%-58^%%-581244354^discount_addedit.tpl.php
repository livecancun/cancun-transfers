<?php /* Smarty version 2.6.0, created on 2009-11-03 15:26:29
         compiled from discount_addedit.tpl */ ?>
<script language="javascript">
	var Empty_Discount_For   = '<?php echo $this->_tpl_vars['Empty_Discount_For']; ?>
';
	var Empty_Discount_Rate  = '<?php echo $this->_tpl_vars['Empty_Discount_Rate']; ?>
';
	var Valid_Discount_Rate  = '<?php echo $this->_tpl_vars['Valid_Discount_Rate']; ?>
';
</script>


<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
<form name="frmTravelDiscount" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%"><?php echo $this->_tpl_vars['L_Discount_Manager']; ?>
 (<?php echo $this->_tpl_vars['Add'];  echo $this->_tpl_vars['Edit']; ?>
)</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<?php echo $this->_tpl_vars['L_Manage_Discount']; ?>

					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;<?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
			</table>
			<table border="0" align="center" cellpadding="0" cellspacing="0" width="95%">
	<tr>
    	<td>
        	<table border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
               <tr><td colspan="5" height="5"></td></tr>	
			   <tr>
		          <td align="left">
                      <table border="0" cellpadding="2" cellspacing="2" width="98%">	
							<tr><td align="center" class="errorMsg" colspan="2"><?php echo $this->_tpl_vars['ErrorMsg']; ?>
</td></tr>
							<tr>
								<td class="fieldLabelLeft" valign="top" width="15%"><?php echo $this->_tpl_vars['L_Discount_For']; ?>
 (English) : </td>
								<td class="fieldInputStyle"> 
									<input type="text" name="discount_for" size="60" value="<?php echo $this->_tpl_vars['discount_for']; ?>
" maxlength="50">
								</td>
							</tr>	
							
							<tr>
								<td class="fieldLabelLeft" valign="top" width="15%"><?php echo $this->_tpl_vars['L_Discount_For']; ?>
 (Espanol) : </td>
								<td class="fieldInputStyle"> 
									<input type="text" name="discount_for_sp" size="60" value="<?php echo $this->_tpl_vars['discount_for_sp']; ?>
" maxlength="50">
								</td>
							</tr>	

							<tr>
								<td class="fieldLabelLeft" valign="top" width="20%"><?php echo $this->_tpl_vars['L_Discount_Rate']; ?>
 : </td>
								<td class="fieldInputStyle"> 		
									<input class="price" type="text" name="discount_rate" size="60" value="<?php echo $this->_tpl_vars['discount_rate']; ?>
" maxlength="50" onKeyPress="javascript: isNumericKey(this.value)">
									%
								</td>
							</tr>		
							<tr>
								<td height="21" colspan="2">&nbsp;</td>
							</tr>
	                </table>
				</td>
			</tr>
		</table>
	 </td>
 </tr>
</table>
 <br>
<table border="0" cellpadding="0" cellspacing="1" width="100%">
	<tr>
		 <td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF">
				<tr>
					<td align="center">
						<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Save']; ?>
" class="nrlButton" onclick="javascript:return Form_Submit(document.frmTravelDiscount);">&nbsp;&nbsp;
						<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Cancel']; ?>
" class="nrlButton">
						<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
						<input type="hidden" name="discount_id" value="<?php echo $this->_tpl_vars['discount_id']; ?>
">
					</td>
				</tr>
			</table>
		 </td>
	</tr>
</table>
		</td>
	</tr>
	</form>
</table>