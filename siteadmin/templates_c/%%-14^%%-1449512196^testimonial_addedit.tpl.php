<?php /* Smarty version 2.6.0, created on 2010-02-02 19:07:13
         compiled from testimonial_addedit.tpl */ ?>
<script>
	var Empty_Name		= '<?php echo $this->_tpl_vars['Empty_Name']; ?>
';
	var Empty_Company  	= '<?php echo $this->_tpl_vars['Empty_Company']; ?>
';
	var Valid_Zip 		= '<?php echo $this->_tpl_vars['Valid_Zip']; ?>
';
	var Empty_Phone		= '<?php echo $this->_tpl_vars['Empty_Phone']; ?>
';
	var Valid_Phone  	= '<?php echo $this->_tpl_vars['Valid_Phone']; ?>
';
	var Empty_Fax 		= '<?php echo $this->_tpl_vars['Empty_Fax']; ?>
';
	var Valid_Fax 		= '<?php echo $this->_tpl_vars['Valid_Fax']; ?>
';
	var Empty_TollFree 	= '<?php echo $this->_tpl_vars['Empty_TollFree']; ?>
';
	var Valid_TollFree 	= '<?php echo $this->_tpl_vars['Valid_TollFree']; ?>
';
	var Empty_Email 	= '<?php echo $this->_tpl_vars['Empty_Email']; ?>
';
	var Valid_Email 	= '<?php echo $this->_tpl_vars['Valid_Email']; ?>
';
	var Valid_URL 		= '<?php echo $this->_tpl_vars['Valid_URL']; ?>
';
	var Valid_Photo 	= '<?php echo $this->_tpl_vars['Valid_Photo']; ?>
';			
	var Empty_Description	= '<?php echo $this->_tpl_vars['Empty_Description']; ?>
';
	var Empty_Comment 	= '<?php echo $this->_tpl_vars['Empty_Comment']; ?>
';	

	var Delete_Testimonial 		= '<?php echo $this->_tpl_vars['Delete_Testimonial']; ?>
';	
	var Delete_AllTestimonials 	= '<?php echo $this->_tpl_vars['Delete_AllTestimonials']; ?>
';	
</script>

<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="100%"> 
	<form name="frmTestimonial" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post" enctype="multipart/form-data"> 
	<tr> 
		<td class="stdSectionHeader"> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%"> 
				<tr> 
					<td class="stdSection"><?php echo $this->_tpl_vars['Testimonial_Header']; ?>
</td> 
					<td class="stdSection" align="right"><a href="testimonial.php" class="otherLink"><?php echo $this->_tpl_vars['Back']; ?>
</a></td>
				</tr> 
			</table>
		</td> 
	</tr> 
	<tr> 
		<td valign="top" align="center"> 
			<table border="0" cellpadding="2" cellspacing="2" width="97%"> 
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr> 
					<td class="fieldLabelRight" valign="top" width="15%"><?php echo $this->_tpl_vars['Name']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_name" value="<?php echo $this->_tpl_vars['Person_Name']; ?>
" size="35" maxlength="50"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Address']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><textarea name="person_address" rows="5" cols="50"><?php echo $this->_tpl_vars['Person_Address']; ?>
</textarea></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['City']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_city" value="<?php echo $this->_tpl_vars['Person_City']; ?>
" size="25" maxlength="25"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['State']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_state" value="<?php echo $this->_tpl_vars['Person_State']; ?>
" size="10" maxlength="5"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Country']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><SELECT name="person_country" class="texto_content"><option value="0">-----------<?php echo $this->_tpl_vars['L_Select_Country']; ?>
--------</option><?php echo $this->_tpl_vars['CountryList']; ?>
</SELECT></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Zip']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_zip" value="<?php echo $this->_tpl_vars['Person_Zip']; ?>
" size="10" maxlength="5" onKeyPress="javascript: isNumericKey(this.value)"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Phone']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3">
						<input type="text" name="person_phone_areacode" value="<?php echo $this->_tpl_vars['Person_Phone_Areacode']; ?>
" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
						<input type="text" name="person_phone_citycode" value="<?php echo $this->_tpl_vars['Person_Phone_Citycode']; ?>
" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
						<input type="text" name="person_phone_no" value="<?php echo $this->_tpl_vars['Person_Phone_No']; ?>
" size="4" maxlength="4" onKeyPress="javascript: isNumericKey(this.value)">
					</td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Fax']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3">
						<input type="text" name="person_fax_areacode" value="<?php echo $this->_tpl_vars['Person_Fax_Areacode']; ?>
" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
						<input type="text" name="person_fax_citycode" value="<?php echo $this->_tpl_vars['Person_Fax_Citycode']; ?>
" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
						<input type="text" name="person_fax_no" value="<?php echo $this->_tpl_vars['Person_Fax_No']; ?>
" size="4" maxlength="4" onKeyPress="javascript: isNumericKey(this.value)">
					</td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Email']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_email" value="<?php echo $this->_tpl_vars['Person_Email']; ?>
" size="35" maxlength="255"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top" width="22%"><?php echo $this->_tpl_vars['Website']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="text" name="person_website" value="<?php echo $this->_tpl_vars['Person_Website']; ?>
" size="50" maxlength="255"></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Photo']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3">
						<input type="file" name="person_pic" size="25" maxlength="255" <?php if ($this->_tpl_vars['Picture_Filename']): ?> onchange="javascript: UploadImage_Change(this, pic, '<?php echo $this->_tpl_vars['Picture_Filename']; ?>
','');" <?php endif; ?>>
						<input type="hidden" name="person_pic_edit" value="<?php echo $this->_tpl_vars['Person_Pic']; ?>
"><br>
						<img src="<?php echo $this->_tpl_vars['Picture_Filename']; ?>
" id="pic" border="0"><br>
						<?php if ($this->_tpl_vars['Person_Pic'] != ''): ?>
						<input type="checkbox" name="delete_picture" value="1" class="stdCheckBox"> <?php echo $this->_tpl_vars['Delete_Picture']; ?>

						<?php endif; ?>
					</td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Comment']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><textarea name="person_comment" rows="5" cols="50"><?php echo $this->_tpl_vars['Person_Comment']; ?>
</textarea></td> 
				</tr> 
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Display_On_Website']; ?>
 :</td> 
					<td class="fieldInputStyle" colspan="3"><input type="checkbox" name="person_status" value="1" <?php echo $this->_tpl_vars['Is_Status_Check']; ?>
></td> 
				</tr> 
				<tr> 
					<td></td> 
					<td>
						<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Save']; ?>
" class="stdButton" onClick="javascript: return Form_Submit(document.frmTestimonial);"> 
						&nbsp;&nbsp;<input type="button" name="Submit" value="<?php echo $this->_tpl_vars['Cancel']; ?>
" class="stdButton" onClick="javascript: Cancel_Click('<?php echo $this->_tpl_vars['Cancel_Action']; ?>
');">&nbsp;&nbsp;
						<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
						<input type="hidden" name="person_id" value="<?php echo $this->_tpl_vars['Person_Id']; ?>
">
					</td> 
				</tr> 
			</table>
		</td> 
	</tr> 
	<tr> 
		<td>&nbsp;</td> 
	</tr> 
	</form> 
</table>