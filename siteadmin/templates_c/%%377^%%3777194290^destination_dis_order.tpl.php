<?php /* Smarty version 2.6.0, created on 2015-12-27 22:52:04
         compiled from destination_dis_order.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'destination_dis_order.tpl', 25, false),)), $this); ?>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="stdTableBorder" height="100%">
  <form name="frmDestination" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
    <tr>
      <td class="stdSectionHeader"><table border="0" cellpadding="0" cellspacing="0" width="100%">
          <tr>
            <td class="stdSection">&nbsp;Destination Display Order Management</td>
            <td align="right"><a href="menu_settings.php" class="otherLink">Back</a>&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td align="center"><table border="0" cellpadding="3" cellspacing="2">
           <tr>
            <td class="successmsg" colspan="2"><?php echo $this->_tpl_vars['SuccMessage']; ?>
</td>
          </tr>
           <tr>
            <td  colspan="2">&nbsp;</td>
          </tr>
		  <tr>
            <td  class="fieldLabelRight" valign="top">Destination Title</td>
            <td  class="fieldLabelLeft" valign="top">
			<select name="sort_data" size="10" style="width:150;">
        

<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['SortData_List']), $this);?>



              </select>
            </td>
            <td  align="left" ><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
top.gif" style="cursor:hand" border="0" onClick="JavaScript: SortUpDown_Click(-1);"><br>
              <br>
              <img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
bottom.gif" style="cursor:hand" border="0" onClick="JavaScript: SortUpDown_Click(1);"> </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input type="hidden" name="sorted_list">
              <input type="submit" name="Submit"  value="Save"   class="stdButton" onClick="javascript: Sort_Form_Submit(document.frmDestination,'Sort');">
              <input type="submit" name="Submit" value="Cancel" class="stdButton">
              <input type="hidden" name="dest_id" value="<?php echo $this->_tpl_vars['dest_id']; ?>
">
              <input type="hidden" name="Action" id="Action" value="<?php echo $this->_tpl_vars['Action']; ?>
">
            </td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td valign="top" align="center">&nbsp;</td>
    </tr>
  </form>
</table>