<?php /* Smarty version 2.6.0, created on 2010-07-12 20:05:05
         compiled from control/page.tpl */ ?>
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif"></td>
					<td class="stdSection" width="99%"><?php echo $this->_tpl_vars['ModuleTitle']; ?>
 <?php if ($this->_tpl_vars['Action'] != ''): ?> [<?php echo $this->_tpl_vars['Action']; ?>
] <?php endif; ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td height="5"></td></tr>
				<tr>
					<td>
						<?php echo $this->_tpl_vars['HelpText']; ?>

					</td>
				</tr>
				<tr><td height="5"></td></tr>
				<tr><td class="errorMsg" align="center"><?php echo $this->_tpl_vars['errorMessage']; ?>
</td></tr>
				<tr><td class="successMsg" align="center"><?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
			</table>
			<?php if ($this->_tpl_vars['searchForm']): ?>
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['searchForm'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				<br>
			<?php endif; ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['includeFile'], 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php if ($this->_tpl_vars['backUrl']): ?>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td class="successMsg" align="left">
						<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
t_back.gif"><a  href="<?php echo $this->_tpl_vars['backUrl']; ?>
" class="stdNormalLink"> Back</a>
					</td>
				</tr>
			</table>				
			<?php endif; ?>
			<br>
		</td>
	</tr>
</table>