<?php /* Smarty version 2.6.0, created on 2015-12-27 22:51:58
         compiled from destination_manage.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'destination_manage.tpl', 38, false),)), $this); ?>
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmDestination" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post"> 
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%"><?php echo $this->_tpl_vars['L_Destination_Manager']; ?>
</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<?php echo $this->_tpl_vars['L_Manage_Destinations']; ?>

					</td>
				</tr>
				<tr><td class="successMsg" align="center">&nbsp;<?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td colspan="4" align="right">
						<a href="<?php echo $_SERVER['PHP_SELF']; ?>
?Action=Add"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_add.gif" class="imgAction" title="Add Page"></a>&nbsp;&nbsp;
						<!--a href="<?php echo $_SERVER['PHP_SELF']; ?>
?Action=Sort"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_sort.gif" class="imgAction" title="Sort Pages"></a-->&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td width="2%" class="listHeader">&nbsp;</td>
					<td class="listHeader" width="71%"><?php echo $this->_tpl_vars['L_Destination_Title']; ?>
</td>
					<td class="listHeader" width="15%"><?php echo $this->_tpl_vars['L_Visible']; ?>
</td>
					<td class="listHeader" width="15%"><?php echo $this->_tpl_vars['L_Action']; ?>
</td>
				</tr>
				<?php if (isset($this->_foreach['DestinationInfo'])) unset($this->_foreach['DestinationInfo']);
$this->_foreach['DestinationInfo']['name'] = 'DestinationInfo';
$this->_foreach['DestinationInfo']['total'] = count($_from = (array)$this->_tpl_vars['DestinationInfo']);
$this->_foreach['DestinationInfo']['show'] = $this->_foreach['DestinationInfo']['total'] > 0;
if ($this->_foreach['DestinationInfo']['show']):
$this->_foreach['DestinationInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Destination']):
        $this->_foreach['DestinationInfo']['iteration']++;
        $this->_foreach['DestinationInfo']['first'] = ($this->_foreach['DestinationInfo']['iteration'] == 1);
        $this->_foreach['DestinationInfo']['last']  = ($this->_foreach['DestinationInfo']['iteration'] == $this->_foreach['DestinationInfo']['total']);
?>
				<tr class="<?php echo smarty_function_cycle(array('values' => 'list_A, list_B'), $this);?>
">
					<td><input type="checkbox" name="dest_id1[]" class="stdCheckBox" value="<?php echo $this->_tpl_vars['Destination']->dest_id; ?>
"></td>
					<td><?php echo $this->_tpl_vars['Destination']->dest_title; ?>
</td>
					<td align="center">
						<?php if ($this->_tpl_vars['Destination']->dest_status): ?>
							<b>Yes</b>
							(<a href="JavaScript: ToggleStatus_Click('<?php echo $this->_tpl_vars['Destination']->dest_id; ?>
', '0');" class="actionLink">No</a>)
						<?php else: ?>
							<b>No</b>
							(<a href="JavaScript: ToggleStatus_Click('<?php echo $this->_tpl_vars['Destination']->dest_id; ?>
', '1');" class="actionLink">Yes</a>)
						<?php endif; ?>
					</td>					
					<td align="center">
						<!--img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_view.gif" class="imgAction" title="View" onClick="JavaScript: View_Click('<?php echo $this->_tpl_vars['Destination']->dest_id; ?>
');"-->&nbsp;
						<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_edit.gif" class="imgAction" title="Edit" onClick="JavaScript: Edit_Click('<?php echo $this->_tpl_vars['Destination']->dest_id; ?>
');">&nbsp;
						<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: Delete_Click('<?php echo $this->_tpl_vars['Destination']->dest_id; ?>
');">&nbsp;
						<!--img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_email.gif" class="imgAction" title="Send Mail" onClick="JavaScript: SendMail_Click('<?php echo $this->_tpl_vars['Destination']->dest_id; ?>
');"-->
					</td>
				</tr>
				<?php endforeach; unset($_from); else: ?>
				<tr>
					<td colspan="5">No Destination available.</td>
				</tr>
				<?php endif; ?>
			</table>
			<?php if ($this->_foreach['DestinationInfo']['total'] > 1): ?>
			<table border="0" cellpadding="1" cellspacing="1" width="95%">
				<tr>
					<td>
						<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
arrow_ltr.png"> 
						<a href="JavaScript: CheckUncheck_Click(document.all['dest_id1[]'], true);" onMouseMove="window.status='Check All';" onMouseOut="window.status='';" class="actionLink">Check All</a> / 
						<a href="JavaScript: CheckUncheck_Click(document.all['dest_id1[]'], false);" onMouseMove="window.status='Uncheck All';" onMouseOut="window.status='';" class="actionLink">Uncheck All</a>  &nbsp;&nbsp;
						With selected
						<img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
a_delete.gif" class="imgAction" title="Delete" onClick="JavaScript: DeleteChecked_Click();">
					</td>
				</tr>
			</table>
			<?php endif; ?>
				<!-- <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'tree_node.tpl', 'smarty_include_vars' => array('node' => $this->_tpl_vars['R_RecordSet'],'level' => 0)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> -->
			</table>
			<br><br>
			<input type="hidden" name="Action">
			<input type="hidden" name="dest_id">
			<input type="hidden" name="status">
		</td>
	</tr>
	</form>
</table>