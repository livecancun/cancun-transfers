<?php /* Smarty version 2.6.0, created on 2018-07-23 11:11:26
         compiled from transfercart_detail.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'transfercart_detail.tpl', 155, false),)), $this); ?>
<script>
	var Confirm_Delete = '<?php echo $this->_tpl_vars['Confirm_Delete']; ?>
';
</script>


<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_master.gif" height="16"></td>
					<td class="stdSection" width="99%">Reservation</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td class="successMsg" align="center"> <?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
			</table>
<table border="0" cellpadding="0" cellspacing="0" width="750">
<form name="frmTransferCart" action="<?php echo $this->_tpl_vars['A_TransferCart']; ?>
" method="post">
 <tr>
	<td>
      <table border="0" cellpadding="0" cellspacing="1" width="100%">
		<tr><td colspan="2" height="5"></td></tr>
		   <tr>
			  <td align="center" valign="top">
				<table border="0" cellpadding="2" cellspacing="2" width="98%">
					<tr> 
					  <td colspan="2" class="blueHeader"> <?php echo $this->_tpl_vars['View']; ?>
 -> <?php echo $this->_tpl_vars['L_Trasferdetail']; ?>
</td>
					</tr>
					<tr>
						 <td colspan="2">&nbsp;</td>
					</tr>


				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<?php if ($this->_tpl_vars['trip_status'] == 0 || $this->_tpl_vars['trip_status'] == 2): ?>		
								<td width="50%">
									<table width="100%" cellpadding="2" cellspacing="2" >
										<tr> 
											<td valign="top" class="fieldLabelRight" width="40%"><?php echo $this->_tpl_vars['L_Destinations']; ?>
 :</td>
											<td width="60%"><?php echo $this->_tpl_vars['destination']; ?>
</td>
										</tr>
										<tr> 
											<td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_Hotelname']; ?>
 :</td>
											<td><?php echo $this->_tpl_vars['hotelName']; ?>
</td>
										</tr>
										<tr> 
											<td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_No_Person']; ?>
 :</td>
											<td><?php echo $this->_tpl_vars['tres_no_person']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_arrivalDate']; ?>
 :</td>
										  <td><?php echo $this->_tpl_vars['arrivalDate']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_arrivalTime']; ?>
 :</td>
										  <td  ><?php echo $this->_tpl_vars['arrivalTime']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_arrivalAirline']; ?>
 :</td>
										  <td  ><?php echo $this->_tpl_vars['arrivalAirline']; ?>
</td>
										</tr>
										
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_arrivalFlight']; ?>
 :</td>
										  <td  ><?php echo $this->_tpl_vars['arrivalFlight']; ?>
</td>
										</tr>
									</table>
								</td>
								<?php endif; ?>
								<td width="3%"></td>
								
								<?php if ($this->_tpl_vars['trip_status'] == 1 || $this->_tpl_vars['trip_status'] == 2): ?>	
								<td width="50%">
									<table width="100%" cellpadding="2" cellspacing="2" >
										<tr> 
										  <td valign="top" class="fieldLabelRight" width="40%"><?php echo $this->_tpl_vars['L_Origin']; ?>
 :</td>
										  <td width="60%"><?php echo $this->_tpl_vars['departureDestination']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_Hotel']; ?>
 :</td>
										  <td><?php echo $this->_tpl_vars['departureHotelName']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_No_Person']; ?>
 :</td>
										  <td><?php echo $this->_tpl_vars['departureTotalPerson']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_departureDate']; ?>
 :</td>
										  <td><?php echo $this->_tpl_vars['departureDate']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_departureTime']; ?>
 :</td>
										  <td><?php echo $this->_tpl_vars['departureTime']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_departureAirline']; ?>
 :</td>
										  <td><?php echo $this->_tpl_vars['departureAirline']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_Pickup_Time']; ?>
 :</td>
										  <td  ><?php echo $this->_tpl_vars['pickupTime']; ?>
</td>
										</tr>
										<tr> 
										  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_departureFlight']; ?>
 :</td>
										  <td><?php echo $this->_tpl_vars['departureFlight']; ?>
</td>
										</tr>
									</table>
								</td>
								<?php endif; ?>								
							</tr>				
						</table>
					</td>
				</tr>

				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="2" cellspacing="2">
							<?php if ($this->_tpl_vars['is_airline_empl'] == 1): ?>
							<tr> 
							  <td valign="top" class="fieldLabelRight" width="40%"><?php echo $this->_tpl_vars['L_Applicable_Discount']; ?>
 :</td>
							  <td width="60%"><?php echo $this->_tpl_vars['discount_title']; ?>
</td>
							</tr>
							<tr> 
							  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_Company_Name']; ?>
 :</td>
							  <td><?php echo $this->_tpl_vars['empl_airline']; ?>
</td>
							</tr>
							<tr> 
							  <td valign="top" class="fieldLabelRight"><?php echo $this->_tpl_vars['L_Company_ID']; ?>
 :</td>
							  <td><?php echo $this->_tpl_vars['empl_id']; ?>
</td>
							</tr>
							<?php endif; ?>
						</table>
					</td>
				</tr>

				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="2" cellspacing="2">
							<?php if ($this->_tpl_vars['is_airline_empl'] == 1): ?>
							<tr> 
							  <td valign="top" class="fieldLabelRight" width="40%"><?php echo $this->_tpl_vars['L_Total']; ?>
 :</td>
							  <td width="60%"> $ <?php echo $this->_tpl_vars['transferCharge']; ?>
</td>
							</tr>

							<tr> 
							  <td valign="top" class="fieldLabelRight" width="40%"><?php echo $this->_tpl_vars['L_Discount']; ?>
 [<?php echo $this->_tpl_vars['discount_rate']; ?>
%] :</td>
							  <td width="60%">- $ <?php echo smarty_function_math(array('equation' => "(x * y)/100",'x' => $this->_tpl_vars['transferCharge'],'y' => $this->_tpl_vars['discount_rate'],'format' => "%.2f"), $this);?>
</td>
							</tr>
							<?php endif; ?>

							<tr> 
								<td valign="top" class="fieldLabelRight" width="40%"><?php echo $this->_tpl_vars['L_Total']; ?>
 :</td>
								<td width="60%">$ 
									<?php if ($this->_tpl_vars['is_airline_empl'] == 1): ?>
										<?php echo smarty_function_math(array('equation' => "x - ((x * y)/100)",'x' => $this->_tpl_vars['transferCharge'],'y' => $this->_tpl_vars['discount_rate'],'format' => "%.2f"), $this);?>

									<?php else: ?>
										<?php echo $this->_tpl_vars['transferCharge']; ?>

									<?php endif; ?>
								</td>
							</tr>
							<tr> 
							  <td valign="top" class="fieldLabelRight" width="40%"><?php echo $this->_tpl_vars['L_Transferstatus']; ?>
 :</td>
							  <td width="60%"><b><?php echo $this->_tpl_vars['cartStatus']; ?>
</b></td>
							</tr>
						</table>
					</td>
				</tr>

                <tr>
                  <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" height="10"></td>
                </tr>
				<tr>
					<td colspan="2" align="center">
						<input type="button" name="Submit" value="<?php echo $this->_tpl_vars['Back']; ?>
" class="greyButton" onclick="javascript:location.href('reservation.php')">
					</td>
				</tr>
                <tr> 
                  <td align="center" colspan="2"> 
                    <input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
"> 
					<input type="hidden" name="user_id" value="<?php echo $this->_tpl_vars['user_id']; ?>
"> 
					<input type="hidden" name="carttranId" value="<?php echo $this->_tpl_vars['carttranId']; ?>
"> 
					<input type="hidden" name="confirmationNo" value="<?php echo $this->_tpl_vars['V_ConfirmationNo']; ?>
">
					<input type="hidden" name="cartId" value="<?php echo $this->_tpl_vars['cartId']; ?>
"> 
					<input type="hidden" name="Start" value="<?php echo $this->_tpl_vars['Start']; ?>
"> </td>
                </tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr><td colspan="2"><b><?php echo $this->_tpl_vars['Note']; ?>
 : </b></td></tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr> 
				  <td colspan="2" height="1" bgcolor="#000066"></td>
				</tr>
				<tr>
					<td colspan="2">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td><b>NR</b> - <?php echo $this->_tpl_vars['NoRequest']; ?>
</td>
								<td><b>PR</b> - <?php echo $this->_tpl_vars['PendingRequest']; ?>
</td>
								<td><b>AR</b> - <?php echo $this->_tpl_vars['ApproveRequest']; ?>
</td>
							</tr>
							<tr>
								<td><b>NA</b> - <?php echo $this->_tpl_vars['NotApprove']; ?>
</td>
								<td><b>RP</b> - <?php echo $this->_tpl_vars['RequestPayment']; ?>
</td>
								<td><b>PD</b> - <?php echo $this->_tpl_vars['PaymentDone']; ?>
</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr> 
				  <td colspan="2" height="1" bgcolor="#000066"></td>
				</tr>
				</table>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
        </td>
	</tr>
	</form>
	</table>		
		</td>
	</tr>
</table>



