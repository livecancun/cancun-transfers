<?php /* Smarty version 2.6.0, created on 2008-09-21 15:26:47
         compiled from testimonial_show.tpl */ ?>
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="100%"> 
	<form name="frmTestimonial" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post" enctype="multipart/form-data"> 
	<tr> 
		<td class="stdSectionHeader"> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%"> 
				<tr> 
					<td class="stdSection"><?php echo $this->_tpl_vars['Testimonial_Header']; ?>
</td> 
				</tr> 
			</table>
		</td> 
	</tr> 
	<tr> 
		<td valign="top" align="center"> 
			<table border="0" cellpadding="2" cellspacing="2" width="97%"> 
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr> 
					<td class="fieldLabelRight" valign="top" width="15%"><?php echo $this->_tpl_vars['Name']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Name']; ?>
</td> 
				</tr> 
<!--				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Company']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Company']; ?>
</td> 
				</tr> 
-->				
				<?php if ($this->_tpl_vars['Person_Address']): ?>
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Address']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Address']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Person_City']): ?>				
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['City']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_City']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Person_State']): ?>				
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['State']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_State']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Person_Country']): ?>				
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Country']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Country']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Person_Zip']): ?>				
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Zip']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Zip']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Person_Phone']): ?>				
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Phone']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Phone']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Person_Fax']): ?>				
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Fax']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Fax']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Person_Email']): ?>				
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Email']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Email']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Person_Website']): ?>				
				<tr> 
					<td class="fieldLabelRight" valign="top" width="22%"><?php echo $this->_tpl_vars['Website']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Website']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['Person_Pic'] != ''): ?>
				<tr>
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Photo']; ?>
 :</td> 
					<td class="fieldInputStyle" align="left">
						<img src="<?php echo $this->_tpl_vars['Picture_Filename']; ?>
" id="pic" border="0">
						<input type="hidden" name="person_pic_del" value="<?php echo $this->_tpl_vars['Person_Pic']; ?>
">
					</td>
				</tr>
				<?php endif; ?>

				
				<?php if ($this->_tpl_vars['Person_Comment']): ?>				
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Comment']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Comment']; ?>
</td> 
				</tr> 
				<?php endif; ?>
				
				<tr> 
					<td class="fieldLabelRight" valign="top"><?php echo $this->_tpl_vars['Display_On_Website']; ?>
 :</td> 
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['Person_Status']; ?>
</td> 
				</tr> 
				<tr> 
					<td colspan="2" height="5" class="divider"></td> 
				</tr> 
				<tr> 
					<td></td> 
					<td>
						<?php if ($this->_tpl_vars['ACTION'] == $this->_tpl_vars['A_View']): ?>
						<input type="button" name="Submit" value="<?php echo $this->_tpl_vars['Back']; ?>
" class="stdButton" onclick="javascript: Cancel_Click('<?php echo $this->_tpl_vars['Cancel_Action']; ?>
')"> 
						<?php else: ?>
						<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Delete']; ?>
" class="stdButton"> 
						<input type="button" name="Submit" value="<?php echo $this->_tpl_vars['Cancel']; ?>
" class="stdButton" onclick="javascript: Cancel_Click('<?php echo $this->_tpl_vars['Cancel_Action']; ?>
')"> 
						<?php endif; ?>
						<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
						<input type="hidden" name="person_id" value="<?php echo $this->_tpl_vars['Person_Id']; ?>
">
					</td> 
				</tr> 
			</table>
		</td> 
	</tr> 
	<tr> 
		<td>&nbsp;</td> 
	</tr> 
	</form> 
</table>