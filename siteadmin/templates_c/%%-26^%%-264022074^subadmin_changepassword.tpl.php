<?php /* Smarty version 2.6.0, created on 2009-07-17 22:51:18
         compiled from subadmin_changepassword.tpl */ ?>
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="stdTableBorder" height="97%">
	<form name="frmChangePassword" action="<?php echo $_SERVER['PHP_SELF']; ?>
" method="post">
	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="stdSection" width="1%"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_login.gif"></td>
					<td class="stdSection" width="49%">Change Password</td>
					<td class="stdSection" width="50%" align="right"><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
icon_help.gif" title="Help">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						Enter your old and new password and click <b>Update</b> to change the password.
						Click <b>Cancel</b> to cancel the action.
					</td>
				</tr>
				<tr><td height="2"></td></tr>
				<tr><td align="center" class="successMsg"><?php echo $this->_tpl_vars['succMessage']; ?>
</td></tr>
				<tr><td align="center" class="errorMsg"><?php echo $this->_tpl_vars['Error_Message']; ?>
</td></tr>
				<tr><td height="2"></td></tr>
			</table>
			<table border="0" cellpadding="1" cellspacing="2" width="95%">
				<tr>
					<td align="left">
						<input type="submit" name="Submit" value="Update" class="stdButton" onClick="JavaScript: return ChangePassword_Form(document.frmChangePassword);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
					<td align="right" class="mandatory">Fields marked with (<font class="mandatoryMark">*</font>) are mandatory.</td>
				</tr>
				<tr>
					<td colspan="2" class="stdSubSection" align="center">
						Login Information
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" width="25%" valign="top">Username: </td>
					<td class="fieldInputStyle"><?php echo $this->_tpl_vars['user_login_id']; ?>
</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Old Password: <font class="mandatoryMark">*</font></td>
					<td class="fieldInputStyle">
						<input type="password" name="user_old_password" size="25" maxlength="15">
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">New Password: <font class="mandatoryMark">*</font></td>
					<td class="fieldInputStyle">
						<input type="password" name="user_new_password" size="25" maxlength="15"><br>
						Specify a password of at least six (6) characters, and not to exceed 15 characters. Passwords are case sensitive.
					</td>
				</tr>
				<tr>
					<td class="fieldLabelLeft" valign="top">Retype Password: <font class="mandatoryMark">*</font></td>
					<td class="fieldInputStyle">
						<input type="password" name="retype_password" size="25" maxlength="15">
					</td>
				</tr>
				<tr><td class="divider" colspan="2"></td></tr>
				<tr>
					<td align="left" colspan="2">
						<input type="submit" name="Submit" value="Update" class="stdButton" onClick="JavaScript: return ChangePassword_Form(document.frmChangePassword);">
						<input type="submit" name="Submit" value="Cancel" class="stdButton">
					</td>
				</tr>
			</table>	
			<br>
		</td>
	</tr>
	</form>
</table>