<?php
#====================================================================================================
# File Name : transfercartedit.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_ADMIN', 	true);

if($_GET['Action'] == 'View')
	define('POPUP_WIN', 	true);

# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'Cart.php');
include_once($physical_path['DB_Access']. 'Reservation.php');
include_once($physical_path['DB_Access']. 'Destination.php');
include_once($physical_path['DB_Access']. 'UserMaster.php');

# Initialize object with required module
$objCart	= new Cart();
$objRes		= new Reservation();
$objDest 	= new Destination();
$objUser 	= new UserMaster();

$scriptName = "transfercartedit.php";

//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 10;
$num_records = '';
//==================================================================================================	


if($_POST['Action']==MODIFY && $_POST['Submit']==UPDATE)
{
	$sql = " UPDATE ".CARTTRANSFER." SET "
		 . " transferId 				=   '". $_POST['transferId']."' ";
	$sql .= " WHERE carttranId			= 	'". $_POST['carttranId'] ."' ";

	$db->query($sql);
	$transferId = $_POST['transferId'];

	 $sql = " SELECT * FROM ".TRANSFERMASTER
		   . " LEFT JOIN ". CARTTRANSFER. " ON ". TRANSFERMASTER. ".transferId = ". CARTTRANSFER. ".transferId "
		   . " WHERE  ". CARTTRANSFER .".cartId = '".$_POST['cartId']."' and ".TRANSFERMASTER.".transferId = '".$transferId."'"; 
	
	$db->query($sql);
	$db->next_record();
	
	
	//$totalPrice = 0;
	$TotalPerson = 0;
	$TotalPerson = $_POST['tres_no_adult'] + $_POST['tres_no_kid'];
	$totalPrice = 0;
	
	//$totalcost=0;
	$sql1 = "SELECT * FROM ".TRANSFERMASTER." WHERE ".TRANSFERMASTER.".transferId = '".$_POST['transferId']."'";
	$res1 = $db1->query($sql1);
	$db1->next_record();
	
	$totaladultcost=$_POST['tres_no_adult']*$db1->f('transferAdultRate');
	$totalkidcost=$_POST['tres_no_kid']*$db1->f('transferKidRate');
	$totalcost=$totaladultcost+$totalkidcost;

	/*****************************************/
	$sql = " SELECT * FROM ".TRANSFERDISCOUNT
	. " WHERE  " .TRANSFERDISCOUNT.".discount_lbound <= '". $TotalPerson ."' and " .TRANSFERDISCOUNT.".discount_ubound >= '". $TotalPerson ."'";
	$db->query($sql);
	$db->next_record();
	$discount=$db->f('discount_rate');
	
	
	/*if($_POST['singleTrip'] && $_POST['roundTrip'])
	{
		//$totalPrice = ( $_POST['tres_no_adult']  + $_POST['tres_no_kid'] ) * $db->f('transferRoundTripRate');
		//$totalPrice = ( $totaladultcost+ $totalkidcost + $db->f('transferRoundTripRate');
		$totalPrice = $totalcost+ $db1->f('transferRoundTripRate');
		$totalPrice = $totalPrice - $discount;
		$transferStatus = 2;
	}
	else
	{
		if($_POST['singleTrip'])
		{
		  	//$totalPrice = ( $_POST['tres_no_adult']  + $_POST['tres_no_kid'] ) * $db->f('transferSingleTripRate');
		  	//$totalPrice = ( $totaladultcost+ $totalkidcost + $db->f('transferSingleTripRate');
		  	$totalPrice = $totalcost + $db1->f('transferSingleTripRate');
		   	$totalPrice = $totalPrice - $discount;
			$transferStatus = 0;
		}
		else
		{
			//$totalPrice = ( $_POST['tres_no_adult']  + $_POST['tres_no_kid'] ) * $db->f('transferSingleTripRate');
			//$totalPrice = ( $totaladultcost+ $totalkidcost + $db->f('transferSingleTripRate');
			$totalPrice =  $totalcost + $db1->f('transferSingleTripRate');
			$totalPrice = $totalPrice - $discount;
			$transferStatus = 1;
		}	
	}*/
	if($_POST['singleTrip'] && $_POST['roundTrip'])
			{
				//$totalPrice = ( $_POST['tres_no_adult']  + $_POST['tres_no_kid'] ) * $db->f('transferRoundTripRate');
			//	$totalPrice = $totalcost+ $db1->f('transferRoundTripRate');
				$totalPrice = $totalcost;
				$totalPrice = $totalPrice - (($discount*$totalPrice)/100);
				$transferStatus = 2;
			}
			else
			{
				if($_POST['singleTrip'])
				{
					//$totalPrice = ( $_POST['tres_no_adult']  + $_POST['tres_no_kid'] ) * $db->f('transferSingleTripRate');
			//		$totalPrice = $totalcost + $db1->f('transferSingleTripRate');
					$totalPrice = $totalcost ;
					$totalPrice = $totalPrice - (($discount*$totalPrice)/100);			
					$transferStatus = 0;
				}
				else
				{
					//$totalPrice = ( $_POST['tres_no_adult']  + $_POST['tres_no_kid'] ) * $db->f('transferSingleTripRate');
			//		$totalPrice =  $totalcost + $db1->f('transferSingleTripRate');
					$totalPrice =  $totalcost ;					
					$totalPrice = $totalPrice - (($discount*$totalPrice)/100);
					$transferStatus = 1;
				}	
			}
	
	
	if(isset($_POST['arrival_year']))
	 {
		 $arrivalDate = $_POST['arrival_year']."-".$_POST['arrival_month']."-".$_POST['arrival_day'];
		 $departureDate = $_POST['departure_year']."-".$_POST['departure_month']."-".$_POST['departure_day'];
	 }
	 else
	 {
	   $arrivalDate  = $_POST['arrivalDate'];
	   $departureDate = $_POST['departureDate'];
	 }	
		#========================= Date portion end here ============#
		#========================= TIME portion Start here ============#
	 if(isset($_POST['arrival_hour']))
	 {
	    $arrivalTime  = $_POST['arrival_hour'].":".$_POST['arrival_minute']." ".$_POST['arrival_DayPart'];
		$departureTime  = $_POST['departure_hour'].":".$_POST['departure_minute']." ".$_POST['departure_DayPart'];
	 }
	 else
	 {
	    $arrivalTime  = $_POST['arrivalTime'];
		$departureTime  = $_POST['departureTime'];
	 }

	$cartDateTime = mktime (date('G'),date('i'),date('s'),date('m'),date('d'),date('Y'));
	
	$sql = " UPDATE ".CARTMASTER." SET "
		  . " totalCharge				=   '". $totalPrice."',  "
		  . " cartDateTime				=   '". $cartDateTime ."' ";
	$sql .= " WHERE cartId				= 	'". $_POST['cartId'] ."' ";
	$db->query($sql);


	$sql = " UPDATE ".CARTTRANSFER." SET "
		 . " tres_no_adult	 			=   '". $_POST['tres_no_adult'] ."',  "
		 . " tres_no_kid	 			=   '". $_POST['tres_no_kid'] ."',  "
		 . " hotelName		 			=   '". $_POST['hotelName'] ."',  "
 		 . " arrivalDate				=   '". $arrivalDate ."',  "
 		 . " arrivalTime				=   '". $arrivalTime ."',  "
 		 . " arrivalAirline				=   '". $_POST['arrivalAirline'] ."',  "
  		 . " arrivalFlight				=   '". $_POST['arrivalFlight'] ."',  "
 		 . " departureDate				=   '". $departureDate ."',  "
 		 . " departureTime				=   '". $departureTime ."',  "
 		 . " departureAirline			=   '". $_POST['departureAirline'] ."',  "
  		 . " departureFlight			=   '". $_POST['departureFlight'] ."',  "
 		 . " transferStatus				=   '". $transferStatus ."',  "
  		 . " totalAmount				=   '". $totalPrice ."' ";
	$sql .= " WHERE carttranId			= 	'". $_POST['carttranId'] ."' ";

$db->query($sql);
header("location: reservationdetail.php?&confirmationNo=".$_POST['confirmationNo']."&update=true");
}
//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 15;
$num_records = '';

#====================================================================================================
#====================================================================================================
if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL || $_POST['Submit']==CANCEL || $_POST['Action']==BACK || $_POST['Action'] == SHOWCART)
{
	header("location: reservationdetail.php?confirmationNo=".$_POST['confirmationNo']);	
}
elseif($_GET['Action']==MODIFY || $_POST['Action']==MODIFY)
{
	
	$tpl->assign(array( 'T_Body'		=>	'custom_page'. $config['tplEx'],
						'JavaScript'	=>	array('transfercart_edit.js','browserSniffer.js',$_SESSION['lng'].'_dynCalendar.js'),
						'A_Action'		=>	$scriptName,
						'L_Module'		=>	$objRate->Data['L_Module'],
						'H_HelpText'	=>	$objRate->Data['H_Manage'],
						'includeFile'	=>	'transfercart_edit'. $config['tplEx'],
						));
	
	$tpl->assign(array("L_Transfer_Service"     => $lang['L_Transfer_Service'],
					   "L_Destinations"   		=> $lang['L_Destinations']));
	
	$tpl->assign(array( "A_TransferCart"		=> "transfercartedit.php?&start=$start_record",	
						"Start"					=>	$start_record,
						"ACTION"				=>   MODIFY
						));
						
	$sql =  " SELECT * FROM ".TRANSFERMASTER." LIMIT ". $start_record . ", ". $Page_Size ;
	
	$res = $db->query($sql);
	$i = 0;
	$rscnt = $db->num_rows();
	if($rscnt == 0)
	{
		$tpl->assign("nodata",$lang['nodata']);
	}
    else
	{
		while($db->next_record())
		{
			$tpl->newBlock("MainBlock");	
			$tpl->assign(array("V_Destination"          => $db->f('transferOrigin')." - ".$db->f('transferDestination'),
							   "transferSingleTripRate" => number_format($db->f('transferSingleTripRate'),2,'.',','),
							   "transferRoundTripRate"	=> number_format($db->f('transferRoundTripRate'),2,'.',',')	
							));
		}					
	}
    $tpl->gotoBlock( "_ROOT" );
    $tpl->assign(array("A_Transfer"		  	=> "transfer.php",
					   "Edit"			  	=> $lang['edit'],
					   "V_ConfirmationNo" 	=> $_POST['confirmationNo'],
					   "L_Book_Transfer"  	=> $lang['L_Book_Transfer'],
					   "L_Destinations"   	=> $lang['L_Destinations'],
					   "L_Adults" 		  	=> $lang['L_Adults'],
					   "L_Childs"		  	=> $lang['L_Childs'],
					   "L_Hotel"          	=> $lang['L_Hotel_LinkName'],
					   "L_Date"			  	=> $lang['L_Date'],
					   "L_Time"          	=> $lang['L_Time'],
					   "L_Airline"       	=> $lang['L_Airline'],
					   "L_Flight"        	=> $lang['L_Flight'],
					   "L_Airport_To_Hotel"   	=> $lang['L_Airport_To_Hotel'],
					   "L_Hotel_To_Airport"   	=> $lang['L_Hotel_To_Airport'],
					   "L_Airport_To_Hotel_Msg" => $lang['L_Airport_To_Hotel_Msg'],
					   "L_Hotel_To_Airport_Msg" => $lang['L_Hotel_To_Airport_Msg'],
					   "Empty_No_Adult_Msg"	 	=> $lang['Empty_No_Adult_Msg'],
					   "Valid_No_Adult_Msg" 	=> $lang['Valid_No_Adult_Msg'],
					   "Valid_No_Child_Msg"		=> $lang['Valid_No_Child_Msg'],
					   "Empty_Hotel_Name" 		=> $lang['Empty_Hotel_Name'],
					   "Valid_Date"       		=> $lang['Valid_Date'],
					   "Empty_Check_Box"   		=> $lang['Empty_Check_Box'],
					   "Valid_Arrival_Date" 	=> $lang['Valid_Arrival_Date'],
					   "Empty_Airline_Name" 	=> $lang['Empty_Airline_Name'],
					   "Empty_Flight_No"    	=> $lang['Empty_Flight_No'],
					   "Empty_Arrival_Date"    	=> $lang['Valid_Arrival_Date'],
   					   "Empty_Departure_Date"   => $lang['Valid_Departure_Date'],
					   "Valid_Departure_Date"	=> $lang['Valid_Departure_Date'],
					   "Valid_BothDate_Msg" 	=> $lang['Valid_BothDate_Msg']
						));
						
	$tpl->assign("cartId" ,$_POST['cartId']);
						
   $sql = " SELECT ".TRANSFERMASTER.".*,".CARTTRANSFER.".* FROM ".TRANSFERMASTER
		   . " LEFT JOIN ". CARTTRANSFER. " ON ". TRANSFERMASTER. ".transferId = ". CARTTRANSFER. ".transferId "
		   . " WHERE  ". CARTTRANSFER .".cartId = '".$_POST['cartId']."'"; 
		   
	$res = $db->query($sql);
	$db->next_record();
							
	$tansferId = $db->f('transferId');
	$arrivalDate = $db->f('arrivalDate');
	$departureDate = $db->f('departureDate');

	$arrivalTime = $db->f('arrivalTime');
	$departureTime = $db->f('departureTime');
	
	$tpl->assign(array("tres_no_adult"	     => $db->f('tres_no_adult'),
					   "tres_no_kid"	     => $db->f('tres_no_kid'),
					   "hotelName" 	    	 => $db->f('hotelName'),
					   "arrivalDate"		 => $db->f('arrivalDate'),
   					   "departureDate"		 => $db->f('departureDate'),	
					   "arrivalAirline"		 => $db->f('arrivalAirline'),
   					   "departureAirline"	 => $db->f('departureAirline'),
					   "arrivalFlight"		 => $db->f('arrivalFlight'),
   					   "departureFlight"	 => $db->f('departureFlight'),
					   "L_Update"        	 => $lang['update'],
					   "Cancel"			 	 =>	$lang['cancel'],
					   "carttranId" 		 => $db->f('carttranId')));
	
		if($db->f('transferStatus') == 0)
			{
				$tpl->assign("CHECKED","CHECKED");
			}
			elseif($db->f('transferStatus') == 1)
			{
				$tpl->assign("CHECKED1","CHECKED");
			}
			elseif($db->f('transferStatus') == 2)
			{
				$tpl->assign("CHECKED","CHECKED");
				$tpl->assign("CHECKED1","CHECKED");
			}
		#===== Show Destinations ===== #
		$sql = " SELECT * FROM ".TRANSFERMASTER;
		$res = $db->query($sql);
		while($db->next_record())
			{
				$tpl->newBlock("Transfers");
				$tpl->assign(array( "V_transferId"		   => $db->f('transferId'),
									"V_transferOrgin"      => $db->f('transferOrigin'),
							   		"V_transferDestination" => $db->f('transferDestination')
					));
				if($tansferId == $db->f('transferId'))
				{
					$tpl->assign("transferselected","selected");
				}				
		   }
			#========================#
	
			
			list($arrival_Hour,$temparrival_Minute) = explode(":",$arrivalTime); 
				  list($arrival_Minute,$arrival_DayPart)  = explode(" ",$temparrival_Minute); 
 		  list($departure_Hour,$tempdeparture_Minute) = explode(":",$departureTime); 
		 		 list($departure_Minute,$departure_DayPart)  = explode(" ",$tempdeparture_Minute); 

			#======Show Hour =======#
				if (isset($arrHour)) 
				{
 					reset($arrHour);
				    while (list ($key, $val) = each ($arrHour)) 
					{ 
						$tpl->newBlock("Departure_Hour");
						$tpl->assign("V_Departure_Hour",$key);
						 if($departure_Hour == $key)
							  $tpl->assign("hourselected","selected");
					
						$tpl->newBlock("Arrival_Hour");
						$tpl->assign("V_Arrival_Hour",$key);
						 if($arrival_Hour == $key)
							  $tpl->assign("hourselected","selected");
					}		
			  }			
			#========================#
			#======Show Minutes =======#
				if (isset($arrMinute)) 
				{
 					reset($arrMinute);
				    while (list ($key, $val) = each ($arrMinute)) 
					{ 
						$tpl->newBlock("Departure_Minute");
						$tpl->assign("V_Departure_Minute",$key);
						 if($departure_Minute == $key)
							  $tpl->assign("minuteselected","selected");
						
						$tpl->newBlock("Arrival_Minute");
						$tpl->assign("V_Arrival_Minute",$key);
					    if($arrival_Minute == $key)
							  $tpl->assign("minuteselected","selected");

		  			}		
			  }			
			#========================#
			#======Show Day Part =======#
				if (isset($arrDayPart)) 
				{
 					reset($arrDayPart);
				    while (list ($key, $val) = each ($arrDayPart)) 
					{ 
						$tpl->newBlock("Arrival_DayPart");
						$tpl->assign("V_arrivalDayPart",$key);
							if($arrival_DayPart == $key)
								$tpl->assign("arrivaldaypartselected","selected");

						$tpl->newBlock("Departure_DayPart");
						$tpl->assign("V_departureDayPart",$key);
							if($departure_DayPart == $key)		
								$tpl->assign("departuredaypartselected","selected");
					
		  			}		
			  }			
			#========================#
}

elseif($_GET['Action']==SHOW || $_POST['Action']==SHOW)
{

	$tpl->assign(array( 'T_Body'		=>	'custom_page'. $config['tplEx'],
						'A_Action'		=>	$scriptName,
						'L_Module'		=>	$objRate->Data['L_Module'],
						'H_HelpText'	=>	$objRate->Data['H_Manage'],
						'includeFile'	=>	'transfercart_show'. $config['tplEx'],
						));

	 $tpl->assign(array( "A_TransferCart"		=> "transfercart.php?&start=$start_record",	
						 "Start"				=>	$start_record,
						 "Back"					=>  $lang['Back'],
						 "ACTION"				=>  BACK
						));	

	$tpl->assign(array( "L_Transferdetail"   => $lang['transferdetail'],
						"TransferRates" 	 => $lang['L_Transfer_Rates'],
						"L_Total"			 =>	$lang['L_Total'],
						"L_transferRequestNo"=> $lang['transferRequestNo'],
						"L_Hotelname"		 => $lang['L_hotelTitle'],
						"NoofAdultPerson" 	 => $lang['noofadult'],
						"NoofKid"			 => $lang['noofkid'],
						"L_Transferstatus"	 => $lang['transferStatus'],
						"Pay"           	 => $lang['payment'],
						"L_Print_Version"	 => $lang['L_Print_Version'],
						"L_Note"			 => $lang['L_Note'],
						"Note_PR"			 => $lang['Note_PR'],
						"Note_AR"			 => $lang['Note_AR'],
						"Note_NA"			 => $lang['Note_NA'],
						"Note_RP"			 => $lang['Note_RP'],
						"Note_PD"			 => $lang['Note_PD'],
						"SingleTrip"		 => $lang['L_singleTrip'],
						"RoundTrip"			 => $lang['L_roundTrip']	
						));	

	$tpl->assign(array( "A_TransferCart"		=> "transfercart.php?&start=$start_record",
							"L_Transfer_Reservation_Details" => $lang['L_Transfer_Reservation_Details'], 
							"L_Request"				=> $lang['L_Request'],
							"Confirm_Delete"	    => $lang['msgConfirmDelete'],
							"Delete"   			    => $lang['Delete'],
							"Message"				=>  $succ_message,
							"L_transferRequestNo"	=> $lang['transferRequestNo'],
							"L_transferStatus"		=> $lang['transferStatus'],
						    "L_Destinations"  		=> $lang['L_Destinations'],
							"L_ReserveMore"			=> $lang['L_ReserveMore'],
							"L_Checkout"			=> $lang['L_Checkout'],
							"Valid_Delete"			=> $lang['Valid_Delete'],
							"Valid_Select"			=> $lang['Valid_Select'],
							"Valid_Select_Tour"		=> $lang['Valid_Select_Tour']
						));
						
						
	$sql = " SELECT * FROM ".CARTTRANSFER
		   . " LEFT JOIN ". TRANSFERMASTER. " ON ". CARTTRANSFER. ".transferId = ". TRANSFERMASTER. ".transferId "
		   . " LEFT JOIN ". CARTMASTER. " ON ". CARTTRANSFER. ".cartId = ". CARTMASTER. ".cartId "
		   . " WHERE ".CARTMASTER.".cartId = '".$_POST['cartId']."'" ;
	$rs = $db->query($sql);
	
	if($db->num_rows() <= 0)
			$tpl->assign("Notfound_Message",$lang['Msg_TransferReserve_Norecord']);
	else
	{
		if($db->num_rows() > 0 )
		{
			$db->next_record();
			$tpl->assign(array( "cartId"			 => $db->f('cartId'),
								"carttranId"   		 =>	$db->f('carttranId'),
								"transferRequestNo"	 =>	$db->f('transferRequestNo'),
								"tres_no_adult" 	 => $db->f('tres_no_adult'),
						   		"tres_no_kid"		 => $db->f('tres_no_kid'),
								"trStatus"	 		 => $db->f('trStatus'),	
								"hotelName"          => stripslashes($db->f('hotelName')),	
								"transferCharge"     => $db->f('totalAmount'),
								"V_transferId"		   => $db->f('transferId'),
								"V_transferOrgin"      => $db->f('transferOrigin'),
						   		"V_transferDestination" => $db->f('transferDestination'),
								"transferSingleTripRate"=> $db->f('transferSingleTripRate'),
								"transferRoundTripRate" => $db->f('transferRoundTripRate')));
				
				
			if($db->f('transferStatus') == 2)
			{	
				$tpl->assign("Transfer_Image", "BRoundTrip.gif");
				$tpl->assign("imagepath", $physical_path['Image_Root']);

			}					
			else
			{
				$tpl->assign("Transfer_Image", "BOneWay.gif");
				$tpl->assign("imagepath", $physical_path['Image_Root']);
			}
			if($db->f('transferStatus') == 2)
			{
				$tpl->newBlock("Both");
				$tpl->assign(array("L_arrivalDate"		 => $lang['arrivaldate'],
									"L_departureDate"	 => $lang['departuredate'],
									"L_arrivalTime"		 => $lang['arrivaltime'],
									"L_departureTime"	 => $lang['departuretime'], 
									"L_arrivalAirline"	 => $lang['arrivalairline'],
									"L_departureAirline" => $lang['departureairline'],
									"L_arrivalFlight" 	 => $lang['arrivalflight'],
									"L_departureFlight"  => $lang['departureflight']));
																		
				$tpl->assign(array(	"arrivalTime"		 => $db->f('arrivalTime'),
									"departureTime"		 => $db->f('departureTime'),
									"arrivalDate"	     =>	formatDate($db->f('arrivalDate'),'d-M-Y'),
									"departureDate"	     =>	formatDate($db->f('departureDate'),'d-M-Y'),
									"arrivalAirline"	 => $db->f('arrivalAirline'),
									"arrivalFlight"		 => $db->f('arrivalFlight'),
									"departureAirline"	 => $db->f('departureAirline'),
									"departureFlight"	 => $db->f('departureFlight')));
									
			}	
			elseif($db->f('transferStatus') == 0)
			{
				$tpl->newBlock("Return");
				$tpl->assign(array("L_arrivalDate"		 => $lang['arrivaldate'],
									"L_arrivalTime"		 => $lang['arrivaltime'],
									"L_arrivalAirline"	 => $lang['arrivalairline'],
									"L_arrivalFlight" 	 => $lang['arrivalflight']));

				$tpl->assign(array(	"arrivalDate"	     =>	formatDate($db->f('arrivalDate'),'d-M-Y'),
									"arrivalAirline"	 => $db->f('arrivalAirline'),
									"arrivalTime"		 => $db->f('arrivalTime'),
									"arrivalFlight"		 => $db->f('arrivalFlight')
));
			}
			elseif($db->f('transferStatus') == 1)
			{
				$tpl->newBlock("Single");
				$tpl->assign(array(	"L_departureDate"	 => $lang['departuredate'],
									"L_departureTime"	 => $lang['departuretime'], 
									"L_departureAirline" => $lang['departureairline'],
									"L_departureFlight"  => $lang['departureflight']));

				$tpl->assign(array(	"departureAirline"	 => $db->f('departureAirline'),
									"departureFlight"	 => $db->f('departureFlight'),
									"departureTime"		 => $db->f('departureTime'),
									"departureDate"	     =>	formatDate($db->f('departureDate'),'d-M-Y')));
			}	
	

		}
	
    }

}
elseif($_GET['Action']=="Print" || $_POST['Action']=="Print")
{
  $tpl->assignInclude("B_Content", $physical_path['Templates_Root']."/transfercart_print.".$config['tplEx']);
  $tpl->prepare();
  $tpl->assignGlobal(array ("imagepath"		=>	$physical_path['Image_Root'],
							"jsroot"        =>  $physical_path['Js_Root'],
							"cssSiteroot"   =>	$physical_path['Css_Root'])); 

 $tpl->assign(array( "A_TransferCart"		=> "transfercart.php?&start=$start_record",	
					 "Start"				=>	$start_record,
					 "CurrDate"			    =>	formatDate(date("Y-m-d"),'d F, Y')					 						
					));	

	$tpl->assign(array( "L_Transferdetail"   => $lang['transferdetail'],
						"TransferRates" 	 => $lang['L_Transfer_Rates'],
						"L_Total"			 =>	$lang['L_Total'],
						"L_transferRequestNo"=> $lang['transferRequestNo'],
						"L_Hotelname"		 => $lang['L_hotelTitle'],
						"NoofAdultPerson" 	 => $lang['noofadult'],
						"NoofKid"			 => $lang['noofkid'],
						"L_Transferstatus"	 => $lang['transferStatus'],
						"Pay"           	 => $lang['payment'],
						"L_Print_Version"	 => $lang['L_Print_Version'],
						"L_Note"			 => $lang['L_Note'],
						"Note_PR"			 => $lang['Note_PR'],
						"Note_AR"			 => $lang['Note_AR'],
						"Note_NA"			 => $lang['Note_NA'],
						"Note_RP"			 => $lang['Note_RP'],
						"Note_PD"			 => $lang['Note_PD'],
						"SingleTrip"		 => $lang['L_singleTrip'],
						"RoundTrip"			 => $lang['L_roundTrip']	
						));	

		$tpl->assign(array( "A_TransferCart"		=> "transfercart.php?&start=$start_record",
							"L_Transfer_Reservation_Details" => $lang['L_Transfer_Reservation_Details'], 
							"L_Request"				=> $lang['L_Request'],
							"Confirm_Delete"	    => $lang['msgConfirmDelete'],
							"Delete"   			    => $lang['Delete'],
							"Message"				=>  $succ_message,
							"L_transferRequestNo"	=> $lang['transferRequestNo'],
							"L_transferStatus"		=> $lang['transferStatus'],
						    "L_Destinations"  		=> $lang['L_Destinations'],
							"L_ReserveMore"			=> $lang['L_ReserveMore'],
							"L_Checkout"			=> $lang['L_Checkout'],
							"Valid_Delete"			=> $lang['Valid_Delete'],
							"Valid_Select"			=> $lang['Valid_Select'],
							"Valid_Select_Tour"		=> $lang['Valid_Select_Tour']
						));
			
	$sql = " SELECT * FROM ".CARTTRANSFER
		   . " LEFT JOIN ". TRANSFERMASTER. " ON ". CARTTRANSFER. ".transferId = ". TRANSFERMASTER. ".transferId "
		   . " LEFT JOIN ". CARTMASTER. " ON ". CARTTRANSFER. ".cartId = ". CARTMASTER. ".cartId "
		   . " LEFT JOIN ". USERMASTER. " ON ". CARTMASTER. ".user_id = ". USERMASTER. ".user_id "	
		   . " LEFT JOIN ". AUTH_USER. " ON ". USERMASTER. ".user_id = ". AUTH_USER. ".user_id "	
		   . " WHERE ".CARTMASTER.".cartId = '".$_GET['cartId']."'" ;
		   		
						
	$rs = $db->query($sql);
	
	if($db->num_rows() <= 0)
			$tpl->assign("Notfound_Message",$lang['Msg_TransferReserve_Norecord']);
	else
	{
		if($db->num_rows() > 0 )
		{
			$db->next_record();
			$tpl->assign(array( "carttranId"   		 =>	$db->f('carttranId'),
								"userName"			 =>	$db->f('user_login_id'),
						    	"FirstName"      	 =>  ucfirst(stripslashes($db->f('firstname'))),
						  	  	"LastName"           =>  ucfirst(stripslashes($db->f('lastname'))),
								"transferRequestNo"	 =>	$db->f('transferRequestNo'),
								"tres_no_adult" 	 => $db->f('tres_no_adult'),
						   		"tres_no_kid"		 => $db->f('tres_no_kid'),
								"trStatus"	 		 => $db->f('trStatus'),	
								"hotelName"          => stripslashes($db->f('hotelName')),	
								"transferCharge"     => $db->f('totalAmount'),
								"V_transferId"		   => $db->f('transferId'),
								"V_transferOrgin"      => $db->f('transferOrigin'),
						   		"V_transferDestination" => $db->f('transferDestination'),
								"transferSingleTripRate"=> $db->f('transferSingleTripRate'),
								"transferRoundTripRate" => $db->f('transferRoundTripRate')));
				
					if($db->f('transferStatus') == 2)
			{	
				$tpl->assign("Transfer_Image", "BRoundTrip.gif");
				$tpl->assign("imagepath", $physical_path['Image_Root']);

			}					
			else
			{
				$tpl->assign("Transfer_Image", "BOneWay.gif");
				$tpl->assign("imagepath", $physical_path['Image_Root']);
			}
			if($db->f('transferStatus') == 2)
			{
				$tpl->newBlock("Both");
				$tpl->assign(array("L_arrivalDate"		 => $lang['arrivaldate'],
									"L_departureDate"	 => $lang['departuredate'],
									"L_arrivalTime"		 => $lang['arrivaltime'],
									"L_departureTime"	 => $lang['departuretime'], 
									"L_arrivalAirline"	 => $lang['arrivalairline'],
									"L_departureAirline" => $lang['departureairline'],
									"L_arrivalFlight" 	 => $lang['arrivalflight'],
									"L_departureFlight"  => $lang['departureflight']));
																		
				$tpl->assign(array(	"arrivalTime"		 => $db->f('arrivalTime'),
									"departureTime"		 => $db->f('departureTime'),
									"arrivalDate"	     =>	formatDate($db->f('arrivalDate'),'d-M-Y'),
									"departureDate"	     =>	formatDate($db->f('departureDate'),'d-M-Y'),
									"arrivalAirline"	 => $db->f('arrivalAirline'),
									"arrivalFlight"		 => $db->f('arrivalFlight'),
									"departureAirline"	 => $db->f('departureAirline'),
									"departureFlight"	 => $db->f('departureFlight')));
									
			}	
			elseif($db->f('transferStatus') == 0)
			{
				$tpl->newBlock("Return");
				$tpl->assign(array("L_arrivalDate"		 => $lang['arrivaldate'],
									"L_arrivalTime"		 => $lang['arrivaltime'],
									"L_arrivalAirline"	 => $lang['arrivalairline'],
									"L_arrivalFlight" 	 => $lang['arrivalflight']));

				$tpl->assign(array(	"arrivalDate"	     =>	formatDate($db->f('arrivalDate'),'d-M-Y'),
									"arrivalAirline"	 => $db->f('arrivalAirline'),
									"arrivalTime"		 => $db->f('arrivalTime'),
									"arrivalFlight"		 => $db->f('arrivalFlight')
));
			}
			elseif($db->f('tranferStatus') == 1)
			{
				$tpl->newBlock("Single");
				$tpl->assign(array(	"L_departureDate"	 => $lang['departuredate'],
									"L_departureTime"	 => $lang['departuretime'], 
									"L_departureAirline" => $lang['departureairline'],
									"L_departureFlight"  => $lang['departureflight']));

				$tpl->assign(array(	"departureAirline"	 => $db->f('departureAirline'),
									"departureFlight"	 => $db->f('departureFlight'),
									"departureTime"		 => $db->f('departureTime'),
									"departureDate"	     =>	formatDate($db->f('departureDate'),'d-M-Y')));
			}	

		}
	
		$tpl->gotoBlock( "_ROOT" );
    }

}
$tpl->gotoBlock("_ROOT");
$tpl->printToScreen();
?>