<?php
#====================================================================================================
# File Name : reservationdetail.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define("IN_ADMIN", true);
 
if($_POST['Action']=="Print" || $_GET['Action']=="Print")
	define("POPUP_WINDOW", true);


# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'Cart.php');
include_once($physical_path['DB_Access']. 'UserMaster.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
# Initialize object with required module
$objCart	= new Cart();
$objUser 	= new UserMaster();

$scriptName = "reservationdetail.php?&start=$start_record";

//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 10;
$num_records = '';

//	print "Action=".$_POST['Action'];
	
//==================================================================================================
if($_POST['Submit'] == APPROVE)
{
	header("location: paymentemail.php?&confirmationNo=".$_POST['confirmationNo']."&update=true");
	exit;
}
if($_POST['Submit'] == NOTAPPROVE)
{

	$Status = $objCart->chageRequestStatus($_POST['confirmationNo'],'NA');
	
	header("location: reservationdetail.php?&confirmationNo=".$_POST['confirmationNo']."&update=true");
	exit;
}
if($_POST['Submit'] == PENDING)
{
	$Status = $objCart->chageRequestStatus($_POST['confirmationNo'],'PR');

	header("location: reservationdetail.php?&confirmationNo=".$_POST['confirmationNo']."&update=true");
	exit;

}
if($_POST['Submit'] == PAYMENTDONE)
{
	$Status = $objCart->chageRequestStatus($_POST['confirmationNo'],'PD');

	header("location: reservationdetail.php?&confirmationNo=".$_POST['confirmationNo']."&update=true");
	exit;
}


#====================================================================================================
	if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL || $_POST['Submit']==CANCEL || $_POST['Action']==BACK || $_POST['Action'] == SHOWCART)
	{
		$tpl->assign(array( 'T_Body'		=>	'reservationdetail_showall'. $config['tplEx'],
							'JavaScript'	=>	array('reservation.js'),
							'A_Action'		=>	$scriptName,
							));
	
		if($update == true)
		{
			$succ_message = $lang['Msg_CartDetail_Updated'];
		}
		if($requestsent == true)
		{
			$succ_message = $lang['Msg_Request_Sent'];
		}
		if($delete == true)
		{
			$succ_message = $lang['Msg_CartDetail_Deleted'];
		}
		
		$confirmationNo = isset($_POST['confirmationNo'])?$_POST['confirmationNo']:$_GET['confirmationNo'];
		$db->free();

		
		$tpl->assign(array( "A_Reservation"			=> "reservationdetail.php?&start=$start_record",
							"ACTION"			    =>  SHOW_ALL,
							"Back"				    =>  $lang['Back'],
							"Action"				=> 	$lang['Action'],
							"L_Final"	 		 	 =>	$lang['L_Final'],
							"L_Discount"	 		 =>	$lang['L_Discount'],
							"L_Reservation_Details"	=> 	$lang['L_Reservation_Details'],
							"L_ConfirmationNo"  	=> 	$lang['L_ConfirmationNo'],
							"V_ConfirmationNo"		=> 	$confirmationNo,
							"L_cartItem"			=> 	$lang['L_cartItem'],
							"L_ItemType"			=> 	$lang['L_ItemType'],
							"L_Cart"				=> 	$lang['L_Cart'],
							"Confirm_Delete"	    => 	$lang['msgConfirmDelete'],
							"Message"				=>  $succ_message,
							"L_Total"   			=> 	$lang['L_Total'],
							"Valid_Delete_Cart"		=> 	$lang['Valid_Delete_Cart'],
							"Valid_Select_Cart"		=> 	$lang['Valid_Select_Cart'],
							"View"					=>  $lang['View'],
							"Modify"  				=>  $lang['Modify'],
							"Delete"   			    =>  $lang['Delete'],
							));
			
		$sql = $objUser -> ShowUserInfoByConfNo($confirmationNo); 
		$db->next_record();
	
		$tpl->assign(array("is_airline_empl"	=> $db->f('is_airline_empl'),
						   "discount_id"     	=> $db->f('discount_id'),
						   "discount_rate"	    => $db->f('discount_rate'),
						   "discount_title"	    => ucfirst($db->f('discount_title')),
						   "empl_id" 	    	=> $db->f('empl_id'),
						   "empl_airline"		=> ucfirst($db->f('empl_airline')),
						   ));
			
		$SortBy = "itemType";

		$tpl->assign(array( 'CartInfo'	=>	$objCart->Show_Admin_Cart_Detail($confirmationNo,$SortBy, $start_record, $Page_Size, $num_records),
							));
			
		$tpl->assign(array( "NA"	=> 	NOTAPPROVE,
							"PR"	=>  PENDING,
							"PD"	=> 	PAYMENTDONE,
							"AR"	=>  APPROVE,
							));
		
		  $tpl->assign(array("L_status"			 => $lang['L_status'],
							 "Note"			     => $lang['L_Note'],
							 "NoRequest"	 	 => $lang['Note_NR'],
							 "PendingRequest"	 => $lang['Note_PR'],							
							 "ApproveRequest"	 => $lang['Note_AR'],							
							 "NotApprove"		 => $lang['Note_NA'],							
							 "RequestPayment"	 => $lang['Note_RP'],							
							 "PaymentDone"		 => $lang['Note_PD']
							 ));	
							
		
		if($num_records >= $Page_Size)
			$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));
	}

	elseif($_GET['Action'] == SHOWREQUEST || $_POST['Action'] == SHOWREQUEST)
	{
			$tpl->assign(array( 'T_Body'		=>	'cart_request'. $config['tplEx'],
								'JavaScript'	=>	array('reservation.js'),
								'A_Action'		=>	$scriptName,
								));
	
		
			$tpl->assign(array( "A_Cart"				=> "cart.php?&start=$start_record",
								"L_Reservation_Details"	=> $lang['L_Reservation_Details'], 
								"L_Request"				=> $lang['L_Request'],
								"L_View_Cart"			=> $lang['L_View_Cart'],
								"L_View_Request"		=> $lang['L_View_Request'],
								"L_cartItem"			=> $lang['L_cartItem'],
								"L_ItemType"			=> $lang['L_ItemType'],
								"Confirm_Delete"	    => $lang['msgConfirmDelete'],
								"Delete"   			    => $lang['Delete'],
								"Message"				=>  $succ_message,
								"L_RequestNo"   		=> $lang['RequestNo'],
								"L_status"				=> $lang['L_status'],
								"L_Total"   			=> $lang['L_Total'],
								"L_ReserveMore"			=> $lang['L_ReserveMore'],
								"L_Checkout"			=> $lang['L_Checkout'],
								"Valid_Delete_Cart"		=> $lang['Valid_Delete_Cart'],
								"Valid_Select_Cart"		=> $lang['Valid_Select_Cart'],
								"L_Note"				=> $lang['L_Note'],
								"Note_PR"				=> $lang['Note_PR'],
								"Note_AR"				=> $lang['Note_AR'],
								"Note_NA"				=> $lang['Note_NA'],
								"Note_RP"				=> $lang['Note_RP'],
								"Note_PD"				=> $lang['Note_PD']
							));
	
		$tpl->assign("ReserveMoreLink",$url_path['Site_URL']."Reservation/reservemore.php");
		$tpl->assign("CheckoutLink","#");
		$db->free();
		$SortBy = "itemType";
		$record = Show_Request_Detail($_SESSION['User_Id'],$cartId='',$SortBy, $start_record, $Page_Size, $num_records);
		
		$i = 0;
		$rscnt = $db->num_rows();
		if($rscnt == 0)
		{
			$tpl->assign("Notfound_Message",$lang['Msg_Request_Norecord']);
		}
		$grandTotal = 0;
		while($i < $rscnt)
		{
			$db->next_record();
			$tpl->newBlock("CartDetail");
			$tpl->assign(array( "CartId"				=>  $db->f('cartId'),
								"ItemType"				=>  $db->f('itemType'),
								"V_ItemType" 			=>  $arrCart[$db->f('itemType')],
								"V_RequestNo"			=>	stripslashes($db->f('requestNo')),
								"V_Status"				=>	stripslashes($db->f('cartStatus')),
								"View"					=>  $lang['View'],
								"V_Total"				=>  number_format($db->f('totalCharge'),2,'.',',')
			));
				$itemName = Show_Item_Detail($db->f('cartId'),$db->f('itemType'));
					$tpl->assign("V_cartItem",$itemName);
				
			if($db->f('cartStatus') != AR)
					$tpl->assign("disableCheckBox","disabled");
			
			$grandTotal += $db->f('totalCharge');
			$i++;
		}
		$tpl->gotoBlock( "_ROOT" );
		$tpl->assign("V_grandTotal",number_format($grandTotal,2,'.',','));
		
		if($num_records >= $Page_Size)
			$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));
	
								
	}
	#====================================================================================================
	#	Delete Cart Record
	#----------------------------------------------------------------------------------------------------
	elseif($_GET['Action']==DELETE || $_POST['Action']==DELETE)
	{
		$status = $objCart -> Delete_Cart_Detail($_POST['cartId']);
		header("location: reservationdetail.php?&confirmationNo=".$_POST['confirmationNo']."&delete=true");
	}
	

	$tpl->display('default_layout'. $config['tplEx']);

?>