<?php
#====================================================================================================
# File Name : triprate.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_ADMIN', 	true);
if($_GET['Action'] == 'View')
	define('POPUP_WIN', 	true);

# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'TripRate.php');
include_once($physical_path['DB_Access']. 'TripType.php');
include_once($physical_path['DB_Access']. 'Destination.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : 'ShowAll');
//print "Action=".$Action;

# Initialize object with required module
$objRate	= new TripRate();
$objType	= new TripType();
$objDest 	= new Destination();

$scriptName = "triprate.php";

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Add disk case
#-----------------------------------------------------------------------------------------------------------------------------
if($Action == 'Add')
{
	$ret = $objRate->Insert($_POST);
	header("location: $scriptName?add=true");
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Save disk case
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == 'Edit')
{
	$ret = $objRate->Update($_POST);
	/*header("location: $scriptName?save=true");
	exit();*/
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Delete disk case
#-----------------------------------------------------------------------------------------------------------------------------
elseif($Action == "Delete")
{
	$ret = $objRate->Delete($_POST['pk']);
	header("location: $scriptName?delete=true");
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
if($_GET['add']==true)
	$succMessage = $objRate->Data['L_Module']. " has been added successfully!!";
elseif($_GET['save']==true)
	$succMessage = $objRate->Data['L_Module']. " has been saved successfully!!";
elseif($_GET['delete']==true)
	$succMessage = $objRate->Data['L_Module']. " been deleted successfully!!";

$tpl->assign(array( 'T_Body'		=>	'custom_page'. $config['tplEx'],
					'JavaScript'	=>	array('triprate.js'),
					'A_Action'		=>	$scriptName,
					'Action'		=>	'Edit',
					'L_Module'		=>	$lang['L_TripRate'],
					'H_HelpText'	=>	$lang['L_Manage_TripRate'],
					'includeFile'	=>	'triprate'. $config['tplEx'],
					'DestinationInfo'=>	$objDest->getAdminAll(" GROUP BY dest_id ORDER BY dest_id "),
					'PassengerRange'=>	$objDest->getAllPassenger(),					
					'TripTypeInfo'	=>	$objType->ViewAll(),
					'R_RecordSet'	=>	$objRate->getAll(),
					'succMessage'	=>	$succMessage,
					));

$tpl->assign(array(	"L_TripRate"			=>	$lang['L_TripRate'],
					"L_No_Gaps"				=>	$lang['L_No_Gaps'],
					"L_Manage_TripRate"  	=>  $lang['L_Manage_TripRate'],						
					"L_Destinations"		=>	$lang['L_Destinations'],
					"L_Add_New"				=>	$lang['L_Add_New'],
					"L_Passenger_Range"		=>	$lang['L_Passenger_Range'],						
					));

$tpl->display('default_layout'. $config['tplEx']);

?>