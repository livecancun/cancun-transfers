<?php
#====================================================================================================
# File Name : user.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_ADMIN', 	true);
if($_GET['Action'] == 'View')
	define('POPUP_WIN', 	true);

# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'Cart.php');
include_once($physical_path['DB_Access']. 'Reservation.php');
include_once($physical_path['DB_Access']. 'Destination.php');
include_once($physical_path['DB_Access']. 'UserMaster.php');

#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
# Initialize object with required module
$objCart	= new Cart();
$objRes		= new Reservation();
$objDest 	= new Destination();
$objUser 	= new UserMaster();

$scriptName = "user.php?&start=$start_record";

#==================================================================================================
# Set the Starting Page, Page Size
#==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 10;
$num_records = '';


	if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL ||  $_POST['Submit']==CANCEL || $_POST['Submit']=="Back To Search" ||  $_POST['Submit']==BACK)
	{

		$tpl->assign(array( 'T_Body'		=>	'custom_page'. $config['tplEx'],
							'JavaScript'	=>	array('user.js'),
							'A_Action'		=>	$scriptName,
							'includeFile'	=>	'user_showall'. $config['tplEx'],
							));
	
		if($delete == true)
		{
			$succ_message = $lang['Msg_User_Deleted'];
		}
		if($nodelete == true)
		{
			$succ_message = $lang['Msg_User_NoDeleted'];
		}
	
		$tpl->assign(array(	"ACTION"			=>  SHOW_ALL,
							"start_record"		=>	$start_record,
							"Message"			=>  $succ_message,
							"ErrorMessage"		=>  $err_message,
							"L_Reservation"			=>	$lang['L_Reservation'],
							"L_Manage_Reservation"	=>	$lang['L_Manage_Reservation'],
							"Confirm_Delete"	=>	$lang['msgConfirmDeleteAll'],
							"L_User_Details" 	=> 	$lang['L_Reservation_Details'],
							"L_ConfirmationNo"  => 	$lang['L_ConfirmationNo']
							));
							
		$tpl->assign(array( "SrNo"				=> $lang['SrNo'],
							"L_Name"			=> $lang['name'],
							"L_User_Name"		=> $lang['L_User_Name'],
							"L_Email"			=> $lang['email'],
							"Action"			=> $lang['Action'],
							'UserInfo'			=> $objUser->ViewUsers($start_record,$Page_Size),
							));
		
		
		if($num_records >= $Page_Size)
			$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));
	
	}		

	elseif($_POST['Action']==SHOW || $_GET['Action']==SHOW)
	{

		$tpl->assign(array( 'T_Body'		=>	'custom_page'. $config['tplEx'],
							'JavaScript'	=>	array('user.js'),		
							'A_Action'		=>	$scriptName,
							'includeFile'	=>	'user_detail'. $config['tplEx'],
							));

		$tpl->assign(array( "A_User"			=>  "user.php?&start=$start_record",
							"ACTION"			=>  SHOW,
							"start_record"		=>	$start_record,
							"Message"			=>  $succ_message,
							"user_id"			=>  $_POST['user_id'],
							"Back"				=>  $lang['Back'],
							"L_User_Details"    =>  $lang['L_User_Details']
							));
		
		$tpl->assign(array( "L_User_Name"		 => $lang['L_User_Name'],
							"L_Name"			 => $lang['name'],
							"L_Address"			 => $lang['address'],
							"L_Country"			 => $lang['country'],
							"L_State"			 => $lang['state'],
							"L_City"			 => $lang['city'],
							"L_Zip"				 => $lang['zip'],
							"L_Phone_Number"	 => $lang['phoneno'],
							"L_Email"			 => $lang['email']
						));	
	
		$sql = $objUser->ShowUser($_POST['user_id']);
		
		if($db->num_rows() <= 0)
				$tpl->assign("Notfound_Message",$lang['Msg_User_Norecord']);
		else
		{
				$db->next_record();
				$tpl->assign(array( "userName"			=>	$db->f('email'),
									"Firstname"  	 	=>	ucfirst(stripslashes($db->f('firstname'))),
									"Lastname"			=>  ucfirst(stripslashes($db->f('lastname'))),
									"V_Address1"        =>  stripslashes($db->f("address1")),
									"V_Address2"        =>  stripslashes($db->f("address2")),
									"CountryName"		=>	stripslashes($db->f('CountryName')),
									"V_City"			=>	stripslashes($db->f('city')),
									"V_State"			=>	stripslashes($db->f("state")),
									"V_Zip"		        =>	$db->f("zip"),
									"V_Phone_Number"	=>	$db->f("phoneno"),
									"V_Email"			=>	stripslashes($db->f("email"))
									));
		 }
	
	}

	elseif($_POST['Action']==DELETE || $_GET['Action']==DELETE)
	{
		$status = Delete_Reservation_Detail($_POST['user_id'],$_POST['confirmationNo']);
		header("location: user.php?&delete=true");
	}
	
	$tpl->display('default_layout'. $config['tplEx']);

?>