<?php
#====================================================================================================
# File Name : emailconfig.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
#define('IN_SITE', 	true);
define('IN_ADMIN', 	true);

include_once("../includes/common.php");

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------------------
#	Update Config information
#-----------------------------------------------------------------------------------------------------------------------------
if($_POST['Submit'] == "Update")
{
	$webConf->Set(WC_EMAILFROM_NAME, 	$_POST[WC_EMAILFROM_NAME]);
	$webConf->Set(WC_SUPPORT_EMAIL, 	$_POST[WC_SUPPORT_EMAIL]);
	$webConf->Set(WC_REPLY_TO,       	$_POST[WC_REPLY_TO]);
	$webConf->Set(WC_DEFAULT_SIGNATURE, $_POST[WC_DEFAULT_SIGNATURE]);
	$webConf->Set(WC_EMAIL_FOOTER, 		$_POST[WC_EMAIL_FOOTER]);

	header('location: emailconfig.php?update=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
else if($_POST['Submit'] == "Cancel")
{
	header('location: index.php');
	exit();
}

#=======================================================================================================================================
#											RESPONSE CREATING CODE
#---------------------------------------------------------------------------------------------------------------------------------------

if($_GET['update']==true)
{
	$succMessage = "Email configuration has been updated successfully!!";
}

$tpl->assign(array("T_Body"			=>	'emailconfig'. $config['tplEx'],
					"JavaScript"	=>  array("emailconfig.js"),
					"A_Action"		=>	"emailconfig.php",
					"succMessage"	=>	$succMessage));

$tpl->assign(array( WC_EMAILFROM_NAME		=>	$config[WC_EMAILFROM_NAME],
					WC_SUPPORT_EMAIL		=>	$config[WC_SUPPORT_EMAIL],
					WC_REPLY_TO	            =>	$config[WC_REPLY_TO],
					WC_DEFAULT_SIGNATURE	=>	$config[WC_DEFAULT_SIGNATURE],
					WC_EMAIL_FOOTER		    =>	$config[WC_EMAIL_FOOTER],
					));

$tpl->display('default_layout'. $config['tplEx']);
?>