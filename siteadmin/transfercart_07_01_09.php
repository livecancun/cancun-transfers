<?php
#====================================================================================================
# File Name : transfercart.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_ADMIN', 	true);

if($_GET['Action'] == 'View')
	define('POPUP_WIN', 	true);

# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'Cart.php');
include_once($physical_path['DB_Access']. 'Reservation.php');
include_once($physical_path['DB_Access']. 'Destination.php');
include_once($physical_path['DB_Access']. 'UserMaster.php');
include_once($physical_path['DB_Access']. 'TripRate.php');


# Initialize object with required module
$objCart	= new Cart();
$objRes		= new Reservation();
$objDest 	= new Destination();
$objUser 	= new UserMaster();
$objRate	= new TripRate();

$scriptName = "transfercart.php";

//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 10;
$num_records = '';

if($_POST['Submit'] == APPROVE)
{
	$sql = $objDest -> UpdateStatus_AP($_POST['carttranId'],$_POST['cartId']);
	$update = 1;

	//header("location: tourcart.php?update=true");
}

if($_POST['Submit'] == NOTAPPROVE)
{
	$sql = $objDest -> UpdateStatus_NA($_POST['carttranId'],$_POST['cartId']);
	$update = 1;

	//header("location: tourcart.php?update=true");
}


if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL ||  $_POST['Submit']==CANCEL || $_POST['Submit']=="Back To Search" ||  $_POST['Submit']==BACK || $_POST['Submit'] == APPROVE || $_POST['Submit'] == NOTAPPROVE)
{
	//header("location: reservationdetail.php?&confirmationNo=".$_POST['confirmationNo']);

	$tpl->assign(array( 'T_Body'		=>	'transfercart_showall'. $config['tplEx'],
						'JavaScript'	=>	array('transfercart.js'),
						'A_Action'		=>	$scriptName,
						));

	$start_record = 0;

	if($delete == true)
	{
		$succ_message = $lang['Reservation_Deleted'];
	}
	if($nodelete == true)
	{
		$succ_message = $lang['CannotDelete'];
	}
	if($update == 1)
	{
		$succ_message = $lang['Transfer_Cart_Updated'];
	}

	$user_id = isset($_POST['user_id'])?$_POST['user_id']:$_GET['user_id'];
	
	$SortBy = isset($_GET['SortBy'])?$_GET['SortBy']:"transferRequestNo";

	$sql = $objUser -> ShowUserDisc($user_id);
	$db->next_record();
			 
	$tpl->assign(array(	"A_TransferCart"		=>  "transfercart.php?&start=$start_record",
						"ACTION"				=>  SHOW_ALL,
						"user_id"				=>  $user_id,
						"Firstname"    	   		=>  ucfirst(stripslashes($db->f('firstname'))),
						"Lastname"         		=>  ucfirst(stripslashes($db->f('lastname'))),
						"start_record"			=>	$start_record,
						"Message"				=>  $succ_message,
						"ErrorMessage"			=>  $err_message,
						"Confirm_Delete"		=>	$lang['msgConfirmDeleteAll'],
						"Transfer_Cart_Details" =>  $lang['Transfer_Cart_Details']));
						
	$tpl->assign(array( "SrNo"					=> $lang['SrNo'],
						"L_transferRequestNo"	=> $lang['transferRequestNo'],
						"L_Destinations" 		=>	$lang['L_Destinations'],
						"L_transferStatus"		=> $lang['transferStatus'],	
						"Action"				=> $lang['Action']));

	$sql = $objDest -> Select_CartDest($user_id,$SortBy,$start_record,$Page_Size);
	
	$i = 0;
	$rscnt = $db->num_rows();
	
	if($rscnt == 0)
	{
		$tpl->assign("Notfound_Message",$lang['Msg_TransferReserve_Norecord']);
	}

		if($SortBy == 'tourStatus')
			$tpl->assign("imgSortStatus", "<img src='../templates/images/arr.gif' border='0'>");
		else
			$tpl->assign("imgSortNo", "<img src='../templates/images/arr.gif' border='0'>");

	while($i < $rscnt)
	{
		$db->next_record();
		$tpl->newBlock("B_Cart_Info");
		$tpl->assign(array( "cartId"				=>  $db->f('cart_id'),
							"carttranId"			=>  $db->f('carttranId'),
							"V_transferRequestNo"	=>	$db->f('transferRequestNo'),
							"V_transferStatus"		=>  $db->f('trStatus'),
							"Sr_No" 				=>  $start_record+$i+1,
							"View_Details" 			=>  $lang['View'],
							"Delete"   				=>  $lang['Delete']));
							
			if($db->f('trip_status') == 2)
			{
				$tpl->assign(array( "V_transferOrgin"		 => $db->f('transferOrigin'),	
									"V_transferDestination"	 => $db->f('transferDestination'),
									"Transfer_Image"		 => "BRoundTrip.gif",
									"imagepath"		 		 =>	 $physical_path['Image_Root']
								));	
			}
			if($db->f('trip_status') == 1)
			{
				$tpl->assign(array( "V_transferOrgin"		 => $db->f('transferDestination'),	
									"V_transferDestination"	 => $db->f('transferOrigin'),
									"Transfer_Image"		 => "BOneWay.gif",
									"imagepath"		 		 =>	 $physical_path['Image_Root']
								));	
		
			}
			if($db->f('trip_status') == 0)
			{
					$tpl->assign(array( "V_transferOrgin"		 => $db->f('transferOrigin'),	
										"V_transferDestination"	 => $db->f('transferDestination'),
										"Transfer_Image"		 => "BOneWay.gif",
										"imagepath"		 		 =>	 $physical_path['Image_Root']
								));	
			}		
		$i++;
	}

	
	if($num_records >= $Page_Size)
		$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));

}		
elseif($_POST['Action']==SHOW || $_GET['Action']==SHOW)
{

	$tpl->assign(array( 'T_Body'		=>	'transfercart_detail'. $config['tplEx'],
						'JavaScript'	=>	array('transfercart.js'),
						'A_Action'		=>	$scriptName,
						'L_Module'		=>	$objRate->Data['L_Module'],
						'H_HelpText'	=>	$objRate->Data['H_Manage'],
						"L_Final"	 	=>	$lang['L_Final'],
						"L_Discount"	=>	$lang['L_Discount'],
						"L_Origin"		=>	$lang['L_Origin'],
						"L_No_Person"	=>	$lang['L_No_Person'],
						"L_Hotel"		=>	$lang['L_Hotel'],
						"Arrival"		=>	$lang['Arrival'],
						"Departure"		=>	$lang['Departure'],	
						"L_Applicable_Discount"	=>	$lang['L_Applicable_Discount'],							
						"L_Company_Name"=>	$lang['L_Company_Name'],	
						"L_Company_ID"	=>	$lang['L_Company_ID'],	
						"L_Discount"	=>	$lang['L_Discount'],							
						));

	$user_id = isset($_POST['user_id'])?$_POST['user_id']:$_GET['user_id'];
	$cartId = isset($_POST['cartId'])?$_POST['cartId']:$_GET['cartId'];

	$sql = $objUser -> ShowUserInfoByConfNo($confirmationNo); 
	$db->next_record();
	
			 
	$tpl->assign(array(	"Firstname"    		   	=>  ucfirst(stripslashes($db->f('firstname'))),
						"Lastname"    	     	=>  ucfirst(stripslashes($db->f('lastname'))),
						"Transfer_Cart_Details" =>  $lang['Transfer_Cart_Details'],
						"V_ConfirmationNo"	    =>  $_POST['confirmationNo'],
						"View"				    =>  $lang['View']
						));


	$tpl->assign(array( "A_TransferCart"	=>  "transfercart.php?&start=$start_record",
						"ACTION"			=>  SHOW,
						"start_record"		=>	$start_record,
						"Message"			=>  $succ_message,
						"user_id"			=>  $_POST['user_id'],
						"Back"				=>  $lang['Back'],
						"L_Transferdetail"  =>  $lang['transferdetail']));
						
    $tpl->assign(array( "Note"				 => $lang['L_Note'],
						"NoRequest"	 		 => $lang['Note_NR'],
						"PendingRequest"	 => $lang['Note_PR'],							
						"ApproveRequest"	 => $lang['Note_AR'],							
						"NotApprove"		 => $lang['Note_NA'],							
						"RequestPayment"	 => $lang['Note_RP'],							
						"PaymentDone"		 => $lang['Note_PD']));							

	$tpl->assign(array( "TransferDetails"    => $lang['transferdetail'],
						"TransferRates" 	 => $lang['L_Transfer_Rates'],
					    "L_Destinations"  	 => $lang['L_Destinations'],
						"L_Total"			 =>	$lang['L_Total'],
						"L_transferRequestNo"=> $lang['transferRequestNo'],
						"L_Hotelname"		 => $lang['L_Hotel'],
						"L_No_Person" 	 	 => $lang['L_No_Person'],
						"NoofAdultPerson" 	 => $lang['noofadult'],
						"NoofKid"			 => $lang['noofkid'],
						"L_Transferstatus"	 => $lang['transferStatus'],
						"Pay"           	 => $lang['payment'],
						"SingleTrip"		 => $lang['L_singleTrip'],
						"L_Trasferdetail"	 => $lang['transferdetail'],
						"RoundTrip"			 => $lang['L_roundTrip']));
		
	$tpl->assign(array(	"I_Template_URL"      =>  $url_path['AdminTemplates_URL'],
						"A_TransferReservation"	 =>  "reservationdetail.php?&start=$start_record",
						"ACTION"			 =>  SHOW,
						"Message"			 =>  $succ_message));


	$tpl->assign(array(	"L_arrivalDate"		 => $lang['arrivaldate'],
						"L_departureDate"	 => $lang['departuredate'],
						"L_arrivalTime"		 => $lang['arrivaltime'],
						"L_departureTime"	 => $lang['departuretime'], 
						"L_arrivalAirline"	 => $lang['arrivalairline'],
						"L_departureAirline" => $lang['departureairline'],
						"L_arrivalFlight" 	 => $lang['arrivalflight'],
						"L_departureFlight"  => $lang['departureflight']));

	$tpl->assign(array(	"L_arrivalDate"		 => $lang['arrivaldate'],
						"L_arrivalTime"		 => $lang['arrivaltime'],
						"L_arrivalAirline"	 => $lang['arrivalairline'],
						"L_arrivalFlight" 	 => $lang['arrivalflight']));

	$tpl->assign(array(	"L_departureDate"	 => $lang['departuredate'],
						"L_departureTime"	 => $lang['departuretime'], 
						"L_departureAirline" => $lang['departureairline'],
						"L_departureFlight"  => $lang['departureflight']));

	$sql =  $objDest -> Show_CartDest($cartId,$start_record,$Page_Size);
	
	if($db->num_rows() <= 0)
			$tpl->assign("Notfound_Message",$lang['Msg_TransferReserve_Norecord']);
	else
	{
		if($db->num_rows() > 0 )
		{
			$db->next_record();

			$user_id = $db->f('user_id');
			$departureDestId = $db->f('departureDestId');

			$tpl->assign(array("cartId"			 	=> $db->f('cart_id'),
							"is_airline_empl"		=> $db->f('is_airline_empl'),
							"carttranId"   	 		=> $db->f('carttranId'),
							"transferRequestNo"	 	=> $db->f('transferRequestNo'),
							"destination" 	 		=> $db->f('dest_title'),
							"tres_no_person" 	 	=> $db->f('total_person'),
							"tres_no_adult" 	 	=> $db->f('tres_no_adult'),
							"tres_no_kid"		 	=> $db->f('tres_no_kid'),
							"cartStatus" 		 	=> $db->f('cartStatus'),	
							"hotelName"          	=> stripslashes($db->f('hotelName')),	
							"transferCharge"     	=> $db->f('totalAmount'),
							"V_transferId"		   	=> $db->f('transferId'),
							"V_transferOrgin"      	=> $db->f('transferOrigin'),
							"V_transferDestination" => $db->f('transferDestination'),
							"trip_status" 			=> $db->f('trip_status'),
						//	"departureDestination"	=> $objDest -> Show_Destinations($departureDestId),
							"departureTotalPerson" 	=> $db->f('departureTotalPerson'),
							"departureHotelName" 	=> $db->f('departureHotelName'),																					
							));

			if($db->f('triptype_id') == 1)
			{	
				$tpl->assign("transferSingleTripRate", $db->f('rate'));
			}					
			
			if($db->f('triptype_id') == 2)
			{
				$tpl->assign("transferRoundTripRate", $db->f('rate'));
			}


			$tpl->assign(array(	"arrivalTime"		 => $db->f('arrivalTime'),
								"departureTime"		 => $db->f('departureTime'),
								"arrivalAirline"	 => $db->f('arrivalAirline'),
								"arrivalFlight"		 => $db->f('arrivalFlight'),
								"departureAirline"	 => $db->f('departureAirline'),
								"departureFlight"	 => $db->f('departureFlight')));
												
/*				if($db->f('arrivalDate')!=0000-00-00)
				{
*/					$tpl->assign(array(	"arrivalDate"	     =>	formatDate($db->f('arrivalDate'),'d-M-Y'),
										));
/*				}
*/
/*				if($db->f('departureDate')!= 0000-00-00)
				{
*/					$tpl->assign(array(	"departureDate"	     =>	formatDate($db->f('departureDate'),'d-M-Y'),
										));
/*				}
*/

			$tpl->assign("imagepath", $physical_path['Image_Root']);

			if($db->f('trip_status') == 2)
			{	
				$tpl->assign("Transfer_Image", "BRoundTrip.gif");
			}					
			else
			{
				$tpl->assign("Transfer_Image", "BOneWay.gif");
			}

			if($db->f('cartStatus') == "AR")
			{
				$tpl->assign("NA",NOTAPPROVE);
			}
			if($db->f('cartStatus') == "NA")
			{
				$tpl->assign("AR",APPROVE);
			}
			if($db->f('cartStatus') == "PR")
			{
				$tpl->assign("AR",APPROVE);
				$tpl->assign("NA",NOTAPPROVE);
			}
		}
	

    }
	
	$tpl->assign(array("departureDestination"	=> $objDest -> Show_Destinations($departureDestId),
					));

	//================================== Discount Info ==================================//
	$sql = $objUser -> ShowUserInfoByConfNo($confirmationNo); 
	$db->next_record();
	
	$tpl->assign(array("is_airline_empl"	=> $db->f('is_airline_empl'),
					   "discount_id"     	=> $db->f('discount_id'),
					   "discount_rate"	    => $db->f('discount_rate'),
					   "discount_title"	    => ucfirst($db->f('discount_title')),
					   "empl_id" 	    	=> $db->f('empl_id'),
					   "empl_airline"		=> ucfirst($db->f('empl_airline')),
					   ));
	//=====================================================================================//	

	if($num_records > $Page_Size)
		$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));
}

elseif($_POST['Action']==DELETE || $_GET['Action']==DELETE)
{
	$sql = $objCart	-> Show_ConfrmCartDetail($_POST['carttranId']);
	$db->next_record();

	$arrivaldate = $db->f('arrivalDate');
	$today = date("Y-m-d"); 
	$user_id = $db->f('user_id');
	
	if ($arrivaldate > $today)
	{
		header("location: transfercart.php?&nodelete=true&user_id=".$user_id);
	}
	else
	{
		$sql = $objCart	-> Delete_Res_Detail($_POST['carttranId'],$_POST['cartId']);
		
		header("location: transfercart.php?&delete=true&user_id=".$user_id);
	}
}
	$tpl->display('default_layout'. $config['tplEx']);

?>