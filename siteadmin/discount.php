<?php
#====================================================================================================
# File Name : discount.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_ADMIN', 	true);

if($_GET['Action'] == 'View')
	define('POPUP_WIN', 	true);

# include the required file
include_once('../includes/common.php');
include_once($physical_path['DB_Access']. 'Discount.php');

# Initialize object with required module
$objDisco	= new Discount();

$scriptName = "discount.php?&start=$start_record";

/*print "Action=".$_POST['Action'];
print "Submit=".$_POST['Submit'];
*/#====================================================================================================
#	Add Service into database
#----------------------------------------------------------------------------------------------------
if($_POST['Action']==ADD && $_POST['Submit']==SAVE)
{	
	$no = $objDisco->Add_Discount($_POST['discount_for'],$_POST['discount_for_sp'],$_POST['discount_rate']);
	$startpt = $_GET['start'];
	header("location: discount.php?&start=$startpt&add=true");
	exit;
}
#====================================================================================================
#	Update Service into database
#----------------------------------------------------------------------------------------------------
if($_POST['Action']==MODIFY && $_POST['Submit']==SAVE)
{	
	$no = $objDisco->Update_Discount($_POST['discount_id'],$_POST['discount_for'],$_POST['discount_for_sp'],$_POST['discount_rate']);
	$startpt = $_GET['start'];
	header("location: discount.php?&start=$startpt&update=true");
	exit;
}
#====================================================================================================
#	Delete Service
#----------------------------------------------------------------------------------------------------
elseif($_GET['Action']==DELETE || $_POST['Action']==DELETE)
{
	$no = $objDisco->Delete_Discount($_POST['discount_id']);
	$startpt = $_GET['start'];
	header("location: discount.php?&start=$startpt&delete=true");
	exit;
}
//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0; 
else
	$start_record = $_GET['start']; 
$Page_Size = 10;
$num_records = '';

#====================================================================================================
#	List all Pages
#----------------------------------------------------------------------------------------------------

if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Submit']==BACK || $_POST['Submit']==CANCEL)
{

	$tpl->assign(array( 'T_Body'		=>	'discount_showall'. $config['tplEx'],
						'JavaScript'	=>	array('discount.js'),
						'A_Action'		=>	$scriptName,
						));

     $tpl->assign(array( "L_Discount_Manager" 	=>  $lang['L_Discount_Manager'],
						 "L_Manage_Discount"	=>  $lang['L_Manage_Discount'],
						));	


	if($_GET['add'] == true)
	{
		$succ_message = "<br>". $lang['Msg_Discount_Added'];
	}
	if($_GET['update'] == true)
	{
		$succ_message = "<br>". $lang['Msg_Discount_Updated'];
	}
	if($_GET['delete'] == true)
	{
		$succ_message = "<br>". $lang['Msg_Discount_Deleted'];
	}
	$tpl->assign(array(	"A_Action"			 =>  "discount.php?&start=$start_record",
						"ACTION"			 =>  SHOW_ALL,
						"Start"				 =>	 $start_record,
						"Message"			 =>	 $succ_message,
						"L_Back"			 =>	 BACK,
						"Confirm_Delete_Msg" =>  $lang['Confirm_Delete_Msg'],
						"Heading"			 =>	 $lang['Heading_Manage_TravelRental'],
						"SrNo"				 =>  $lang['SrNo'],
						"Action"			 =>  $lang['Action']
						));

     $tpl->assign(array( "L_Travel_Discount" 	=>  $lang['L_Travel_Discount'],
						 "L_Discount_For"	 	=>  $lang['L_Discount_For'],
						 "L_Discount_Rate"	 	=>  $lang['L_Discount_Rate'],
						 "L_Manage_Travel_Discount"=>  $lang['L_Manage_Travel_Discount'],						
						 "Empty_Discount_For"   =>  $lang['Empty_Discount_For'],
						 "Confirm_Delete_Msg"	=>  $lang['Msg_Confirm_Delete_Discount'],
						));	

		
	$tpl->assign(array("L_Travel_Discount" 		=>  $lang['L_Travel_Discount'],
						"L_Add_Travel_Discount"	=>  $lang['L_Add_Travel_Discount'],
					    "L_Discount_For"		=>	$lang['L_Discount_For'],
						"L_Action"		   		=>	$lang['L_Action'],
						"Modify"				=>  $lang['Modify'],
						"Delete"   				=>  $lang['Delete']
						));
			
	$tpl->assign(array("DiscInfo" 	=>  $objDisco->Show_Discount($discount_id='', $start_record, $Page_Size, $num_records),
						));

		
	if($num_records > $Page_Size)
		$tpl->assign("Page_Link", $lang['L_GoTO'] . ' ' . generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));

}
#====================================================================================================
#	update page
#----------------------------------------------------------------------------------------------------
elseif($_GET['Action']==ADD || $_POST['Action']==ADD || $_GET['Action']==MODIFY || $_POST['Action']==MODIFY)
{

	$tpl->assign(array( 'T_Body'		=>	'discount_addedit'. $config['tplEx'],
						'JavaScript'	=>	array('discount.js'),
						'A_Action'		=>	$scriptName,
						));

	$tpl->assign(array(	"A_Action"			=> "discount.php?&start=$start_record",
						"Start"				=> $start_record,
						"L_Back"			=> BACK,
						"Save" 				=> SAVE,
						"Cancel"			=> CANCEL,
						));

     $tpl->assign(array( "L_Discount_Manager" 	=> $lang['L_Discount_Manager'],
						 "L_Manage_Discount"	=> $lang['L_Manage_Discount'],
						));	

	$tpl->assign(array( "L_Travel_Discount" 	  => $lang['L_Travel_Discount'],
						"L_Discount_For"	 	  => $lang['L_Discount_For'],
						"L_Discount_Rate"	 	  => $lang['L_Discount_Rate'],
						"Empty_Discount_For" 	  => $lang['Empty_Discount_For'],
						"Empty_Discount_Rate" 	  => $lang['Empty_Discount_Rate'],
						"Valid_Discount_Rate"     => $lang['Valid_Discount_Rate'],
						"Heading"			 	  => $lang['Heading_Manage_TravelRental'],
						"L_LinkName_Discount" 	  => $lang['L_LinkName_Discount'],						
				));	
	#====================================================================================================
	#	Add Service details
	#----------------------------------------------------------------------------------------------------
	if($_GET['Action']==ADD || $_POST['Action']==ADD)
	{
		$tpl->assign(array("ACTION"		=> 	ADD,
						   "L_Action"	=>  $lang['L_Add'],
						   "Add" 		=>  ADD,
					));
	}							
	#====================================================================================================
	#	Modify Service details
	#----------------------------------------------------------------------------------------------------
	else if($_GET['Action']==MODIFY || $_POST['Action']==MODIFY)
	{
		$tpl->assign(array("ACTION"			=> 	MODIFY,
						   "L_Action"		=>  $lang['L_Modify'],
						   "discount_id"	=>	$_POST['discount_id'],
						   "Edit" 			=>  EDIT,
					));


		$record = $objDisco->Show_Discnt($_POST['discount_id']);
		$db->next_record();

		$tpl->assign(array(	"discount_id"   	=> 	$db->f('discount_id'),
							"discount_for"		=>  stripslashes($db->f('discount_title')),
							"discount_for_sp"	=>  stripslashes($db->f('discount_title_sp')),							
							"discount_rate" 	=> 	$db->f('discount_rate'),
							));
	}
	
}

	$tpl->display('default_layout'. $config['tplEx']);
	
?>