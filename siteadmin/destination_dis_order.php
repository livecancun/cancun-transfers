<?php
#====================================================================================================
#	File Name		:	destination_dis_order.php
#----------------------------------------------------------------------------------------------------
#	Purpose			:	This is to manage dispaly order of sport
#	Copyright		:	Copyrights � 2003 Dot Infosys
#	Email			:	info@dotinfosys.com
#====================================================================================================

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
//define('IN_SITE', 	true);
define("IN_ADMIN", true);
include_once("../includes/common.php");
include_once($physical_path['DB_Access']."Destination_dis_order.php");

define("DEBUG", false);

if(DEBUG)
{
	print "<br>Post Action = ". $_POST['Action'];
	print "<br>Get Action = ". $_GET['Action'];
	print "<br>Post Submit = ". $_POST['Submit'];
	print "<br>Get Submit = ". $_GET['Submit'];
}


#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : A_VIEW_ALL);

$objdest = new Destination();

#=======================================================================================================================================
#								RESPONSE PROCESSING CODE
#---------------------------------------------------------------------------------------------------------------------------------------
$redirectUrl = 'destination_dis_order.php';
#====================================================================================================
#	Sort all sports
#----------------------------------------------------------------------------------------------------
if($Action == "Sort" && $_POST['Submit'] == "Save")
{
	# SPLIT DISPLAY ORDER
	$sorted_list = split(":", $_POST['sorted_list']);
	# UPDATE THE DISPLAY ORDER POSITION
	for($i=0; $i<count($sorted_list); $i++)
	{
		$ret = $objdest ->setDestDisOrder($sorted_list[$i], $i+1);
	}
	header('location: destination_dis_order.php?sort=true');
	exit();
}
#-----------------------------------------------------------------------------------------------------------------------------
#	Cancel
#-----------------------------------------------------------------------------------------------------------------------------
elseif($_POST['Submit'] == "Cancel")
{
	header('location: menu_settings.php');
	exit();
}

#====================================================================================================
#	List all SPORTS
#----------------------------------------------------------------------------------------------------
	$tpl->assign(array("T_Body"				=>	'destination_dis_order'. $config['tplEx'],
						"JavaScript"		=>  array("destination_dis_order.js"),
						"A_Action"			=>  "destination_dis_order.php",
					));
		
	$tpl->assign(array(	"Action"				=>	$Action,
					));
	if($_GET['sort'] == true)
		$succ_message = "Destinations display order is changed successfully.";
		
	$SortData_List = $objdest->ViewallDestinationsList();
//	print_r($SortData_List );	
	$arrDestinations= array();
	foreach($SortData_List as $DestinationList)
	{
		$arrDestinations[$DestinationList['dest_id']] = $DestinationList['dest_title'];
	}
	
	$tpl->assign(array(	"SuccMessage"	=>	$succ_message,
						"SortData_List"	=>	$arrDestinations,
						));	
		

$tpl->display('default_layout'. $config['tplEx']);
?>