<?php
#====================================================================================================
# File Name : AdminPrivilege.php 
#----------------------------------------------------------------------------------------------------
# Purpose : This file contains all application configuration details
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#====================================================================================================

#====================================================================================================
# Admin Privileges 
#====================================================================================================
# Master
$lang['AdminPrivileges']['Master']	=	array(
				TITLE		=>	'Master',
				SUBOPTION	=>	array(	'Home' 				=> array(TITLE	=>	'Home',
																	 DESC	=>	'Home',
																	 LINK	=>	'index.php'),
										'DestinationManager'=> array(TITLE	=>	'Destination Manager',
																	 DESC	=>	'Manage destination content',
																	 LINK	=>	'destination.php'),
										'DestinationOrderManager'=> array(TITLE	=>	'Destination Order Manager',
																	 DESC	=>	'Manage destination order',
																	 LINK	=>	'destination_dis_order.php'),
										'TripTypeManager'	=> array(TITLE	=>	'Trip Type Manager',
																	 DESC	=>	'Manage Trip Type',
																	 LINK	=>	'triptype.php'),
										'TripRateManager'	=> array(TITLE	=>	'Trip Rate Manager',
																	 DESC	=>	'Manage Trip Rate',
																	 LINK	=>	'triprate.php'),
										'Reservation' 		=> array(TITLE	=>	'Reservation',
																	 DESC	=>	'Manage Reservation',
																	 LINK	=>	'reservation.php'),
										'Discount' 			=> array(TITLE	=>	'Discount',
																	 DESC	=>	'Manage Discount',
																	 LINK	=>	'discount.php'),
										'TestimonialManager'=> array(TITLE	=>	'Testimonial Manager',
																	 DESC	=>	'Manage Testimonial',
																	 LINK	=>	'testimonial.php'),
										'LinksManager' 		=> array(TITLE	=>	'Link Manager',
																	 DESC	=>	'Manage Links',
																	 LINK	=>	'links.php'),

									)
			);

# Settings
$lang['AdminPrivileges']['Settings']	=	array(
				TITLE		=>	'Settings',
				SUBOPTION	=>	array(	'SiteConfig' 		=> array(TITLE	=>	'Site Config',
																	 DESC	=>	'Update Site Configuration',
																	 LINK	=>	'siteconfig.php'),
										'ChangePassword' 	=> array(TITLE	=>	'Change Password',
																	 DESC	=>	'Change your account password',
																	 LINK	=>	'changepassword.php'),
/*										'EmailConfig' 		=> array(TITLE	=>	'Email Config',
																	 DESC	=>	'Email Configuration',
																	 LINK	=>	'config.php?config=Email'),										
										'EmailTemplate' 	=> array(TITLE	=>	'Email Template',
																	 DESC	=>	'Manage Email Templates',																	 
																	 LINK	=>	'emailtemplate.php'),
*/									)
			);
?>