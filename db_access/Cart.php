<?php
#====================================================================================================
# File Name : Cart.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contain the required function used during login to site
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

class Cart
{

	/* public: filter parameters */
	var $dest_id		=	'';
	var $cart_id		=	'';	
	
	/* public: Pagination  */
	var $page_size		=	'';
	var $start_record	=	'';
	var $total_record	=	'';

	/* public: result array and current row number */
	var $Record   		= array();

	var $Debug         	= false;     ## Set to true for debugging messages.

	#====================================================================================================
	#	Function Name	:   Cart
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function Cart()
    {
		# Do nothing
	}

		#====================================================================================================
		#	Function Name	:   Show_Cart_Detail
		#	Purpose			:	It shows user's Cart data from database
		#	Return			:	returns the recordset for the specified partner_id
		#	Creation Date	:	03-June-2003
		#----------------------------------------------------------------------------------------------------
		function Show_Cart_Detail($user_id,$cart_id='',$userType,$SortBy,$start_record, $Page_Size, &$num_records)
		{
			global $db;

					$sql = "SELECT * FROM ".CART_MASTER 
						  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "					
						  . " WHERE " .CART_MASTER.".user_id  = '".$user_id."' AND ".CART_MASTER.".cartStatus ='".NR."'"
						  . " GROUP BY ".CART_MASTER.".cart_id "
						  . " ORDER BY ".CART_MASTER.".".$SortBy;
						  
					 $db->query($sql);
					 $num_records = $db->num_rows();
					 $db->free();
		
					if($start_record >= $num_records && $start_record!=0)
						$start_record -= $Page_Size;
					
					$sql = "SELECT * FROM ".CART_MASTER 
						  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "					
						  . " WHERE " .CART_MASTER.".user_id  = '".$user_id."' AND ".CART_MASTER.".cartStatus ='".NR."'"
						  . " GROUP BY ".CART_MASTER.".cart_id "
						  . " ORDER BY ".CART_MASTER.".".$SortBy
						  . " LIMIT ". $start_record . ", ". $Page_Size ;
					
					$rs = $db->query($sql);	  		  		

			//print "sql=".$sql;
			$rs = $db->fetch_array();
	
			return($rs);
		}
		

		function Show_Request_Detail($user_id,$cart_id='',$SortBy,$start_record, $Page_Size, &$num_records)
		{
			global $db;
			
				$sql = "SELECT * FROM ".CART_MASTER 
					. " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "						  				
					. " WHERE " .CART_MASTER.".user_id  = '".$user_id."' AND ".CART_MASTER.".cartStatus NOT IN ('".NR."')"
					. " GROUP BY ".CART_MASTER.".cart_id ";

					 $db->query($sql);
					 $num_records = $db->num_rows();
					 $db->free();
		
					if($start_record >= $num_records && $start_record!=0)
						$start_record -= $Page_Size;
					
			$sql = "SELECT * FROM ".CART_MASTER 
				. " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "						  					
				. " WHERE " .CART_MASTER.".user_id  = '".$user_id."' AND ".CART_MASTER.".cartStatus NOT IN ('".NR."')"
				. " GROUP BY ".CART_MASTER.".cart_id "
				. " LIMIT ". $start_record . ", ". $Page_Size ;
		
			//echo $sql;			
			$rs = $db->query($sql);	  		  		
			$rs = $db->fetch_array();
						
			return ($rs);	
		}
		

		function Show_Cart($confirmationNo,$cart_id='',$SortBy,$start_record, $Page_Size, &$num_records)
		{
			global $db;
			
				$sql = "SELECT * FROM ".CART_MASTER 
					. " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "						  				
					. " WHERE " .CART_MASTER.".confirmationNo  = '".$confirmationNo."' AND ".CART_MASTER.".cartStatus NOT IN ('".NR."')"
					. " GROUP BY ".CART_MASTER.".cart_id ";

					 $db->query($sql);
					 $num_records = $db->num_rows();
					 $db->free();
		
					if($start_record >= $num_records && $start_record!=0)
						$start_record -= $Page_Size;
					
			$sql = "SELECT * FROM ".CART_MASTER 
				. " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "						  					
				. " WHERE " .CART_MASTER.".confirmationNo  = '".$confirmationNo."' AND ".CART_MASTER.".cartStatus NOT IN ('".NR."')"
				. " GROUP BY ".CART_MASTER.".cart_id "
				. " LIMIT ". $start_record . ", ". $Page_Size ;
		
			//echo $sql;			
			$rs = $db->query($sql);	  		  		
			$rs = $db->fetch_array();
						
			return ($rs);	
		}
		
		
		function Show_Send_Request_Detail($user_id,$cart_id,$SortBy)
		{
			global $db;
			
			$sql = "SELECT * FROM ".CART_MASTER 
				  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "			
				  . " WHERE ".CART_MASTER.".cart_id  = '".$cart_id."' AND ".CART_MASTER.".user_Id  = '".$user_id."' AND ".CART_MASTER.".cartStatus NOT IN ('".NR."')"
				  . " GROUP BY ".CART_MASTER.".cart_id "
				  . " ORDER BY ".CART_MASTER.".".$SortBy;
		//	print "<br>sql=".$sql."<br>";			
				  
			$rs = $db->query($sql);	  		  		
			$rs = $db->fetch_array();
	
			return($rs);
		}
		
		
		
		function Show_Item_Detail($cart_id,$itemType)
		{
			global $db;
			$itemName = '';
			

			if($itemType == 2)
			{
				 $sql = " SELECT ".CART_MASTER.".*,".CART_DESTINATIONS.".* FROM ".CART_MASTER
					   . " LEFT JOIN ". CART_DESTINATIONS. " ON ". CART_MASTER. ".dest_id = ". CART_DESTINATIONS. ".dest_id "
					   . " WHERE  ". CART_DESTINATIONS .".cart_id = '".$cart_id."'"; 
		
				
				$newrs = $db->query($sql);		   
				$db->next_record();
				
				if($db->f('transferStatus') == 2)
				{
					$itemName =	stripslashes(ucfirst($db->f('transferOrigin')))." <-> ".stripslashes(ucfirst($db->f('transferDestination')));
				}
				
				if($db->f('transferStatus') == 1)
				{
					$itemName =	stripslashes(ucfirst($db->f('transferDestination')))." -> ".stripslashes(ucfirst($db->f('transferOrigin')));
				}
				
				if($db->f('transferStatus') == 0)
				{
					$itemName =	stripslashes(ucfirst($db->f('transferOrigin')))." -> ".stripslashes(ucfirst($db->f('transferDestination')));
				}
			
		
				$db->free();	   
			}

		return ($itemName);

		}
		
		
		
		function Delete_Cart_Detail($cart_id)
		{
			global $db;
			
			$sql = "SELECT * FROM ".CART_MASTER 
				. " WHERE  ". CART_MASTER .".cart_id = '".$cart_id."'"; 
				
			$db->query($sql);
			$db->next_record();
			$confirmationNo = $db->f('confirmationNo');
			
			$sql = " DELETE FROM ".CART_DESTINATIONS
				 . " WHERE cart_id = '". $cart_id ."' ";

			$db -> query($sql);

			$sql = " DELETE FROM ".CART_MASTER
				 . " WHERE cart_id = '". $cart_id ."' ";

			$db -> query($sql);
		
			return true;	
		}


		
		function cartItemType($cart_id)
		{
			global $db;
			$itemType = 0;
			
			$sql = "SELECT * FROM ".CART_MASTER 
					. " WHERE  ". CART_MASTER .".cart_id = '".$cart_id."'"; 
			$db->query($sql);
			$db->next_record();
			$itemType = $db->f('itemType');
		
			$db->free();	   
			return ($itemType);
		}
		
		
		function updatePaymentDetail($confirmationNo)
		{
			global $db;
			global $db;
		
			$itemType = 0;
			$cart_id	=	0;
			
			$sql = "SELECT * FROM ".CART_MASTER 
					. " WHERE  ". CART_MASTER .".confirmationNo = '".$confirmationNo."'"; 
			$db->query($sql);
			
			while($db->next_record())
			{
					$itemType = $db->f('itemType');
					$cart_id	  = $db->f('cart_id');
					
					 $newsql = " UPDATE ".CART_DESTINATIONS." SET "
							 . " trStatus =   '".PD."'";
					 $newsql .= " WHERE cart_id = '".$cart_id."'";
			
					 $db->query($newsql);
			}	
			
		return true;	
		
		}
		
		
		
		function updateAllCart($chkCartId,$confirmation_no)
		{
			global $db;
		
			$itemType = 0;
			
			$transferconfirmation_no = 0;

			$sql = "SELECT * FROM ".CART_MASTER 
					. " WHERE  ". CART_MASTER .".cart_id = '".$chkCartId."'"; 

			$db->query($sql);
			$db->next_record();

			$itemType = $db->f('itemType');

			$newsql = " UPDATE ".CART_MASTER." SET "
					  . " cartStatus	 =   '". AR ."', "
					  . " confirmationNo =   '".$confirmation_no."'";
			$newsql .= " WHERE cart_id = '".$chkCartId."'";

			$db->query($newsql);

			return true;	
		}
	
	
	function arrivalDateChecking($cart_id,$itemType)
	{
	  global $db;

	   $itemType= 0;
	   $sql 	= '';
	   $newsql 	= '';
	   
	   $arrivalDate = 0;
	   $diff 		= 0;
	   $status 		= 0;
	   
	   $sql = "SELECT * FROM ".CART_MASTER 
				. " WHERE  ". CART_MASTER .".cart_id = '".$cart_id."'"; 
		$db->query($sql);
		$db->next_record();
		$itemType = $db->f('itemType');

		  $newsql = "SELECT * FROM ".CART_DESTINATIONS
					. " WHERE cart_id = '". $cart_id ."' ";
		  $db->query($newsql);
		  $db->next_record();
		  $arrivalDate = $db->f('arrivalDate');		
		  $diff = date_diff($arrivalDate,date('Y-m-d'));
			
			if($diff > 10)
				$status = 1;
			else
				$status = 0;

		 return $status;			
	}
		


function Delete_Reservation_Detail($userId,$ConfirmationNo)
{
	global $db;
	$sql = " SELECT * FROM ".CART_MASTER 
		 . " WHERE  ". CART_MASTER .".ConfirmationNo = '".$ConfirmationNo."'"; 
	$db->query($sql);
	
	while($db->next_record())
	{
		$newsql = '';
		$itemType = 0;
		$cartId = 0;
		$itemType 	= $db->f('itemType');
		$cartId 	= $db->f('cart_id');
	   	if($itemType == 2)
		{		
			$newsql = " DELETE FROM ".CART_DESTINATIONS." WHERE cart_id = '". $cartId ."' ";
			$db -> query($newsql);
		}	

		$newsql = " DELETE FROM ".CART_MASTER
			 	. " WHERE cart_id = '". $cartId ."' ";
			 
		$db -> query($newsql);
  	 }	
	
	$sql = "SELECT * FROM ".USER_MASTER 
		. " WHERE  ". USER_MASTER .".index_id = '".$userId."'"; 

	$db->query($sql);
	$db->next_record();
	$authid = $db->f('auth_id');

	$sql = "DELETE FROM ".AUTH_USER 
			. " WHERE  ". AUTH_USER .".autoId = '".$authid."'"; 

	$db->query($sql);
	

	$sql = "DELETE FROM ".USER_MASTER 
			. " WHERE  ". USER_MASTER .".index_id = '".$userId."'"; 

	$db->query($sql);

	return true;
}
		
	#====================================================================================================
	#	Function Name	:   Show_Admin_Cart_Detail
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#	Creation Date	:	03-June-2003
	#----------------------------------------------------------------------------------------------------
	function Show_Admin_Cart_Detail($confirmationNo,$SortBy,$start_record, $Page_Size, &$num_records)
	{
		global $db;
		
		$sql = "SELECT * FROM ".CART_MASTER 
			  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "					
			  . " WHERE " .CART_MASTER.".confirmationNo  = '".$confirmationNo."' "
			  . " GROUP BY ".CART_MASTER.".cart_id "
			  . " ORDER BY ".CART_MASTER.".".$SortBy;
			  
		 $db->query($sql);
		 $num_records = $db->num_rows();
		 $db->free();

		if($start_record >= $num_records && $start_record!=0)
			$start_record -= $Page_Size;
		
		$sql = "SELECT * FROM ".CART_MASTER 
			  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "					
			  . " WHERE " .CART_MASTER.".confirmationNo  = '".$confirmationNo."' "
			  . " GROUP BY ".CART_MASTER.".cart_id "
			  . " ORDER BY ".CART_MASTER.".".$SortBy
			  . " LIMIT ". $start_record . ", ". $Page_Size ;

		$rs = $db->query($sql);	  		  		
						
		$rs = $db->fetch_array();

		return ($rs);
	}
	
	#====================================================================================================
	#	Function Name	:   chageRequestStatus
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function chageRequestStatus($confirmationNo,$Status)
	{
		global $db;
		
		$sql = "SELECT * FROM ".CART_MASTER 
				. " WHERE  ". CART_MASTER .".ConfirmationNo = '".$confirmationNo."'"; 
		$db->query($sql);
		
		while($db->next_record())
		{
			$newsql = '';
			$itemType = 0;
			$cartId = 0;
			$itemType = $db->f('itemType');
			$cartId	= $db->f('cartId');
			
			$newsql = " UPDATE ".CART_DESTINATIONS." SET "
					. " trStatus =   '".$Status."'";
			$newsql .= " WHERE ". CART_DESTINATIONS .".cart_id = '".$cartId."'"; 
			$db->query($newsql);
		}	
		
		 $sql = " UPDATE ".CART_MASTER." SET "
				 . " cartStatus =   '".$Status."'";
		 $sql .= " WHERE ". CART_MASTER .".ConfirmationNo = '".$confirmationNo."'"; 

		 $db->query($sql);
	
	 return true;
	}	
	
	#====================================================================================================
	#	Function Name	:   Show_UserCart
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Show_UserCart($chkCartId)
	{
		global $db;
			
		$sql = "SELECT * FROM ".CART_MASTER 
			  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "					
			  . " WHERE " .CART_MASTER.".user_id  = '".$_SESSION['User_Id']."' AND ".CART_MASTER.".cart_id IN (".$chkCartId.")"
			  . " GROUP BY ".CART_MASTER.".cart_id ";
			
		$rs = $db->query($sql);	  		  		
		$rs = $db->fetch_array();

		return($rs);
	}	

	#====================================================================================================
	#	Function Name	:   Show_UserCart_Detail
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Show_UserCart_Detail($confirmationNumber)
	{
		global $db;
		$sql = "SELECT * FROM ".CART_MASTER 
			  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "			
			  . " WHERE " .CART_MASTER.".confirmationNo  = '".$confirmationNumber."' "
			  . " GROUP BY ".CART_MASTER.".cart_id "			  
			  . " ORDER BY ". CART_MASTER .".itemType ";
		
			
		$rs = $db->query($sql);	  		  		
		$rs = $db->fetch_array();

		return($rs);
	}
	#====================================================================================================
	#	Function Name	:   Show_UserRes_Detail
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Show_UserRes_Detail($cart_id)
	{
		global $db;
		
		$sql = "SELECT * FROM ".CART_DESTINATIONS 
			  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_DESTINATIONS. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "			
			  . " WHERE " .CART_DESTINATIONS.".cart_id  = '".$cart_id."' "
			  . " GROUP BY ".CART_DESTINATIONS.".dest_id "			  
			  . " ORDER BY ". CART_DESTINATIONS .".dest_id ";

		$rs = $db->query($sql);	  		  		
		$rs = $db->fetch_array();

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   Show_CartDest_Detail
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Show_CartDest_Detail($SortBy,	$start_record,	$Page_Size)
	{
		global $db;
		
		$sql = "SELECT * FROM ".CART_MASTER 
			  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "						  
			  . " WHERE ".CART_MASTER.".user_Id  = '".$_SESSION['User_Id']."' AND ".CART_MASTER.".cartStatus ='".NR."'"
			  . " GROUP BY ".CART_MASTER.".cart_id "
			  . " ORDER BY ".CART_MASTER.".".$SortBy
			  . " LIMIT ". $start_record . ", ". $Page_Size ;
	
		$rs = $db->query($sql);	  		  		

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   Show_CartUser
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Show_CartUser()
	{
		global $db;
		
		$sql = "SELECT * FROM ".CART_MASTER 
			  . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "						  
			  . " WHERE ".CART_MASTER.".user_Id  = '".$_SESSION['User_Id']."' "
			  . " GROUP BY ".CART_MASTER.".cart_id ";

		$rs = $db->query($sql);	  		  		

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   Show_ConfrmCart
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Show_ConfrmCart($confirmationNumber)
	{
		global $db;
		
		$sql = "SELECT * FROM ".CART_MASTER 
			  . " WHERE " .CART_MASTER.".confirmationNo  = '".$confirmationNumber."' ";
	
		$rs = $db->query($sql);	  		  		

		return($rs);
	}
	
	#====================================================================================================
	#	Function Name	:   Show_ConfrmCartDetail
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Show_ConfrmCartDetail($carttranId)
	{
		global $db;
		
		$sql = "SELECT * FROM ".CART_DESTINATIONS 
			. " LEFT JOIN ". CART_MASTER. " ON ". CART_DESTINATIONS. ".cart_id = ". CART_MASTER. ".cart_id "	
			. " LEFT JOIN ". USER_MASTER. " ON ". CART_MASTER. ".user_id = ". USERMASTER. ".user_id "	
			. " WHERE " .CART_DESTINATIONS.".carttranId = '". $carttranId ."'" ;
	
		$rs = $db->query($sql);	  		  		

		return($rs);
	}
	
	#====================================================================================================
	#	Function Name	:   Delete_Res_Detail
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Delete_Res_Detail($carttranId,$cartId)
	{
		global $db;
	
		$sql = "DELETE FROM ".CART_DESTINATIONS . " WHERE " .CART_DESTINATIONS.".carttranId = '". $carttranId ."'" ;
		$rs = $db->query($sql);
		
		$sql = "DELETE FROM ".CART_MASTER . " WHERE " .CART_MASTER.".cart_id = '". $cartId ."'" ;
		$rs = $db->query($sql);
	
		return true;
	}	

	#====================================================================================================
	#	Function Name	:   Show_CartDetail
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Show_CartDetail($cartId)
	{
		global $db;
		
	   $sql = " SELECT * FROM ".CART_MASTER
			. " LEFT JOIN ". CART_DESTINATIONS. " ON ". CART_MASTER. ".dest_id = ". CART_DESTINATIONS. ".dest_id "
			. " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_MASTER. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "			   
			. " WHERE  ". CART_MASTER .".cart_id = '".$cartId."'"
			. " GROUP BY ".CART_MASTER.".cart_id "; 

		$rs = $db->query($sql);	  		  		

		return($rs);
	}
	

	#====================================================================================================
	#	Function Name	:   Update_Cart
	#	Purpose			:	It shows user's Cart data from database
	#	Return			:	returns the recordset for the specified partner_id
	#----------------------------------------------------------------------------------------------------
	function Update_Cart($chkCartId,$confirmation_no)
	{
		global $db;

		$itemType = 0;
		$cartconfirmation_no = 0;

		$sql = "SELECT * FROM ".CART_MASTER 
				. " WHERE  ". CART_MASTER .".cart_id = '".$chkCartId."'"; 

		$db->query($sql);
		$db->next_record();

		$itemType = $db->f('itemType');


		#====================== Update RequestNo =========================#
		$cartconfirmation_no = $confirmation_no."R";
		
		$newsql = " UPDATE ".CART_MASTER." SET "
				. " requestNo		= '".$cartconfirmation_no."'";
		$newsql .= " WHERE cart_id	= '".$chkCartId."'";
		
		$db->query($newsql);
		#=================================================================#

		#========== Update CartStatus & ConfirmationNo====================#
		$newsql = " UPDATE ".CART_MASTER." SET "
				  . " cartStatus	 =   '". PR ."', "
				  . " confirmationNo =   '".$confirmation_no."'";
		$newsql .= " WHERE cart_id = '".$chkCartId."'";

		$db->query($newsql);
		#=================================================================#
		
		return true;	
	}


	#====================================================================================================
	#	Function Name	:   UpdateCartAmnt
	#	Purpose			:	Update the office information
	#	Return			:	return update status
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function UpdateCartAmnt($cart_id,	$totalPrice,	$cartDateTime)
	{
		global $db;
		
		$sql = " UPDATE ".CART_MASTER." SET "
			 . " totalCharge	=   '". $totalPrice."',  "
			 . " cartDateTime	=   '". $cartDateTime ."' ";
		$sql .= " WHERE cart_id	= 	'". $cart_id ."' ";

		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}
	
	#====================================================================================================
	#	Function Name	:   UpdateCartDest
	#	Purpose			:	Update the office information
	#	Return			:	return update status
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function UpdateCartDest($dest_id,	$cart_id)
	{
		global $db;
		
		$sql = " UPDATE ".CART_MASTER." SET "
			 . " dest_id 		= '". $dest_id."' ";
		$sql .=" WHERE cart_id	= '". $cart_id ."' ";

		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}
	


	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}
}
		
?>