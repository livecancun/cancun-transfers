<?php
#====================================================================================================
# File Name : TripRateData.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# include custom class
require_once(dirname(__FILE__) . '/CustomClass.php');

class TripRateData extends CustomClass
{
	#====================================================================================================
	#	Function Name	:   RateData
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Adnan Sarela
	#	Creation Date	:	04-May-2005
	#----------------------------------------------------------------------------------------------------
    function TripRateData()
    {
		global $asset;
		
		# Initialize all default data
		
		# Table Name
//		$this->Data['TableName']			=	$config['Table_Prefix']. 'destination_master';
		$this->Data['TableName']			=	$config['Table_Prefix']. 'passenger_master';
		$this->Data['TableGrid']			=	$config['Table_Prefix']. 'rate_master';
		
		# Module Title
		$this->Data['L_Module']				=	'Trip Rate';

		# Help Text
		$this->Data['H_Manage']				=	'Manage Destinations\' Trip Rates, add/edit/delete one or more prices.';	
		$this->Data['H_AddEdit']			=	'Update the type information and click <b>Save</b> to save the changes.
												Click <b>Cancel</b> to discard the changes.';
		# Command list
		$this->Data['C_CommandList']		=	array(A_ADD, A_EDIT, A_DELETE);

		# Primary field info
		$this->Data['F_PrimaryKey']			=	'passenger_id';
		$this->Data['F_PrimaryField']		=	'passenger_starting_range';


		# Intialize parent class
		parent::CustomClass();
	}
}
?>