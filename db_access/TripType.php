<?php
#====================================================================================================
# File Name : TripType.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# Page Status
define('TRIPTYPE_VISIBLE',	'Visible');
define('TRIPTYPE_HIDDEN',	'Hidden');

# Page Type
$arrPageType	=	array(	'SimplePage'	=>	'Simple Page',
							'InternalLink'	=>	'Internal Link',
					);
# Page Class
class TripType
{
   	#====================================================================================================
	#	Function Name	:   Page
	#	Purpose			:	Constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function TripType()
    {
		// Do nothing
	}

	#====================================================================================================
	#	Function Name	:   ViewAll
	#	Purpose			:	Provide list of triptype information
	#	Return			:	return recordset with triptype info
	#----------------------------------------------------------------------------------------------------
    function ViewAll($triptype_status='',$lang='')
    {
		global $db;
		$lang = $_SESSION['lng'];
			 
		$sql	=	"SELECT * FROM " . TRIPTYPE_MASTER
				.	" LEFT JOIN " . TRIPTYPE_DETAILS . " ON  triptype_id =triptype_master_id "
				.	" WHERE triptype_langcode = '" .$lang. "'"
				. ($triptype_status ? " AND triptype_status = '". $triptype_status."'" : '');
			 
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_array());
	}
	
	/*
	#====================================================================================================
	#	Function Name	:   ViewAllLanguages
	#	Purpose			:	Provide list of language information
	#	Return			:	return recordset with language info
	#----------------------------------------------------------------------------------------------------
    function ViewAllLanguages()
    {
		global $db;

		$sql = " SELECT * FROM ".LANGUAGE_MASTER
			 . " ORDER BY languages_id ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object());
	}
*/

	#====================================================================================================
	#	Function Name	:   getPage
	#	Purpose			:	get the triptype information
	#	Return			:	return recordset with triptype info
	#----------------------------------------------------------------------------------------------------
    function getTripType($triptype_id )
    {
		global $db;
				
		$sql = " SELECT * FROM ".TRIPTYPE_MASTER
				. " WHERE ".TRIPTYPE_MASTER.". triptype_id  =  '". $triptype_id  ."'";
				
		//test- piyush
		$db->query($sql);
		$rs = $db->fetch_array();

		$triptype_info = array();
		foreach($rs[0] as $key => $val)
		{
			$triptype_info[$key] = $val;
		}
		
		$sql	=	"SELECT * FROM " . TRIPTYPE_DETAILS
				.	" WHERE triptype_master_id   = " . $triptype_id 
				.	" ORDER BY triptype_langcode";
		
		$db->query($sql);		
		$rs = $db->fetch_array();
		$triptype_title = array();  
		
		foreach($rs as $key => $row)
		{
			$triptype_title[]			= $row['triptype_title'];
		}		
		
		$triptype_info['triptype_title']		 = $triptype_title;
		return($triptype_info);

		//test-end
	}

	#====================================================================================================
	#	Function Name	:   Insert
	#	Purpose			:	Insert the new triptype
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function Insert($triptype_title)
	{
		global $db,$LangInfo;
		
		# Save Page info
		$sql = 	"INSERT INTO ". TRIPTYPE_MASTER. " (triptype_status)"
			.	" VALUES ( "
			.	" '	Visible' "
			.	" )"; 

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);
		$triptype_id =$db->sql_inserted_id();
		//echo $triptype_id ;
		for($i=0; $i<count($LangInfo); $i++)
		{
			$sql = 	"INSERT INTO ". TRIPTYPE_DETAILS. " (triptype_master_id  ,	triptype_langcode ,		 "
				.	" 								triptype_title	"
				.	"								)"
				.	" VALUES ( "
				. 	" '".	$triptype_id 			.		"', " 				
				. 	" '".	$LangInfo[$i]['code'].		"', " 				
				. 	" '".	$triptype_title[$i].			"' " 
				.	" )"; 
				
				$db->query($sql);
		}

		# Set the triptype order as last
		$sql = " UPDATE ".TRIPTYPE_MASTER
			 . " SET "
			 . " 	triptype_order 			=  '". $triptype_id . "' "
			 . " WHERE  triptype_id  =  '". $triptype_id . "'";

		# Execute Query
		$db->query($sql);
		
		return $db->sql_inserted_id();
	}

	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	get the triptype information
	#	Return			:	return recordset with triptype info
	#----------------------------------------------------------------------------------------------------
    function Update($triptype_id ,$triptype_title)
    {
		//echo "in update";
		global $db, $LangInfo;
				
		/*$sql = " UPDATE ".TRIPTYPE_MASTER
			 . " SET "
			 . " 	triptype_parent_id 		=  '". $triptype_parent_id ."', "
			 . " 	triptype_url 			=  '". $triptype_url ."', "			 
			 . " WHERE  triptype_id  =  '". $triptype_id  ."'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);*/
		for( $i=0; $i<count($LangInfo); $i++)
		{
			//echo $i."<br>";
			$sql = 	"REPLACE INTO ". TRIPTYPE_DETAILS
				. 	"(triptype_master_id ,triptype_langcode ,triptype_title)"
				.	" VALUES ( "
				. 	" ".	$triptype_id 	.", " 
				. 	" '".	$LangInfo[$i]['code']	."', " 				
				. 	" '".	$triptype_title[$i]	."' " //test
				.	" )";
			
		//echo $sql."<br>";
			$db->query($sql);
			//print $sql."<br><br>";
		}
		//die;
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
		return;
		
	}

	#====================================================================================================
	#	Function Name	:   Delete
	#	Purpose			:	get the triptype information
	#	Return			:	return recordset with triptype info
	#----------------------------------------------------------------------------------------------------
    function Delete($triptype_id )
    {
		global $db;
				
		$sql = " DELETE FROM ".TRIPTYPE_MASTER
			 . " WHERE  triptype_id IN (".$triptype_id.")";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
			
		$sql = " DELETE FROM ".TRIPTYPE_DETAILS
			 . " WHERE  triptype_master_id IN (".$triptype_id.")";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);	

		return ($db->query($sql));
	}
	
		#====================================================================================================
	#	Function Name	:   Sort
	#	Purpose			:	Update the triptype order
	#	Return			:	update status
	#----------------------------------------------------------------------------------------------------
    function ToggleStatus($triptype_id , $triptype_status)
    {
		global $db;
		
		$sql = " UPDATE ".TRIPTYPE_MASTER
			 . " SET "
			 . " 	triptype_status =  '". $triptype_status. "' "
			 . " WHERE  triptype_id  	=  '". $triptype_id . "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		return ($db->query($sql));
	}

	#====================================================================================================
	#	Function Name	:   getKeyValueArray
	#	Purpose			:	
	#	Return			:	key => value
	#----------------------------------------------------------------------------------------------------
	function getKeyValueArray()
	{
		global $db;
		
		$sql	=	" SELECT  triptype_id , triptype_title FROM " . TRIPTYPE_MASTER
				.	" LEFT JOIN " . TRIPTYPE_DETAILS . " ON  triptype_id =triptype_master_id "
				.	" WHERE dest_langcode  = 'en'"
			//	.	" AND triptype_parent_id = '0'"				
				. 	" ORDER BY triptype_order ";
		
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		$arr	= array();
		foreach ($db->fetch_array() as $triptype)
		{
			$arr[$triptype['triptype_id']] = $triptype['triptype_title'];
		}
		
		return ($arr);
	}

	#====================================================================================================
	#	Function Name	:   getKeyValueArray
	#	Purpose			:	
	#	Return			:	key => value
	#----------------------------------------------------------------------------------------------------
	function updateMeta($site_title, $meta_title, $meta_keyword, $meta_desc)
	{
		global $db;
		
		$sql	= " UPDATE ". TRIPTYPE_MASTER . " SET"
				. " triptype_brower_title 	=  '". $triptype_site_title ."', "
				. " triptype_metatitle 		=  '". $meta_title ."', "			 
			 	. " triptype_metakeyword 	=  '". meta_keyword ."'"
			 	. " triptype_metadesc		=  '". meta_desc ."'";
				
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
							
		$db->query($sql);
								
		return($db->affected_rows());
	}
	#====================================================================================================
	#	Function Name	:   getAll
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getAll($addParameters='')
    {
		global $db;

		$sql	=	" SELECT  triptype_id , triptype_title FROM " . TRIPTYPE_MASTER
				.	" LEFT JOIN " . TRIPTYPE_DETAILS . " ON  triptype_id = triptype_master_id "
				.	" WHERE triptype_langcode  = 'en'"
				. ($addParameters != ''? $addParameters :'');
				
/*		$sql	=	"SELECT * "
				.	" FROM ". TRIPTYPE_MASTER
				. " WHERE 1 "
				. ($addParameters != ''? $addParameters :'');
*/
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		
		return ($db->fetch_array());
	}
	
	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}	
}
?>