<?php
#====================================================================================================
#	File Name	:	Office.php
#----------------------------------------------------------------------------------------------------
#
#	Purpose			: 	Provide the Schedule's information
#
#	Author			: 	Dinesh Sailor <dinesh@dotinfosys.com>
#	Creation Date	: 	22-Oct-2004
#
#====================================================================================================

class Destination
{
	/* public: result array and current row number */
	var $Record   		= array();

	var $Debug         	= false;     ## Set to true for debugging messages.

#====================================================================================================
#	Function Name	:   Schedule
#----------------------------------------------------------------------------------------------------
function Destination()
{

}

	#====================================================================================================
	#	Function Name	:   ViewallDestinationList
	#	Purpose			:	Lists all sport according to display order
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
    function ViewallDestinationsList()
    {
		global $db,$db1;
		$lang = $_SESSION['lng'];
		
		$sql	=	"SELECT * "
				. " FROM ". DESTINATION_MASTER ." AS DM "
				. " LEFT JOIN " . DESTINATION_LANG . " AS DL ON  DM.dest_id =	DL.dest_master_id "
				. " WHERE DL.dest_langcode = '" .$lang. "' AND DM.dest_status=1 ORDER BY DM.dest_order ";
//				. ($addParameters != ''? $addParameters :'');

//		$sql = " SELECT * FROM ".SPORTS_MASTER." as SM "
//			 . " ORDER BY SM.sport_disp_order"; 
	
		# Show debug info
		if($Debug)
			$this->__debugMessage($sql); 
		
		$rs = $db->query($sql);
	
/*		$arr= array();
		foreach($db->fetch_array()as $SportList)
		{
			$arr[$SportList['sport_id']] = $SportList['sport_name'];
		}
*/		//print_r($arr); die;
		return ($db->fetch_array());
	}
	#====================================================================================================
	#	Function Name	:   setSportDisOrder
	#----------------------------------------------------------------------------------------------------
	function setDestDisOrder($dest_id,$dest_order)
    {
		global $db;
	
		$sql = " UPDATE ".DESTINATION_MASTER
			 . " SET "
			 . " dest_order  			='".$dest_order ."'"
			 . " WHERE dest_id 			='".$dest_id."'";

		return ($db->query($sql));
	}
	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	22-Oct-2004
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	22-Oct-2004
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}
	
	/*function Destroy()
	{
	    session_unregister($_SESSION['Soccer_County_Id']);
	    session_unregister($_SESSION['Soccer_Level_Id']);
	    session_unregister($_SESSION['Soccer_Gender_Id']);
	    @session_unset();
	    @session_destroy();
        return true;
	}*/
	
}
?>