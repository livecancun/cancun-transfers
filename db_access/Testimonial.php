<?php
#====================================================================================================
# File Name : Testimonial.php 
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contains all application configuration details
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#====================================================================================================

class Testimonial
{

	/* public: filter parameters */
	var $person_id		=	'';
	var $person_status	=	'';

	/* public: Pagination  */
	var $page_size		=	'';
	var $start_record	=	'';
	var $total_record	=	'';

	/* public: result array and current row number */
	var $Record   		= array();

	var $Debug         	= false;     ## Set to true for debugging messages.

	#====================================================================================================
	#	Function Name	:   Testimonail
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function Testimonail()
    {
		# Do nothing
	}

	#====================================================================================================
	#	Function Name	:   ViewAll
	#	Purpose			:	Provide list of office information
	#	Return			:	return recordset with office info
	#----------------------------------------------------------------------------------------------------
    function ViewAll($personid='')
    {
		global $db;

		# Required only signle office info
		if($personid != '')
		{
			$sql	=	"SELECT * "
					.	" FROM ". TESTIMONIAL_MASTER
					.	" WHERE person_id = '". $personid. "' ";
			$rs = $db->query($sql);
			return ($db->next_record());
		}
		else
		{
			# Get the total office count
			$sql	=	"SELECT count(*) as cnt "
					.	" FROM " . TESTIMONIAL_MASTER
					.	" WHERE 1 "
					.	(empty($this->person_id)	? "" : " AND person_id IN (". $this->person_id. ")")
					.	(empty($this->person_status) ? "" : " AND person_status = '". $this->person_status. "'");

			# Show debug info
			if($Debug)
				$this->__debugMessage($sql);

			$db->query($sql);
			$db->next_record();
			$this->total_record = $db->f("cnt") ;
			$db->free();

			# Reset the start record if required
			if($this->start_record >= $this->total_record)
				$this->start_record = 0;
			
			# Get the required records
			$sql	=	"SELECT * "
					.	" FROM " . TESTIMONIAL_MASTER
					.	" WHERE 1 "
					.	(empty($this->person_id)	? "" : " AND person_id IN (". $this->person_id. ")")
					.	(empty($this->person_status) ? "" : " AND person_status = '". $this->person_status. "'")
					.	(empty($this->page_size) ? "" : " LIMIT " . $this->start_record . ", ". $this->page_size);
//					.	" LIMIT " . $this->start_record . ", ". $this->page_size;
			# Show debug info
			if($Debug)
				$this->__debugMessage($sql);
			
			$rs = $db->query($sql);
			return ($db->fetch_object());
		}	
	}

	function ViewByID()
	{
		global $db;

		$sql	=	"SELECT * "
				.	" FROM " . TESTIMONIAL_MASTER
				.	" WHERE 1 "
				.	(empty($this->person_id)	? "" : " AND person_id IN (". $this->person_id. ")")
				.	(empty($this->person_status) ? "" : " AND person_status = '". $this->person_status. "'");

		return ($db->query($sql));
	}
	
	#====================================================================================================
	#	Function Name	:   Insert
	#	Purpose			:	Insert the new office information
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function Insert($person_name,		$person_company,	$person_address,	$person_city,
					$person_state,		$person_country,		$person_zip,		$person_phone,		$person_fax,
					$person_tollfree,	$person_email,		$person_website,	$person_pic,
					$person_desc, 		$person_comment,	$person_status)
	{
		global $db;

		$sql = 	"INSERT INTO ". TESTIMONIAL_MASTER. " ( person_name,		person_company,	person_address,	person_city, "
			.	"										person_state,		person_country,	person_zip,		person_phone,	person_fax, "
			.	"										person_tollfree,	person_email,	person_website,	person_pic, "
			.	"										person_desc,		person_comment,	person_status) "
			.	" VALUES ( "
			.	" '". $person_name 			."' , "
			.	" '". $person_company 		."' , "
			.	" '". $person_address 		."' , "
			.	" '". $person_city 			."' , "
			.	" '". $person_state 		."' , "
			.	" '". $person_country 		."' , "			
			.	" '". $person_zip 			."' , "
			.	" '". $person_phone 		."' , "
			.	" '". $person_fax 			."' , "
			.	" '". $person_tollfree 		."' , "
			.	" '". $person_email			."' , "
			.	" '". $person_website 		."' , "
			.	" '". $person_pic 			."' , "
			.	" '". $person_desc 			."' , "
			.	" '". $person_comment 		."' , "
			. 	" '". $person_status 		."' ) " ;

		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);

		return $db->sql_inserted_id();
	}

	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	Update the office information
	#	Return			:	return update status
	#----------------------------------------------------------------------------------------------------
	function Update($person_id,		$person_name,		$person_company,	$person_address,
					$person_city,	$person_state,		$person_country,	$person_zip,		$person_phone,
					$person_fax,	$person_tollfree,	$person_email,		$person_website,
					$person_pic, 	$person_desc,		$person_comment,	$person_status)
	{
		global $db;

		$sql = 	"UPDATE ". TESTIMONIAL_MASTER
			.	" SET "
			.	"	person_name		=	'". $person_name	."', "
			.	"	person_company	=	'". $person_company	."', "
			.	"	person_address	=	'". $person_address	."', "
			.	"	person_city		=	'". $person_city	."', "
			.	"	person_state	=	'". $person_state	."', "
			.	"	person_country	=	'". $person_country	."', "			
			.	"	person_zip		=	'". $person_zip		."', "
			.	"	person_phone	=	'". $person_phone	."', "
			.	"	person_fax		=	'". $person_fax		."', "
			.	"	person_tollfree	=	'". $person_tollfree."', "
			.	"	person_email	=	'". $person_email	."', "
			.	"	person_website	=	'". $person_website	."', "
			.	"	person_pic		=	'". $person_pic		."', "
			.	"	person_desc		=	'". $person_desc	."', "
			.	"	person_comment	=	'". $person_comment	."', "
			.	"	person_status	=	'". $person_status	."' "
			.	" WHERE person_id = '". $person_id. "' ";

		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}

	#====================================================================================================
	#	Function Name	:   Delete
	#	Purpose			:	Delete the office information
	#	Return			:	return delete status
	#----------------------------------------------------------------------------------------------------
	function Delete()
	{
		global $db;
		
		if(!$this->person_id)
			return false;

		$sql = 	"DELETE FROM ". TESTIMONIAL_MASTER
			.	" WHERE person_id IN (". $this->person_id. ") ";

		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}

	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}
}
?>