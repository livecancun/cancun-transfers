<?php
#====================================================================================================
# File Name : Discount.php 
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contains all application configuration details
# Author	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# Page Class
class Discount
{
   	#====================================================================================================
	#	Function Name	:   Page
	#	Purpose			:	Constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function Discount()
    {
		// Do nothing
	}

	#====================================================================================================
	#	Function Name	:   Show_Pages
	#	Purpose			:	It shows report's data from database
	#	Parameters		: 	doc_id - id of the rpoet
	#						start_record - starting limit, per_page - no of records per page,
	#						num_records - no of records
	#	Return			:	returns the recordset for the specified report
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	16-Jun-2005
	#----------------------------------------------------------------------------------------------------
	function Show_Discount($discount_id='', $start_record, $per_page, &$num_records)
	{
		global $db;
	
		if($discount_id)
		{
			$sql = " SELECT * FROM ".DESTINATION_DISCOUNT
				 . " WHERE ".DESTINATION_DISCOUNT.".discount_id =  '". $discount_id ."'";
			$rs = $db->query($sql);
		}
		else
		{
			$sql  = " SELECT count(*) as cnt FROM " . DESTINATION_DISCOUNT;
			$db->query($sql);
			$db->next_record();
			$num_records = $db->f("cnt") ;
			$db->free();
	
			if($start_record >= $num_records && $start_record!=0)
				$start_record -= $per_page;
	
			$sql = " SELECT * FROM ".DESTINATION_DISCOUNT
				 . " order by discount_id LIMIT ". $start_record . ", ". $per_page;
			$rs = $db->query($sql);
		}

		$rs = $db->fetch_array();
		return ($rs);
	}
	
	#====================================================================================================
	#	Function Name	:   getAllDisco
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function getAllDisco($addParameters='')
    {
		global $db;

			$sql = " SELECT * FROM ".DESTINATION_DISCOUNT
				. " WHERE 1 "
				. ($addParameters != ''? $addParameters :'');

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$rs = $db->query($sql);
		
		return ($rs);
	}
	
	#====================================================================================================
	#	Function Name	:   Show_Discnt
	#	Purpose			:	It shows report's data from database
	#	Parameters		: 	doc_id - id of the rpoet
	#						start_record - starting limit, per_page - no of records per page,
	#						num_records - no of records
	#	Return			:	returns the recordset for the specified report
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	16-Jun-2005
	#----------------------------------------------------------------------------------------------------
	function Show_Discnt($discount_id)
	{
		global $db;
	
		$sql = " SELECT * FROM ".DESTINATION_DISCOUNT
			 . " WHERE ".DESTINATION_DISCOUNT.".discount_id =  '". $discount_id ."'";
		$rs = $db->query($sql);

		return ($rs);
	}

	#====================================================================================================
	#	Function Name	:   Add_Hotel_Service
	#	Purpose			:	Update the web site config details
	#	Parameters		:	$config_name		:	Name of config parameter
	#						$config_value		:	Value of config
	#	Return			:	Return the status of updation (No of rows affected)
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	16-Jun-2005
	#----------------------------------------------------------------------------------------------------
	function Add_Discount($discount_title,$discount_title_sp,$discount_rate)
	{
		global $db;
	
		$sql = " INSERT INTO ".DESTINATION_DISCOUNT. " (discount_title,discount_title_sp,discount_rate) "
			  . " VALUES ( "
			 . " '". addslashes($discount_title) ."' , "
			 . " '". addslashes($discount_title_sp) ."' , "			 
			 . " '". ($discount_rate) ."'  ) " ;
	
	
		$db->query($sql);
	
		return ($db->sql_inserted_id());
		
	}
	#====================================================================================================
	#	Function Name	:   Update_Hotel_Service
	#	Purpose			:	Update the web site config details
	#	Parameters		:	$config_name		:	Name of config parameter
	#						$config_value		:	Value of config
	#	Return			:	Return the status of updation (No of rows affected)
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	16-Jun-2005
	#----------------------------------------------------------------------------------------------------
	function Update_Discount($discount_id, $discount_title,	$discount_title_sp, $discount_rate)
	{
		global $db;
	
		$sql = " UPDATE " .DESTINATION_DISCOUNT
			 . " SET "
			 . " discount_title		=   '". addslashes($discount_title) ."', "
			 . " discount_title_sp	=   '". addslashes($discount_title_sp) ."', "			 
			 . " discount_rate  	=   '". $discount_rate ."' "
			 . " WHERE discount_id	= 	'". $discount_id ."' ";
		$db->query($sql);
	
		return ($db->affected_rows());
	}
	#====================================================================================================
	#	Function Name	:   Delete_Car_Discount
	#	Purpose			:	Update the web site config details
	#	Parameters		:	$config_name		:	Name of config parameter
	#						$config_value		:	Value of config
	#	Return			:	Return the status of updation (No of rows affected)
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	16-Jun-2005
	#----------------------------------------------------------------------------------------------------
	function Delete_Discount($discount_id)
	{
		global $db;
	
		$sql = " DELETE FROM ".DESTINATION_DISCOUNT
			 . " WHERE discount_id 	= '". $discount_id. "' ";
			 
		$db->query($sql);
	
		return ($db->affected_rows());
	}

}
?>