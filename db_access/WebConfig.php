<?php
#====================================================================================================
# File Name : WebConfig.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 
# Website Config Constant
define('WC_COMPANY_TITLE',			'company_title');
define('WC_SITE_TITLE',				'site_title');
define('WC_META_TITLE',				'meta_title');
define('WC_META_KEYWORD',			'meta_keyword');
define('WC_META_DESCRIPTION',		'meta_description');
define('WC_COPYRIGHT_TEXT',			'copyright_text');
define('WC_POWEREDBY',				'powered_by');
define('WC_PAGESIZE',				'page_size');

# Lang
define('WC_LANG',	'default_lang');
define('WC_SUPPORT_EMAIL',		    'support_email');
define('WC_PAYPAL_EMAIL',			'paypal_email');

class WebConfig
{
   	#====================================================================================================
	#	Function Name	:   WebConfig
	#	Purpose			:	Constructor
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	03-Jan-2005
	#----------------------------------------------------------------------------------------------------
    function WebConfig()
    {
		global $db;
		global $config;

		$sql  = "SELECT * "
				. " FROM " . WEBSITE_CONFIG;

		$db->query($sql);
	    while($db->next_record())
	    {
	        $config[$db->f('config_name')] = stripslashes($db->f('config_value'));
	    }
	}

	#====================================================================================================
	#	Function Name		:	Get
	#	Purpose				:	Get the web site config details
	#	Parameters			:	none
	#	Return				:	Return the record set of config
	#	Author				:	Dinesh Sailor
	#	Creation Date		:	03-Jan-2005
	#----------------------------------------------------------------------------------------------------
	function Get($config_name)
	{
		global $config;
		return $config[$config_name];
	}

	#====================================================================================================
	#	Function Name		:	Set
	#	Purpose				:	Set the web site config details
	#	Parameters			:	none
	#	Return				:	Return the record set of config
	#	Author				:	Dinesh Sailor
	#	Creation Date		:	03-Jan-2005
	#----------------------------------------------------------------------------------------------------
	function Set($config_name, $config_value)
	{
		global $config;
		$config[$config_name] = stripslashes($config_value);

        return $this->_Update($config_name, $config_value);
	}

	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	it update website configuration
	#	Return			:	returns update status
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	03-Jan-2005
	#----------------------------------------------------------------------------------------------------
	function _Update($config_name, $config_value)
	{
		global $db;

		$sql = " UPDATE ".WEBSITE_CONFIG
			 . " SET  config_value	= '". $config_value. "' "
			 . " WHERE config_name 	= '". $config_name. "' ";

		$db->query($sql);

		return ($db->affected_rows());
	}
}
?>