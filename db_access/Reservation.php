<?php
#====================================================================================================
# File Name : Reservation.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 
class Reservation
{

	/* public: filter parameters */
	var $dest_id		=	'';
	var $cart_id		=	'';	
	
	/* public: Pagination  */
	var $page_size		=	'';
	var $start_record	=	'';
	var $total_record	=	'';

	/* public: result array and current row number */
	var $Record   		= array();

	var $Debug         	= false;     ## Set to true for debugging messages.


	#====================================================================================================
	#	Function Name	:   Office
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function Reservation()
    {
		# Do nothing
	}

	#====================================================================================================
	#	Function Name	:   InsertDest 
	#	Purpose			:	Insert the new office information
	#	Return			:	return insert status
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function InsertDest($dest_id,			$cart_id,			$total_person,			$hotelName,			
						$arrivalDate,		$arrivalTime,		$arrivalAirline,		$arrivalFlight,		
						$departureDestId,	$departureTotalPerson,$departureHotelName,  $pickupTime,	$departureDate,		
						$departureTime,		$departureAirline,	$departureFlight,		$trip_status,			
						$currentDate,		$totalAmount)
	{
		global $db;


		$sql = "INSERT INTO ".CART_DESTINATIONS." ( dest_id,		cart_id,				total_person,		hotelName,		
													arrivalDate,	arrivalTime,			arrivalAirline,		arrivalFlight,	
													departureDestId,departureTotalPerson,	departureHotelName,	pickupTime, departureDate,		
													departureTime,	departureAirline,		departureFlight,	trip_status,	
													currentDate,	totalAmount) "
			 . " VALUES ( "
			 . " '". $dest_id ."' , "
			 . " '". $cart_id ."' , "
			 . " '". $total_person ."' , "
			 . " '". addslashes($hotelName) ."' , "
			 . " '". $arrivalDate ."' , "
			 . " '". $arrivalTime ."' , "
			 . " '". addslashes($arrivalAirline) ."' , "
			 . " '". addslashes($arrivalFlight) ."' , "
			 . " '". $departureDestId ."' , "
			 . " '". $departureTotalPerson ."' , "			 
			 . " '". addslashes($departureHotelName) ."' , "
			 . " '". $pickupTime ."' , "
			 . " '". $departureDate ."' , "
			 . " '". $departureTime ."' , "
			 . " '". addslashes($departureAirline) ."' , "
			 . " '". addslashes($departureFlight) ."' , "
			 . " '". $trip_status ."', "
			 . " '". $currentDate ."',"
			 . " '". $totalAmount ."'  ) ";
		
		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);

		return $db->sql_inserted_id();
	}

	#====================================================================================================
	#	Function Name	:   UpdateDest
	#	Purpose			:	Update the office information
	#	Return			:	return update status
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function UpdateDest($dest_id, 		$no_person,			$hotelName,			$arrivalDate,	
						$arrivalTime, 	$arrivalAirline,	$arrivalFlight,		$departureDestId,	$departureTotalPerson,	
						$departureHotelName,$departureDate,	$departureTime,		$departureAirline,	$departureFlight,	
						$trip_status,	$currentDate,		$totalPrice,		$cart_id)
	{
		global $db;
		
		$sql = 	"UPDATE ". CART_DESTINATIONS
			.	" SET "
			.	" dest_id				= '". $dest_id. "', "
			.	" total_person			= '". $no_person. "', "
			.	" hotelName				= '". $hotelName. "', "
			.	" arrivalDate			= '". $arrivalDate. "', "
			.	" arrivalTime			= '". $arrivalTime. "', "
			.	" arrivalAirline		= '". $arrivalAirline. "', "
			.	" arrivalFlight			= '". $arrivalFlight. "', "
			.	" departureDestId		= '". $departureDestId. "', "
			.	" departureTotalPerson	= '". $departureTotalPerson. "', "
			.	" departureHotelName	= '". $departureHotelName. "', "
			.	" departureDate			= '". $departureDate. "', "
			.	" departureTime			= '". $departureTime. "', "
			.	" departureAirline		= '". $departureAirline. "', "
			.	" departureFlight		= '". $departureFlight. "', "
			.	" trip_status			= '". $trip_status. "', "
			.	" currentDate			= '". $currentDate. "', "
			.	" totalAmount			= '". $totalPrice. "' "
			.	" WHERE cart_id 		= '". $cart_id. "' ";

		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}
	
	#====================================================================================================
	#	Function Name	:   InsertCart1 
	#	Purpose			:	Insert the new office information
	#	Return			:	return insert status
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function InsertCart1($User_Id, $Dest_Id, $cartDateTime,	$totalCharge)
	{
		global $db;


		$sql = "INSERT INTO ".CART_MASTER." (user_id,dest_id,cartDateTime,totalCharge) "
			 . " VALUES ( "
			 . " '". $User_Id ."' , "
			 . " '". $Dest_Id ."' , "			 
			 . " '". $cartDateTime ."' , "
			 . " '". $totalCharge ."'  ) ";

		//print "sql=".$sql;
		
		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);

		return $db->sql_inserted_id();
	}
	
	#====================================================================================================
	#	Function Name	:   InsertCart2 
	#	Purpose			:	Insert the new office information
	#	Return			:	return insert status
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function InsertCart2($Session_User_Id,$Dest_Id,	$itemType,	$cartDateTime,	$totalCharge)
	{
		global $db;


		$sql = "INSERT INTO ".CART_MASTER." (session_user_Id,dest_id,itemType,cartDateTime,totalCharge) "
			 . " VALUES ( "
			 . " '". $Session_User_Id ."' , "
			 . " '". $Dest_Id ."' , "			 
			 . " '". $itemType ."' , "
			 . " '". $cartDateTime ."' , "
			 . " '". $totalCharge ."'  ) ";
		
		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);

		return $db->sql_inserted_id();
	}
	

	#====================================================================================================
	#	Function Name	:   UpdateCart
	#	Purpose			:	Update the office information
	#	Return			:	return update status
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function UpdateCart($cart_id, $user_id,	$itemType,	$cartDateTime,	$totalCharge)
	{
		global $db;

		$sql = 	"UPDATE ". CART_MASTER
			.	" SET "
			.	" user_id				= '". $user_id. "', "
			.	" itemType				= '". $itemType. "', "
			.	" cartDateTime			= '". $cartDateTime. "', "
			.	" totalCharge			= '". $totalCharge. "', "
			.	" WHERE 	cart_id 	= '". $cart_id. "' ";

		# Show debug info
		if($Debug)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}

	
	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Urvashi Solanki
	#	Creation Date	:	21-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}
}
?>