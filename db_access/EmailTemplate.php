<?php
#====================================================================================================
# File Name : EmailTemplate.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contain the required function used during login to site
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# Include CoverDesign related data
require_once(dirname(__FILE__) . '/EmailTemplateData.php');

class EmailTemplate extends EmailTemplateData
{
	#====================================================================================================
	#	Function Name	:   EmailTemplate
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	05-May-2005
	#----------------------------------------------------------------------------------------------------
    function EmailTemplate()
    {
		# Make call to parent constructor 
		parent::EmailTemplateData();
	}

	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}
}
?>