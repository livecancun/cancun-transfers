<?php
#====================================================================================================
# File Name : Language.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contain the required function used during login to site
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

class Language
{
   	#====================================================================================================
	#	Function Name	:   Language
	#	Purpose			:	Constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function Language()
    {
		// Do nothing
	}
	
	#====================================================================================================
	#	Function Name	:   ViewAll
	#	Purpose			:	Provide list of Language information
	#	Return			:	return recordset with language info
	#----------------------------------------------------------------------------------------------------
	function ViewAll()
	{
		global $db;
		$sql	=	"SELECT * FROM " . LANGUAGE_MASTER
				.	" ORDER BY code";

		if(DEBUG)
			$this->__debugMessage($sql);
			
		$db->query($sql);
		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   GetLanguageCode
	#	Purpose			:	Provide list of Language information
	#	Return			:	return recordset with language info
	#----------------------------------------------------------------------------------------------------
	function GetLanguageCode()
	{
		global $db;
		$sql	=	"SELECT code FROM " . LANGUAGE_MASTER;

		if(DEBUG)
			$this->__debugMessage($sql);
			
		$db->query($sql);
		
		$rsLang = array();
		while ($db->next_record())
			$rsLang []= $db->f('code'); 
		
		return ($rsLang);
	}

	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}	
}
?>