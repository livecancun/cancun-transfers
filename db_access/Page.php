<?php
#====================================================================================================
# File Name : Page.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# Page Status
define('PAGE_VISIBLE',	'Visible');
define('PAGE_HIDDEN',	'Hidden');

# Page Type
$arrPageType	=	array(	'SimplePage'	=>	'Simple Page',
							//'Popup'			=>	'Popup',
							//'iFrame'		=>	'iFrame',
							//'OutsideLink'	=>	'Outside Link',
							'InternalLink'	=>	'Internal Link',
					);
# Page Class
class Page
{
   	#====================================================================================================
	#	Function Name	:   Page
	#	Purpose			:	Constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function Page()
    {
		// Do nothing
	}

	#====================================================================================================
	#	Function Name	:   ViewAll
	#	Purpose			:	Provide list of page information
	#	Return			:	return recordset with page info
	#----------------------------------------------------------------------------------------------------
    function ViewAll($page_status='',$lang='en')
    {
		global $db;

		/*$sql = " SELECT * FROM ".PAGE_MASTER
			 . ($page_status ? " WHERE page_status = '". $page_status."'" : '')
			 . " ORDER BY page_order ";*/
			 
		$sql	=	"SELECT * FROM " . PAGE_MASTER
				.	" LEFT JOIN " . PAGE_DETAILS . " ON page_id=page_master_id"
				.	" WHERE lang_code = '" .$lang. "'"
				. ($page_status ? " AND page_status = '". $page_status."'" : '')
				. " ORDER BY page_order ";
			 
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object());
	}
	
	#====================================================================================================
	#	Function Name	:   ViewAllLanguages
	#	Purpose			:	Provide list of language information
	#	Return			:	return recordset with language info
	#----------------------------------------------------------------------------------------------------
    function ViewAllLanguages()
    {
		global $db;

		$sql = " SELECT * FROM ".LANGUAGE_MASTER
			 . " ORDER BY languages_id ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object());
	}
	#====================================================================================================
	#	Function Name	:   getPage
	#	Purpose			:	get the page information
	#	Return			:	return recordset with page info
	#----------------------------------------------------------------------------------------------------
    function getPage($page_id)
    {
		global $db;
				
		$sql = " SELECT * FROM ".PAGE_MASTER
			//.	" LEFT JOIN " . PAGE_DETAILS . " ON page_id=page_master_id" //
			. " WHERE ".PAGE_MASTER.".page_id =  '". $page_id ."'";
				
		/*# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object(MYSQL_FETCH_SINGLE));*/
		
		//test- piyush
		$db->query($sql);
		$rs = $db->fetch_array();

		$page_info = array();
		foreach( $rs[0] as $key => $val)
		{
			$page_info[$key] = $val;
		}
		
		$sql	=	"SELECT * FROM " . PAGE_DETAILS
				.	" WHERE page_master_id  = " . $page_id
				.	" ORDER BY lang_code";
		
		$db->query($sql);		
		$rs = $db->fetch_array();
		$page_title = array();  
		$page_browser_title = array();
		$page_metatitle = array();
		$page_metadesc = array();
		$page_metakeyword = array();
		$page_content = array();
		
		foreach( $rs as $key => $row)
		{
			$page_title[]			= $row['page_title'];
			$page_browser_title []	= $row['page_browser_title'];
			$page_metatitle []		= $row['page_metatitle'];
			$page_metadesc []		= $row['page_metadesc'];			
			$page_metakeyword []	= $row['page_metakeyword'];			
			$page_content []		= stripslashes($row['page_content']);			

		}		
		
		$page_info['page_title']		 = $page_title;
		$page_info['page_browser_title'] = $page_browser_title;
		$page_info['page_metatitle']	 = $page_metatitle;
		$page_info['page_metadesc'] 	 = $page_metadesc;
		$page_info['page_metakeyword'] = $page_metakeyword;
		$page_info['page_content'] = $page_content;
		
		/*print "<pre>";
		print_r($page_info);
		print "<pre>";*/
		return($page_info);

		//test-end
		
	}

	#====================================================================================================
	#	Function Name	:   Insert
	#	Purpose			:	Insert the new page
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function Insert($page_type,				$page_url,
					$page_title,			$page_browser_title,
					$page_metatitle,		$page_metakeyword,	
					$page_metadesc,			$page_content,
					$page_parent_id)
	{
		global $db,$LangInfo;
		
		# Save Page info
		$sql = 	"INSERT INTO ". PAGE_MASTER. " (page_type,	page_url,"
			.	"								page_timestamp, page_parent_id)	"
			.	" VALUES ( "
			.	" '". 	$page_type.			"', "
			.	" '". 	$page_url.			"', "
			. 	" '".	time().				"', " 
			.	" '".	$page_parent_id.	"'"
			.	" )"; 

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);
		$page_id=$db->sql_inserted_id();
		//echo $page_id;
		for($i=0; $i<count($LangInfo); $i++)
		{
			$sql = 	"INSERT INTO ". PAGE_DETAILS. " (page_master_id,	lang_code,		 "
				.	" 								page_title,			page_browser_title,	"
				.	" 								page_metatitle,		page_metakeyword,	"
				.	" 								page_metadesc,		page_content		"
				.	"								)"
				.	" VALUES ( "
				. 	" '".	$page_id			.		"', " 				
				. 	" '".	$LangInfo[$i]['code'].		"', " 				
				. 	" '".	$page_title[$i].			"', " 
				. 	" '".	$page_browser_title[$i].	"', " 
				. 	" '".	$page_metatitle[$i].		"', " 
				. 	" '".	$page_metakeyword[$i].		"', " 
				. 	" '".	$page_metadesc[$i].			"', " 
				. 	" '".	addslashes($page_content[$i]).		"' " 
				.	" )"; 
				
				$db->query($sql);
		}

		# Set the page order as last
		$sql = " UPDATE ".PAGE_MASTER
			 . " SET "
			 . " 	page_order 			=  '". $page_id. "' "
			 . " WHERE page_id =  '". $page_id. "'";

		# Execute Query
		$db->query($sql);
		
		return $db->sql_inserted_id();
	}

	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	get the page information
	#	Return			:	return recordset with page info
	#----------------------------------------------------------------------------------------------------
    function Update($page_id, 				$page_title,
					$page_browser_title,	$page_url,
					$page_metatitle,		$page_metakeyword,
					$page_metadesc,			$page_content,
					$page_parent_id
					)
    {
		global $db, $LangInfo;
				
		$sql = " UPDATE ".PAGE_MASTER
			 . " SET "
			 . " 	page_parent_id 		=  '". $page_parent_id ."', "
			 . " 	page_url 			=  '". $page_url ."', "			 
			 . " 	page_timestamp 		=  '". time()."'"
			 . " WHERE page_id =  '". $page_id ."'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
		for( $i=0; $i<count($LangInfo); $i++)
		{
				
			$sql = 	"REPLACE INTO ". PAGE_DETAILS
				. 	"( page_master_id,	lang_code,	page_title,	page_browser_title,"
				.	"  page_metatitle,	page_metakeyword,	page_metadesc,"
				.	"  page_content) "
				.	" VALUES ( "
				. 	" ".	$page_id	.", " 
				. 	" '".	$LangInfo[$i]['code']	."', " 				
				. 	" '".	$page_title[$i]	."', " //test
				. 	" '".	$page_browser_title[$i]	."', " 
				. 	" '".	$page_metatitle[$i].	"', " 
				. 	" '".	$page_metakeyword[$i].	"', " 
				. 	" '".	$page_metadesc[$i].		"', " 
				. 	" '".	addslashes($page_content[$i])	.	"' " 
				.	" )";
			
			$db->query($sql);
			//print $sql."<br><br>";
		}
		//die;
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
		return;
		
	}

	#====================================================================================================
	#	Function Name	:   Delete
	#	Purpose			:	get the page information
	#	Return			:	return recordset with page info
	#----------------------------------------------------------------------------------------------------
    function Delete($page_id)
    {
		global $db;
				
		$sql = " DELETE FROM ".PAGE_MASTER
			 . " WHERE page_id =  '". $page_id ."'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
			
		$sql = " DELETE FROM ".PAGE_DETAILS
			 . " WHERE page_master_id =  '". $page_id ."'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);	

		return ($db->query($sql));
	}

	#====================================================================================================
	#	Function Name	:   Sort
	#	Purpose			:	Update the page order
	#	Return			:	update status
	#----------------------------------------------------------------------------------------------------
    function Sort($page_order)
    {
		global $db;
		
		# Explode the string to array
		$arrPageList = explode(':', $page_order);
		
		for($i=0; $i < count($arrPageList); $i++)
		{
			$sql = " UPDATE ".PAGE_MASTER
				 . " SET "
				 . " 	page_order 			=  '". ($i+1). "' "
				 . " WHERE page_id =  '". $arrPageList[$i]. "'";

			# Show debug info
			if(DEBUG)
				$this->__debugMessage($sql);
			
			# Update	
			$db->query($sql);
		}

		return (true);
	}

	#====================================================================================================
	#	Function Name	:   Sort
	#	Purpose			:	Update the page order
	#	Return			:	update status
	#----------------------------------------------------------------------------------------------------
    function ToggleStatus($page_id, $page_status)
    {
		global $db;
		
		$sql = " UPDATE ".PAGE_MASTER
			 . " SET "
			 . " 	page_status =  '". $page_status. "' "
			 . " WHERE page_id 	=  '". $page_id. "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		return ($db->query($sql));
	}
	#====================================================================================================
	#	Function Name	:   getPagedetails
	#	Purpose			:	get the page information
	#	Return			:	return recordset with page info
	#----------------------------------------------------------------------------------------------------
    function getPageDetails($page_id,$lang_code)
    {
		global $db;

		$sql = " SELECT * FROM ".PAGE_MASTER
			.	" LEFT JOIN " . PAGE_DETAILS . " ON page_id=page_master_id" 
			. " WHERE ".PAGE_MASTER.".page_id =  '". $page_id ."' "
			. " AND ".PAGE_DETAILS.".lang_code = '". $lang_code."' ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object(MYSQL_FETCH_SINGLE));

	}
	
	#====================================================================================================
	#	Function Name	:   getLinks
	#	Purpose			:	get menu links for that page
	#	Return			:	return recordset with page info
	#----------------------------------------------------------------------------------------------------
    function getLinks($parent_id, $lang_code)
    {
		global $db;
		
		$sql = " SELECT page_id, page_title FROM ".PAGE_MASTER
				 . " LEFT JOIN " . PAGE_DETAILS . " ON page_id=page_master_id" 		
				 . " WHERE page_parent_id =". $parent_id
				 . " AND lang_code = '". $lang_code."' "
				 . " AND page_status = 'Visible'"
				 . " ORDER BY page_order ";
			 
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object());			 
	}


	#====================================================================================================
	#	Function Name	:   ViewAll
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getSubPage($parent_id='0')
    {
		$rsPage = $this->ViewAllByParent($parent_id);

		if($rsPage)
		{
			foreach($rsPage as $key=>$row)
			{
				$rsSubPage = $this->getSubPage($row['page_id']);
				if($rsSubPage)
				{
					$rsPage[$key]['child'] = $rsSubPage;
				}
			}
		}
		return $rsPage;
	}

	#====================================================================================================
	#	Function Name	:   ViewAllByParent
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function ViewAllByParent($parent_id='0')
    {
		global $db;

		/*$sql = " SELECT * FROM ".PAGE_MASTER
			 . " WHERE page_parent_id = '". $parent_id."'"
			 . " ORDER BY page_order ";*/
			 
		$sql	=	"SELECT page_id, page_title, page_default FROM " . PAGE_MASTER
				.	" LEFT JOIN " . PAGE_DETAILS . " ON page_id=page_master_id"
				.	" WHERE lang_code = 'en'"
				.	" AND page_parent_id = '". $parent_id."'"
				. 	" ORDER BY page_order ";
			 

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   getKeyValueArray
	#	Purpose			:	
	#	Return			:	key => value
	#----------------------------------------------------------------------------------------------------
	function getKeyValueArray()
	{
		global $db;
		
		$sql	=	" SELECT page_id, page_title FROM " . PAGE_MASTER
				.	" LEFT JOIN " . PAGE_DETAILS . " ON page_id=page_master_id"
				.	" WHERE lang_code = 'en'"
				.	" AND page_parent_id = '0'"				
				. 	" ORDER BY page_order ";
		
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		$arr	= array();
		foreach ($db->fetch_array() as $page)
		{
			$arr[$page['page_id']] = $page['page_title'];
		}
		
		return ($arr);
	}

	#====================================================================================================
	#	Function Name	:   getKeyValueArray
	#	Purpose			:	
	#	Return			:	key => value
	#----------------------------------------------------------------------------------------------------
	function updateMeta($site_title, $meta_title, $meta_keyword, $meta_desc)
	{
		global $db;
		
		$sql	= " UPDATE ". PAGE_MASTER . " SET"
				. " page_brower_title 	=  '". $page_site_title ."', "
				. " page_metatitle 		=  '". $meta_title ."', "			 
			 	. " page_metakeyword 	=  '". meta_keyword ."'"
			 	. " page_metadesc		=  '". meta_desc ."'";
				
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
							
		$db->query($sql);
								
		return($db->affected_rows());
	}
	
	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}	
}
?>