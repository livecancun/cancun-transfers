<?php
#====================================================================================================
# File Name : CustomClass.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contain the required function used during login to site
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# Thumnail creation
include_once($physical_path['Libs']. 'thumbnail.php');

class CustomClass
{
	/* Private member */
	var	$Data 	= array();

	var $page_size			=	'';
	var $so					=	'';
	var $sd					=	'';
	var $alpha				=	'';
	var $total_record		=	'';

	#====================================================================================================
	#	Function Name	:   CustomClass
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function CustomClass()
    {
		# Update Post Data
		if($this->Data['F_FieldInfo'])
		{
			foreach($this->Data['F_FieldInfo'] as $key => $val)
			{
				if(array_key_exists($key, $_POST))
					$this->Data['F_FieldInfo'][$key][SEL_VAL] = $_POST[$key];
			}
		}

		if (strpos($_SERVER['HTTP_REFERER'], $_SERVER['PHP_SELF']) !== false)
		{
			# Set sort order
			$this->so 	=	($_GET['so']?$_GET['so']:$_COOKIE['so']);
			# Set sort direction
			$this->sd 	=	($_GET['sd']?$_GET['sd']:$_COOKIE['sd']);
			# Set filter character
			$this->alpha =	(isset($_GET['alpha'])?$_GET['alpha']:$_COOKIE['alpha']);
		}

		# Save filter data
		setcookie('so', 		$this->so, 		COOKIE_EXPIRE_TIME_MIN);
		setcookie('sd', 		$this->sd, 		COOKIE_EXPIRE_TIME_MIN);
		setcookie('alpha', 		$this->alpha,	COOKIE_EXPIRE_TIME_MIN);
	}

#====================================================================================================
# Default Data manupulation function
#====================================================================================================

	#====================================================================================================
	#	Function Name	:   ViewAll
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function ViewAll($addParameters='', $allRecord=false)
    {
		global $db;

		$sql = $this->getSelectQuery(	$this->Data['TableName'], 
										$this->Data['F_HeaderItem'],
										$this->Data['F_PrimaryField'], 
										$addParameters,
										$allRecord);

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   ViewAllAlt
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Adnan Sarela
	#	Creation Date	:	23-Sep-2005
	#----------------------------------------------------------------------------------------------------
    function ViewAllAlt($addParameters='', $allRecord=false)
    {
		global $db;

		$sql = $this->getSelectQuery(	$this->Data['TableName'], 
										$this->Data['F_HeaderItem_Alt'],
										$this->Data['F_PrimaryField'], 
										$addParameters,
										$allRecord);
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_array());
	}
	
	#====================================================================================================
	#	Function Name	:   getAll
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getAll($addParameters='')
    {
		global $db;

		$sql	=	"SELECT * "
				.	" FROM ". $this->Data['TableName']
				. " WHERE 1 "
				. ($addParameters != ''? $addParameters :'');

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		
		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   getInfo
	#	Purpose			:	get the information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getInfoById($id)
    {
		global $db;
		
		if(is_array($id))
			$idList = implode("','", $id);
		else
			$idList = $id;

		$sql	=	"SELECT * "
				.	" FROM ". $this->Data['TableName']
				.	" WHERE ". $this->Data['F_PrimaryKey']. " IN ('". $idList. "') ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		return ($db->fetch_array(is_array($id)?'':MYSQL_FETCH_SINGLE));
	}

	#====================================================================================================
	#	Function Name	:   getInfoByParam
	#	Purpose			:	get the information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getInfoByParam($param)
    {
		global $db;

		$sql	=	"SELECT * "
				.	" FROM ". $this->Data['TableName']
				.	" WHERE ". $param;

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		
		return ($db->fetch_array(MYSQL_FETCH_SINGLE));
	}

	#====================================================================================================
	#	Function Name	:   getCount
	#	Purpose			:	get the information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getCount()
    {
		global $db;

		$sql	=	"SELECT count(*) as 'count' "
				.	" FROM ". $this->Data['TableName'];

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		$db->next_record();
		
		return ($db->f('count'));
	}

	#====================================================================================================
	#	Function Name	:   getKeyValueArray
	#	Purpose			:	get the information in key=>value format
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getKeyValueArray()
    {
		$rs 	= $this->getAll();

		$arr 	= array();
		
		foreach($rs as $key => $row)
		{
			$arr[$row[$this->Data['F_PrimaryKey']]]  = $row[$this->Data['F_PrimaryField']];
		}

		return ($arr);
	}

	#====================================================================================================
	#	Function Name	:   Insert
	#	Purpose			:	Insert the new information
	#	Return			:	return insert status
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function Insert($POST)
	{
		global $db;
		$sql = $this->getInputQuery($this->Data['TableName'], 
									$this->Data['F_FieldInfo'],
									$POST);

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);

		# Get inserted record id
		$pk_id = $db->sql_inserted_id();
		
		# If sort order exist, set sort order value to primary key
		if($this->Data['F_Sort'])
		{
			$sql = " UPDATE " . $this->Data['TableName']
				 . " SET "
				 . $this->Data['F_Sort'] . "='" . $pk_id. "' "
				 . " WHERE " . $this->Data['F_PrimaryKey'] . "=  '". $pk_id. "'";

			# Show debug info
			if(DEBUG)
				$this->__debugMessage($sql);
			
			# Update	
			$db->query($sql);
		}
		
		return $pk_id;
	}

	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	Update the information
	#	Return			:	return update status
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function Update($pkValue, $POST)
	{
		global $db;
		
		$sql = $this->getUpdateQuery($this->Data['TableName'], 
									$this->Data['F_FieldInfo'],
									$POST, 
									$this->Data['F_PrimaryKey']."='$pkValue'");
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);
		
		return $db->affected_rows();
	}

	#====================================================================================================
	#	Function Name	:   UpdateAll
	#	Purpose			:	Update the information
	#	Return			:	return update status
	#	Author			:	Adnan Sarela
	#	Creation Date	:	23-Sep-2005
	#----------------------------------------------------------------------------------------------------
	function UpdateAll($POST)
	{
		global $db;
	
		for($i=0; $i<count($POST[$this->Data['F_PrimaryKey']]); $i++)
		{
			$sqlSets = '';

			foreach($this->Data['F_HeaderItem_Alt'] as $FieldName=>$FieldValues)
			{
				if ($FieldValues[IS_CNT])
				{
				   if ($sqlSets != '')
					  $sqlSets .= ', ';
				   $sqlSets .= "$FieldName = '" . trim($POST[$FieldName][$i]) . "'";
				}
			}
	
			$sql	=	"UPDATE " .  $this->Data['TableName']
					.	" SET " . $sqlSets
					.	" WHERE " . $this->Data['F_PrimaryKey'] . "='" . $POST[$this->Data['F_PrimaryKey']][$i] . "'";
			
			# Show debug info
			if(DEBUG)
				$this->__debugMessage($sql);
			
			# Execute Query
			$db->query($sql);
		}
	}

	#====================================================================================================
	#	Function Name	:   Delete
	#	Purpose			:	Delete the cover design information
	#	Return			:	return delete status
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function Delete($id, $retField='')
	{
		global $db;

		if(!$id)
			return false;

		if(is_array($id))
			$idList = implode("','", $id);
		else
			$idList	= $id;

		# if need any field value, store it first
		if($retField != '')
		{
			$sql = 	"SELECT $retField "
				.   " FROM ". $this->Data['TableName']
				.	" WHERE ". $this->Data['F_PrimaryKey']. " IN ('". $idList. "') ";
	
			# Execute Query
			$db->query($sql);
	
			$retValue = array();
	
			while($db->next_record())
			{
				if($db->f($retField) != '')
					array_push($retValue, $db->f($retField));
			}
		}
	
		# Is have picture field
		if(count($this->Data['F_DataField']) > 0)
		{
			foreach($this->Data['F_DataField'] as $key=>$val)
			{
				# Get the picture file name and delete all files alognwith thumbnail
				# Define query
				$sql = 	"SELECT ". $this->Data['F_DataField'][$key][0]
					.   " FROM ". $this->Data['TableName']
					.	" WHERE ". $this->Data['F_PrimaryKey']. " IN ('". $idList. "') ";

				# Execute Query
				$db->query($sql);
		
				$data = array();
		
				while($db->next_record())
				{
					if($db->f($this->Data['F_DataField'][$key][0]) != '')
						array_push($data, $db->f($this->Data['F_DataField'][$key][0]));
				}
				$ret = $this->deleteFiles($data, $this->Data['F_DataField'][$key][1]);
			}
		}

		# Define query
		$sql = 	"DELETE FROM ". $this->Data['TableName']
			.	" WHERE ". $this->Data['F_PrimaryKey']. " IN ('". $idList. "') ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $retValue;
	}

	#====================================================================================================
	#	Function Name	:   VisibleHide
	#	Purpose			:	Hide/Visible information
	#	Return			:	return status
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function VisibleHide($id, $visibility)
	{
		global $db;

		if(!$id)
			return false;

		if(is_array($id))
			$idList = implode("','", $id);
		else
			$idList	= $id;

		# Define query
		$sql = 	"UPDATE ". $this->Data['TableName']
			.	" SET ". $this->Data['F_Visibility']. " = '". $visibility. "' "
			.	" WHERE ". $this->Data['F_PrimaryKey']. " IN ('". $idList. "') ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		# Execute Query
		return $db->query($sql);
	}

	#====================================================================================================
	#	Function Name	:   Sort
	#	Purpose			:	Update the display order
	#	Return			:	true
	#	Author			:	Adnan Sarela
	#	Creation Date	:	16-Sep-2005	
	#----------------------------------------------------------------------------------------------------
    function Sort123($sort_order)
    {
		global $db;
		
		# Explode the string to array
		$arrSortList = explode(';', $sort_order);
		
		for($i=0; $i < count($arrSortList); $i++)
		{
			$sql = " UPDATE " . $this->Data['TableName']
				 . " SET "
				 . $this->Data['F_Sort'] . "='" . ($i+1). "' "
				 . " WHERE " . $this->Data['F_PrimaryKey'] . "=  '". $arrSortList[$i]. "'";

			# Show debug info
			if(DEBUG)
				$this->__debugMessage($sql);
			
			# Update	
			$db->query($sql);
		}

		return (true);
	}	

	#====================================================================================================
	#	Function Name	:   Sort
	#	Purpose			:	Update the display order
	#	Return			:	true
	#	Author			:	Adnan Sarela
	#	Creation Date	:	16-Sep-2005	
	#----------------------------------------------------------------------------------------------------
    function Sort($id, $move, $param='')
    {
		global $db;

		# Get info 
		$curRS = $this->getInfoById($id);		

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($curRS);
		
		if($move == 'Up')
		{
			# Get the up record
			$sql =	"SELECT * "
				 .	" FROM ". $this->Data['TableName']
				 .	" WHERE ". $this->Data['F_Sort']. " < ". $curRS[$this->Data['F_Sort']]. " "
				 .	"	AND " . $this->Data['F_PrimaryKey'] . " !=  '". $id. "'"
				 . ($param != ''? $param :'')
				 .	" ORDER BY ". $this->Data['F_Sort']. " DESC ";

			# Show debug info
			if(DEBUG)
				$this->__debugMessage($sql);

			# Execute Query
			if($db->query($sql))
				$repRS = $db->fetch_array(MYSQL_FETCH_SINGLE);
			else
				return false;
		}
		else
		{
			# Get the down record
			$sql =	"SELECT * "
				 .	" FROM ". $this->Data['TableName']
				 .	" WHERE ". $this->Data['F_Sort']. " > ". $curRS[$this->Data['F_Sort']]. " "
				 .	"	AND " . $this->Data['F_PrimaryKey'] . " !=  '". $id. "'"
				 . ($param != ''? $param :'')
				 .	" ORDER BY ". $this->Data['F_Sort']. " ASC ";

			# Show debug info
			if(DEBUG)
				$this->__debugMessage($sql);

			# Execute Query
			if($db->query($sql))
				$repRS = $db->fetch_array(MYSQL_FETCH_SINGLE);
			else
				return false;
		}
			
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($repRS);

		# Update order of first record			
		$sql = " UPDATE " . $this->Data['TableName']
			 . " SET " . $this->Data['F_Sort']. "='" . $repRS[$this->Data['F_Sort']]. "' "
			 . " WHERE " . $this->Data['F_PrimaryKey'] . "=  '". $id. "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		# Update	
		$db->query($sql);

		# Update order of Second record			
		$sql = " UPDATE " . $this->Data['TableName']
			 . " SET " . $this->Data['F_Sort']. "='" . $curRS[$this->Data['F_Sort']]. "' "
			 . " WHERE " . $this->Data['F_PrimaryKey'] . "=  '". $repRS[$this->Data['F_PrimaryKey']]. "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		# Update	
		$db->query($sql);

		return (true);
	}	

	#====================================================================================================
	#	Function Name	:   getSortListArray
	#	Purpose			:	get the information in key=>value format
	#	Return			:	return recordset with info
	#	Author			:	Adnan Sarela
	#	Creation Date	:	17-Sep-2005
	#----------------------------------------------------------------------------------------------------
    function getSortListArray()
    {
		$rs 	= $this->getAll("ORDER BY " . $this->Data['F_Sort']);

		$arr 	= array();
		
		foreach($rs as $key => $row)
		{
			$arr[$row[$this->Data['F_PrimaryKey']]]  = $row[$this->Data['F_PrimaryField']];
		}
		return ($arr);
	}
	
#====================================================================================================
# Query creating code
#====================================================================================================

	#====================================================================================================
	#	Function Name	:   getSelectQuery
	#	Purpose			:	Create the input query
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getSelectQuery($TableName, $HeaderItem, $defaultField, $addParameters, $allRecord)
    {
		global $db;

		$sort 	= $this->arrayKeyExists($this->so, $HeaderItem);
		$dir 	= $this->arrayKeyExists($this->sd, array('asc'=>'asc', 'desc'=>'desc'));

		$_SESSION['page_size'] = ($this->page_size?$this->page_size:$_SESSION['page_size']);

		$sql	= "SELECT count(*) as cnt "
				. " FROM ". $TableName
				. " WHERE 1 "
				. ($this->alpha != ''? " AND ". $defaultField. " LIKE '". $this->alpha. "%' ":'')
				. ($addParameters != ''? $addParameters :'');

		$db->query($sql);
		$db->next_record();
		$this->total_record = $db->f("cnt") ;

		$db->free();

		# Reset the start record if required
		if($_SESSION['start_record'] >= $this->total_record || $_SESSION['page_size'] >= $this->total_record )
			$_SESSION['start_record'] = 0;

		$sql	= "SELECT * "
				. " FROM ". $TableName
				. " WHERE 1 "
				. ($this->alpha != ''? " AND ". $defaultField. " LIKE '". $this->alpha. "%' ":'')
				. ($addParameters != ''? $addParameters :'')
				. ($sort != ''? ' ORDER BY '. $sort:'')
				. ($dir != '' && $sort != ''? ' '. $dir:'')
				. (!$allRecord?" LIMIT ". $_SESSION['start_record']. ", ". $_SESSION['page_size']:"");

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

        return $sql;
	}

	#====================================================================================================
	#	Function Name	:   getInputQuery
	#	Purpose			:	Create the input query
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getInputQuery($TableName, $FieldInfo, $POST)
    {
        $sqlFields = '';
        $sqlValues = '';
		
		$FieldList  = $this->getFieldsValue($FieldInfo, $POST);
		
		foreach($FieldList as $FieldName=>$FieldValue)
		{
		   if ($sqlFields != '')
				 $sqlFields .= ", \r\n";
		   $sqlFields .= $FieldName;
		   
		   if ($sqlValues != '')
				 $sqlValues .= ", \r\n";
		   $sqlValues .= "'" . addslashes(stripslashes(trim($FieldValue))) . "'";
		}

        return "INSERT INTO $TableName($sqlFields) VALUES ($sqlValues)";
	}

	#====================================================================================================
	#	Function Name	:   getUpdateQuery
	#	Purpose			:	Create update query
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getUpdateQuery($TableName, $FieldInfo, $POST, $primaryKey)
    {
        $sqlSets = '';

		$FieldList  = $this->getFieldsValue($FieldInfo, $POST);
	
        foreach($FieldList as $FieldName=>$FieldValue)
        {
           if ($sqlSets != '')
              $sqlSets .= ', ';
           $sqlSets .= "$FieldName = '" . addslashes(stripslashes(trim($FieldValue))) . "'";
        }

        return "UPDATE $TableName SET $sqlSets WHERE $primaryKey";;
	}

#====================================================================================================
# Field manupulating code
#====================================================================================================

	#====================================================================================================
	#	Function Name	:   getEditFieldInfo
	#	Purpose			:	Create update query
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getEditFieldInfo($rsRecord, &$FieldInfo)
    {
		foreach($FieldInfo as $key => $val)
		{
			if(array_key_exists($key, $rsRecord))
				$FieldInfo[$key][SEL_VAL] = $rsRecord[$key];
		}

        return true;
	}

	#====================================================================================================
	#	Function Name	:   getFieldsValue
	#	Purpose			:	Strip all field values
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function getFieldsValue($FieldInfo, $POST)
	{
		foreach ($FieldInfo as $fieldName => $params)
		{
			if(isset($POST[$fieldName]) || isset($_FILES[$fieldName]) || $params[CNT_TYPE] == C_DATE)
			{
//				print $params[CNT_TYPE]
				if(is_array($_FILES[$fieldName]) && (in_array($params[CNT_TYPE], array(C_PICFILE, C_AUDIOFILE, C_FILE))))
					$FieldsValue[$fieldName] = $this->uploadFile($_FILES[$fieldName], $POST['prev_'.$fieldName], $params[CNT_TYPE] == C_PICFILE?true:false, $POST['del_'.$fieldName]);
				elseif($params[CNT_TYPE] == C_DATE)
				{
//					print mktime(0,0,0, $POST[$fieldName. '_month'], $POST[$fieldName. '_day'], $POST[$fieldName. '_year']);
					$FieldsValue[$fieldName] = mktime(0,0,0, $POST[$fieldName. '_month'], $POST[$fieldName. '_day'], $POST[$fieldName. '_year']);
				}
				else
					$FieldsValue[$fieldName] = trim($POST[$fieldName]);
			}
	   }
	   return $FieldsValue;
	}

	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function uploadFile($File, $prevFileName, $createThumb=false, $deleteFile=0)
	{
		global $config;
		
		# Define file name and path
		$destFile 		= strtolower($this->getUniqueFilePrefix(). ereg_replace("[^[:alnum:].]", "", $File['name']));
		$destFolder		= $this->Data['P_Upload'];
		
		# Is file uploaded
		if($File['size'] !=0 && is_uploaded_file($File['tmp_name']))
		{
			# delete any existing file with same name
			@unlink($destFolder. $destFile);
			
			# Upload file
			$uploadStatus 	= move_uploaded_file($File['tmp_name'], $destFolder. $destFile);
			
			# if file uploaded, create required thumb
			if($uploadStatus)
			{
				# Delete previous file
				if($prevFileName)
					@unlink($destFolder. $prevFileName);
			
				if($createThumb)
				{
					# Make thumb
					$thumb = new Thumbnail();

					$thumb->image($destFolder. $destFile);
					$thumb->jpeg_quality(100);
				
					# Small thumbnail - create new and delete old one
					list($width, $height) = explode('x', strtolower($config['thumb_small']));
					
					$thumb->size_smart($width, $height);
					$thumb->get('small');
					if($prevFileName)
						@unlink($destFolder. 'small_'. $prevFileName);
	
					# Medium thumbnail - create new and delete old one
					list($width, $height) = explode('x', strtolower($config['thumb_medium']));
					$thumb->size_smart($width, $height);
					$thumb->get('medium');
					if($prevFileName)
						@unlink($destFolder. 'medium_'. $prevFileName);
	
					# Big thumbnail - create new and delete old one
					list($width, $height) = explode('x', strtolower($config['thumb_big']));
					$thumb->size_smart($width, $height);
					$thumb->get('big');
					if($prevFileName)
						@unlink($destFolder. 'big_'. $prevFileName);
				}
			}
			# return file
			return $destFile;
		}

		# If have any default file, return it
		if($prevFileName)
		{
			# Delete previous file if mention
			if($deleteFile)
			{
				@unlink($destFolder. $prevFileName);
			
				if($createThumb)
				{
					@unlink($destFolder. 'small_'. $prevFileName);
					@unlink($destFolder. 'medium_'. $prevFileName);
					@unlink($destFolder. 'big_'. $prevFileName);
				}
				return '';
			}
			return $prevFileName;
		}

		return '';
	}

	#====================================================================================================
	#	Function Name		:	getUniqueFilePrefix
	#	Purpose				:	Get Unique filename
	#	Return				:	unique filename based on time
	#----------------------------------------------------------------------------------------------------
	function getUniqueFilePrefix()
	{
		list($usec, $sec) = explode(" ",microtime());
		list($trash, $usec) = explode(".",$usec);
		return (date("YmdHis").substr(($sec + $usec), -10).'_');
	}

	#====================================================================================================
	#	Function Name		:	getUniqueFilePrefix
	#	Purpose				:	Get Unique filename
	#	Return				:	unique filename based on time
	#----------------------------------------------------------------------------------------------------
	function arrayKeyExists($pos, $array)
	{
		$keys = array_keys($array);
		return $keys[$pos-1];
	}

	#====================================================================================================
	#	Function Name		:	deleteFiles
	#	Purpose				:	Delete the required file list
	#	Return				:	delete status
	#----------------------------------------------------------------------------------------------------
	function deleteFiles($fileList, $isPicture)
	{
		if(count($fileList) == 0) return false;
		
		foreach($fileList as $key=>$fileName)
		{
			@unlink($this->Data['P_Upload']. $fileName);
			if($isPicture)
			{
				@unlink($this->Data['P_Upload']. 'small_'. $fileName);
				@unlink($this->Data['P_Upload']. 'medium_'. $fileName);
				@unlink($this->Data['P_Upload']. 'big_'. $fileName);
			}
		}
	}

	#====================================================================================================
	#	Function Name	:   RandomPassword
	#	Purpose			:	Generate random password
	#	Return			:	return password
	#----------------------------------------------------------------------------------------------------
	function RandomPassword($num_letters) 
	{ 
		$array = array( 
						 "a","b","c","d","e","f","g","h","i","j","k","l", 
						 "m","n","o","p","q","r","s","t","u","v","w","x","y", 
						  "z","1","2","3","4","5","6","7","8","9" 
						 ); 
	
		//$num_letters = 10; 
		$uppercased = 3; 
		mt_srand ((double)microtime()*1000000); 
		for($i=0; $i<$num_letters; $i++) 
			$pass .= $array[mt_rand(0, (count($array) - 1))]; 
		
		for($i=1; $i<strlen($pass); $i++) 
		{ 
			if(substr($pass, $i, 1) == substr($pass, $i-1, 1)) 
				$pass = substr($pass, 0, $i) . substr($pass, $i+1); 
		} 
		for($i=0; $i<strlen($pass); $i++) 
		{ 
			if(mt_rand(0, $uppercased) == 0) 
				$pass = substr($pass,0,$i) . strtoupper(substr($pass, $i,1)) . 
				substr($pass, $i+1); 
		} 
		$pass = substr($pass, 0, $num_letters); 
		return($pass); 
	}

	#====================================================================================================
	#	Function Name	:   ArrayToStr
	#	Purpose			:	Convert Array to string
	#	Return			:	string
	#----------------------------------------------------------------------------------------------------
	function ArrayToStr($arr)
	{
		$str = '';

		if(!is_array($arr))	return $str;
		
		foreach($arr as $key=>$val)
		{
			if(is_array($val))
			{
				$str .= $key. ':{'. $this->ArrayToStr($val). '};';
			}
			else
				$str .= $key. ':'. $val. ';';
		}
		return $str;
	}

	#====================================================================================================
	#	Function Name	:   StrToArray
	#	Purpose			:	Convert String to Array
	#	Return			:	Array
	#----------------------------------------------------------------------------------------------------
	function StrToArray(&$str)
	{
		$arr	=	'';

		do
		{
			$pos = strpos($str, ":"); 

			if($pos !== false)
			{
				$key	= substr($str, 0, $pos);
				
				$subStr = substr($str, $pos+1);
				$pos = strpos($subStr, "{"); 

				if($pos == 0 && $pos !== false)
				{
					$str = substr($subStr, 1);
					$val = $this->StrToArray($str);
					$str = substr($str, 2);
				}
				else
				{
					$val = substr($subStr, 0, strpos($subStr, ";"));
					$str = substr($subStr, strpos($subStr, ";")+1);
				}
				$arr[$key] = $val;
				
				$pos = strpos($str, "}");
				if($pos == 0 && $pos !== false)
					return $arr;
			}
		} while($str != '');
		
		return $arr;
	}

	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		if(is_array($message))
		{
			print "<pre>";
			print_r($message);
			printf("</pre><br>%s<br>", str_repeat("-=", 65));
		}
		else
		{
			printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
		}
	}
}
?>