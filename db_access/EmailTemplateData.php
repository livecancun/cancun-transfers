<?php
#====================================================================================================
# File Name : EmailTemplateData.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contain the required function used during login to site
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# include custom class
require_once(dirname(__FILE__) . '/CustomClass.php');

class EmailTemplateData extends CustomClass
{
	#====================================================================================================
	#	Function Name	:   EmailTemplate
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	04-May-2005
	#----------------------------------------------------------------------------------------------------
    function EmailTemplateData()
    {
		global $physical_path;
		global $virtual_path;
		global $asset;
		global $config;

		# Initialize all default data
		
		# Table Name
		$this->Data['TableName']			=	$config['Table_Prefix']. 'email_template';
		
		# Module Title
		$this->Data['L_Module']				=	'Email Template';

		# Help Text
		$this->Data['H_Manage']				=	'Manage all Email Template information. Add/Edit/Delete all email information. <font color=red>Do not remove default Email Template used by system.</font>';	
		$this->Data['H_AddEdit']			=	"Design email content. When you finish click <b>Save</b> to save the changes.
												Click <b>Cancel</b> to discard the changes.";
		# Command list
		$this->Data['C_CommandList']		=	array(A_ADD, A_EDIT, A_DELETE, A_VIEW);
		$this->Data['C_PopupSize']			=	'675,550';
		
		# Primary field info
		$this->Data['F_PrimaryKey']			=	'email_id';
		$this->Data['F_PrimaryField']		=	'email_title';
		
		# Upload location
		$this->Data['P_Upload']				=	$physical_path['Upload']. 'newsletter_template/';
		$this->Data['V_Upload']				=	$virtual_path['Upload']. 'newsletter_template/';
		
		# Field Information
		$this->Data['F_HeaderItem']			=	
			array(
					'email_subject'				=>	array(	TITLE	=>	'Subject',
															WIDTH	=>	'60%',
														),
					'email_default'				=>	array(	TITLE	=>	'Default',
															WIDTH	=>	'20%',
															ALIGN	=>	'center',
														),
				);

		# Field Information
		$this->Data['F_FieldInfo']	=	
			array(
					'EmailInformation'			=>	array(	GROUP_TITLE		=>	'Email Information'),
					'email_subject'				=>	array(	TITLE			=>	'Subject',
															DESC			=>	'<font color="#FF0000">* * Maximum 250 characters </font>',
															CNT_TYPE		=>	C_TEXT,
															CNT_SIZE		=>	50,
															CNT_MAXLEN		=>	250,
															VALIDATE		=>	true,
															VAL_TYPE		=>	V_EMPTY,
														),
					'email_content'				=>	array(	TITLE			=>	'Content',
															CNT_TYPE		=>	C_RICHTEXT,
															CNT_TOOLBAR		=>	'full',
															VALIDATE		=>	false,
															VAL_TYPE		=>	V_EMPTY,
														),
					'email_default'				=>	array(	CNT_TYPE		=>	C_HIDDEN,
															DEF_VAL			=>	'No',
														),
					'email_timestamp'			=>	array(	CNT_TYPE		=>	C_HIDDEN,
															DEF_VAL			=>	time(),
														),
				);

		# Intialize parent class
		parent::CustomClass();
	}
}
?>