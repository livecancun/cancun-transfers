<?php
#====================================================================================================
# File Name : User.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright  2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 
# User Type
define('ANONYMOUS', 			'Anonymous');
define('ADMIN', 				'Admin');
define('USER', 					'User');

class User
{
	var $User_Id		=	'';
    var $User_Perm		=	'';
	
   	#====================================================================================================
	#	Function Name	:   User
	#	Purpose			:	Constructor
	#	Return			:	None
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	03-Jan-2005
	#----------------------------------------------------------------------------------------------------
    function User()
    {
		// Do nothing
	}

	#====================================================================================================
	#	Function Name	:   StartSession
	#	Purpose			:	Is it valid login
	#	Return			:	returns update status
	#----------------------------------------------------------------------------------------------------
	function StartSession()
	{
	    if($_SESSION['User_Id'])
	    {

	        $this->User_Id       =   $_SESSION['User_Id'];
	        $this->User_Perm     =   $_SESSION['User_Perm'];

            if(DEBUG)
            {
            	print '<br>User Id : '. 	$this->User_Id;
	        	print '<br>User Perm: '. 	$this->User_Perm;
            }

	        switch($this->User_Perm)
	        {
	            case ADMIN:
                	# Check for proper authorization
	                if(!defined("IN_ADMIN") && !defined("IN_SITE"))
	                    $this->Destroy();
		            break;
	        }
	    }
	    else
	    {
			if(defined("IN_SITE"))
			{
				$_SESSION['User_Id']	= md5(uniqid($this->getUserIP()));
				$_SESSION['User_Perm']	= USER;
				$this->User_Id       =   $_SESSION['User_Id'];
				$this->User_Perm     =   $_SESSION['User_Perm'];
			}
			else
			{
				$path_parts = pathinfo($_SERVER['PHP_SELF']);
	
				if($path_parts["basename"] != 'login.php' && defined("IN_ADMIN"))
					$this->Destroy();
			}
	    }
	}

	#====================================================================================================
	#	Function Name	:   IsValidLogin
	#	Purpose			:	Is it valid login
	#	Return			:	returns update status
	#----------------------------------------------------------------------------------------------------
	function IsValidLogin($username, $password, $user_perm)
	{
		global $db;

	    $sql =  "SELECT user_auth_id, user_login_id, user_password, user_perm "
	        .   " FROM ". AUTH_USER
            . 	" WHERE user_login_id = '". $username. "' "
            . 	" 	AND user_perm = '". $user_perm. "' ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

	    $db->query($sql);

	    if($db->num_rows() > 0)
	    {
	        $db->next_record();

	        if($db->f('user_password') == md5($password))
	            return true;
	        return false;
	    }
	    return false;
	}

	#====================================================================================================
	#	Function Name	:   IsAuthorized
	#	Purpose			:	Is user authorized
	#	Return			:	returns authorize status
	#----------------------------------------------------------------------------------------------------
	function IsAuthorized($username, $user_perm)
	{
		global $db;

	    $sql =  "SELECT user_authorize "
	        .   " FROM ". AUTH_USER
            . 	" WHERE user_login_id = '". $username. "' "
            . 	" 	AND user_perm = '". $user_perm. "' ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

	    $db->query($sql);
		$db->next_record();

		if($db->f('user_authorize') == YES)
			return true;
		return false;
	}
	
	#====================================================================================================
	#	Function Name	:   doLogin
	#	Purpose			:	Is it valid login
	#	Return			:	returns update status
	#----------------------------------------------------------------------------------------------------
	function doLogin($username, $user_perm)
	{
		global $db;
		
		$user_auth_id = md5($username);
		
		$sql = 	"UPDATE ". AUTH_USER
			.	" SET "
			.	" 	user_lastlogin		= '". time(). 	"' "
			.	" WHERE user_auth_id	= '". $user_auth_id. "' ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		# Execute Query
		$db->query($sql);

		$_SESSION['User_Id']	=	$user_auth_id;
		$_SESSION['User_Perm']	=	$user_perm;
		$this->User_Id			=   $_SESSION['User_Id'];
		$this->User_Perm		=   $_SESSION['User_Perm'];
	}
	
	#====================================================================================================
	#	Function Name	:   Destroy
	#	Purpose			:	Destroy the user session
	#	Return			:	returns status
	#----------------------------------------------------------------------------------------------------
	function Destroy()
	{
	    session_unregister($_SESSION['User_Id']);
	    session_unregister($_SESSION['User_Perm']);
	    @session_unset();
	    @session_destroy();

//		if(defined("IN_ADMIN"))
//		{
			header("location: ./login.php");
//		}
//		else
//		{
//			header("location: ../index.php");
//		}

        return true;
	}

	#====================================================================================================
	#	Function Name	:   getLogin
	#	Purpose			:	Insert the new contact information
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function getLogin($user_auth_id)
	{
		global $db;

		$sql	=	"SELECT * "
				.	" FROM ". AUTH_USER
				.	" WHERE user_auth_id = '". $user_auth_id. "' ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);

		return ($db->next_record());
	}

	#====================================================================================================
	#	Function Name	:   InsertLogin
	#	Purpose			:	Insert the new contact information
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function InsertLogin($user_login_id, $user_password, $user_perm, $user_authorize)
	{
		global $db;

		$sql = 	"INSERT INTO ". AUTH_USER. " (user_auth_id, user_login_id, user_password, user_perm, user_authorize)"
			.	" VALUES ( "
			. 	" '".	md5($user_login_id).	"', " 
			.	" '".	$user_login_id.			"', "
			. 	" '".	md5($user_password).	"', " 
			. 	" '".	$user_perm.				"', " 
			. 	" '".	$user_authorize.		"') "; 

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);
		
		return md5($user_login_id);
	}

	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	Update the contact information
	#	Return			:	return update status
	#----------------------------------------------------------------------------------------------------
	function UpdateLogin($user_auth_id,	$user_password)
	{
		global $db;

		$sql = 	"UPDATE ". AUTH_USER
			.	" SET "
			.	" 	user_password		= '". md5($user_password). 	"' "
			.	" WHERE user_auth_id	= '". $user_auth_id. "' ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		# Execute Query
		$db->query($sql);
		
		return $db->affected_rows();
	}

	#====================================================================================================
	#	Function Name	:   Delete
	#	Purpose			:	Delete the contact information
	#	Return			:	return delete status
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	28-Dec-2004
	#----------------------------------------------------------------------------------------------------
	function DeleteLogin($user_auth_id)
	{
		global $db;

		if(is_array($user_auth_id))
			$user_list = implode("','", $user_auth_id);
		else
			$user_list = $user_auth_id;

		$sql = 	"DELETE FROM ". AUTH_USER
            . 	" WHERE user_auth_id IN ('". $user_list. "')";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}

	#====================================================================================================
	#	Function Name	:   IsUsernameExists
	#	Purpose			:	Check username already exists or not
	#	Return			:	returns update status
	#----------------------------------------------------------------------------------------------------
	function IsUsernameExists($user_login_id)
	{
		global $db;

	    $sql =  "SELECT * "
	        .   " FROM ". AUTH_USER
            . 	" WHERE user_login_id = '". $user_login_id. "' ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);
		
		if($db->num_rows()==0)
			return false;

	    return true;
	}

	#====================================================================================================
	#	Function Name	:   ChangePassword
	#	Purpose			:	Change the password
	#	Return			:	returns update status
	#----------------------------------------------------------------------------------------------------
	function ChangePassword($user_auth_id,	$user_old_password,	$user_new_password)
	{
		global $db;
   
	   $sql =  "SELECT user_password "
	        .   " FROM ". AUTH_USER
            . 	" WHERE user_auth_id = '". $user_auth_id. "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
		$db->next_record();
		
		if($db->f('user_password') == md5($user_old_password))
		{
			$this->UpdateLogin($user_auth_id, $user_new_password);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	#====================================================================================================
	#	Function Name	:   Sort
	#	Purpose			:	Update the page order
	#	Return			:	update status
	#----------------------------------------------------------------------------------------------------
    function ToggleAuthorization($user_auth_id, $user_authorize)
    {
		global $db;
		
		$sql = " UPDATE ".AUTH_USER
			 . " SET "
			 . " 	user_authorize 	=  '". $user_authorize. "' "
			 . " WHERE user_auth_id =  '". $user_auth_id. "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		return ($db->query($sql));
	}
	
	#====================================================================================================
	#	Function Name	:   ShowAdminUser
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowAdminUser()
    {
		global $db;
				
		$sql =	 " SELECT * FROM ". AUTH_USER. " WHERE user_perm = '".ADMIN."'";
			
		$rs	=	$db->query($sql);

		return($rs);
	}


	#====================================================================================================
	#	Function Name	:   ShowAuthUser
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowAuthUser($confirmationNo)
    {
		global $db;

		$sql = "SELECT * FROM ". AUTH_USER. " WHERE user_auth_id = '".$confirmationNo."' and user_perm = '".USER."'";
			
		$rs	=	$db->query($sql);

		return($rs);
	}


	#====================================================================================================
	#	Function Name	:   Show_ConfrmAuthUser
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function Show_ConfrmAuthUser($confirmationNo)
    {
		global $db;

		$sql = "SELECT * FROM ".AUTH_USER 
			 . " LEFT JOIN ". USER_MASTER. " ON ". AUTH_USER. ".autoId = ". USER_MASTER. ".auth_id "						
			 . " WHERE " . AUTH_USER .".user_login_id = '".$confirmationNo."'  AND " .AUTH_USER.".user_perm = '".USER."'"; 
			
		$rs	=	$db->query($sql);

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   getUserIP
	#	Purpose			:	Obtain user ip
	#	Return			:	returns IP address
	#----------------------------------------------------------------------------------------------------
	function getUserIP()
	{
		$client_ip = '';

		if( getenv('HTTP_X_FORWARDED_FOR') != '' )
		{
			$client_ip = ( !empty($_SERVER['REMOTE_ADDR']) ) ? $_SERVER['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : $REMOTE_ADDR );
		
			if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", getenv('HTTP_X_FORWARDED_FOR'), $ip_list) )
			{
				$private_ip = array('/^0\./', '/^127\.0\.0\.1/', '/^192\.168\..*/', '/^172\.16\..*/', '/^10.\.*/', '/^224.\.*/', '/^240.\.*/');
				$client_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
			}
		}
		else
		{
			$client_ip = ( !empty($_SERVER['REMOTE_ADDR']) ) ? $_SERVER['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : $REMOTE_ADDR );
		}
		
		return $client_ip;
	}

	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}
}
?>