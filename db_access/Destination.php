<?php
#====================================================================================================
# File Name : Destination.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contain the required function used during login to site
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# Page Status
define('DESTINATION_VISIBLE',	'Visible');
define('DESTINATION_HIDDEN',	'Hidden');

# Page Type
$arrPageType	=	array(	'SimplePage'	=>	'Simple Page',
							'InternalLink'	=>	'Internal Link',
					);
# Page Class
class Destination
{
   	#====================================================================================================
	#	Function Name	:   Page
	#	Purpose			:	Constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function Destination()
    {
		// Do nothing
	}

	#====================================================================================================
	#	Function Name	:   ViewAll
	#	Purpose			:	Provide list of destination information
	#	Return			:	return recordset with destination info
	#----------------------------------------------------------------------------------------------------
    function ViewAll($destination_status='',$lang='')
    {
		global $db;
		$lang = $_SESSION['lng'];
		
		$sql	=	"SELECT * FROM " . DESTINATION_MASTER
				.	" LEFT JOIN " . DESTINATION_DETAILS . " ON  dest_id =dest_master_id "
				.	" WHERE dest_langcode = '" .$lang. "'"
				. ($destination_status ? " AND destination_status = '". $destination_status."'" : '');

			 
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object());
	}
	
	#====================================================================================================
	#	Function Name	:   getPage
	#	Purpose			:	get the destination information
	#	Return			:	return recordset with destination info
	#----------------------------------------------------------------------------------------------------
    function getDestination($dest_id )
    {
		global $db;
				
		$sql = " SELECT * FROM ".DESTINATION_MASTER
				. " WHERE ".DESTINATION_MASTER.". dest_id  =  '". $dest_id  ."'";
				
		//test- piyush
		$db->query($sql);
		$rs = $db->fetch_array();

		$destination_info = array();
		foreach($rs[0] as $key => $val)
		{
			$destination_info[$key] = $val;
		}
		
		$sql	=	"SELECT * FROM " . DESTINATION_DETAILS
				.	" WHERE dest_master_id   = " . $dest_id 
				.	" ORDER BY dest_langcode";
		
		$db->query($sql);		
		$rs = $db->fetch_array();
		$dest_title = array();  
		
		foreach($rs as $key => $row)
		{
			$dest_title[]			= $row['dest_title'];
		}		
		
		$destination_info['dest_title']		 = $dest_title;
		return($destination_info);

		//test-end
	}

	#====================================================================================================
	#	Function Name	:   Insert
	#	Purpose			:	Insert the new destination
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function Insert($dest_title)
	{
		global $db,$LangInfo;
		
		# Save Page info
		$sql = 	"INSERT INTO ". DESTINATION_MASTER. " (dest_status)"
			.	" VALUES ( "
			.	" '	Visible' "
			.	" )"; 

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);
		$dest_id =$db->sql_inserted_id();
		//echo $dest_id ;
		for($i=0; $i<count($LangInfo); $i++)
		{
			$sql = 	"INSERT INTO ". DESTINATION_DETAILS. " (dest_master_id  ,	dest_langcode ,		 "
				.	" 								dest_title	"
				.	"								)"
				.	" VALUES ( "
				. 	" '".	$dest_id 			.		"', " 				
				. 	" '".	$LangInfo[$i]['code'].		"', " 				
				. 	" '".	$dest_title[$i].			"' " 
				.	" )"; 
				
				$db->query($sql);
		}

		# Set the destination order as last
		$sql = " UPDATE ".DESTINATION_MASTER
			 . " SET "
			 . " 	dest_order 			=  '". $dest_id . "' "
			 . " WHERE  dest_id  =  '". $dest_id . "'";

		# Execute Query
		$db->query($sql);
		
		return $db->sql_inserted_id();
	}

	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	get the destination information
	#	Return			:	return recordset with destination info
	#----------------------------------------------------------------------------------------------------
    function Update($dest_id ,$dest_title)
    {
		global $db, $LangInfo;
				
		/*$sql = " UPDATE ".DESTINATION_MASTER
			 . " SET "
			 . " 	destination_parent_id 		=  '". $destination_parent_id ."', "
			 . " 	destination_url 			=  '". $destination_url ."', "			 
			 . " WHERE  dest_id  =  '". $dest_id  ."'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);*/
		for( $i=0; $i<count($LangInfo); $i++)
		{
			//echo $i."<br>";
			$sql = 	"REPLACE INTO ". DESTINATION_DETAILS
				. 	"(dest_master_id ,dest_langcode ,dest_title)"
				.	" VALUES ( "
				. 	" ".	$dest_id 	.", " 
				. 	" '".	$LangInfo[$i]['code']	."', " 				
				. 	" '".	$dest_title[$i]	."' " //test
				.	" )";
			
		//echo $sql."<br>";
			$db->query($sql);
			//print $sql."<br><br>";
		}
		//die;
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
		return;
		
	}

	#====================================================================================================
	#	Function Name	:   Delete
	#	Purpose			:	get the destination information
	#	Return			:	return recordset with destination info
	#----------------------------------------------------------------------------------------------------
    function Delete($dest_id )
    {
		global $db;
				
		$sql = " DELETE FROM ".DESTINATION_MASTER
			 . " WHERE  dest_id IN (".$dest_id.")";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
			
		$sql = " DELETE FROM ".DESTINATION_DETAILS
			 . " WHERE  dest_master_id IN (".$dest_id.")";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);	

		return ($db->query($sql));
	}
	
	#====================================================================================================
	#	Function Name	:   Sort
	#	Purpose			:	Update the destination order
	#	Return			:	update status
	#----------------------------------------------------------------------------------------------------
    function ToggleStatus($dest_id , $dest_status)
    {
		global $db;
		
		$sql = " UPDATE ".DESTINATION_MASTER
			 . " SET "
			 . " 	dest_status =  '". $dest_status. "' "
			 . " WHERE  dest_id  	=  '". $dest_id . "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		return ($db->query($sql));
	}

	#====================================================================================================
	#	Function Name	:   getKeyValueArray
	#	Purpose			:	
	#	Return			:	key => value
	#----------------------------------------------------------------------------------------------------
	function getKeyValueArray()
	{
		global $db;
		
		$sql	=	" SELECT  dest_id , destination_title FROM " . DESTINATION_MASTER
				.	" LEFT JOIN " . DESTINATION_DETAILS . " ON  dest_id =dest_master_id "
				.	" WHERE lang_code = 'en'"
				.	" AND destination_parent_id = '0'"				
				. 	" ORDER BY destination_order ";
		
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		$arr	= array();
		foreach ($db->fetch_array() as $destination)
		{
			$arr[$destination[' dest_id ']] = $destination['destination_title'];
		}
		
		return ($arr);
	}

	#====================================================================================================
	#	Function Name	:   getKeyValueArray
	#	Purpose			:	
	#	Return			:	key => value
	#----------------------------------------------------------------------------------------------------
	function updateMeta($site_title, $meta_title, $meta_keyword, $meta_desc)
	{
		global $db;
		
		$sql	= " UPDATE ". DESTINATION_MASTER . " SET"
				. " destination_brower_title 	=  '". $destination_site_title ."', "
				. " destination_metatitle 		=  '". $meta_title ."', "			 
			 	. " destination_metakeyword 	=  '". meta_keyword ."'"
			 	. " destination_metadesc		=  '". meta_desc ."'";
				
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
							
		$db->query($sql);
								
		return($db->affected_rows());
	}

	#====================================================================================================
	#	Function Name	:   getAll
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function getAll($addParameters='')
    {
		global $db;
		$lang = $_SESSION['lng'];
		
		$sql	=	"SELECT * "
				. " FROM ". DESTINATION_MASTER
				. " LEFT JOIN " . DESTINATION_DETAILS . " ON  dest_id =	dest_master_id "
				. " WHERE dest_langcode = '" .$lang. "' AND dest_status=1 "
				. ($addParameters != ''? $addParameters :'');

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		
		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   getAdminAll
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function getAdminAll($addParameters='')
    {
		global $db;

		$sql	=	"SELECT * "
				.	" FROM ". DESTINATION_MASTER
				.	" LEFT JOIN " . DESTINATION_DETAILS . " ON  dest_id =	dest_master_id "
				. " WHERE 1 "
				. ($addParameters != ''? $addParameters :'');

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		
		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   getAllDest
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function getAllDest($addParameters='')
    {
		global $db;

		$sql	=	"SELECT * "
				.	" FROM ". DESTINATION_MASTER
				.	" LEFT JOIN " . DESTINATION_DETAILS . " ON  dest_id =	dest_master_id "
				. " WHERE 1 "
				. ($addParameters != ''? $addParameters :'');

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$rs = $db->query($sql);
		
		return ($rs);
	}

	#====================================================================================================
	#	Function Name	:   getAllPassenger
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function getAllPassenger()
    {
		global $db;

		$sql	=	"SELECT * FROM " . PASSENGER_MASTER
				.	" WHERE 1";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   getMaxPassenger
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function getMaxPassenger($passenger_id)
    {
		global $db;

		$sql	= " SELECT * FROM " . PASSENGER_MASTER
			 	. " WHERE  passenger_id  	=  '". $passenger_id . "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   Show_Cart_Dest
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function Show_Cart_Dest($cartId,$start_record,$Page_Size)
	{
		global $db;
		$sql = " SELECT * FROM ".CART_DESTINATIONS
			. " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_DESTINATIONS. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "			
//		   	. " LEFT JOIN ". TRANSFERMASTER. " ON ". CART_DESTINATIONS. ".transferId = ". TRANSFERMASTER. ".transferId "
			. " LEFT JOIN ". CART_MASTER. " ON ". CART_DESTINATIONS. ".cart_id = ". CART_MASTER. ".cart_id "	
//   		. " LEFT JOIN ". USER_MASTER. " ON ". CART_MASTER. ".user_id = ". USER_MASTER. ".user_id "	
			. " WHERE ".CART_MASTER.".cart_id = '".$cartId."'"  
			. " ORDER BY ".CART_DESTINATIONS.".currentDate"
			. " LIMIT ". $start_record . ", ". $Page_Size ;

		$rs = $db->query($sql);	  		  		
		$rs = $db->fetch_array();

		return($rs);
	}


	#====================================================================================================
	#	Function Name	:   getRate
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function getRate($dest_id,$passenger_id,$triptype_id)
    {
		global $db;

		$sql	= " SELECT * FROM " . RATE_MASTER
				. " WHERE  dest_id  	=  '". $dest_id . "' AND passenger_id  	=  '". $passenger_id . "'AND triptype_id  	=  '". $triptype_id . "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$rs = $db->query($sql);

		return($rs);

	}

	#====================================================================================================
	#	Function Name	:   getDestDetail
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
    function getDestDetail($dest_id,$passenger_id,$triptype_id)
    {
		global $db;

		$sql	= " SELECT * FROM " . RATE_MASTER
				. " LEFT JOIN ". PASSENGER_MASTER. " ON ". RATE_MASTER. ".passenger_id = ". PASSENGER_MASTER. ".passenger_id "			
				. " LEFT JOIN ". TRIPTYPE_LANG. " ON ". RATE_MASTER. ".triptype_id = ". TRIPTYPE_LANG. ".triptype_master_id "								
			 	. " WHERE  dest_id  	=  '". $dest_id . "' AND passenger_id  	=  '". $passenger_id . "'AND triptype_id  	=  '". $triptype_id . "'";
	//print "sql=".$sql;
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$rs = $db->query($sql);

		return($rs);

	}

	#====================================================================================================
	#	Function Name	:   Show_ConfrmCartDest
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function Show_ConfrmCartDest($cart_id)
	{
		global $db;
		
		$sql = " SELECT * FROM ".CART_DESTINATIONS
					   . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_DESTINATIONS. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "	
					   . " WHERE " .CART_DESTINATIONS.".cart_id  = '".$cart_id."' ";

		$rs = $db->query($sql);	  		  		

		return($rs);
	}
	
	#====================================================================================================
	#	Function Name	:   Show_CartDest
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function Show_CartDest($cartId,$start_record,$Page_Size)
	{
		global $db;
		
		$sql = " SELECT * FROM ".CART_DESTINATIONS
			   . " LEFT JOIN ". DESTINATION_LANG. " ON ". CART_DESTINATIONS. ".dest_id = ". DESTINATION_LANG. ".dest_master_id "			
			   . " LEFT JOIN ". RATE_MASTER. " ON ". CART_DESTINATIONS. ".dest_id = ". RATE_MASTER. ".dest_id "					   
			   . " LEFT JOIN ". CART_MASTER. " ON ". CART_DESTINATIONS. ".cart_id = ". CART_MASTER. ".cart_id "	
			   . " WHERE ".CART_MASTER.".cart_id = '".$cartId."'"  
  			   . " GROUP BY ".CART_DESTINATIONS.".cart_destId"
			   . " ORDER BY ".CART_DESTINATIONS.".currentDate"
			   . " LIMIT ". $start_record . ", ". $Page_Size ;
//print $sql;
		$rs = $db->query($sql);	  		  		

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   Show_Destinations
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function Show_Destinations($dest_id)
	{
		global $db;

		$sql = " SELECT * FROM ".DESTINATION_LANG
			   . " LEFT JOIN ". CART_DESTINATIONS. " ON ". DESTINATION_LANG. ".dest_master_id = ". CART_DESTINATIONS. ".departureDestId "			
			   . " WHERE ".CART_DESTINATIONS.".departureDestId = '".$dest_id."'" ; 

		$rs = $db->query($sql);	  		  		
		$db->next_record();
		
		$dest_title	=	$db->f('dest_title');

		return($dest_title);
	}
	
	#====================================================================================================
	#	Function Name	:   UpdateStatus_AP
	#	Purpose			:	Update the destination order
	#	Return			:	update status
	#----------------------------------------------------------------------------------------------------
    function UpdateStatus_AP($carttranId , $cartId)
    {
		global $db;
		
		$sql = " UPDATE ".CART_DESTINATIONS." SET "
				 . " trStatus 				=   '". AR ."' ";
		$sql .= " WHERE carttranId			= 	'". $carttranId ."' ";

		$rs = $db->query($sql);	  		  		
		
	
		$sql = " UPDATE ".CART_MASTER." SET "
			 . " cartStatus 				=   '". AR ."' ";
		$sql .= " WHERE cart_id					= 	'". $cartId ."' ";

		$rs = $db->query($sql);	  		  		

		return($rs);
	}

	
	#====================================================================================================
	#	Function Name	:   UpdateStatus_NA
	#	Purpose			:	Update the destination order
	#	Return			:	update status
	#----------------------------------------------------------------------------------------------------
    function UpdateStatus_NA($carttranId , $cartId)
    {
		global $db;
		
		$sql = " UPDATE ".CART_DESTINATIONS." SET "
			 . " trStatus 			=   '". NA ."' ";
		$sql .= " WHERE carttranId	= 	'". $carttranId ."' ";

		$rs = $db->query($sql);	  		  		
		
	
		$sql = " UPDATE ".CART_MASTER." SET "
			 . " cartStatus 		=   '". NA ."' ";
		$sql .= " WHERE cart_id		= 	'". $cartId ."' ";

		$rs = $db->query($sql);	  		  		

		return($rs);
	}
	
	#====================================================================================================
	#	Function Name	:   Select_CartDest
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	27-Jan-2006
	#----------------------------------------------------------------------------------------------------
	function Select_CartDest($user_id,$SortBy,$start_record,$Page_Size)
	{
		global $db;
		
		$sql = " SELECT * FROM ".CART_DESTINATIONS
		   . " LEFT JOIN ". CART_MASTER. " ON ". CART_DESTINATIONS. ".cart_id = ". CART_MASTER. ".cart_id "	
		   . " LEFT JOIN ". USER_MASTER. " ON ". CART_MASTER. ".user_id = ". USER_MASTER. ".user_id "	
		   . " WHERE ". USER_MASTER.".user_id = '".$user_id. "'"
		   . " ORDER BY ".CART_DESTINATIONS.".currentDate";	
				
		$db->query($sql);
		$num_records = $db->num_rows();
						
		$sql = " SELECT * FROM ".CART_DESTINATIONS
		   . " LEFT JOIN ". CART_MASTER. " ON ". CART_DESTINATIONS. ".cart_id = ". CART_MASTER. ".cart_id "	
		   . " LEFT JOIN ". USER_MASTER. " ON ". CART_MASTER. ".user_id = ". USER_MASTER. ".user_id "	
		   . " WHERE ". USER_MASTER.".user_id = '".$user_id. "' and ".CART_DESTINATIONS.".trStatus != '".NR."' and ".CART_MASTER.".cartStatus != '".NR."'" 
		   . " ORDER BY ".$SortBy	
		   . " LIMIT ". $start_record . ", ". $Page_Size ;
		$rs = $db->query($sql);

		return($rs);
	}
	
	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}	
}
?>