<?php
#====================================================================================================
# File Name : linkcat_functions.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contain utility functions
# Author : PIMSA.COM 
# Copyright : Copyright � 2005 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# Links Class
class Links
{
   	#====================================================================================================
	#	Function Name	:   Links
	#	Purpose			:	Constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function Links()
    {
		// Do nothing
	}

	#====================================================================================================
	#	Function Name	:   Show_Link_Category
	#	Purpose			:	It shows category data from database
	#	Parameters		: 	category_id - id of the category
	#						start_record - starting limit, per_page - no of records per page,
	#						num_records - no of records
	#	Return			:	returns the recordset for the specified person_id
	#	Author			:	Prashant Naik
	#	Creation Date	:	19-Jan-2005
	#----------------------------------------------------------------------------------------------------
	
	function Show_Link_Category($cat_id='', $start_record, $per_page, &$num_records)
	{
		global $db;
		
		if($cat_id)
		{
			$sql = "SELECT L1.cat_id, L1.cat_name, L1.cat_status "
					. " FROM ".LINK_CATEGORY." AS L1"
					. " WHERE L1.cat_id = '".$cat_id."' AND L1.cat_status='1' "
					. " GROUP BY L1.cat_id"
					. " ORDER BY L1.cat_name"; 
	
			$rs = $db->query($sql);
		}
		else
		{
			$sql = "SELECT L1.cat_id, L1.cat_name"
					. " FROM ".LINK_CATEGORY." AS L1"
					. " WHERE L1.cat_status='1' "
					. " GROUP BY L1.cat_id"
					. " ORDER BY L1.cat_name";
	
			$db->query($sql);
			$db->next_record();
			$num_records = $db->num_rows();
			$db->free();
	
			if($start_record >= $num_records && $start_record!=0)
				$start_record -= $per_page;
	
			$sql = "SELECT L1.cat_id, L1.cat_name"
					. " FROM ".LINK_CATEGORY." AS L1"
					. " WHERE L1.cat_status='1' "					
					. " GROUP BY L1.cat_id"
					. " ORDER BY L1.cat_name LIMIT ".$start_record.", ".$per_page.""; 
	
			$rs = $db->query($sql);

		}
//		print $sql;
		$rs = $db->fetch_array();		
		return ($rs);
	}
	
	
	#====================================================================================================
	#	Function Name	:   Show_Link_Category2
	#	Purpose			:	It shows category data from database
	#	Parameters		: 	category_id - id of the category
	#						start_record - starting limit, per_page - no of records per page,
	#						num_records - no of records
	#	Return			:	returns the recordset for the specified person_id
	#	Author			:	Prashant Naik
	#	Creation Date	:	19-Jan-2005
	#----------------------------------------------------------------------------------------------------
	function Show_Link_Category2($cat_id='', $start_record, $per_page, &$num_records)
	{
		global $db;
		
		if($cat_id)
		{
			$sql = "SELECT L1.cat_id, L1.cat_name, L1.cat_status "
					. " FROM ".LINK_CATEGORY." AS L1"
					. " WHERE L1.cat_id = '".$cat_id."' "					
					. " GROUP BY L1.cat_id"
					. " ORDER BY L1.cat_name"; 
	
			$rs = $db->query($sql);
		}
		else
		{
			$sql = "SELECT L1.cat_id, L1.cat_name "
					. " FROM ".LINK_CATEGORY." AS L1"
					. " GROUP BY L1.cat_id"
					. " ORDER BY L1.cat_name";
	
			$db->query($sql);
			$db->next_record();
			$num_records = $db->num_rows();
			$db->free();
	
			if($start_record >= $num_records && $start_record!=0)
				$start_record -= $per_page;
	
			$sql = "SELECT L1.cat_id, L1.cat_name "
					. " FROM ".LINK_CATEGORY." AS L1"
					. " GROUP BY L1.cat_id"
					. " ORDER BY L1.cat_name LIMIT ".$start_record.", ".$per_page.""; 
	
			$rs = $db->query($sql);

		}

		return ($rs);
	}
	
	
	#====================================================================================================
	#	Function Name	:   Show_Link_Category2
	#	Purpose			:	It shows category data from database
	#	Parameters		: 	category_id - id of the category
	#						start_record - starting limit, per_page - no of records per page,
	#						num_records - no of records
	#	Return			:	returns the recordset for the specified person_id
	#	Author			:	Prashant Naik
	#	Creation Date	:	19-Jan-2005
	#----------------------------------------------------------------------------------------------------
	function Show_Link_Category3($cat_id='', $start_record, $per_page, &$num_records)
	{
		global $db;
		
		if($cat_id)
		{
			$sql = "SELECT L1.cat_id, L1.cat_name, L1.cat_status "
					. " FROM ".LINK_CATEGORY." AS L1"
					. " WHERE L1.cat_id = '".$cat_id."' "					
					. " GROUP BY L1.cat_id"
					. " ORDER BY L1.cat_name"; 
	
			$rs = $db->query($sql);
		}
		else
		{
			$sql = "SELECT L1.cat_id, L1.cat_name "
					. " FROM ".LINK_CATEGORY." AS L1"
					. " GROUP BY L1.cat_id"
					. " ORDER BY L1.cat_name";
	
			$db->query($sql);
			$db->next_record();
			$num_records = $db->num_rows();
			$db->free();
	
			if($start_record >= $num_records && $start_record!=0)
				$start_record -= $per_page;
	
			$sql = "SELECT L1.cat_id, L1.cat_name "
					. " FROM ".LINK_CATEGORY." AS L1"
					. " GROUP BY L1.cat_id"
					. " ORDER BY L1.cat_name LIMIT ".$start_record.", ".$per_page.""; 
	
			$rs = $db->query($sql);

		}
		
		$rs = $db->fetch_array();	
		return ($rs);
	}
	
		
	## funciton to get total links for particlular link category
	
	function Total_Category_Link($cat_id)
	{
		global $db;
	
		$sql  = " SELECT count(*) as cnt FROM " . LINK_MASTER
			  . " WHERE link_category_id = '".$cat_id."' AND link_status='1' ";	
	
		$rs = $db->query($sql);
	
		$db->next_record();
		
		$num_records = $db->f("cnt");
		$db->free();
	
		return ($num_records);
	}
	
	## funciton to get links for particlular link category
	function Show_AllCategory()
	{
		global $db;
	
		$sql = " SELECT * FROM ".LINK_CATEGORY
			 . " WHERE cat_status = 1 "
			 . " ORDER BY cat_name"; 
	
		$rs = $db->query($sql);
	
		return ($rs);
	}
	
	
	## funciton to get category name from particlular link category id
	
	function Get_Category_Name($cat_id)
	{
		global $db;
	
		$sql  = " SELECT cat_name  FROM " . LINK_CATEGORY
			  . " WHERE cat_id = '".$cat_id."' ";	
	
		$rs = $db->query($sql);
	
		$db->next_record();
		
		$cate_name=$db->f("cat_name");
	
		$db->free();
	
		return ($cate_name);
	}
	
	#====================================================================================================
	#	Function Name	:   Update_Category
	#	Purpose			:	It updates customer's testimonial data into database
	#	Parameters		:	$cat_id,	$cat_name,	$cat_status
	#	Return			:	returns the number of rows affected by the last UPDATE query
	#	Author			:	Prashant Naik
	#	Creation Date	:	19-Jan-2005
	#----------------------------------------------------------------------------------------------------
	function Update_Category($cat_id,$cat_name,$cat_status)
	{
		global $db;
		
		$sql = " UPDATE ".LINK_CATEGORY . " SET "
			  . " cat_name				= '". addslashes($cat_name). "', "
			  . " cat_status			= '". addslashes($cat_status). "' "
			  . " WHERE cat_id 			= '". $cat_id. "' ";
			 
		$db->query($sql);
	
		return ($db->affected_rows());
		
	}
	
	#====================================================================================================
	#	Function Name	:   Delete_Category
	#	Purpose			:	It deletes customer's testimonial data from database
	#	Parameters		: 	cat_id - id of the category
	#	Return			:	returns the number of rows affected by the last DELETE query 	
	#	Author			:	Prashant Naik
	#	Creation Date	:	19-Jan-2005
	#----------------------------------------------------------------------------------------------------
	function Delete_Category($cat_id)
	{
		global $db;
		
		$sql = " DELETE FROM ".LINK_CATEGORY
			 . " WHERE cat_id =  '". $cat_id."' ";
		$db -> query($sql);
			
		return ($db -> affected_rows());
	}
	
	#====================================================================================================
	#	Function Name	:   Show_LinkDetail
	#	Purpose			:	It shows LinkDetail data from database
	#	Parameters		: 	Link_id - id of the LinkDetail
	#						start_record - starting limit, per_page - no of records per page,
	#						num_records - no of records
	#	Return			:	returns the recordset for the specified person_id
	#	Author			:	Nilesh Patel
	#	Creation Date	:	13-Sept-2004
	#----------------------------------------------------------------------------------------------------
	function Show_LinkDetail($link_id ='', $start_record, $per_page, &$num_records)
	{
		global $db;
	
		if($link_id)
		{
			$sql = "SELECT * FROM ".LINK_MASTER
					." WHERE link_id ='" . $link_id . "' ";
	
			$rs = $db->query($sql);
		}
		else
		{
			$sql  = " SELECT count(*) as cnt FROM " . LINK_MASTER;
		#		  . " WHERE url_title like '".$lid."%' ";	
			$db->query($sql);
			$db->next_record();
			$num_records = $db->f("cnt") ;
			$db->free();
	
			if($start_record >= $num_records && $start_record!=0)
				$start_record -= $per_page;
	
			$sql = " SELECT * FROM ".LINK_MASTER
		#		 . " WHERE url_title like '".$lid."%' "
				 . " LIMIT ". $start_record . ", ". $per_page;
			$rs = $db->query($sql);
		}
	
		return ($rs);
	}
	
	## funciton to get links for particlular link category
	function Show_Category_Link($cat_id, $start_record, $per_page, &$num_records)
	{
		global $db;
	
		$sql  = " SELECT * FROM " . LINK_MASTER 
			  . " LEFT JOIN ".LINK_CATEGORY." ON  ".LINK_MASTER.".link_category_id = ".LINK_CATEGORY.".cat_id"		
			  . " WHERE ".LINK_MASTER.".link_category_id = '".$cat_id."' AND  ".LINK_MASTER.". link_status='1' "
			  . " ORDER BY ".LINK_MASTER.".link_title" ;

		$rs = $db->query($sql);
		$rs = $db->fetch_array();	

		return ($rs);
	}
	
	
	function Show_Category_Link2($cat_id, $start_record, $per_page, &$num_records)
	{
		global $db;
	
		$sql  = " SELECT count(*) as cnt FROM " . LINK_MASTER 
			  . " WHERE link_category_id = '".$cat_id."' ";	
			  
		$db->query($sql);
		$db->next_record();
		$num_records = $db->f("cnt") ;
		$db->free();
	
		if($start_record >= $num_records && $start_record!=0)
			$start_record -= $per_page;
	
		$sql = " SELECT * FROM ".LINK_MASTER
			 . " WHERE link_category_id = '".$cat_id."' "
			 . " LIMIT ". $start_record . ", ". $per_page;
	
		$rs = $db->query($sq);
		return ($rs);
	}
	
	function Show_Category_Link3($cat_id, $start_record, $per_page, &$num_records)
	{
		global $db;
	
		$sql  = " SELECT * FROM " . LINK_MASTER 
			  . " WHERE link_category_id = '".$cat_id."' AND link_status='1' ";	

		$rs = $db->query($sql);
	//print $sql;
		return ($rs);
	}
	

	function Show_Category()
	{
		global $db;
	
		$sql  = " SELECT * FROM " .LINK_MASTER." ";

		$rs = $db->query($sql);
		$rs = $db->fetch_array();	

		return ($rs);
	}
	
	
	#====================================================================================================
	#	Function Name	:   Insert_LinkDetail
	#	Purpose			:	It inserts	customer's testimonial data into database
	#	Parameters		:	$cat_name, $cat_status
	#	Return			:	returns the number of rows affected by the last INSERT query
	#	Author			:	Nilesh Patel
	#	Creation Date	:	13-Sept-2004
	#----------------------------------------------------------------------------------------------------
	function Add_LinkDetail($link_cat_id,$link_name,$link_address,$link_city,$link_state,$link_zip,$link_country,$link_telno,$link_email,$link_title,$link_desc,$link_url,$link_oururl,$link_date,$link_email,$link_status)
	{
		global $db;
		
		$sql = " INSERT INTO ".LINK_MASTER."(link_category_id,		link_name,		link_address,		link_city,
											link_state,				link_zip,		link_country,		link_telno,
											link_title,				link_desc,		link_url,			link_oururl,
											link_add_date,			link_add_by,	link_status)"
		  ." VALUES ('".$link_cat_id ."',				'". addslashes($link_name) ."',		'". addslashes($link_address) ."',
		  			'". addslashes($link_city) ."',		'". addslashes($link_state) ."',	'". addslashes($link_zip) ."',
					'". addslashes($link_country) ."',	'". addslashes($link_telno) ."',	'". addslashes($link_title) ."',
					'". addslashes($link_desc)."',		'". addslashes($link_url) ."' ,		'". addslashes($link_oururl) ."',
					'". $link_date ."','" .addslashes($link_email). "','".addslashes($link_status)."')" ;
		
		$db->query($sql);
		
		return ($db->sql_inserted_id());
	}

	#====================================================================================================
	#	Function Name	:   Add_Link_Category
	#	Purpose			:	It inserts	customer's testimonial data into database
	#	Parameters		:	$cat_name, $cat_status
	#	Return			:	returns the number of rows affected by the last INSERT query
	#	Author			:	Prashant Naik
	#	Creation Date	:	19-Jan-2005
	#----------------------------------------------------------------------------------------------------
	function Add_Link_Category($cat_name,$cat_parent_id,$cat_status)
	{
		global $db;
		
		$sql = " INSERT INTO ".LINK_CATEGORY." (cat_name,cat_parent_id,cat_status) "
			  ." VALUES ('".addslashes($cat_name) ."','". $cat_parent_id ."','". $cat_status . "')" ;
			 
		$db->query($sql);
	
		return ($db->affected_rows());
	}

	#====================================================================================================
	#	Function Name	:   Update_LinkDetail
	#	Purpose			:	It updates customer's testimonial data into database
	#	Parameters		:	$cat_id,	$cat_name,	$cat_status
	#	Return			:	returns the number of rows affected by the last UPDATE query
	#	Author			:	Nilesh Patel
	#	Creation Date	:	13-Sept-2004
	#----------------------------------------------------------------------------------------------------
	function Update_LinkDetail($link_id,$link_name,$link_address,$link_city,$link_state,$link_zip,$link_country,$link_telno,$link_title,$link_desc,$link_url,$link_oururl,$link_email,$link_status)
	{
		global $db;
			
		$sql = " UPDATE ".LINK_MASTER . " SET "
			  . " link_name				= '". addslashes($link_name) . "' ,"
			  . " link_address 			= '". addslashes($link_address) . "' ,"
			  . " link_city 			= '". addslashes($link_city) . "' ,"
			  . " link_state			= '". addslashes($link_state) . "' ,"
			  . " link_zip				= '". addslashes($link_zip) . "' ,"
			  . " link_country 			= '". addslashes($link_country) . "' ,"
			  . " link_telno 			= '". addslashes($link_telno) . "' ,"
			  . " link_title			= '". addslashes($link_title) . "' ,"
			  . " link_desc				= '". addslashes($link_desc) . "', "
			  . " link_url				= '". addslashes($link_url) . "' ,"
			  . " link_oururl 			= '". addslashes($link_oururl) . "' ,"
			  . " link_add_by			= '". addslashes($link_email) . "' ,"
			  . " link_status			= '". addslashes($link_status) . "'"
			  . " WHERE link_id			= '". $link_id . "' ";
			 
		$db->query($sql);
	
		return ($db->affected_rows());
		
	}
	
	#====================================================================================================
	#	Function Name	:   Delete_LinkDetail
	#	Purpose			:	It deletes customer's testimonial data from database
	#	Parameters		: 	cat_id - id of the LinkDetail
	#	Return			:	returns the number of rows affected by the last DELETE query 	
	#	Author			:	Nilesh Patel
	#	Creation Date	:	13-Sept-2004
	#----------------------------------------------------------------------------------------------------
	function Delete_LinkDetail($link_id)
	{
		global $db;
		
		$sql = " DELETE FROM ".LINK_MASTER
			 . " WHERE link_id =  '". $link_id."' ";
		$db->query($sql);
			
		return($db->affected_rows());
	}
	
	#====================================================================================================
	#	Function Name	:   Delete_Subcategory
	#	Purpose			:	It shows report's data from database
	#	Parameters		: 	doc_id - id of the rpoet
	#						start_record - starting limit, per_page - no of records per page,
	#						num_records - no of records
	#	Return			:	returns the recordset for the specified report
	#	Author			:	Prashant Naik
	#	Creation Date	:	19-Jan-2005
	#===================================================================================================#
	function Delete_Subcategory($sub_cat_id)
	{
		global $db;
		
		$sql = " DELETE FROM ".SUB_CATEGORY_MASTER_TABLE
			  ." WHERE sub_cat_id = '". $sub_cat_id ."' ";
		$db -> query($sql);
		
		return ($db -> affected_rows());
	}
	
	function LinkCatagory_FillCombo()
	{
		global $db;
		
		$sql = " SELECT *  FROM ".LINK_CATEGORY_TABLE
			  ." ORDER BY cat_id" ;
		
		$rs=$db->query($sql);
		
		return($rs);
	}
	
	function SubCatagory_FillCombo($cat_id)
	{
		global $db;
		
			$sql = " SELECT *,CM.cat_id FROM ".LINK_CATEGORY_TABLE." AS CM "
				 . " LEFT JOIN ".SUB_CATEGORY_MASTER_TABLE." AS SCM ON SCM.cat_id = CM.cat_id"
				 . " WHERE CM.cat_id =  '". $cat_id ."'"
				 . " GROUP BY SCM.sub_cat_name ORDER BY CM.cat_id " ;
		
		$rs=$db->query($sql);
		
		return($rs);
	}
		
	
	function Move_Category($Old_Cat, $New_Cat)
	{
		global $db;
		
		$sql = " UPDATE ".LINK_CATEGORY_TABLE . " SET "
			  . " cat_parent_id			= '".$New_Cat. "' "
			  . " WHERE cat_id 			= '". $Old_Cat. "' ";
			  
		$db->query($sql);
		
		return ($db->sql_inserted_id());
	}
	
		
	#====================================================================================================
	#	Function Name		:	GetCategory
	#	Purpose				:	Generates list of category and it subcategory
	#	Parameters			:	$selected	: [Optional] Selected value for date
	#							$startyear  : Starting value of year
	#							$count		: No of years to be generated
	#	Return				:	Return HTML string for select box containing years
	#	Author				:	Prashant Naik
	#	Creation Date		:	19-Jan-2005
	#----------------------------------------------------------------------------------------------------
	  
	function get_cat($cat_id)
	{
	
		global $db;
			$i = 0;
			$arry = array();
			$sql = "SELECT * FROM ".LINK_CATEGORY_TABLE
					." WHERE cat_parent_id ='" . $cat_id . "' "
					." ORDER BY cat_name";
	//echo $sql; 
			$db->query($sql);
			
			while ($i < $db->num_rows()) 
			{
				$db->next_record();
				$arry[$i]['cat_id'] 	= $db->f('cat_id');
				$arry[$i]['cat_name'] 	= $db->f('cat_name');
				$i++;
			} 
	//print_r ($arry);
	 return $arry;
	} 
	
	/**
	 * select_cat_menu::build_menu()
	 * 
	 * @param integer $catgry
	 * @param integer $id
	 * @param string  $line
	 * @return 
	 **/
	function build_menu($cat_id=0 , $line = '', $cnt1='',$Selected='') 
	{
			$category = get_cat($cat_id);
			
			$cnt = count($category);
	
			if($cnt1)
			{
				for ($i = 0; $i < $cnt; $i++)
				{			
					$sel = ($category[$i]['cat_id']=='') ? ' selected="selected"' : '';
					$str1 .= '<option value="' . $category[$i]['cat_id'] . '" ' . $sel . '>' . $line . ' '. $category[$i]['cat_name'] . '</option>' . "\n";
					$str1 .= build_menu($category[$i]['cat_id'], '--' . $line, $cnt1--,$Selected);
				}
			}
			return($str1); 
	} 

}

?>