<?php
#====================================================================================================
# File Name : Utility.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 
class Utility
{
	#====================================================================================================
	#	Function Name	:   Industry
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function Utility()
    {
		global $config;

		# Enable / Disable Debug
		#-----------------------------------------------------------------------------
		$_SESSION['Debug']	=	isset($_GET['debug'])?$_GET['debug']:$_SESSION['Debug'];
		define('DEBUG', $_SESSION['Debug']);

		# Set page size under cookie
		#-----------------------------------------------------------------------------
		$_SESSION['page_size']		=	isset($_POST['page_size'])?$_POST['page_size']:(isset($_SESSION['page_size'])?$_SESSION['page_size']:$config[WC_PAGESIZE]);
		
		# Set starting record
		#-----------------------------------------------------------------------------
		if(strpos($_SERVER['HTTP_REFERER'], $_SERVER['PHP_SELF']) !== false)
			$_SESSION['start_record']	=	$_POST['goto_page'] ? (((int)$_POST['goto_page']-1) * $_POST['page_size']) : (isset($_GET['start']) ? (int)$_GET['start'] : ($_SESSION['start_record']?$_SESSION['start_record']:0));
		else
			$_SESSION['start_record']	=	0;
	}

	#====================================================================================================
	#	Function Name	:   uploadFile
	#	Purpose			:	Upload the file
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
	function uploadFile($File, 			$uploadDirectory, 
					  	$createThumb=false,
					  	$thumbSize='', 	$deleteOldFile='')
    {
		global $physical_path;
		global $config;
		global $thumb;
		
		$retVal 	= '';
		$destPath   = '';
		$destFile   = '';
	
		if(is_array($File))
		{
			# Define file name and path
			$destFile 			= strtolower($this->getUniqueFilePrefix(). ereg_replace("[^[:alnum:].]", "", $File['name']));
			$destFileFullPath	= $physical_path[$uploadDirectory]. $destFile;
			
			# Upload file
			$uploadStatus 		= move_uploaded_file($File['tmp_name'], $destFileFullPath);
			
			# If uploaded
			if($uploadStatus)
			{
				# change required permission
				@chmod($destFileFullPath, decoct($config['File_Perms']));
				
				# Delete old file
				if($deleteOldFile)
					@unlink($physical_path[$uploadDirectory]. $deleteOldFile);
				
				# Is thumbnail creation requested?
				if($createThumb)
				{
					# Make thumb
					$thumb->image($destFileFullPath);
					$thumb->jpeg_quality(100);

					$i='';
					# Go through each size and create thumbnail
					foreach($thumbSize as $key => $val)
					{
						# Set thumb size
						if(count($val) >= 2)
						{
							$thumb->size_smart($val[0], $val[1]);
							//$thumb->size_fix($val[0], $val[1]);
						}
						else
							$thumb->size_auto($val[0]);
						
						# Create thumb
						$thumb->get('thumb'. $i);
		
						# Delete old thumb
						if($deleteOldFile)
							@unlink($physical_path[$uploadDirectory]. 'thumb'. $i. $deleteOldFile);
		
						$i = $i + 1;
					}
				}
				return $destFile;
			}
			else
				return '';
		}
		else
		{
			return '';
		}
	}
	#====================================================================================================
	#	Function Name		:	getUniqueFilePrefix
	#	Purpose				:	Get Unique filename
	#	Return				:	unique filename based on time
	#----------------------------------------------------------------------------------------------------
	function getUniqueFilePrefix()
	{
		list($usec, $sec) = explode(" ",microtime());
		list($trash, $usec) = explode(".",$usec);
		return (date("YmdHis").substr(($sec + $usec), -10).'_');
	}
	#====================================================================================================
	#	Function Name		:	showPagination
	#	Purpose				:	Display the pagination 
	#	Return				:	pagination 
	#----------------------------------------------------------------------------------------------------
	function showPagination($num_items, $add_prevnext_text = TRUE)
	{
		global $lang;
	
		$path_parts = pathinfo($_SERVER['SCRIPT_FILENAME']);
		$base_url = $path_parts["basename"] . "?" . substr($_SERVER['QUERY_STRING'], 0, strpos($_SERVER['QUERY_STRING'],"&start")===false?strlen($_SERVER['QUERY_STRING']):strpos($_SERVER['QUERY_STRING'],"&start"));
		$total_pages = ceil($num_items/$_SESSION['page_size']);

		if ( $total_pages == 1 )
			return '';
	
		$on_page = floor($_SESSION['start_record'] / $_SESSION['page_size']) + 1;
		$page_string = '';
		if ( $total_pages > 10 )
		{
			$init_page_max = ( $total_pages > 3 ) ? 3 : $total_pages;
			for($i = 1; $i < $init_page_max + 1; $i++)
			{
				$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="'. $base_url. "&amp;start=" . ( ( $i - 1 ) * $_SESSION['page_size'] )  . '" >' . $i . '</a>';
//				$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url. "&amp;start=" . ( ( $i - 1 ) * $_SESSION['page_size'] )  . '\';document.forms[0].submit();" >' . $i . '</a>';
				if ( $i <  $init_page_max )
					$page_string .= ' ';
			}
	
			if ( $on_page > 1  && $on_page < $total_pages )
			{
				$page_string .= ( $on_page > 5 ) ? ' ... ' : ', ';
				$init_page_min = ( $on_page > 4 ) ? $on_page : 5;
				$init_page_max = ( $on_page < $total_pages - 4 ) ? $on_page : $total_pages - 4;
	
				for($i = $init_page_min - 1; $i < $init_page_max + 2; $i++)
				{
					$page_string .= ($i == $on_page) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="'. $base_url . "&amp;start=" . ( ( $i - 1 ) * $_SESSION['page_size'] )  . '" >' . $i . '</a>';
//					$page_string .= ($i == $on_page) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $_SESSION['page_size'] )  . '\'; document.forms[0].submit();">' . $i . '</a>';
					if ( $i <  $init_page_max + 1 )
						$page_string .= ' ';
				}
				$page_string .= ( $on_page < $total_pages - 4 ) ? ' ... ' : ', ';
			}
			else
				$page_string .= ' ... ';
	
			for($i = $total_pages - 2; $i < $total_pages + 1; $i++)
			{
				$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>'  : '<a class=pageLink href="' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $_SESSION['page_size'] ) . '">' . $i . '</a>';
//				$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>'  : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $_SESSION['page_size'] ) . '\';document.forms[0].submit();">' . $i . '</a>';
				if( $i <  $total_pages )
					$page_string .= " ";
			}
		}
		else
		{
			for($i = 1; $i < $total_pages + 1; $i++)
			{
				$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $_SESSION['page_size'] ) . '">' . $i . '</a>';
//				$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $_SESSION['page_size'] ) . '\';document.forms[0].submit();">' . $i . '</a>';
				if ( $i <  $total_pages )
						$page_string .= " ";
			}
		}
	
		if ( $add_prevnext_text )
		{
			if ( $on_page > 1 )
//				$page_string = ' <a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $on_page - 2 ) * $_SESSION['page_size'] ) . '\'; document.forms[0].submit();">' . "Previous" . '</a>&nbsp;&nbsp;' . $page_string;
				$page_string = ' <a class=pageLink href="' . $base_url . "&amp;start=" . ( ( $on_page - 2 ) * $_SESSION['page_size'] ) . '">' . "Previous" . '</a>&nbsp;&nbsp;' . $page_string;
			else
				$page_string = '&nbsp;<font class="disabledText">Previous</font>&nbsp;' . $page_string;
	
			if ( $on_page < $total_pages )
//				$page_string .= '&nbsp;&nbsp;<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( $on_page * $_SESSION['page_size'] ) . '\';document.forms[0].submit();">' . "Next" . '</a>';
				$page_string .= '&nbsp;&nbsp;<a class=pageLink href="' . $base_url . "&amp;start=" . ( $on_page * $_SESSION['page_size'] ) . '">' . "Next" . '</a>';
			else
				$page_string .= '&nbsp;<font class="disabledText">Next</font>&nbsp;';
		}
	
		return $page_string;
	}

	#====================================================================================================
	#	Function Name		:	fillArrayCombo
	#	Purpose				:	Creat the option list from array
	#	Return				:	option list
	#----------------------------------------------------------------------------------------------------
	function fillArrayCombo($arrName, $selected='')
	{
		$lstOption = "";
		reset($arrName);
		while(list($key,$val) = each($arrName))
		{	
			$lstOption .= "<option value=\"". $key. "\"";
			if($selected == (string)$key)
				$lstOption .= " selected ";
			$lstOption .= ">".$val. "</option>";
		}
		return $lstOption;
	}

	#====================================================================================================
	#	Function Name		:	fillComboFromRS
	#	Purpose				:	Creat the option list from array
	#	Return				:	option list
	#----------------------------------------------------------------------------------------------------
	function fillComboFromRS($rs, $keyField, $valField, $selectedValue='')
	{		
		$lstOption = "";
		foreach($rs as $id => $Record)
		{
			eval('$key=$Record->$keyField;');
			eval('$val=$Record->$valField;');
			
			$lstOption .= "<option value=\"". $key. "\"";
			if($selectedValue == (string)$key)
				$lstOption .= " selected ";
			$lstOption .= ">". $val. "</option>";
		}
		
		return $lstOption;
	}

	#====================================================================================================
	#	Function Name		:	fillDbCombo
	#	Purpose				:	Creat the option list from array
	#	Return				:	option list
	#----------------------------------------------------------------------------------------------------
	
	function fillDbCombo($arrName, $key, $val, $selected='')
	{
		global $db;
		//print $selected;
		$strHTML = "";
		$i = 0;
		
		while($i < $db->num_rows())
		{
			$db->next_record();
			$strHTML .= "<option value=\"". $db->f($key). "\"";
	
			if($selected == $db->f($key))
				$strHTML .= " selected ";
			if(is_array($val))
				$strHTML .= ">".$db->f($val[0])." ".$db->f($val[1])." </option>";
			else
				$strHTML .= ">".$db->f($val). "</option>";
			$i++;
		}
		
		$db->free();
		//print $strHTML;
		return $strHTML;
	}
		
	#====================================================================================================
	#	Function Name	:   ViewAllState
	#	Purpose			:	Provide list of industry information
	#	Return			:	return recordset with industry info
	#----------------------------------------------------------------------------------------------------
    function  ViewAllState()
    {
		global $db;

		# Get all contact type
		$sql	=	"SELECT * "
				.	" FROM ". STATE_MASTER
				.	" ORDER BY state_name ";
			
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object());
	}
	#====================================================================================================
	#	Function Name	:   formatDate
	#	Purpose			:	foramate date in appropreate manner
	#	Return			:	return formatted date
	#----------------------------------------------------------------------------------------------------
	function formatDate($dateValue, $formate='d-M-Y')
	{
		ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $dateValue, $datePart);
		return $this->makeDate($datePart[3], $datePart[2], $datePart[1], $formate);
	}
	function makeDate($intDay, $intMonth, $intYear, $formate='d-M-Y')
	{
		return date ($formate, mktime (0,0,0,$intMonth, $intDay, $intYear));
	}
	#====================================================================================================
	#	Function Name	:   formatTime
	#	Purpose			:	foramate date in appropreate manner
	#	Return			:	return formatted date
	#----------------------------------------------------------------------------------------------------
	function formateTime($dateValue)
	{
		ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $dateValue, $datePart);		 
		return mktime(0,0,0, $datePart[2],$datePart[3],$datePart[1]);
	}
	#====================================================================================================
	#	Function Name	:   formatDate1
	#	Purpose			:	foramate date in appropreate manner
	#	Return			:	return formatted date
	#----------------------------------------------------------------------------------------------------
	function formatDate1($dateValue,$formate='d-M-Y')
	{
		return date($formate,$dateValue);
	}
	
	#====================================================================================================
	#	Function Name	:   RandomPassword
	#	Purpose			:	Generate random password
	#	Return			:	return password
	#----------------------------------------------------------------------------------------------------
	function RandomPassword($num_letters) 
	{ 
		$array = array( 
						 "a","b","c","d","e","f","g","h","i","j","k","l", 
						 "m","n","o","p","q","r","s","t","u","v","w","x","y", 
						  "z","1","2","3","4","5","6","7","8","9" 
						 ); 
	
		//$num_letters = 10; 
		$uppercased = 3; 
		mt_srand ((double)microtime()*1000000); 
		for($i=0; $i<$num_letters; $i++) 
			$pass .= $array[mt_rand(0, (count($array) - 1))]; 
		
		for($i=1; $i<strlen($pass); $i++) 
		{ 
			if(substr($pass, $i, 1) == substr($pass, $i-1, 1)) 
				$pass = substr($pass, 0, $i) . substr($pass, $i+1); 
		} 
		for($i=0; $i<strlen($pass); $i++) 
		{ 
			if(mt_rand(0, $uppercased) == 0) 
				$pass = substr($pass,0,$i) . strtoupper(substr($pass, $i,1)) . 
				substr($pass, $i+1); 
		} 
		$pass = substr($pass, 0, $num_letters); 
		return($pass); 
	}
	
	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}
}
?>