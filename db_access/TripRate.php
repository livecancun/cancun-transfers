<?php
#====================================================================================================
# File Name : TripRate.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

# Include CoverDesign related data
require_once(dirname(__FILE__) . '/TripRateData.php');
require_once(dirname(__FILE__) . '/TripType.php');

class TripRate extends TripRateData
{
	#====================================================================================================
	#	Function Name	:   Rate
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Adnan Sarela
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function TripRate()
    {
		# Make call to parent constructor 
		parent::TripRateData();
	}

	#====================================================================================================
	#	Function Name	:   getAll
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Adnan Sarela
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getAll()
    {
		global $db;
			
		$sql	= "SELECT * FROM " . $this->Data['TableName']
				. "	ORDER BY passenger_id";
	//	print "sql=".$sql;
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
		$rsRange = $db->fetch_array();
		
		for($i=0; $i < count($rsRange); $i++)
		{
			$sql	= "SELECT * FROM " . $this->Data['TableGrid']
					. " WHERE passenger_id = '" .$rsRange[$i]['passenger_id']. "'"
					. "	ORDER BY dest_id,triptype_id";

			# Show debug info
			if(DEBUG)
				$this->__debugMessage($sql);
	
			$db->query($sql);
			$rsCPG = $db->fetch_array();
			
			$typeData = array();
			$sizeData = array();
			
			foreach($rsCPG as $recRate)
			{
				$typeData[$recRate['triptype_id']] = $recRate['rate'];
				$sizeData[$recRate['dest_id']] = $typeData;
			}
			$rsRange[$i]['price_data'] = $sizeData;
		}
/*		
		print "<pre>";
		print_r($rsRange);
		print "</pre>";
*/
		return($rsRange);
	}


	#====================================================================================================
	#	Function Name	:   getRate
	#	Purpose			:	Get the card price
	#	Return			:	None
	#	Author			:	Adnan Sarela
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getRate($type_id, $dest_id, $qty)
    {
		global $db;
		
		$sql	= "SELECT * FROM " .$this->Data['TableGrid']
				. " LEFT JOIN " .$this->Data['TableName']. " ON passenger_id = passenger_id"
				. " WHERE	passenger_starting_range	>=	'" .$qty. "'"
				. " AND		triptype_id		=	'" .$type_id. "'"
				. " AND		dest_id		=	'" .$dest_id. "'"
				. " ORDER BY passenger_starting_range LIMIT 0,1";
				
		$db->query($sql);
		$rsRate = $db->fetch_array(MYSQL_FETCH_SINGLE);
		
		if (!$rsRate)
		{
			$this->__errorAlert("Error while getting price");
			return 0;
		}
		else
			return $rsRate['rate'];
	}

/*	#====================================================================================================
	#	Function Name	:   getAllGrid
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Adnan Sarela
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getAllGrid($params='')
    {
		global $db;
		
		$sql	= "SELECT * FROM " . $this->Data['TableGrid']
				. " WHERE passenger_id = '" .$rsRange[$i]['passenger_id']. "'"
				. "	ORDER BY dest_id,triptype_id";		
	}
*/	
	#====================================================================================================
	#	Function Name	:   getInfoByParamGrid
	#	Purpose			:	Simple constructor
	#	Return			:	None
	#	Author			:	Adnan Sarela
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getInfoByParamGrid($param)
    {
		global $db;

		$sql	=	"SELECT * "
				.	" FROM ". $this->Data['TableGrid']
				.	" WHERE ". $param;

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		
		return ($db->fetch_array(MYSQL_FETCH_SINGLE));
	}
	
	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	Update price data
	#	Return			:	None
	#	Author			:	Adnan Sarela
	#	Creation Date	:	24-Dec-2005
	#----------------------------------------------------------------------------------------------------
    function Update($POST)
    {
		global $db;

		$this->_truncate($this->Data['TableName']);
		$this->_truncate($this->Data['TableGrid']);

/*		print "<pre>";
		print_r($POST);
		print "</pre>";
		print "<br>".$this->Data['TableGrid'];
		die;
*/
		if(is_array($POST['passenger_starting_range']))
		{
			$objType	= new TripType();
			$objDest	= new Destination();
	
			$rsType	= $objType->getAll(" ORDER BY triptype_id");
			$rsDest	= $objDest->getAdminAll(" ORDER BY dest_id");

//			$dbPsg	= $objDest->getAllPassenger();
			
			$rsPsg	= $POST['passenger_starting_range'];

			for($i=0; $i<count($rsPsg); $i++)
			{	
	//print "i=".$i."<br>";
				$sql	= "INSERT INTO " .$this->Data['TableName']
						. " (passenger_starting_range, passenger_ending_range)"
						. " VALUES ('".$POST['passenger_starting_range'][$i]."', '".$POST['passenger_ending_range'][$i]."')";

//print $sql."<br>";
//print "passenger_id=".$rsPsg[$i]['passenger_id']."<br>";

				if(DEBUG)
					$this->__debugMessage($sql);

				$db->query($sql);

				for($j=0; $j < count($rsDest); $j++)
				{
					for($k=0; $k < count($rsType); $k++)
					{
						$price = $POST["price_" . ($i+1) ."_". $rsDest[$j]['dest_id'] ."_". $rsType[$k]['triptype_id']];

						$sql	= "INSERT INTO " .$this->Data['TableGrid']
								. " (passenger_id, dest_id, triptype_id, rate)"
								. " VALUES ('" .($i+1). "','" .$rsDest[$j]['dest_id']. "','" .$rsType[$k]['triptype_id']. "','" .$price. "')";

//						print $sql."<br>";					
						
						if(DEBUG)
							$this->__debugMessage($sql);
						
						$db->query($sql);
					}
				}
			}

		}

		return;
	}

	#====================================================================================================
	#	Function Name	:   Insert
	#	Purpose			:	insert price data
	#	Return			:	Inserted Id
	#	Author			:	Adnan Sarela
	#	Creation Date	:	24-Dec-2005
	#----------------------------------------------------------------------------------------------------
    function Insert($POST)
    {
/*		print "<pre>";
		print_r($POST);
		print "</pre>";
		die;
*/	
		global $db;

		$objType	= new TripType();
		$objDest	= new Destination();
		
		$rsType	= $objType->getAll(" ORDER BY triptype_id");
		$rsDest	= $objDest->getAll(" ORDER BY dest_id");

		$sql	= "INSERT INTO " .$this->Data['TableName']
				. " (passenger_starting_range, passenger_ending_range)"
				. " VALUES ('".$POST['passenger_start']."', '".$POST['passenger_end']."')";

		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		$id = $db->sql_inserted_id();
		
		for($j=0; $j < count($rsDest); $j++)
		{
		
			for($k=0; $k < count($rsType); $k++)
			{
				$price = $POST["price_". $rsDest[$j]['dest_id'] ."_". $rsType[$k]['triptype_id']];
				
				$sql	= "INSERT INTO " .$this->Data['TableGrid']
						. " (passenger_id, dest_id, triptype_id, rate)"
						. " VALUES ('" . $id . "','" .$rsDest[$j]['dest_id']. "','" .$rsType[$k]['triptype_id']. "','" .$price. "')";
			
				if(DEBUG)
					$this->__debugMessage($sql);
				
				$db->query($sql);
			}
			
		}

		return($id);
	}

	#====================================================================================================
	#	Function Name	:   Delete
	#	Purpose			:	Delete price data
	#	Return			:	affected rows
	#	Author			:	Adnan Sarela
	#	Creation Date	:	24-Dec-2005
	#----------------------------------------------------------------------------------------------------
    function Delete($pk)
    {
		global $db;
		
		$sql	= "SELECT * FROM " .$this->Data['TableName']
				. "	WHERE passenger_id='" .$pk. "'";

		if(DEBUG)
			$this->__debugMessage($sql);
		$db->query($sql);
		$rsRate = $db->fetch_array(MYSQL_FETCH_SINGLE);
		
		if($rsRate)
		{
			$sql	= "UPDATE " .$this->Data['TableName']
					. " SET passenger_starting_range = " .$rsRate['passenger_starting_range']
					. "	WHERE passenger_id='" .($rsRate['passenger_id'] + 1). "'";
			
			if(DEBUG)
				$this->__debugMessage($sql);
			$db->query($sql);
		}
		
		$sql	= "DELETE FROM " .$this->Data['TableName']
				. "	WHERE passenger_id='" .$pk. "'";

		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		
		$sql	= "DELETE FROM " .$this->Data['TableGrid']
				. "	WHERE passenger_id='" .$pk. "'";

		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		
		return($db->affected_rows());
	}

	#====================================================================================================
	#	Function Name	:   _truncate
	#	Purpose			:	empty table
	#	Return			:	affected rows
	#	Author			:	Adnan Sarela
	#	Creation Date	:	24-Dec-2005
	#----------------------------------------------------------------------------------------------------
    function _truncate($tbl)
    {
		global $db;
		
		if (!$tbl)
			return;
			
		$sql	= "TRUNCATE TABLE " .$tbl;

		if(DEBUG)
			$this->__debugMessage($sql);
		
		return($db->query($sql));
	}

	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Adnan Sarela
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#	Author			:	Adnan Sarela
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}
}
?>