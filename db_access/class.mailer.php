<?php

#====================================================================================================
# File Name : class mailer.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contains all application configuration details
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 
class clsMail
{
    var $From  		= "";
    var $FromName   = "";
    var $To			= "";
    var $CC   = "";
    var $BCC   = "";
    var $ToName   = "";
    var $Subject   = "";
    var $Text   = "";
    var $Html   = "";
	var	$AttmFiles = array();
	
	function SendMail(){

	$OB="----=_OuterBoundary_000";
	$IB="----=_InnerBoundery_001";
	$Html=$this->Html?$this->Html:preg_replace("/\n/","{br}",$this->Text)
	 or die("neither text nor html part present.");
	$Text=$this->Text?$this->Text:"Sorry, but you need an html mailer to read this mail.";
	$this->From or die("sender address missing");
	$this->To or die("recipient address missing");

	$headers ="MIME-Version: 1.0\r\n";
	$headers.="From: ".$this->FromName." <".$this->From.">\n";
	$headers.="To: ".$this->To." <".$this->ToName.">\n";
	$headers.="Cc: ".$this->CC."\r\n";
	$headers.="Bcc: ".$this->BCC."\r\n";
	$headers.="Reply-To: ".$this->FromName." <".$this->From.">\n";
	$headers.="X-Priority: 1\n";
	$headers.="X-MSMail-Priority: High\n";
	$headers.="X-Mailer: My PHP Mailer\n";
	$headers.="Content-Type: multipart/mixed;\n\tboundary=\"".$OB."\"\n";
	//Messages start with text/html alternatives in OB
	$Msg ="This is a multi-part message in MIME format.\n";
	$Msg.="\n--".$OB."\n";
	$Msg.="Content-Type: multipart/alternative;\n\tboundary=\"".$IB."\"\n\n";
	//plaintext section
	$Msg.="\n--".$IB."\n";
	$Msg.="Content-Type: text/plain;\n\tcharset=\"iso-8859-1\"\n";
	$Msg.="Content-Transfer-Encoding: quoted-printable\n\n";
	// plaintext goes here
	$Msg.=$Text."\n\n";
	// html section
	$Msg.="\n--".$IB."\n";
	$Msg.="Content-Type: text/html;\n\tcharset=\"iso-8859-1\"\n";
	$Msg.="Content-Transfer-Encoding: base64\n\n";
	// html goes here
	$Msg.=chunk_split(base64_encode($Html))."\n\n";
	// end of IB
	$Msg.="\n--".$IB."--\n";
	// attachments
	if($this->AttmFiles){
	 foreach($this->AttmFiles as $AttmFile){
	  $patharray = explode ("/", $AttmFile);
	  
	 // print $patharray[count($patharray)-1]; exit;
	  
	  $FileName=$patharray[count($patharray)-1];
	  $Msg.= "\n--".$OB."\n";
	  $Msg.="Content-Type: application/octetstream;\n\tname=\"".$FileName."\"\n";
	  $Msg.="Content-Transfer-Encoding: base64\n";
	  $Msg.="Content-Disposition: attachment;\n\tfilename=\"".$FileName."\"\n\n";
	  //file goes here
	  $fd=fopen ($AttmFile, "r") or die("fopen failed");
	  $FileContent=fread($fd,filesize($AttmFile));
	  fclose ($fd);
	  $FileContent=chunk_split(base64_encode($FileContent));
	  $Msg.=$FileContent;
	  $Msg.="\n\n";
	  
	 }
	}

	//message ends
	$Msg.="\n--".$OB."--\n";
	//print $Msg;
	if(mail($this->To,$this->Subject,$Msg,$headers))
		return true;
	else
		return false;
	//syslog(LOG_INFO,"Mail: Message sent to $ToName <$To>");
	}

}
?>