<?php
#====================================================================================================
# File Name : Admin.php 
#----------------------------------------------------------------------------------------------------
# Purpose : This file contains all application configuration details
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#====================================================================================================

# Table Constant
/*define('SUBADMIN_MASTER',			$config['Table_Prefix']. 'subadmin_master');*/

# Include Privilege data
require_once(dirname(__FILE__) . '/AdminPrivilege.php');

class Admin extends User
{
	var $FirstName		=	'';
	var $LastName		=	'';
	var $Email			=	'';
	var $Phone			=	'';
	var $Privileges		=	'';
	var $PrivilegesList	=	'';

	#====================================================================================================
	#	Function Name	:   Admin
	#	Purpose			:	Constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function Admin($admin_auth_id='')
    {
		global $db;
		global $lang;

		if($admin_auth_id == '') return;
				
		$rs = $this->getAdmin($admin_auth_id);		
		
		$this->FirstName		=	$db->f('admin_firstname');
		$this->LastName			=	$db->f('admin_lastname');
		$this->Email			=	$db->f('admin_email');
		$this->Phone			=	$db->f('admin_phone');
		$this->PrivilegesList	=	$lang['AdminPrivileges'];

		if($db->f('admin_default'))
			$this->Privileges	=	$lang['AdminPrivileges'];
		else
			$this->Privileges	=	$this->DecryptPrivilege($db->f('admin_privileges'));
	}

	#====================================================================================================
	#	Function Name	:   ViewAll
	#	Purpose			:	Provide list of admin information
	#	Return			:	return recordset with admin info
	#----------------------------------------------------------------------------------------------------
    function ViewAll()
    {
		global $db;

		# Get all admin
		$sql	=	"SELECT * "
				.	" FROM ". SUBADMIN_MASTER
			 	. 	" 	LEFT JOIN ". AUTH_USER. " ON ". AUTH_USER. ".user_auth_id = ". SUBADMIN_MASTER. ".admin_auth_id "
				.	" ORDER BY admin_firstname ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object());
	}

	#====================================================================================================
	#	Function Name	:   getAdmin
	#	Purpose			:	get the admin information
	#	Return			:	return recordset with admin info
	#----------------------------------------------------------------------------------------------------
    function getAdmin($admin_auth_id)
    {
		global $db;

		$sql	=	"SELECT * "
				.	" FROM ". SUBADMIN_MASTER
			 	. 	" 	LEFT JOIN ". AUTH_USER. " ON ". AUTH_USER. ".user_auth_id = ". SUBADMIN_MASTER. ".admin_auth_id "
				.	" WHERE admin_auth_id = '". $admin_auth_id. "' ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);

		return ($db->next_record());
	}

	#====================================================================================================
	#	Function Name	:   Insert
	#	Purpose			:	Insert the new admin
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function Insert($user_login_id,		$user_password,
					$admin_firstname,	$admin_lastname,
					$admin_phone,		$admin_email,
					$admin_privileges,	$privilegeKeyField)
	{
		global $db;

		# Call the base method and save login information
		$admin_auth_id = parent::InsertLogin($user_login_id, $user_password, ADMIN, YES);

		# Save admin info
		$sql = 	"INSERT INTO ". SUBADMIN_MASTER. " (admin_auth_id,							"
			.	" 									admin_firstname,	admin_lastname,	"
			.	" 									admin_phone,		admin_email,		"
			.	" 									admin_privileges)						"
			.	" VALUES ( "
			. 	" '".	$admin_auth_id.	"', " 
			.	" '". 	$admin_firstname.	"', "
			. 	" '".	$admin_lastname.	"', " 
			. 	" '".	$admin_phone.		"', " 
			. 	" '".	$admin_email.		"', " 
			. 	" '".	$this->GrabePrivilege($admin_privileges, $privilegeKeyField).	"' " 
			.	" )"; 

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);

		return $db->sql_inserted_id();
	}

	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	Update the admin title
	#	Return			:	return update status
	#----------------------------------------------------------------------------------------------------
	function Update($admin_auth_id,		$user_password,
					$admin_firstname,	$admin_lastname,
					$admin_phone,		$admin_email,
					$admin_privileges,	$privilegeKeyField)
	{
		global $db;

		# Call the base method and save login information
		if($user_password != '')
			$ret = parent::UpdateLogin($admin_auth_id, $user_password);

		$sql = 	"UPDATE ". SUBADMIN_MASTER
			.	" SET "
			.	"	admin_firstname		= '". $admin_firstname. "', "
			.	"	admin_lastname		= '". $admin_lastname. "', "
			.	"	admin_phone			= '". $admin_phone. "', "
			.	"	admin_email			= '". $admin_email. "', "
			.	"	admin_privileges	= '". $this->GrabePrivilege($admin_privileges, $privilegeKeyField). "' "
			.	" WHERE admin_auth_id 	= '". $admin_auth_id. "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}

	#====================================================================================================
	#	Function Name	:   UpdateProfile
	#	Purpose			:	Update the admin title
	#	Return			:	return update status
	#----------------------------------------------------------------------------------------------------
	function UpdateProfile($admin_auth_id,
							$admin_firstname,	$admin_lastname,
							$admin_phone,		$admin_email)
	{
		global $db;

		$sql = 	"UPDATE ". SUBADMIN_MASTER
			.	" SET "
			.	"	admin_firstname		= '". $admin_firstname. "', "
			.	"	admin_lastname		= '". $admin_lastname. "', "
			.	"	admin_phone			= '". $admin_phone. "', "
			.	"	admin_email			= '". $admin_email. "' "
			.	" WHERE admin_auth_id 	= '". $admin_auth_id. "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}

	#====================================================================================================
	#	Function Name	:   Delete
	#	Purpose			:	Delete the admin information
	#	Return			:	return delete status
	#----------------------------------------------------------------------------------------------------
	function Delete($admin_auth_id)
	{
		global $db;
		
		if(!$admin_auth_id)
			return false;

		if(is_array($admin_auth_id))
			$admin_list = implode("','", $admin_auth_id);
		else
			$admin_list = $admin_auth_id;

		# Call the base method and delete login information
		$ret = parent::DeleteLogin($admin_list);

		$sql = 	"DELETE FROM ". SUBADMIN_MASTER
			.	" WHERE admin_auth_id IN ('". $admin_list. "') ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		# Execute Query
		$db->query($sql);

		return $db->affected_rows();
	}

	#====================================================================================================
	#	Function Name	:   DecryptPrivilege
	#	Purpose			:	Decrypt privilege string
	#	Return			:	Privilege array
	#----------------------------------------------------------------------------------------------------
	function GrabePrivilege($arrPrivilege, $postField)
	{
		$strPrivilege 	=	'';
		
		if(!is_array($arrPrivilege)) return false;

		foreach($arrPrivilege as $key => $val)
			if(is_array($_POST[$postField. $val]))
				$strPrivilege .= $val. ':'. implode(',', $_POST[$postField. $val]). '|';

		# Show debug info
		if(DEBUG)
			$this->__debugMessage(substr($strPrivilege, 0, strlen($strPrivilege)-1));

		return substr($strPrivilege, 0, strlen($strPrivilege)-1);
	}

	#====================================================================================================
	#	Function Name	:   DecryptPrivilege
	#	Purpose			:	Decrypt privilege string
	#	Return			:	Privilege array
	#----------------------------------------------------------------------------------------------------
	function DecryptPrivilege($strPrivilege)
	{
		$arrMain	=	'';
		$arrDecrypt = 	array();

		$arrMain = explode('|',$strPrivilege);

		foreach($arrMain as $key => $val)
		{
			$arrSub = explode(':',$val);
			$arrDecrypt[$arrSub[0]]	=	explode(',',$arrSub[1]);
		}

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($arrDecrypt);
		
		return $arrDecrypt;
	}

	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}
}
?>