<?php
#====================================================================================================
# File Name : UserMaster.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 
# Page Status
define('USER_VISIBLE',	'Visible');
define('USER_HIDDEN',	'Hidden');

# Page Type
$arrPageType	=	array(	'SimplePage'	=>	'Simple Page',
							'InternalLink'	=>	'Internal Link',
					);
# Page Class
class UserMaster
{
   	#====================================================================================================
	#	Function Name	:   Page
	#	Purpose			:	Constructor
	#	Return			:	None
	#----------------------------------------------------------------------------------------------------
    function UserMaster()
    {
		// Do nothing
	}

	#====================================================================================================
	#	Function Name	:   ViewAll
	#	Purpose			:	Provide list of destination information
	#	Return			:	return recordset with destination info
	#----------------------------------------------------------------------------------------------------
    function ViewAll($destination_status='',$lang='')
    {
		global $db;
		$lang = $_SESSION['lng'];
		
		$sql	=	"SELECT * FROM " . USER_MASTER
				.	" LEFT JOIN " . AUTH_USER . " ON  user_auth_id = auth_id "
				.	" WHERE dest_langcode = '" .$lang. "'"
				. ($destination_status ? " AND destination_status = '". $destination_status."'" : '');
			 
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object());
	}
	
	
	#====================================================================================================
	#	Function Name	:   ViewAllUsers
	#	Purpose			:	Provide list of User information
	#	Return			:	return recordset with language info
	#----------------------------------------------------------------------------------------------------
    function ViewAllUsers()
    {
		global $db;

		$sql = " SELECT * FROM ".USER_MASTER
			 . " ORDER BY index_id ";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_object());
	}

	#====================================================================================================
	#	Function Name	:   ViewUsers
	#	Purpose			:	Provide list of User information
	#	Return			:	return recordset with language info
	#----------------------------------------------------------------------------------------------------
    function ViewUsers($start_record,$Page_Size)
    {
		global $db;

		$sql = " SELECT * FROM ".USER_MASTER
			 . " ORDER BY ".USER_MASTER.".confirmationNo DESC"
			 . " LIMIT ". $start_record . ", ". $Page_Size ;

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   UsersRecords
	#	Purpose			:	Provide list of User information
	#	Return			:	return recordset with language info
	#----------------------------------------------------------------------------------------------------
    function UsersRecords()
    {
		global $db;

		$sql = " SELECT * FROM ".USER_MASTER
			 . " ORDER BY ".USER_MASTER.".confirmationNo DESC";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
		$num_records = $db->num_rows();		

		return ($num_records);
	}

	#====================================================================================================
	#	Function Name	:   ShowUserInfo
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowUserInfo($index_id)
    {
		global $db;
				
		$sql = " SELECT * FROM ".USER_MASTER
			 . " LEFT JOIN ". DESTINATION_DISCOUNT. " ON ". USER_MASTER. ".discount_id = ". DESTINATION_DISCOUNT. ".discount_id "					
			 . " WHERE ".USER_MASTER.". index_id  =  '". $index_id ."'";

	//print "sql=".$sql;
		$rs=$db->query($sql);

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   ShowUserInfoByConfNo
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowUserInfoByConfNo($confirmationNo)
    {
		global $db;
				
		$sql = " SELECT * FROM ".USER_MASTER
			 . " LEFT JOIN ". DESTINATION_DISCOUNT. " ON ". USER_MASTER. ".discount_id = ". DESTINATION_DISCOUNT. ".discount_id "					
			 . " WHERE ".USER_MASTER.". confirmationNo  =  '". $confirmationNo ."'";

		$rs=$db->query($sql);

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   ShowUserDisc
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowUserDisc($user_id)
    {
		global $db;
				
		$sql = " SELECT * FROM ".USER_MASTER
			 . " LEFT JOIN ". DESTINATION_DISCOUNT. " ON ". USER_MASTER. ".discount_id = ". DESTINATION_DISCOUNT. ".discount_id "							  
			 . " WHERE ".USER_MASTER.".user_id = '".$user_id."'"; 

		$rs=$db->query($sql);

		return($rs);
	}


	#====================================================================================================
	#	Function Name	:   getUser
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function getUser($user_id)
    {
		global $db;
				
		$sql = " SELECT * FROM ".USER_MASTER
				. " WHERE ".USER_MASTER.". user_id  =  '". $user_id  ."'";
				
		$db->query($sql);
		$rs = $db->fetch_array();

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   Insert
	#	Purpose			:	Insert the new destination
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function InsertUser($is_airline_empl,	$discount_id,	$empl_id,	$empl_airline,	$firstname,	$lastname,	
						$address1,			$address2,		$city,		$state,			$country,	$zip,	
						$phoneno,			$email,			$password)
	{
		global $db;
		
		# Save Page info
		$sql = "INSERT INTO ".USER_MASTER." (is_airline_empl,	discount_id,	empl_id,	empl_airline,	firstname,
											lastname,			address1,		address2,	city,			state,
											country,			zip,			phoneno,	email) "
			 . " VALUES ( "
			 . " '". $is_airline_empl ."' , "
			 . " '". $discount_id ."' , "
			 . " '". addslashes($empl_id) ."' , "
			 . " '". addslashes($empl_airline) ."' , "
			 . " '". addslashes($firstname) ."' , "
			 . " '". addslashes($lastname) ."' , "
			 . " '". addslashes($address1) ."' , "
			 . " '". addslashes($address2) ."' , "
			 . " '". addslashes($city) ."', "
			 . " '". addslashes($state) ."' , "
			 . " '". addslashes($country) ."' , "
			 . " '". addslashes($zipcode) ."' , "
			 . " '". addslashes($phoneno) ."' , "
			 . " '". addslashes($email) ."' ) ";
			 
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);

		$userId = $db->sql_inserted_id();
//		$password = rand_pass(6);


		$email = $_POST['email'];
		$confirmation_no = date("YmdHi").$userId;
	

		$sql = "INSERT INTO ".AUTH_USER." (user_auth_id,	user_login_id,	user_password,	user_perm) "
				 . " VALUES ( "
				 . " '". md5($confirmation_no) ."' , "
				 . " '". $confirmation_no ."' , "
				 . " '". md5($password) ."' , "
				 . " '". USER ."' ) " ;
				 
		$db->query($sql);
		$authId = $db->sql_inserted_id();
		$db->free();

		$sql = " UPDATE ".USER_MASTER." SET "
			. " user_id					=  '" .md5($confirmation_no)."', "
			. " auth_id					=  '" .$authId."', "
			. " confirmationNo			=   '".$confirmation_no."' ";
		$sql .= " WHERE  ". USER_MASTER .".index_id  = '".$userId."'"; 
	
		 if(DEBUG)
			$this->__debugMessage($sql);
			
		 $db->query($sql);
	
		return $userId;
	}


	#====================================================================================================
	#	Function Name	:   UpdatetUserDetails
	#	Purpose			:	Insert the new destination
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function UpdatetUserDetails($index_id,	$firstname,	$lastname,	$address1,	$address2,	$city,	$state,			
								$country,	$zip,		$phoneno,	$email,		$password,	$cart_id,	
								$cart_destId,$total)
	{
		global $db;

		# Save Page info
		if($index_id)
		{	
			$sql = " UPDATE ".USER_MASTER." SET "
				. " firstname		=  '" .$firstname."', "
				. " lastname		=  '" .$lastname."', "
				. " address1		=  '" .$address1."', "
				. " address2		=  '" .$address2."', "
				. " city			=  '" .$city."', "
				. " state			=  '" .$state."', "
				. " country			=  '" .$country."', "
				. " zip				=  '" .$zip."', "
				. " phoneno			=  '" .$phoneno."', "
				. " email			=  '" .$email."' ";
			$sql.= " WHERE  ". USER_MASTER .".index_id  = '".$index_id."'"; 
		}
		else
		{
			$sql = "INSERT INTO ".USER_MASTER." (firstname,	lastname,	address1,	address2,	city,
												state,		country,	zip,		phoneno,	email) "
				 . " VALUES ( "
				 . " '". addslashes($firstname) ."' , "
				 . " '". addslashes($lastname) ."' , "
				 . " '". addslashes($address1) ."' , "
				 . " '". addslashes($address2) ."' , "
				 . " '". addslashes($city) ."', "
				 . " '". addslashes($state) ."' , "
				 . " '". addslashes($country) ."' , "
				 . " '". $zip ."' , "
				 . " '". $phoneno ."' , "
				 . " '". addslashes($email) ."' ) ";
		}
		

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);


		if($index_id)
		{
			$userId = $index_id;		
		}
		else
		{
			$userId = $db->sql_inserted_id();
		}

		$email = $_POST['email'];
		$confirmation_no = date("YmdHi").$userId;

		$sql = "INSERT INTO ".AUTH_USER." (user_auth_id,	user_login_id,	user_password,	user_perm) "
				 . " VALUES ( "
				 . " '". md5($confirmation_no) ."' , "
				 . " '". $confirmation_no ."' , "
				 . " '". md5($password) ."' , "
				 . " '". USER ."' ) " ;
				 
		$db->query($sql);
		$authId = $db->sql_inserted_id();


		$sql = " UPDATE ".USER_MASTER." SET "
			. " user_id					=  '" .md5($confirmation_no)."', "
			. " auth_id					=  '" .$authId."', "
			. " confirmationNo			=   '".$confirmation_no."' ";
		$sql .= " WHERE  ". USER_MASTER .".index_id  = '".$userId."'"; 
	
		 if(DEBUG)
			$this->__debugMessage($sql);
			
		 $db->query($sql);

		return $userId;
	}



	#====================================================================================================
	#	Function Name	:   Insert
	#	Purpose			:	Insert the new destination
	#	Return			:	return insert status
	#----------------------------------------------------------------------------------------------------
	function InsertAirlineUser($discount_id,	$empl_id,	$empl_airline)
	{
		global $db;
		
		# Save Page info
		$sql = "INSERT INTO ".USER_MASTER." (is_airline_empl,	discount_id,	empl_id,	empl_airline) "
			 . " VALUES ( "
			 . " '". 1 ."' , "
			 . " '". $discount_id ."' , "
			 . " '". addslashes($empl_id) ."' , "
			 . " '". addslashes($empl_airline) ."' ) ";
			 
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
			
		# Execute Query
		$db->query($sql);

		$userId = $db->sql_inserted_id();

		return $userId;
	}

	#====================================================================================================
	#	Function Name	:   Update
	#	Purpose			:	get the destination information
	#	Return			:	return recordset with destination info
	#----------------------------------------------------------------------------------------------------
    function Update($index_id ,$dest_title)
    {
		//echo "in update";
		global $db, $LangInfo;
				
		/*$sql = " UPDATE ".USER_MASTER
			 . " SET "
			 . " 	destination_parent_id 		=  '". $destination_parent_id ."', "
			 . " 	destination_url 			=  '". $destination_url ."', "			 
			 . " WHERE  index_id  =  '". $index_id  ."'";


		$db->query($sql);*/

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		for( $i=0; $i<count($LangInfo); $i++)
		{
			$sql = 	"REPLACE INTO ". DESTINATION_DETAILS
				. 	"(dest_master_id ,dest_langcode ,dest_title)"
				.	" VALUES ( "
				. 	" ".	$index_id 	.", " 
				. 	" '".	$LangInfo[$i]['code']	."', " 				
				. 	" '".	$dest_title[$i]	."' " //test
				.	" )";
			
			$db->query($sql);
		}

		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
		return;
		
	}


	#====================================================================================================
	#	Function Name	:   UpdateUser
	#	Purpose			:	Upadate the users information
	#	Return			:	return recordset with users info
	#----------------------------------------------------------------------------------------------------
    function UpdateUser($confirmation_no, $authId, $userId)
    {
		//echo "in update";
		global $db;
				

	  $sql = " UPDATE ".USER_MASTER." SET "
			. " user_id					=  '" .md5($confirmation_no)."', "
			. " auth_id					=  '" .$authId."', "
			. " confirmationNo			=   '".$confirmation_no."' ";
	  $sql .= " WHERE  ". USER_MASTER .".index_id  = '".$userId."'"; 

	 if(DEBUG)
		$this->__debugMessage($sql);
		
	 $db->query($sql);
	 $db->free();
		
	 return;
		
	}

	#====================================================================================================
	#	Function Name	:   Delete
	#	Purpose			:	get the destination information
	#	Return			:	return recordset with destination info
	#----------------------------------------------------------------------------------------------------
    function Delete($index_id )
    {
		global $db;
				
		$sql = " DELETE FROM ".USER_MASTER
			 . " WHERE  index_id IN (".$index_id.")";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);
			
		$sql = " DELETE FROM ".DESTINATION_DETAILS
			 . " WHERE  dest_master_id IN (".$index_id.")";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);	

		return ($db->query($sql));
	}
	
		#====================================================================================================
	#	Function Name	:   Sort
	#	Purpose			:	Update the destination order
	#	Return			:	update status
	#----------------------------------------------------------------------------------------------------
    function ToggleStatus($index_id , $dest_status)
    {
		global $db;
		
		$sql = " UPDATE ".USER_MASTER
			 . " SET "
			 . " 	dest_status =  '". $dest_status. "' "
			 . " WHERE  index_id  	=  '". $index_id . "'";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		return ($db->query($sql));
	}

	#====================================================================================================
	#	Function Name	:   getKeyValueArray
	#	Purpose			:	
	#	Return			:	key => value
	#----------------------------------------------------------------------------------------------------
	function getKeyValueArray()
	{
		global $db;
		
		$sql	=	" SELECT  index_id , destination_title FROM " . USER_MASTER
				.	" LEFT JOIN " . DESTINATION_DETAILS . " ON  index_id =dest_master_id "
				.	" WHERE lang_code = 'en'"
				.	" AND destination_parent_id = '0'"				
				. 	" ORDER BY destination_order ";
		
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		$arr	= array();
		foreach ($db->fetch_array() as $destination)
		{
			$arr[$destination[' index_id ']] = $destination['destination_title'];
		}
		
		return ($arr);
	}

	#====================================================================================================
	#	Function Name	:   getKeyValueArray
	#	Purpose			:	
	#	Return			:	key => value
	#----------------------------------------------------------------------------------------------------
	function updateMeta($site_title, $meta_title, $meta_keyword, $meta_desc)
	{
		global $db;
		
		$sql	= " UPDATE ". USER_MASTER . " SET"
				. " destination_brower_title 	=  '". $destination_site_title ."', "
				. " destination_metatitle 		=  '". $meta_title ."', "			 
			 	. " destination_metakeyword 	=  '". meta_keyword ."'"
			 	. " destination_metadesc		=  '". meta_desc ."'";
				
		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
							
		$db->query($sql);
								
		return($db->affected_rows());
	}

	#====================================================================================================
	#	Function Name	:   getAll
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getAll($addParameters='')
    {
		global $db;

		$sql	=	"SELECT * "
				.	" FROM ". USER_MASTER
				.	" LEFT JOIN " . DESTINATION_DETAILS . " ON  index_id =	dest_master_id "
				. " WHERE 1 "
				. ($addParameters != ''? $addParameters :'');

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);
		
		$db->query($sql);
		
		return ($db->fetch_array());
	}

	#====================================================================================================
	#	Function Name	:   getAllPassenger
	#	Purpose			:	Provide list of information
	#	Return			:	return recordset with info
	#	Author			:	Dinesh Sailor
	#	Creation Date	:	19-Apr-2005
	#----------------------------------------------------------------------------------------------------
    function getAllPassenger()
    {
		global $db;

		$sql	=	"SELECT * FROM " . PASSENGER_MASTER
				.	" WHERE 1";

		# Show debug info
		if(DEBUG)
			$this->__debugMessage($sql);

		$db->query($sql);

		return ($db->fetch_array());
	}
	
	#====================================================================================================
	#	Function Name	:   ShowUserMaster
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowUserMaster($confirmationNo)
    {
		global $db;

		$sql = " SELECT * FROM ".USER_MASTER
			   . " LEFT JOIN ". AUTH_USER. " ON ". USER_MASTER. ".user_id = ". AUTH_USER. ".user_auth_id "	
			   . " LEFT JOIN ". CART_MASTER. " ON ". USER_MASTER. ".user_id = ". CART_MASTER. ".user_id "	
			   . " WHERE " .AUTH_USER.".user_auth_id  = '".$confirmationNo."' AND ".AUTH_USER.".user_perm = '". USER. "' AND ".CART_MASTER.".cartStatus='".AR."'";

		$rs = $db->query($sql);

		return($rs);
	}
	
	#====================================================================================================
	#	Function Name	:   ShowUserMaster
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowUserMaster2($User_Id)
    {
		global $db1;
		
		$sql = " SELECT * FROM ".USER_MASTER
			   . " LEFT JOIN ". AUTH_USER. " ON ". USER_MASTER. ".user_id = ". AUTH_USER. ".user_auth_id "	
			   . " LEFT JOIN ". CART_MASTER. " ON ". USER_MASTER. ".confirmationNo = ". CART_MASTER. ".confirmationNo "	
			   . " WHERE " .CART_MASTER.".user_id  = '".$User_Id."' AND ".AUTH_USER.".user_perm = '". USER. "' AND ".CART_MASTER.".cartStatus='".AR."'";			   

		$rs = $db1->query($sql);

		return($rs);
	}	

	#====================================================================================================
	#	Function Name	:   ShowAuthUserMaster
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowAuthUserMaster($auth_id)
    {
		global $db;

		$sql =	"SELECT * FROM ".USER_MASTER 
			  . " WHERE " .USER_MASTER.".auth_id  = '".$auth_id."' "
			  . " ORDER BY ". USER_MASTER .".auth_id ";

		$rs = $db->query($sql);

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   ShowUser
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowUser($user_id)
    {
		global $db;

		$sql = " SELECT * FROM ".USER_MASTER
			   . " LEFT JOIN ". COUNTRY_MASTER. " ON ". USER_MASTER. ".country = ". COUNTRY_MASTER. ".country_iso_2"	
			   . " WHERE " .USER_MASTER.".index_id = '". $user_id ."'" ;
			   
		$rs = $db->query($sql);

		return($rs);
	}


	#====================================================================================================
	#	Function Name	:   ShowConfrmAuthUser
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowConfrmAuthUser($confirmationNo)
    {
		global $db;

		$sql = " SELECT * FROM ".USER_MASTER
			   . " LEFT JOIN ". AUTH_USER. " ON ". USER_MASTER. ".user_id = ". AUTH_USER. ".user_auth_id "	
 	  	       . " WHERE " .AUTH_USER.".user_login_id  = '".$confirmationNo."' AND ".AUTH_USER.".user_perm = '". USER. "'";

		$rs = $db->query($sql);

		return($rs);
	}

	#====================================================================================================
	#	Function Name	:   ShowUserById
	#	Purpose			:	get the User information
	#	Return			:	return recordset with User info
	#----------------------------------------------------------------------------------------------------
    function ShowUserById($user_id)
    {
		global $db;

		$sql = " SELECT * FROM ".USER_MASTER
			   . " WHERE ". USER_MASTER.".user_id = '".$user_id. "'";
			   
		$rs = $db->query($sql);

		return($rs);
	}

	
	#====================================================================================================
	#	Function Name	:   __errorAlert
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __errorAlert( $message )
	{
		print( '<br>'. $message .'<br>'."\r\n");
	}
	
	#====================================================================================================
	#	Function Name	:   __debugMessage
	#	Purpose			:	display custom error message
	#	Return			:	Nothing
	#----------------------------------------------------------------------------------------------------
	function __debugMessage($message)
	{
		printf("%s<br>%s<br>", $message, str_repeat("-=", 65));
	}	
}
?>