<?php
#====================================================================================================
# File Name : login.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contains all application configuration details
# Author 	: PIMSA.COM 
# Copyright : Copyright  2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#==================================================================================================== 
define('IN_SITE', 	true);
define('RIGHT_PANEL', 	true);

# include the required file
include_once("includes/common.php");
include_once($physical_path['DB_Access']. 'User.php');

# Initialize object with required module
$objAuthUser= new User();	

if($_POST['Submit']== LOGIN && $_POST['Action'] == CHECK)
{
	$sql = $objAuthUser -> ShowAuthUser(md5($_POST['confirmationNo']));

	if($db->num_rows() > 0)
	{
		$db->next_record();

		if($db->f('user_password') == md5($_POST['password']))
		{
			$_SESSION['User_Id'] = md5($_POST['confirmationNo']);
			$_SESSION['User_Perm'] = $db->f('user_perm');

				if(isset($_SESSION['User_Id']))
				{
					$checkUser = updateCartUser();	
				}

			header("location: ".$url_path['Site_URL']."usercart.php?&confirmationNo=".$_POST['confirmationNo']);
		}
		else
		{
			$Error_Message = $error['incorrectValue'];
		}
	}
	else
	{
		$Error_Message = $error['incorrectValue'];
	}
}

	$tpl->assign(array( 'T_Body'		=>	'login'.$config['tplEx'],
						'JavaScript'	=>	array('login.js'),
						"banner"    	=>  "Login",
						"Header"		=> 	"Resv_Rates",
						));

	$tpl->assign( array(	"A_Login"				=>  "login.php",
							"ACTION"				=>  CHECK,
							"Login_Msg"             =>  $lang['Login_Msg'],
							"loginImage"            =>  $lang['loginImage'],
							"LoginHere"				=>  $lang['Login_Here'],
							"Error_Message"			=>	$Error_Message,
							"L_Username"			=>	$lang['User_Name'],
							"L_Password"			=>	$lang['Password'],
							"L_ConfirmationNo"  	=> $lang['L_ConfirmationNo'],
							"confirmationNo"		=>	$_POST['confirmationNo'],
							"msgEmptyConfirmationNo"=>  $lang['msgEmptyConfirmationNo'],
							"msgEmptyUserPassword"	=>	$lang['msgEmptyUserPassword'],
							"L_Login"				=> 	$lang['Login'],
							"L_Reset"				=>	$lang['Reset'],
							"L_Register_Here"		=>  $lang['L_Register_Here'],
							"L_Forgot_Password"		=>  $lang['L_Forgot_Password'],
							"User_Id"				=>  $_SESSION['User_Id'],							
							"L_Note"				=>  $lang['L_Note'],
							"Reserervation_Note_Msg"=> $lang['Reserervation_Note_Msg'],							
							));

	$tpl->display('default_layout'. $config['tplEx']);

?>
<?php include_once("analyticstracking.php") ?>
