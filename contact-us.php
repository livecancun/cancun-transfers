<!DOCTYPE html>
<html>
<head>
	<title>Contact US | Official Airport Transportation</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="cancun airport transfers, kinmont transfers contact us, cancun shuttle, cancun transfers"/>
	<meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
	<meta property="og:url" content="http://www.cancun-transfers.com/" />
	<meta property="og:title" content="Cancun Airport shuttle">
	<meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="300">
	<meta property="og:image:height" content="300">
	<meta property="og:locale" content="en_US" />
	<meta property="og:locale:alternate" content="es_ES" />
	<link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
	<link href="css/main-style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
	<div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<div class="container">
  <div class="row">
   <h2 class="font-bl1-20">Contact US</h2>
                    <p>Should you have any question or comment about our Ground Transportation Service from/to Cancun International Airport to/from Cancun, Playa del Carmen or Riviera Maya, please, fill out this form an we will get back to you as soon as possible.</p>
  <br><br>
   <form action="envio-contacto.php" class="form-horizontal" role="form" method="POST" id="form" >

    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-3 ">
    <div id="form-group">
        <label for="nombre">Name</label>
        <input type="text" name="name" class="form-control"  placeholder="Name" >
    </div></div>
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
        <label for="pais">Country</label>
        <input type="text" name="pais" class="form-control"  placeholder="Country" >
    </div></div>
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
        <label for="telefono">Phone</label>
        <input type="text" name="phone" class="form-control"  placeholder="Phone" >
    </div></div>
    <div class="col-xs-12 col-md-2 col-offset-3 ">
  <div class="form-group">
  <label for="correo" >E-Mail</label>
    <input type="text" name="email" class="form-control" placeholder="E-Mail">
  </div></div>
    <span class="clearfix"></span>
    <div class="col-xs-12 col-md-9 col-offset-3 ">
    <div id="form-group">
        <label for="comentarios">Comments</label>
        <textarea name="message" class="form-control" placeholder="comments"></textarea>
    </div></div>
    
    </div>
    <br /><br />
        <input type="submit" class="btn btn-info btn-lg" name="Submit" value="Send">
    </div>

</form>
   <br><br><br><br>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>