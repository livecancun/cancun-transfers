<!DOCTYPE html>
<html>
<head>
	<title>Cancun Airport Shuttle | Official Airport Transportation</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="cancun airport shuttle, cancun shuttle, cancun airport taxi, cancun airport transportation, cancun transportation, cancun transfers, cancun airport hotel, cancun transfers rates, shuttle cancun"/>
	<meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
	<meta property="og:url" content="http://www.cancun-transfers.com/" />
	<meta property="og:title" content="Cancun Airport shuttle">
	<meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="300">
	<meta property="og:image:height" content="300">
	<meta property="og:locale" content="en_US" />
	<meta property="og:locale:alternate" content="es_ES" />
	<link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
	<link href="css/main-style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<SCRIPT type=text/javascript 
src="http://maps.google.com/maps/api/js?sensor=false"></SCRIPT>
<SCRIPT type=text/javascript>
    function initialize() {
      var fenway = new google.maps.LatLng(21.038525,-86.864862);
      var mapOptions = {
        center: fenway,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: true
      };
      var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    }
    </SCRIPT>

</head>
<body onload=initialize()>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
	<div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>

<br><br>
<div class="container">
   <div class="row">
                <div class="col-xs-12 col-md-12">
                <p>We understand what it means to arrive in a foreign country with no clue of what to expect or what you are supposed to do or where you are supposed to go. To make your <strong>arrivals and departures transfers</strong> as easy as possible, cancun-transfers offers you NON-STOP, NO-WAITING PRIVATE airport transfers in Cancun & Riviera Maya</p><br><br>
                <div id="map_canvas" align="center" style="width: 800px; height: 250px"></div><br><br>
                <h3>Cancun International Airport Tips</h3>
<ul><li>Once you have gotten your bags and cleared immigration, go through customs and walk out the terminal where our team will be waiting for you holding up a sign with your name.</li>
    <li>It is very important to know that inside the airport building, on your way out, you will find lots of people claiming to be your transportation company or offering discounts or free activities. Be aware that those are TIMESHARE sellers.</li>
    <li>There are lots of transportation company reps outside waiting for their clients. You will not have to look further. Our KinMont host can be found straight out in front of the terminal.</li>
    <li>If by any chance our host is busy with other travelers, you can simply walk to the “Air Margaritaville” bar that is one side of the exit. He will look for you once he is available.</li>
    <li>If for some reason your flight got delayed, changed or you had to spend extra time inside the terminal due to lost luggage or any other situation, and you cannot find our host, please use the payphone located right behind the “Air Margaritaville” bar. You do not need to buy a calling card using our 01 800 727 8112 number.</li></ul>
<br /><br /><center>
<iframe src="http://www.flightstats.com/go/weblet?guid=d56928966b03ae86:-3be13f33:1197c2fe4bc:38ea&imageColor=black&language=english&weblet=status&action=AirportFlightStatus&airportCode=CUN&airportQueryType=1" frameborder="0" scrolling="auto" width=900 height=500></iframe>
</center>

</div>
  </div>
    </div>
 

 <div class="container">
  <div class="row">
    <div class="col-md-4">
      <h3>
        Cancun Hotels
      </h3>
      <img src="images/cancun-shuttle-locaciones.jpg">
        <a class="btn" href="#">View details »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Playa del Carmen
      </h3>
      <img src="images/playa-del-carmen-shuttle-locaciones.jpg">
        <a class="btn" href="#">View details »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Riviera Maya Hotels
      </h3>
      <img src="images/riviera-maya-shuttle-locaciones.jpg">
        <a class="btn" href="#">View details »</a>
      </p>
    </div>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>