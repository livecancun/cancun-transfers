<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Cancun transfers - Contac Us</title>
<meta name="description" content="Cancun transfers and private shuttles to Cancun Airport - Hotel - Cancun Airport" />
<meta name="keywords" content="Cancun tranfers, cancun shuttles, cancun airport tranfers, cancun ground transporttation" />
<meta name="robots" content="index,follow" />

<link href="css/train.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/table.css" type="text/css" media="screen"/>

<script type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>


</head>

<body onload="MM_preloadImages('images/home-hover.gif','images/about-hover.gif','images/order-hover.gif','images/contact-hover.gif','images/affiliates-hover.gif','images/newsletter-hover.gif')">
<center>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="389"><img src="images/header.gif" width="389" height="112" /></td>
        <td><table width="250" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="4">&nbsp;</td>
            <td background="images/call-us.jpg"><table width="242" border="0" cellspacing="0" cellpadding="0" height="62">
              <tr>
                <td>&nbsp;</td>
                <td class="search-text">&nbsp;</td>
              </tr>
            </table></td>
            <td width="4">&nbsp;</td>
          </tr>
        </table></td>
        <td width="20">&nbsp;</td>
      </tr>
    </table>
      <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="97"><a href="http://www.cancun-transfers.com/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Home','','images/home-hover.gif',1)"><img src="images/home.gif" alt="Home" name="Home" width="97" height="39" border="0" id="Home" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="116"><a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=en" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('About Us','','images/about-hover.gif',1)"><img src="images/about.gif" alt="Rates" name="Rates" width="116" height="39" border="0" id="About Us" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="93"><a href="arrivals.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Order','','images/order-hover.gif',1)"><img src="images/order.gif" alt="Arrivals" name="Order" width="93" height="39" border="0" id="Order" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="130"><a href="contact-us.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Contact Us','','images/contact-hover.gif',1)"><img src="images/contact.gif" alt="Contact Us" name="Contact Us" width="130" height="39" border="0" id="Contact Us" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="117"><a href="about-us.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Affiliates','','images/affiliates-hover.gif',1)"><img src="images/affiliates.gif" alt="About US" name="Affiliates" width="117" height="39" border="0" id="Affiliates" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="131"><a href="http://www.cancun-transfers.com/es/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Newsletter','','images/newsletter-hover.gif',1)"><img src="images/newsletter.gif" alt="Espa�ol" name="Newsletter" width="131" height="39" border="0" id="Newsletter" /></a></td>
          <td background="images/button-bg.gif">&nbsp;</td>
          <td width="20"><img src="images/button-right.gif" width="20" height="39" /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="782" valign="top"><img src="images/header-a.gif" width="782" height="272" hspace="0" vspace="0" /></td>
          <td valign="top"><table width="218" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img src="images/header-b.gif" width="218" height="131" /></td>
            </tr>
            <tr>
              <td height="149" background="images/login-bg.gif"><table width="218" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="user-heading">�Private Transfers! </td>
                </tr>
                <tr>
                  <td align="left"><table width="180" border="0" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="user-subheading">&nbsp;</td>
                      <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="user-subheading">&nbsp;</td>
                      <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table></td>
                </tr>
                
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="635" valign="top"><table width="635" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="grey-left">&nbsp;</td>
              <td valign="top" class="grey-center"><table width="621" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="621" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="heading-grey"><img src="images/arrow-top.gif" width="12" height="14" />Contac Us <span class="heading-red"><em>Feedback</em></span> </td>
                    </tr>
                    <tr>
                      <td class="warea"><br /><p>
                                            Should you have any question or comment about our Ground Transportation Service from/to Cancun International Airport to/from Cancun, Playa del Carmen or Riviera Maya, please, fill out this form an we will get back to you as soon as possible.
                      <br /></p>
   <br /><p>
<?php 
$nombre = $_POST['nombre'];
$mail = $_POST['mail'];
$pais = $_POST['pais'];
$telefono = $_POST['telefono'];
$empresa = $_POST['comentario'];
$tipo = $_POST['tipo'];
$hotel = $_POST['hotel'];
$vuelo = $_POST['vuelo'];
$llegadavlo = $_POST['llegadavlo'];
$salidavlo = $_POST['salidavlo'];
$pasajeros = $_POST['pasajeros'];


if ($mail ==""){

echo '<strong>Please, Check the boxes name and email are not empty</strong> <br><br>';
include ("efcontacto2.htm");

} else {
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/plain";

$mensaje = "Este mensaje fue enviado por " . $nombre. " \r\n";
$mensaje .= "Su e-mail es: " . $mail . " \r\n";
$mensaje .= "Su Direccion es: " . $pais . " \r\n";
$mensaje .= "Su telefono es: " . $telefono . " \r\n";
$mensaje .= "Mensaje: " . $_POST['comentario'] . " \r\n";
$mensaje .= "tipo reserva: " . $tipo . " \r\n";
$mensaje .= "Hotel: " . $hotel . " \r\n";
$mensaje .= "En el vuelo: " . $vuelo . " \r\n";
$mensaje .= "Horario de llegada: " . $llegadavlo . " \r\n";
$mensaje .= "Horario de salida: " . $salidavlo . " \r\n";
$mensaje .= "Pasajeros: " . $pasajeros . " \r\n";
$mensaje .= "Enviado el " . date('d/m/Y', time());

$para = 'info@cancun-transfers.com';
$asunto = 'Mensaje Cancun-transfers';

mail($para, $asunto, utf8_decode($mensaje), $header);

echo $nombre. ' , Your comment has been sent successfully, thanks,'.'<br><br><a href="http://www.cancun-transfers.com">Back to Home page</a>';
}
?></p><br />
          <br />             </td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td><img src="images/grey-line-big.gif" width="591" height="1" vspace="15" /></td>
                </tr>
                <tr>
                  <td><table width="621" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="heading-grey-bottom"><img src="images/arrow-bottom.gif" width="12" height="14" /> Why <span class="heading-red"><em>Us ?</em></span> </td>
                    </tr>
                    <tr>
                      <td class="warea">Cancun-transfers.com by KinMont, is a reliable and secure transportation company providing transportation to individuals, families and small groups. All
shuttles, vans have air conditioned, licensed and insured. All drivers are
bilingual and professional. Let us know your special needs...

Special Assistance for disabled passengers.
                        </td></tr>
                  </table></td>
                </tr>
              </table></td>
              <td class="grey-right">&nbsp;</td>
            </tr>
          </table></td>
          <td width="9">&nbsp;</td>
          <td valign="top"><table width="316" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="grey-left">&nbsp;</td>
              <td valign="top" class="grey-center"><table width="302" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top"><table width="302" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="heading-grey"><img src="images/arrow-top.gif" width="12" height="14" />Private <span class="heading-red"><em>Cancun Tranfers</em></span> </td>
                      </tr>
                      <tr>
                        <td><br /><img src="images/trav-cancun-transfers.jpg" /></td></tr>
                    </table></td>
                </tr>
                <tr>
                  <td><img src="images/grey-line-small.gif" width="275" height="1" vspace="15" /></td>
                </tr>
                <tr>
                  <td><table width="302" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="heading-grey-bottom"><img src="images/arrow-bottom.gif" width="12" height="14" /> Alliance <span class="heading-red"><em>&amp;</em></span> Partners </td>
                    </tr>
                    <tr>
                      <td><table width="302" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td ><img src="images/partners-cancun-transfers.jpg" /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
              <td class="grey-right">&nbsp;</td>
            </tr>
          </table></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <table width="1000" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td bgcolor="#F4F4F4" class="footer-heading"><a href="http://www.cancun-transfers.com/">Home</a> | <a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=en">Rates</a> | <a href="arrivals.html">Arrivals</a> | <a href="contact-us.html">Contact Us</a> | <a href="about-us.html">About Us</a> | <a href="http://www.cancun-transfers.com/es/">Espa&ntilde;ol</a></td>
        </tr>
        <tr>
          <td bgcolor="#F4F4F4" class="footer-subheading"><p>Copyright &copy; 2011 cancun-transfers.com All Rights Reserved</p>
            <a href="http://www.cancun-transfers.com">Cancun shuttle</a> | <a href="http://www.cancun-transfers.com/">Cancun transfers </a> | <a href="http://www.airport-cancun-shuttle.com/">Airport Cancun Shuttle </a> | 
                <a href="http://www.cancun-shuttle.com/">Cancun Shuttle </a> | <a href="http://www.transtrail.com.mx/">Cancun ground transportation </a></td>
        </tr>
      </table>
 	</td>
  </tr>
</table>
</center>
<?php include_once("analyticstracking.php") ?>
</body>
</html>