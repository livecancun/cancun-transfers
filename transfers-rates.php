<!DOCTYPE html>
<html>
<head>
	<title>Transfers Rates | Official Airport Transportation</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="cancun airport transfers, kinmont transfers contact us, cancun shuttle, cancun transfers"/>
	<meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
	<meta property="og:url" content="http://www.cancun-transfers.com/" />
	<meta property="og:title" content="Cancun Airport shuttle">
	<meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.cancun-transfers.com./img/og-cancunshuttle.jpg">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="300">
	<meta property="og:image:height" content="300">
	<meta property="og:locale" content="en_US" />
	<meta property="og:locale:alternate" content="es_ES" />
	<link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
	<link href="css/main-style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
	<div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
 <div class="container">
      <div class="row">
      <div class="col-md-12">
  <h2 class="font-bl1-20">Rates</h2>
  </div>
   </div>
  </div>
<div class="services-ribbon">
  <div class="container">
      <div class="row">
      <div class="col-md-12">
  <p>Cancun Zone</p>
  </div>
   </div>
  </div>
</div>
<div class="container">
  <div class="row">
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/playa-mujeres-cancun-airport.php"><h3>Airport <- -> Playa Mujeres Hotels</h3></a>
            <p>Cancun Airport - Playa Mujeres hotels Only, Private Van, 1-8 passengers max, <b>Round trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                90 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/playa-mujeres-cancun-airport.php"><h3>
                80 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/playa-mujeres-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/puerto-juarez-cancun-airport.php"><h3>Airport <- -> Puerto Juarez</h3></a>
            <p>Cancun Airport - Puerto Juarez hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                90 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/puerto-juarez-cancun-airport.php"> 
              <h3>
                80 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/puerto-juarez-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/cancun-downtown-cancun-airport.php"><h3>Airport <- -> Cancun Centro</h3></a>
            <p>Cancun Airport - Cancun Centro hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                90 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/cancun-downtown-cancun-airport.php"><h3>
                80 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/cancun-downtown-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/cancun-airport-hotel-zone.php"><h3>Airport <- -> Cancun Hotel Zone</h3></a>
            <p>Cancun Airport - Cancun Hotel zone hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                70 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/cancun-airport-hotel-zone.php"> 
              <h3>
                60 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/cancun-airport-hotel-zone.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<div class="services-ribbon">
  <div class="container">
      <div class="row">
      <div class="col-md-12">
  <p>Playa del carmen Zone</p>
  </div>
   </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/puerto-morelos-cancun-airport.php"><h3>Airport <- -> Puerto Morelos</h3></a>
            <p>Cancun Airport - Puerto Morelos hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                80 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/puerto-morelos-cancun-airport.php"><h3>
                75 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/puerto-morelos-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/playa-del-secreto-cancun-airport.php"><h3>Airport <- -> Playa del secreto</h3></a>
            <p>Cancun Airport - Playa del secreto hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                102 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/playa-del-secreto-cancun-airport.php"> 
              <h3>
                90 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/playa-del-secreto-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/vidanta-cirque-do-solei-cancun-airport.php"><h3>Airport <- -> Vidanta Cirque do Solei</h3></a>
            <p>Cancun Airport - Vidanta Cirque do Solei hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                102 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/vidanta-cirque-do-solei-cancun-airport.php"><h3>
                90 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/vidanta-cirque-do-solei-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/maroma-cancun-airport.php"><h3>Airport <- -> Maroma hotels</h3></a>
            <p>Cancun Airport - Maroma hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                102 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/maroma-cancun-airport.php"> 
              <h3>
                90 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/maroma-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/playa-del-carmen-cancun-airport.php"><h3>Airport <- -> Playa del Carmen</h3></a>
            <p>Cancun Airport - Playa del Carmen hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                110 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/playa-del-carmen-cancun-airport.php"><h3>
                100 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/playa-del-carmen-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/ferry-playa-del-carmen-cancun-airport.php"><h3>Airport <- -> Ferry Playa del Carmen</h3></a>
            <p>Cancun Airport - Ferry Playa del Carmen, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                110 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/ferry-playa-del-carmen-cancun-airport.php"> 
              <h3>
                100 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/ferry-playa-del-carmen-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<br><br>
<div class="services-ribbon">
  <div class="container">
      <div class="row">
      <div class="col-md-12">
  <p>Riviera Maya Zone</p>
  </div>
   </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/puerto-aventuras-cancun-airport.php"><h3>Airport <- -> Puerto Aventuras</h3></a>
            <p>Cancun Airport - Puerto Aventuras hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                130 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/puerto-aventuras-cancun-airport.php"><h3>
                120 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/puerto-aventuras-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/akumal-cancun-airport.php"><h3>Airport <- -> Akumal</h3></a>
            <p>Cancun Airport - Akumal hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                155 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/akumal-cancun-airport.php"> 
              <h3>
                140 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/akumal-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/tulum-cancun-airport.php"><h3>Airport <- -> Tulum Downtown</h3></a>
            <p>Cancun Airport - Tulum Downtown hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                200 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/tulum-cancun-airport.php"><h3>
                180 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/tulum-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/tulum-hotel-zone-cancun-airport.php"><h3>Airport <- -> Tulum hotel Zone</h3></a>
            <p>Cancun Airport - Tulum hotel Zone hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                220 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/tulum-hotel-zone-cancun-airport.php"> 
              <h3>
                200 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/tulum-hotel-zone-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
   <br><br><br><br>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>