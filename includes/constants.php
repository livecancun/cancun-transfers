<?php
#====================================================================================================
# File Name : constants.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Check for Hacking Attempt
#----------------------------------------------------------------------------------------------------
/*if ( !defined('IN_SITE') )
{
	die("Hacking attempt");
}
*/
#====================================================================================================
#	Debug Level		- Dinesh Sailor (06-July-2005)
#----------------------------------------------------------------------------------------------------
//define('DEBUG', true); // Debugging on
define('DEBUG', false); // Debugging off

#====================================================================================================
#   Database Halt on Error	- Dinesh Sailor (07-Jan-2005)
#----------------------------------------------------------------------------------------------------
define('DB_HALT_ON_ERROR', 		'report');	 ## "yes" (halt with message), "no" (ignore errors quietly), "report" (ignore errror, but spit a warning)

#====================================================================================================
#	Upload Type					- Dinesh Sailor (07-Jan-2005)
#----------------------------------------------------------------------------------------------------
define('PICTURE',				'Picture');
define('DATAFILE',				'DataFile');
define('IMG_TESTIMONIALS',		"Testimonials");

#====================================================================================================
#	Module Name					- Dinesh Sailor (07-Jan-2005)
#----------------------------------------------------------------------------------------------------
define('MD_LIST',				'List');
define('MD_FSBO',				'Fsbo');
define('MD_CONTACT',			'ContactUs');

#====================================================================================================
#	Flag						- Piyush Patel (07-Jan-2005)
#----------------------------------------------------------------------------------------------------
define('CAT_VISIBLE',	'Visible');
define('CAT_HIDDEN',	'Hidden');
#====================================================================================================
#	Flag						- Dinesh Sailor (07-Jan-2005)
#----------------------------------------------------------------------------------------------------
define('YES',					'Yes');
define('NO',					'No');

#====================================================================================================
#	Data Formate				- Dinesh Sailor (07-Jan-2005)
#----------------------------------------------------------------------------------------------------
define('DF_ARRAY',				'Array');
define('DF_XML',				'XML');

#====================================================================================================
#	Flag						- Dinesh Sailor (07-Jan-2005)
#----------------------------------------------------------------------------------------------------
define('COOKIE_EXPIRE_TIME_MIN',	time()+12*3600);
define('COOKIE_EXPIRE_TIME_MAX',	time()+24*3600);

#====================================================================================================
#	visible						- Dinesh Sailor (07-Jan-2005)
#----------------------------------------------------------------------------------------------------
define('EMAIL_VISIBLE',	'Subscribe');
define('EMAIL_HIDDEN',	'UnSubscribe');

#====================================================================================================
define('ANONYMOUS', "Anonymous");
define('ADMIN', 	"Admin");
define('SUBADMIN', 	"SubAdmin");
define('MASTER', 	"Master");
define('AGENT', 	"Agent");
define('USER',	 	"User");

define('A_ADD',					"Add");
define('A_BACK',				"Back");
define('A_EDIT',				"Edit");
define('A_CANCEL',				"Cancel");
define('A_DELETE',				"Delete");
define('A_DELETE_SELECTED',		"Delete Selected");
define('A_DELETE_PHOTO',		"Delete Photo");
define('A_MAIN_PHOTO',			"Main Photo");
define('A_SAVE',				"Save");
define('A_UPLOAD',				"Upload");
define('A_VIEW',				"View");
define('A_VIEW_ALL',			"View All");
define('A_SORT',				"Sort");
define('A_SUBMIT',				"Submit");

#====================================================================================================
#	Action codes	
#----------------------------------------------------------------------------------------------------
define('ADD',			"Add");
define('BACK',			"Back");
define('CANCEL',		"Cancel");
define('CURRENCY', 		"$");
define('DELETE',		"Delete");
define('DONE',			"Done");

define('EDIT',			"Edit");
define('GO',			"Go");

define('LOGIN',			"Login");

define('MODIFY',		"Modify");
define('MOVE',			"Move");

define('NO',			"No");
define('NEXT',			"Next");

define('SAVE', 			"Save");
define('SEARCH', 		"Search");
define('SEND', 			"Send");
define('SHOW', 			"Show");
define('SHOWALL',		"Showall");
define('SUBMIT', 		"Submit");

define('UPDATE',		"Update");

define('REMOVE', 		"Remove");
define('REGISTER', 		"Register");

define('YES',			"Yes");
define('SORT',			"Sort");

#====================================================================================================
#	Cart Status		
#----------------------------------------------------------------------------------------------------
define('NR', 	"NR"); //No Request
define('PR', 	"PR"); //Pending Request
define('AR', 	"AR"); //Approved Request
define('NA', 	"NA"); //Not Approved
define('RP', 	"RP"); //Request Payment (in case if diff payment method select than paypal)
define('PD', 	"PD"); //Payment Done
#====================================================================================================

#====================================================================================================
#	Cart Status		- Urvashi Solanki
#----------------------------------------------------------------------------------------------------
define('CURRENCY',      "$");
define('SHOW_ALL', 		"Show_All");
define('SEND', 			"Send");
define('SENDREQUEST',	"Send_Request");
define('SHOW', 			"Show");
define('SHOWWEEK', 		"Show_Week");
define('SHOWCHOICE', 	"Show_Choice");
define('SHOWREQUEST', 	"Show_Request");
define('SHOWDETAILS',	"Show_Details");
define('SHOWCART', 		"Show_Cart");
define('ADD', 			"Add");
define('REMOVE', 		"Remove");
define('DELETE', 		"Delete");
define('MODIFY', 		"Modify");
define('MODIFYINFO', 	"ModifyInfo");
define('VIEW', 			"View");
define('OK',			"OK");
define('GO',			"Go");
define('DONE',			"Done");
define('CLOSE',			"Close");
define('PREVIOUS',		"Previous");
define('CHECK',			"Check");
define('RESERVE',		"Reserve");
define('RESERVEINFO',	"ReserveInfo");
define('RESERVED',		"Reserved");
#============ Button Varibles ==============#
define('APPROVE',       $lang['Note_AR']);
define('NOTAPPROVE',    $lang['Note_NA']);
define('PENDING',       $lang['Note_PR']);
define('PAYMENTDONE',   $lang['Note_PD']);     
define('SAVE', 			$lang['save']);
define('UPDATE', 		$lang['update']);
define('CANCEL',		$lang['cancel']);
define('BACK',			$lang['Back']);
define('BOOK',			$lang['Book']);
define('SUBMIT',		$lang['submit']);
define('RESET',			$lang['Reset']);
define('NEXT',			$lang['Next']);
define('CHANGE',		$lang['Change']);
define('LOGIN',			$lang['Login']);
#===========================================#

#===========================================#
define('ADDLINK',			'AddLink');
define('DELETELINK',		'DeleteLink');
define('MODIFYLINK',		'ModifyLink');
#===========================================#

?>