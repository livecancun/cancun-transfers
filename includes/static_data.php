<?php
#====================================================================================================
# File Name : static_data.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Check for Hacking Attempt
#----------------------------------------------------------------------------------------------------
/*if ( !defined('IN_SITE') )
{
	die("Hacking attempt");
}
*/
# Constant
define(TITLE,			'Title');
define(SUBOPTION,		'SubOption');
define(DESC,			'Desc');
define(LINK,			'Link');

$lang['PageSize_List'] = array(	'2' 	=> '2',
								'15' 	=> '15',
								'30' 	=> '30',
								'50' 	=> '50',
								'100' 	=> '100',
								'200' 	=> '200',
								'500' 	=> '500',
							 	);

$lang['Direction_List'] = array('Asc' 	=> 'Asc',
								'Desc' 	=> 'Desc',
								);

$lang['YesNo_List'] 	= array('Yes' 	=> 'Yes',
								'No' 	=> 'No',
								);

$lang['OutputType_List'] = array('browser'	=> 'Browser',
								'excel'		=> 'Microsoft Excel (.xls)',
								'csv'		=> 'Delimited Text(CSV)',
								);

#==================================================================================================#
#	Day Array
#==================================================================================================#
$asset['arrDay']=  array("01"	=>	"01",
	  					 "02"  	=>	"02",
						 "03"   =>	"03",
 						 "04"   =>	"04",
 						 "05"   =>	"05",
						 "06"   =>	"06",						 
 						 "07"   =>	"07",
 						 "08"   =>	"08",
						 "09"   =>	"09",						 
						 "10"   =>	"10",						 
 						 "11"   =>	"11",
						 "12"   =>	"12",						 
 						 "13"   =>	"13",
						 "14"   =>	"14",						 
 						 "15"   =>	"15",
						 "16"   =>	"16",						 
 						 "17"   =>	"17",
						 "18"   =>	"18",						 
 						 "19"   =>	"19",
						 "20"   =>	"20",						 
 						 "21"   =>	"21",
						 "22"   =>	"22",
						 "23"   =>	"23",
						 "24"   =>	"24",
						 "25"   =>	"25",
						 "26"   =>	"26",
						 "27"   =>	"27",
						 "28"   =>	"28",
						 "29"   =>	"29",
						 "30"   =>	"30",
 						 "31"   =>	"31"
						);

#==================================================================================================#
#	Hour Array
#==================================================================================================#
$lang['arrHour'] =	array("0"	=>	"0",
						 "1"	=>	"1",
	  					 "2"  	=>	"2",
						 "3"    =>	"3",
 						 "4"    =>	"4",
 						 "5"    =>	"5",
						 "6"    =>	"6",						 
 						 "7"    =>	"7",
 						 "8"    =>	"8",
						 "9"    =>	"9",						 
						 "10"   =>	"10",						 
 						 "11"   =>	"11",
						 "12"   =>	"12",						 
						);
						
#==================================================================================================#
#	Minute Array
#==================================================================================================#
$lang['arrMinute'] =  array("00"	=>	"00",
							 "10"   =>	"10",						 
							 "20"   =>	"20",
							 "30"   =>	"30",
							 "40"   =>	"40",						 
							 "50"   =>	"50"
							);

#==================================================================================================#
#	AM/PM  Array
#==================================================================================================#
$lang['arrDayPart']	= array("AM"  => "AM",
							"PM"  => "PM",
							);				
							
							
#==================================================================================================#
#	Link Category Status	
#==================================================================================================#
$arrLinkStatus	=	array(	"0"	=>  'Disable',
							"1"	=>	'Enable',
						); 		
						
#====================================================================================================
#	States	List
#----------------------------------------------------------------------------------------------------
$lang['State_List']['AK'] 	= 'Alaska';
$lang['State_List']['AL'] 	= 'Alabama';
$lang['State_List']['AR'] 	= 'Arkansas';
$lang['State_List']['AZ'] 	= 'Arizona';
$lang['State_List']['CA'] 	= 'California';
$lang['State_List']['CO'] 	= 'Colorado';
$lang['State_List']['CT'] 	= 'Connecticut';
$lang['State_List']['DC'] 	= 'District of Columbia';
$lang['State_List']['DE'] 	= 'Delaware';
$lang['State_List']['FL'] 	= 'Florida';
$lang['State_List']['GA'] 	= 'Georgia';
$lang['State_List']['HI'] 	= 'Hawaii';
$lang['State_List']['IA'] 	= 'Iowa';
$lang['State_List']['ID'] 	= 'Idaho';
$lang['State_List']['IL'] 	= 'Illinois';
$lang['State_List']['IN'] 	= 'Indiana';
$lang['State_List']['KS'] 	= 'Kansas';
$lang['State_List']['KY'] 	= 'Kentucky';
$lang['State_List']['LA'] 	= 'Louisiana';
$lang['State_List']['MA'] 	= 'Massachusetts';
$lang['State_List']['MD'] 	= 'Maryland';
$lang['State_List']['ME'] 	= 'Maine';
$lang['State_List']['MI'] 	= 'Michigan';
$lang['State_List']['MN'] 	= 'Minnesota';
$lang['State_List']['MO'] 	= 'Missouri';
$lang['State_List']['MS'] 	= 'Mississippi';
$lang['State_List']['MT'] 	= 'Montana';
$lang['State_List']['NC'] 	= 'North Carolina';
$lang['State_List']['ND'] 	= 'North Dakota';
$lang['State_List']['NE'] 	= 'Nebraska';
$lang['State_List']['NH'] 	= 'New Hampshire';
$lang['State_List']['NJ'] 	= 'New Jersey';
$lang['State_List']['NM'] 	= 'New Mexico';
$lang['State_List']['NV'] 	= 'Nevada';
$lang['State_List']['NY'] 	= 'New York';
$lang['State_List']['OH'] 	= 'Ohio';
$lang['State_List']['OK'] 	= 'Oklahoma';
$lang['State_List']['OR'] 	= 'Oregon';
$lang['State_List']['PA'] 	= 'Pennsylvania';
$lang['State_List']['PR'] 	= 'Puerto Rico';
$lang['State_List']['PW'] 	= 'Palau';
$lang['State_List']['RI'] 	= 'Rhode Island';
$lang['State_List']['SC'] 	= 'South Carolina';
$lang['State_List']['SD'] 	= 'South Dakota';
$lang['State_List']['TN'] 	= 'Tennessee';
$lang['State_List']['TX'] 	= 'Texas';
$lang['State_List']['UT'] 	= 'Utah';
$lang['State_List']['VA'] 	= 'Virginia';
$lang['State_List']['VT'] 	= 'Vermont';
$lang['State_List']['WA'] 	= 'Washington';
$lang['State_List']['WI'] 	= 'Wisconsin';
$lang['State_List']['WV'] 	= 'West Virginia';
$lang['State_List']['WY'] 	= 'Wyoming';
													

?>