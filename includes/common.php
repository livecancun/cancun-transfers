<?php
#====================================================================================================
# File Name : common.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Check for Hacking Attempt
#----------------------------------------------------------------------------------------------------
/*if (!defined('IN_SITE'))
{
	die("Hacking Attempt");
}
*/
#====================================================================================================
#	Start Session and define site access valid
#----------------------------------------------------------------------------------------------------
session_start();
//define('IN_SITE', 	true);
#====================================================================================================
#	Set Language Variable
#----------------------------------------------------------------------------------------------------
	if ((!isset($_GET['lng'])) && (!isset($_SESSION['lng'])) )
	{
		if (eregi("es", $_SERVER['HTTP_ACCEPT_LANGUAGE']))
		 {
			// $_SESSION['lng'] = 'sp';
			 $_SESSION['lng'] = 'en';
			//$lng = 'en';
			$lng = $_SESSION['lng'];
		 }
		else
		{
			$_SESSION['lng'] = 'en';
			//$lng = 'en';
			$lng = $_SESSION['lng'];
		}	
		
	}
	elseif(isset($_GET['lng']))
	{
	  $_SESSION['lng'] = $_GET['lng'];
	  $lng = $_SESSION['lng'];
	}	
	else
	{
		//$_SESSION['lng'] = $_GET['lng'];
		//$lng = $_GET['lng'];
		$lng = $_SESSION['lng'];
	}	

//print "<br>lng=".$lng;
$today = date("l , F j, Y"); 

#====================================================================================================
#	addslashes to vars if magic_quotes_gpc is off
#	this is a security precaution to prevent someone
#	trying to break out of a SQL statement.
#----------------------------------------------------------------------------------------------------
error_reporting(E_ERROR | E_WARNING | E_PARSE); // This will NOT report uninitialized variables

#====================================================================================================
#	Define some basic configuration arrays. This also prevents
#	malicious rewriting of language and otherarray values via
#	URI params
#----------------------------------------------------------------------------------------------------
$physical_path	= array();
$virtual_path	= array();
$config 		= array();

#====================================================================================================
#	Define Site state and set site root
#----------------------------------------------------------------------------------------------------

// Set the server name
$config['Server_Name'] = strtoupper($_SERVER['SERVER_NAME']);

// Set the Table Prefix
$config['Table_Prefix']= $lng;


// Set the installation directory
switch($config['Server_Name'])
{
	// Dotworld (local)
	case "DOTWORLD":
		define("INSTALL_DIR", "/cancuntransfer/site/");
        break;

	// Dotnet (local)
	case "DOTNET":
		define("INSTALL_DIR", "/cancuntransfer/site/");
		$_SERVER['DOCUMENT_ROOT']	=	'd:/websites';
		$_SERVER['HTTP_HOST']		=	'dotnet';
        break;

    // Dot Infosys (Company Server)
	case "CANCUNTRANSFER.DOTINFOSYS.COM":
		define("INSTALL_DIR", "/");
        break;

    // Client Server
    default:
		define("INSTALL_DIR", "/");
    	break;
}

// Define site root
$physical_path['Site_Root']			=	$_SERVER['DOCUMENT_ROOT']. INSTALL_DIR;
$virtual_path['Site_Root']			=	'http://'. $_SERVER['HTTP_HOST']. INSTALL_DIR;

#====================================================================================================
#	Including required configuration
#----------------------------------------------------------------------------------------------------
$physical_path['Site_Include']		=	$physical_path['Site_Root']. 'includes/';
$physical_path['Site_Language']		=	$physical_path['Site_Root']. 'language/';

include($physical_path['Site_Language'].$config['Table_Prefix']."-inc.php");
include($physical_path['Site_Include']. 'config.php');
include($physical_path['Site_Include']. 'tableConstants.php');
include($physical_path['Site_Include']. 'constants.php');
include($physical_path['Site_Include']. 'static_data.php');
include($physical_path['Site_Include']. 'functions.php');

#====================================================================================================
#	Including all required library
#----------------------------------------------------------------------------------------------------
$physical_path['Libs']				=	$physical_path['Site_Root']. 'libs/';
$virtual_path['Libs']				=	$virtual_path['Site_Root']. 'libs/';

include($physical_path['Libs']. 'mysql.php');
include($physical_path['Libs']. 'htmlMimeMail.php');
include($physical_path['Libs']. 'thumbnail.php');
include($physical_path['Libs']. 'Smarty.class.php');
 	
#====================================================================================================
#	Defining the Class url
#----------------------------------------------------------------------------------------------------
$physical_path['Class_Root']	=	$physical_path['Site_Root'].'class/';
$virtual_path['Class_Root']		=	$virtual_path['Class_Root']. 'class/';
#====================================================================================================
#	Define Rich HTML editor path
#----------------------------------------------------------------------------------------------------
define('EDITOR_ROOT', 	$physical_path['Libs']. 'spaw/' );
define('EDITOR_URL', 	$virtual_path['Libs']. 'spaw/');

#====================================================================================================
#	Define Data Access root
#----------------------------------------------------------------------------------------------------
$physical_path['DB_Access']			=	$physical_path['Site_Root']. 'db_access/';
include($physical_path['DB_Access']. 'WebConfig.php');
include($physical_path['DB_Access']. 'User.php');
include($physical_path['DB_Access']. 'Utility.php');
include($physical_path['DB_Access']. 'Language.php');
#====================================================================================================
#	File upload directory
#----------------------------------------------------------------------------------------------------
$physical_path['Upload']				=	$physical_path['Site_Root']. 'upload/';
$virtual_path['Upload']					=	$virtual_path['Site_Root']. 'upload/';

$physical_path['Testimonials']			=	$physical_path['Upload']. 'testimonials/';
$virtual_path['Testimonials']			=	$virtual_path['Upload']. 'testimonials/';
#====================================================================================================
#	Email Template
#----------------------------------------------------------------------------------------------------
$physical_path['EmailTemplate']			=	$physical_path['Site_Root']. 'email_templates/';
$virtual_path['EmailTemplate']			=	$virtual_path['Site_Root']. 'email_templates/';

#====================================================================================================
#	Define User the user root file path
#----------------------------------------------------------------------------------------------------
if(defined("IN_SITE"))
{
	$physical_path['User_Root']			=	$physical_path['Site_Root'];
	$virtual_path['User_Root']			=	$virtual_path['Site_Root'];
}
elseif(defined("IN_ADMIN"))
{
	$physical_path['User_Root']			=	$physical_path['Site_Root']. 'siteadmin/';
	$virtual_path['User_Root']			=	$virtual_path['Site_Root']. 'siteadmin/';
}

// Define Template root
$physical_path['Templates_Root']		=	$physical_path['User_Root']. 'templates/';
$virtual_path['Templates_Root']			=	$virtual_path['User_Root'].  'templates/';

$virtual_path['Templates_CSS']			=	$virtual_path['Templates_Root']. 'css/';
$virtual_path['Templates_JS']			=	$virtual_path['Templates_Root']. 'js/';
$virtual_path['Templates_Image']		=	$virtual_path['Templates_Root']. 'images/';
#====================================================================================================
#	Initial the required object
#----------------------------------------------------------------------------------------------------

# Make the database connection
#----------------------------------------------------------------------------------------------------
global $db,$db1;
$db = '';
$db1 = '';
$db = new DB_Sql($config['DB_Host'], $config['DB_Name'], $config['DB_User'], $config['DB_Passwd'], false);
$db1 = new DB_Sql($config['DB_Host'], $config['DB_Name'], $config['DB_User'], $config['DB_Passwd'], false);

//$db->query("SET character_set_database = utf8");
//$db->query("SET character set utf8");

if(!$db->link_id())
{
  die("Could not connect to the database");
}

if(!$db1->link_id())
{
  die("Could not connect to the database");
}

# 	Make the mail object
global $mimemail;
$mimemail = '';
$mimemail = new htmlMimeMail();

#	Read site configuration
#----------------------------------------------------------------------------------------------------
global $webConf;
$webConf = '';
$webConf = new WebConfig();

#	Read site configuration
#----------------------------------------------------------------------------------------------------
global $utility;
$utility = '';
$utility = new Utility();

#	Read site configuration
#----------------------------------------------------------------------------------------------------
global $thumb;
$thumb = '';
$thumb = new thumbnail();

#	Read site configuration
#----------------------------------------------------------------------------------------------------
global $language;
$language = '';
$language = new language();

global $LangInfo;
$LangInfo = $language->ViewAll();

# Initiate User and start session
#----------------------------------------------------------------------------------------------------
$user = new User();
$user->StartSession();

# Get the logged user info
switch($user->User_Perm)
{
	# Admin
	case ADMIN:
		include_once($physical_path['DB_Access']. 'Admin.php');
		$usrInfo = new Admin($user->User_Id);
		break;
}

# Initial the smarty object
#----------------------------------------------------------------------------------------------------
$tpl = new Smarty;

$tpl->compile_check = true;
$tpl->force_compile = true;
$tpl->debugging 	= DEBUG;

# Define page layout
$tpl->template_dir 	= $physical_path['Templates_Root'];
$tpl->compile_dir 	= $physical_path['User_Root']. 'templates_c/';

# Assign default values
$tpl->assign(array(	"T_Header"				=>	'default_header'. $config['tplEx'],
					"T_Menu"				=>	'default_menu'	. $config['tplEx'],
					"T_Footer"				=>	'default_footer'. $config['tplEx'],
					"Templates_CSS"			=> 	$virtual_path['Templates_CSS'],
					"Templates_JS"			=> 	$virtual_path['Templates_JS'],
					"Templates_Image"		=> 	$virtual_path['Templates_Image'],
					"Site_Root"				=> 	$virtual_path['Site_Root'],
                    "Company_Title"			=>	$config[WC_COMPANY_TITLE],
                    "Site_Title"			=>	$config[WC_SITE_TITLE],
					"Meta_Title"			=>	$config[WC_META_TITLE],
					"Meta_Keyword"			=>	$config[WC_META_KEYWORD],
					"Meta_Description"		=>	$config[WC_META_DESCRIPTION],
					"Copyright_Text"		=>	$config[WC_COPYRIGHT_TEXT],
					"Powered_By"			=>	$config[WC_POWEREDBY],
					"LangInfo"				=>	$LangInfo,
					));

if($lng=="sp")
{
	$tpl->assign(array(	"lang_link"		=>	"ground_transportation_rates.php?&lng=en",
						"lng"			=>	"sp",												
						));
}
else
{						
	$tpl->assign(array(	"lang_link"		=>	"ground_transportation_rates.php?&lng=sp",
						"lng"			=>	"en",						
						));
}

#=========================== Header Image & Language Variables ============================#
$tpl->assign(array(	"hed_img1"					=>	$lang['hed_img1'],
					"hed_home_img"				=>	$lang['hed_home_img'],					
					"hed_rates_img"				=>	$lang['hed_rates_img'],					
					"hed_arrival_img"			=>	$lang['hed_arrival_img'],					
					"hed_contact_img"			=>	$lang['hed_contact_img'],					
					"hed_login_img"				=>	$lang['hed_login_img'],					
					"hed_espanol_img"			=>	$lang['hed_espanol_img'],
					"hed_espanol_imgALT"		=>	$lang['hed_espanol_imgALT'],					
					));

$tpl->assign(array(	"L_ACS_Address"		=>	$lang['L_ACS_Address'],
				));

#==================================== Footer Messages =========================================#
$tpl->assign(array(	"L_Footer_Msg"		=>	$lang['L_Footer_Msg'],
				));

#==============================================================================================#					
					
if(defined("POPUP_WINDOW"))
{
/*	$tpl->display('popupwin_layout'. $config['tplEx']);
*/
}

if(defined("RIGHT_PANEL"))
{
	$tpl->assign(array(	"right_menu"		=>	YES,
					));
}

if(defined("IN_SITE"))
{
	#=============== Set language ===================#
	$_SESSION['lng']		=	isset($_GET['lng'])?$_GET['lng']:(isset($_SESSION['lng'])?$_SESSION['lng']:$config[WC_LANG]);
	
	$base_url = "index.php?" . substr($_SERVER['QUERY_STRING'], 0, strpos($_SERVER['QUERY_STRING'],"&lng")===false?strlen($_SERVER['QUERY_STRING']):strpos($_SERVER['QUERY_STRING'],"&lng"));
	
	$tpl->assign(array(	"Lang"		=>	$lang[$_SESSION['lng']],
						"base_url"	=>	$base_url,
					));
}
# If admin then assign actions
else if(defined("IN_ADMIN"))
{
	$tpl->assign(array("User_Name"				=>	$usrInfo->FirstName. ' '. $usrInfo->LastName,
						"AssignedPrivileges"	=>	$usrInfo->Privileges,
						"Privileges"			=>	$usrInfo->PrivilegesList,
						));
}

?>