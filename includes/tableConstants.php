<?php
#====================================================================================================
# File Name : tableConstants.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contains all application configuration details
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#====================================================================================================

#====================================================================================================
#	Check for Hacking Attempt
#----------------------------------------------------------------------------------------------------
/*if ( !defined('IN_SITE') )
{
	die("Hacking attempt");
}
*/	
#====================================================================================================
#	Cart Status		
#----------------------------------------------------------------------------------------------------
define('NR', 	"NR"); //No Request
define('PR', 	"PR"); //Pending Request
define('AR', 	"AR"); //Approved Request
define('NA', 	"NA"); //Not Approved
define('RP', 	"RP"); //Request Payment (in case if diff payment method select than paypal)
define('PD', 	"PD"); //Payment Done
 
#====================================================================================================
#====================================================================================================
#	Room Reservation Type (for Tour and Hotel)		
#----------------------------------------------------------------------------------------------------
define('AI', 0); //Reserve Room(All Inclusive)
define('RO', 1); //Reserve Room(Only Room)
define('PK', 2); //Reserve Package
#====================================================================================================
define('CURRENCY',      "$");
define('SHOW_ALL', 		"Show_All");
define('SEND', 			"Send");
define('SENDREQUEST',	"Send_Request");
define('SHOW', 			"Show");
define('SHOWWEEK', 		"Show_Week");
define('SHOWCHOICE', 	"Show_Choice");
define('SHOWREQUEST', 	"Show_Request");
define('SHOWDETAILS',	"Show_Details");
define('SHOWCART', 		"Show_Cart");
define('ADD', 			"Add");
define('REMOVE', 		"Remove");
define('DELETE', 		"Delete");
define('MODIFY', 		"Modify");
define('MODIFYINFO', 	"ModifyInfo");
define('VIEW', 			"View");
define('OK',			"OK");
define('GO',			"Go");
define('DONE',			"Done");
define('CLOSE',			"Close");
define('PREVIOUS',		"Previous");
define('CHECK',			"Check");
define('RESERVED',		"Reserved");
#===============================================#

#============ Button Varibles =================#
define('APPROVE',       $lang['Note_AR']);
define('NOTAPPROVE',    $lang['Note_NA']);
define('PENDING',       $lang['Note_PR']);
define('PAYMENTDONE',   $lang['Note_PD']);     
define('SAVE', 			$lang['save']);
define('UPDATE', 		$lang['update']);
define('CANCEL',		$lang['cancel']);
define('BACK',			$lang['Back']);
define('BOOK',			$lang['Book']);
define('SUBMIT',		$lang['submit']);
define('RESET',			$lang['Reset']);
define('NEXT',			$lang['Next']);
define('CHANGE',		$lang['Change']);
define('LOGIN',			$lang['Login']);
#===============================================#

//print "Table_Prefix=".$config['Table_Prefix'];
#====================================================================================================
#	Table names			
#----------------------------------------------------------------------------------------------------
	define('AUTH_USER',					'auth_user');
	
	define('CART_DESTINATIONS',			'cart_destinations');
	define('CART_MASTER',				'cart_master');
	define('COUNTRY_MASTER',			'country_master');
	
	define('DESTINATION_LANG',			'destination_lang');
	define('DESTINATION_MASTER',		'destination_master');
	define('DESTINATION_DETAILS',		'destination_lang');
	define('DESTINATION_DISCOUNT',		'destination_discount');
	
	define('LANGUAGE_MASTER',			'languages');

	define('LINK_CATEGORY',				$config['Table_Prefix'].'link_category');
	define('LINK_MASTER',				$config['Table_Prefix'].'link_master');
	
	define('PASSENGER_MASTER',			'passenger_master');
	define('PAGE_MASTER',				'page_master');
	define('PAGE_DETAILS',				'page_details');
	
	define('RATE_MASTER',				'rate_master');
	
	define('STATE_MASTER',				'state_master');
	define('SUBADMIN_MASTER',			'subadmin_master');


	define('TESTIMONIAL_MASTER',		$config['Table_Prefix'].'testimonial_master');

	define('TRIPTYPE_MASTER',			'triptype_master');
	define('TRIPTYPE_DETAILS',			'triptype_lang');
	
	define('USER_MASTER',				'user_master');
	
	define('WEBSITE_CONFIG',			$config['Table_Prefix'].'web_config');

?>