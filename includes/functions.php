<?php
#====================================================================================================
# File Name : functions.php
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contain the required function used during login to site
# Author 	: PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email 	: info@pimsa.com <mailto:info@pimsa.com>
#
#==================================================================================================== 

#====================================================================================================
#	Check for Hacking Attempt
#----------------------------------------------------------------------------------------------------
/*if ( !defined('IN_SITE') )
{
	die("Hacking attempt");
}
*/
define('CART_MASTER',	$config['Table_Prefix']. 'cart_master');
#====================================================================================================
#	Function Name		:	getLanguage
#	Purpose				:	Provide path to language
#	Return				:	Encoded IP Address
#	Author				:	Dinesh Sailor
#	Creation Date		:	12-Oct-2005
#----------------------------------------------------------------------------------------------------
function getLanguage($langName, $moduleName='')
{
	global $physical_path;
	return ($physical_path['Site_Language']. ($moduleName!=''?strtolower($moduleName). '/':''). $langName);
}

#====================================================================================================
#	Function Name		:	getAsset
#	Purpose				:	Provide path to assets
#	Return				:	Encoded IP Address
#	Author				:	Dinesh Sailor
#	Creation Date		:	12-Oct-2005
#----------------------------------------------------------------------------------------------------
function getAsset($assetName, $moduleName='')
{
	global $physical_path;
	return ($physical_path['Assets']. ($moduleName!=''?strtolower($moduleName). '/':''). $assetName);
}

#====================================================================================================
#	Function Name		:	getDBAccess
#	Purpose				:	Provide path to data access
#	Return				:	Encoded IP Address
#	Author				:	Dinesh Sailor
#	Creation Date		:	12-Oct-2005
#----------------------------------------------------------------------------------------------------
function getDBAccess($dbfileName, $moduleName='')
{
	global $physical_path;
	return ($physical_path['DB_Access']. ($moduleName!=''?strtolower($moduleName). '/':''). $dbfileName);
}

#====================================================================================================
#	Function Name		:	getTemplate
#	Purpose				:	Provide path to templates
#	Return				:	Encoded IP Address
#	Author				:	Dinesh Sailor
#	Creation Date		:	12-Oct-2005
#----------------------------------------------------------------------------------------------------
function getTemplate($tmplName, $moduleName='')
{
	global $physical_path, $config;
	return ($physical_path['Templates_Root']. ($moduleName!=''?strtolower($moduleName). '/':''). $tmplName. '.'. $config['tplEx']);
}

#====================================================================================================
#	Function Name		:	getEmailTemplate
#	Purpose				:	Provide path to Email templates
#	Return				:	
#	Author				:	Parimal Gajjar
#	Creation Date		:	08-Nov-2005
#----------------------------------------------------------------------------------------------------
function getEmailTemplate($tmplName, $moduleName='')
{
	global $physical_path, $config;
	return ($physical_path['Email_Templates_Root']. ($moduleName!=''?strtolower($moduleName). '/':''). $tmplName. '.'. $config['tplEx']);
}

#====================================================================================================
#	Function Name		:	getDBAccess
#	Purpose				:	Provide path to data access
#	Return				:	Encoded IP Address
#	Author				:	Dinesh Sailor
#	Creation Date		:	12-Oct-2005
#----------------------------------------------------------------------------------------------------
function getJavaScript($jsfileName, $moduleName='')
{
	global $virtual_path;
	return (($moduleName!=''?strtolower($moduleName). '/':''). $jsfileName);
}


#====================================================================================================
#	Function Name		:	getNavigationLink
#	Purpose				:	Create the top navigation link
#	Return				:	Navigation link
#	Author				:	Dinesh Sailor
#	Creation Date		:	21-Oct-2005
#----------------------------------------------------------------------------------------------------
function getNavigationLink($linkData)
{
	global $virtual_path, $format, $lang, $navigateLink;
	
	$link = '';

	for($i=0; $i<count($linkData)-1; $i++)
	{
		if(is_array($linkData[$i]))
			$link	.=	sprintf($format['Navigation_Link'], $linkData[$i][0], $linkData[$i][1]);
		else
			$link	.=	sprintf($format['Navigation_Link'], $lang['Menu'][$linkData[$i]][TITLE], $virtual_path['User_Root']. $navigateLink['Menu'][$linkData[$i]]);
	}

	$link	.=	$linkData[$i];

	return $link;
}

#====================================================================================================
#	Function Name		:	getSiteNavigationLink
#	Purpose				:	Create the top navigation link
#	Return				:	Navigation link
#	Author				:	Parimal Gajjar
#	Creation Date		:	21-Oct-2005
#----------------------------------------------------------------------------------------------------
function getSiteNavigationLink($linkData)
{
	global $format, $HomeLink;
	$link = '';
	
	if(count($linkData) > 0)
	{
		$link	.=	sprintf($format['Navigation_Link'], $HomeLink[0], $HomeLink[1]);
		for($i=0; $i<count($linkData)-1; $i++)
		{
			if(is_array($linkData[$i]))
				$link	.=	sprintf($format['Navigation_Link'], $linkData[$i][0], $linkData[$i][1]);
			else
				$link	.=	sprintf($format['Navigation_Text'], $linkData[$i]);
		}
		$link	.=	$linkData[$i];
	}
	else
		$link = "Home";
		
		return $link;
}

#====================================================================================================
#	Function Name		:	getPageSize
#	Purpose				:	Create the top navigation link
#	Return				:	Navigation link
#	Author				:	Dinesh Sailor
#	Creation Date		:	21-Oct-2005
#----------------------------------------------------------------------------------------------------
function getPageSize()
{
	global $data, $lang;
	
	return $_POST['page_size'] ? $_POST['page_size'] : ($_COOKIE['page_size'] ? $_COOKIE['page_size']:key($lang['PageSize_List']));
}

#====================================================================================================
#	Function Name		:	getStartRecord
#	Purpose				:	Create the top navigation link
#	Return				:	Navigation link
#	Author				:	Dinesh Sailor
#	Creation Date		:	21-Oct-2005
#----------------------------------------------------------------------------------------------------
function getStartRecord()
{
	return $_POST['goto_page'] ? (((int)$_POST['goto_page']-1) * $_POST['page_size']) : ($_GET['start'] ? (int)$_GET['start'] : 0 );
}

#====================================================================================================
#	Function Name		:	generate_pagination2
#	Purpose				:	Display the pagination 
#	Return				:	pagination 
#----------------------------------------------------------------------------------------------------
function generate_pagination2($num_items, $per_page, $start_item, $add_prevnext_text = TRUE , $path)
{
	global $lang;

	$path_parts = pathinfo($_SERVER['SCRIPT_FILENAME']);
	$base_url = $path_parts["basename"] . "?" . substr($_SERVER['QUERY_STRING'], 0, strpos($_SERVER['QUERY_STRING'],"&start")===false?strlen($_SERVER['QUERY_STRING']):strpos($_SERVER['QUERY_STRING'],"&start"));

	$total_pages = ceil($num_items/$per_page);

	if ( $total_pages == 1 )
	{
		return '';
	}

	$on_page = floor($start_item / $per_page) + 1;

	$page_string = '';
	if ( $total_pages > 10 )
	{
		$init_page_max = ( $total_pages > 3 ) ? 3 : $total_pages;

		for($i = 1; $i < $init_page_max + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url. "&amp;start=" . ( ( $i - 1 ) * $per_page )  . '\';document.forms[0].submit();" >' . $i . '</a>';
			if ( $i <  $init_page_max )
			{
				$page_string .= ' ';
//				$page_string .= ", ";

			}
		}

		if ( $total_pages > 3 )
		{
			if ( $on_page > 1  && $on_page < $total_pages )
			{
				$page_string .= ( $on_page > 5 ) ? ' ... ' : ', ';

				$init_page_min = ( $on_page > 4 ) ? $on_page : 5;
				$init_page_max = ( $on_page < $total_pages - 4 ) ? $on_page : $total_pages - 4;

				for($i = $init_page_min - 1; $i < $init_page_max + 2; $i++)
				{
					$page_string .= ($i == $on_page) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page )  . '\'; document.forms[0].submit();">' . $i . '</a>';
					if ( $i <  $init_page_max + 1 )
					{
						$page_string .= ' ';
//						$page_string .= ', ';
					}
				}

				$page_string .= ( $on_page < $total_pages - 4 ) ? ' ... ' : ', ';
			}
			else
			{
				$page_string .= ' ... ';
			}

			for($i = $total_pages - 2; $i < $total_pages + 1; $i++)
			{
				//$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>'  : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) . '\';document.forms[0].submit();">' . $i . '</a>';
				if( $i <  $total_pages )
				{
					$page_string .= " ";
//					$page_string .= ", ";
				}
			}
		}
	}
	else
	{
		for($i = 1; $i < $total_pages + 1; $i++)
		{
			//$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) . '\';document.forms[0].submit();">' . $i . '</a>';
			if ( $i <  $total_pages )
			{
					$page_string .= " ";
//					$page_string .= ", ";
			}
		}
	}

	if ( $add_prevnext_text )
	{
		if ( $on_page > 1 )
		{
			$page_string = ' <a class=pageLink href="javascript: document.forms[1].action=\'' . $base_url . "&amp;start=" . ( ( $on_page - 2 ) * $per_page ) . '\'; document.forms[1].submit();">&lt;&lt;back </a>';
		}
		else
		{
			//$page_string = '&nbsp;&nbsp;' . $page_string;
//			$page_string = ' <font class=disabledText>' . 'Previous'. '</font>&nbsp;&nbsp;' . $page_string;
		}

		if ( $on_page < $total_pages )
		{
			$page_string .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class=pageLink href="javascript: document.forms[1].action=\'' . $base_url . "&amp;start=" . ( $on_page * $per_page ) . '\';document.forms[1].submit();">next&gt;&gt;</a>';
		}
		else
		{
			//$page_string .= '&nbsp;&nbsp;';
//			$page_string .= '&nbsp;&nbsp;<font class=disabledText>' . 'Next'. '</font>';
		}

	}

	return $page_string;
}

#====================================================================================================
#	Function Name		:	encode_ip
#	Purpose				:	This function will encode the ip address
#	Parameters			:	dotquad_ip	-	Dot quaded IP Address e.g. 127.0.0.1
#	Return				:	Encoded IP Address
#	Author				:	Dinesh Sailor
#	Creation Date		:	07-Oct-2005
#----------------------------------------------------------------------------------------------------
function encode_ip($dotquad_ip)
{
	$ip_sep = explode('.', $dotquad_ip);
	return sprintf('%02x%02x%02x%02x', $ip_sep[0], $ip_sep[1], $ip_sep[2], $ip_sep[3]);
}

#====================================================================================================
#	Function Name		:	decode_ip
#	Purpose				:	This function will decode ip address encode by encode_ip function
#	Parameters			:	int_ip	-	Encoded IP Address
#	Return				:	Decoded IP Address
#	Author				:	Dinesh Sailor
#	Creation Date		:	07-Oct-2005
#----------------------------------------------------------------------------------------------------
function decode_ip($int_ip)
{
	$hexipbang = explode('.', chunk_split($int_ip, 2, '.'));
	return hexdec($hexipbang[0]). '.' . hexdec($hexipbang[1]) . '.' . hexdec($hexipbang[2]) . '.' . hexdec($hexipbang[3]);
}

#====================================================================================================
#	Function Name		:	start_session
#	Purpose				:	This function will assign the session variable to user_data array
#	Parameters			:	none
#	Return				:	none
#	Author				:	Dinesh Sailor
#	Creation Date		:	07-Oct-2005
#----------------------------------------------------------------------------------------------------
function start_session()
{
	global $virtual_path;
	global $lang;
	
	$user_data = array();

	#====================================================================================================
	#	Obtain and encode users IP
	#----------------------------------------------------------------------------------------------------
	if( getenv('HTTP_X_FORWARDED_FOR') != '' )
	{
		$client_ip = ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : $REMOTE_ADDR );
	
		if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", getenv('HTTP_X_FORWARDED_FOR'), $ip_list) )
		{
			$private_ip = array('/^0\./', '/^127\.0\.0\.1/', '/^192\.168\..*/', '/^172\.16\..*/', '/^10.\.*/', '/^224.\.*/', '/^240.\.*/');
			$client_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
		}
	}
	else
	{
		$client_ip = ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : $REMOTE_ADDR );
	}

	$user_data['User_IP'] 	= 	encode_ip($client_ip);

	#====================================================================================================
	#	Obtain and set the user_id from session 
	#----------------------------------------------------------------------------------------------------
	
	if(isset($_SESSION['User_Id']))
	{
		
		$user_data['User_Id']		=	$_SESSION['User_Id'];
		$user_data['User_Name']		=	$_SESSION['User_Name'];
		$user_data['User_Perm']		=	$_SESSION['User_Perm'];
		
		switch($user_data['User_Perm'])
		{
			case ADMIN:
			//	if(!defined("IN_ADMIN"))
			//	{
			//		header('location: '. $virtual_path['User_Root']. 'logout.php'); 
			//		exit;
			//	}
				break;

			case USER:
				if(!defined("IN_MEMBER")&&!defined("POPUP_WINDOW"))
				{
					header('location: '. $virtual_path['User_Root']. 'logout.php'); 
					exit;
				}
				break;
		}
	}
	else
	{
		$path_parts = pathinfo($_SERVER['PHP_SELF']);

		if($path_parts["basename"] != 'login.php' && defined("IN_ADMIN"))
			header('location: '. $virtual_path['User_Root']. 'logout.php'); 
	
		$user_data['User_Id']	=	md5(ANONYMOUS);
		$user_data['User_Perm']	=	"";
	}
	
	return $user_data;
}

//
// Pagination routine, generates
// page number sequence
//
function generate_pagination($num_items, $per_page, $start_item, $add_prevnext_text = TRUE)
{
	global $lang;

	$path_parts = pathinfo($_SERVER['SCRIPT_FILENAME']);
	$base_url = $path_parts["basename"] . "?" . substr($_SERVER['QUERY_STRING'], 0, strpos($_SERVER['QUERY_STRING'],"&start")===false?strlen($_SERVER['QUERY_STRING']):strpos($_SERVER['QUERY_STRING'],"&start"));
	$total_pages = ceil($num_items/$per_page);

	if ( $total_pages == 1 )
		return '';

	$on_page = floor($start_item / $per_page) + 1;
	$page_string = '';
	if ( $total_pages > 10 )
	{
		$init_page_max = ( $total_pages > 3 ) ? 3 : $total_pages;
		for($i = 1; $i < $init_page_max + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url. "&amp;start=" . ( ( $i - 1 ) * $per_page )  . '\';document.forms[0].submit();" >' . $i . '</a>';
			if ( $i <  $init_page_max )
				$page_string .= ' ';
		}

		if ( $on_page > 1  && $on_page < $total_pages )
		{
			$page_string .= ( $on_page > 5 ) ? ' ... ' : ', ';
			$init_page_min = ( $on_page > 4 ) ? $on_page : 5;
			$init_page_max = ( $on_page < $total_pages - 4 ) ? $on_page : $total_pages - 4;

			for($i = $init_page_min - 1; $i < $init_page_max + 2; $i++)
			{
				$page_string .= ($i == $on_page) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page )  . '\'; document.forms[0].submit();">' . $i . '</a>';
				if ( $i <  $init_page_max + 1 )
					$page_string .= ' ';
			}
			$page_string .= ( $on_page < $total_pages - 4 ) ? ' ... ' : ', ';
		}
		else
			$page_string .= ' ... ';

		for($i = $total_pages - 2; $i < $total_pages + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>'  : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) . '\';document.forms[0].submit();">' . $i . '</a>';
			if( $i <  $total_pages )
				$page_string .= " ";
		}
	}
	else
	{
		for($i = 1; $i < $total_pages + 1; $i++)
		{
			$page_string .= ( $i == $on_page ) ? '<font class=activePage>' . $i . '</font>' : '<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $i - 1 ) * $per_page ) . '\';document.forms[0].submit();">' . $i . '</a>';
			if ( $i <  $total_pages )
					$page_string .= " ";
		}
	}

	if ( $add_prevnext_text )
	{
		if ( $on_page > 1 )
			$page_string = ' <a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( ( $on_page - 2 ) * $per_page ) . '\'; document.forms[0].submit();">' . "Previous" . '</a>&nbsp;&nbsp;' . $page_string;
		else
			$page_string = '&nbsp;<font class="disabledText">Previous</font>&nbsp;' . $page_string;

		if ( $on_page < $total_pages )
			$page_string .= '&nbsp;&nbsp;<a class=pageLink href="javascript: document.forms[0].action=\'' . $base_url . "&amp;start=" . ( $on_page * $per_page ) . '\';document.forms[0].submit();">' . "Next" . '</a>';
		else
			$page_string .= '&nbsp;<font class="disabledText">Next</font>&nbsp;';
	}

	return $page_string;
}

#====================================================================================================
#	Function Name		:	readFileContent
#	Purpose				:	Read the content of file
#	Parameters			:	$file	: [Optional] gives file path
#	Return				:	Return HTML string for select box containing all parking types
#	Author				:	Dinesh Sailor
#	Creation Date		:	07-Oct-2005
#----------------------------------------------------------------------------------------------------
function readFileContent($filename)
{
	if(!file_exists($filename))
		return "File does not exists !!";
	
	$fd = fopen ($filename, "r"); 
	$contents = fread ($fd, filesize($filename)); 
	fclose ($fd); 

	return $contents;
}

#====================================================================================================
#	Function Name		:	writeFileContent
#	Purpose				:	Write content in the file
#	Parameters			:	$file	: [Optional] gives file path
#	Return				:	Return HTML string for select box containing all parking types
#	Author				:	Lamrin
#	Creation Date		:	25-June-2003
#----------------------------------------------------------------------------------------------------
function writeFileContent($filename, $contents, $create=false)
{
	if(!$create)
	{
		if(!file_exists($filename))
			return "File [$filename] does not exists !!";
	}
	
	$fd = fopen ($filename, "w"); 
	$ret = fwrite($fd, $contents, strlen($contents)); 
	fclose ($fd); 

	return $ret;
}


#====================================================================================================
#	Function Name		:	fileUpload
#	Purpose				:	Uploads the content of the file
#	Parameters			:	$File			:	File to be uploaded	$Upload_Type
#							$Upload_Tppe    :	Upload type
#	Return				:	If uploaded successfully then return uploaded file name
#							Else null string.
#	Author				:	Jignasa Naik
#	Creation Date		:	26-Apr-2003
#----------------------------------------------------------------------------------------------------
function fileUpload($File, $Upload_Type, $Dir_Path='')
{
	global $physical_path;
    $retVal 	= '';
    $destPath   = '';
    $destFile   = '';

	if(is_array($File))
    {
		switch($Upload_Type)
		{
			case CLASSIFIED_PHOTO:
				$destPath =	$physical_path['Classified_Photos'];	
				break;
			case AGENT_PHOTO:
				$destPath =	$physical_path['Agent_Photo'];	
				break;
			case RESOURCES:
				$destPath =	$physical_path['Resources'];	
				break;
			default:
	            $destPath = $Dir_Path."/";
	            break;
	    }
        $destFile 	= getUniqueFilePrefix(). $File['name'];
		$retVal 	= move_uploaded_file($File['tmp_name'], $destPath. $destFile);
		
		@chmod($destPath. $destFile, 0744);

        if($retVal)
			return $destFile;
        else
        	return '';
    }
    else
    {
    	return '';
    }
}

#====================================================================================================
#	Function Name		:	fileUpload
#	Purpose				:	Uploads the content of the file
#	Parameters			:	$File			:	File to be uploaded	$Upload_Type
#							$Upload_Tppe    :	Upload type
#	Return				:	If uploaded successfully then return uploaded file name
#							Else null string.
#	Author				:	Jignasa Naik
#	Creation Date		:	26-Apr-2003
#----------------------------------------------------------------------------------------------------
function fileUpload2($File, $uploadDirectory, $thumbSize='', $updatedFileName)
{
	global $physical_path;
	global $config;
	global $thumb;
	
	$retVal 	= '';
    $destPath   = '';
    $destFile   = '';

   	if(is_array($File))
    {
		# Define file name and path
        $destFile 			= strtolower(getUniqueFilePrefix(). ereg_replace("[^[:alnum:].]", "", $File['name']));
        $destFileFullPath	= $physical_path[$uploadDirectory]. $destFile;
		
		
		# Upload file
		$uploadStatus 	= move_uploaded_file($File['tmp_name'], $destFileFullPath);
		
		# If uploaded
        if($uploadStatus)
		{
			# change required permission
			@chmod($destFileFullPath, decoct($config['File_Perms']));
			
			# Delete old file
			if($updatedFileName)
				@unlink($physical_path[$uploadDirectory]. $updatedFileName);

			# Make thumb
			$thumb->image($destFileFullPath);
			$thumb->jpeg_quality(100);
			$i='';

			foreach($thumbSize as $key => $val)
			{
				# Set thumb size
				if(count($val) >= 2)
				{
					$thumb->size_smart($val[0], $val[1]);
					//$thumb->size_fix($val[0], $val[1]);
				}
				else
					$thumb->size_auto($val[0]);
				
				# Create thumb
				$thumb->get('thumb'. $i);

				# Delete old thumb
				if($updatedFileName)
					@unlink($physical_path[$uploadDirectory]. 'thumb'. $i. $updatedFileName);

				$i = $i + 1;
			}
			return $destFile;
		}
        else
        	return '';
    }
    else
    {
    	return '';
    }
}

function getUniqueFilePrefix()
{
    list($usec, $sec) = explode(" ",microtime());
	list($trash, $usec) = explode(".",$usec);
    return (date("YmdHis").substr(($sec + $usec), -10).'_');
}

#====================================================================================================
#	Function Name		:	fillDbCombo
#	Purpose				:	Generates list of Square Feets
#	Parameters			:	$arrName, $key, $val, $selected	: [Optional] Selected value for array
#	Return					:	Return HTML string for select box containing all parking types
#	Author					:	Lamrin
#	Creation Date		:	16-July-2003
#----------------------------------------------------------------------------------------------------
function fillDbCombo($arrName, $key, $val, $selected='')
{
 	global $db;
	$strHTML = "";
	$i = 0;
	
    while($i < $db->num_rows())
    {
		$db->next_record();
		$strHTML .= "<option value=\"". $db->f($key). "\"";

		if($selected == $db->f($key))
        	$strHTML .= " selected ";
		if(is_array($val))
			$strHTML .= ">".$db->f($val[0])." ".$db->f($val[1])." ( ".$db->f($val[2]). " ) </option>";
		else
			$strHTML .= ">".$db->f($val). "</option>";
	
		$i++;
    }

	$db->free();
	return $strHTML;
}


function fillPayLevelCombo($arrName, $key, $val1='',$val2='',$str='', $selected='')
{
 	global $db;
	$strHTML = "";
	$i = 0;
    while($i < $db->num_rows())
    {
		$db->next_record();

    	$strHTML .= "<option value=\"". $db->f($key). "\"";
    	if($selected == $db->f($key))
        	$strHTML .= " selected ";
			$strHTML .= ">".$db->f($val1)."    ".$db->f($val2)."".$str."</option>";
		$i++;
    }
	
	$db->free();
	return $strHTML;
}
#====================================================================================================
#	Function Name	:   getstatelist
#	Purpose			:	Generate JavaScript Array of Email
#	Parameters		:	$selected	-	(Optional) Selected Email Id
#	Return			:	returns the HTML Code the generate the list box
#	Author			:	Jignasa Naik
#	Creation Date	:	14-June-2003
#----------------------------------------------------------------------------------------------------
function getstatelist($selected='')
{
	global $db;

	$sql = " SELECT * FROM ".STATE_MASTER
		 . " ORDER BY state_id";

	$db->query($sql);
	$db->next_record();

	$strJSArray = '';
	$strJSArray = 'var arrState = new Array();';
	//print $db->num_rows();
	for($i=0; $i < $db->num_rows();)
    {
	    $country_id = $db->f('country_id');

		$strJSArray .= "arrState.length++;";
		$strJSArray .= "arrState[arrState.length-1] = new Array();";
		$strJSArray .= "arrState[arrState.length-1].length++;";
		$strJSArray .= "arrState[arrState.length-1][0] = '". $db->f('country_id'). "';";

	    for($j=1; $i<$db->num_rows() && $country_id==$db->f('country_id'); $i++)
        {
	        $strJSArray .= "arrState[arrState.length-1].length+=3;";
			$strJSArray .= "arrState[arrState.length-1][".$j++."] = '". $db->f('state_id'). "';";
			$strJSArray .= "arrState[arrState.length-1][".$j++."] = '". $db->f('state_title'). "';";
			$strJSArray .= "arrState[arrState.length-1][".$j++."] = '". addslashes($db->f('state_title')). "';";
			$db->next_record();
        }
    }
//	$strJSArray .= "alert(arrState.length)";
    return $strJSArray;
}

function formatDate($dateValue, $formate='d-M-Y')
{
	ereg ("([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})", $dateValue, $datePart);
	return makeDate($datePart[3], $datePart[2], $datePart[1], $formate);
}

function makeDate($intDay, $intMonth, $intYear, $formate='d-M-Y')
{
	return date ($formate, mktime (0,0,0,$intMonth, $intDay, $intYear));
}

////change the date to standard format // input yyyy-mm-dd
function standard_dateformat($in_date) 
{
	global $lang;
	$pattern = "([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})";
	ereg( $pattern, $in_date, $submitdate);
	$out_date = $submitdate[3]." ".$lang['Month_List'][$submitdate[2]].", ".$submitdate[1];
	return $out_date;
}

#====================================================================================================
#	Function Name		:	get_WebConfig
#	Purpose				:	Get the web site config details
#	Parameters			:	none
#	Return				:	Return the record set of config
#	Author				:	Dinesh Sailor
#	Creation Date		:	03-Jul-2003
#----------------------------------------------------------------------------------------------------
function get_WebSiteConfig()
{
	global $db;

	$sql = " SELECT * "
		.  "FROM " .WEBSITE_CONFIG;

	$db->query($sql);
	return $db->num_rows();
}

function db_To_php($date, $format) // Parameter format mm/dd/yyyy
{
	# Spite date in day, month and year
	list($y,$m,$d)	=	explode("-", $date);

	# Make timestemp
	$datetime		=	mktime (0,0,0, $m, $d, $y); 
	
	# return date in required formate
	return date($format, $datetime);
}

function dbdate_To_timestamp($date) // Parameter format mm/dd/yyyy
{
	# Spite date in day, month and year
	list($y,$m,$d)	=	explode("-", $date);

	# Make timestemp
	return	mktime (0,0,0, $m, $d, $y); 
}

function time_diff($from, $to, $type) {

	if ($from > $to) {
		$t = $to;
		$to = $from;
		$from = $t;
	}

	$year1 = date("Y", $from);
	$year2 = date("Y", $to);
	$month1 = date("n", $from);
	$month2 = date("n", $to);
	$day1 = date("j", $from);
	$day2 = date("j", $to);
	
	if ($day2 < $day1) {
		if (($month2 - 1) == 4 || ($month2 - 1) == 6 || ($month2 - 1) == 9 || ($month2 - 1) == 11) {
			$day2 += 30;
			$month2 --;
		} else if (($month2 - 1) == 2) {
			if (date("L", $to)) {
				$day2 += 29;
				$month2 --;
			} else {
				$day2 += 28;
				$month2 --;
			}
		} else {
			$day2 += 31;
			$month2 --;
		}
	}
	$days = $day2 - $day1;

	if ($month2 < $month1) {
		$month2 += 12;
		$year2 --;
	}
	$months = $month2 - $month1;
	$years = $year2 - $year1;

	switch($type)
	{
		case YEAR:
			return $years;
		case MONTH:
			return ($years*12)+$months;
		case DAY:
			return ($years*12)+($months*30)+$days;
	}
}

function convDate($date) // Parameter format mm/dd/yyyy
{
	list($m,$d,$y)=explode("/",$date);
	$dt=$y."-".$m."-".$d;
	return $dt;  // Return format yyyy-mm-dd  Database format Y-m-d
}

function RevDate($date) // Parameter format yyyy-mm-dd
{
	list($y,$m,$d)=explode("-",$date);
	$dt=$m."/".$d."/".$y;
	return $dt;  // Return format dd/mm/yyyy 
}

function fillArrayCombo($arrName, $selected='')
{
	$strHTML = "";
	reset($arrName);
	while(list($key,$val) = each($arrName))
    {	
    	$strHTML .= "<option value=\"". $key. "\"";
		if($selected == (string)$key)
        	$strHTML .= " selected ";
		$strHTML .= ">".$val. "</option>";
    }
	return $strHTML;
}

function fillDbCombo1($selected='')
{
	global $db;
	$strHTML = "";
	
	for($i=0;$i<$db->num_rows();$i++)
    {	
    	$strHTML .= "<option value=\"". $db->type_id. "\"";
		if($selected == (string)$key)
        	$strHTML .= " selected ";
		$strHTML .= ">".$db->type_title. "</option>";

    }

	return $strHTML;
}

#====================================================================================================
#	Function Name		:	Country_List
#	Purpose				:	Get the web site config details
#	Parameters			:	none
#	Return				:	Return the record set of config
#	Author				:	Dinesh Sailor
#	Creation Date		:	03-Jul-2003
#----------------------------------------------------------------------------------------------------
function Country_List()
{
	global $db;

	$sql = " SELECT * FROM ".COUNTRY_MASTER." ORDER BY country_name";
	$res=$db->query($sql);

	return $res;
}

function Show_Country_Details($country_id)
{
	global $db1;

	$sql = " SELECT * FROM ".COUNTRY_MASTER." "
		 . " WHERE country_id = '".$country_id."' ";

	$res=$db1->query($sql);
	return $res;
}
#====================================================================================================
#	Function Name		:	State_List
#	Purpose				:	Get the web site config details
#	Parameters			:	none
#	Return				:	Return the record set of config
#	Author				:	Dinesh Sailor
#	Creation Date		:	03-Jul-2003
#----------------------------------------------------------------------------------------------------
function State_List($country_id)
{
	global $db;

	$sql = " SELECT * FROM ".STATE_MASTER." "
		 . " WHERE country_id = '".$country_id."' order by state_title";

	$res=$db->query($sql);
	return $res;
}

function Show_State_Details($state_id)
{
	global $db1;

	$sql = " SELECT * FROM ".STATE_MASTER." "
		 . " WHERE state_id = '".$state_id."' ";

	$res=$db1->query($sql);
	return $res;
}
#====================================================================================================
#	Function Name		:	City_List
#	Purpose				:	Get the web site config details
#	Parameters			:	none
#	Return				:	Return the record set of config
#	Author				:	Dinesh Sailor
#	Creation Date		:	03-Jul-2003
#----------------------------------------------------------------------------------------------------
function City_List($country_id,$state_id)
{
	global $db;

	if($country_id && $state_id)
	{
		$sql = " SELECT * FROM " . CITY_MASTER
			 . " WHERE country_iso = '".$country_id."' and state_iso = '".$state_id."' order by city_title ";
	}
	//print $sql;
	$res=$db->query($sql);
	return $res;
}

function Show_City_Details($city_id)
{
	global $db1;

	$sql = " SELECT * FROM ".CITY_MASTER." "
		 . " WHERE city_id = '".$city_id."' ";

	$res=$db1->query($sql);
	return $res;
}

#====================================================================================================
#	Function Name		:	getUniqueKey
#	Purpose				:	Generates list of years
#	Parameters			:	$selected	: [Optional] Selected value for date
#							$startyear  : Starting value of year
#							$count		: No of years to be generated
#	Return				:	Return HTML string for select box containing years
#	Author				:	Jignasa Naik
#	Creation Date		:	29-Apr-2003
#----------------------------------------------------------------------------------------------------
function getUniqueKey()
{
	return (mktime (date('G'),date('i'),date('s'),date('m'),date('d'),date('Y')));
}	
#====================================================================================================
#	Function Name		:	get_EmailConfig
#	Purpose				:	Get the web site config details
#	Parameters			:	none
#	Return				:	Return the record set of config
#	Author				:	Dinesh Sailor
#	Creation Date		:	03-Jul-2003
#----------------------------------------------------------------------------------------------------
function get_EmailConfig()
{
	global $db;

	$sql = " SELECT * "
		.  " FROM " .EMAIL_CONFIG;

	return($db->query($sql));
}

////generate random password
function rand_pass($num_letters) 
{ 
	$array = array( 
    	             "a","b","c","d","e","f","g","h","i","j","k","l", 
        	         "m","n","o","p","q","r","s","t","u","v","w","x","y", 
            	      "z","1","2","3","4","5","6","7","8","9" 
                	 ); 

	//$num_letters = 10; 
	$uppercased = 3; 
  	mt_srand ((double)microtime()*1000000); 
  	for($i=0; $i<$num_letters; $i++) 
    	$pass .= $array[mt_rand(0, (count($array) - 1))]; 
  	
	for($i=1; $i<strlen($pass); $i++) 
	{ 
    	if(substr($pass, $i, 1) == substr($pass, $i-1, 1)) 
      		$pass = substr($pass, 0, $i) . substr($pass, $i+1); 
  	} 
  	for($i=0; $i<strlen($pass); $i++) 
	{ 
    	if(mt_rand(0, $uppercased) == 0) 
			$pass = substr($pass,0,$i) . strtoupper(substr($pass, $i,1)) . 
			substr($pass, $i+1); 
	} 
	$pass = substr($pass, 0, $num_letters); 
	return($pass); 
}

#====================================================================================================
#	Function Name		:	date_diff
#	Purpose				:	This function give Diff bet two date in No of Days
#	Parameters			:	$fromDate and $toDate in Mysql format.
#	Return				:	diff in Days
#	Creation Date		:	20-Jan-2005
#----------------------------------------------------------------------------------------------------
function date_diff($dat1,$dat2)
/* Dat1 and Dat2 passed as "YYYY-MM-DD" */
{
  $tmp_dat1 = mktime(0,0,0,
     substr($dat1,5,2),substr($dat1,8,2),substr($dat1,0,4));
  $tmp_dat2 = mktime(0,0,0,
     substr($dat2,5,2),substr($dat2,8,2),substr($dat2,0,4));

  $yeardiff = date('Y',$tmp_dat1)-date('Y',$tmp_dat2);
  /* a leap year in every 4 years and the days-difference */
  $diff = date('z',$tmp_dat1)-date('z',$tmp_dat2) + 
           floor($yeardiff /4)*1461;

  /* remainder */
  for ($yeardiff = $yeardiff % 4; $yeardiff>0; $yeardiff--)
   {
     $diff += 365 + date('L',
         mktime(0,0,0,1,1,
           intval(
             substr(
               (($tmp_dat1>$tmp_dat2) ? $dat1 : $dat2),0,4))
           -$yeardiff+1));
   }

  return $diff;
} 

///calculate age from birthdate
function getage($_datefrom)  ///Parameter format yyyy-mm-dd
{
	list($year,$mon,$day) = split('[/.-]', $_datefrom);
	$year_c = date("Y");
	$age = $year_c - $year;
	return $age;
}

/////Get Affiliate Site Config
function Get_Affiliate_Site_Config()
{
	global $db;
	
	$sql  = " SELECT count(*) as cnt FROM " . AFF_SITE_CONFIG;
	$db->query($sql);

	if($db->num_rows())
	{
		$db->free();
		$sql = " SELECT * FROM ". AFF_SITE_CONFIG ;
		$rs = $db->query($sql);
	}
	
    return ($rs);
}

/////Get Subadmin
function Get_Subadmin($user_id)
{
	global $db;
	
	$sql = " SELECT * FROM ".ADMIN_MASTER
		 . " WHERE admin_auth_id = '".$user_id."'";
	
	return($db->query($sql));
}

#====================================================================================================
#	Function Name		:	getTopNavigation
#	Purpose				:	Get the web site config details
#	Parameters			:	none
#	Return				:	Return the record set of config
#	Author				:	Dinesh Sailor
#	Creation Date		:	03-Jul-2003
#----------------------------------------------------------------------------------------------------
function getTopNavigation($lang, $navigateLink, $actionType, $actionTypeForOther='')
{
	global $physical_path;
	global $config;

	$tpl2 = new TemplatePower($physical_path['Templates_Root']."cpanel_topmenu.". $config['tplEx']);
	$tpl2->prepare();

	# Set default option
/*	if($selOption == '')
	{
		# Get the first option
		list($key, $option) = each($lang);
		$selOption			= $key;
	}
*/
	# Get the first option
	list($key, $option) = each($lang);
	$selOption			= $key;

	# Set selected option
	$selOption		=	$_GET['type'];

	# Define Subkey
	$subKey 	= ($navigateLink[$key][SUBKEY])?$navigateLink[$key][SUBKEY]:'sub';

	# Set the selected suboption
	$selSubOption	=	$_GET[$subKey];

	# Prepare the first level of menu
    foreach($lang as $key => $option)
    {
		if($selOption == $key)
		{
			$optionType 		= $key;
			$subLang 			= $lang[$key];
			$subNavigateLink 	= $navigateLink[$key];
			$CSS_Style 			= 'selectedTopNavigation';
		}
		else
		{
			$CSS_Style = 'stdTopNavigation';
		}

		# Get the first option
		list($firstKey, $fistSuboption) = each($navigateLink[$key][SUBOPTION]);

		# Define link
		$optionLink = $navigateLink[$key][LINK]. '?type='. $key.'&'. $subKey. '='. $fistSuboption. '&Action='. ($actionTypeForOther==''?$actionType:$actionTypeForOther);

		# Show option
		$tpl2->newBlock('B_Option');
		$tpl2->assign(array("Option_Title"	=>  $option[TITLE],
							"Option_Link"	=>  $optionLink,
							"CSS_Style"		=>	$CSS_Style,
							));
    }

	// Prepare the second level of menu
	// -----------------------------------------------------------
/*
	# Set default sub option
	if($selSubOption == '')
	{
		# Get the first option
		list($key, $val) = each($subNavigateLink[SUBOPTION]);
		$selSubOption		= $val;
	}
*/	
	if(is_array($subLang[SUBOPTION]))
	{
		$i=1;
		foreach($subLang[SUBOPTION] as $key => $option)
		{
			if($selSubOption == $subNavigateLink[SUBOPTION][$key])
			{
				$CSS_Style = 'selectedTopSubNavigation';
				# Define link
				$optionLink = '#';
			}
			else
			{
				$CSS_Style = 'stdTopSubNavigation';
				# Define link
				$optionLink = $subNavigateLink[LINK]. '?type='. $optionType. '&'. $subKey. '='. $subNavigateLink[SUBOPTION][$key]. '&Action='. ($actionTypeForOther==''?$actionType:$actionTypeForOther);
			}
	
			# Show option
			$tpl2->newBlock('B_SubOption');
			$tpl2->assign(array("Option_Title"	=>  $option[TITLE],
								"Option_Link"	=>  $optionLink,
								"CSS_Style"		=>	$CSS_Style,
								"Seperator"		=>	(count($subLang[SUBOPTION])>$i)?'|':'',
								));
			$i++;
		}
	}

	# Return top navigation
	return $tpl2->getOutputContent();
}

#====================================================================================================
#	Function Name		:	getActionLink
#	Purpose				:	Get the web site config details
#	Parameters			:	none
#	Return				:	Return the record set of config
#	Author				:	Dinesh Sailor
#	Creation Date		:	03-Jul-2003
#----------------------------------------------------------------------------------------------------
function getActionLink($navigateLink, $actionType, $selOption='', $selSubOption='')
{
	# Get the first option
	list($key, $option) = each($navigateLink);

	# Set selected option
	$selOption		=	($selOption!='')?$selOption:(($_GET['type']!='')?$_GET['type']:$key);

	# Define Subkey
	$subKey 		= 	($navigateLink[$key][SUBKEY])?$navigateLink[$key][SUBKEY]:'sub';

	# Set the selected suboption
	$selSubOption	=	($selSubOption!='')?$selSubOption:$_GET[$subKey];

	return $navigateLink[$selOption][LINK]. '?type='. $selOption. '&'. $subKey. '='. $selSubOption. '&Action='. $actionType;
}

#====================================================================================================
#	Function Name		:	DateArithmatic
#	Purpose				:	Get the Date after/before no of days,month,year
#	Parameters			:	
#	Return				:	
#	Author				:	Parimal Gajjae
#	Creation Date		:	22-Oct-2005
#----------------------------------------------------------------------------------------------------
function DateArithmatic($givenDate,$day=0,$month=0,$year=0,$Format='F d, Y')
{
	$timeStamp=strtotime($givemDate);
	return date($Format,mktime(0,0,0,date("m",$timeStamp) + $month ,date("d",$timeStamp) + $day,date("Y",$timeStamp) + $year));
}

#====================================================================================================
#	Function Name		:	getDateList()
#	Purpose				:	Get Date List
#	Parameters			:	
#	Return				:	
#	Author				:	Parimal Gajjae
#	Creation Date		:	22-Oct-2005
#----------------------------------------------------------------------------------------------------
function getDateList($selected='')
{
	$strHTML = "";
	
    for($i=1;$i<32;$i++)
    {
    	$strHTML .= "<option value=\"". $i. "\"";
    	if($selected == $i)
        	$strHTML .= " selected ";
		$strHTML .= ">".$i. "</option>";
    }
	return $strHTML;
}

#====================================================================================================
#	Function Name		:	getAppointmentDate
#	Purpose				:	Generates list of dates for appointments
#	Parameters			:	$selected	: [Optional] Selected value for appontment
#	Return				:	Return HTML string for select box containing all appointment
#	Author				:	Jignasa
#	Creation Date		:	20-Aug-2003
#----------------------------------------------------------------------------------------------------
function getAppointmentDate($selected='')
{
	global $lang;

	$days_vars = array( 0=>"Monday", 1=>"Tuesday", 2=>"Wednesday", 3=>"Thursday", 4=>"Friday", 5=>"Saturday", 6=>"Sunday");
	$currdate = date("d") + 0;
	$currmonth = date("n");
	$curryear = date("Y");
	$date=getdate(mktime(0,0,0,$currmonth+1,0,$curryear));
	$daysinmonth = $date["mday"];

	$strHTML = "";
    for($i=0; $i < 15; $i++)
    {
    	if($currmonth < 10)
			$monthdisp = "0".$currmonth;
		else
			$monthdisp = $currmonth;
			 	 	
    	if($currdate < 10)
			$datedisp = "0".$currdate;
		else
			$datedisp = $currdate;

		$timestamp = mktime(0,0,0,$currmonth,$currdate-1,$curryear);
		$date = getdate ($timestamp);
		$dayofweek = $date['wday'];
		$strHTML .= "<option value=\"". $monthdisp."/".$datedisp."/".$curryear." - ".$days_vars[$dayofweek]. "\"";
    	if($selected == $currdate)
        	$strHTML .= " selected ";
		$strHTML .= ">".$monthdisp."/".$datedisp."/".$curryear." - ".$days_vars[$dayofweek]. "</option>";
		$currdate++;
		if($currdate > $daysinmonth)
		{
			$currdate = 1;
			$currmonth++;
		}
    }
	return $strHTML;
}

function updateCartDateTime()
{
	global $db;
	if(isset($_SESSION['User_Id']))
	{
	  $newDateTime = mktime (date('G'),date('i'),date('s'),date('m'),date('d'),date('Y'));
	
		$sql = " UPDATE ".CART_MASTER." SET "
			  . " cartDateTime		=   '". $newDateTime ."' ";
		$sql .= " WHERE user_Id		= 	'". $_SESSION['User_Id'] ."' ";
		$db->query($sql);
		$sql = '';
		$db->free();
	}
		   
	return true;
}
function deleteOldCartItem()
{
	global $db;
	global $dbtemp;
	$itemType = '';

	$currentTime = mktime (date('G'),date('i'),date('s'),date('m'),date('d'),date('Y')) - 3600;
	
	$sql = "SELECT * FROM ".CART_MASTER 
		   . " WHERE  ". CART_MASTER .".cartDateTime < '".$currentTime."' AND ". CART_MASTER .".confirmationNo = ''"; 
	$db->query($sql);
	while($db->next_record())
	{
		$cartId = $db->f('cartId');
			#=============== Delete Process Start Here============#
			   $newsql = "SELECT * FROM ".CART_MASTER 
					    . " WHERE  ". CART_MASTER .".cartId = '".$cartId."'"; 
				$dbtemp->query($newsql);
				$dbtemp->next_record();
				$itemType = $dbtemp->f('itemType');
	
						if($itemType == 2)
							{		
								$tsql = " DELETE FROM ".CARTTRANSFER
										 . " WHERE cartId = '". $cartId ."' ";
									$dbtemp -> query($tsql);
							}	

						$tsql = " DELETE FROM ".CART_MASTER
							 . " WHERE cartId = '". $cartId ."' ";
						$dbtemp -> query($tsql);

					$newsql = '';
					$tsql = '';
					$dbtemp->free();
			#================Delete Process End Here ====#
	}	
	$sql = '';
	$db->free();
	return true;
}

#========not changing this function currently to avoid errors instead created next function=========#
function updateCartUser()
{
	global $db;
	if(isset($_SESSION['User_Id']))
	{
	 	
		$sql = " UPDATE ".CART_MASTER." SET "
			  . " user_id					=   '". $_SESSION['User_Id'] ."' ";
		$sql .= " WHERE user_Id		= 	'". $_SESSION['User_Id'] ."' ";
		$db->query($sql);
		$sql = '';
		
		$sql = " UPDATE ".CART_MASTER." SET "
			  . " user_Id			=   '' ";
		$sql .= " WHERE user_Id		= 	'". $_SESSION['User_Id'] ."' ";
		$db->query($sql);
		
		$db->free();
		session_start();
		session_unregister("User_Id");
	}
		   
	return true;
}
#===============================================================================#
function genTransferRequestNo($transferCartId)
{
  return("R". date("Ym"). substr("0000".$transferCartId, -4, 4));
}

#===============================================================================#
function updateUserinCart($userId)
{
	global $db;
	$sql = " UPDATE ".CART_MASTER." SET "
		  . " user_id			=   '". $userId ."' ";
	$sql .= " WHERE user_id		= 	'". $_SESSION['User_Id'] ."' ";
	
	//print "sql=".$sql;
	
	$db->query($sql);
	$db->free();
		
	return true;
}


?>