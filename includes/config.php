<?php
#====================================================================================================
#	File Name		:	config.php
#----------------------------------------------------------------------------------------------------
#	Purpose			:	This file contain all application configaration details
#	Author			:	Adnan Sarela
#	Creation Date	:	07-Oct-2005
#	Copyright		:	Copyrights  2005 DotInfosys
#	Email			:	dinesh@dotinfosys.com
#	History			:
#						Date				Author					Remark
#						07-Oct-2005			Adnan Sarela			Initial Release
#
#====================================================================================================

#====================================================================================================
#	Check for Hacking Attempt
#----------------------------------------------------------------------------------------------------
/*if ( !defined('IN_SECURE') )
{
	die("Hacking attempt");
}
*/#====================================================================================================
#	Default Setting		- Adnan Sarela (07-Oct-2005)
#----------------------------------------------------------------------------------------------------
$config['Table_Prefix']			= $config['Table_Prefix']."_";
$config['tplEx'] 				= '.tpl';
$config['Folder_Perms'] 		= decoct('0765');
$config['File_Perms'] 			= decoct('0744');
$config['picDefault'] 			= 'default.jpg';
$config['picSizeAuto'] 			= 100;

#Default Setting For site Templates

#====================================================================================================
#	Database		- Adnan Sarela (07-Oct-2005)
#----------------------------------------------------------------------------------------------------
switch($config['Server_Name'])
{
	// Dotworld (local)
	case "DOTWORLD":
	    $config['DB_Type']      = 'mysql';
	    $config['DB_Host']      = 'localhost';
	    $config['DB_Name']      = 'cancuntransfer';
	    $config['DB_User']      = 'root';
	    $config['DB_Passwd']    = 'dot1235';
        break;

    // Dotnet [local]
	case "DOTNET":
	    $config['DB_Type']      = 'mysql';
	    $config['DB_Host']      = 'manoj';
	    $config['DB_Name']      = 'cancuntransfer';
	    $config['DB_User']      = 'root';
	    $config['DB_Passwd']    = 'dot1235';
        break;

    // Dot Infosys (Company Server)
	case "CANCUNTRANSFER.DOTINFOSYS.COM":
	    $config['DB_Type']      = 'mysql';
	    $config['DB_Host']      = 'localhost';
	    $config['DB_Name']      = 'wwwuser_cancuntransfer';
	    $config['DB_User']      = 'wwwuser_cancuntransfer';
	    $config['DB_Passwd']    = 'swiss157';
        break;

	default:
	    $config['DB_Type']      = 'mysql';
	    $config['DB_Host']      = 'localhost';
	    $config['DB_Name']      = 'cancunai_cuntransfers';
	    $config['DB_User']      = 'cancunai_cancunt';
	    $config['DB_Passwd']    = 'cantransfers';
    	break;
}
?>