<?php session_start();?>
<?php 
if ($_POST['mail'] == "") {
  header("Location: http://cancun-shuttle.com/index.php");
  die();
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Contact US | Official Airport Transportation</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="cancun airport transfers, kinmont transfers contact us, cancun shuttle, cancun transfers"/>
  <meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
  <meta property="og:url" content="http://www.cancun-shuttle.com/" />
  <meta property="og:title" content="Cancun Airport shuttle">
  <meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="http://www.cancun-shuttle.com./img/og-cancunshuttle.jpg">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="300">
  <meta property="og:image:height" content="300">
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="es_ES" />
  <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
  <link href="../css/main-style.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css" type="text/css">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("../_structure/menu-int.php"); ?>
<?php include("_structure/tels-box.php"); ?>
<div class="container">
   <div class="row">
        <div class="col-xs-12 col-md-12">
        <br><br><br><br>
        <p>Thank you for booking with us, we have sent an email to the address you indicated, please check your inbox, sometimes your mail client can put it on the spam tray, please check it.<br><br>
If you have any questions regarding our services, please contact us or call us at the following office phone: Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25, Office: 998 214 90 12, we are for Serve them.
</p>
        </div>
       </div>
      </div>
<br><br><br>
<?php include("../_structure/footer.php"); ?>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>
<?php
//Seleccion de transfers
@$tiporeserva=$_SESSION['tiporeserva'];
@$checkon = isset($_POST['opcion']) ? $_POST['opcion'] : 'off';
@$zonatra = $_POST['destr'];
//Seleccion de transfers
@$tiporeserva= $_POST['seleccionrs'];
@$costetot= $_POST['prctrn'];
@$preciomail= $_POST['prctrn'];
//declaracion de variables que siempre llegaran
@$tipollegada = $_POST['tipollegada'];
@$hoteldest = $_POST['hoteldest'];
@$pax = $_POST['pax'];
@$llegvuelo = $_POST['llegvuelo'];
@$numvuelo = $_POST['numvuelo'];
@$horalleg = $_POST['horalleg'];
@$ampm = $_POST['ampm'];
@$aerolinea = $_POST['aerolinea'];

@$nombre = $_POST['nombre'];
@$pais = $_POST['pais'];
@$mail = $_POST['mail'];
@$telefono = $_POST['telefono'];
@$comentarios = $_POST['comentarios'];

$path = 'parte1.html';
if(file_exists($path)){
    $path = file_get_contents($path);
}
$path2 = 'parte2.html';
if(file_exists($path2)){
    $path2 = file_get_contents($path2);
}
if ($tiporeserva != "Round Trip") {

  //es one way ****

  //correo local
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/html; charset=iso-8859-1\n";
//Pasamos al cliente
$headerclt ="Content-Type: text/html; charset=iso-8859-1\n";
$headerclt.="From: Cancun Transfers <info@cancun-shuttle.com>\r\n";
$headerclt.="Replay-to: info@cancun-shuttle.com \r\n";
$headerclt.="Return-path: info@cancun-shuttle.com \r\n";
//Mensaje Principal
$mensaje = '<br><br>'.$nombre.'
                            Thanks for reserve with Kinmont Cancun Shuttle

                            </td>
                            <td class="hide" width="23">&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="20">&nbsp;</td>
                            <td class="hide" height="20">&nbsp;</td>
                          </tr>
                        </table>
                        <table width="50%" border="0" cellspacing="0" cellpadding="0" align="right" class="inner">
                          <tr>
                            <td style="font:14px/19px Arial, Helvetica, sans-serif; color:#333332;">'  . " \r\n";
$mensaje .= "Requested service: " . $tipollegada .'  '.$tiporeserva. "<br> \r\n";
$mensaje .= "Passengers: " . $pax . "<br> \r\n";
$mensaje .= "Arrival hotel: " . $hoteldest . "<br> \r\n";
$mensaje .= "Airline: " . $aerolinea . "<br> \r\n";
$mensaje .= "No. Flight: " . $numvuelo . "<br> \r\n";
$mensaje .= "Arrival date: " . $llegvuelo . "<br> \r\n";
$mensaje .= "Pickup Time: " . $horalleg .' '.$ampm. "<br> \r\n";
$mensaje .= "Your e-mail is: " . $mail . "<br> \r\n";
$mensaje .= "Your Movil phone is: " . $telefono . "<br> \r\n";
$mensaje .= "Your country is: " . $pais . "<br> \r\n";
$mensaje .= "Message had send us: " . $comentarios . "<br> \r\n";
$mensaje .= '</td>
                          </tr>
                          <tr>
                            <td height="20">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="left" class="readmore-button"> Total  '. $preciomail .'  USD <br>Send Date'.date('d/m/Y', time()).' </td>'. " \r\n";
//Envio de datos extra a mail principal
$datoscontrol ="<br><br>La casilla de reserva en linea esta". $checkon . "<br> \r\n";
//correo de envio
$para = 'info@cancun-shuttle.com';
$asunto = 'Mensaje cancun-shuttle';


mail($para, $asunto, utf8_decode($path.$mensaje.$path2.$datoscontrol), $header);
sleep(2); #Pausa de 2 Segundos cliente
mail($mail, "Cancun Transfers reservations", utf8_decode($path.$mensaje.$path2), $headerclt);

//Mostrar html 



} else {
  //es round trip
@$horasal = $_POST['horasal'];
@$salampm = $_POST['salampm'];
@$salvuelo = $_POST['salvuelo'];
@$aerosalida = $_POST['aerosalida'];

//correo local
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/html; charset=iso-8859-1\n";
//Pasamos al cliente
$headerclt ="Content-Type: text/html; charset=iso-8859-1\n";
$headerclt.="From: Cancun Transfers <info@cancun-shuttle.com>\r\n";
$headerclt.="Replay-to: info@cancun-shuttle.com \r\n";
$headerclt.="Return-path: info@cancun-shuttle.com \r\n";
//Mensaje Principal
$mensaje = '<br><br>'.$nombre.'
                            Thanks for reserve with Kinmont Cancun Shuttle

                            </td>
                            <td class="hide" width="23">&nbsp;</td>
                          </tr>
                          <tr>
                            <td height="20">&nbsp;</td>
                            <td class="hide" height="20">&nbsp;</td>
                          </tr>
                        </table>
                        <table width="50%" border="0" cellspacing="0" cellpadding="0" align="right" class="inner">
                          <tr>
                            <td style="font:14px/19px Arial, Helvetica, sans-serif; color:#333332;">'  . " \r\n";
$mensaje .= "Requested Service: " . $tipollegada .'  '.$tiporeserva. "<br> \r\n";
$mensaje .= "Passengers: " . $pax . "<br> \r\n";
$mensaje .= "Arrival hotel: " . $hoteldest . "<br> \r\n";
$mensaje .= "Airline: " . $aerolinea . "<br> \r\n";
$mensaje .= "No. Flight: " . $numvuelo . "<br> \r\n";
$mensaje .= "Arrival date: " . $llegvuelo . "<br> \r\n";
$mensaje .= "Arrival Time: " . $horalleg .' '.$ampm. "<br> \r\n";
$mensaje .= "Pickup time: " . $aerosalida . "<br> \r\n";
$mensaje .= "Depature date: " . $salvuelo . "<br> \r\n";
$mensaje .= "Depature flight time: " . $horasal.' '.$salampm. "<br> \r\n";
$mensaje .= "Your email is: " . $mail . "<br> \r\n";
$mensaje .= "Your Movil phone is: " . $telefono . "<br> \r\n";
$mensaje .= "Your country is: " . $pais . "<br> \r\n";
$mensaje .= "Message had send us: " . $comentarios . "<br> \r\n";
$mensaje .= '

                            </td>
                          </tr>
                          <tr>
                            <td height="20">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="left" class="readmore-button"> Total'. $preciomail .'  USD <br>Send Date'.date('d/m/Y', time()).' </td>'. " \r\n";
//Envio de datos extra a mail principal
$datoscontrol ="<br><br>La casilla de reserva en linea esta ". $checkon . "<br> \r\n";
//correo de envio
$para = 'info@cancun-shuttle.com';
$asunto = 'Mensaje cancun-shuttle';


mail($para, $asunto, utf8_decode($path.$mensaje.$path2.$datoscontrol), $header);
sleep(2); #Pausa de 2 Segundos cliente
mail($mail, "Cancun Transfers reservations", utf8_decode($path.$mensaje.$path2), $headerclt);

}
?>

<?php

if ($checkon =="on") { ?>
<body onload="document.paypalform.submit();">
<form action="https://www.paypal.com/cgi-bin/webscr" name="paypalform" method="post">
<input name="cmd" type="hidden" value="_cart" />
<input name="upload" type="hidden" value="1" />
<input name="business" type="hidden" value="info@cancun-transfers.com" />
<input name="shopping_url" type="hidden" value="http://www.cancun-transfers.com/" />
<input name="currency_code" type="hidden" value="USD" />


<input name="return" type="hidden" value="http://www.cancun-shuttle.com" />
<input name="notify_url" type="hidden" value="www.cancun-shuttle.com/paypal_ipn.php" />
<input name="cancel_return" type="hidden" value="http://www.cancun-shuttle.com/">


<input name="rm" type="hidden" value="2" />

<input name="item_name_1" type="hidden" value=<?php echo '"'.$tipollegada.' - '.$tiporeserva.' - '.$zonatra.'"'; ?> />
<input name="amount_1" type="hidden" value=<?php echo '"'.$costetot.'"'; ?> />
<input name="quantity_1" type="hidden" value="1" />  <?php


}



?>