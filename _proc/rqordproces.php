<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
  <title>Contact US | Official Airport Transportation</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="cancun airport transfers, kinmont transfers contact us, cancun shuttle, cancun transfers"/>
  <meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
  <meta property="og:url" content="http://www.cancun-transfers.com/" />
  <meta property="og:title" content="Cancun Airport shuttle">
  <meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="300">
  <meta property="og:image:height" content="300">
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="es_ES" />
  <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
  <link href="../css/main-style.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css" type="text/css">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("../_structure/menu-int.php"); ?>

<div class="tel-box">
  <div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<div class="container">
   <div class="row">
        <div class="col-xs-12 col-md-12">
        <p>Gracias por reservar con nosotros, hemos enviado un correo a la dirección que nos indicó, por favor revise su bandeja de entrada, algunas veces su cliente de correo puede ponerlo en la bandeja de spam, favor de verificarlo.<br><br>
Si tiene alguna duda con respecto a nuestros servicios, por favor contáctenos o llámenos a los siguientes telefono de oficina: Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Oficina: 998 214 90 12, estamos para servirles.
</p>
        </div>
       </div>
      </div>
<br><br><br>
<?php include("../_structure/footer.php"); ?>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>
<?php
//Seleccion de transfers
@$tiporeserva=$_SESSION['tiporeserva'];
@$checkon = isset($_POST['opcion']) ? $_POST['opcion'] : 'off';
@$zonatra = $_POST['destr'];
//Seleccion de transfers
@$tiporeserva= $_POST['seleccionrs'];
@$costetot= $_POST['prctrn'];
//declaracion de variables que siempre llegaran
@$tipollegada = $_POST['tipollegada'];
@$hoteldest = $_POST['hoteldest'];
@$pax = $_POST['pax'];
@$llegvuelo = $_POST['llegvuelo'];
@$numvuelo = $_POST['numvuelo'];
@$horalleg = $_POST['horalleg'];
@$ampm = $_POST['ampm'];
@$aerolinea = $_POST['aerolinea'];

@$nombre = $_POST['nombre'];
@$pais = $_POST['pais'];
@$mail = $_POST['mail'];
@$telefono = $_POST['telefono'];
@$comentarios = $_POST['comentarios'];


if ($tiporeserva != "Round Trip") {

  //es one way ****

  //correo local
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/html; charset=iso-8859-1\n";
//Pasamos al cliente
$headerclt ="Content-Type: text/html; charset=iso-8859-1\n";
$headerclt.="From: Cancun Transfers <info@cancun-transfers.com>\r\n";
$headerclt.="Replay-to: info@cancun-transfers.com \r\n";
$headerclt.="Return-path: info@cancun-transfers.com \r\n";
//Mensaje Principal
$mensaje = '<img src="http://www.cancun-transfers.com/images/logo-small.jpg" /><br><br><br>'  . " \r\n";
$mensaje .= "Dear ". $nombre." :<br><br><p>cancun-transfers.com Thanks you for your preference, in this message you can confirm the information that you give us, if you have any doubt or you want to make any change, please send us an email with your request and we will contact you as soon as is posible, you can do it 24 hours before your arrival, the vehicle will be waiting for you, the date of your arrival.</p>
Also you can contact us with the following numbers:<br>
Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Oficce: +529982149012<br>
We are avaliable the seven days of the week the 24 hours.</p><br><hr><br> " . " \r\n";
$mensaje .= "Requested service: " . $tipollegada .'  '.$tiporeserva. "<br> \r\n";
$mensaje .= "Passengers: " . $pax . "<br> \r\n";
$mensaje .= "Arrival hotel: " . $hoteldest . "<br> \r\n";
$mensaje .= "Airline: " . $aerolinea . "<br> \r\n";
$mensaje .= "No. Flight: " . $numvuelo . "<br> \r\n";
$mensaje .= "Arrival date: " . $llegvuelo . "<br> \r\n";
$mensaje .= "Pickup Time: " . $horalleg .' '.$ampm. "<br> \r\n";
$mensaje .= "Your e-mail is: " . $mail . "<br> \r\n";
$mensaje .= "Your Movil phone is: " . $telefono . "<br> \r\n";
$mensaje .= "Your country is: " . $pais . "<br> \r\n";
$mensaje .= "Message had send us: " . $comentarios . "<br> \r\n";
$mensaje .= "<br><br>cancun-transfers reservations<br>Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Office: +529982149012<br>E-mail: info@cancun-transfers.com<br> Cancun Quintana Roo Mexico<br>
www.cancun-transfers.com" . '<br><br><img src="http://www.cancun-transfers.com/images/logo-small.jpg" /><br><br><br><p>The content of this mail is informative, cancun-transfers.com appreciates your preference </p><br><br>'  . " \r\n";
@$mensaje .= "Send date " . date('d/m/Y', time());
//Envio de datos extra a mail principal
$datoscontrol ="<br><br>La casilla de reserva en linea esta". $checkon . "<br> \r\n";
//correo de envio
$para = 'info@cancun-transfers.com';
$asunto = 'Mensaje cancun-transfers';


mail($para, $asunto, utf8_decode($mensaje.$datoscontrol), $header);
sleep(2); #Pausa de 2 Segundos cliente
mail($mail, "Cancun Transfers reservations", utf8_decode($mensaje), $headerclt);

//Mostrar html 



} else {
  //es round trip
@$horasal = $_POST['horasal'];
@$salampm = $_POST['salampm'];
@$salvuelo = $_POST['salvuelo'];
@$aerosalida = $_POST['aerosalida'];

//correo local
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/html; charset=iso-8859-1\n";
//Pasamos al cliente
$headerclt ="Content-Type: text/html; charset=iso-8859-1\n";
$headerclt.="From: Cancun Transfers <info@cancun-transfers.com>\r\n";
$headerclt.="Replay-to: info@cancun-transfers.com \r\n";
$headerclt.="Return-path: info@cancun-transfers.com \r\n";
//Mensaje Principal
$mensaje = '<img src="http://www.cancun-transfers.com/images/logo-small.jpg" /><br><br><br>'  . " \r\n";
$mensaje .= "Dear  ". $nombre." :<br><br><p>cancun-transfers.com Thanks you for your preference, in this message you can confirm the information that you give us, if you have any doubt or you want to make any change, please send us an email with your request and we will contact you as soon as is posible, you can do it 24 hours before your arrival, the vehicle will be waiting for you, the date of your arrival.</p>
Also you can contact us with the following numbers:<br>
Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Office: +529982149012<br>
We are avaliable the seven days of the week the 24 hours.</p><br><hr><br> " . " \r\n";
$mensaje .= "Requested Service: " . $tipollegada .'  '.$tiporeserva. "<br> \r\n";
$mensaje .= "Passengers: " . $pax . "<br> \r\n";
$mensaje .= "Arrival hotel: " . $hoteldest . "<br> \r\n";
$mensaje .= "Airline: " . $aerolinea . "<br> \r\n";
$mensaje .= "No. Flight: " . $numvuelo . "<br> \r\n";
$mensaje .= "Arrival date: " . $llegvuelo . "<br> \r\n";
$mensaje .= "Arrival Time: " . $horalleg .' '.$ampm. "<br> \r\n";
$mensaje .= "Pickup time: " . $aerosalida . "<br> \r\n";
$mensaje .= "Depature date: " . $salvuelo . "<br> \r\n";
$mensaje .= "Depature flight time: " . $horasal.' '.$salampm. "<br> \r\n";
$mensaje .= "Your email is: " . $mail . "<br> \r\n";
$mensaje .= "Your Movil phone is: " . $telefono . "<br> \r\n";
$mensaje .= "Your country is: " . $pais . "<br> \r\n";
$mensaje .= "Message had send us: " . $comentarios . "<br> \r\n";
$mensaje .= "<br><br>cancun-transfers Reservations<br>Tel: Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Office: +529982149012<br>email: info@cancun-transfers.com<br> Cancun Quintana Roo Mexico<br>
www.cancun-transfers.com" . '<br><br><img src="http://www.cancun-transfers.com/img/logo-mediano.png" /><br><br><br><p>The content of this mail is informative, cancun-transfers.com appreciates your preference.</p><br><br>'  . " \r\n";
@$mensaje .= "Enviado el " . date('d/m/Y', time());
//Envio de datos extra a mail principal
$datoscontrol ="<br><br>La casilla de reserva en linea esta". $checkon . "<br> \r\n";
//correo de envio
$para = 'info@cancun-transfers.com';
$asunto = 'Mensaje cancun-transfers';


mail($para, $asunto, utf8_decode($mensaje.$datoscontrol), $header);
sleep(2); #Pausa de 2 Segundos cliente
mail($mail, "Cancun Transfers reservations", utf8_decode($mensaje), $headerclt);

}
?>