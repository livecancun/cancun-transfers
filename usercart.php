<?php
#====================================================================================================
# File Name : usercart.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contains all application configuration details
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#====================================================================================================

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
	define("IN_SITE", true);
	
	include_once("includes/common.php");
	include_once($physical_path['DB_Access']. 'TripRate.php');
	include_once($physical_path['DB_Access']. 'TripType.php');
	include_once($physical_path['DB_Access']. 'Destination.php');
	include_once($physical_path['DB_Access']. 'Cart.php');
	include_once($physical_path['DB_Access']. 'Reservation.php');
	include_once($physical_path['DB_Access']. 'UserMaster.php');
	include_once($physical_path['DB_Access']. 'class.mailer.php');	

	global $virtual_path;

#====================================================================================================
#	Initialize object with required module
#----------------------------------------------------------------------------------------------------
	$objRate	= new TripRate();
	$objType	= new TripType();
	$objDest 	= new Destination();
	$objRes 	= new Reservation();
	$objUser 	= new UserMaster();
	$objCart 	= new Cart();
	
	$redirectUrl = "usercart.php";

//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 15;
$num_records = '';

#====================================================================================================
if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL || $_POST['Submit']==CANCEL || $_POST['Action']==BACK || $_POST['Action'] == SHOWCART)
{

	$tpl->assign(array( 'T_Body'		=>	'cart_request'.$config['tplEx'],
						'JavaScript'	=>	array('cart_request.js'),
						));

	 $tpl->assign("Currency",$_SESSION['CURRENCY']);							

	$tpl->assign(array(	"right_menu"		=>	YES,
					));

	if($update == true)
	{
		$succ_message = $lang['Msg_CartDetail_Updated'];
	}
	if($requestsent == true)
	{
		$succ_message = $lang['Msg_Request_Sent'];
	}
	if($delete == true)
	{
		$succ_message = $lang['Msg_CartDetail_Deleted'];
	}
	 
	$db->free();
	$tpl->assign(array( "A_Cart"				=> "usercart.php?&start=$start_record",
						"L_Requested_Service"	=> $lang['L_Requested_Service'],
						"L_Rate"				=> $lang['L_Rate'],
						"L_cartItem"			=> $lang['L_cartItem'],
						"L_ItemType"			=> $lang['L_ItemType'],
						"L_Cart"				=> $lang['L_Cart'],
						"Confirm_Delete"	    => $lang['msgConfirmDelete'],
						"L_ConfirmationNo"	    => $lang['L_ConfirmationNo'],						
						"Delete"   			    => $lang['Delete'],
						"Message"				=>  $succ_message,
					    "L_Total"   			=> $lang['L_Total'],
						"L_ReserveMore"			=> $lang['L_ReserveMore'],
						"L_Send_Reservation_Request" => $lang['L_Send_Reservation_Request'],
						"Valid_Delete_Cart"		=> $lang['Valid_Delete_Cart'],
						"Valid_Select_Cart"		=> $lang['Valid_Select_Cart'],
						"L_status"				=> $lang['L_status'],
						"L_Note"				=> $lang['L_Note'],
						"Note_PR"				=> $lang['Note_PR'],
						"Note_AR"				=> $lang['Note_AR'],
						"Note_NA"				=> $lang['Note_NA'],
						"Note_RP"				=> $lang['Note_RP'],
						"Note_PD"				=> $lang['Note_PD']
						));
	 	
		$SortBy = "itemType";
		$tpl->assign(array( "L_Reservation_Details" => $lang['L_Reservation_Details'],
							"L_Checkout"			=> $lang['L_Checkout'],
							"L_ReserveMore"		    => $lang['L_ReserveMore'],
							"View"					=>  $lang['View'],
							"Modify"  				=>  $lang['Modify'],
							"L_Discount"  			=> $lang['L_Discount'],								
							"ReserveMoreLink"		=> $url_path['Site_URL']."Reservation/reservemore.php",
						));
	
		$tpl->assign(array( 'CartInfo' => $objCart->Show_Cart($_GET['confirmationNo'],$cartId='',$SortBy, $start_record, $Page_Size, $num_records),
							));
							
		//========================================
		//$CartInfos=array( 'CartInfo' => $objCart->Show_Cart($_GET['confirmationNo'],$cartId='',$SortBy, $start_record, $Page_Size, $num_records),);
		//$mycart=$objCart->Show_UserCart_ID($_GET['confirmationNo']);
		$sql = "SELECT cart_id FROM ".CART_MASTER 	
				  . " Where ".CART_MASTER.".confirmationNo = '".$_GET['confirmationNo']."'"; 
			$db ->query($sql);
			$db->next_record();
			$mycart	= $db->f('cart_id');
		
		$tpl->assign(array( 'CartDetails' => $objCart->Show_UserRes_Detail($mycart),));
		//========================================
		$confirmationNo = $_GET['confirmationNo'];

		//================================== Discount Info ==================================//
		$sql = $objUser -> ShowUserInfoByConfNo($confirmationNo); 
		$db->next_record();
		
		$is_airline_empl	=	$db->f('is_airline_empl');
		$discount_rate		=	$db->f('discount_rate');

		$tpl->assign(array("is_airline_empl"	=> $db->f('is_airline_empl'),
						   "discount_id"     	=> $db->f('discount_id'),
						   "discount_rate"	    => $db->f('discount_rate'),
						   "discount_title"	    => ucfirst($db->f('discount_title')),
						   "empl_id" 	    	=> $db->f('empl_id'),
						   "empl_airline"		=> ucfirst($db->f('empl_airline')),
						   ));
		//=====================================================================================//	

	if($num_records >= $Page_Size)
		$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));

}

#====================================================================================================
#	Delete Cart Record
#----------------------------------------------------------------------------------------------------
elseif($_GET['Action']==DELETE || $_POST['Action']==DELETE)
{
	for($i=0; $i < count($_POST['chkCartId']) ; $i++)
	{
	   $status = $objCart -> Delete_Cart_Detail($_POST['chkCartId'][$i]);
	}
	
	header("location: usercart.php?&confirmationNo=$confirmationNo&delete=true");
	
}

	$tpl->display('default_layout'. $config['tplEx']);
	
?>