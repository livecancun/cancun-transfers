<!DOCTYPE html>
<html>
<head>
	<title>Cancun Airport Transfers | Official Airport Transportation</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="cancun airport shuttle, cancun shuttle, cancun airport taxi, cancun airport transportation, cancun transportation, cancun transfers, cancun airport hotel, cancun transfers rates, shuttle cancun"/>
	<meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
	<meta property="og:url" content="http://www.cancun-transfers.com/" />
	<meta property="og:title" content="Cancun Airport shuttle">
	<meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="300">
	<meta property="og:image:height" content="300">
	<meta property="og:locale" content="en_US" />
	<meta property="og:locale:alternate" content="es_ES" />
	<link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
	<link href="css/main-style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
	<div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<div id="cunshuttle-mainbaner">
<div class="row">
                <div class="col-lg-5">
                    <div class="cunshuttle-rbx-mb">
                    <div class="container">
            		<div class="row">
            		<div class="col-lg-4">
                    	<br>
                    	<form action="reserve.php" method="POST">
  <label for="seleeccionarlb">Select</label>
  <select class="form-control" name="tipors">
     <option value="Round Trip">Round Trip</option>
     <option value="One way">One Way</option>
    </select>
  <div class="form-group">
    <br>
    <label for="hotellb">Hotel</label>
       <input type="text" class="form-control" name="hotelleg" id="hotel" placeholder="Hotel">
  </div>
  <label for="exampleInputEmail1">Passengers</label>
  <select class="form-control" name="pasajerors">
     <option value="1">1</option>
     <option value="2">2</option>
     <option value="3">3</option>
     <option value="4">4</option>
     <option value="5">5</option>
     <option value="6">6</option>
     <option value="7">7</option>
     <option value="8">8</option>
    </select>
  <br>
  <button type="submit" class="btn btn-success btn-lg">Reserve</button>
</form>
		</div>
	</div>
</div>

                    </div>
                </div>
                <div class="col-lg-7"><br>
                    <p class="font-bl-17">Providing airport shuttle service for over 20 years. </p>
                </div>
            </div>
        </div>


</div>

<div class="services-ribbon">
	<div class="container">
    	<div class="row">
    	<div class="col-md-12">
	<p>Services</p>
	</div>
   </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/cancun-airport-hotel-zone.php"><h3>Airport <- -> Cancun Hotel Zone</h3></a>
            <p>Cancun Airport - Cancun Hotel Zone hotels Only, Private Van, 1-8 passengers max, <b>Round trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                70 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/cancun-airport-hotel-zone.php"><h3>
                60 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/cancun-airport-hotel-zone.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/playa-del-carmen-cancun-airport.php"><h3>Airport <- -> Playa del Carmen</h3></a>
            <p>Cancun Airport - Playa del Carmen Zone hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                110 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/playa-del-carmen-cancun-airport.php"> 
              <h3>
                100 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/playa-del-carmen-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/puerto-morelos-cancun-airport.php"><h3>Airport <- -> Puerto Morelos</h3></a>
            <p>Cancun Airport - Puerto Morelos hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                80 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/puerto-morelos-cancun-airport.php"><h3>
                75 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/puerto-morelos-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/puerto-aventuras-cancun-airport.php"><h3>Airport <- -> Puerto Aventuras</h3></a>
            <p>Cancun Airport - Puerto Aventuras hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                130 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/puerto-aventuras-cancun-airport.php"> 
              <h3>
                120 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/puerto-aventuras-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/cancun-downtown-cancun-airport.php"><h3>Airport <- -> Downtown Hotels</h3></a>
            <p>Cancun Airport - Cancun downtown hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                50 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/cancun-downtown-cancun-airport.php"><h3>
                40 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/cancun-downtown-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/playa-mujeres-cancun-airport.php"><h3>Airport <- -> Playa Mujeres</h3></a>
            <p>Cancun Airport - Playa mujeres Zone hotels Only, Private Van, 1-8 passengers max, <b>Round Trip</b>, non-stops. Limited Offer</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                50 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/playa-mujeres-cancun-airport.php"> 
              <h3>
                40 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/playa-mujeres-cancun-airport.php" role="button">Bookit</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-8">
    </div>
    <div class="col-md-4">
      <h3 class="text-center text-success">
        Destinations list
      </h3> <a href="http://www.cancun-transfers.com/transfers-rates.php" class="btn btn-info btn-lg btn-block" type="button">See all destinations</a>
    </div>
  </div>
</div>
<br><br>
<div class="container">
    <div class="row">
<div class="col-md-6 md-margin-bottom-30">
                    <h2 class="font-bl1-20">WATCH ABOUT US</h2>
                    <p>Welcome to Mexico, Quintana Roo is the State which the most beautiful places such as Cancun and the Riviera Maya, as well as take you from the airport to your hotel and hotel to airport, we offer service from Cancun transfers so that you can visit the best places in the area, we have spent 15 years traveling and recommending the best places in Cancun and the Riviera MayaWe are at your service, Kinmont Cancun Transfers, you want to have a beautiful vacation starting with your completely private transportation.</p><br>
                    <a href="#">Check our rates</a><br><br>
                </div>
<div class="col-md-6 md-margin-bottom-30">
                    <h2 class="font-gs-20">Cancun Airport location</h2>
                    <iframe width="580" height="360" src="https://www.youtube.com/embed/uVTFuHZfVzs?rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
	</div>
</div>
<br><br>
<div class="container">
    <div class="row">
<div class="col-md-6 md-margin-bottom-30">
                    <h2 class="font-bl1-20">LASTMINUT OFFER</h2>
                    <img src="images/cancun-deal1-transfers.jpg">
                </div>
<div class="col-md-6 md-margin-bottom-30">
                    <h2 class="font-gs-20">SERVICES</h2>
                    <p>Cancun & Riviera Maya airport private transfers by KinMont offers premier door-to-door ground transportation services from/to Cancun International Airport to your hotel in Cancun, Playa del Carmen or Riviera Maya. We provide a VIP specialized shuttle or taxi service that offers you the safety and punctuality that you deserve.</p><p>
Our representatives at the airport will be waiting for you holding a banner with our logo (Kinmont), and they will lead you to the unit that will drive you to your destination (please, check Cancun Airport Arrival Section). We always keep in mind that you would like to feel at home, and we will give you all the information you need to enjoy your trip since the very beginning, as our drivers are highly qualified to transport people from every part of the world. Our Cancun & Riviera Maya transfers rates are competitive and you will save some money if your group is big. Enjoy the comfort of our private shuttles after your flight and get rid of stress trying to find a taxi in Cancun. </p>
                </div>
  </div>
</div>
<br><br>
  <div class="container">
    	<div class="row">
    	<div class="col-md-12">
	<p class="font-gr-20"> <i class="fa fa-bus fa-2x wow bounceIn"></i> | Get the best rates |</p>
	</div>
   </div>
  </div>
 

 <div class="container">
  <div class="row">
    <div class="col-md-4">
      <h3>
        Cancun Hotels
      </h3>
      <img src="images/cancun-shuttle-locaciones.jpg">
        <a class="btn" href="#">View details »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Playa del Carmen
      </h3>
      <img src="images/playa-del-carmen-shuttle-locaciones.jpg">
        <a class="btn" href="#">View details »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Riviera Maya Hotels
      </h3>
      <img src="images/riviera-maya-shuttle-locaciones.jpg">
        <a class="btn" href="#">View details »</a>
      </p>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
    <p>
    cancun-transfers by KinMont, is a reliable and secure transportation company providing transportation to individuals, families and small groups. All shuttles, vans have air conditioned, licensed and insured. All drivers are bilingual and professional. Let us know your special needs... Special Assistance for disabled passengers. 
    </p>
    </div>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>