<?php session_start();?>
<?php
include("_lib/lst-prc-nor.php");
@$tipores = $_POST['tipors'];
@$fechalleg = $_POST['datearr'];
@$htlleg = $_POST['hotelleg'];
@$paxrs = $_POST['pasajerors'];
$reson=0;
$destinotr = "Cancun Airport";

?>
<!DOCTYPE html>
<html>
<head>
    <title>Cancun Airport Shuttle | Official Airport Transportation</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="cancun airport shuttle, cancun shuttle, cancun airport taxi, cancun airport transportation, cancun transportation, cancun transfers, cancun airport hotel, cancun transfers rates, shuttle cancun"/>
    <meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
    <meta property="og:url" content="http://www.cancun-transfers.com/" />
    <meta property="og:title" content="Cancun Airport shuttle">
    <meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="300">
    <meta property="og:image:height" content="300">
    <meta property="og:locale" content="en_US" />
    <meta property="og:locale:alternate" content="es_ES" />
    <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
    <link href="css/main-style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php include("_structure/header-tag-res.php"); ?>

</head>
<body>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
    <div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<?php include("_structure/service-ribbon.php"); ?>
<br><br>
<div class="container">
   <div class="row">
                <div class="col-xs-12 col-md-12">
                        <p class="htxtbnc">PRIVATE SERVICE</p>
                    </div>
      </div>
      <div class="row">
                <div class="col-xs-6 col-md-6">
                        <p>SERVICE: <?php echo $destinotr.' '.$tipores; ?> </p>
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <p >PRICE: ON REQUEST</p>
                    </div>
      </div>
   </div>
   <br>
   <div class="container">
   <div class="row">
    <div class="row">
    <!--Frm-->
   <?php include("_structure/form-offline.php"); ?>
    <!--Frm end-->
        </div></div></div>
   
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <h3>
        Cancun Hotels
      </h3>
      <img src="images/cancun-shuttle-locaciones.jpg">
        <a class="btn" href="#">View details »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Playa del Carmen
      </h3>
      <img src="images/playa-del-carmen-shuttle-locaciones.jpg">
        <a class="btn" href="#">View details »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Riviera Maya Hotels
      </h3>
      <img src="images/riviera-maya-shuttle-locaciones.jpg">
        <a class="btn" href="#">View details »</a>
      </p>
    </div>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>