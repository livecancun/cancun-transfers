<?php
include("_lib/lst-prc-nor.php");
$reson=1;
$destinotr = "Playa del secreto hoteles";
$prctrasonw = $onwtransppds;
$prctrasrtp = $rtwtransppds;
$preonwfinal = $prctrasonw * 2;
$descuentofinal = $preonwfinal - $prctrasrtp;
@$tipores = $_POST['tiporeserva'];
if($tipores != null)
{ @$tipores = $_POST['tiporeserva'];
} else {  $tipores = "Viaje Redondo";}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Aeropuerto Cancun a Playa del Secreto hoteles | Transportacion Oficial Aeropuerto</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="cancun airport shuttle, cancun shuttle, cancun airport taxi, cancun airport transportation, cancun transportation, cancun transfers, cancun airport hotel, cancun transfers rates, shuttle cancun"/>
    <meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
    <meta property="og:url" content="http://www.cancun-transfers.com/" />
    <meta property="og:title" content="Cancun Airport shuttle">
    <meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="300">
    <meta property="og:image:height" content="300">
    <meta property="og:locale" content="en_US" />
    <meta property="og:locale:alternate" content="es_ES" />
    <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
    <link href="css/main-style.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php include("_structure/header-tag-res.php"); ?>

</head>
<body>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
    <div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<?php include("_structure/service-ribbon.php"); ?>
<br><br>
<div class="container">
   <div class="row">
                <div class="col-xs-12 col-md-12">
                        <p class="htxtbnc">Servicio Privado: <?php echo $tipores; ?></p>
                    </div>
      </div>
      <div class="row">
                <div class="col-xs-6 col-md-6">
                        <p class="servicerd">Servicio: <?php echo $destinotr; ?> </p>
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <p class="pricerd">Precio: <?php if ($tipores == "Viaje Redondo") {echo $prctrasrtp ."USD / Ahorre  ".$descuentofinal;} else { echo $prctrasonw; } ?> USD</p>
                    </div>
      </div>
   </div>
   <br>
   <div class="container">
   <div class="row">
   <!--Frm-->
   <?php include("_structure/formulario-general.php"); ?>
    <!--Frm end-->
        </div></div></div>

  <div class="container">
  <div class="row">
    <div class="col-md-4">
      <h3>
        Cancun Hoteles
      </h3>
      <img src="images/cancun-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Playa del Carmen
      </h3>
      <img src="images/playa-del-carmen-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Riviera Maya Hoteles
      </h3>
      <img src="images/riviera-maya-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
      $(".micheckbox").on( 'change', function() {
    if( $(this).is(':checked') ) {
        // Hacer algo si el checkbox ha sido seleccionado
        $("#formapago").html('<a href="#" class="btn btn-success btn-lg disabled" role="button" aria-disabled="true">Pago en linea seleccionado</a><img src="images/arrow.png" /> - <a href="#" class="btn btn-outline-secondary btn-lg disabled" role="button" aria-disabled="true">Deseo pagar en aeropuerto</a>');
    } else {
        // Hacer algo si el checkbox ha sido deseleccionado
        $("#formapago").html('<a href="#" class="btn btn-primary btn-lg disabled" role="button" aria-disabled="true">Pago en aeropuerto seleccionado</a><img src="images/arrow.png" /> - <a href="#" class="btn btn-outline-secondary btn-lg disabled" role="button" aria-disabled="true">Deseo pagar en linea</a>');
    }
});
</script>
</body>
</html>