<!DOCTYPE html>
<html>
<head>
	<title>Llegadas Cancun | Transportacion Oficial Aeropuerto Cancun</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="cancun airport shuttle, cancun shuttle, cancun airport taxi, cancun airport transportation, cancun transportation, cancun transfers, cancun airport hotel, cancun transfers rates, shuttle cancun"/>
	<meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
	<meta property="og:url" content="http://www.cancun-transfers.com/" />
	<meta property="og:title" content="Cancun Airport shuttle">
	<meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="300">
	<meta property="og:image:height" content="300">
	<meta property="og:locale" content="en_US" />
	<meta property="og:locale:alternate" content="es_ES" />
	<link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
	<link href="css/main-style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<SCRIPT type=text/javascript 
src="http://maps.google.com/maps/api/js?sensor=false"></SCRIPT>
<SCRIPT type=text/javascript>
    function initialize() {
      var fenway = new google.maps.LatLng(21.038525,-86.864862);
      var mapOptions = {
        center: fenway,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        streetViewControl: true
      };
      var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    }
    </SCRIPT>

</head>
<body onload=initialize()>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
	<div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>

<br><br>
<div class="container">
   <div class="row">
                <div class="col-xs-12 col-md-12">
                <p>Entendemos las expectativas que tiene para llegar a su destino, desde el aeropuerto local, aunque muchas de las veces ya en el destino nos suelen suceder problemas que no habíamos contemplado, como los sitios donde acudir, las demoras que puedan haber, o como llegar al lugar deseado, en cancun-transfers hemos contemplado estos inconvenientes y lo hemos solucionado por usted, de tal modo que tan solo salir del <b>aeropuerto, nosotros le brindaremos un servicio privado directo a su hotel sin escalas ni esperas</b>.</p><br><br>
                <div id="map_canvas" align="center" style="width: 800px; height: 250px"></div><br><br>
                <h3>Aeropuerto internacional de Cancun, TIPS</h3>
<ul>
<li> Una vez que han obtenido su equipaje, pasan por aduanas (vuelos internacionales) y al salir de la terminal es donde nuestro equipo estará esperando sosteniendo  un cartel con su nombre.</li>
<li> Es muy importante saber que dentro del  aeropuerto en su camino, encontrará gran cantidad de personas que afirman ser su empresa de transporte y/o ofreciendo descuentos y/o actividades gratuitas. Tenga en cuenta que son vendedores de tiempo compartido.</li>

<li> Hay muchos de los representantes de empresas de transporte fuera a la espera de sus clientes. Nuestro anfitrión KinMont estara esperando directamente fuera de la terminal.</li>
<li> Si por casualidad nuestro anfitrión está ocupado con otros viajeros, simplemente puede caminar a la barra de "Air margaritaville" que esta justo a la salida de la terminal 3 estarácon usted una vez que éste disponible.</li>
<li> Si por algún motivo su vuelo se retrasa, cambiado o tuvieron que pasar más tiempo dentro de la terminal debido al equipaje perdido o cualquier otra situación, y no puede encontrar nuestro representante, utilice el teléfono público situado justo detrás de la barra de "Air margaritaville". No necesita comprar una tarjeta de telefonos mediante nuestro número <strong>01 800 727 8112</strong> </li>
</ul>
<br /><br /><center>
<iframe src="http://www.flightstats.com/go/weblet?guid=d56928966b03ae86:-3be13f33:1197c2fe4bc:38ea&imageColor=black&language=english&weblet=status&action=AirportFlightStatus&airportCode=CUN&airportQueryType=1" frameborder="0" scrolling="auto" width=900 height=500></iframe>
</center>

</div>
  </div>
    </div>
 

 <div class="container">
  <div class="row">
    <div class="col-md-4">
      <h3>
        Cancun Hoteles
      </h3>
      <img src="images/cancun-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Playa del Carmen
      </h3>
      <img src="images/playa-del-carmen-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Riviera Maya Hoteles
      </h3>
      <img src="images/riviera-maya-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>