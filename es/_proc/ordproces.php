<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
  <title>Contact US | Official Airport Transportation</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="cancun airport transfers, kinmont transfers contact us, cancun shuttle, cancun transfers"/>
  <meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
  <meta property="og:url" content="http://www.cancun-transfers.com/" />
  <meta property="og:title" content="Cancun Airport shuttle">
  <meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="300">
  <meta property="og:image:height" content="300">
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="es_ES" />
  <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
  <link href="../css/main-style.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css" type="text/css">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("../_structure/menu-int.php"); ?>

<div class="tel-box">
  <div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<div class="container">
   <div class="row">
        <div class="col-xs-12 col-md-12">
        <p>Gracias por reservar con nosotros, hemos enviado un correo a la dirección que nos indicó, por favor revise su bandeja de entrada, algunas veces su cliente de correo puede ponerlo en la bandeja de spam, favor de verificarlo.<br><br>
Si tiene alguna duda con respecto a nuestros servicios, por favor contáctenos o llámenos a los siguientes telefono de oficina: USA / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Oficina: 998 214 90 12, estamos para servirles.
</p>
        </div>
       </div>
      </div>
<br><br><br>
<?php include("../_structure/footer.php"); ?>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>
<?php
//Seleccion de transfers
@$tiporeserva=$_SESSION['tiporeserva'];
@$checkon = isset($_POST['opcion']) ? $_POST['opcion'] : 'off';
@$zonatra = $_POST['destr'];
//Seleccion de transfers
@$tiporeserva= $_POST['seleccionrs'];
@$costetot= $_POST['prctrn'];
//declaracion de variables que siempre llegaran
@$tipollegada = $_POST['tipollegada'];
@$hoteldest = $_POST['hoteldest'];
@$pax = $_POST['pax'];
@$llegvuelo = $_POST['llegvuelo'];
@$numvuelo = $_POST['numvuelo'];
@$horalleg = $_POST['horalleg'];
@$ampm = $_POST['ampm'];
@$aerolinea = $_POST['aerolinea'];

@$nombre = $_POST['nombre'];
@$pais = $_POST['pais'];
@$mail = $_POST['mail'];
@$telefono = $_POST['telefono'];
@$comentarios = $_POST['comentarios'];


if ($tiporeserva != "Viaje Redondo") {

  //es one way ****

  //correo local
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/html; charset=iso-8859-1\n";
//Pasamos al cliente
$headerclt ="Content-Type: text/html; charset=iso-8859-1\n";
$headerclt.="From: Cancun Transfers <info@cancun-transfers.com>\r\n";
$headerclt.="Replay-to: info@cancun-transfers.com \r\n";
$headerclt.="Return-path: info@cancun-transfers.com \r\n";
//Mensaje Principal
$mensaje = '<img src="http://www.cancun-transfers.com/images/logo-small.jpg" /><br><br><br>'  . " \r\n";
$mensaje .= "Estimado ". $nombre." :<p>cancun-transfers.com, Gracias por su preferencia, hemos enviado un mensaje con la información que nos proporciono, si usted tiene alguna duda o solicita algún cambio, por favor envienos un correo con su solicitud y nos comunicaremos con usted lo las pronto como nos sea posible, es necesario sea con 24 hrs antes de su llegada,  nuestra unidad estará llegando por usted a la hora y fecha de su llegada.</p><p>
Tambien puede comunicarse a nuestros siguientes números:<br>
Telefonos de oficina: USA / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Movil: 
+529982149012<br>
Estamos disponibles todos los días, las 24 hrs. </p>
<br><hr><br> " . " \r\n";
$mensaje .= "Servicio Solicitado: " . $tipollegada .'  '.$tiporeserva. "<br> \r\n";
$mensaje .= "Pasajeros: " . $pax . "<br> \r\n";
$mensaje .= "Hotel de llegada: " . $hoteldest . "<br> \r\n";
$mensaje .= "Aerolinea: " . $aerolinea . "<br> \r\n";
$mensaje .= "Num Vuelo: " . $numvuelo . "<br> \r\n";
$mensaje .= "Fecha llegada: " . $llegvuelo . "<br> \r\n";
$mensaje .= "Hora de arrivo: " . $horalleg .' '.$ampm. "<br> \r\n";
$mensaje .= "Su correo es: " . $mail . "<br> \r\n";
$mensaje .= "Su telefono movil es: " . $telefono . "<br> \r\n";
$mensaje .= "Su pais es: " . $pais . "<br> \r\n";
$mensaje .= "Mensaje que nos envio: " . $comentarios . "<br> \r\n";
$mensaje .= "<br><br>Reservaciones cancun-transfers <br>Tel: USA / CAN 888 811 4255 MEX 01 800 161 4957, Cancun 286 15 25 , Movil: +529982149012<br>email: info@cancun-transfers.com<br> Cancun Quintana Roo Mexico<br>
www.cancun-transfers.com" . '<br><br><img src="http://www.cancun-transfers.com/images/logo-small.jpg" /><br><br><br><p>El contenido de este correo es informativo, agradecemos su preferencia. </p><br><br>'  . " \r\n";
@$mensaje .= "Enviado el " . date('d/m/Y', time());
//Envio de datos extra a mail principal
$datoscontrol ="<br><br>La casilla de reserva en linea esta". $checkon . "<br> \r\n";
//correo de envio
$para = 'info@cancun-transfers.com';
$asunto = 'Mensaje cancun-transfers Español';


mail($para, $asunto, utf8_decode($mensaje.$datoscontrol), $header);
sleep(2); #Pausa de 2 Segundos cliente
mail($mail, "Reservaciones Cancun-Transfers", utf8_decode($mensaje), $headerclt);

//Mostrar html 



} else {
  //es round trip
@$horasal = $_POST['horasal'];
@$salampm = $_POST['salampm'];
@$salvuelo = $_POST['salvuelo'];
@$aerosalida = $_POST['aerosalida'];

//correo local
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/html; charset=iso-8859-1\n";
//Pasamos al cliente
$headerclt ="Content-Type: text/html; charset=iso-8859-1\n";
$headerclt.="From: Cancun Transfers <info@cancun-transfers.com>\r\n";
$headerclt.="Replay-to: info@cancun-transfers.com \r\n";
$headerclt.="Return-path: info@cancun-transfers.com \r\n";
//Mensaje Principal
$mensaje = '<img src="http://www.cancun-transfers.com/images/logo-small.jpg" /><br><br><br>'  . " \r\n";
$mensaje .= "Estimado  ". $nombre." :<br><br><p>cancun-transfers.com, Gracias por su preferencia, hemos enviado un mensaje con la información que nos proporciono, si usted tiene alguna duda o solicita algún cambio, por favor envienos un correo con su solicitud y nos comunicaremos con usted lo las pronto como nos sea posible, es necesario sea con 24 hrs antes de su llegada,  nuestra unidad estará llegando por usted a la hora y fecha de su llegada.</p><p>Tambien puede comunicarse a nuestros siguientes números:<br>
Telefonos de oficina: USA / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Movil: 
+529982149012<br>
Estamos disponibles todos los días, las 24 hrs. </p>
<br><hr><br> " . " \r\n";
$mensaje .= "Servicio solicitado: " . $tipollegada .'  '.$tiporeserva. "<br> \r\n";
$mensaje .= "Pasajeros: " . $pax . "<br> \r\n";
$mensaje .= "Hotel de llegada: " . $hoteldest . "<br> \r\n";
$mensaje .= "Aerolinea: " . $aerolinea . "<br> \r\n";
$mensaje .= "Num de vuelo: " . $numvuelo . "<br> \r\n";
$mensaje .= "Fecha de llegada: " . $llegvuelo . "<br> \r\n";
$mensaje .= "Hora de llegada: " . $horalleg .' '.$ampm. "<br> \r\n";
$mensaje .= "Pasamos por usted: " . $aerosalida . "<br> \r\n";
$mensaje .= "Su vuelo sale: " . $salvuelo . "<br> \r\n";
$mensaje .= "Su hora de vuelo es: " . $horasal.' '.$salampm. "<br> \r\n";
$mensaje .= "Su correo: " . $mail . "<br> \r\n";
$mensaje .= "Su telefono movil: " . $telefono . "<br> \r\n";
$mensaje .= "Pais: " . $pais . "<br> \r\n";
$mensaje .= "Menaje que nos envio: " . $comentarios . "<br> \r\n";
$mensaje .= "<br><br>Reservaciones cancun-transfers <br>Tel: USA / CAN 888 811 4255 MEX 01 800 161 4957, Cancun 286 15 25 , Movil: +529982149012<br>email: info@cancun-transfers.com<br> Cancun Quintana Roo Mexico<br>
www.cancun-transfers.com" . '<br><br><img src="http://www.cancun-transfers.com/img/logo-mediano.png" /><br><br><br><p>El contenido de este correo es informativo, agradecemos su preferencia.</p><br><br>'  . " \r\n";
@$mensaje .= "Enviado el " . date('d/m/Y', time());
//Envio de datos extra a mail principal
$datoscontrol ="<br><br>La casilla de reserva en linea esta". $checkon . "<br> \r\n";
//correo de envio
$para = 'info@cancun-transfers.com';
$asunto = 'Mensaje cancun-transfers español';


mail($para, $asunto, utf8_decode($mensaje.$datoscontrol), $header);
sleep(2); #Pausa de 2 Segundos cliente
mail($mail, "Reservaciones Cancun Transfers", utf8_decode($mensaje), $headerclt);

}
?>

<?php

if ($checkon =="on") { ?>
<body onload="document.paypalform.submit();">
<form action="https://www.paypal.com/cgi-bin/webscr" name="paypalform" method="post">
<input name="cmd" type="hidden" value="_cart" />
<input name="upload" type="hidden" value="1" />
<input name="business" type="hidden" value="info@cancun-transfers.com" />
<input name="shopping_url" type="hidden" value="http://www.cancun-transfers.com/" />
<input name="currency_code" type="hidden" value="USD" />


<input name="return" type="hidden" value="http://www.cancun-transfers.com" />
<input name="notify_url" type="hidden" value="www.cancun-transfers.com/paypal_ipn.php" />
<input name="cancel_return" type="hidden" value="http://www.cancun-transfers.com/">


<input name="rm" type="hidden" value="2" />

<input name="item_name_1" type="hidden" value=<?php echo '"'.$tipollegada.' - '.$tiporeserva.' - '.$zonatra.'"'; ?> />
<input name="amount_1" type="hidden" value=<?php echo '"'.$costetot.'"'; ?> />
<input name="quantity_1" type="hidden" value="1" />  <?php


}



?>