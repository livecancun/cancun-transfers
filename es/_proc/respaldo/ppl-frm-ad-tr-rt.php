<?php
session_cache_limiter();
session_start();


$checkon = isset($_POST['opcion']) ? $_POST['opcion'] : 'off';
$transcunzh = $_SESSION['transcunzh'] ;
$transfertype = $_SESSION['transfertype'] ;

if ($checkon == "off") {

$tipollegada = $_POST['tipollegada'];
$hoteldest = $_POST['hoteldest'];
$pax = $_POST['pax'];
$llegvuelo = $_POST['llegvuelo'];
$numvuelo = $_POST['numvuelo'];
$aerolinea = $_POST['aerolinea'];
//termina llegada desde aeropuerto

$pickupvuelo = $_POST['pickupvuelo'];
$salvuelo = $_POST['salvuelo'];
$aerosalida = $_POST['aerosalida'];


$pickuphotel = isset($_POST['pickuphotel']) ? $_POST['pickuphotel'] : '';
$salvuelo = isset($_POST['salvuelo']) ? $_POST['salvuelo'] : '';
//termina salida desde aeropuerto

$nombre = $_POST['nombre'];
$pais = $_POST['pais'];
$mail = $_POST['mail'];
$telefono = $_POST['telefono'];
$comentarios = $_POST['comentarios'];


$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/plain";
$mensaje = "Este mensaje fue enviado por " . $nombre . " \r\n";
$mensaje .= "Su e-mail es: " . $mail . " \r\n";
$mensaje .= "Su Direccion es: " . $pais . " \r\n";
$mensaje .= "Su telefono es: " . $telefono . " \r\n";
$mensaje .= "--\r\n";
$mensaje .= "--\r\n";
$mensaje .= "Llegada desde aeropuerto " . $tipollegada . " \r\n";
$mensaje .= "Hotel de destino " . $hoteldest . " \r\n";
$mensaje .= "Numero de pax " . $pax . " \r\n";
$mensaje .= "Llegada de vuelo " . $llegvuelo . " \r\n";
$mensaje .= "Numero de vuelo" . $numvuelo . " \r\n";
$mensaje .= "Aerolinea" . $aerolinea . " \r\n";
$mensaje .= "Pickup de salida" . $pickupvuelo . " \r\n";
$mensaje .= "Hora de salida de vuelo" . $salvuelo . " \r\n";
$mensaje .= "Aerolinea y numero vuelo salida" . $aerosalida . " \r\n";
$mensaje .= "----- \r\n";
$mensaje .= "---- \r\n";
$mensaje .= "Mensaje: " . $_POST['comentarios'] . " \r\n";
$mensaje .= "Enviado el " . date('d/m/Y', time());
$para = 'contacto@cancunrep.com';
$asunto = 'Mensaje transfers Cancunrep';

mail($para, $asunto, utf8_decode($mensaje), $header);

?>


<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Transfers Cancun Riviera Maya - Vacaciones Cancun</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="taxi cancun, cancun transfers, paquetes cancun, hotel transfers, aeropuerto cancun " />
        <meta name="robots" content="all" />
        <?php include("../_gral/head-int.php"); ?>
        <link rel="stylesheet" href="../css/form-tran.css">
        <script type="text/javascript" src="../js/form.js"></script>
        <?php 
      require_once("../_gral/fnc-prc-list.php"); ?>
        
</head>
    <body>
    <div id="contenedor">
    <?php include("../_gral/logo-int.php"); ?>
    <?php include("../_gral/menu-head.php"); ?>
    <?php include("../_gral/redes-box-int.php"); ?>
        <div id="mapbox">
        <div class="container">
         <div class="row">
          <div class="col-xs-12"><p><a href="http://www.cancunrep.com/">Inicio</a> > <a href="http://www.cancunrep.com/transfers/"> transfers </a></p></div>
         </div>
        </div>
    </div>
       
    <div id="head-pic">
        <img src="../images/cancun-transfers.jpg" class="img-responsive" />
        </div>
 
    <div id="descripcion">
    <br /><br />
    <br>
    <div class="container">
        <div class="row">
           <div class="col-xs-12 col-md-9">
    <br /><br />
    <p>Gracias <strong><?php  echo $nombre;  ?></strong> por enviarnos su peticion y su confianza, en breve le estaremos enviando a su correo la informacion de nuestros servicios, por favor revise tambien su carpeta de spam para 
    revisar que nuestra respuesta se aloje en dichas carpetas de correo y no puedan ser vistas, hoy mas que nunca estamos trabajando para brindarle el servicio que se merece, de nuevo agradecemos la confianza, estamos en contacto. Equipo Cancunrep.com
    <br /><br />
     
        </p>
    </div></div></div>

    

    </div></div>

    <div id="menuin">
        <p>Informacion:</p><br>
    <div id="listinfo"><a href="http://www.cancunrep.com/informacion/cancun.php"><span>Cancun</span></a> | <a href="http://www.cancunrep.com/informacion/isla-mujeres.php"><span>Isla Mujeres</span></a> | <a href="http://www.cancunrep.com/informacion/playa-del-carmen.php"><span>Playa del Carmen</span></a> | <a href="http://www.cancunrep.com/informacion/cozumel.php"><span>Cozumel</span></a> | <a href="http://www.cancunrep.com/informacion/riviera-maya.php"><span>Riviera Maya</span></a> | <a href="http://www.cancunrep.com/informacion/tulum-informacion.php"><span>Tulum</span></a> | <a href="http://www.cancunrep.com/informacion/akumal-informacion.php"><span>Akumal</span></a> | <a href="http://www.cancunrep.com/informacion/puerto-morelos-informacion.php"><span>Puerto Morelos</span></a><br /><br /></div>
    </div>
    <div id="footer">
    <!--Inicio submenu -->
    <div id="sub-menu">
    <?php include("../_gral/submenu-footer-int.php"); ?>
    </div>



</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
<?php include("../_gral/tracking-code.php"); ?>
  </body>
    
</html>

<?php




//fin del constructor
} else {

//construccion del correo
$tipollegada = $_POST['tipollegada'];
$hoteldest = $_POST['hoteldest'];
$pax = $_POST['pax'];
$llegvuelo = $_POST['llegvuelo'];
$numvuelo = $_POST['numvuelo'];
$aerolinea = $_POST['aerolinea'];
//termina llegada desde aeropuerto

$pickupvuelo = $_POST['pickupvuelo'];
$salvuelo = $_POST['salvuelo'];
$aerosalida = $_POST['aerosalida'];


$pickuphotel = isset($_POST['pickuphotel']) ? $_POST['pickuphotel'] : '';
$salvuelo = isset($_POST['salvuelo']) ? $_POST['salvuelo'] : '';
//termina salida desde aeropuerto

$nombre = $_POST['nombre'];
$pais = $_POST['pais'];
$mail = $_POST['mail'];
$telefono = $_POST['telefono'];
$comentarios = $_POST['comentarios'];


$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/plain";
$mensaje = "Este mensaje fue enviado por " . $nombre . " \r\n";
$mensaje .= "Su e-mail es: " . $mail . " \r\n";
$mensaje .= "Su Direccion es: " . $pais . " \r\n";
$mensaje .= "Su telefono es: " . $telefono . " \r\n";
$mensaje .= "--\r\n";
$mensaje .= "--\r\n";
$mensaje .= "Llegada desde aeropuerto " . $tipollegada . " \r\n";
$mensaje .= "Hotel de destino " . $hoteldest . " \r\n";
$mensaje .= "Numero de pax " . $pax . " \r\n";
$mensaje .= "Llegada de vuelo " . $llegvuelo . " \r\n";
$mensaje .= "Numero de vuelo" . $numvuelo . " \r\n";
$mensaje .= "Aerolinea" . $aerolinea . " \r\n";
$mensaje .= "Pickup de salida" . $pickupvuelo . " \r\n";
$mensaje .= "Hora de salida de vuelo" . $salvuelo . " \r\n";
$mensaje .= "Aerolinea y numero vuelo salida" . $aerosalida . " \r\n";
$mensaje .= "----- \r\n";
$mensaje .= "---- \r\n";
$mensaje .= "Mensaje: " . $_POST['comentarios'] . " \r\n";
$mensaje .= "Enviado el " . date('d/m/Y', time());
$para = 'contacto@cancunrep.com';
$asunto = 'Mensaje transfers Cancunrep';

mail($para, $asunto, utf8_decode($mensaje), $header);
 	


?>


<h5>Por favor espere, estamos procesando su reserva.</h5><br><br>
<img src="../images/cargando-ldng.gif" >



<body onload="document.paypalform.submit();">
<form action="https://www.paypal.com/cgi-bin/webscr" name="paypalform" method="post">
<input name="cmd" type="hidden" value="_cart" />
<input name="upload" type="hidden" value="1" />
<input name="business" type="hidden" value="infocancunrep@gmail.com" />
<input name="shopping_url" type="hidden" value="http://www.cancunrep.com/" />
<input name="currency_code" type="hidden" value="USD" />


<input name="return" type="hidden" value="http://www.cancunrep.com/mensaje_compra_exitosa.php" />
<input name="notify_url" type="hidden" value="http://www.cancunrep.com/paypal_ipn.php" />
<input name="cancel_return" type="hidden" value="http://www.cancunrep.com/">


<input name="rm" type="hidden" value="2" />

<input name="item_name_1" type="hidden" value=<?php echo '"'.$transfertype.'"' ?> />
<input name="amount_1" type="hidden" value=<?php echo '"'.$transcunzh.'"' ?> />
<input name="quantity_1" type="hidden" value="1" /> 

<?php } ?>
