<?php
//Seleccion de transfers
@$destinotransfer= $_POST['tipotransfer'];
@$tiporeserva= $_POST['seleccionrs'];
@$costetot= $_POST['prctrn'];
@$checkon = isset($_POST['opcion']) ? $_POST['opcion'] : 'off';
if($tiporeserva == null)
{ $tiporeserva="One Way";
}
//declaracion de variables que siempre llegaran
@$tipollegada = $_POST['tipollegada'];
@$hoteldest = $_POST['hoteldest'];
@$pax = $_POST['pax'];
@$llegvuelo = $_POST['llegvuelo'];
@$numvuelo = $_POST['numvuelo'];
@$horalleg = $_POST['horalleg'];
@$ampm = $_POST['ampm'];
@$aerolinea = $_POST['aerolinea'];

@$nombre = $_POST['nombre'];
@$pais = $_POST['pais'];
@$mail = $_POST['mail'];
@$telefono = $_POST['telefono'];
@$comentarios = $_POST['comentarios'];

if ($tiporeserva =="One Way") {

	//es one way ****

	//correo local
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/html; charset=iso-8859-1\n";
//Pasamos al cliente
$headerclt ="Content-Type: text/html; charset=iso-8859-1\n";
$headerclt.="From: Cancun Transfers <info@cancun-shuttle.com>\r\n";
$headerclt.="Replay-to: info@cancun-shuttle.com \r\n";
$headerclt.="Return-path: info@cancun-shuttle.com \r\n";
//Mensaje Principal
$mensaje = '<img src="http://www.cancun-shuttle.com/images/logo-small.jpg" /><br><br><br>'  . " \r\n";
$mensaje .= "Apreciable  ". $nombre." :<br><br><p>Cancun-shuttle.com te da las gracias, a continuación podrás verificar los datos que nos has enviado, si tienes alguna duda o algun cambio, por favor puedes respondernos a este correo para resolver tu peticion, los cambios podrán hacerse hasta 24 horas antes de su llegada, estaremos esperándolo a la hora y fecha indicada en el formulario que amablemente nos envió.<br>También puede comunicarse con nosotros a nuestros siguientes teléfonos, telefono de oficina: Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Oficina: 998 214 90 12, en horarios y días habiles con gusto le atenderemos.</p> <br><hr><br> " . " \r\n";
$mensaje .= "Servicio solicitado: " .$destinotransfer.'  '.$tiporeserva. "<br> \r\n";
$mensaje .= "Passajeros: " . $pax . "<br> \r\n";
$mensaje .= "Hotel llegada: " . $hoteldest . "<br> \r\n";
$mensaje .= "Fecha llegada: " . $llegvuelo . "<br> \r\n";
$mensaje .= "Hora de la llegada: " . $horalleg .' '.$ampm. "<br> \r\n";
$mensaje .= "Su e-mail es: " . $mail . "<br> \r\n";
$mensaje .= "Su telefono es: " . $telefono . "<br> \r\n";
$mensaje .= "Su pais es: " . $pais . "<br> \r\n";
$mensaje .= "Mensaje que nos envio: " . $comentarios . "<br> \r\n";
$mensaje .= "<br><br>Reservaciones Cancun-Shuttle<br>Tel: desde Cancun 286 15 25, Cel. Oficina: 998 214 90 12, desde Mexico 01 800 161 4957, Usa / CAN 888 811 4255<br>Correo: info@cancun-shuttle.com<br> Cancun Quintana Roo Mexico<br>
www.cancun-shuttle.com" . '<br><br><img src="http://www.cancun-shuttle.com/images/logo-small.jpg" /><br><br><br><p>El contenido de este correo es de caracter informativo, Cancun-shuttle.com agradece su preferencia.</p><br><br>'  . " \r\n";
@$mensaje .= "Enviado el " . date('d/m/Y', time());
//Envio de datos extra a mail principal
$datoscontrol ="<br><br>La casilla de reserva en linea esta". $checkon . "<br> \r\n";
//correo de envio
$para = 'info@cancun-shuttle.com';
$asunto = 'Mensaje Cancun Shuttle';


mail($para, $asunto, utf8_decode($mensaje.$datoscontrol), $header);
sleep(2); #Pausa de 2 Segundos cliente
mail($mail, "Cotizacion de Reservaciones Cancun Shuttle", utf8_decode($mensaje), $headerclt);

//Mostrar html 
?>
<!DOCTYPE html>
<html>
<head>
  <title>Contact US | Official Airport Transportation</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="cancun airport transfers, kinmont transfers contact us, cancun shuttle, cancun transfers"/>
  <meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
  <meta property="og:url" content="http://www.cancun-shuttle.com/" />
  <meta property="og:title" content="Cancun Airport shuttle">
  <meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="http://www.cancun-shuttle.com./img/og-cancunshuttle.jpg">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="300">
  <meta property="og:image:height" content="300">
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="es_ES" />
  <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
  <link href="../css/main-style.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css" type="text/css">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("../_structure/menu-int.php"); ?>

<div class="tel-box">
  <div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<div class="container">
   <div class="row">
        <div class="col-xs-12 col-md-12">
        <p><?php echo $nombre.'  ';  ?>Gracias por reservar con nosotros, hemos enviado un correo a la dirección que nos indicó, por favor revise su bandeja de entrada, algunas veces su cliente de correo puede ponerlo en la bandeja de spam, favor de verificarlo.<br><br>
Si tiene alguna duda con respecto a nuestros servicios, por favor contáctenos o llámenos a los siguientes telefono de oficina: Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Oficina: 998 214 90 12, estamos para servirles.
</p>
        </div>
       </div>
      </div>
<br><br><br>
<?php include("../_structure/footer.php"); ?>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>

<?php


} else {
	//es round trip
$horasal = $_POST['horasal'];
$salampm = $_POST['salampm'];
$salvuelo = $_POST['salvuelo'];
$aerosalida = $_POST['aerosalida'];

//correo local
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/html; charset=iso-8859-1\n";
//Pasamos al cliente
$headerclt ="Content-Type: text/html; charset=iso-8859-1\n";
$headerclt.="From: Cancun Shuttle <info@cancun-shuttle.com>\r\n";
$headerclt.="Replay-to: info@cancun-shuttle.com \r\n";
$headerclt.="Return-path: info@cancun-shuttle.com \r\n";
//Mensaje Principal
$mensaje = '<img src="http://www.cancun-shuttle.com/images/logo-small.jpg" /><br><br><br>'  . " \r\n";
$mensaje .= "Apreciable  ". $nombre." :<br><br><p>cancun-shuttle.com te da las gracias, a continuación podrás verificar los datos que nos has enviado, si tienes alguna duda o algún cambio, por favor puedes respondernos a este correo para resolver tu petición, los cambios podrán hacerse hasta 24 horas antes de su llegada, estaremos esperándolo a la hora y fecha indicada en el formulario que amablemente nos envió.<br>También puede comunicarse con nosotros a nuestros siguientes teléfonos, telefono de oficina: Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Oficina: 998 214 90 12, en horarios y días hábiles con gusto le atenderemos.</p> <br><hr><br> " . " \r\n";
$mensaje .= "Servicio solicitado: " .$destinotransfer.'  '.$tiporeserva. "<br> \r\n";
$mensaje .= "Passajeros: " . $pax . "<br> \r\n";
$mensaje .= "Hotel llegada: " . $hoteldest . "<br> \r\n";
$mensaje .= "Fecha llegada: " . $llegvuelo . "<br> \r\n";
$mensaje .= "Hora de la llegada: " . $horalleg .' '.$ampm. "<br> \r\n";
$mensaje .= "Fecha de salida: " . $salvuelo . "<br> \r\n";
$mensaje .= "Hora de salida: " . $horasal.' '.$salampm. "<br> \r\n";
$mensaje .= "Su e-mail es: " . $mail . "<br> \r\n";
$mensaje .= "Su telefono es: " . $telefono . "<br> \r\n";
$mensaje .= "Su pais es: " . $pais . "<br> \r\n";
$mensaje .= "Mensaje que nos envio: " . $comentarios . "<br> \r\n";
$mensaje .= "<br><br>Reservaciones cancun-shuttle<br>Tel: desde Cancun 286 15 25, Cel. Oficina: 998 214 90 12, desde Mexico 01 800 161 4957, Usa / CAN 888 811 4255<br>Correo: info@cancun-shuttle.com<br> Cancun Quintana Roo Mexico<br>
www.cancun-shuttle.com" . '<br><br><img src="http://www.cancun-shuttle.com/img/logo-mediano.png" /><br><br><br><p>El contenido de este correo es de caracter informativo, cancun-shuttle.com agradece su preferencia.</p><br><br>'  . " \r\n";
@$mensaje .= "Enviado el " . date('d/m/Y', time());
//Envio de datos extra a mail principal
$datoscontrol ="<br><br>La casilla de reserva en linea esta". $checkon . "<br> \r\n";
//correo de envio
$para = 'info@cancun-shuttle.com';
$asunto = 'Mensaje Cancun Shuttle';


mail($para, $asunto, utf8_decode($mensaje.$datoscontrol), $header);
sleep(2); #Pausa de 2 Segundos cliente
mail($mail, "Cotizacion de Reservaciones Cancun Shuttle", utf8_decode($mensaje), $headerclt);


?>
<!DOCTYPE html>
<html>
<head>
  <title>Contact US | Official Airport Transportation</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="cancun airport transfers, kinmont transfers contact us, cancun shuttle, cancun transfers"/>
  <meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
  <meta property="og:url" content="http://www.cancun-shuttle.com/" />
  <meta property="og:title" content="Cancun Airport shuttle">
  <meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="http://www.cancun-shuttle.com./img/og-cancunshuttle.jpg">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="300">
  <meta property="og:image:height" content="300">
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="es_ES" />
  <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
  <link href="../css/main-style.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css" type="text/css">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("../_structure/menu-int.php"); ?>

<div class="tel-box">
  <div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<div class="container">
   <div class="row">
        <div class="col-xs-12 col-md-12">
        <p><?php echo $nombre.'  ';  ?>Gracias por reservar con nosotros, hemos enviado un correo a la dirección que nos indicó, por favor revise su bandeja de entrada, algunas veces su cliente de correo puede ponerlo en la bandeja de spam, favor de verificarlo.<br><br>
Si tiene alguna duda con respecto a nuestros servicios, por favor contáctenos o llámenos a los siguientes telefono de oficina: Usa / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Oficina: 998 214 90 12, estamos para servirles.
<br><br><br><br><br><br><br><br><br>
</p>
        </div>
       </div>
      </div>
<br><br><br>
<?php include("../_structure/footer.php"); ?>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>

<?php
}
?>