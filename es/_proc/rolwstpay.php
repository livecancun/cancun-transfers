<?php
/**
 Wstravel v1.0.0 (2018-Sep)
 Copyright (c) 2017-2019 Wstravel.
 Proceso de pago usando Paypay: https://www.paypal.com
 */
$ROL_URL_INDEX = '../index.php';
$WST = include '../../apiwst/rolwstcfg.php';
// validacion del CLIENTE ID
if (empty($_POST['ROL_CLIENT_ID']) || $_POST['ROL_CLIENT_ID'] != $WST['ROL_CLIENT_ID'] || $_SERVER['REQUEST_METHOD'] != 'POST') {
    header("Location: {$ROL_URL_INDEX}");
}
// validamos el costo
if (!isset($_POST['costo']) || empty($_POST['costo']) || !is_numeric($_POST['costo']) || (float)$_POST['costo'] <= 0) {
    header("Location: {$ROL_URL_INDEX}");
}
// validamos el servicio
if (!isset($_POST['servicio_nombre']) || empty($_POST['servicio_nombre'])) {
    header("Location: {$ROL_URL_INDEX}");
}
// validamos el traslado
if (!isset($_POST['traslado_nombre']) || empty($_POST['traslado_nombre'])) {
    header("Location: {$ROL_URL_INDEX}");
}
// validamos el folio
if (!isset($_POST['folio']) || empty($_POST['folio']) || strlen($_POST['folio']) != 19) {
    header("Location: {$ROL_URL_INDEX}");
}
// validamos si esta marcado el pago en linea
$ROL_WEB_PAY = false;
if (isset($_POST['webpay']) && is_string($_POST['webpay'])) {
    $flag = filter_var($_POST['webpay'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    if (!is_null($flag)) {
        $ROL_WEB_PAY = $flag;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
  <title>Contact US | Official Airport Transportation</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="cancun airport transfers, kinmont transfers contact us, cancun shuttle, cancun transfers"/>
  <meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
  <meta property="og:url" content="http://www.cancun-transfers.com/" />
  <meta property="og:title" content="Cancun Airport shuttle">
  <meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="300">
  <meta property="og:image:height" content="300">
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="es_ES" />
  <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
  <link href="../css/main-style.css" rel="stylesheet">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css" type="text/css">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<?php include("../_structure/menu-int.php"); ?>
<div class="tel-box">
  <div class="container">
      <div class="row">
          <div class="col-lg-1 col-lg-offset-6 text-right">
              <i class="fa fa-phone fa-2x wow bounceIn"></i>
          </div>
          <div class="col-lg-5">
              <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
          </div>
      </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-md-12">
      <p>Gracias por reservar con nosotros, hemos enviado un correo a la dirección que nos indicó, 
        por favor revise su bandeja de entrada, algunas veces su cliente de correo puede ponerlo en la bandeja de spam, 
        favor de verificarlo.
        <br><br>
        Si tiene alguna duda con respecto a nuestros servicios, por favor contáctenos o llámenos a los siguientes telefono 
        de oficina: USA / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25 , Cel. Oficina: 998 214 90 12, estamos para servirles.
      </p>
    </div>
  </div>
</div>
<br><br><br>
<?php include("../_structure/footer.php"); ?>
<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>
<?php if ($ROL_WEB_PAY):?>
<body onload="document.paypalform.submit();">
<form action="https://www.paypal.com/cgi-bin/webscr" name="paypalform" method="post">
<input name="cmd" type="hidden" value="_cart" />
<input name="upload" type="hidden" value="1" />
<input name="business" type="hidden" value="info@cancun-transfers.com" />
<input name="shopping_url" type="hidden" value="http://www.cancun-transfers.com/" />
<input name="currency_code" type="hidden" value="USD" />
<input name="return" type="hidden" value="http://www.cancun-transfers.com" />
<input name="notify_url" type="hidden" value="www.cancun-transfers.com/paypal_ipn.php" />
<input name="cancel_return" type="hidden" value="http://www.cancun-transfers.com/">
<input name="rm" type="hidden" value="2" />
<input name="item_name_1" type="hidden" value="<?php echo "{$_POST['traslado_nombre']} - {$_POST['servicio_nombre']}";?>"/>
<input name="amount_1" type="hidden" value="<?php echo $_POST['costo']; ?>"/>
<input name="quantity_1" type="hidden" value="1" />
<?php endif;?>