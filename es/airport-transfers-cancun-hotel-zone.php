<?php
include("_lib/lst-prc-nor.php");
$reson=1;
$destinotr = "Hotel Zone Cancun";
$prctrasonw = $onwtranscunzh;
$prctrasrtp = $rtwtranscunzh;
$tipores = "One Way";
@$tipores = $_POST['tiporeserva'];
if($tipores != null)
{ $eval=1;
} else { $eval=0;}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cancun Airport Hotel Zone | Luxury Cancun Transfers</title>
    <meta name="description"  content="We take you directly from Cancun Airport to the hotel zone of Cancun, in a luxury private service" />
    <meta name="keywords" content="cancun airport transfers, cancun shuttle, cancun transfers, cancun airport transportation, cancun airport hotel zone" />
	<meta name="robots" content="all" />
	<?php include("_bodyglobal/head-main.php"); ?>
    <link rel="stylesheet" href="css/pikaday.css">
    <link rel="alternate" hreflang="en" href="http://www.luxurytransferscancun.com/" />
    <link rel="alternate" hreflang="es" href="http://www.luxurytransferscancun.com/es/" />
    <script type="text/javascript" src="js/form.js"></script>
</head>
<body class="">
<?php include("_bodyglobal/menu-head.php"); ?>
<br><br>
<div class="kwd-space"><p>Cancun Airport Hotel Zone | Luxury World Transfers<p></div>
   <div class="container">
   <div class="row">
                <div class="col-xs-12 col-md-12 transfer-lnrn">
                        <p class="htxtbnc">Private service: <?php echo $tipores; ?></p>
                    </div>
      </div>
      <div class="row">
                <div class="col-xs-6 col-md-6">
                        <p class="servicerd">Service: <?php echo $destinotr; ?> </p>
                    </div>
                    <div class="col-xs-6 col-md-6">
                        <p class="pricerd">Price: <?php if ($tipores == "Round Trip") {echo $prctrasrtp; } else { echo $prctrasonw; } ?> USD</p>
                    </div>
      </div>
   </div>
   <br>
   <div class="container">
   <div class="row">
                <div class="col-xs-12 col-md-12">
                        <form name="selecopcion" method="POST" action="#">
<select name="tiporeserva" onchange="">
<option value="One Way" selected>One Way</option>
<option value="Round Trip">Round Trip</option>
</select>
<button type="submit" name="button" value="send">Modify</button>
</form>
                    </div>
      </div></div>
   <br>
   <?php if ($tipores == "Round Trip") { ?>
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-12 ">
        <form action="_proc/ordproces.php" onsubmit="return validar(this);" method="POST"  >
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-3 ">
    <div id="form-group">
         <label for="tipollegada">Arrival</label>
         <input type="text" name="tipollegada" value="Aeropuerto Cancun" class="form-control"  disabled />
         <input type="hidden" value="Cancun Airportt" name="tipollegada" />
    </div></div>
    <div class="col-xs-12 col-md-3 ">
    <div id="form-group">
         <label for="hoteldest">Hotel</label>
         <input type="text" name="hoteldest" value="" class="form-control" placeholder="Hotel" />
    </div></div>
    <div class="col-xs-12 col-md-3 col-offset-3  ">
    <div id="form-group">
         <label for="pax">Passengers</label>
    <select class="form-control" name="pax">
     <option value="1">1</option>
     <option value="2">2</option>
     <option value="3">3</option>
     <option value="4">4</option>
     <option value="5">5</option>
     <option value="6">6</option>
     <option value="7">7</option>
     <option value="8">8</option>
    </select>
    </div></div>

    </div>
    </div>
    <br>
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="llegvuelo">Arrival DATE</label>
         <input type="text" name="llegvuelo" class="form-control" id="datepicker" placeholder="Arrival date">
    </div></div>
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="numvuelo">Flight num.</label>
         <input type="text" name="numvuelo" class="form-control"  placeholder="Flight number" >
    </div></div>
     <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="horallegada">Arrival hour</label>
         <input type="text" name="horalleg" class="form-control"  placeholder="Arrival hour" >
    </div></div>
    <div class="col-xs-12 col-md-1 ">
    <div id="form-group">
         <label for="dianoche">AM/PM</label>
         <select class="form-control" name="ampm">
     <option value="noeligio" selected="selected"></option>
     <option value="AM">AM</option>
     <option value="PM">PM</option>
    </select>
    </div></div>

    <div class="col-xs-12 col-md-2 col-offset-3 ">
    <div id="form-group">
         <label for="aerolinea">Airline</label>
         <input type="text" name="aerolinea" class="form-control" placeholder="Airline" >
    </div></div>

    </div>
    </div>
     <br /><br />
         <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="horasalida">Depature hour</label>
         <input type="text" name="horasal" class="form-control"  placeholder="hora de llegada" >
    </div></div>
    <div class="col-xs-12 col-md-1 ">
    <div id="form-group">
         <label for="dianoche">AM/PM</label>
         <select class="form-control" name="salampm">
     <option value="noeligio" selected="selected"></option>
     <option value="AM">AM</option>
     <option value="PM">PM</option>
    </select>
    </div></div>
    <div class="col-xs-12 col-md-3 ">
    <div id="form-group">
         <label for="salvuelo">Depature DATE</label>
         <input type="text" name="salvuelo" class="form-control" id="datepicker2" placeholder="Depature date">
    </div></div>
    <div class="col-xs-12 col-md-3 col-offset-3 ">
    <div id="form-group">
         <label for="aerosalida">Airline / Flight number</label>
         <input type="text" name="aerosalida" class="form-control" placeholder="Airline / Flight number" >
    </div></div>

    </div>
    </div>
         <br /><br />
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-3 ">
    <div id="form-group">
         <label for="nombre">Name</label>
         <input type="text" name="nombre" class="form-control"  placeholder="Name" onblur="revisar(this);" >
    </div></div>
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="Pais">Country</label>
         <input type="text" name="pais" class="form-control"  placeholder="Country" >
    </div></div>
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="mail">Mail</label>
         <input type="text" name="mail" placeholder="Mail required**" class="form-control" onblur="revisar(this); revisaremail(this);" >
    </div></div>
    <div class="col-xs-12 col-md-2 col-offset-3 ">
    <div id="form-group">
         <label for="telefono">Phone</label>
         <input type="text" name="telefono" class="form-control"  placeholder="Phone" onblur="revisar(this);" >
    </div></div>
    <span class="clearfix"></span>
    <div class="col-xs-12 col-md-9 col-offset-3 ">
    <div id="form-group">
         <label for="comentarios">Comments</label>
         <textarea name="comentarios" class="form-control" rows="5" placeholder="comments"></textarea>

    <input type="hidden" name="prctrn" value="<?php echo $prctrasrtp; ?>">

  
    </div></div>

    </div>
    </div>
    <?php } else { ?>
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-12 ">
        <form action="_proc/ordproces.php" onsubmit="return validar(this);" method="POST"  >
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-3 ">
    <div id="form-group">
         <label for="tipollegada">Arrival</label>
         <input type="text" name="tipollegada" value="Cancun Airport" class="form-control"  disabled />
         <input type="hidden" value="Aeropuerto Cancun" name="tipollegada" />
    </div></div>
    <div class="col-xs-12 col-md-3 ">
    <div id="form-group">
         <label for="hoteldest">Hotel</label>
         <input type="text" name="hoteldest" value="" class="form-control" placeholder="Hotel" />
    </div></div>
    <div class="col-xs-12 col-md-3 col-offset-3  ">
    <div id="form-group">
         <label for="pax">Passengers</label>
    <select class="form-control" name="pax">
     <option value="1">1</option>
     <option value="2">2</option>
     <option value="3">3</option>
     <option value="4">4</option>
     <option value="5">5</option>
     <option value="6">6</option>
     <option value="7">7</option>
     <option value="8">8</option>
    </select>
    </div></div>

    </div>
    </div>
    <br>
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="llegvuelo">Arrival date</label>
         <input type="text" name="llegvuelo" class="form-control" id="datepicker" placeholder="Arrival date">
    </div></div>
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="numvuelo">Flight number</label>
         <input type="text" name="numvuelo" class="form-control"  placeholder="Flight number" >
    </div></div>
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="horallegada">Arrival hour</label>
         <input type="text" name="horalleg" class="form-control"  placeholder="Arrival Hour" >
    </div></div>
    <div class="col-xs-12 col-md-1 ">
    <div id="form-group">
         <label for="dianoche">AM/PM</label>
         <select class="form-control" name="ampm">
     <option value="noeligio" selected="selected"></option>
     <option value="AM">AM</option>
     <option value="PM">PM</option>
    </select>
    </div></div>

    <div class="col-xs-12 col-md-2 col-offset-3 ">
    <div id="form-group">
         <label for="aerolinea">Airline</label>
         <input type="text" name="aerolinea" class="form-control" placeholder="Airline" >
    </div></div>

    </div>
    </div>
         <br /><br />
    <div class="container">
    <div class="row">
    <div class="col-xs-12 col-md-3 ">
    <div id="form-group">
         <label for="nombre">Name</label>
         <input type="text" name="nombre" class="form-control"  placeholder="Name" onblur="revisar(this);" >
    </div></div>
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="Pais">Pais</label>
         <input type="text" name="pais" class="form-control"  placeholder="Country" >
    </div></div>
    <div class="col-xs-12 col-md-2 ">
    <div id="form-group">
         <label for="mail">Mail</label>
         <input type="text" name="mail" placeholder="Mail required" class="form-control" onblur="revisar(this); revisaremail(this);" >
    </div></div>
    <div class="col-xs-12 col-md-2 col-offset-3 ">
    <div id="form-group">
         <label for="telefono">Phone</label>
         <input type="text" name="telefono" class="form-control"  placeholder="Phone" >
    </div></div>
    <span class="clearfix"></span>
    <div class="col-xs-12 col-md-9 col-offset-3 ">
    <div id="form-group">
         <label for="comentarios">Comments</label>
         <textarea name="comentarios" class="form-control" rows="5" placeholder="Comments"></textarea>

         <input type="hidden" name="prctrn" value="<?php echo $prctrasonw; ?>">

  
    </div></div>

    </div>
    </div>
    

    <?php } ?>
    <?php if($reson == 1) { ?>
  <div class="checkbox">
  <label>
  <input type="checkbox" name="opcion">I would like to pay online</label></div>
  <?php } ?>
        <br /><br />
        <input type="hidden" name="tipotransfer" value="<?php echo $destinotr; ?>">
        <input type="hidden" name="seleccionrs" value="<?php echo $tipores; ?>">
        <input type="submit" class="btn btn-primary" name="Submit" value="Reserve">
        
        </form>

          </div></div></div>
<br><br><br><br><br><br><br><br><br>
<div id="footer">
Luxury World Transport 2015 all reserved
<br><br><br><br>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/pikaday.js"></script>
   <script>

    var picker = new Pikaday(
    {
        field: document.getElementById('datepicker'),
        firstDay: 1,
        format: 'L',
        minDate: new Date('2000-01-01'),
        maxDate: new Date('2020-12-31'),
        yearRange: [2000,2020]
    });
        var picker = new Pikaday(
    {
        field: document.getElementById('datepicker2'),
        firstDay: 1,
        format: 'L',
        minDate: new Date('2000-01-01'),
        maxDate: new Date('2020-12-31'),
        yearRange: [2000,2020]
    });

    </script>
</body>
</html>
