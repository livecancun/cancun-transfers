<!DOCTYPE html>
<html>
<head>
	<title>Contact US | Official Airport Transportation</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="cancun airport transfers, kinmont transfers contact us, cancun shuttle, cancun transfers"/>
	<meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
	<meta property="og:url" content="http://www.cancun-shuttle.com/" />
	<meta property="og:title" content="Cancun Airport shuttle">
	<meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="http://www.cancun-shuttle.com./img/og-cancunshuttle.jpg">
	<meta property="og:image:type" content="image/png">
	<meta property="og:image:width" content="300">
	<meta property="og:image:height" content="300">
	<meta property="og:locale" content="en_US" />
	<meta property="og:locale:alternate" content="es_ES" />
	<link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
	<link href="css/main-style.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
	<div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<div class="container">
  <div class="row">
   <h2 class="font-bl1-20">Contact US</h2>
                    <p>Should you have any question or comment about our Ground Transportation Service from/to Cancun International Airport to/from Cancun, Playa del Carmen or Riviera Maya, please, fill out this form an we will get back to you as soon as possible.</p>
<br /><br />
     <?php 
$nombre = $_POST['nombre'];
$mail = $_POST['mail'];
$pais = $_POST['pais'];
$telefono = $_POST['telefono'];
$empresa = $_POST['comentario'];
$tipo = $_POST['tipo'];
$hotel = $_POST['hotel'];
$vuelo = $_POST['vuelo'];
$llegadavlo = $_POST['llegadavlo'];
$salidavlo = $_POST['salidavlo'];
$pasajeros = $_POST['pasajeros'];


if ($mail ==""){

echo '<strong>Please, Check the boxes name and email are not empty </strong> <br><br>';
include ("efcontacto.htm");

} else {
$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/plain";

$mensaje = "Este mensaje fue enviado por " . $nombre. " \r\n";
$mensaje .= "Su e-mail es: " . $mail . " \r\n";
$mensaje .= "Su Direccion es: " . $pais . " \r\n";
$mensaje .= "Su telefono es: " . $telefono . " \r\n";
$mensaje .= "Mensaje: " . $_POST['comentario'] . " \r\n";
$mensaje .= "tipo reserva: " . $tipo . " \r\n";
$mensaje .= "Hotel: " . $hotel . " \r\n";
$mensaje .= "En el vuelo: " . $vuelo . " \r\n";
$mensaje .= "Horario de llegada: " . $llegadavlo . " \r\n";
$mensaje .= "Horario de salida: " . $salidavlo . " \r\n";
$mensaje .= "Pasajeros: " . $pasajeros . " \r\n";
$mensaje .= "Enviado el " . date('d/m/Y', time());

$para = 'info@cancun-shuttle.com';
$asunto = 'Mensaje Cancun-transfers';

mail($para, $asunto, utf8_decode($mensaje), $header);

echo $nombre. ' , <stron>Your comment has been sent successfully</strong>, thanks,'.'<br><br><a href="http://www.cancun-shuttle.com">Back to Home page</a>';
}
?></p><br /><br />
 
<br><br><br><br>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>