<!DOCTYPE html>
<html lang="es">
<head>
  <title>Traslado Aeropuerto | Taxi Oficial | Cancun Shuttle</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="cancun taxi, transportacion cancun aeropuerto hotel, traslado aeropuerto, cancun aeropuerto, taxi aeropuerto cancun, playa del carmen, riviera maya, cancun shuttle, canucn transfers "/>
  <meta name="description" content="transportación privada desde y hacia el aeropuerto, Puerto Juárez, Zona Hotelera, así como a cualquier parte de la Riviera Maya"/>
  <meta property="og:url" content="http://www.cancun-transfers.com/" />
  <meta property="og:title" content="Cancun Airport shuttle">
  <meta property="og:description" content="Tenemos un equipo profesional con mucha experiencia ubicado en el aeropuerto internacional de la ciudad de Cancún, para su traslado desde el aeropuerto hacia su hotel y viceversa, directo ¡sin escalas! !" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="http://www.cancun-transfers.com/img/og-cancunshuttle.jpg">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="300">
  <meta property="og:image:height" content="300">
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="es_ES" />
  <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
  <link href="css/main-style.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/bootstrap-select.min.css" rel="stylesheet">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
  <div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<div id="cunshuttle-mainbaner">
    <div class="row">
                <div class="col-lg-5">
                    <div class="cunshuttle-rbx-mb">
                      <div class="container">

                        <!-- inicio box -->
                      <form action="reserve.php" method="POST">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="col-md-12">
                                  <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <label for="seleeccionarlb">Seleccionar</label>
                                        <!--<select class="form-control" name="tipors" id="wst-tipo-traslado"></select>-->
                                        <select class="form-control show-tick" name="wst-tipo-traslado" id="wst-tipo-traslado" data-width="100%" data-title="Seleccione"></select>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group ">
                                        <label for="seleeccionarlb">Date:</label>
                                        <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                        <input class="form-control" name="wst-fecha" id="wst-fecha" type="date">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label for="hotellb">Hotel</label>
                                        <!--<input type="text" class="form-control" name="hotelleg" id="hotel" placeholder="Hotel">-->
                                        <!--<select class="form-control" name="hotelleg" id="wst-hotel"></select>-->
                                        <select class="form-control show-tick" name="wst-hotel" id="wst-hotel" data-live-search="true" data-width="100%" data-title="Seleccione hotel" data-size="5"></select>
                                      </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <div class="res-btn">
                                          <button type="submit" class="btn btn-success btn-lg ">Reserve</button>
                                        </div>
                                        
                                      </div>
                                  </div>
                            </div>
                        </div><!--End row -->
                      </form>
                      </div><!-- End container -->
                    </div>
                </div>

    </div>
</div>
</div>
                <div class="col-lg-7"><br>
                    <p class="font-bl-17">Contamos con 20 años de experiencia a su servicio. </p>
                </div>
            </div>
        </div>


</div>

<div class="services-ribbon">
  <div class="container">
      <div class="row">
      <div class="col-md-12">
  <p>Servicios</p>
  </div>
   </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/es/cancun-aeropuerto-zona-hotelera.php"><h3>Aeropuerto <- -> Zona Hotelera Cancun</h3></a>
            <p>Cancun Aeropuerto - Zona hotelera de Cancun unicamente, Van privada para 1-8 pasajeros, <b>Viaje Redondo</b>, Viaje directo, Oferta limitada</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                70 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/es/cancun-aeropuerto-zona-hotelera.php"><h3>
                60 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/es/cancun-aeropuerto-zona-hotelera.php" role="button">Reservar</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/es/playa-del-carmen-cancun-aeropuerto.php"><h3>Aeropuerto <- -> Playa del Carmen</h3></a>
            <p>Cancun Aeropuerto - Playa del Carmen unicamente, Van privada para 1-8 pasajeros, <b>Viaje Redondo</b>, Viaje directo, Oferta limitada</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                110 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/es/playa-del-carmen-cancun-aeropuerto.php"> 
              <h3>
                100 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/es/playa-del-carmen-cancun-aeropuerto.php" role="button">Reservar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/es/puerto-morelos-cancun-aeropuerto.php"><h3>Aeropuerto <- -> Puerto Morelos</h3></a>
            <p>Cancun Aeropuerto - Puerto Morelos unicamente, Van privada para 1-8 pasajeros, <b>Viaje Redondo</b>, Viaje directo, Oferta limitada</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                80 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/es/puerto-morelos-cancun-aeropuerto.php"><h3>
                75 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/es/puerto-morelos-cancun-aeropuerto.php" role="button">Reservar</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/es/puerto-aventuras-cancun-aeropuerto.php"><h3>Aeropuerto <- -> Puerto Aventuras</h3></a>
            <p>Cancun Aeropuerto - Puerto Aventuras unicamente, Van privada para 1-8 pasajeros, <b>Viaje Redondo</b>, Viaje directo, Oferta limitada</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                130 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/es/puerto-aventuras-cancun-aeropuerto.php"> 
              <h3>
                120 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/es/puerto-aventuras-cancun-aeropuerto.php" role="button">Reservar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/es/cancun-centro-cancun-aeropuerto.php"><h3>Aeropuerto <- -> Hoteles Cancun centro</h3></a>
            <p>Cancun Aeropuerto - Hoteles Centro Cancun unicamente, Van privada para 1-8 pasajeros, <b>Viaje Redondo</b>, Viaje directo, Oferta limitada</p><br>
            </div>
            <div class="col-md-4">
              <h4 class="font-rd-17"><del>
                50 USD</del>
              </h4> 
              <a href="http://www.cancun-transfers.com/es/cancun-centro-cancun-aeropuerto.php"><h3>
                40 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/es/cancun-centro-cancun-aeropuerto.php" role="button">Reservar</a>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-8 bg-info">
            <a href="http://www.cancun-transfers.com/es/playa-mujeres-cancun-aeropuerto.php"><h3>Aeropuerto <- -> Playa Mujeres</h3></a>
            <p>Cancun Aeropuerto - Playa Mujeres hoteles, Van privada para 1-8 pasajeros, <b>Viaje Redondo</b>, Viaje directo, Oferta limitada</p><br>
            </div>
            <div class="col-md-4">
            <h4 class="font-rd-17"><del>
                50 USD</del>
              </h4>
              <a href="http://www.cancun-transfers.com/es/playa-mujeres-cancun-aeropuerto.php"> 
              <h3>
                40 USD
              </h3> </a>
              <a class="btn btn-primary btn-lg" href="http://www.cancun-transfers.com/es/playa-mujeres-cancun-aeropuerto.php" role="button">Reservar</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<br><br>
<div class="container">
  <div class="row">
    <div class="col-md-8">
    </div>
    <div class="col-md-4">
      <h3 class="text-center text-success">
        Lista de destinos
      </h3> <a href="http://www.cancun-transfers.com/es/tarifas-aeropuerto.php" class="btn btn-info btn-lg btn-block" type="button">Ver lista de destinos</a>
    </div>
  </div>
</div>
<br><br>
<div class="container">
    <div class="row">
<div class="col-md-6 md-margin-bottom-30">
                    <h2 class="font-bl1-20">NOSOTROS</h2>
                    <p>Estamos disponibles las 24 hrs / 365 dias del año. Tenemos un equipo profesional con mucha experiencia ubicado en el aeropuerto internacional de la ciudad de Cancún, monitoreado exclusivamente su vuelo. Listo para darle la bienvenida y guiarlos hasta el vehículo reservado exclusivamente para su transportación. Desde el aeropuerto hacia su hotel y viceversa, directo ¡sin escalas! </p><br>
                    <a href="http://www.cancun-transfers.com/transfers-rates.php">Mira nuestras tarifas</a><br><br>
                </div>
<div class="col-md-6 md-margin-bottom-30">
                    <h2 class="font-gs-20">Cancun Airport location</h2>
                    <iframe width="580" height="360" src="http://www.youtube.com/embed/eIBgWC5Jxxw?rel=0" frameborder="0" allowfullscreen></iframe>
                </div>
  </div>
</div>
<br><br>
<div class="container">
    <div class="row">
<div class="col-md-6 md-margin-bottom-30">
                    <h2 class="font-bl1-20">LASTMINUT OFFER</h2>
                    <img src="images/cancun-deal1-transfers.jpg">
                </div>
<div class="col-md-6 md-margin-bottom-30">
                    <h2 class="font-gs-20">NUESTROS SERVICIOS</h2>
                    <p>Nuestro equipo cuenta con una gama de servicios que van desde la transportación terrestre Hotel – Aeropuerto – Hotel, así como servicios de transportación privada desde y hacia el aeropuerto, Puerto Juárez, Zona Hotelera, así como a cualquier parte de la Riviera Maya.
Reserve con nosotros privacidad y servicio, no se pierda la oportunidad de obtener los grandes beneficios que le brindamos, reserve con cancun-transfers.com de manera Fácil , Segura y Sencilla.</p>
                </div>
  </div>
</div>
<br><br>
  <div class="container">
      <div class="row">
      <div class="col-md-12">
  <p class="font-gr-20"> <i class="fa fa-bus fa-2x wow bounceIn"></i> | OBTEN LOS MEJORES PRECIOS |</p>
  </div>
   </div>
  </div>
 

 <div class="container">
  <div class="row">
    <div class="col-md-4">
      <h3>
        Cancun Hoteles
      </h3>
      <img src="images/cancun-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Playa del Carmen
      </h3>
      <img src="images/playa-del-carmen-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Riviera Maya Hotels
      </h3>
      <img src="images/riviera-maya-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
    <p>
    cancun-transfers.com por KinMont, es una empresa de transporte confiable y seguro para proveer transporte a viajeros, parejas, familias, grupos pequeños y grandes. Todos los autobuses y van tienen aire acondicionado, con licencia y aseguradas. Todos los conductores son profesionales y bilingües. Háganos saber sus necesidades especiales... Asistencia especial para viajeros discapacitados. 
    </p>
    </div>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="../js/underscore-min.js"></script>
<script src="../js/backbone-min.js"></script>
<script src="../js/bootstrap-select.min.js"></script>
<script src="../js/rolapiwst.js"></script>
</body>
</html>