<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="http://www.cancun-transfers.com"><img src="images/cancun-shuttle-logo.png" class="img-responsive" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="http://www.cancun-transfers.com/es">Inicio</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.cancun-transfers.com/es/tarifas-aeropuerto.php">Tarifas</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.cancun-transfers.com/es/llegadas.php">Llegadas</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.cancun-transfers.com/es/contactanos.php">Contactanos</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.cancun-transfers.com/">Ingles</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>