<div class="reservation-ribbon">
  <div class="container font-bn1-10">
  <div class="row">
    <div class="col-md-6">
      <p>Siganos al llegar al aeropuerto</p>
      <img src="images/cancun-transfers-reserve-secure.jpg" alt="Secure Cancun Transfers" />
    </div>
    <div class="col-md-6">
      <div class="row">
        <div class="col-md-4">
        <p>Aire acondicionado</p>
        <i class="fa fa-snowflake-o fa-4x" aria-hidden="true"></i>
        </div>
        <div class="col-md-4">
        <p>Servicio profesional</p>
        <i class="fa fa-diamond fa-4x" aria-hidden="true"></i>

        </div>
        <div class="col-md-4">
        <p>Privado, Reserva Facil</p>
        <i class="fa fa-plus-square-o fa-4x" aria-hidden="true"></i>
        </div>
      </div>
    </div>
  </div>
</div>
</div>