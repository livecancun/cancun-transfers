<div class="container">
    <form id="wst-form-transfer">
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-5 col-md-5">
                    <h4><p class="htxtbnc label label-info" id="wst-servicio-nombre"></p></h4>
                </div>
                <div class="col-xs-7 col-md-7">
                    <h4><p class="label label-default">Precio:$ <span id="wst-tarifa">0</span> USD</p></h4>
                </div>
            </div>    
            <div class="row">
                <div class="col-md-12 frm-res">
                <div class="row">
                        <div class="col-md-5">
                            <label for="tipo">Tipo</label>
                            <select class="form-control show-tick" name="tipors" id="wst-tipo-traslado" data-width="100%" data-title="Seleccione"></select>      
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="hoteldest">Hotel</label> 
                                <div class="pull-right">
                                    <input type="checkbox" id="wst-hotel_nofound"> <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No encontre mi Hotel
                                </div>
                                <div id="wst-hotel-list">
                                    <select class="form-control show-tick" id="wst-hotel-destino" data-live-search="true" data-width="100%" data-title="Seleccione hotel destino" data-size="5"></select>
                                </div>
                                <div id="wst-hotel-input" class="hide">
                                    <input type="text" class="form-control" id="wst-hotel-destino-nombre" placeholder="Nombre de mi Hotel">
                                </div>
                            </div>   
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="pax">Pasajeros</label>
                                <select class="form-control show-tick" name="owpaxSal" id="wst-pasajeros" data-width="100%" data-size="5">
                                    <option value="1">1</option>
                                    <option value="2" selected>2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                 </select>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>    
            <div class="row" id="wst-row-llegada">
                <div class="col-md-12 frm-res">
                    <h4><p class="text-primary">Llegada </p></h4>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="seleeccionarlb">Fecha llegada</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                    <input class="form-control" name="datearr" id="wst-fecha-llegada" type="date">
                                </div>
                            </div>
                        </div>                    
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="lbHorallegada">Hora llegada</label>
                                <div class="input-group date" id="horallegada">
                                    <input class="form-control" name="horallegada" id="wst-hora-llegada" placeholder="24 hrs 13:25" type="time">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>                    
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="aerolinea">Aerolinea</label>
                                <input type="text" class="form-control" id="wst-aerolinea" name="aerolinea" placeholder="Ej Americana">
                            </div>  
                        </div>                    
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="numvuelo">Numero vuelo</label>
                                <input type="text" class="form-control" id="wst-num-vuelo" name="num-vuelo" placeholder="Ej ABC-123">
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
            <!-- Datos de salida -->
            <div class="row" id="wst-row-salida">
                <div class="col-md-12 frm-res">
                    <h4><p class="text-primary">Salida </p></h4>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label for="owfechaSal">Fecha salida</label>
                                <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></span>
                                <input class="form-control" name="owfechaSal" id="wst-fecha-salida" type="date">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="owpickupSal">Hora Pick-up</label>
                                <div class="input-group date" id="owpickupSal">
                                <input class="form-control" name="owpickupSal" id="wst-pickup-salida" placeholder="24 hrs 13:25" type="time">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                                </div>
                            </div>  
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="owsalVuelo">Salida Vuelo</label>
                                <div class="input-group date" id="owsalVuelo">
                                <input class="form-control" name="owsalVuelo" id="wst-hora-vuelo-salida" placeholder="24 hrs 13:25" type="time">
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-time"></span>
                                </span>
                                </div>
                            </div>  
                        </div>                    
                    </div>    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 frm-res">
                    <h4><p class="text-primary">Datos personales </p></h4>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="nombre">Nombre completo</label>
                                <input type="text" class="form-control" id="wst-nombre-completo" name="nombre" placeholder="John Doe">
                            </div>   
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="wst-email" name="email" placeholder="john@gmail.com" required="required">
                            </div>   
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="pais">Pais</label>
                                <select class="form-control show-tick" id="wst-pais" data-live-search="true" data-width="100%" data-title="Seleccione pais" data-size="5"></select>
                            </div> 
                        </div>
                        <div class="col-md-3">
                            <div id="form-group">
                                <label for="telefono">Celular de Contacto</label>
                                <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-whatsapp fa-1.5x" aria-hidden="true"></i></div>
                                <input type="text" name="telefono" class="form-control"  id="wst-telefono" placeholder="Su numero celular para contacto" >
                                </div>
                            </div></div>
                        </div>
                    </div>    
                </div>
            <div class="row">
                <div class="col-md-12 frm-res">
                    <div id="form-group">
                            <label for="comentarios">Comentarios</label>
                            <textarea name="comentarios" id="wst-comentarios" class="form-control" rows="5" placeholder="comentarios"></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 frm-res">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="opcion" class="micheckbox" id="wst-pago-opcion">
                                <div id="wst-pago-forma">
                                    <a href="#" class="btn btn-primary btn-lg disabled" role="button" aria-disabled="true">Pago en aeropuerto seleccionado<img src="images/arrow.png" /></a> - <a href="#" class="btn btn-outline-secondary btn-lg disabled" role="button" aria-disabled="true">Deseo pagar en linea</a>
                                </div>
                        </label>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 frm-res">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="alert alert-danger hide" role="alert" id="wst-alerta">
                                <h4 class="alert-heading">Alerta !</h4>
                                <div id="wst-alerta-detalle"></div>
                            </div>                
                        </div>
                        <div class="col-md-3">
                            <div class="form-group btn-send">
                                <button type="button" class="btn btn-lg btn-success" data-loading-text="Procesando..." id="wst-submit-reserva">Reservar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End frst row -->
        </div><!-- End frst col -->
    </form>
</div><!-- End frst cont -->
<!--<div class="col-xs-12 col-md-12">
                        
                    