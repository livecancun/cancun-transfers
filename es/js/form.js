function revisar(elemento) {
    if (elemento.value==""){
        elemento.className='form-control';
    } else {
        elemento.className='form-control';
    }
}

function revisaremail(elemento) {
    if (elemento.value!=""){
        var dato = elemento.value;
        var expresion = /^([a-zA-Z0-9_.-])+@(([a-zA-z0-9-])+.)+([a-zA-Z0-9-]{2,4})+$/;
        if (!expresion.test(dato)) {
            elemento.className='form-control';
        } else {
        elemento.className='form-control';
        }
	}
}


function validar(form) {
  if(form.nombre.value=="") { //Si este campo está vacío
    alert('Por Favor ingrese su nombre'); // Mensaje a mostrar
    return false; //devolvemos un valor negativo
  }
  
  if(form.mail.value=="") { //Si este campo está vacío
    alert('Por Favor ingrese su E-Mail'); // Mensaje a mostrar
    return false; //devolvemos un valor negativo
  }

 
  return true; // Si esta todo bien, devolvemos Ok, positivo
}

/*
$(document).on('change', 'input', function() {
  $('#boton').attr('type="hidden"', true);
  // Does some stuff and logs the event to the console
});
*/
  