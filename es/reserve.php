<!DOCTYPE html>
<html lang="es">
<head>
  <title>Aeropuerto Cancun a Zona Hotelera | Transportacion Oficial Aeropuerto</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="cancun airport shuttle, cancun shuttle, cancun airport taxi, cancun airport transportation, cancun transportation, cancun transfers, cancun airport hotel, cancun transfers rates, shuttle cancun"/>
  <meta name="description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!"/>
  <meta property="og:url" content="http://www.cancun-transfers.com/" />
  <meta property="og:title" content="Cancun Airport shuttle">
  <meta property="og:description" content="Cancun Airport shuttle are completely private so you will not share the vehicle like a taxi with anyone else, from Cancun Airport you will go directly to your hotel, No Waiting and No Scales!" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="http://www.cancun-transfers.com./img/og-cancunshuttle.jpg">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="300">
  <meta property="og:image:height" content="300">
  <meta property="og:locale" content="en_US" />
  <meta property="og:locale:alternate" content="es_ES" />
  <link rel="shortcut icon" href="favicon.ico" type='image/x-icon'/>
  <link href="css/main-style.css" rel="stylesheet">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/bootstrap-select.min.css" rel="stylesheet">
  <link href="../css/jquery.loading.min.css" rel="stylesheet">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet"> 
  <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600" rel="stylesheet"> 
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <?php include("_structure/header-tag-res.php"); ?>
</head>
<body>
<?php include("_structure/menu-main.php"); ?>

<div class="tel-box">
  <div class="container">
            <div class="row">
                <div class="col-lg-1 col-lg-offset-6 text-right">
                    <i class="fa fa-phone fa-2x wow bounceIn"></i>
                </div>
                <div class="col-lg-5">
                    <p>USA: 1 888 811 42 55  MEXICO: 01 800 161 4957</p>
                </div>
            </div>
        </div>
</div>
<?php include("_structure/service-ribbon.php"); ?>
<br><br>
<div class="container">
   <br>
   <div class="container">
   <div class="row">
    <div class="row">
    <!--Frm-->
   <?php include("_structure/formulario-offline.php"); ?>
    <!--Frm end-->
        </div></div></div>

  <div class="container">
  <div class="row">
    <div class="col-md-4">
      <h3>
        Cancun Hoteles
      </h3>
      <img src="images/cancun-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Playa del Carmen
      </h3>
      <img src="images/playa-del-carmen-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
    <div class="col-md-4">
      <h3>
        Riviera Maya Hoteles
      </h3>
      <img src="images/riviera-maya-shuttle-locaciones.jpg">
        <a class="btn" href="#">Ver listado »</a>
      </p>
    </div>
  </div>
</div>
<?php include("_structure/footer.php"); ?>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="../js/jquery.loading.min.js"></script>
<script src="../js/underscore-min.js"></script>
<script src="../js/backbone-min.js"></script>
<script src="../js/bootstrap-select.min.js"></script>
<script src="../js/rolapiwstres.js"></script>
<script type="text/javascript">
  var wstGlobal = {
    "wst_tipo_traslado" : '<?php if (!empty($_POST['wst-tipo-traslado'])) echo $_POST['wst-tipo-traslado']; else echo '-1';?>',
    "wst_fecha" : '<?php if (!empty($_POST['wst-fecha'])) echo $_POST['wst-fecha']; else echo '00-00-0000'; ?>',
    "wst_hotel" : '<?php if (!empty($_POST['wst-hotel'])) echo $_POST['wst-hotel']; else echo '-1'?>'
  }
</script>
</body>
</html>