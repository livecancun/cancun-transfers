<?php
#====================================================================================================
# File Name : sp_inc.php 
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contains all application configuration details
#
#====================================================================================================

#====================================================================================================
# Please provide language array per pages.
#====================================================================================================
#=====================================================================#
$lang['L_Cancun_Ground_Trans']		= "Transporte Terrestre Cancun";
$lang['L_Cancun_Airport_Trans']		= "Transporte Aeropuerto Cancun";
$lang['L_Cancun_Private_Trans']		= "Transporte Privado Cancun";


$lang['hed_espanol_img']			= "english.jpg";
$lang['hed_espanol_imgALT']			= "english";

#=================== User Side Image =================================#

// ground_transportation_rates.php
$lang['site_rates_img']		= "tarifas.jpg";
$lang['site_paynow_img']	= "reserva.jpg";

$lang['site_res_img']		= "reserva.jpg";
$lang['site_booknow_img']	= "reserve_ya.gif";

$lang['L_One_Way']			 = "Viaje Sencillo";
$lang['L_Round_Trip']		 = "Viaje Redondo";

$lang['L_Paypal_Msg']		 = "Por favor Espera...  La pagina se esta redireccionando al sitio de Paypal.......";

#======== Reservation Page(User Side)=================================#

$lang['L_Applicable_Discount']			 = "Descuento Aplicable";
$lang['L_Personal_Information']			 = "Su Informacion Personal";
$lang['L_First_Name']					 = "Nombre";
$lang['L_Last_Name']					 = "Apellido";
$lang['L_Email']					     = "Email";
$lang['L_Address']					     = "Direccion";
$lang['L_Country']					     = "Pais";
$lang['L_City']					         = "Ciudad";
$lang['L_State']					     = "Estado";
$lang['L_Zip']					         = "C.P.";
$lang['L_Phone_Number']					 = "Telefono";
$lang['L_Fax']					         = "Fax";
$lang['L_Special_Request']				 = "Solicitud Especial";
$lang['L_Select_Country']				 = "Seleccionar Pais";

$lang['L_Resv_Msg']				 		 = "
<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'> Reservas Prepagadas Traslados Aeropuerto Cancun </h1>
<p align='justify' class='texto_content'><SPAN class='texto_content Estilo6'><span class='Estilo24'>Para reservaciones PREPAGADAS, por favor, complete el siguiente formulario y recibir&aacute; un n&uacute;mero de confirmaci&oacute;n y una contrase&ntilde;a del servicio contratado. Todos los campos son requeridos. Por favor, facil&iacute;tenos informaci&oacute;n detallada de su Vuelo de llegada al Aeropuerto INternacional de Cancun. </span></SPAN></p>
";

$lang['L_Emp_Msg']				 		 = "
<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'> Reservas Prepagadas Traslados Aeropuerto Cancun </h1>
<p align='justify' class='texto_content'><SPAN class='texto_content Estilo6'>
<span class='Estilo24'>
Por favor, verifique el servicio requerido y el importe total. Si est&aacute; correcto, se&ntilde;ale la casilla de la izquierda y presione le bot&oacute;n &quot;Continuar&quot;.<br>
<br>
</span></SPAN></p>";

$lang['L_Success_Msg']				 	 = "
<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'>Reservas Prepagadas Traslados Aeropuerto Cancun </h1>";

$lang['L_Airline_Employee']				 = "Tipo de Empleado";
$lang['L_Is_Airline_Employee']			 = "Soy empleado Aerol�nea / Industria Hotelera";
$lang['L_ID']					 		 = "ID";
$lang['L_Airline_Name']					 = "Nombre Aerol�nea";
$lang['L_Company_Name']					 = "Nombre Compa��a";
$lang['L_Comments']					 	 = "Comentarios";
$lang['L_Company_ID']					 = "ID Compa��a";


#============= javascript lang variable for searchresult ==========#
$lang['Select_Airline_Employee']		 = "Por favor, Seleccione Empleado Aerol�nea.";
$lang['Empty_ID']				 		 = "Por favor, Indique ID Compa��a.";
$lang['Empty_Airline_Name']				 = "Por favor, Indique Nombre Compa��a.";
$lang['Empty_First_Name']				 = "Por favor, Indique su Nombre";
$lang['Empty_Last_Name']				 = "Por favor, Indique su Apellido.";
$lang['Empty_Email']				     = "Por favor, Indique Direcci�n Email.";
$lang['Valid_Email']				     = "Por favor, Indique Direcci�n V�lida de Email.";
$lang['Empty_Address']				     = "Por favor, Indique Direcci�n.";
$lang['Empty_Country']				     = "Por favor, Indique Pa�s.";
$lang['Empty_City']				         = "Por favor, Indique Ciudad .";
$lang['Empty_State']				     = "Por favor, Indique Estado.";
$lang['Empty_Zip']				         = "Por favor, Indique C.P..";
$lang['Valid_Zip']				         = "Por favor, Indique C.P. V�lido.";
$lang['Empty_Area_Code']			  	 = "Por favor, Indique C�digo de Area"; 	
$lang['Empty_City_Code']			  	 = "Por favor, Indique C�digo Ciudad";	
$lang['Empty_Phone_No']				  	 = "Por favor, Indique Tel�fono";
$lang['Valid_PhoneNo']				     = "Por favor, Indique Tel�fono V�lido.";
$lang['Valid_Fax']                       = "Por favor, Indique N�mero Fax V�lido.";


#======== Reserve Page (reserve.php)(User Side)====================================#
$lang['L_Destinations']				= "Destino";
$lang['L_No_Person']				= "Numero de Personas";
$lang['L_Adults']					= "Adultos ";
$lang['L_Childs']					= "Ni�os ";
$lang['L_Hotel']					= "Ingrese su Hotel";
$lang['L_To']						= "A";

$lang['Empty_No_Adult_Msg']			= "Por favor, Indique No de Personas Adultas.";
$lang['Empty_No_Person']			= "Por favor, Indique No de Personas.";
$lang['Valid_No_Person']			= "Por favor, Indique No v�lido de Personas.";
$lang['Empty_Hotel_Name']			= "Por favor, Indique Nombre de su Hotel";
#===========Site Transfer Variables (transfer.php)(userSide)=============#
$lang['L_Transfer_Service']          = "Servicio Traslado";
$lang['L_Book_Transfer']             = "Reserve Traslado";
$lang['L_Date']						 = "Fecha y hora de llegada";
$lang['L_Time']						 = "Fecha y hora a la que pasaremos por usted";
$lang['L_Airline']					 = "Aerolinea y numero de vuelo";
$lang['L_Flight']					 = "Hora que pasaremos por usted ";
$lang['L_Airport_To_Hotel']			 = "Necesito transporte Aeropuerto a Hotel";
$lang['L_Hotel_To_Airport']			 = "Necesito transporte Hotel a Aeropuerto";
$lang['L_Airport_To_Hotel_Msg']		 = "Por favor, facil�tenos los datos del <b>vuelo de llegada a su destino</b>.";
$lang['L_Hotel_To_Airport_Msg']		 = "Por favor, facil�tenos los datos del <b>vuelo de regreso a su destino</b>.";
#============= javascript lang variable for transfer (User Side)==========#
$lang['Empty_Check_Box']				= "Por favor, seleccione al menos una opcion";
$lang['Valid_Arrival_Date']				= "Por favor, Indique Fecha Vlida de Llegada";
$lang['Empty_Airline_Name']				= "Por favor, Indique Nombre Aerol nea";
$lang['Empty_Flight_No']				= "Por favor, Indique No. de Vuelo";
$lang['Valid_Departure_Date']           = "Por favor, Indique Fecha Vlida de Regreso";
$lang['Valid_BothDate_Msg']				= "La Fecha de Regreso debe ser mayor a la Fecha de Llegada";				
#===========================================================================#

#===========Site Transfer Variables (transferreservation.php)(admin Side)=============#
$lang['L_User_Details']				= "Detalles de Usuario";
$lang['Msg_Transfer_Norecord']		= "No se encontr� ninguna Reservaci�n de Traslado";
$lang['transferdetail']				= "Detalles Traslado";
$lang['arrivaldate']				= "Fecha de Llegada";
$lang['departuredate']				= "Fecha de Regreso";
$lang['arrivaltime']				= "Hora de Llegada";
$lang['departuretime']				= "Hora de Regreso";
$lang['arrivalflight']				= "Vuelo de Llegada";
$lang['departureflight']			= "Vuelo de Regreso";
$lang['arrivalairline']				= "Aerol�nea de Llegada";
$lang['departureairline']			= "Aerol�nea de Regreso";
$lang['transfer_reservation']		= "Reservaci�n Traslado";
$lang['L_Pickup_Time']				= "Hora de Recogida";
$lang['Arrival']					= "Llegada";
$lang['Departure']					= "Salida";
#============= javascript lang variable for transfer (admin Side)==========#

#======== Index Page (index.php)(User Side)====================================#
$lang['JS_Empty_Title_Name'] 	= "Sin Nombre";
$lang['Msg_Service_Added']		= "Servicio Hotel Agregado exitosamente";
$lang['Msg_Service_Updated']	= "Servicio Hotel Actualizado exitosamente";
$lang['Msg_Service_Deleted']	= "Servicio Hotel Eliminado exitosamente";

$lang['L_Reserve']			   	= "Reservar";


#=============== Site Config (siteconfig.php)(Admin Panel)=================================#
$lang['L_Site_Config'] 		= "Config Del Sitio";
$lang['L_Site_ConfigDesc'] 	= "Cambie su t�tulo de la compa��aa, t�tulo del navegador, t�tulo del meta, & de las palabras claves;  descripci�n, texto del copyright y tecleo <b>Update</b> para guardar los cambios.  Click <b>Cancelar</b> para desechar los cambios .";
								
$lang['L_Comp_Info'] 		= "Informaci�n de la Compa��a";
$lang['L_Max100_Chars'] 	= "M�ximo de Caracteres 100";
$lang['L_Max300_Chars'] 	= "M�ximo de Caracteres 300";
$lang['L_Max400_Chars'] 	= "M�ximo de Caracteres 400";
$lang['L_Max1000_Chars'] 	= "M�ximo de Caracteres 1000";

$lang['L_Site_Title'] 		= "T�tulo del Sitio";
$lang['L_Copyright_Text'] 	= "Texto del Copyright";
$lang['L_Support_Email'] 	= "Email de Ayuda";
$lang['L_Support_EmailDesc']= "Fije la direcci�n de email de Ayuda para la solicitud recibida del contacto.";
$lang['L_Paypal_Email'] 	= "Email De Paypal";
$lang['L_Paypal_EmailDesc'] = "Fije la direcci�n de email para pago con Paypal.";
$lang['L_Meta_Info'] 		= "Informaci�n Del Meta";

$lang['L_Meta_Title'] 		= "T�tulo Del Meta";
$lang['L_Meta_TitleDesc']	= "<b>Site Title</b> - probablemente la cualidad m�s importante considerada por los motores de b�squeda.  En muchos casos, usted desear� utilizar el t�rmino que describe sus servicios as� como su localizaci�n geogr�fica. ";

$lang['L_Meta_Description'] = "Descripci�n Del Meta";
$lang['L_Meta_DescriptionDesc'] = "<b>Meta Description</b> - este texto aparece en el listado para su sitio en los resultados del motor de b�squeda (e.g., una breve descripci�n de los servicios y de los productos que usted ofrece, as� como su localizaci�n).  Comience esta descripci�n con su t�tulo del servicio, y despu�s incluya otras palabras claves y/o frases que describen sus servicios. ";

$lang['L_Meta_Keyword'] 	= "Palabra clave Del Meta";
$lang['L_Meta_KeywordDesc'] = "<b>Meta Keywords</b> - una lista de t�rminos y de motores de b�squeda de las frases utiliza encontrar y alinear su sitio.  Sus palabras claves deben describir exactamente sus servicios, caracter�sticas, productos, y localizaci�n. ";

$lang['Site_Config_Updated'] = "La configuraci�n del sitio ha sido actualizada con �xito!!! ";

#==========================================================================================#



# Site Restaurant Variables (transfer.php)(Admin Panel)====================================#
$lang['Heading_Transfer_Info']	 		= "Mantener Informaci�n Traslado";
$lang['Msg_Trans_Added']              	= "Informaci�n Traslado Agregada";
$lang['Msg_Trans_Updated']              = "Informaci�n Traslado Actualizada";
$lang['Msg_Trans_Deleted']              = "Informaci�n Traslado Eliminada";
$lang['Msg_Trans_Norecord']			  	= "No se encontr� ning�n Traslado";
$lang['L_transferOrigin']               = "Origen";
$lang['L_transferDestination']		    = "Destino";
$lang['L_singleTrip']				    = "Tarifa Viaje Sencillo";
$lang['L_roundTrip']				    = "Tarifa Viaje Redondo";
$lang['L_adultRate']					= "Tarifa Adulto";
$lang['L_kidRate']						= "Tarifa Ni�o";
$lang['L_TransferInfo']				  	= "Informacin Traslado";
$lang['transferRequestNo']		  	  	= "Solicitud Traslado No.";
$lang['transferStatus']				  	= "Estatus Traslado";
$lang['Transfer_Cart_Details']		 	= "Detalles del Traslado en Carrito";

$lang['L_Origin']						= "Origen";

#===========Site Transfer Variables (transferreservation.php)(User Side)=============#
$lang['L_transferType']			   = "Tipo de Traslado";
$lang['L_singleTrip']			   = "Viaje Sencillo";
$lang['L_roundTrip']			   = "Viaje Redondo";	
$lang['L_Transfer_Rates']		   = "Tarifas Traslados";

#======= Transfercart.php ===========#
$lang['L_Total_Amount']					= "Importe Total";
$lang['L_Total']						= "Total";
$lang['L_CartDetail']		 			= "Detalle Carrito de Compra";
$lang['L_Transfer_Reservation_Details']	= "Detalle Reserva Traslado";
$lang['Valid_Transfer_Select']			= "Por favor, Seleccionar el Traslado que Solicita";
$lang['Valid_Select_Transfer']			= "Por favor, Seleccionar el Traslado";		
$lang['L_Checkout']						= "Continuar";
$lang['L_ReserveMore']					= "Reservar Mas";



#==========================Success.php====================#
$lang['L_Reservation_Confirmation']	= "Confirmaci�n Reservaci�n";
$lang['Confirmation_Success_Msg1']	= "Su Solicitud de Reservaci�n ha sido enviada correctamente.<br><br>Su N�mero de Confirmaci�n es ";
$lang['Confirmation_Success_Msg2']	= "<br><br>Su confirmaci�n es temporal y recibir� un e-mail con el n�mero confirmaci�n FINAL e instrucciones para finalizar el proceso de pago.<br>";
$lang['Thanks_Msg']					= "GRACIAS POR SU PREFERENCIA";
$lang['L_Subject']					= "Asunto";
$lang['L_Message']					= "Mensaje";	
$lang['adminMessage'] 				= "Tambi�n puede indicar su mensaje en el recuadro de texto. Por favor, No modifique la URL de Pago Do not Change Payment Url.";
$lang['Reservation_Error']			= "La pagina que est� visitando no tiene acceso autorizado sin su No. de Confirmaci�n.";
$lang['Approval_Error']				= "Puede que su No. de Confirmaci�n sea err�neo o que el Administrador haya rechazado su solicitud de Reservaci�n.<br><br>Por favor, contacte al Administrador del Sistema a la siguiente direcci�n electr�nica: ";

$lang['L_Res_Info']					= "
<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo23'>Reservas Prepagadas Traslados Aeropuerto Cancun </h1>
<p align='justify' class='texto_content'><SPAN class='texto_content Estilo6'>
<span class='Estilo24'>Por favor, verifique el servicio requerido y el importe total. Si est&aacute; correcto, se&ntilde;ale la casilla de la izquierda y presione le bot&oacute;n 'Continuar'.<BR>
<BR></span></SPAN></p>
";
			 	
#==========================End here====================#

#==========================Login Registration and Forgot password variable====================
$lang['L_Register_Here']	= "Registrarse Aqu�";
$lang['L_Forgot_Password']  = "�Olvid� su contrase�a?";
$lang['L_Update_Profile']	= "Actualizar Perfil";

#========================== additional services ====================
$lang['L_Back_to_Home']	= "Regresar a Inicio";

$lang['msg_Name'] 		= "Por favor, Indique su Nombre.";
$lang['msg_Company']	= "Por favor, agregar nombre de compa��a. ";
$lang['msg_Address'] 	= "Por favor, Indique su Direcci�n.";
$lang['msg_City'] 		= "Por favor, Indique su Ciudad.";
$lang['msg_State'] 		= "Por favor, Indique su estado.";
$lang['msg_Zip'] 		= "Por favor, Indique su C.P..";
$lang['msg_Country'] 	= "Por favor, Indique su Pa�s.";
$lang['msg_Telephone'] 	= "Por favor, Indique su Tel�fono.";
$lang['msg_Title'] 		= "Por favor, Indique T�tulo";
$lang['msg_Category'] 	= "Por favor, Indique Categor�a.";
$lang['msg_Description']= "Por favor, Indique Descripci�n.";
$lang['msg_Url'] 		= "Por favor, Indique URL.";
$lang['msg_Our_Url'] 	= "Por favor, Indique el URL de Nuestro link en su sitio.";
$lang['msg_Email'] 		= "Por favor, Indique su Email.";
$lang['msg_validEmail'] = "Por favor, email address v�lido. ";

#==========================Login Registration and Forgot password variable====================
$lang['L_Register_Here']	    = "Registrar Aqu�";
$lang['L_Forgot_Password']      = "�Olvid� su contrase�a?";
$lang['L_Update_Profile']	    = "Actualizar Perfil";
$lang['L_Cart_Details']		    = "Detalles Carrito";
$lang['Msg_Profile_Added']		= "Su Perfil ha sido Agregado";
$lang['Msg_Already_Exists']		= "Este nombre de usuario ya existe, por favor, seleccione uno diferente";
$lang['Msg_No_User']			= "No existe tal informaci�n. Por favor, verifique sus detalles.";
$lang['Msg_Mail_Sent']			= "Su Contrase�a se envi� a su Correo Electr�nico";
$lang['L_Your_Password']		= "Su Contrase�a es: ";
$lang['Empty_Login_Id']			= "Por favor, indique Nombre de Usuario";
$lang['L_User_Name']			= "Nombre de Usuario";
$lang['L_Registration_Detail']	= "Detalles de Registro";
$lang['Msg_Profile_Updated']	= "Su Perfil ha sido Actualizado";
$lang['Login_Here']				= "Login Aqu�";	
$lang['Login_Msg']				= "
<h1 align='justify' class='style6 Estilo1 Estilo8'>Login </h1>
Si necesita verificar el estatus de su reservaci&oacute;n o realizar cambios, por favor, accese con los datos de N�mero de Confirmaci�n y Contrase�a.";	

# Error Messages ====================================================================================
$error['incorrectZip']            	= "C.P. Incorrecto.Por favor, indique C.P. v�lido.";
$error['incorrectValue']			= "No. Confirmaci�n / Contrase�a incorrecta";
$error['incorrectUserValue']		= "Nombre Usuario / Contrase�a incorrecta";
$error['emptyZip']        	    	= "Por favor, indique C.P. v�lido.";
$error['usernameExist']             = "El nombre de usuario que selecci�n ya est� en uso. Por favor, indique uno diferente.";
$error['emailExist']				= "El Email que seleccion� ya est� en uso. Por favor, seleccione otro direcci�n de Email.";
# Java Script Common Confirm Messages ======================================================================
$lang['msgConfirmDelete']  			= "�Est� seguro que desear Eliminar?";
$lang['msgEmptyUsername']			= "Por favor, Indique nombre de usuario.";
$lang['msgEmptyConfirmationNo']		= "Por favor, Indique No. de Confirmaci�n";
$lang['msgEmptyUserPassword']		= "Por favor, Indique Contrase�a.";
$lang['no_records']					= "No se encontr� ning�n elemento ";
$lang['msgConfirmDeleteAll']  		= "Todo el Sub-contenido de esto ser� eliminado. �Desea Continuar?";
# Tour and Hotel Note Msg Variables ======================================================================#
$lang['L_Note']						= "Nota";
$lang['Note_NR']					= "Ninguna Solicitud";
$lang['Note_PR']	   				= "Solicitud Pendiente";
$lang['Note_AR']	    			= "Solicitud Aprobada";  
$lang['Note_NA']	    			= "No Aprobada";      
$lang['Note_RP']	     			= "Solicitud de Pago";   
$lang['Note_PD']	     			= "Pago Realizado";      

# ======================================================================================================#
#			 				Button Variables															#
# ======================================================================================================#
$lang['add']						= "Agregar";
$lang['edit']						= "Editar";
$lang['Login'] 						= "Login";
$lang['Reset'] 						= "Reset";
$lang['cancel']						= "Cancelar";
$lang['update']						= "Actualizar";
$lang['save']						= "Guardar";
$lang['submit']						= "Enviar";
$lang['Change']						= "Cambiar";
$lang['Book']						= "Reservar";
$lang['Check']						= "Verificar";
$lang['Back']						= "Atr�s";
$lang['Send']						= "Enviar";
$lang['L_Send']						= "Enviar";

# ======================================================================================================#
#			 		Site Common Lang Variables															#
# ======================================================================================================#
$lang['User_Name']			    		= "Nombre de Usuario";
$lang['Password']               		= "Contrase�a";
$lang['Modify']							= "Modificar";
$lang['Delete']							= "Borrar";
$lang['View']							= "Ver Detalles";
$lang['Remove']							= "Eliminar";
$lang['RemoveAll']						= "Elimina Todo";
$lang['Action']							= "Acci�n";
$lang['SrNo']							= "Sr No #";
$lang['Name']							= "Nombre";
$lang['HeadingControlPanel']			= "Panel de Control";
$lang['Help']							= "Ayuda";
$lang['Empty_File_Msg']                 = "Por favor, suba el archivo de imagen";
$lang['Valid_File_Msg']                 = "Por favor, suba solo archivo de imagen. [ jpg,jpeg or gif ]";
$lang['Valid_File_Msg1']                = "Por favor, suba solo archivo de imagen.";
$lang['select']                         = "Por favor, Seleccione Destino ";
$lang['L_Yes']							= "Si";
$lang['L_No']							= "No";

# Site User Details Variables (user.php)(Admin Panel)====================================#
$lang['L_Reservation_Details_of_User']	= "Detalles Reservaci�n del Usuario";
$lang['L_Reservation_Details']			= "Detalles Reservaci�n";
$lang['name']							= "Nombre";
$lang['Tour_Cart_Details']				= "Detalles Carrito Tour";
$lang['Hotel_Cart_Details']				= "Detalles Carrito Hotel";
$lang['firstname']						= "Nombre";
$lang['lastname']						= "Apellido";
$lang['address']						= "Direccion";
$lang['country']						= "Pais";
$lang['city']							= "Ciudad";
$lang['state']							= "Estado";
$lang['zip']							= "C.P.";
$lang['fax']							= "Fax";
$lang['email']							= "Email";
$lang['phoneno']						= "Telefono";
$lang['personaldetail']					= "Detalles Personales";
$lang['Msg_User_Norecord']				= "No hay Detalles usuario";
$lang['Msg_User_Deleted']               = "Informaci�n Usuario Eliminada";
$lang['Msg_User_NoDeleted']             = "Informaci�n Usuario No puede ser Eliminada";
$lang['L_ConfirmationNo']				= "N�mero de Confirmaci�n";
# Site Cart Variables (cart.php)(User Side)( UPDATED 19TH December,2003 )====================================#
$lang['L_Additional_Discounts']			= "Descuentos Adicionales";
$lang['L_View_Cart']					= "Ver Carrito";
$lang['L_View_Request']					= "Ver Solicitud";
$lang['L_cartItem']						= "Art�culo Carrito";
$lang['L_ItemType']						= "Tipo de Artculo";
$lang['L_status']						= "Estatus";
$lang['RequestNo']						= "No. de Solicitud";
#======================Back & Rates Images =========#
$lang['L_Rates_Img']					= "butt_rates.jpg";
$lang['L_Back_Img']						= "back.gif";
$lang['L_Destination_Heading_Img']      = "menuheading.jpg";
#=============================================================================================#
#============= javascript lang variable for Cart Variables (User Side) ( UPDATED 19TH December,2003 )==========#
$lang['L_Confirm_Delete']			  	 = "Est� seguro de querer eliminar todos? los datos? ";	
$lang['Valid_Select_Cart']				= "Por favor, Seleccione el Art�culo";
$lang['Valid_Delete_Cart']				= "Por favor, Seleccione el art�culo que desea eliminar";
# Site Login Page Message (login.php)(User Side)( UPDATED 19TH December,2003 )====================================#

$lang['Reserervation_Note_Msg']         = "Your 'Visitors Cart' contents will be merged with your <br>'Members Cart' contents once 
									      you have logged on.<br><br>Creating a login profile with Cancun-Transfers.com allows you to 
										  Reserve faster, track the status of your current Reservation and review your previous
										  Reservation.";

$lang['L_Requested_Service']			= "Servicio Requerido";
$lang['L_Rate']							= "Tarifa";

$lang['L_From'] 						= "Desde";
$lang['L_To'] 							= "A";


$lang['Select_Payment_Method']   ="Por favor, seleccione forma de pago.";
$lang['Empty_Payment_Method']	 ="Por favor, seleccione, al menos, un sistema de pago.";
$lang['L_Payment_Note']			 ="Nota: Por favor, verifique la forma de pago que desea seleccionar.";
$lang['Valid_Field_Size']		= "No puede insertar ms de 250 caracteres";
$lang['L_exchangeRate'] 	    = "Tipo de Cambio";
$lang['L_Example']				= "Ejemplo";
$lang['Empty_Exchange_Rate']	= "Por favor, Indique Tipo de Cambio";
$lang['Valid_Exchange_Rate']	= "Indique Tipo de Cambio V lido";
$lang['Msg_ExchangeRate_Change']= "Tipo de Cambio Modificado exitosamente";
$lang['msgExample']				= "Si 100 US $ = 110 MN entonces el tipo de cambio es 1.00 US $ = 1.10 MN";
$lang['msgCurrencyConversion']  = "El pago final de su reservacin ser  realizado en dlares EEUU. La conversion de divisa ser efectuada al tipo de cambio de";

//************************************** Discount.php ********************************************//
$lang['L_Manage_Discount']			= "Manejar Descuento";
$lang['L_Discount_Manager']			= "Descuentos";
$lang['L_Manage_Travel_Discount']	= "Manejar Descuentos de Renta de Viaje";
$lang['L_Add_Travel_Discount']		= "Agregar Descuento Viaje";

$lang['L_Travel_Discount']		= "Descuento Viaje";
$lang['L_Travel_Discount_Rate']	= "Tarifa Descuento Viaje";

$lang['L_Discount']				= "Descuento";
$lang['L_Final']				= "Final";
$lang['L_Discount_For']			= "Descuento por";
$lang['L_Discount_Title']		= "T�tulo Descuento";
$lang['L_Discount_Lbound']		= "Descuento Lbound";
$lang['L_Discount_Ubound']		= "Descuento Ubound";
$lang['L_Discount_Rate']		= "Tarifa Descuento";

$lang['Empty_Discount_For']		= "Por favor, Indique Descuento por.";
$lang['Empty_Discount_Title']	= "Por favor, Indique T�tulo";
$lang['Empty_Discount_Lbound']	= "Por favor, Indique Lbound.";
$lang['Empty_Discount_Ubound']	= "Por favor, Indique Ubound.";
$lang['Empty_Discount_Rate']	= "Por favor, Indique Tarifa.";
$lang['Empty_Discount_Title']	= "Por favor, Indique T tulo.";
$lang['Valid_Discount_Lbound']	= "Por favor, Indique Lbound v�lido.";
$lang['Valid_Discount_Ubound']	= "Por favor, Indique Ubound v�lido.";
$lang['Valid_Discount_Rate']	= "Por favor, Indique Tarifa v�lida.";

$lang['Msg_Confirm_Delete_Discount']= "Est� seguro que desea eliminar la tarifa de Descuento?.";

$lang['Msg_Discount_Added']		= "Tarifa Descuento Agregada exitosamente";
$lang['Msg_Discount_Updated']	= "Tarifa Descuento Actualizada exitosamente";
$lang['Msg_Discount_Deleted']	= "Tarifa Descuento Eliminada exitosamente";
//===================================================================================================//


#================== ADMIN SIDE Language Variables ========================#

/******************** Destination.php *************************/
$lang['L_Destination_Manager']		= "Configurador Destinos";
$lang['L_Manage_Destinations']		= "Maneje los Destinos.";
$lang['L_Destination_Title']		= "T�tulo del Destino";
$lang['L_Visible']					= "Visible";
$lang['L_Action']					= "Acci�n";
$lang['L_Design_Content']			= "Dise�e su contenido.  Cuando usted acaba tecleo ahorre para ahorrar los cambios.  Click la cancelaci�n para desechar los cambios.";
$lang['L_Mandatory_Fields']			= "Los campos marcados con (<font class='mandatoryMark'>*</font>) son obligatorios.";
$lang['L_Destination_Info']			= "Informaci�n del destino";

/******************** TripType.php *************************/
$lang['L_TripType_Manager']			= "Encargado Del Tipo Del Viaje";
$lang['L_Manage_TripType']			= "Maneje El Tipo Del Viaje.";
$lang['L_TripType']					= "Tipo Del Viaje";
$lang['L_TripType_Info']			= "Informaci�n Del Tipo Del Viaje";
$lang['L_ShowAll']					= "ShowAll";

/******************** TripRate.php *************************/
$lang['L_TripRate']					= "Tarifa Del Viaje";
$lang['L_TripRate_Manager']			= "Encargado De la Tarifa Del Viaje";
$lang['L_Manage_TripRate']			= "Maneje el  ndice del viaje de destinaciones.";
$lang['L_TripRate_Info']			= "Informacin De la Tarifa Del Viaje";

$lang['L_Destinations']				= "Destinos";
$lang['L_Passenger_Range']			= "Gama Del Pasajero";
$lang['L_Add_New']					= "Agregue Nuevo";

$lang['L_No_Gaps']					= "*  Cerci�rese de por favor que no haya boquetes o traslapos en la gama";

#************************** Reservation.php *****************************#
$lang['L_Reservation']				= "Reservacion";
$lang['L_Manage_Reservation']		= "Maneje sus reservaciones.";
#=========================================================================#


#=========================== Link Category & Link Show ==========================================================================#
$lang['L_Link_Manager']			= "Configurador Ligas";
$lang['L_Manage_Links']			= "Configura Ligas";

$lang['L_Add_Category']			= "Agregue La Categor�a";
$lang['L_Link_Category']		= "Categor�a Ligas";
$lang['L_Link Directory']		= "Directorio Ligas";
$lang['L_Link_AddLink']			= "Agregar Ligas";

$lang['L_Title']				= "T�tulo";
$lang['L_Select_Status']		= "Seleccione El Estado";
$lang['L_Category_Detail']		= "Detalle De la Categor�a";
$lang['L_New_Category_Name']	= "Nuevo Nombre De la Categor�a";
$lang['L_Category_Status']		= "Estado De la Categor�a";

$lang['L_Back_to_Categories']	= "De nuevo a Categor�a";
$lang['L_No_Record_Found']		= "No se encontr� ninguna categor�a";
$lang['L_No_Record_Found_Category'] = "No se encontr� ninguna categor�a ";

$lang['L_LinkCat_Added'] 			= "Detalles de la categor�a de la liga agregados con �xito.";
$lang['L_LinkCat_Updated'] 			= "Los detalles de la categor�a de la liga se han actuualizado con �xito.";
$lang['L_LinkCat_Deleted'] 			= "Los detalles de la categor�a de la liga se han suprimido con �xito.";
$lang['L_Link_Added']	 			= "Detalles de la liga agregados con �xito.";
$lang['L_Link_Updated'] 			= "Los detalles de la liga se han actualizado con �xito.";
$lang['L_Link_Deleted'] 			= "Los detalles de la liga se han suprimido con �xito.";

$lang['L_Link_Heading'] 			= "Ligas";
$lang['L_Link_Directory'] 			= "Directorio De Ligas";

$lang['L_Step_Message']			= "Para ligar nuestro sitio desde el tuyo, por favor, sigue estos pasos: 
	<br><br>
	<a href='http://www.Cancun-Transfers.com'><b>Traslados Terrestres Aeropuerto Cancun</b></a> <br>
	Transportaci�n privada Aeropuerto Cancun. Servicio de traslados desde/hacia el Aeropuerto de Cancun hasta su hotel en Cancun, Playa del Carmen o Riviera Maya.
	<br><br>
	Este es el c�digo html para el link arriba mencionado
	<br><br>
	&lt;a href='http://www.Cancun-Transfers.com'&gt;&lt;b&gt;Traslados Terrestres Aeropuerto Cancun&lt;/b&gt;&lt/a&gt; &lt;br&gt;
	<br>
	Transportaci�n privada Aeropuerto Cancun. Servicio de traslados desde/hacia el Aeropuerto de Cancun hasta su hotel en Cancun, Playa del Carmen o Riviera Maya.
	";
	
//$lang['L_Step_Message']		= "Transportaci�n privada Aeropuerto Cancun. Servicio de traslados desde/hacia el Aeropuerto de Cancun hasta su hotel en Cancun, Playa del Carmen o Riviera Maya.";

$lang['L_Link_Name'] 		= "Nombre";
$lang['L_Link_Address'] 	= "Direcci�n";
$lang['L_Link_City'] 		= "Ciudad";
$lang['L_Link_State'] 		= "Estado";
$lang['L_Link_Zip'] 		= "C�digo Postal";
$lang['L_Link_Country'] 	= "Pa�s";
$lang['L_Link_Telephone'] 	= "N�mero Telef�nico";
$lang['L_Link_Title'] 		= "T�tulo";
$lang['L_Category'] 		= "Categor�a";
$lang['L_Link_Description'] = "Descripci�n";
$lang['L_Link_Url'] 		= "Url";
$lang['L_Link_Our_Url'] 	= "URL  de nuestra liga en tu sitio"; 
$lang['L_Link_Email'] 		= "Email";

$lang['msg_CatName'] 	= "Por favor, incluya el nombre de la categora.";
$lang['msg_CatDel'] 	= "Est� usted seguro usted desea suprimir esta categor�a?";

$lang['msg_Name'] 		= "Favor de ingresar Nombre.";
$lang['msg_Address'] 	= "Favor de ingresar Direcci�n.";
$lang['msg_City'] 		= "Favor de ingresar Ciudad.";
$lang['msg_State'] 		= "Favor de ingresar Estado.";
$lang['msg_Zip'] 		= "C�digo Postal inv�lido, favor de ingresar C�digo Postal v�lido.";
$lang['msg_Country'] 	= "Favor de ingresar Pa�s.";
$lang['msg_Telephone'] 	= "Favor de ingresar N�mero Telef�nico.";
$lang['msg_Title'] 		= "Favor de ingresar T�tulo.";
$lang['msg_Category'] 	= "Favor de ingresar Categor�a.";
$lang['msg_Description']= "Favor de ingresar Descripci�n.";
$lang['msg_ValidSize'] 	= "Usted puede agregar un m�ximo de 300 caracteres para la descripci�n.";
$lang['msg_Url'] 		= "Favor de ingresar Url.";
$lang['msg_Our_Url'] 	= "Favor de ingresar la direcci�n de URL donde aparece la liga a nuestro sitio.";
$lang['msg_Email'] 		= "Favor de ingresar Email.";

$lang['L_Link_Add_Link']= "Agregar Liga";
$lang['L_Link_Detail'] 	= "Detalle Liga";

$lang['L_Link_Publish']	= "Todas las solicitudes ser�n revisadas antes de publicarse.";
#========================================================================================#



#==================== ground_transportation_rates.php ===========================================#
$lang['L_Rate_Text']	= "
	<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo30 Estilo38'>Tarifas Traslados Aeropuerto Cancun</h1>
	<p align='justify' class='Estilo35 Estilo36'>En esta secci&oacute;n usted puede realizar un prepago de nuestros Servicio de Transportaci&oacute;n del/al Aeropuerto de Cancun. Si usted prefiere pagar en efectivo a su llegada, por favor, complete nuestro <A 
	href='reserva.htm'><B>Formulario de Reservacion para PAGO EN EFECTIVO</B></A>. <BR>
	<BR>
	La mejor forma de garantizar su Servicio de Transporte en el Aeropuerto de Cancun hasta su hotel en Cancun, Playa del Carmen o Riviera Maya es atrav&eacute;s de nuestro sistema de pago anticipado. Nuestro sistema le enviar&aacute; de forma autom&aacute;tica a su correo electr&oacute;nico, un n&uacute;mero de confirmaci&oacute;n y una contrase&ntilde;a con el cual usted podr&aacute; verificar el estado de su reservaci&oacute;n en todo momento. <BR>
	<BR>
	(*) Los empleados de L&iacute;neas A&eacute;reas e Industria Hotelera podr&aacute;n obtener un descuento adicional dependiendo de la temporada, indicando el nombre de la compa&ntilde;&iacute;a para la cual trabajan y su n&uacute;mero de identificaci&oacute;n al final del proceso de registro de pago. La identificaci&oacute;n personal DEBERA mostrarse a cualquiera de nuestros representantes antes de subir al veh&iacute;culo, de lo contrario se le requerir&aacute; el pago de la diferencia del servicio.</p>
";

$lang['L_Rate_Text2']	= "
	<p align='justify' class='Estilo14'> <span class='Estilo37'>Todas las tarifas son en d&oacute;lares EE.UU.<BR>
	<BR>
	Si requiere transportaci&oacute;n terrestre a un destino diferente, por favor, use nuestro <A 
	href='reserva.htm'><B>formulario en l&iacute;nea </B></A><BR>
	<BR>
	Haga su reservaci&oacute;n AHORA y garantice su transporte a su llegada al Aeropuerto Internacional de Cancun. </span><br>
	<br>
	<img src='images/tarjetascredito.gif' width='304' height='23'> <br>
	</p>";


#==================== Address ===========================================#
$lang['L_ACS_Address']	=
	"<p><span class='Estilo10'><span class='Estilo15'><span class='style1'><strong>KINMONT S.A </strong>.<br>
	Av. Tecnol&oacute;gico SM 503, M 1<br> 
	Lote 13, C-4<BR>
	Residencial del Bosque<BR>
	Cancun 77500, Quintana Roo<br>
	Mexico<br> 
	Tel.: + 52 (998) 914 9040 / 41<BR>
	Rep. Aeropuerto Cel.: +(998) 214 9012<br>
    GRATIS desde Mexico 01 800 727 8112<br>
    GRATIS des EE UU + Canada 1 888 811 4255<br> 
    Fax: + 52 (998) 914 9041<br>
	email: <a href='mailto:info@cancun-transfers.com'>info@cancun-transfers.com</a></span><BR>
	</span></span></p>";


#==================== Footer ===========================================#
$lang['L_Footer_Msg']	=
	"KinMont Transporte Aeropuerto Cancun - <a href='http://www.cancun-transfers.com' target='_blank'>Cancun Transfers</a>";

#==================== Reservation Confirmation Mail ===========================================#
$lang['L_Res_ReqDetail']	= "Detalle De la Solicitud De la Reservaci�n";
$lang['L_Your_ResDetail']	= "Su Detalle De la Reservaci�n";


$lang['L_ConfMail_Detail_1']	= 
		"<table width='100%' border='0' align='center' cellpadding='0'cellspacing='0' bgcolor='ADD6F6'>
			  <tr>							  
				 <td>&nbsp;</td>
			  </tr>
			  <tr>
					<td valign='top'>
						<font size='2' face='Verdana, Arial, Helvetica,sans-serif'>Estimado/a Sr/a ";

$lang['L_ConfMail_Detail_2']	= 						
						 ",<br></font>
						<font face='Verdana, Arial, Helvetica, sans-serif' size='2'><br><br>
						   Bienvenido a Cancun-Transfers.com,<br><br>
						   Agradecemos su inter�s en nuestros servicios de transporte terrestre atrav�s de Cancun-Transfers.com.<br><br>
						   Su N�mero de Confirmaci�n Temporal es ";
						   
$lang['L_ConfMail_Detail_3']	= 												   
						   ".<br><br>
						   Puede regresar a nuestro sitio con los siguientes datos de registro y realizar modificaciones en su reservaci�n.<br><br>
						   No. Confirmaci�n : ";

$lang['L_ConfMail_Detail_4']	= 												   						   
						   "<br>
						   Contrase�a : ";
						   
$lang['L_ConfMail_Detail_5']	=
						   "<br><br>
						   Url : <a href = 'http://www.Cancun-Transfers.com/login.php?&lng=sp' target='_blank'>http://www.Cancun-Transfers.com/login.php?&lng=sp</a>
						   <br><br><br>		
						</font>
						<font face='Verdana, Arial, Helvetica, sans-serif' size='2'><br>
							Muchas Gracias,<br><br>
							Atentamente,<br>
							Cancun-Transfers Team.
						 </font>
						 <font size='2' face='Verdana, Arial, Helvetica, sans-serif'><br></font> 
					</td>
			   </tr>
			   <tr><td>&nbsp;</td></tr>
		 </table>";

#==================== Payment Link Mail ===========================================#
$lang['L_Payment_Instruction']	= "Liga para Pago Reservaci�n de Cancun-Transfers.com";

$lang['L_PaymentMail_Detail_1']	= 
			"Estimado/a Sr/a ";

$lang['L_PaymentMail_Detail_2']	= 
			"\n\rFelicidades,\n\rSu Solicitud de Reservaci�n en Cancun-Transfers.com ha sido aprobada!
			\n\rEste es su No. de Confirmaci�n: ";

$lang['L_PaymentMail_Detail_3']	= 
			"\n\rA continuaci�n le indicamos la liga para completar el proceso de Pago. Haga Click Aqu para completar el proceso de Pago.";

$lang['L_PaymentMail_Detail_4']	= 
			"\n\rMuchas Gracias,\n\rAtentamente,\n\rCancun-Transfers Team";
#============================================================================#

#==================== Testimonial ===========================================# 
$lang['Testimonials']			= "Testimonios ";
$lang['L_Testimonial_Manager']	= "Encargado de Testimonio";
$lang['L_Manage_Testimonial']	= "Manejar Testimonio ";

$lang['NewTestimonial'] 		= "New Testimonio";

$lang['Add_Testimonial']		= "Agregar Testimonio";
$lang['Edit_Testimonial']		= "Corregir Testimonio";
$lang['View_Testimonial']		= "Ver Detalles Testimonio";

$lang['L_Testimonial_Desc']		= "Manejar los comentarios del cliente. Puedes ver todos los detalles testimonial que has incorporado en el sistema. Podr�s agregar, corriges, suprimes los detalles testimonial de aqu�. ";

$lang['Company']				= "Compa��a";
$lang['Select_State']			= "Seleccionar el estado";
$lang['Toll_Free']				= "Gratis";
$lang['Website']				= "Website";
$lang['Photo']					= "Foto";
$lang['Delete_Picture']			= "Cuadro de la cancelaci�n";
$lang['Description']			= "Descripci�n";
$lang['Comment']				= "Comentarios";
$lang['Display_On_Website']		= "Exhibici�n en Web site";

$lang['msg_validZip'] 			= "Por favor, introducir el c�digo postal v�lido.";
$lang['msg_compTelephone'] 		= "Por favor, incorporar el nmero de tel�fono completo.";
$lang['msg_validTelephone'] 	= "Por favor, incorporar el nmero de tel�fono v�lido.";
$lang['msg_Fax'] 				= "Por favor, incorporar el No. completo del fax.";
$lang['msg_validFax'] 			= "Por favor, incorporar el No. v�lido del fax.";
$lang['msg_ToolFree'] 			= "Por favor, incorporar no gratis completo.";
$lang['msg_validToolFree'] 		= "Por favor, incorporar no gratis vlido.";
$lang['msg_validURL']			= "Por favor, incorporar el URL v�lido para el Web site. El URL debe comenzar con http://";
$lang['msg_validPhoto'] 		= "Por favor, incorporar la imagen v�lida con la extensi�n .jpg o .jpeg";
$lang['msg_Description']		= "Por favor, incorporar la descripci�n.";
$lang['msg_Comment'] 			= "Por favor, incorporar tu comentario.";

$lang['Delete_Testimonial'] 	= "Por favor, Testimonio selecto que deseas suprimir.";
$lang['Delete_AllTestimonials'] = "Est�s seguro que deseas suprimir este Testimonio?";

$lang['Msg_Testimonial_Added']	= "Testimonio se ha agregado con �xito.";
$lang['Msg_Testimonial_Updated']= "Testimonio se ha puesto al da con �xito.";
$lang['Msg_Testimonial_Deleted']= "Testimonio se ha suprimido con �xito.";

$lang['L_TestimonialMsg1']		= "Testimonio nuevo se ha agregado cerca ";
$lang['L_TestimonialMsg2']		= "Please go through <a href = 'http://www.Cancun-Transfers.com/siteadmin/login.php' target='_blank'>Site Admin Link</a> and give permission to testimonial to display on the site.";
#==================================================================================#

?>