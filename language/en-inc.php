<?php
#====================================================================================================
# File Name : en_inc.php 
#----------------------------------------------------------------------------------------------------
# Purpose 	: This file contains all application configuration details
#
#====================================================================================================

#====================================================================================================
// Please provide language array per pages.
#====================================================================================================
#=====================================================================#
$lang['hed_espanol_img']			= "espanol.jpg";
$lang['hed_espanol_imgALT']			= "versi�n espa�ol";

#=================== User Side Image =================================#
//---- ground_transportation_rates.php -----//
$lang['site_rates_img']		= "rates.jpg";
$lang['site_paynow_img']	= "book_now.jpg";

$lang['site_res_img']		= "book_now.jpg";
$lang['site_booknow_img']	= "book_now.jpg";

$lang['L_One_Way']			 = "One Way";
$lang['L_Round_Trip']		 = "Round Trip";

$lang['L_Paypal_Msg']		 = "Please Wait... Page is redirecting to the Paypal Site.......";

#======== Reservation Page(User Side)=================================#
$lang['L_Applicable_Discount']			 = "Applicable Discount";
$lang['L_Personal_Information']			 = "Your Personal Information";
$lang['L_First_Name']					 = "First Name";
$lang['L_Last_Name']					 = "Last Name";
$lang['L_Email']					     = "Email";
$lang['L_Address']					     = "Address";
$lang['L_Country']					     = "Country";
$lang['L_City']					         = "City";
$lang['L_State']					     = "State";
$lang['L_Zip']					         = "Zip";
$lang['L_Phone_Number']					 = "Phone Number";
$lang['L_Fax']					         = "Fax";
$lang['L_Special_Request']				 = "Special Request";
$lang['L_Select_Country']				 = "Select Country";

$lang['L_Resv_Msg']				 		 = "
<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'>Cancun Transfers Pre-Paid Reservations</h1>
<p align='justify' class='texto_content'><SPAN class='texto_content Estilo6'><span class='Estilo24'>For PREPAID reservations, please, fill out the following form/s and you will receive a confirmation number for the service requested.All fields are required. Please provide us with detailed information about your arrival/departure flight. </span></SPAN></p>
";

$lang['L_Emp_Msg']				 		 = "
<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'> Pre-paid Reservations Cancun Airport Transfers</h1>
<p align='justify' class='texto_content'><SPAN class='texto_content Estilo6'>
<span class='Estilo24'>Please, review the service requested and total amount. If it is ok, then mark the box on the left column and proceed&nbsp; to Check Out.<br>
<br>
</span></SPAN></p>";			

$lang['L_Success_Msg']				 = "
<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'>Pre-paid Reservations Cancun Airport Transfers</h1>";


$lang['L_Airline_Employee']				 = "Type of Employee";
$lang['L_Is_Airline_Employee']			 = "I&acute;m an Airline/ Hotel Industry Employee";
$lang['L_ID']					 		 = "Company ID";
$lang['L_Airline_Name']					 = "Company Name";
$lang['L_Company_Name']					 = "Company Name";
$lang['L_Comments']					 	 = "Comments";
$lang['L_Company_ID']					 = "Company ID";

#============= javascript lang variable for searchresult ==========#
$lang['Select_Airline_Employee']		 = "Please, Select Airline Employee.";
$lang['Empty_ID']				 		 = "Please, Enter Company ID.";
$lang['Empty_Airline_Name']				 = "Please, Enter Company Name.";
$lang['Empty_First_Name']				 = "Please, Enter First Name.";
$lang['Empty_Last_Name']				 = "Please, Enter Last Name.";
$lang['Empty_Email']				     = "Please, Enter Email Address.";
$lang['Valid_Email']				     = "Please, Enter Valid Email Address.";
$lang['Empty_Address']				     = "Please, Enter Address.";
$lang['Empty_Country']				     = "Please, Select Country.";
$lang['Empty_City']				         = "Please, Enter City .";
$lang['Empty_State']				     = "Please, Enter State.";
$lang['Empty_Zip']				         = "Please, Enter Zip Code.";
$lang['Valid_Zip']				         = "Please, Enter Valid Zip Code.";
$lang['Empty_Area_Code']			  	 = "Please, Enter Area Code"; 	
$lang['Empty_City_Code']			  	 = "Please, Enter City Code";	
$lang['Empty_Phone_No']				  	 = "Please, Enter Phone No";
$lang['Valid_PhoneNo']				     = "Please, Enter Valid Phone Number.";
$lang['Valid_Fax']                       = "Please, enter Valid Fax No.";


#======== Reserve Page (reserve.php)(User Side)====================================#
$lang['L_Destinations']				= "Destination";
$lang['L_No_Person']				= "How many passengers";
$lang['L_Adults']					= "Adults ";
$lang['L_Childs']					= "Kids ";
$lang['L_Hotel']					= "Type your hotel";
$lang['L_To']						= "To";

$lang['Empty_No_Adult_Msg']			= "Please, Enter No of Adult Person.";
$lang['Empty_No_Person']			= "Please, Enter No of Persons.";
$lang['Valid_No_Person']			= "Please, Valid No of Persons.";
$lang['Empty_Hotel_Name']			= "Please, Provide Your Hotel Name";
#===========Site Transfer Variables (transfer.php)(userSide)=============#
$lang['L_Transfer_Service']          = "Transfer Service";
$lang['L_Book_Transfer']             = "Book Transfer";
$lang['L_Date']						 = "Arrival Date & time";
$lang['L_Time']						 = "Pickup Date & time";
$lang['L_Airline']					 = "Airline & Flight Number";
$lang['L_Flight']					 = "Pickup time";
$lang['L_Airport_To_Hotel']			 = "I need Transportaion from Airport to Hotel";
$lang['L_Hotel_To_Airport']			 = "I need Transportaion from Hotel to Airport";
$lang['L_Airport_To_Hotel_Msg']		 = "Please, provide us with the data of the <b>flight you will arrive to your vacation destination</b>.";
$lang['L_Hotel_To_Airport_Msg']		 = "Please, provide us with the data of the <b>flight you will leave your vacation destination</b>.";
#============= javascript lang variable for transfer (User Side)==========#
$lang['Empty_Check_Box']				= "Please, check atleast on check box";
$lang['Valid_Arrival_Date']				= "Please, Enter Valid Arrival Date";
$lang['Empty_Airline_Name']				= "Please, Enter Airline Name";
$lang['Empty_Flight_No']				= "Please, Enter Flight No";
$lang['Valid_Departure_Date']           = "Please, Enter Valid Departure Date";
$lang['Valid_BothDate_Msg']				= "Departure Date must be greater than Arrival Date";				
#===========================================================================#

#===========Site Transfer Variables (transferreservation.php)(admin Side)=============#
$lang['L_User_Details']				= "User Details";
$lang['Msg_Transfer_Norecord']		= "No Transfer Reservation Found";
$lang['transferdetail']				= "Transfer Details";
$lang['arrivaldate']				= "Arrival Date";
$lang['departuredate']				= "Departure Date";
$lang['arrivaltime']				= "Arrival Time";
$lang['departuretime']				= "Departure Time";
$lang['arrivalflight']				= "Arrival Flight";
$lang['departureflight']			= "Departure Flight";
$lang['arrivalairline']				= "Arrival Airline";
$lang['departureairline']			= "Departure Airline";
$lang['transfer_reservation']		= "Transfer Reservation";
$lang['L_Pickup_Time']				= "Pick Up Time";
$lang['Arrival']					= "Arrival";
$lang['Departure']					= "Departure";
#============= javascript lang variable for transfer (admin Side)==========#

#======== Index Page (index.php)(User Side)====================================#
$lang['JS_Empty_Title_Name'] 	= "Empty Title Name";
$lang['Msg_Service_Added']		= "Hotel Service has been Added Successfully";
$lang['Msg_Service_Updated']	= "Hotel Service has been Updated Successfully";
$lang['Msg_Service_Deleted']	= "Hotel Service has been Deleted Successfully";

$lang['L_Reserve']			   	= "Reserve";


#=============== Site Config (siteconfig.php)(Admin Panel)=================================#
$lang['L_Site_Config'] 		= "Site Config";
$lang['L_Site_ConfigDesc'] 	= "Change your company title, browser title, meta title, keywords &amp; description, copyright text and
								click <b>Update</b> to save the changes. Click <b>Cancel</b> to discard the changes.";
								
$lang['L_Comp_Info'] 		= "Company Information";
$lang['L_Max100_Chars'] 	= "Maximum 100 characters";
$lang['L_Max300_Chars'] 	= "Maximum 300 characters";
$lang['L_Max400_Chars'] 	= "Maximum 400 characters";
$lang['L_Max1000_Chars'] 	= "Maximum 1000 characters";

$lang['L_Site_Title'] 		= "Site Title";
$lang['L_Copyright_Text'] 	= "Copyright Text";
$lang['L_Support_Email'] 	= "Support Email";
$lang['L_Support_EmailDesc']= "Set the support email address for recieving contact request.";
$lang['L_Paypal_Email'] 	= "Paypal Email";
$lang['L_Paypal_EmailDesc'] = "Set the paypal email address for payment.";
$lang['L_Meta_Info'] 		= "Meta Information";

$lang['L_Meta_Title'] 		= "Meta Title";
$lang['L_Meta_TitleDesc']	= "<b>Site Title</b> - Probably the most important attribute considered by search engines. 
								In many cases, you will want to use the term describing your services as well as your geographic location.";

$lang['L_Meta_Description'] = "Meta Description";
$lang['L_Meta_DescriptionDesc'] = "<b>Meta Description</b> - This text appears in the listing for your site in search engine results 
									(e.g., a brief description of the services and products you offer, as well as your location). 
									Begin this description with your service title, and then include other keywords and/or phrases 
									describing your services.";

$lang['L_Meta_Keyword'] 	= "Meta Keyword";
$lang['L_Meta_KeywordDesc'] = "<b>Meta Keywords</b> - A list of terms and phrases search engines use to find and rank your site. 
								Your keywords should accurately describe your services, features, products, and location.";


$lang['Site_Config_Updated'] = "Site configuration has been updated successfully !!!";

#==========================================================================================#



# Site Restaurant Variables (transfer.php)(Admin Panel)====================================#
$lang['Heading_Transfer_Info']	 		= "Maintain Tranfer Information";
$lang['Msg_Trans_Added']              	= "Tranfer Information has been Added";
$lang['Msg_Trans_Updated']              = "Tranfer Information has been Updated";
$lang['Msg_Trans_Deleted']              = "Tranfer Information has been Deleted";
$lang['Msg_Trans_Norecord']			  	= "No Tranfer Record Found";
$lang['L_transferOrigin']               = "Origin ";
$lang['L_transferDestination']		    = "Destination ";
$lang['L_singleTrip']				    = "Single Trip Rate";
$lang['L_roundTrip']				    = "Round Trip Rate";
$lang['L_adultRate']					= "Adult Rate";
$lang['L_kidRate']						= "Kid Rate";
$lang['L_TransferInfo']				  	= "Transfer Information";
$lang['transferRequestNo']		  	  	= "Transfer Request No";
$lang['transferStatus']				  	= "Transfer Status";
$lang['Transfer_Cart_Details']		 	= "Transfer Cart Details";

$lang['L_Origin']						= "Origin";
#===========Site Transfer Variables (transferreservation.php)(User Side)=============#
$lang['L_transferType']			   = "Transfer Type";
$lang['L_singleTrip']			   = "Single Trip";
$lang['L_roundTrip']			   = "Round Trip";	
$lang['L_Transfer_Rates']		   = "Transfer Rates";

#======= Transfercart.php ===========#
$lang['L_Total_Amount']					= "Total Amount";
$lang['L_Total']						= "Total";
$lang['L_CartDetail']		 			= "Cart Details ";
$lang['L_Transfer_Reservation_Details']	= "Transfer Reservation Details";
$lang['Valid_Transfer_Select']			= "Please, Select the transfer for Request";
$lang['Valid_Select_Transfer']			= "Please, Select the Transfer";		
$lang['L_Checkout']						= "Checkout";
$lang['L_ReserveMore']					= "Reserve More";



#==========================Success.php====================#
$lang['L_Reservation_Confirmation']	= "Reservation Confirmation";
$lang['Confirmation_Success_Msg1']	= "Your Reservation Request Sent Successfully.<br><br>Your Confirmation Number is";
$lang['Confirmation_Success_Msg2']	= "<br><br>Your confirmation is temporary and you will receive an e-mail with the FINAL confirmation number and instructions to finalize the payment process.<br>";
$lang['Thanks_Msg']					= "THANKS FOR YOUR PREFERENCE";
$lang['L_Home_Page']				= "Back to Home Page";
$lang['L_Subject']					= "Subject";
$lang['L_Message']					= "Message";	
$lang['adminMessage'] 				= "You Can also Provide your message in Text Box.Please Do not Change Payment Url.";
$lang['Reservation_Error']			= "The Page you are visiting is not Authorized to Visit without your confirmation No.";
$lang['Approval_Error']				= "Either Your Confirmation No is Wrong or Administrator may have disapproved your Request for Reservation.<br><br>Please contact System Administrator at Following Email Address : ";

$lang['L_Res_Info']					= "
<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo23'>Cancun Airport Transfers Pre-paid Reservations</h1>
<p align='justify' class='texto_content'><SPAN class='texto_content Estilo6'>
<span class='Estilo24'>Please, review the service requested and total amount. If it is ok, then mark the box on the left column and proceed&nbsp; to Check Out.<BR>
<BR></span></SPAN></p>
";

#==========================End here====================#

#==========================Login Registration and Forgot password variable===================#
$lang['L_Register_Here']	= "Register Here";
$lang['L_Forgot_Password']  = "Forgot Your Password?";
$lang['L_Update_Profile']	= "Update Profile";

#========================== additional services ==============================================#
$lang['L_Back_to_Home']	= "Back to Home";

$lang['msg_Name'] 		= "Please, Enter Your Name.";
$lang['msg_Company']	= "Please, Enter Your Company Name.";
$lang['msg_Address'] 	= "Please, Enter Your Address.";
$lang['msg_City'] 		= "Please, Enter Your City.";
$lang['msg_State'] 		= "Please, Enter Your State.";
$lang['msg_Zip'] 		= "Please, Enter Your Zip Code.";
$lang['msg_Country'] 	= "Please, Enter Your Country.";
$lang['msg_Telephone'] 	= "Please, Enter Your Phone Number.";
$lang['msg_Title'] 		= "Please, Enter Title.";
$lang['msg_Category'] 	= "Please, Enter Category.";
$lang['msg_Description']= "Please, Enter Description.";
$lang['msg_Url'] 		= "Please, Enter Url.";
$lang['msg_Our_Url'] 	= "Please, Enter Your Our Url link in your site.";
$lang['msg_Email'] 		= "Please, Enter Your Email Address.";
$lang['msg_validEmail'] = "Please, Valid Email Address.";

#==========================Login Registration and Forgot password variable====================
$lang['L_Register_Here']	    = "Register Here";
$lang['L_Forgot_Password']      = "Forgot Your Password?";
$lang['L_Update_Profile']	    = "Update Profile";
$lang['L_Cart_Details']		    = "Cart Details";
$lang['Msg_Profile_Added']		= "Your Profile is Added";
$lang['Msg_Already_Exists']		= "This User Name Already Exists Please select different one";
$lang['Msg_No_User']			= "No Such Information Exists Please Check Your Details";
$lang['Msg_Mail_Sent']			= "Your Password has been sent @ Your Email Address";
$lang['L_Your_Password']		= "Your Password is : ";
$lang['Empty_Login_Id']			= "Please Enter User Name";
$lang['L_User_Name']			= "User Name";
$lang['L_Registration_Detail']	= "Registration Details";
$lang['Msg_Profile_Updated']	= "Your Profile is Updated";
$lang['Login_Here']				= "Login Here";	
$lang['Login_Msg']				= "
<h1 align='justify' class='style6 Estilo1 Estilo8'>Login </h1>
If you need to check the status of your resevation or make changes, please login with your confirmation number and password.";	

# Error Messages ====================================================================================
$error['incorrectZip']            	= "Incorrect Zipcode , please Enter valid Zipcode.";
$error['incorrectValue']			= "Incorrect Confirmation No / Password";
$error['incorrectUserValue']		= "Incorrect User Name / Password";
$error['emptyZip']        	    	= "Please, Enter valid Zipcode.";
$error['usernameExist']             = "The username you selected is already in use. Please enter another username.";
$error['emailExist']				= "The Email you selected is already in use. Please choose another Email Address.";
# Java Script Common Confirm Messages ======================================================================
$lang['msgConfirmDelete']  			= "Are you sure want to delete??";
$lang['msgEmptyUsername']			= "Please, Enter Username.";
$lang['msgEmptyConfirmationNo']		= "Please, Enter Confirmation No.";
$lang['msgEmptyUserPassword']		= "Please, Enter Password.";
$lang['no_records']					= "No Records Found ";
$lang['msgConfirmDeleteAll']  		= "All Sub Contents of this will be Deleted..Do you want to Continue??";
# Tour and Hotel Note Msg Variables ======================================================================#
$lang['L_Note']						= "Note";
$lang['Note_NR']					= "No Request";
$lang['Note_PR']	   				= "Pending Request";
$lang['Note_AR']	    			= "Approved Request";  
$lang['Note_NA']	    			= "Not Approved";      
$lang['Note_RP']	     			= "Request Payment";   
$lang['Note_PD']	     			= "Payment Done";      

# ======================================================================================================#
#			 				Button Variables															#
# ======================================================================================================#
$lang['add']						= "Add";
$lang['edit']						= "Edit";
$lang['Login'] 						= "Login";
$lang['Reset'] 						= "Reset";
$lang['cancel']						= "Cancel";
$lang['update']						= "Update";
$lang['save']						= "Save";
$lang['submit']						= "Submit";
$lang['Change']						= "Change";
$lang['Book']						= "Book";
$lang['Check']						= "Check";
$lang['Back']						= "Back";
$lang['Send']						= "Send";
$lang['L_Send']						= "Send";

# ======================================================================================================#
#			 		Site Common Lang Variables															#
# ======================================================================================================#
$lang['User_Name']			    		= "Username";
$lang['Password']               		= "Password";
$lang['Modify']							= "Modify";
$lang['Delete']							= "Delete";
$lang['View']							= "View Details";
$lang['Remove']							= "Remove";
$lang['RemoveAll']						= "Remove All";
$lang['Action']							= "Action";
$lang['SrNo']							= "Sr No #";
$lang['Name']							= "Name";
$lang['HeadingControlPanel']			= "Control Panel";
$lang['Help']							= "Help";
$lang['Empty_File_Msg']                 = "Please, Upload The Image File";
$lang['Valid_File_Msg']                 = "Please, Upload only Image File. [ jpg,jpeg or gif ]";
$lang['Valid_File_Msg1']                = "Please, Upload only Image File.";
$lang['select']                         = "Please, Select Destination ";
$lang['L_Yes']							= "Yes";
$lang['L_No']							= "No";

# Site User Details Variables (user.php)(Admin Panel)====================================#
$lang['L_Reservation_Details_of_User']	= "Reservation Details of User";
$lang['L_Reservation_Details']			= "Reservation Details";
$lang['name']							= "Name";
$lang['Tour_Cart_Details']				= "Tour Cart Details";
$lang['Hotel_Cart_Details']				= "Hotel Cart Details";
$lang['firstname']						= "First Name";
$lang['lastname']						= "Last Name";
$lang['address']						= "Address";
$lang['country']						= "Country";
$lang['city']							= "City";
$lang['state']							= "State";
$lang['zip']							= "Zip";
$lang['fax']							= "Fax";
$lang['email']							= "Email";
$lang['phoneno']						= "Phone Number";
$lang['personaldetail']					= "Personal Details";
$lang['Msg_User_Norecord']				= "No User Detail";
$lang['Msg_User_Deleted']               = "User Information Deleted";
$lang['Msg_User_NoDeleted']             = "User Information can not be Deleted";
$lang['L_ConfirmationNo']				= "Confirmation No";
# Site Cart Variables (cart.php)(User Side)( UPDATED 19TH December,2003 )====================================#
$lang['L_Additional_Discounts']			= "Additional Discounts";
$lang['L_View_Cart']					= "View Cart";
$lang['L_View_Request']					= "View Request";
$lang['L_cartItem']						= "Cart Item";
$lang['L_ItemType']						= "Item Type";
$lang['L_status']						= "Status";
$lang['RequestNo']						= "Request No";
#======================Back & Rates Images =========#
$lang['L_Rates_Img']					= "butt_rates.jpg";
$lang['L_Back_Img']						= "back.gif";
$lang['L_Destination_Heading_Img']      = "menuheading.jpg";
#=============================================================================================#
#============= javascript lang variable for Cart Variables (User Side) ( UPDATED 19TH December,2003 )==========#
$lang['L_Confirm_Delete']			  	 = "Are you sure for deleting all entry ?? ";	
$lang['Valid_Select_Cart']				= "Please, Select the Cart Item";
$lang['Valid_Delete_Cart']				= "Please, Select the Cart Item you want to delete";
# Site Login Page Message (login.php)(User Side)( UPDATED 19TH December,2003 )====================================#

$lang['Reserervation_Note_Msg']         = "Your 'Visitors Cart' contents will be merged with your <br>'Members Cart' contents once 
									      you have logged on.<br><br>Creating a login profile with Cancun-Transfers.com allows you to 
										  Reserever faster, track the status of your current Reservation and review your previous
										  Reservation.";


$lang['L_Requested_Service']			= "Requested Service";
$lang['L_Rate']							= "Rate";

$lang['L_From'] 						= "From";
$lang['L_To'] 							= "To";

$lang['Select_Payment_Method']   ="Please, select Payment Method.";
$lang['Empty_Payment_Method']	 ="Please, Select atleast one payment system.";
$lang['L_Payment_Note']			 ="Note : Please Check payment method you want to select.";
$lang['Valid_Field_Size']		= "You can not enter more than 250 character in ";
$lang['L_exchangeRate'] 	    = "Exchange Rate";
$lang['L_Example']				= "Example";
$lang['Empty_Exchange_Rate']	= "Please, Enter Exchange Rate";
$lang['Valid_Exchange_Rate']	= "Enter Valid Exchange Rate";
$lang['Msg_ExchangeRate_Change']= "Exchange Rate Change Successfully";
$lang['msgExample']				= "If 100 US $ = 110 MN than Exchange Rate would  be 1.00 US $ = 1.10 MN";
$lang['msgCurrencyConversion']  = "Final Payment of your Resrevation will be done in US $. Currency Conversion will be done at Rate of";

/**************** Discount.php ***************/
$lang['L_Manage_Discount']			= "Manage Discount";
$lang['L_Discount_Manager']			= "Discount Manager";
$lang['L_Manage_Travel_Discount']	= "Manage Travel Rental Discount";
$lang['L_Add_Travel_Discount']		= "Add Travel Discount";

$lang['L_Travel_Discount']		= "Travel Discount";
$lang['L_Travel_Discount_Rate']	= "Travel Discount Rate";
/*********************************/
$lang['L_Discount']				= "Discount";
$lang['L_Final']				= "Final";
$lang['L_Discount_For']			= "Discount For";
$lang['L_Discount_Title']		= "Discount Title";
$lang['L_Discount_Lbound']		= "Discount Lbound";
$lang['L_Discount_Ubound']		= "Discount Ubound";
$lang['L_Discount_Rate']		= "Discount Rate";


$lang['Empty_Discount_For']		= "Please, Enter Discount For.";
$lang['Empty_Discount_Title']	= "Please, Enter Title.";
$lang['Empty_Discount_Lbound']	= "Please, Enter Lbound.";
$lang['Empty_Discount_Ubound']	= "Please, Enter Ubound.";
$lang['Empty_Discount_Rate']	= "Please, Enter Rate.";
$lang['Empty_Discount_Title']	= "Please, Enter Title.";
$lang['Valid_Discount_Lbound']	= "Please, Enter valid Lbound.";
$lang['Valid_Discount_Ubound']	= "Please, Enter valid Ubound.";
$lang['Valid_Discount_Rate']	= "Please, Enter valid Rate.";

$lang['Msg_Confirm_Delete_Discount']= "Are you sure you want delete this discount rate.";

$lang['Msg_Discount_Added']		= "Discount Rate has been Added Successfully";
$lang['Msg_Discount_Updated']	= "Discount Rate has been Updated Successfully";
$lang['Msg_Discount_Deleted']	= "Discount Rate has been Deleted Successfully";
//===================================================================================================//



#================== ADMIN SIDE Language Variables ========================#

/******************** Destination.php *************************/
$lang['L_Destination_Manager']		= "Destination Manager";
$lang['L_Manage_Destinations']		= "Manage Destinations.";
$lang['L_Destination_Title']		= "Destination Title";
$lang['L_Visible']					= "Visible";
$lang['L_Action']					= "Action";
$lang['L_Design_Content']			= "Design your content. When you finish click Save to save the changes. Click Cancel to discard the changes.";
$lang['L_Mandatory_Fields']			= "Fields marked with (<font class='mandatoryMark'>*</font>) are mandatory.";
$lang['L_Destination_Info']			= "Destination Information";

/******************** TripType.php *************************/
$lang['L_TripType_Manager']			= "Trip Type Manager";
$lang['L_Manage_TripType']			= "Manage Trip Type.";
$lang['L_TripType']					= "Trip Type";
$lang['L_TripType_Info']			= "Trip Type Information";
$lang['L_ShowAll']					= "ShowAll";

/******************** TripRate.php *************************/
$lang['L_TripRate']					= "Trip Rate";
$lang['L_TripRate_Manager']			= "Trip Rate Manager";
$lang['L_Manage_TripRate']			= "Manage Trip Rate of Destinations.";
$lang['L_TripRate_Info']			= "Trip Rate Information";

$lang['L_Destinations']				= "Destinations";
$lang['L_Passenger_Range']			= "Passenger Range";
$lang['L_Add_New']					= "Add New";

$lang['L_No_Gaps']					= "* Please make sure there are no gaps or overlaps in the range";

#******************** Reservation.php *****************************#
$lang['L_Reservation']				= "Reservation";
$lang['L_Manage_Reservation']		= "Manage your Reservations.";
#==================================================================#



#=========================== Link Category & Link Show ==========================================================================#
$lang['L_Link_Manager']				= "Link Manager";
$lang['L_Manage_Links']				= "Manage Links";

$lang['L_Add_Category']				= "Add Category";
$lang['L_Link_Category']			= "Link Category";
$lang['L_Link Directory']			= "Link Directory";
$lang['L_Link_AddLink']				= "Add Link";

$lang['L_Title']					= "Title";
$lang['L_Select_Status']			= "Select Status";
$lang['L_Category_Detail']			= "Category Detail";
$lang['L_New_Category_Name']		= "New Category Name";
$lang['L_Category_Status']			= "Category Status";

$lang['L_Back_to_Categories']		= "Back to Categories";
$lang['L_No_Record_Found'] 			= "No Links available for the category selected";
$lang['L_No_Record_Found_Category'] = "No Category Record Found";

$lang['L_LinkCat_Added'] 			= "Link Category details has been added successfully.";
$lang['L_LinkCat_Updated'] 			= "Link Category details has been updated successfully.";
$lang['L_LinkCat_Deleted'] 			= "Link Category details has been deleted successfully.";
$lang['L_Link_Added']	 			= "Link details has been added successfully.";
$lang['L_Link_Updated'] 			= "Link details has been updated successfully.";
$lang['L_Link_Deleted'] 			= "Link details has been deleted successfully.";

$lang['L_Link_Heading'] 			= "Links";
$lang['L_Link_Directory'] 			= "Link Directory";

$lang['L_Step_Message']				= "To link our website from your site, please, follow these easy steps. 
	<br><br>
	<a href='http://www.Cancun-Transfers.com'><b>Cancun Airport Ground Transportation</b></a> <br>
	Kinmont offers private transportation from Cancun airport to your hotel in Cancun, 	Playa del Carmen or Riviera Maya.
	<br><br>
	This is the html code for the above link
	<br><br>
	&lt;a href='http://www.Cancun-Transfers.com'&gt;&lt;b&gt;Cancun Airport Ground Transportation&lt;/b&gt;&lt;/a&gt; &lt;br&gt;	<br>
	Kinmont offers private transportation from Cancun airport to your hotel in Cancun, 	Playa del Carmen or Riviera Maya.";

//$lang['L_Step_Message']				= "Kinmont offers private transportation from Cancun airport to your hotel in Cancun, Playa del Carmen or Riviera Maya.";

$lang['L_Link_Name'] 				= "Name";
$lang['L_Link_Address'] 			= "Address";
$lang['L_Link_City'] 				= "City";
$lang['L_Link_State'] 				= "State";
$lang['L_Link_Zip'] 				= "Zip";
$lang['L_Link_Country'] 			= "Country";
$lang['L_Link_Telephone'] 			= "Telephone";
$lang['L_Link_Title'] 				= "Title";
$lang['L_Category'] 				= "Category";
$lang['L_Link_Description'] 		= "Description";
$lang['L_Link_Url'] 				= "Url";
$lang['L_Link_Our_Url'] 			= "Our Url link in your site";
$lang['L_Link_Email'] 				= "Email";
//$lang['msgConfirmDelete']  			= "�Est� seguro que quiere eliminar?";

$lang['msg_CatName'] 		= "Please, enter the Category Name.";
$lang['msg_CatDel'] 		= "Are you sure you want to delete this Category?";

$lang['msg_Name'] 			= "Please, Enter Your Name.";
$lang['msg_Address'] 		= "Please, Enter Your Address.";
$lang['msg_City'] 			= "Please, Enter Your City.";
$lang['msg_State'] 			= "Please, Enter Your State.";
$lang['msg_Zip'] 			= "Please, Enter Your Zip Code.";
$lang['msg_Country'] 		= "Please, Enter Your Country.";
$lang['msg_Telephone'] 		= "Please, Enter Your Telephone Number.";
$lang['msg_Title'] 			= "Please, Enter Title.";
$lang['msg_Category'] 		= "Please, Enter Category.";
$lang['msg_Description'] 	= "Please, Enter Description.";
$lang['msg_ValidSize'] 		= "You can add maximum 300 characters for the Description.";
$lang['msg_Url'] 			= "Please, Enter Valid Url.";
$lang['msg_Our_Url'] 		= "Please, Enter Your Our Url link in your site.";
$lang['msg_Email'] 			= "Please, Enter Your Email.";

$lang['L_Link_Add_Link'] 	= "Add Link";
$lang['L_Link_Detail'] 		= "Link Detail";

$lang['L_Link_Publish']		= "All submissions will be reviewed before publishing.";
#========================================================================================#


#==================== ground_transportation_rates.php ===========================================#
$lang['L_Rate_Text']	= "
<h1 align='justify' class='style6 Estilo1 Estilo8'>Cancun Airport  Ground Transportation Rates </h1>
<p align='justify' class='Estilo14 Estilo30 Estilo33'>In this section you may or may not prepay your Cancun Airport Shuttle Service. If you prefer to pay cash at your arrival, please, feel free to use our <A 
href='reserve.htm'><B>NON PREPAYMENT Reservation Form</B>.</A> <BR>
<BR>
By using our Prepaid Cancun Airport Shuttle Service you guarantee your transportation to your hotel in Cancun, Playa del Carmen or Riviera Maya. <BR>
<BR>
(*) Airline and Hotel Indusdry employees may get and additional discount depending on season by indicating name of the company they work for and an ID number at the end of the reservation check out process. ID MUST be shown at their arrival, otherwise they will be charged for the difference on the total amount of the service. </p>
";

$lang['L_Rate_Text2']	= "
<p align='justify' class='Estilo14'>
<span class='Estilo34'>All rates are in U.S. dollars. We accept all major credit cards. <BR>
<BR>
If you require transportation to a different destination, please, use our <A 
href='reserve.htm'><B>online form.</B></A> <BR>
<BR>
Book NOW and guarantee your transportation at your arrival.<br>
</span><br>
<img src='images/creditcards.gif' width='304' height='23'><br>
</p>";





#===================== Address ===========================================#
$lang['L_ACS_Address']	=
	"<p><span class='Estilo10'><span class='Estilo15'><span class='style1'><strong>KINMONT S.A </strong>.<br>
	Av. Tecnol&oacute;gico SM 503, M 1<br> 
	Lote 13, C-4<BR>
	Residencial del Bosque<BR>
	Cancun 77500, Quintana Roo<br>
	Mexico<br> 
	Tel.: + 52 (998) 914 9040 / 41<BR>
	Fax: + 52 (998) 9149041<br>
	email: <a href='mailto:info@cancun-transfers.com'>info@cancun-transfers.com</a></span><BR>
	</span></span></p>";

$lang['L_Footer_Msg']	=
"KinMont Cancun Shuttle Service - Site Maintenance By <a href='http://www.grupocreative.com' target='_blank'>CMS</a> ";


#==================== Reservation Confirmation Mail ===========================================#
$lang['L_Res_ReqDetail']	= "Reservation Request Detail";
$lang['L_Your_ResDetail']	= "Your Reservation Detail";


$lang['L_ConfMail_Detail_1']	= 
	"<table width='100%' border='0' align='center' cellpadding='0'cellspacing='0' bgcolor='ADD6F6'>
				  <tr>							  
					 <td>&nbsp;</td>
				  </tr>
				  <tr>
						<td valign='top'>
							<font size='2' face='Verdana, Arial, Helvetica,sans-serif'>Dear ";		 

$lang['L_ConfMail_Detail_2']	= 
						",<br></font>
						<font face='Verdana, Arial, Helvetica, sans-serif' size='2'><br><br>
						   Welcome to cancun-transfers.com,<br><br>
						   Thank you for your interest in cancun-transfers.com.<br><br>
						   Your Temporary Confirmation Number is ";		 

$lang['L_ConfMail_Detail_3']	= 
						".<br><br>
						   You can get back to site with following login detail and modify your reservation.<br><br>
						   ConfirmationNo : ";		 

$lang['L_ConfMail_Detail_4']	= 
						"<br>
						   Password : ";		 

$lang['L_ConfMail_Detail_5']	= 
						"<br><br>
						   Url : <a href = 'http://www.cancun-transfers.com/login.php' target='_blank'>http://www.cancun-transfers.com/login.php</a>
						   <br><br><br>		
						</font>
						<font face='Verdana, Arial, Helvetica, sans-serif' size='2'><br>
							Thank You,<br><br>
							Regards,<br>
							Cancun-Transfers Team.
						 </font>
						 <font size='2' face='Verdana, Arial, Helvetica, sans-serif'><br></font> 
					</td>
			   </tr>
			   <tr><td>&nbsp;</td></tr>
		 </table>";		 

#==================== Payment Link Mail ===========================================# 
$lang['L_Payment_Instruction']	= "Cancun-Transfers.com Reservation Payment Link";

$lang['L_PaymentMail_Detail_1']	= 
			"Dear ";

$lang['L_PaymentMail_Detail_2']	= 
			"\n\rCongratulation,\n\r Your Request for Reservation At bookitcancuntransfers.com has been approved !
			\n\rThis is your Confirmation No : ";

$lang['L_PaymentMail_Detail_3']	= 
			"\n\rHere it is link for Completation of Payment process. Click Here to Proceed for Payment Process.";

$lang['L_PaymentMail_Detail_4']	= 
			"\n\rThank You,\n\rRegards,\n\rCancun-Transfers Team";
#============================================================================#

#==================== Testimonial ===========================================# 
$lang['Testimonials']			= "Testimonials";
$lang['L_Testimonial_Manager']	= "Testimonial Manager";
$lang['L_Manage_Testimonial']	= "Manage Testimonial";

$lang['NewTestimonial'] 		= "New Testimonial";

$lang['Add_Testimonial']		= "Add Testimonial";
$lang['Edit_Testimonial']		= "Edit Testimonial";
$lang['View_Testimonial']		= "View Testimonial";

$lang['L_Testimonial_Desc']		= "Manage client's comments. You can view all of the testimonial details you have entered into the system. You will be able to add, edit, delete testimonial details from here.";

$lang['Company']				= "Company";
$lang['Select_State']			= "Select State";
$lang['Toll_Free']				= "Toll Free";
$lang['Website']				= "Website";
$lang['Photo']					= "Photo";
$lang['Delete_Picture']			= "Delete Picture";
$lang['Description']			= "Description";
$lang['Comment']				= "Feedback";
$lang['Display_On_Website']		= "Display On Website";


$lang['msg_validZip'] 			= "Please, Enter valid Zip Code.";
$lang['msg_compTelephone'] 		= "Please, Enter complete Phone Number.";
$lang['msg_validTelephone'] 	= "Please, Enter valid Phone Number.";
$lang['msg_Fax'] 				= "Please, Enter complete Fax No.";
$lang['msg_validFax'] 			= "Please, Enter valid Fax No.";
$lang['msg_ToolFree'] 			= "Please, Enter complete Toll Free No.";
$lang['msg_validToolFree'] 		= "Please, Enter valid Toll Free No.";
$lang['msg_validURL']			= "Please, Enter valid URL for website. URL should start with http://";
$lang['msg_validPhoto'] 		= "Please, Enter valid Image with extension .jpg or .jpeg";
$lang['msg_Description']		= "Please, Enter Description.";
$lang['msg_Comment'] 			= "Please, Enter Your Feedback.";

$lang['Delete_Testimonial'] 	= "Please, Select Testimonial you want to Delete.";
$lang['Delete_AllTestimonials'] = "Are you sure you want to delete these Testimonials?";

$lang['Msg_Testimonial_Added']	= "Testimonial has been added successfully.";
$lang['Msg_Testimonial_Updated']= "Testimonial has been updated successfully.";
$lang['Msg_Testimonial_Deleted']= "Testimonial has been deleted successfully.";

$lang['L_TestimonialMsg1']		= "New Testimonial has been added by ";
$lang['L_TestimonialMsg2']		= "Please go through <a href = 'http://www.cancun-transfers.com/siteadmin/login.php' target='_blank'>Site Admin Link</a> and give permission to testimonial to display on the site.";

#==================================================================================#

?>