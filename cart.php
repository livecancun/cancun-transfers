<?php
#====================================================================================================
# File Name : cart.php 
#----------------------------------------------------------------------------------------------------
# Purpose : This file contains all application configuration details
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#====================================================================================================

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
	define("IN_SITE", true);
	define('RIGHT_PANEL', 	true);
	
	if($_POST['Action']=="Print" || $_GET['Action']=="Print")
		define("POPUP_WINDOW", true);
	
	include_once("includes/common.php");

	include_once($physical_path['DB_Access']. 'Destination.php');
	include_once($physical_path['DB_Access']. 'Discount.php');	
	include_once($physical_path['DB_Access']. 'Page.php');
	include_once($physical_path['DB_Access']. 'Reservation.php');
	include_once($physical_path['DB_Access']. 'Cart.php');	
	include_once($physical_path['DB_Access']. "class.mailer.php");
	include_once($physical_path['DB_Access']. 'UserMaster.php');
	include_once($physical_path['DB_Access']. 'User.php');	
	
#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
$Action = isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : SHOWALL);
$cart_id 	 = isset($_GET['cart_id']) ? $_GET['cart_id'] : $_POST['cart_id'];
$dest_id 	 = isset($_GET['dest_id']) ? $_GET['dest_id'] : $_POST['dest_id'];
$discount_id = isset($_GET['discount_id']) ? $_GET['discount_id'] : $_POST['discount_id'];
$index_id	 = isset($_GET['userId']) ? $_GET['userId'] : $_POST['userId'];

# Initialize object with required module
$objDisco	= new Discount();	
$objDest 	= new Destination();
$objRes 	= new Reservation();
$objCart 	= new Cart();
$objUser 	= new UserMaster();
$objAuthUser= new User();		
	
$redirectUrl = "cart.php";

if($_POST['Save']== A_SUBMIT || $_POST['Save']== "Enviar")
{
	$userId = $objUser -> InsertAirlineUser($discount_id,		$_POST['empl_id'],		$_POST['empl_airline'],		$_POST['empl_comments']);

	header("location: cart.php?&save=true&userId=$userId");
}

if($_POST['Action']==SENDREQUEST)
{
	$i = 0;
	$tourCartId = '';
	$tourRequestNo = '';
	$itemType = 0;
	while($i < count($_POST['chkCartId']))
	{
		$itemType = 0;
		$cartId = $_POST['chkCartId'][$i];
		$itemType = cartItemType($cartId);

			$sql = "SELECT * FROM ".CARTTRANSFER 	
				  . " Where ".CARTTRANSFER.".cartId = '".$cartId."'"; 
			$db ->query($sql);
			$db->next_record();
			$transferCartId	= $db->f('carttranId');

			$transferRequestNo = genTransferRequestNo($transferCartId);
			$requestNo = $transferRequestNo;
					
			$sql = " UPDATE ".CARTTRANSFER." SET "
				   . " transferRequestNo	=   '". $transferRequestNo ."', "
				   . " trStatus				=  	 '". PR ."' "
				   . " Where ".CARTTRANSFER.".carttranId = '".$transferCartId."'"; 
	
			$db ->query($sql);

		
			$sql = " UPDATE ".CART_MASTER." SET "
				   . " requestNo				=   '". $requestNo ."', "
				   . " cartStatus				=   '". PR ."' "
				   . " Where ".CART_MASTER.".cartId = '".$cartId."'"; 
			$db ->query($sql);	   
	
		
		$i++;
	}

	header("location: cart.php?&requestsent=true");
}

//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
if(!isset($_GET['start']))
	$start_record = 0;
else
	$start_record = $_GET['start'];

$Page_Size = 15;
$num_records = '';

#====================================================================================================
if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL || $_POST['Submit']==CANCEL || $_POST['Action']==BACK || $_POST['Action'] == SHOWCART)
{

	$tpl->assign(array( 'T_Body'		=>	'cart'. $config['tplEx'],
						'JavaScript'	=>	array('cart.js'),
						'A_Action'		=>	$scriptName,
						"Header"		=>  "Resv_Rates",
						));

	 $tpl->assign("Currency",$_SESSION['CURRENCY']);							

	if($update == true)
	{
		$succ_message = $lang['Msg_CartDetail_Updated'];
	}
	if($requestsent == true)
	{
		$succ_message = $lang['Msg_Request_Sent'];
	}
	if($delete == true)
	{
		$succ_message = $lang['Msg_CartDetail_Deleted'];
	}
	 
	$tpl->assign(array( "A_Cart"				=> "cart.php",
						"L_Emp_Msg"				=> $lang['L_Emp_Msg'],
						"L_Requested_Service"	=> $lang['L_Requested_Service'],
						"L_Rate"				=> $lang['L_Rate'],												
						"L_cartItem"			=> $lang['L_cartItem'],
						"L_ItemType"			=> $lang['L_ItemType'],
						"L_Cart"				=> $lang['L_Cart'],
						"Modify"   			    => $lang['Modify'],
						"Confirm_Delete"	    => $lang['msgConfirmDelete'],
						"Delete"   			    => $lang['Delete'],
						"Message"				=>  $succ_message,
					    "L_Total"   			=> $lang['L_Total'],
						"L_ReserveMore"			=> $lang['L_ReserveMore'],
						"L_Send_Reservation_Request" => $lang['L_Send_Reservation_Request'],
						"Valid_Delete_Cart"		=> $lang['Valid_Delete_Cart'],
						"Valid_Select_Cart"		=> $lang['Valid_Select_Cart'],
						"L_Additional_Discounts"=> $lang['L_Additional_Discounts'],						
						));

	$tpl->assign(array(	"SUBMIT"				 => A_SUBMIT,							
						"Send"					 => $lang['L_Send'],	
						"Submit"  				 => $lang['submit'],
					    "Cancel"    	  		 => $lang['cancel'],
						"L_Final"	 		 	 =>	$lang['L_Final'],
						"L_Discount"	 		 =>	$lang['L_Discount'],
						"L_Airline_Employee"	 =>	$lang['L_Airline_Employee'],
						"L_Is_Airline_Employee"	 =>	$lang['L_Is_Airline_Employee'],
						"L_ID"	  		 		 =>	$lang['L_ID'],
						"L_Airline_Name"		 =>	$lang['L_Airline_Name'],
						"L_Comments"		 	 =>	$lang['L_Comments'],
						"Select_Airline_Employee"=> $lang['Select_Airline_Employee'],
						"Empty_ID"		 		 => $lang['Empty_ID'],
						"Empty_Airline_Name"  	 => $lang['Empty_Airline_Name'],
						"site_res_img"  	 	 => $lang['site_res_img'],		
						"site_booknow_img"  	 => $lang['site_booknow_img'],								
						));
		
	$SortBy = "itemType";
	$tpl->assign(array( "L_Reservation_Details" => $lang['L_Reservation_Details'],
						"L_Checkout"			=> $lang['L_Checkout'],
						"L_ReserveMore"		    => $lang['L_ReserveMore'],
						"ReserveMoreLink"		=> $url_path['Site_URL']."ground_transportation_rates.php",
					));

	$sql = $objUser -> ShowUserInfo($index_id); 
	$db->next_record();

	$tpl->assign(array("is_airline_empl"	=> $db->f('is_airline_empl'),
					   "discount_id"     	=> $db->f('discount_id'),
					   "discount_rate"	    => $db->f('discount_rate'),
					   "discount_title"	    => ucfirst($db->f('discount_title')),
					   "empl_id" 	    	=> $db->f('empl_id'),
					   "empl_airline"		=> ucfirst($db->f('empl_airline')),
					   "empl_comments" 	    => $db->f('empl_comments'),
					   ));

	$tpl->assign(array(	"index_id"	=> $index_id,
					));
	
	$result				=	$objDisco->getAllDisco(" GROUP BY discount_id ORDER BY discount_title ");

	if($lng=="en")
	{	
		$airline_empl_list 	=	fillDbCombo($result,'discount_id','discount_title',$discount_id);
	}
	else
	{	
		$airline_empl_list 	=	fillDbCombo($result,'discount_id','discount_title_sp',$discount_id);
	}

	$tpl->assign(array( "Airline_Empl_List" => $airline_empl_list,
					));
	
	$tpl->assign(array( 'CartInfo'		=>	$objCart -> Show_Cart_Detail($_SESSION['User_Id'],$cartId='',2,$SortBy, $start_record, $Page_Size, $num_records),
						));

	$tpl->assign("V_grandTotal",number_format(($_SESSION['ExchangeRate']*$grandTotal),2,'.',','));
	
	if($num_records >= $Page_Size)
		$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));
}

#====================================================================================================
#	Delete Cart Record
#----------------------------------------------------------------------------------------------------
elseif($_GET['Action']==DELETE || $_POST['Action']==DELETE)
{
	for($i=0; $i < count($_POST['chkCartId']) ; $i++)
	{
	   $status = $objCart -> Delete_Cart_Detail($_POST['chkCartId'][$i]);
	}

	header("location: cart.php?&delete=true");
}

	$tpl->display('default_layout'. $config['tplEx']);

?>

<?php include_once("analyticstracking.php") ?>