<?php
#====================================================================================================
# File Name : paymentProcess.php
#----------------------------------------------------------------------------------------------------
# Purpose : This file contains all application configuration details
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#====================================================================================================
define('IN_SITE', 	true);
#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
# include the required file
include_once("includes/common.php");
include_once($physical_path['DB_Access']. 'TripRate.php');
include_once($physical_path['DB_Access']. 'TripType.php');
include_once($physical_path['DB_Access']. 'Destination.php');
include_once($physical_path['DB_Access']. 'Cart.php');
include_once($physical_path['DB_Access']. 'Reservation.php');
include_once($physical_path['DB_Access']. 'UserMaster.php');
include_once($physical_path['DB_Access']. 'User.php');
include_once($physical_path['DB_Access']. "class.mailer.php");

global $virtual_path;


	# Initialize object with required module
	$objRate	= new TripRate();
	$objType	= new TripType();
	$objDest 	= new Destination();
	$objRes 	= new Reservation();
	$objUser 	= new UserMaster();
	$objCart 	= new Cart();
	$objAuthUser= new User();		

//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
	if(!isset($_GET['start']))
		$start_record = 0;
	else
		$start_record = $_GET['start'];
	
	$Page_Size = 15;
	$num_records = '';


if((!$_GET['Action'] && !$_POST['Action']) || $_POST['Action']==SHOW_ALL || $_POST['Action']==BACK )
{

	$tpl->assign(array( 'T_Body'		=>	'paypal'. $config['tplEx'],
						"L_Paypal_Msg"  => $lang['L_Paypal_Msg'],
						));


//	if(isset($_GET['confirmationNo']))
	if($_SESSION['User_Id'])
	{

		$confirmationNo = $_GET['confirmationNo'];

		$tpl->assign(array(	"L_Reservation_Details"  => $lang['L_Reservation_Details'],
							"ACTION"				 => ADD,
							"Submit"  				 => $lang['submit'],
							"Cancel"    	  		 => $lang['cancel'],
							));
		

		if($_SESSION['CURRENCY'] == "MN")
			$tpl->assign("Msg",$lang['msgCurrencyConversion']." 1 US = ".$_SESSION['ExchangeRate']." ".$_SESSION['CURRENCY']);
		
		$tpl->assign(array( 'CartInfo'		=>	$objCart -> Show_UserCart_Detail($confirmationNumber),
							));
									
	
		#=========================================== User Info ================================================================#
	
				$sql = $objUser -> ShowUserMaster2($_SESSION['User_Id']);
				
				$rsUsercnt = $db1->num_rows();

				if($rsUsercnt == 0)
				{
					$support_email = $config[WC_SUPPORT_EMAIL];
					$tpl->assign("ErrorMessage",$lang['Approval_Error'].$support_email);
				}
				else
				{
					$k = 0;

					$DestTitle = array();
					while($k < $rsUsercnt)
					{
						$db1->next_record();
						$cart_id 	= $db1->f("cart_id");					

						$grandTotal = stripslashes($db1->f("totalCharge"));
						$CountryName= stripslashes($db1->f('country'));
	
						$auth_id = $db1->f('auth_id');
						
	
						$sql = $objUser -> ShowAuthUserMaster($auth_id);
						$db->next_record();
	
						$confirmationNumber = $db->f('confirmationNo');
	
						$areacode = split(")",stripslashes($db->f('phoneno')));
						$phone = split("-",$areacode[1]);
						$areacode1 = substr($areacode[0],1,strlen($areacode[0]));
					
						$Firstname 	= ucfirst(stripslashes($db->f('firstname')));
						$Lastname 	= ucfirst(stripslashes($db->f('lastname')));
						$Address1 	= ucfirst(stripslashes($db->f('address1')));
						$City 		= ucfirst(stripslashes($db->f('city')));																		
						$State 		= ucfirst(stripslashes($db->f('state')));
												
						$tpl->assign(array( "Firstname"  	 	 =>	 ucfirst(stripslashes($db->f('firstname'))),
											"Lastname"	    	 =>	 ucfirst(stripslashes($db->f('lastname'))),
											"Address1"       	 =>  ucfirst(stripslashes($db->f("address1"))),
											"Address2"		 	 =>  ucfirst(stripslashes($db->f("address1"))),
											"City"			 	 =>	 ucfirst(stripslashes($db->f('city'))),
											"State"				 =>	 ucfirst(stripslashes($db->f("state"))),
											"Zip"		       	 =>	$db->f("zip"),
											"Fax"				 =>	$db->f("res_fax"),
											"Email"				 =>	stripslashes($db->f("email")),
											"Phone_Areacode"     => $areacode1,
											"Phone_Citycode"     => $phone[0],
											"Phone_No" 	    	 => $phone[1],
											"Company_Title"    	 => $config[WC_COMPANY_TITLE],		
											"auth_id"    	 	 => $db->f("auth_id"),	
											"paypal_id"    	 	 => $config[WC_PAYPAL_EMAIL],	
											"L_Reservation"  	 => $lang['L_Reservation'],
											));
											
											
						$record  		= Country_List();
						$countryList	= fillDbCombo($record, "country_name", "country_name", $CountryName);
		
						$tpl->assign(array( 'CountryList'	=>	$countryList,						
										));
		
		
						#===========================Start Total Amount ==================#
						$sql = $objCart -> Show_ConfrmCart($confirmationNumber);
						
						$i = 0;
						$rscnt = $db->num_rows();
						$grandTotal = 0;
		
						while($i < $rscnt)
						{
							$db->next_record();						
		
							$grandTotal += $db->f('totalCharge');
							$i++;
						}
						#===========================End Total Amount ==================#
						
						
						#===========================Start Discount ==================#				
						$sql = $objUser -> ShowUserInfoByConfNo($confirmationNumber); 
						
						$db->next_record();
						$is_airline_empl	=	$db->f('is_airline_empl');
						$discount_rate		=	$db->f('discount_rate');
		
						if($is_airline_empl == 1)
						{
							$grandTotal = $grandTotal - (($discount_rate*$grandTotal)/100);
						}
		
						$tpl->assign("V_paymentTotal",number_format($grandTotal,2,'.',','));
						#===========================End Discount ==================#								
						
						
						#========================== Cart Destination ==================#									
						$sql = $objDest -> Show_ConfrmCartDest($cart_id); 
						$rscnt = $db->num_rows();
						$db->next_record();
						
						$departureDate 	= $db->f('departureDate');
						$dest_title 	= $db->f('dest_title');
						$total_person 	= $db->f('total_person');						

						$tpl->assign(array( "dest_title"  	 	 =>	$db->f('dest_title'),
											"total_person"		 =>	$db->f("total_person"),
											));
					
						if($departureDate == "0000-00-00")
						{
							$trip_type = $lang['L_One_Way'];
							$tpl->assign(array( "trip_type"  	=>	$lang['L_One_Way'],
												));
						
						}
						else
						{
							$trip_type = $lang['L_Round_Trip'];						
							$tpl->assign(array( "trip_type"  	=>	$lang['L_Round_Trip'],
												));
					
						}
						#===============================================================#													

						#=========================== Destination Title ============================#											
						$DestTitle[$k] = $dest_title."(".$trip_type.", ".$total_person." Person(s))";
						#==========================================================================#	

						$k++;	
					}

		//				print_r($DestTitle);
						$tpl->assign("chkdestTitle",$DestTitle);			
				 }		
  }		
  else
  {
  	$tpl->assign("Message",$lang['Reservation_Error']);
  }
  
}

	$tpl->display('default_layout'. $config['tplEx']);

?>