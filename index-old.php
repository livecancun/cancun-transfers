﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<Meta http-equiv="Content-Language" Content="en-us" />
<title>Cancun transfers - Airport private transfers</title>
<meta name="description" content="Cancun transfers so that you can visit the best places in the area, we have spent 15 years traveling and recommending the best places in Cancun and the Riviera Maya" />
<meta name="keywords" content="cancun transfers, cancun shuttle, cancun airport, cancun taxi, airport cancun transfers, cancun cab" />
<meta name="robots" content="index,follow" />
<link rel="Shortcut Icon" href="favicon.ico" />
<link href="css/train.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/presentationCycle.css" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script src="js/script.js"></script>
<script src="js/menu.js" type="text/JavaScript"></script>
<script src="js/menu.js" type="text/JavaScript">
 <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js?ver=1.3.2'></script>
    <script type='text/javascript' src='js/jquery.cycle.all.min.js'></script>
    <script type='text/javascript' src='js/presentationCycle.js'></script>
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-31294623-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body onload="MM_preloadImages('images/home-hover.gif','images/about-hover.gif','images/order-hover.gif','images/contact-hover.gif','images/affiliates-hover.gif','images/newsletter-hover.gif')">
<center>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="389"><a href="http://www.cancun-transfers.com"><img src="images/header.gif" width="389" height="112" alt="cancun transfers private services" border="0" /></a></td>
        <td width="180"><img src="images/cancun-asta-member.jpg" width="160" height="112" /></td>
        <td width="180"><img src="images/cancun-trip-advisor.jpg" width="149" height="112" alt="trip advisor reviews" /></td>
        <td><table width="250" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="4">&nbsp;</td>
            <td background="images/call-us.jpg"><table width="242" border="0" cellspacing="0" cellpadding="0" height="62">
              <tr>
                <td>&nbsp;</td>
                <td class="search-text">&nbsp;</td>
              </tr>
            </table></td>
            <td width="4">&nbsp;</td>
          </tr>
        </table></td>
        <td width="20">&nbsp;</td>
      </tr>
    </table>
<table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="97"><a href="http://www.cancun-transfers.com/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Home','','images/home-hover.gif',1)"><img src="images/home.gif" alt="Home" name="Home" width="97" height="39" border="0" id="Home" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="116"><a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=en" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('About Us','','images/about-hover.gif',1)"><img src="images/about.gif" alt="Rates" name="Rates" width="116" height="39" border="0" id="About Us" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="93"><a href="arrivals.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Order','','images/order-hover.gif',1)"><img src="images/order.gif" alt="Arrivals" name="Order" width="93" height="39" border="0" id="Order" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="130"><a href="contact-us.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Contact Us','','images/contact-hover.gif',1)"><img src="images/contact.gif" alt="Contact Us" name="Contact Us" width="130" height="39" border="0" id="Contact Us" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="117"><a href="about-us.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Affiliates','','images/affiliates-hover.gif',1)"><img src="images/affiliates.gif" alt="About US" name="Affiliates" width="117" height="39" border="0" id="Affiliates" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="131"><a href="http://www.cancun-transfers.com/es/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Newsletter','','images/newsletter-hover.gif',1)"><img src="images/newsletter.gif" alt="Español" name="Newsletter" width="131" height="39" border="0" id="Newsletter" /></a></td>
          <td background="images/button-bg.gif">&nbsp;</td>
          <td width="20"><img src="images/button-right.gif" width="20" height="39" /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <table width="1000" border="0" cellspacing="5" cellpadding="0">
        <tr>
          <td width="782" valign="top">
          <div id="presentation_container" class="pc_container">
            <div class="pc_item">
                
                <img src="images/slide1.jpg" alt="Cancun Transfers" />
            </div>
            <div class="pc_item">
              <div class="desc" style="left: 0px;">
                    <h1>¡NO MORE WAIT'S!</h1>
                    Private Cancun transfers, no wait, no troubles, get
                    first at your hotel
                </div>
                <img src="images/slide2.jpg" alt="Cancun Shuttle" />
            </div>
            <div class="pc_item">
                <div class="desc">
                    <h1>Profesional Cancun Transfers</h1>
                    KinMont offers premier door-to-door ground transportation
                    services from/to Cancun International Airport to your hotel in Cancun,
                </div>
                <img src="images/slide3.jpg" alt="slide3" />
            </div>
            
            <div class="pc_item">
            <div class="desc">
                    <h1>Enjoy Cancun - Riviera Maya</h1>
                    Family Vacations, Couple Vacations or bussines and conventions 
                    </div>
              
                <img src="images/slide5.jpg" alt="Private Transfers" />
            </div>
            
            <div class="pc_item">
              
                <img src="images/slide6.jpg" alt="Cancun Mexico" />
            </div>
        </div>
        
        <script type="text/javascript">
            presentationCycle.init();
        </script>
    
    </div></td>
        </tr>
      </table>
      <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="635" valign="top"><table width="635" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="grey-left">&nbsp;</td>
              <td valign="top" class="grey-center"><table width="621" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td><table width="621" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="heading-grey"><img src="images/arrow-top.gif" width="12" height="14" /> Welcome <span class="heading-red"><em>to Cancun-transfers.com</em></span> </td>
                    </tr>
                     <tr>
                      <td ><br />
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style ">
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a class="addthis_button_tweet"></a>
<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e7036252d567f8b"></script>
<!-- AddThis Button END -->
</td>
                    </tr>
                    <tr>
                      <td class="warea">
                      <br /><a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=en"><img src="images/bnews-cancun-transfers.jpg" border="0" /></a><br />
                      <br />
                        	
	
Welcome to <strong>Mexico, Quintana Roo</strong> is the State which the most beautiful places such as Cancun and the Riviera Maya, as well as take you from the <strong>airport to your hotel and hotel to airport</strong>, we offer service from Cancun transfers so that you can visit the best places in the area, we have spent 15 years traveling and recommending the best places in Cancun and the Riviera Maya, we are at your service, Kinmont <strong>Cancun Transfers</strong>, you want to have a beautiful vacation starting with your completely private transportation.
                      <br />

<table width="607"><td width="488"><iframe width="480" height="360" src="http://www.youtube.com/embed/6vZ4hhSRx28?rel=0" frameborder="0" allowfullscreen></iframe></td><tr>&nbsp;&nbsp;&nbsp;<td width="107"></td></tr></table><br />
<strong>Cancun &amp; Riviera Maya airport private transfers</strong>  by KinMont  offers premier <strong>door-to-door ground transportation</strong> services from/to Cancun International Airport to your hotel in Cancun, Playa del Carmen or Riviera Maya. We provide a VIP specialized shuttle or taxi service that offers you the safety and punctuality that you deserve.
<br />
Our representatives at the airport will be waiting for you holding a banner with our logo (Kinmont), and they will lead you to the unit that will drive you to your destination (please, check <a href="http://www.cancun-transfers.com/arrivals.html">Cancun Airport Arrival Section</a>). We always keep in mind that you would like to feel at home, and we will give you all the information you need to enjoy your trip since the very beginning, as our drivers are highly qualified to transport people from every part of the world.
Our <a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=en">Cancun &amp; Riviera Maya transfers rates </a> are competitive and you will save some money if your group is big. Enjoy the comfort of our private shuttles after your flight and get rid of stress trying to find a <strong>taxi in Cancun</strong>.
                        <div class="warea-more"><a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=en"><img src="images/rates-bton.png" alt="Check Rates" border="0" /></a></div></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td><img src="images/grey-line-big.gif" width="591" height="1" vspace="15" /></td>
                </tr>
                <tr>
                  <td><table width="621" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="heading-grey-bottom"><img src="images/arrow-bottom.gif" width="12" height="14" /> Why <span class="heading-red"><em>Us ?</em></span> </td>
                    </tr>
                    <tr>
                      <td class="warea">
                      <div id="testimonials">
        <ul>
			<?php
            
                $xmlFile = 'xml/testimonials.xml';
                $xslFile = 'xml/transform.xml';
                
                $doc = new DOMDocument();
                $xsl = new XSLTProcessor();
                
                $doc->load($xslFile);
                $xsl->importStyleSheet($doc);
                
                $doc->load($xmlFile);
                echo $xsl->transformToXML($doc);
            
            ?>
        </ul>
    </div>
                      
                      <br /><br />Cancun-transfers.com by KinMont, is a reliable and secure transportation company providing transportation to individuals, families and small groups. All
private taxi service, our vans have air conditioned, licensed and insured. All drivers are
bilingual and professional. Let us know your special needs...

Special Assistance for disabled passengers.
                        </td></tr>
                  </table></td>
                </tr>
              </table></td>
              <td class="grey-right">&nbsp;</td>
            </tr>
          </table></td>
          <td width="9">&nbsp;</td>
          <td valign="top"><table width="316" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td class="grey-left">&nbsp;</td>
              <td valign="top" class="grey-center"><table width="302" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td valign="top"><table width="302" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="heading-grey"><img src="images/arrow-top.gif" width="12" height="14" />Private <span class="heading-red"><em>Cancun Tranfers</em></span> </td>
                      </tr>
                      <tr>
                        <td><br /><img src="images/trav-cancun-transfers.jpg" /></td></tr>
                    </table></td>
                </tr>
                <tr>
                  <td><img src="images/grey-line-small.gif" width="275" height="1" vspace="15" /></td>
                </tr>
                <tr>
                  <td><table width="302" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="heading-grey-bottom"><img src="images/arrow-bottom.gif" width="12" height="14" /> Alliance <span class="heading-red"><em>&amp;</em></span> Partners </td>
                    </tr>
                    <tr>
                      <td><table width="302" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td ><img src="images/partners-cancun-transfers.jpg" /></td>
                        </tr>
                      </table></td>
                    </tr>
                  </table></td>
                </tr>
              </table></td>
              <td class="grey-right">&nbsp;</td>
            </tr>
          </table></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <table width="1000" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td bgcolor="#F4F4F4" class="footer-heading"><a href="http://www.cancun-transfers.com/">Home</a> | <a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=en">Rates</a> | <a href="arrivals.html">Arrivals</a> | <a href="contact-us.html">Contact Us</a> | <a href="about-us.html">About Us</a> | <a href="http://www.cancun-transfers.com/es/">Espa&ntilde;ol</a></td>
        </tr>
        <tr>
          <td bgcolor="#F4F4F4" class="footer-subheading"><p>Copyright &copy; 2011 cancun-transfers.com All Rights Reserved</p>
            <a href="http://www.cancun-transfers.com">Cancun shuttle</a> | <a href="http://www.cancun-transfers.com/">Cancun transfers </a> | <a href="http://www.airport-cancun-shuttle.com/">Airport Cancun Shuttle </a> | 
                <a href="http://www.cancun-shuttle.com/">Cancun Shuttle </a></td>
        </tr>
      </table>
 	</td>
  </tr>
</table>
</center>
</body>
</html>