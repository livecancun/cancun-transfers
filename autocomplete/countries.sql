-- --------------------------------------------------------

-- 
-- Table structure for table `countries`
-- 

CREATE TABLE `countries` (
  `id` int(6) NOT NULL auto_increment,
  `value` varchar(250) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=243 ;

-- 
-- Dumping data for table `countries`
-- 

INSERT INTO `countries` VALUES (1, 'Akumal');
INSERT INTO `countries` VALUES (2, 'Cancun Down Town');
INSERT INTO `countries` VALUES (3, 'Cancun Hotel Zone');
INSERT INTO `countries` VALUES (4, 'El dorado royale');
INSERT INTO `countries` VALUES (5, 'Excellence Playa mujeres');
INSERT INTO `countries` VALUES (6, 'Cozumel ferry');
INSERT INTO `countries` VALUES (7, 'Isla Mujeres Ferry');
INSERT INTO `countries` VALUES (8, 'Grand Palladium');
INSERT INTO `countries` VALUES (9, 'Ibero Star Paraiso');
INSERT INTO `countries` VALUES (10, 'Holbox Island');
INSERT INTO `countries` VALUES (11, 'Maroma');
INSERT INTO `countries` VALUES (12, 'Moon Palace');
INSERT INTO `countries` VALUES (13, 'Playa de Carmen');
INSERT INTO `countries` VALUES (14, 'Puerto Aventuras');
INSERT INTO `countries` VALUES (15, 'Puerto Morelos');
INSERT INTO `countries` VALUES (16, 'The Grand Mayan');
INSERT INTO `countries` VALUES (17, 'Tulum');
INSERT INTO `countries` VALUES (18, 'Tulum Hotel Zone');
