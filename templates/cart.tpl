<script>
	var Select_Airline_Employee	= '{$Select_Airline_Employee}';
	var Empty_ID				= '{$Empty_ID}';
	var Empty_Airline_Name  	= '{$Empty_Airline_Name}';
	var Valid_Select_Cart		= '{$Valid_Select_Cart}';
	var Valid_Delete_Cart  		= '{$Valid_Delete_Cart}';
	var Confirm_Delete 			= '{$Confirm_Delete}';
</script>

<br /><br />
<table>
<tr>
<td>
{if $lng == "en"}
<img src="images/step-2.jpg" /></td><td><h2>Please confirm services</h2></td>
{else}
<img src="images/step-2.jpg" /></td><td><h2>Porfavor confirme servicio(s)</h2></td>
{/if}
</tr></table>
<table border="0" cellpadding="1" cellspacing="1" width="100%">
<FORM name="frmCart" action="{$A_Cart}" method="POST">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="92%" height="100%" border="0" cellspacing="0">
					<tr>
						<td>  
						<table width="100%" border="0" cellspacing="0">
							<tr>
							  <td valign="top"><div align="justify">
								  {$L_Emp_Msg}
								  <table width="100%" border="0" align="center" cellspacing="0">
									<tr>
									  <td colspan="3"class="successMsg">{$Message}</td>
									<tr bgcolor="#007ABD">
									  <td><div align="center">
										  <input type="checkbox" name="chkAll" onclick="javascript: return checkAll(document.frmCart, this.checked);">
										</div></td>
									  <td width="60%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Requested_Service}</span></span></div></td>
									  <td width="40%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Rate}</span></span></div></td>
									</tr>
									<tr>
									  <td class="successMsg" colspan="3" align="center">{$Notfound_Message}</td>
									</tr>
									{foreach name=CartInfo from=$CartInfo item=Cart}
									{assign var='TotalPrice' value=`$TotalPrice+$Cart.totalCharge`}
									<tr bgcolor="#EBEBEB">
									  <td><div align="center"><span class="texto_content">
										  <input type='checkbox' name="chkCartId[]" id="chkCartId[]" value="{$Cart.cart_id}">
										  </span></div></td>
									  <td><div align="center" class="texto_content">{$Cart.dest_title}</div></td>
									  <td><div align="center"><span class="texto_content">$ {$Cart.totalCharge} {$Currency}</span></div></td>
									</tr>
									{/foreach}
									<tr>
									  <td><div align="center"><span class="texto_content"> </span></div></td>
									  <td><div align="right"><span class="texto_content"><strong>{$L_Total}</strong></span></div></td>
									  <td><div align="center"><span class="texto_content"><strong>$  {$TotalPrice|string_format:"%.2f"}
										 {$Currency}</strong></span></div>
										<input type="hidden" name="cartId">
										<input type="hidden" name="itemType">
									  </td>
									</tr>
									{if $is_airline_empl == 1}
									<tr>
									  <td>&nbsp;</td>
									  <td><div align="right"><span class="texto_content">
										<strong>{$L_Discount} {$discount_rate}% </strong>
										{assign var='discount' value=`$discount_rate`}
										</span></div>
									  </td>
									  <td><div align="center"><span class="texto_content"><strong>- $ {math equation="(x * y)/100" x=$TotalPrice y=$discount format="%.2f"}</strong></span></div></td>
									</tr>
									<tr>
									  <td>&nbsp;</td>
									  <td><div align="right"><span class="texto_content"><strong>{$L_Final} {$L_Total}</strong></span></div></td>
									  <td><div align="center"><span class="texto_content"><strong>$ {math equation="(x-((x * y)/100))" x=$TotalPrice y=$discount format="%.2f"}</strong></span></div></td>
									</tr>
									{/if}
								  </table>
								  <br>
								  <table width="100%" border="0" align="center" cellspacing="0">
									<tr>
									  <td>&nbsp;</td>
									  <td>&nbsp;</td>
									  <td>&nbsp;</td>
									</tr>
									
									<tr>
									  <td>&nbsp;</td>
									  <td align="center"><input type="button" Value="{$Delete}" Name="Submit" class="texto_content" onclick="javascript : return Form_Delete(frmCart); return false;">
										<input type="hidden" name="index_id" value="{$index_id}">
										<input type="hidden" name="cart_id" value="{$cart_id}">
										<input type="hidden" name="dest_id" value="{$dest_id}">
										<input type="hidden" name="Action" value="{$ACTION}">
										<!--<input type="button" value="{$L_ReserveMore}" class="texto_content" onclick="javascript: location.href('{$ReserveMoreLink}');">-->
										 <a href="{$ReserveMoreLink}" class="texto_content">Reserve more </a>   
										<input type="button" value="{$L_Checkout}" class="texto_content" onclick="javascript : return Form_Checkout(frmCart,'{$index_id}'); return false;">
									  </td>
									</tr>
								  </table>
								</div></td>
							</tr>
						  </table>
					
					</td>
					</tr>

				  </table>
			</td>
		</tr>
	</FORM>
</table>
