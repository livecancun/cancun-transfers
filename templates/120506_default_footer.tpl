{if $lng == "en"}

	<table width="100%" border="0" cellspacing="0" align="center">
		<tr>
			<td width="100%">
				<div align="center" class="Estilo15">
					<a href="index.htm">home</a>&nbsp; :&nbsp;
					<a href="ground_transportation_rates.php?&lng=en"> rates</a>&nbsp; : &nbsp; 
					<a href="cancun_airport_arrivals.htm">arrivals</a>&nbsp; : &nbsp; 
					<a href="contact.htm">contact us</a>&nbsp; :&nbsp; 
					<a href="testimonial.php">testimonial</a>&nbsp; :&nbsp; 					
					<a href="links.php?&lng=en">links</a>&nbsp; :&nbsp; 
					<a href="{$smarty.server.PHP_SELF}?&lng=sp">&nbsp; espa&ntilde;ol</a><br>
					<span class="Estilo17">
						KinMont Cancun Ground Transportation &copy; 2006 Design, SEO &amp; Hosting by <a href="http://www.pimsa.com" target="_blank">PIMSA</a>
					</span>
				</div>
			</td>
			<td><img src="{$Templates_Image}spacer.gif" width="1" height="44" border="0" alt=""></td>
		</tr>
	</table>

{else}

	<table width="100%" border="0" cellspacing="0" align="center">
		<tr>
			<td width="100%">
				<div align="center">
					<span class="Estilo15">
						<a href="index_esp.htm">inicio</a>&nbsp; :&nbsp;
						<a href="ground_transportation_rates.php?&lng=sp"> tarifas </a>&nbsp; : &nbsp; 
						<a href="aeropuerto_cancun_llegadas.htm">llegadas</a>&nbsp; : &nbsp; 
						<a href="contacto.htm">contacto</a>&nbsp; :&nbsp; 
						<a href="testimonial.php">testimonial</a>&nbsp; :&nbsp; 
						<a href="links.php?&lng=sp">links</a>&nbsp; :&nbsp; 						
						<a href="{$smarty.server.PHP_SELF}?&lng=en">english</a><br>
						<span class="Estilo17">
							KinMont Traslados Aeropuerto Cancun &copy; 2006 Dise&ntilde;o, SEO &amp; Hospedaje de <a href="http://www.pimsa.com" target="_blank">PIMSA</a>
						</span>
					</span>
				</div>			
			</td>
			<td><img src="{$Templates_Image}spacer.gif" width="1" height="44" border="0" alt=""></td>
		</tr>
	</table>

{/if}