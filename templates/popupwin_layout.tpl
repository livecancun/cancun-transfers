<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{$Site_Title}</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="title" 			content="{$Meta_Title}">
<meta name="description" 	content="{$Meta_Description}">
<meta name="keywords" 		content="{$Meta_Keyword}">
<link href="{$Templates_CSS}style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="{$Templates_JS}validate.js"></script>
<script language="javascript" src="{$Templates_JS}functions.js"></script>
<script language="javascript" src="{$Templates_JS}index.js"></script>
<script language="javascript" src="{$Templates_JS}menu.js"></script>
{section name=FileName loop=$JavaScript}
<script language="javascript" src="{$Templates_JS}{$JavaScript[FileName]}"></script>
{/section}
</head>

<body>
<br>
<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
	<tr>
		<td align="center" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="99%">
				<tr>
					<td align="center" valign="top">
						{include file="$T_Body"}
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

</body>
</html>
