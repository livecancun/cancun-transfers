<table border="0" cellpadding="0" cellspacing="1" width="595">
	<form name="frmLinkDetail"  action="{A_Action}" method="post">

	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="boldHeader">Link Detail [ {L_Action} ]</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="3" width="97%">
				<tr height="20">
					<td colspan="2" align="center" class="successMsg">&nbsp;{Message}</td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Name :</td>
					<td class="fieldLabel" ><input name="url_name" type="text" id="url_name" value="{url_name}"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Address :</td>
					<td class="fieldLabel" ><textarea name="url_address" cols="50" rows="3" id="url_address">{url_address}</textarea></td>
				</tr>	
				<tr>
					<td class="fieldLabelRight" >City :</td>
					<td class="fieldLabel" ><input name="url_city" type="text" id="url_city" value="{url_city}"  maxlength="255" ></td>
				</tr>	
				<tr>
					<td class="fieldLabelRight" >State  :</td>
					<td class="fieldLabel" ><input name="url_state" type="text" id="url_state" value="{url_state}"  maxlength="255" ></td>
				</tr>	
				<tr>
					<td class="fieldLabelRight" >Zip :</td>
					<td class="fieldLabel" ><input name="url_zip" type="text" id="url_zip" value="{url_zip}"  maxlength="5" ></td>
				</tr>	
				<tr>
					<td class="fieldLabelRight" >Country :</td>
					<td class="fieldLabel" ><input name="url_country" type="text" id="url_country" value="{url_country}"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Telephone :</td>
					<td class="fieldInputStyle">
						<input type="text" name="url_telno_areacode" size="3" maxlength="3" value="{$url_telno_areacode}" onKeyPress="javascript: isNumericKey(this.value)"> -
						<input type="text" name="url_telno_citycode" size="3" maxlength="3" value="{$url_telno_citycode}" onKeyPress="javascript: isNumericKey(this.value)"> -
						<input type="text" name="url_telno_no" size="4" maxlength="4" value="{$url_telno_no}" onKeyPress="javascript: isNumericKey(this.value)">
					</td>	
				</tr>	
				<!--tr>
					<td class="fieldLabelRight" >Telephone :</td>
					<td class="fieldLabel" ><input name="url_telno1" type="text" id="url_telno1" value="{url_telno1}"  maxlength="255" ></td>
				</tr-->	
				<tr>
					<td class="fieldLabelRight" >Link Title  :</td>
					<td class="fieldLabel" ><input name="url_title" type="text" value="{url_title}" size="55"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight" valign="top" width="35%" >Link Catagory : </td>
					<td width="75%" class="fieldLabel"> {Link_Category}	</td>
					<!--td width="75%" class="fieldLabel"> 
					<select name="link_catagory">
					<option value="">Select Catagory</option>
					{cat_fill}
					</select>
					</td-->
				</tr>	
				<tr>
					<td width="35%" class="fieldLabelRight" valign="top">Description : </td>
					<td>
					<textarea name="url_desc" rows="3" cols="50">{url_desc}</textarea>
					</td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Link URL :</td>
					<td class="fieldLabel" ><input name="url_url" type="text" value="{url_url}" size="55"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Our URL link in your site :</td>
					<td class="fieldLabel" ><input name="url_oururl" type="text" id="url_oururl" value="{url_oururl}" size="55"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight">Link Email :</td>
					<td class="fieldLabel" ><input type="text" name="url_email" value="{url_email}"  maxlength="255" ></td>
				</tr>
				<tr>
					<!--td class="fieldLabelRight">Status :</td>
					<td width="75%" class="fieldLabel"> 
					<select name = "cat_status">
					{Status_List}
					</select>
					</td-->
				</tr>
				<tr><td>&nbsp;</td></tr>
				<!--tr>
					<td colspan="8" align="center" >
						<input  type="submit" name="Submit"  value="Save"   class="nrlButton" onClick="javascript: return Form_Submit(document.frmLinkDetail);">&nbsp;&nbsp;&nbsp;
						<input  type="submit" name="Submit"  value="Cancel" class="nrlButton" >
					</td>
				</tr-->
				<tr>
					<td colspan="8" align="center" >
						<input type="submit" name="Submit" value="{Save}{Update}" class="nrlButton" onClick="javascript: return Form_Submit(document.frmLinkDetail);">
						<input type="submit" name="Submit" value="{Cancel}" class="nrlButton">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="hidden" name="Action" value="{ACTION}">
						<input type="hidden" name="start" value="{Start}">
						<input type="hidden" name="link_id" value="{link_id}">
						<input type="hidden" name="cat_id" value="{cat_id}">
						<input type="hidden" name="cat_status" value="0">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>