<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cancun transfers - Private Cancun Airport Transfers</title>
<meta name="description" content="Cancun transfers and private shuttles to Cancun Airport - Hotel - Cancun Airport" />
<meta name="keywords" content="Cancun tranfers, cancun shuttles, cancun airport tranfers, cancun ground transporttation" />
<meta name="robots" content="index,follow" />

<link href="{$Templates_CSS}table.css" rel="stylesheet" type="text/css">
<link href="{$Templates_CSS}train.css" rel="stylesheet" type="text/css">
<link href="{$Templates_JS}jquery.ui.all.css" rel="stylesheet" type="text/css"/>
<script src="{$Templates_JS}jquery-1.4.2.min.js"></script>
<script src="{$Templates_JS}jquery-ui-1.8.custom.min.js"></script>
<script src="{$Templates_JS}jquery.ui.datepicker-es.js"></script>
<script language="javascript" >
{if $lng == "en"}
	var vLangue=1;
{else}
	var vLangue=0;
{/if}
</script>
<script language="javascript" src="{$Templates_JS}validate.js"></script>
<script language="javascript" src="{$Templates_JS}functions.js"></script>
<script language="javascript" src="{$Templates_JS}index.js"></script>
<script language="javascript" src="{$Templates_JS}menu.js"></script>
{section name=FileName loop=$JavaScript}
<script language="javascript" src="{$Templates_JS}{$JavaScript[FileName]}"></script>
{/section}
  
</head>

<body onload="MM_preloadImages('images/home-hover.gif','images/about-hover.gif','images/order-hover.gif','images/contact-hover.gif','images/affiliates-hover.gif','images/newsletter-hover.gif')">

			{include file="$T_Header"} 
		
						{include file="$T_Menu"}
					
						{include file="$T_Body"}
			
			{include file="$T_Footer"}


</body>
</html>