<table border="0" cellpadding="0" cellspacing="0" width="95%">
<form name="frmTransferCart" action="{$A_TransferCart}" method="post">
 <tr>
	<td>
      <table border="0" cellpadding="0" cellspacing="1" width="100%">
		<tr>
			<td colspan="3" width="100%">
				<table width="100%">
					<td class="fieldLabelLeft" align="left" width="70%">{$FirstName} {$LastName}&nbsp;[{$L_ConfirmationNo} : {$confirmationNo}]</td>
					<td class="boldText"  width="30%" align="right">{$CurrDate}</td>
				</table>	
			</td>
		</tr>
		<tr><td class="lightblueLine" width="100%"></td></tr>
		<tr><td>&nbsp;</td></tr>
		   <tr>
			  <td align="center" valign="top">
				<table border="0" cellpadding="2" cellspacing="2" width="98%">
					<tr> 
					  <td colspan="3" height="1" bgcolor="#000066"></td>
					</tr>
					<tr> 
					  <td colspan="3" class="blueHeader">&nbsp;{$L_Transferdetail}</td>
					</tr>
					<tr> 
					  <td colspan="3" height="1" bgcolor="#000066"></td>
					</tr>
 
				<tr><td colspan="3" height="5"></td></tr>
				<tr> 
                  <td  valign="top" class="fieldLabelRight" width="30%">{$L_Destinations} :</td>
				  <td  colspan="2" align="left">{$dest_title}</td>
                </tr>
				{if $transferStatus == 2}		
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalDate} :</td>
				  <td colspan="2" align="left">{$arrivalDate}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalTime} :</td>
				  <td colspan="2" align="left">{$arrivalTime}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalAirline} :</td>
				  <td colspan="2" align="left">{$arrivalAirline}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalFlight} :</td>
				  <td colspan="2" align="left">{$arrivalFlight}</td>
                </tr>
					<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureDate} :</td>
				  <td colspan="2" align="left">{$departureDate}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureTime} :</td>
				  <td colspan="2" align="left">{$departureTime}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureAirline} :</td>
				  <td colspan="2" align="left">{$departureAirline}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureFlight} :</td>
				  <td colspan="2" align="left">{$departureFlight}</td>
                </tr>
				{/if}

				{if $transferStatus == 1}		
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureDate} :</td>
				  <td colspan="2" align="left">{$departureDate}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureTime} :</td>
				  <td colspan="2" align="left">{$departureTime}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureAirline} :</td>
				  <td colspan="2" align="left">{$departureAirline}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureFlight} :</td>
				  <td colspan="2" align="left">{$departureFlight}</td>
                </tr>
				 {/if}
				
				{if $transferStatus ==0}	
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalDate} :</td>
				  <td colspan="2" align="left">{$arrivalDate}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalTime} :</td>
				  <td colspan="2" align="left">{$arrivalTime}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalAirline} :</td>
				  <td colspan="2" align="left">{$arrivalAirline}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalFlight} :</td>
				  <td colspan="2" align="left">{$arrivalFlight}</td>
                </tr>
				{/if}
				
				<tr> 
                  <td class="fieldLabelRight">{$L_Hotelname} :</td>
                  <td  align="left">{$hotelName}</td>
				  <td></td>
                </tr>
                <tr> 
                  <td class="fieldLabelRight">{$L_Total} :</td>
                  <td align="left"><b> $ {$transferCharge} {$Currency}</b></td>
				  <td></td>
                </tr>
				</table>
					</td>
				</tr>
				<tr><td>
					<script language="JavaScript">
						window.print();
					</script>
				&nbsp;</td></tr>
			</table>
        </td>
	</tr>

	</form>
</table>