<link href="css/style.css" rel="stylesheet" type="text/css" />
<table border="0" cellpadding="1" cellspacing="1" width="100%">
<form name="frmTestimonial" action="{$A_Action}" method="post">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" height="100%" border="0" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<table width="92%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top">
												<h1 align='justify' style="margin-bottom:0px" class='style6 Estilo1 Estilo8 Estilo39'>{$Testimonials}
												&nbsp;[ <a href="javascript: Add_Click('{$Add_Action}');" class="actionLink">{$Add}</a> ]</h1>
											</td>
										</tr>
										<tr>
											<td valign="top">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
														{if $SuccMessage}
														<tr>
															<td class="successMsg"><br />{$SuccMessage}<br /></td>
														</tr>
														{/if}

														<tr>
															<td valign="top" align="center">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td valign="top" align="center">
																			<table border="0" cellpadding="2" cellspacing="2" width="100%">
																				
																				<tr><td height="1" class="dividerBg"></td></tr>
																				{foreach from=$PersonData item=Person}    
																				<tr class="{cycle values='list_A, list_B'}">
																					<td valign="top">
																						<table border="0" cellpadding="0" cellspacing="0" width="100%">
																							
																							<tr>
																								<td align="left">
																								<b>{$Person->person_name|regex_replace:"/[\\\\]/":""}</b><br>
																								{if $Person->person_city}
																									{$Person->person_city|regex_replace:"/[\\\\]/":""},&nbsp;

																									{if $Person->person_state}
																										{$Person->person_state|regex_replace:"/[\\\\]/":""}<br>
																									{/if}	
																									
																									{if $Person->person_country}	
																										{$Person->person_country}<Br />
																									{/if}
																									
																								{/if}
																								{if $Person->person_phone}
																								Tel: {$Person->person_phone|regex_replace:"/[\\\\]/":""}<br>
																								{/if}
																								{if $Person->person_fax}
																								Fax: {$Person->person_fax|regex_replace:"/[\\\\]/":""}<br>
																								{/if}
																								{if $Person->person_email}
																								<a href="mailto:{$Person->person_email|regex_replace:"/[\\\\]/":""}" class="siteLink">{$Person->person_email|regex_replace:"/[\\\\]/":""}</a><br>
																								{/if}
																								</td>
																								{if $Person->person_pic}
																								<td>
																								<img src="{if $Person->person_pic} {$Testimonial_Path|cat:'thumb_'|cat:$Person->person_pic|regex_replace:'/[\\\\]/':''} {else} {$Testimonial_Path|cat:$Default_Picture} {/if}" border="0">
																								</td>
																								{/if}
																							</tr>
																							<Tr><Td>&nbsp;</Td></Tr>
																							<tr> 
																								<td colspan="2">
																									<p align="justify">{$Person->person_comment|regex_replace:"/[\\\\]/":""|nl2br}</p>
																								</td> 
																							</tr>
																							<tr><td height="1" class="dividerBg" colspan="2"></td></tr>
																						</table>
																					</td> 
																				</tr>
																				{/foreach}
																				<tr><td colspan="5">&nbsp;</td></tr>
																				<tr>
																					<td colspan="5" class="pageLink" align="right">{$Page_Link}<!-- Page : 1 2 3 Next --></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr><td colspan="2">&nbsp;</td></tr>
																	<tr>
																		<td colspan="2">
																			<input type="hidden" name="person_id">
																			<input type="hidden" name="Action" value="{$ACTION}">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
												</table>		
											</td>
										</tr>		

																				
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</form>
</table>
