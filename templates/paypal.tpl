<table border="0" cellpadding="2" cellspacing="2" width="98%">
	<tr>
		<td height="10">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" width="100%"><br><br>
			<p class="Estilo10"><span class="texto_content">{$L_Paypal_Msg}</span></p>
		</td>
	</tr>						
	<tr>
		<td height="10">&nbsp;</td>
	</tr>
</table>
<form name='frmPayment' 	action='https://www.paypal.com/cgi-bin/webscr' method='post'>
	<input type="hidden" 	name="cmd" 						value="_ext-enter">
	<input type="hidden" 	name="redirect_cmd" 			value="_xclick">
	<input type='hidden' 	name='business' 				value='{$paypal_id}'>

 	<!-- Order Info -->
	<input type="hidden" 		name="item_name" 				value="{$Firstname}, {$Lastname}, {$Address1}, {$City}, {$State}">

	{section name=Cart loop=$chkdestTitle}
		<input type="hidden" 	name="on{$smarty.section.Cart.iteration-1}" value="{$L_Reservation}{$smarty.section.Cart.iteration}">		
		<input type="hidden" 	name="os{$smarty.section.Cart.iteration-1}" value="{$chkdestTitle[Cart]}">
	{/section} 

	<input type='hidden' 	name='amount' 					value='{$V_paymentTotal}'>
	<input type='hidden' 	name='no_shipping' 				value='1'>
	<input type='hidden' 	name='x_invoice_num' 			value='{$order_id}'>

 	<!-- Billing information  -->
	<input type='hidden' 	name='first_name' 				value='{$Firstname}'>
	<input type='hidden' 	name='last_name' 				value='{$Lastname}'>
	<input type='hidden' 	name='address1' 				value='{$Address1}'>
	<input type='hidden' 	name='city' 					value='{$City}'>
	<input type='hidden' 	name='state' 					value='{$State}'>
	<input type='hidden' 	name='zip' 						value='{$Zip}'>
	<input type='hidden' 	name='country_code' 			value='{$Country}'>
	<input type='hidden' 	name='night_phone_a' 			value='{$Phone_No}'>
	<input type='hidden' 	name='email' 					value='{$Email}'>

 	<!-- Additional Parameters -->
	<input type='hidden' 	name='lc' 						value='en'>
    <input type="hidden" 	name="bn" 						value="PP-BuyNowBF">	
	<input type='hidden' 	name='merchant_order_id' 		value='{$order_id}'>
	<input type='hidden' 	name='currency_code' 			value='{$paypal_currency}'>
	<input type='hidden' 	name='no_note' 					value='1'>

 	<!-- Return Parameters -->
	<input type='hidden' 	name='return' 					value='{$Site_Root}paymentProcess.php?Action=Complete'>
	<input type='hidden' 	name='cancel_return' 			value='{$Site_Root}paymentProcess.php?Action=Cancel'>

 	<!-- Custom Parameters -->
	<input type='hidden' 	name='customer_id' 				value='{$auth_id}'>
	<input type="hidden" 	name="custom" 					value="0">
	<input type="hidden" 	name="rm" 						value="2">
	
</form>
<script>setTimeout('document.frmPayment.submit();', 5000);</script>

