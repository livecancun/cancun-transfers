<table border="0" cellpadding="0" cellspacing="1" width="595">
	<form name="frmCategory"  action="{A_Action}" method="post">
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="boldHeader">Category Detail [ {L_Action} ]</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="3" width="97%">
				<tr height="20">
					<td colspan="2" align="center" class="successMsg">&nbsp;{Message}</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td class="fieldLabelRight" >New Category Name :</td>
					<td class="fieldLabel" ><input type="text" name="cat_name" value="{cat_name}"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight">Category Status :</td>
					<td class="fieldLabel">
					<select name="cat_status">
					<option value="">Select Status</option>
					{Status_List}
					</select>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td colspan="8" align="center" >
						<input  type="submit" name="Submit"  value="Save"   class="nrlButton" onClick="javascript: return Form_LinkCat_Submit(document.frmCategory);">&nbsp;&nbsp;&nbsp;
						<input  type="submit" name="Submit"  value="Cancel" class="nrlButton" >
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="hidden" name="Action" value="{ACTION}">
						<input type="hidden" name="start" value="{Start}">
						<input type="hidden" name="cat_id" value="{cat_id}">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>