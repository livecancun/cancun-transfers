
<table border="0" cellpadding="0" cellspacing="0" width="95%">
<form name="frmTransferCart" action="{$A_TransferCart}" method="post">
 <tr>
	<td>
      <table border="0" cellpadding="0" cellspacing="1" width="100%">
		<tr><td colspan="3" height="5"></td></tr>
		   <tr>
			  <td align="center" valign="top">
				<table border="0" cellpadding="2" cellspacing="2" width="98%">
					<tr> 
					  <td colspan="3" height="1" bgcolor="#000066"></td>
					</tr>
					<tr> 
					  <td colspan="3" class="blueHeader">&nbsp;{$L_Transferdetail}</td>
					</tr>
					<tr> 
					  <td colspan="3" height="1" bgcolor="#000066"></td>
					</tr>
 				<tr>
					 <td colspan="3" align="right" height="25"><a href="javascript:Print_Click('{$cartId}');" class="blueLink"><img src="{$Templates_Image}print.gif" border="0">{$L_Print_Version}</a></td>
				</tr>
				<tr><td colspan="3" height="5"></td></tr>
					<tr> 
                  <td  valign="top" class="fieldLabelRight" width="30%">{$L_Destinations} :</td>
				  <td  colspan="2" align="left">{$destination}</td>
                </tr>
<!--				<tr> 
                  <td valign="top" class="fieldLabelRight">{$TransferRates} :</td>
				  <td colspan="2" ><b>{$SingleTrip}($) :</b> {$transferSingleTripRate},<b> {$RoundTrip}($) :</b> {$transferRoundTripRate}</td>
                </tr>
			<tr><td colspan="3" height="5"><hr></td></tr>
			-->	
				<!--tr> 
                  <td valign="top" class="fieldLabelRight">{$L_transferRequestNo} :</td>
				  <td colspan="2" >{$transferRequestNo}</td>
                </tr-->
					    
						
						
						
						
						
				{if $transferStatus == 2}		
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalDate} :</td>
				  <td colspan="2" align="left">{$arrivalDate}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalTime} :</td>
				  <td colspan="2" align="left">{$arrivalTime}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalAirline} :</td>
				  <td colspan="2" align="left">{$arrivalAirline}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalFlight} :</td>
				  <td colspan="2" align="left">{$arrivalFlight}</td>
                </tr>
					<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureDate} :</td>
				  <td colspan="2" align="left">{$departureDate}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureTime} :</td>
				  <td colspan="2" align="left">{$departureTime}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureAirline} :</td>
				  <td colspan="2" align="left">{$departureAirline}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureFlight} :</td>
				  <td colspan="2" align="left">{$departureFlight}</td>
                </tr>
				{/if}

				{if $transferStatus == 1}		
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureDate} :</td>
				  <td colspan="2" align="left">{$departureDate}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureTime} :</td>
				  <td colspan="2" align="left">{$departureTime}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureAirline} :</td>
				  <td colspan="2" align="left">{$departureAirline}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_departureFlight} :</td>
				  <td colspan="2" align="left">{$departureFlight}</td>
                </tr>
				{/if}
				
				{if $transferStatus ==0}		
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalDate} :</td>
				  <td colspan="2" align="left">{$arrivalDate}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalTime} :</td>
				  <td colspan="2" align="left">{$arrivalTime}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalAirline} :</td>
				  <td colspan="2" align="left">{$arrivalAirline}</td>
                </tr>
				<tr> 
                  <td valign="top" class="fieldLabelRight">{$L_arrivalFlight} :</td>
				  <td colspan="2" align="left">{$arrivalFlight}</td>
                </tr>
				{/if}
				
               <tr> 
                  <td class="fieldLabelRight">{$L_Hotelname} :</td>
                  <td align="left">{$hotelName}</td>
				  <td></td>
                </tr>
		       <tr> 
  	               <td class="fieldLabelRight" valign="top">{$L_No_Person} :</td>
                   <td align="left">{$tres_no_person}</td>
				   <td></td>
                </tr>
<!--		       <tr> 
  	               <td class="fieldLabelRight" valign="top">{$NoofAdultPerson} :</td>
                   <td >{$tres_no_adult}</td>
				   <td></td>
                </tr>
                <tr> 
                  <td class="fieldLabelRight">{$NoofKid} :</td>
                  <td > {$tres_no_kid} </td>
				  <td></td>
                </tr>
-->            <tr> 
                  <td class="fieldLabelRight">{$L_Total} :</td>
                  <td align="left">{$transferCharge}</td>
				  <td></td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="3" height="10"></td>
                </tr>
				<tr><td colspan="2" align="center">
			  	<input type="button" name="Submit" value="{$Back}" class="greyButton" onclick="javascript:location.href('usercart.php')">
				</td></tr>
				<tr><td colspan="3">&nbsp;</td></tr>
                <tr> 
                  <td align="center" colspan="3"> 
                    <input type="hidden" name="Action" value="{$ACTION}"> 
					<input type="hidden" name="user_id" value="{$user_id}"> 
					<input type="hidden" name="Start" value="{$Start}"> </td>
                </tr>
				<!--tr><td colspan="3"><b>{$L_Transferstatus} {$L_Note} : </b></td></tr>
				<tr><td colspan="3"><hr></td></tr>
				<tr>
							<td><b>PR</b> - {$Note_PR}</td>
							<td><b>AR</b> - {$Note_AR}</td>
							<td><b>NA</b> - {$Note_NA}</td>
				</tr>
				<tr>
							<td><b>RP</b> - {$Note_RP}</td>
							<td><b>PD</b> - {$Note_PD}</td>
							<td>&nbsp;</td>
				 </tr>
					<tr> 
					  <td colspan="3" height="1" bgcolor="#000066"></td>
					</tr-->
				</table>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
        </td>
	</tr>
	</form>
</table>