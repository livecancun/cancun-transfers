{if $lng == "en"}
<br /><br />
<strong>All Rates are in USD American Dollar, prices per VAN, PAX = Passengers into the 1 Van. , private transfers only.</strong>  
                        </td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td><img src="images/grey-line-big.gif" width="591" height="1" vspace="15" /></td>
                </tr>
                <tr>
                  <td><table width="694" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="694" class="heading-grey-bottom"><img src="images/arrow-bottom.gif" width="12" height="14" /> Why <span class="heading-red"><em>Us ?</em></span> </td>
                    </tr>
                    <tr>
                      <td class="warea">
                                            
                      Cancun-transfers.com by KinMont, is a reliable and secure transportation company providing transportation to individuals, families and small groups. All
shuttles, vans have air conditioned, licensed and insured. All drivers are
bilingual and professional. Let us know your special needs...

Special Assistance for disabled passengers.
                        </td></tr>
                  </table></td>
                </tr>
              </table></td>
              <td width="99" class="grey-right">&nbsp;</td>
            </tr>
          </table></td>
          <td width="218">&nbsp;</td>
          <td width="20" valign="top">&nbsp;</td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <table width="1000" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td bgcolor="#F4F4F4" class="footer-heading"><a href="http://www.cancun-transfers.com/">Home</a> | <a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=en">Rates</a> | <a href="arrivals.html">Arrivals</a> | <a href="contact-us.html">Contact Us</a> | <a href="about-us.html">About Us</a> | <a href="http://www.cancun-transfers.com/es/">Espa&ntilde;ol</a></td>
        </tr>
        <tr>
          <td bgcolor="#F4F4F4" class="footer-subheading"><p>Copyright &copy; 2014 cancun-transfers.com All Rights Reserved - <a href="http://www.cancun-shuttle.com/terms-and-conditions.html">Terms and condtions</a></p>
            <a href="http://www.cancun-transfers.com">Cancun shuttle</a> | <a href="http://www.cancun-transfers.com/">Cancun transfers </a> | <a href="http://www.airport-cancun-shuttle.com/">Airport Cancun Shuttle </a> | 
                <a href="http://www.cancun-shuttle.com/">Cancun Shuttle </a></td>
        </tr>
      </table>
 	</td>
  </tr>
</table>
</center>
</body>
</html>
{else}
<br /><br />
<strong>Todas las tarifas son expresadas en USD Americanos, los precios son por VAN, PAX = Pasajeros dentro de una VAN, la transportacion es privada.</strong>  
                        </td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                 <td><img src="images/grey-line-big.gif" width="591" height="1" vspace="15" /></td>
                </tr>
                <tr>
                  <td><table width="694" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="694" class="heading-grey-bottom"><img src="images/arrow-bottom.gif" width="12" height="14" /> Porque <span class="heading-red"><em>Nosotros ?</em></span> </td>
                    </tr>
                    <tr>
                      <td class="warea">
                       <br /><br />Cancun-transfers.com del grupo KinMont, es una empresa de transporte fiable y segura que proporciona transportacion a individuales, familias y grupos. Todos los autobuses, camionetas tienen aire acondicionado, con licencia y asegurados. Todos los conductores son bilingues y profesionales. Haganos saber sus necesidades especiales... Asistencia especial para pasajeros minusvalidos.
                        </td></tr>
                  </table></td>
                </tr>
              </table></td>
              <td width="99" class="grey-right">&nbsp;</td>
            </tr>
          </table></td>
          <td width="218">&nbsp;</td>
          <td width="20" valign="top">&nbsp;</td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
     <table width="1000" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td bgcolor="#F4F4F4" class="footer-heading"><a href="http://www.cancun-transfers.com/es/">Inicio</a> | <a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=sp">Tarifas</a> | <a href="llegadas-aeropuerto-cancun.html">Llegadas</a> | <a href="contactanos.html">Contactanos</a> | <a href="nosotros.html">Nosotros</a> | <a href="http://www.cancun-transfers.com">English</a></td>
        </tr>
        <tr>
          <td bgcolor="#F4F4F4" class="footer-subheading"><p>Copyright &copy; 2011 cancun-transfers.com Derechos reservados</p>
            <a href="http://www.cancun-transfers.com">Cancun shuttle</a> | <a href="http://www.cancun-transfers.com/">Cancun transfers </a> | <a href="http://www.airport-cancun-shuttle.com/">Airport Cancun Shuttle </a> | 
                <a href="http://www.cancun-shuttle.com/">Cancun Shuttle </a></td>
        </tr>
      </table>
 	</td>
  </tr>
</table>
</center>
</body>
</html>
{/if}