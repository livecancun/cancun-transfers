<script>
	var Valid_Min_Of_Passengers = '{$Valid_Min_Of_Passengers}';
	var Empty_No_Person		= '{$Empty_No_Person}';
	var Valid_No_Person		= '{$Valid_No_Person}';	
	var Empty_Hotel_Name  	= '{$Empty_Hotel_Name}';
	var Empty_Check_Box 	= '{$Empty_Check_Box}';
	var Empty_Arrival_Date 	= '{$Empty_Arrival_Date}';
	var Valid_Arrival_Date 	= '{$Valid_Arrival_Date}';
	var Empty_Airline_Name 	= '{$Empty_Airline_Name}';
	var Empty_Flight_No 	= '{$Empty_Flight_No}';
	var Empty_Departure_Date= '{$Empty_Departure_Date}';
	var Valid_Departure_Date= '{$Valid_Departure_Date}';
	var Valid_BothDate_Msg 	= '{$Valid_BothDate_Msg}';
</script>
<table width="615" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
	<table width="600" border="0" align="center" cellpadding="1" cellspacing="1" >
		<tr>
                <td colspan="2" class="grdTD1 style2">{$L_Transfer_Service}</td>
	   </tr>
	  <tr> 
          <td width="595" height="250" valign="top" bgcolor="#FFFFFF" align="center"><br>
	  
<FORM name="frmTransfer" action="{$A_TransferCart}" method="post">
{section name=Res loop=$ResDetail}
<TABLE cellSpacing=1 cellPadding=1 width=590 bgColor=#EEEEEE  >
<tr>
<td colspan="2">
<TABLE cellSpacing=0 cellPadding=4 width=98% bgColor=#EEEEEE  border=0>
      <TR>
          <TD width="205" class="blueHeading">{$L_Book_Transfer}</TD>
	 </TR>
      <TR>
          <TD align="center">
                <TABLE cellSpacing=0 cellPadding=2 width=80%  bgColor=#EEEEEE border=0>
                  <tr>
                    <td width="22%" class="blackbold" align="right">
                      {$L_Destinations} :
                    </td>
                    <td width="78%" align="left">
						<select name="dest_id" class="black">
							{$Dest_List}
                      	</select>
                    </td>
                  </tr>
                  
                  <tr>
                    <td class="blackbold" align="right">
                      {$L_No_Person} :
                    </td>
                    <td align="left">
                      <INPUT type="text" class="black" size=3 value="{$ResDetail[Res].total_person}" name="total_person" onKeyPress="javascript: isNumericKey(this.value)">
                    </td>
                  </tr>

<!--                  <tr>
                    <td class="blackbold">
                      {$L_Adults} :
                    </td>
                    
                    <td>
                      <INPUT type="text" class="black" size=3 value="{$tres_no_adult}" name="tres_no_adult" onKeyPress="javascript: isNumericKey(this.value)">
                      <span class="blackbold">
						  {$L_Childs}:
						  <INPUT type="text" size=3 value="{$tres_no_kid}"  class="black" name="tres_no_kid"  onKeyPress="javascript: isNumericKey(this.value)">
                      </span>
                    </td>
                  </tr>
-->                  
                  <tr>
                    <td class="blackbold" align="right">{$L_Hotel} :</td>
                    <td align="left">
                      <INPUT type="text" size=20 name="hotelName" class="black" value="{$ResDetail[Res].hotelName}">
                    </td>
                  </tr>
			  </TABLE>
	</td>
  </tr>
</table>
</td>
</tr>

<tr>
<td align="center" valign="top">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			{foreach name=MaxPassenger from=$MaxPassenger item=Passenger}
			<td align="center">
				<input type="hidden" name="Passengr_Id[]" value="{$Passenger.passenger_id}">
				<input type="hidden" name="MaxPassenger[]" value="{$Passenger.passenger_ending_range}">			
			</td>
			{/foreach}
		</tr>
      <tr>
	  {if $ResDetail[Res].trip_status == 0 || $ResDetail[Res].trip_status == 1}
        <td width="100%" align="center">
          <table width="100%" height="181" border="0" cellpadding="1" cellspacing="1" bgcolor="#5270A2">
            <tr>
              <td height="37" align="center" bgcolor="#5470A0" class="whitebold">
                <INPUT type="checkbox" value="1" name="singleTrip" CHECKED>
				<INPUT type="hidden" name="roundTrip">
                &nbsp;{$L_Airport_To_Hotel}
              </td>
            </tr>
            <tr>
              <td valign="top" bgcolor="#EEEEEE">
                <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="black">
                      {$L_Airport_To_Hotel_Msg}
                    </td>
                  </tr>
                  <tr>
                    <td class="black">
                      <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                          <td width="26%" align="right" class="blackbold">
                            {$L_Date} :
                          </td>
                          <td width="74%" align="left">
                            <input type="text" name="arrivalDate" value="{$ResDetail[Res].arrivalDate}" id="arrivalDate" size="17" class="black">
                            
                            <script language="JavaScript" type="text/javascript">
                            fooCalendar = new dynCalendar('fooCalendar', 'calendarCallback', '{$Templates_Image}');
	                        </script>
                          </td>
                      	</tr>
                      
                      <tr>
                        <td align="right" class="blackbold">
                          {$L_Time} :
                        </td>
                        <td align="left">
                          <SELECT name="arrival_hour"	class="black">
                              {$V_Arrival_Hour}
                          </SELECT>
                          
                          <select class="black" name="arrival_minute">
                              {$V_Arrival_Minute}
                          </select>
                          
                          <SELECT class="black" name="arrival_DayPart">
                              {$V_ArrivalDayPart}
                          </SELECT>
                        </td>
                        
                      </tr>
                      
                      <tr>
                        
                        <td align="right" class="blackbold">
                          {$L_Airline} :
                        </td>
                        
                        <td align="left">
                          <input name="arrivalAirline" value="{$ResDetail[Res].arrivalAirline}" type="text" class="black" size="20">
                        </td>
                        
                      </tr>
                      
                      <tr>
                        
                        <td align="right" class="blackbold">
                          {$L_Flight} # :
                        </td>
                        
                        <td align="left">
                          <input name="arrivalFlight" value="{$ResDetail[Res].arrivalFlight}" type="text" class="black" size="20">
                        </td>
                        
                      </tr>
                      
                    </table>
                  </td>
                  
                </tr>
                
              </table>
            </td>
            
          </tr>
          
        </table>
      </td>
	  {/if}
      
  	  {if $ResDetail[Res].trip_status == 2}
        <td width="40%" align="center">
          <table width="100%" height="181" border="0" cellpadding="1" cellspacing="1" bgcolor="#5270A2">
            <tr>
              <td height="37" align="center" bgcolor="#5470A0" class="whitebold">
                <INPUT type="checkbox" value="1" name="singleTrip" CHECKED>
                &nbsp;{$L_Airport_To_Hotel}
              </td>
            </tr>
            <tr>
              <td valign="top" bgcolor="#EEEEEE">
                <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="black">
                      {$L_Airport_To_Hotel_Msg}
                    </td>
                  </tr>
                  <tr>
                    <td class="black">
                      <table width="100%" border="0" cellspacing="0" cellpadding="4">
                        <tr>
                          <td width="26%" align="right" class="blackbold">
                            {$L_Date} :
                          </td>
                          <td width="74%" align="left">
                            <input type="text" name="arrivalDate" value="{$ResDetail[Res].arrivalDate}" id="arrivalDate" size="17" class="black">
                            
                            <script language="JavaScript" type="text/javascript">
                            fooCalendar = new dynCalendar('fooCalendar', 'calendarCallback', '{$Templates_Image}');
	                        </script>
                          </td>
                      	</tr>
                      
                      <tr>
                        <td align="right" class="blackbold">
                          {$L_Time} :
                        </td>
                        <td align="left">
                          <SELECT name="arrival_hour"	class="black">
                              {$V_Arrival_Hour}
                          </SELECT>
                          
                          <select class="black" name="arrival_minute">
                              {$V_Arrival_Minute}
                          </select>
                          
                          <SELECT class="black" name="arrival_DayPart">
                              {$V_ArrivalDayPart}
                          </SELECT>
                        </td>
                        
                      </tr>
                      
                      <tr>
                        
                        <td align="right" class="blackbold">
                          {$L_Airline} :
                        </td>
                        
                        <td align="left">
                          <input name="arrivalAirline" value="{$ResDetail[Res].arrivalAirline}" type="text" class="black" size="20">
                        </td>
                        
                      </tr>
                      
                      <tr>
                        
                        <td align="right" class="blackbold">
                          {$L_Flight} # :
                        </td>
                        
                        <td align="left">
                          <input name="arrivalFlight" value="{$ResDetail[Res].arrivalFlight}" type="text" class="black" size="20">
                        </td>
                        
                      </tr>
                      
                    </table>
                  </td>
                  
                </tr>
                
              </table>
            </td>
            
          </tr>
          
        </table>
      </td>

	  <td width="2%">&nbsp;</td>
	  
      <td width="40%"  align="center">
        <table width="100%" height="181" border="0" cellpadding="1" cellspacing="1" bgcolor="#5270A2">
          
          <tr>
            
            <td height="37" align="center" bgcolor="#5470A0" class="whitebold">
              <INPUT type="checkbox" value="2" name="roundTrip" CHECKED>
              &nbsp;{$L_Hotel_To_Airport}
              
            </td>
            
          </tr>
          
          <tr>
            
            <td valign="top" bgcolor="#EEEEEE">
              <table width="99%" border="0" align="center" cellpadding="0" cellspacing="0">
                
                <tr>
                  
                  <td class="black">
                    {$L_Hotel_To_Airport_Msg}
                  </td>
                  
                </tr>
                
                <tr>
                  
                  <td class="black">
                    <table width="100%" border="0" cellspacing="0" cellpadding="4">
                      
                      <tr>
                        
                        <td width="26%" align="right" class="blackbold">
                          {$L_Date} :
                        </td>
                        
                        <td width="74%" align="left">
                          
                          <input type="text" name="departureDate" value="{$ResDetail[Res].departureDate}" id="departureDate" size="17" class="black">
                          
                          <script language="JavaScript" type="text/javascript">
                          fooCalendar1 = new dynCalendar('fooCalendar1', 'calendarCallback1', '{$Templates_Image}');
                          
                        </script>
                        
                      </td>
                      
                    </tr>
                    
                    <tr>
                      
                      <td align="right" class="blackbold">
                        {$L_Time} :
                      </td>
                      
                      <td  align="left">
                        
                        <SELECT name="departure_hour" class="black">
                            {$V_Departure_Hour}
                        </SELECT>
                        
                        <SELECT class="black" name="departure_minute">
                            {$V_Departure_Minute}
                        </SELECT>
                        
                        <SELECT class="black" name="departure_DayPart">
                            {$V_DepartureDayPart}
                        </SELECT>
                        
                      </td>
                      
                    </tr>
                    
                    <tr>
                      
                      <td align="right" class="blackbold">
                        {$L_Airline} :
                      </td>
                      
                      <td  align="left">
                        <input name="departureAirline"  value="{$ResDetail[Res].departureAirline}" type="text" class="black" size="20">
                      </td>
                      
                    </tr>
                    
                    <tr>
                      
                      <td align="right" class="blackbold">
                        {$L_Flight} # :
                      </td>
                      
                      <td align="left">
                        <input name="departureFlight"  value="{$ResDetail[Res].departureFlight}" type="text" class="black" size="20">
                      </td>
                      
                    </tr>
                    
                  </table>
                </td>
                
              </tr>
              
            </table>
          </td>
          
        </tr>
        
      </table>
    </td>
    {/if}
  </tr>
  
</table>
</td>
</tr>

	<TR>
		<TD colSpan=2 align="center">
			<!--input type="hidden" name="tres_no_kid" value="{$tres_no_kid}"-->
			<input type="hidden" name="cart_destId" value="{$ResDetail[Res].cart_destId}">
			<input type="submit" name="Submit" value="{$L_Update}" class="greyButton" onClick="javascript: return Form_Submit(frmTransfer);">&nbsp;&nbsp;
			<input type="submit" name="Submit" value="{$Cancel}" class="greyButton">
			<input type="hidden" name="Action" value="{$ACTION}">
			<input type="hidden" name="status">
			<input type="hidden" name="dest_id" value="{$dest_id}"> 
			<input type="hidden" name="rate_id" value="{$ResDetail[Res].rate_id}"> 
			<input type="hidden" name="cartId" value="{$ResDetail[Res].cart_id}"> 
			<!--INPUT type=image alt=Search src="{$imagepath}/Book_ING.gif" border=0-->
			<!--input type="button" value="Book" Name="Submit" class="lfmButton1" onClick="javascript: return Form_Submit(frmTransfer);"-->
		</TD>
	</TR>
</table>
{/section}
</form>

			 </td>
        </tr>
      </table>
    </td>
 </tr>
 <tr>
	    <td colspan="8">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr height="15">
						<td class="paggingLine" width="10%">
							&nbsp;&nbsp;{$Page_Link}
						</td>
					</tr>
				</table>
			</td>
		</tr>
</table>
