<table border="0" cellpadding="0" cellspacing="0" width="95%">
	<form name="frmReservation" action="{$A_Reservation}" method="post">
	<tr><td>&nbsp;</td></tr>
<!--	<tr><td align="center" class="errorMsg" width="100%" colspan="6">{$Message}</td></tr>
-->	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr><td colspan="6" height="5"></td></tr>
				<tr>
					<td align="center" valign="top">
						<table border="0" cellpadding="2" cellspacing="2" width="98%">
<!--							<tr><td class="errorMsg" width="100%" colspan="6" align="justify">{$ErrorMessage}</td></tr>
-->							<tr>
								<td colspan="3" class="blueHeader" align="left" width="100%">&nbsp;{$L_Reservation_Details}</td>
							</tr>						
							<tr>
							<td class="blueHeader" align="center" width="40%" colspan="2">{$L_cartItem}</td>
<!--		   				    <td class="blueHeader" align="center" width="40%">{$L_ItemType}</td>-->
			  				<td class="blueHeader" align="center" width="30%">{$L_Total}</td>
							</tr>
							<tr><td class="successMsg" colspan="3" align="center">{$Notfound_Message}</td></tr>

						{foreach name=CartInfo from=$CartInfo item=Cart}
						{assign var='TotalPrice' value=`$TotalPrice+$Cart.totalCharge`}
						<tr>
							<td class="List_B" align="center" height="25" colspan="2">{$Cart.dest_title}</td>
<!--							<td class="List_B" align="center">{$V_ItemType} </td>-->
							<td class="List_B" align="right">$ {$Cart.totalCharge} US</td>
						</tr>	
						{/foreach}
						    <tr>
								<td colspan="2"align="right">{$L_Total}:</td>
								<td  align="right">$ {$TotalPrice} US&nbsp;&nbsp;</td>
						    </tr>
							<tr><td colspan="3" class="NotifyMsg" align="justify">{$Msg}</td></tr>
					 </table>
					<table border="0" cellpadding="2" cellspacing="2" width="98%">
							<tr>
								<td colspan="3" class="blueHeader" align="left">&nbsp;{$L_personal_Information}</td>
							</tr>
							<tr><td colspan="3"><br></td></tr>

							<tr>
								<td class="fieldLabelRight">Payment Method:</td>
								<td align="left">
									<input type="radio" name="paymentMethod" class="stdRadion" value="0" {$Is_CheckedYes}>Pay Pal&nbsp;&nbsp;

<!--									<input type="radio" name="paymentMethod" class="stdRadion" value="1">Link Point&nbsp;&nbsp;
									<input type="radio" name="paymentMethod"  class="stdRadion" value="2">Authorize.net
-->								</td>
							</tr>
							<tr><td colspan="2">&nbsp;</td></tr>
							<tr><td colspan="2" height="10"></td></tr>
							<tr>
								<td>&nbsp;</td>
								<td align="left">
									<input type="submit" name="Submit" value="{$Submit}" class="greyButton" onClick="javascript: return Form_Submit(frmReservation);" {$isDisabled}>&nbsp;&nbsp;
									<input type="submit" name="Submit" value="{$Cancel}" class="greyButton">
									<input type="hidden" name="Action" value="{$ACTION}">
									<input type="hidden" name="paymentTotal" value="{$TotalPrice}">
									<input type="hidden" name="confirmationNo" value="{$V_ConfirmationNo}">
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
        </td>
	</tr>
	</form>
</table>