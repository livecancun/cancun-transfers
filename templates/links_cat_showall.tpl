<script language="javascript">
	var msg_Name		= '{$msg_Name}';
	var msg_Address		= '{$msg_Address}';
	var msg_City		= '{$msg_City}';
	var msg_State		= '{$msg_State}';
	var msg_Zip			= '{$msg_Zip}';
	var msg_Country  	= '{$msg_Country}';
	var msg_Telephone	= '{$msg_Telephone}';
	var msg_Title		= '{$msg_Title}';
	var msg_Description	= '{$msg_Description}';
	var msg_ValidSize	= '{$msg_ValidSize}';	
	var msg_Url			= '{$msg_Url}';
	var msg_Our_Url		= '{$msg_Our_Url}';
	var msg_Email		= '{$msg_Email}';
</script>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" height="100%" border="0" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<table width="92%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top">
												<table border="0" cellpadding="0" cellspacing="1" width="100%">
												<form name="frmLinkCategory" action="{$A_Action}" method="post">
													<tr>
														<td>
															<table border="0" cellpadding="0" cellspacing="1" width="100%">
																<tr>
																	<td>
																		<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'>{$L_Link_Heading}</h1>
																	</td>
																</tr>
																<tr height="20">
																	<td align="left">
																		<div align="justify">
																			<p align='justify' class='texto_content'>
																				<SPAN class='texto_content Estilo6'>
																					<span class='Estilo24'>
																						{$L_Step_Message}
																					</span>
																				</SPAN>
																			</p>
																		</div>
																	</td>
																</tr>
																<tr><td>&nbsp;</td></tr>
																<tr><td>&nbsp;</td></tr>																
																<tr>
																	<td><div align='justify' class='style6 Estilo1 Estilo8 Estilo39'><font size="2"><b>{$L_Link_Category}</b></font></div></td>
																</tr>
															</table>
														</td>
													</tr>
												
													<tr>
														<td valign="top" align="center">
															<table border="0" cellpadding="0" cellspacing="1" width="100%">
																<tr><td colspan="7" class="successMsg" align="center">{$Message}</td></tr>
																<tr><td colspan="7" align="center" class="errorMsg">{$No_Record_Found}</td></tr>

																{assign var="col" value=3}
																{foreach name=LinkInfo from=$LinkInfo item=Link}
																	{if $smarty.foreach.LinkInfo.iteration|mod:$col==1}
																	<tr>
																	{/if}
																		<td height="18" bgcolor="#EFEBEF" width="30%" align="center">
																			<a href="links.php?&cat_id={$Link.cat_id}" class="titleLink"><strong>{$Link.cat_name|capitalize}</strong></a>
																		</td>
																	{if $smarty.foreach.LinkInfo.iteration|mod:$col==0}
																	</tr>
																	{/if}
																	{foreachelse}
																	<tr><td colspan="7">{$no_records}</td></tr>	
																{/foreach}

																<tr>
																  <td colspan="7">&nbsp;</td>
																</tr>
																<input type="hidden" name="cat_id" value="{$cat_id}">
																<input type="hidden" name="Action">
																<input type="hidden" name="start" value="{$Start}">
															</table>
														</td>
													</tr>
												</form>	
												</table>											
											</td>
										</tr>

										<tr><td height="15">&nbsp;</td></tr>
										<tr>
											<td valign="top">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<form name="frmLinkDetail"  action="{$A_Action}" method="post">
														<tr>
															<td>
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td><div align='justify' class='style6 Estilo1 Estilo8 Estilo39'><font size="2"><b>{$L_Link_Detail} [ {$L_Link_Add_Link} ]</b></font></div></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
														<tr>
															<td valign="top" align="center">
																<table border="0" cellpadding="0" cellspacing="0" width="98%">
																	<tr bgcolor="#007ABD">
																		<td colspan="2"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Personal_Information} </span></span></div></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td width="30%"><div align="right"><span class="texto_content">{$L_Link_Name} : </span></div></td>
																		<td width="68%" class="texto_content" align="left"><input name="url_name" type="text" id="url_name" value="{$url_name}" maxlength="255" size="30"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_Address} : </span></div></td>
																		<td class="texto_content" align="left"><textarea name="url_address" cols="20" rows="3" id="url_address">{$url_address}</textarea></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_City} : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_city" type="text" id="url_city" value="{$url_city}"  maxlength="255"  size="30"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_State}  : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_state" type="text" id="url_state" value="{$url_state}"  maxlength="255"  size="30"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_Zip} : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_zip" type="text" id="url_zip" value="{$url_zip}"  maxlength="5"  size="30"  onKeyPress="javascript: isNumericKey(this.value)"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_Country} : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_country" type="text" id="url_country" value="{$url_country}"  maxlength="255"  size="30"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_Telephone} : </span></div></td>
																		<td class="texto_content" align="left">
																			<input type="text" name="url_telno_areacode" size="3" maxlength="3" value="{$url_telno_areacode}" onKeyPress="javascript: isNumericKey(this.value)"> -
																			<input type="text" name="url_telno_citycode" size="3" maxlength="3" value="{$url_telno_citycode}" onKeyPress="javascript: isNumericKey(this.value)"> -
																			<input type="text" name="url_telno_no" size="4" maxlength="4" value="{$url_telno_no}" onKeyPress="javascript: isNumericKey(this.value)">
																		</td>	
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_Title}  : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_title" type="text" value="{$url_title}" maxlength="255"  size="30"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td valign="top" width="35%"><div align="right"><span class="texto_content">{$L_Link_Category} : </span></div></td>
																		<td width="75%" class="texto_content" align="left"> 
																			{$Link_Category}	
																			<select name = "cat_id" class="black" id="cat_id">
																				{$Cat_List}
																			</select>
																		</td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td width="35%" valign="top"><div align="right"><span class="texto_content">{$L_Link_Description} : </span></div></td>
																		<td class="texto_content" align="left"><textarea name="url_desc" rows="3" cols="20">{$url_desc}</textarea><br><font class="validationText">{$L_Max300_Chars} </font></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_Url} : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_url" type="text" value="http://" size="30"  maxlength="255" class="texto_content"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_Our_Url} : </span></div></td>
																		<td class="texto_content" align="left" ><input name="url_oururl" type="text" id="url_oururl" value="http://" size="30"  maxlength="255" class="texto_content"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$L_Link_Email} : </span></div></td>
																		<td class="texto_content" align="left" ><input type="text" name="url_email" value="{$url_email}"  maxlength="255"  size="30"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB"><td colspan="2">&nbsp;</td></tr>
																	<tr bgcolor="#EBEBEB">
																		<td colspan="8" align="center" >
																			<input type="submit" name="Submit" value="{$Save}" class="nrlButton" onClick="javascript: return Form_Submit(document.frmLinkDetail);">
																			<!--<input type="submit" name="Submit" value="{$Cancel}" class="nrlButton">-->
																		</td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td colspan="2">
																			<input type="hidden" name="Action" value="{$ACTION}">
																			<input type="hidden" name="start" value="{$Start}">
																			<input type="hidden" name="link_id" value="{$link_id}">
																			<input name="cat_id_" type="hidden" id="cat_id_" value="{$cat_id}">
																			<input type="hidden" name="cat_status" value="0">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
														<tr>
															<td colspan="2" height="10" class="texto_content">{$L_Link_Publish}</td>
														</tr>
													</form>
												</table>		
											</td>
										</tr>		

																				
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>