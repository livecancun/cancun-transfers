<script>
	var Empty_Name		= '{$Empty_Name}';
	var Empty_Company  	= '{$Empty_Company}';
	var Valid_Zip 		= '{$Valid_Zip}';
	var Empty_Phone		= '{$Empty_Phone}';
	var Valid_Phone  	= '{$Valid_Phone}';
	var Empty_Fax 		= '{$Empty_Fax}';
	var Valid_Fax 		= '{$Valid_Fax}';
	var Empty_TollFree 	= '{$Empty_TollFree}';
	var Valid_TollFree 	= '{$Valid_TollFree}';
	var Empty_Email 	= '{$Empty_Email}';
	var Valid_Email 	= '{$Valid_Email}';
	var Valid_URL 		= '{$Valid_URL}';
	var Valid_Photo 	= '{$Valid_Photo}';			
	var Empty_Description	= '{$Empty_Description}';
	var Empty_Comment 	= '{$Empty_Comment}';	

	var Delete_Testimonial 		= '{$Delete_Testimonial}';	
	var Delete_AllTestimonials 	= '{$Delete_AllTestimonials}';	
		
</script>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
<form name="frmTestimonial" action="{$A_Action}" method="post" enctype="multipart/form-data"> 
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" height="100%" border="0" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<table width="92%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top">
												<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'>{$Testimonial_Header}</h1>
											</td>
										</tr>
										<tr>
											<td valign="top">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">

														<tr>
															<td valign="top" align="center">
																<table border="0" cellpadding="0" cellspacing="0" width="98%">
																	<tr bgcolor="#007ABD">
																		<td colspan="2"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Personal_Information} </span></span></div></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td width="35%"><div align="right"><span class="texto_content">{$Name} : </span></div></td>
																		<td width="63%" class="texto_content" align="left"><input type="text" name="person_name" value="{$Person_Name}" size="35" maxlength="50"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$Address} : </span></div></td>
																		<td class="texto_content" align="left"><textarea name="person_address" rows="5" cols="40">{$Person_Address}</textarea></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$City}  : </span></div></td>
																		<td class="texto_content" align="left"><input type="text" name="person_city" value="{$Person_City}" size="20" maxlength="25"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$State} : </span></div></td>

																		<td class="texto_content" align="left">
																			<input type="text" name="person_state" value="{$Person_State}" size="20" maxlength="25">
																		</td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$Country} : </span></div></td>
																		<td class="texto_content" align="left"><SELECT name="person_country" class="texto_content"><option value="0">-----------{$L_Select_Country}--------</option>{$CountryList}</SELECT></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$Zip}  : </span></div></td>
																		<td class="texto_content" align="left"><input type="text" name="person_zip" value="{$Person_Zip}" size="10" maxlength="5" onKeyPress="javascript: isNumericKey(this.value)"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$Phone} : </span></div></td>
																		<td class="texto_content" align="left">
																			<input type="text" name="person_phone_areacode" value="{$Person_Phone_Areacode}" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
																			<input type="text" name="person_phone_citycode" value="{$Person_Phone_Citycode}" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
																			<input type="text" name="person_phone_no" value="{$Person_Phone_No}" size="4" maxlength="4" onKeyPress="javascript: isNumericKey(this.value)">
																		</td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$Fax} : </span></div></td>
																		<td class="texto_content" align="left">
																			<input type="text" name="person_fax_areacode" value="{$Person_Fax_Areacode}" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
																			<input type="text" name="person_fax_citycode" value="{$Person_Fax_Citycode}" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
																			<input type="text" name="person_fax_no" value="{$Person_Fax_No}" size="4" maxlength="4" onKeyPress="javascript: isNumericKey(this.value)">
																		</td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$Email} : </span></div></td>
																		<td class="texto_content" align="left"><input type="text" name="person_email" value="{$Person_Email}" size="35" maxlength="255"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content">{$Website} : </span></div></td>
																		<td class="texto_content" align="left"><input type="text" name="person_website" value="{$Person_Website}" size="35" maxlength="255"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td valign="top"><div align="right"><span class="texto_content">{$Photo} : </span></div></td>
																		<td class="texto_content" align="left">
																			<input type="file" name="person_pic" size="25" maxlength="255" {if $Picture_Filename} onchange="javascript: UploadImage_Change(this, pic, '{$Picture_Filename}','');" {/if}>
																			<input type="hidden" name="person_pic_edit" value="{$Person_Pic}"><br>
																			<img src="{$Picture_Filename}" id="pic" border="0"><br>
																			{if $Person_Pic != ''}
																			<input type="checkbox" name="delete_picture" value="1" class="stdCheckBox"> {$Delete_Picture}
																			{/if}
																		</td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">																	
																		<td valign="top"><div align="right"><span class="texto_content">{$Comment} : </span></div></td>
																		<td class="texto_content" align="left"><textarea name="person_comment" rows="5" cols="40">{$Person_Comment}</textarea></td>
																	</tr>
																	<tr bgcolor="#EBEBEB"><td colspan="2">&nbsp;</td></tr>
																	<tr bgcolor="#EBEBEB">
																		<td colspan="8" align="center" >
																			<input type="submit" name="Submit" value="{$Save}" class="texto_content" onClick="javascript: return Form_Submit(document.frmTestimonial);"> 
																			&nbsp;&nbsp;<input type="button" name="Submit" value="{$Cancel}" class="texto_content" onClick="javascript: Cancel_Click('{$Cancel_Action}');">&nbsp;&nbsp;
																		</td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td colspan="2">
																			<input type="hidden" name="Action" value="{$ACTION}">
																			<input type="hidden" name="person_id" value="{$Person_Id}">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
												</table>		
											</td>
										</tr>		

																				
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</form>
</table>