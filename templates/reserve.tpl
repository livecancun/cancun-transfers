<script>
	var Empty_No_Person		= '{$Empty_No_Person}';
	var Valid_No_Person		= '{$Valid_No_Person}';	
	var Empty_Hotel_Name  	= '{$Empty_Hotel_Name}';
	var Empty_Check_Box 	= '{$Empty_Check_Box}';
	var Empty_Arrival_Date 	= '{$Empty_Arrival_Date}';
	var Valid_Arrival_Date 	= '{$Valid_Arrival_Date}';
	var Empty_Airline_Name 	= '{$Empty_Airline_Name}';
	var Empty_Flight_No 	= '{$Empty_Flight_No}';
	var Empty_Departure_Date= '{$Empty_Departure_Date}';
	var Valid_Departure_Date= '{$Valid_Departure_Date}';
	var Valid_BothDate_Msg 	= '{$Valid_BothDate_Msg}';
</script>

<br /><br />
<table>
<tr>
<td>
{if $lng == "en"}
<img src="images/step-1.jpg" /></td><td><h2>Please fill the form</h2>
{else}
<img src="images/step-1.jpg" /></td><td><h2>Porfavor llene el formulario</h2>
{/if}

</tr></table>

<table>
  <FORM name="frmReserve" action="{$A_Action}" method="post">
    <tr>
      <td >
<!--Test de formulario -->
<table>
 <tr>
						<td >
							<input type="hidden" name="singleTrip" value="1">	
							<span >
								{$L_Airport_To_Hotel}</span> {foreach name=MaxPassenger from=$MaxPassenger item=Passenger}
								<input type="hidden" name="Passengr_Id[]" value="{$Passenger.passenger_id}">
								<input type="hidden" name="MaxPassenger[]" value="{$Passenger.passenger_ending_range}">
								{/foreach}
							</span>
						</td>
                   </tr>
    
   <tr>
    <td>&nbsp;</td>

    <td>{$L_Destinations} :<br /><input type="text" class="texto_content" value="{$dest_title}" name="dest_title" readonly>
						<input type="hidden" class="texto_content" name="dest_id" value="{$dest_id}" size="35" >
</td>
  </tr>
     <tr>
    <td>&nbsp;</td>
    <td>{$L_No_Person} :<br /><input type="text" class="texto_content" size="10" value="{$no_person}" name="no_person" onkeypress="javascript: isNumericKey(this.value)"></td>
  </tr>
   <tr>
    <td>&nbsp;</td>

    <td>{$L_Hotel} : <br /><input type="text" class="texto_content" size="15" value="{$hotelName}" name="hotelName">
</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>{$L_Date} : <br />

<div class="demo">
<p><input name="arrivalDate" value="{$arrivalDate}" id="datepicker"></p>

</div> - <SELECT class="texto_content" name="arrival_hour">
														  {$V_Arrival_Hour}
                                          </SELECT>
                                          <select class="texto_content" name="arrival_minute">
														  {$V_Arrival_Minute}
                                          </select>
                                          <SELECT class="texto_content" name="arrival_DayPart">
														  {$V_ArrivalDayPart}
                                          </SELECT> &nbsp;</td>
  </tr>

  
    <tr>
    <td>&nbsp;</td>
    <td>{$L_Airline} : <br /><input name="arrivalAirline" value="{$arrivalAirline}" type="text" class="texto_content" size="15"> <input name="arrivalFlight" value="{$arrivalFlight}" type="text" class="texto_content" size="15"></td>
  </tr>  
  
</table>

<!--Fin Test de formulario -->
</td></tr><tr><td>
{if $TriptypeId == 1}
							<br /><br />
						{else}
						
                            <!--Inicia segunda tabla -->
<table>
                   <tr >
                     <td >
						
							<input type="hidden" name="roundTrip" value="2">							
					

						 {$L_Hotel_To_Airport}
					 </td>
                   </tr>
				   
                   <tr>
    <td>&nbsp;</td>

    <td>{$L_Destinations} : <br /><input type="text" class="texto_content" value="{$dest_title}" name="dest_title" readonly>
						<input type="hidden" class="texto_content" name="departureDestId" value="{$dest_id}" size="10" >
</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td>{$L_No_Person} :<br /><input type="text" class="texto_content" size="10" value="{$departureTotalPerson}" name="departureTotalPerson" onkeypress="javascript: isNumericKey(this.value)"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>

    <td>{$L_Hotel} : <br /><input type="text" class="texto_content" size="15" value="{$departureHotelName}" name="departureHotelName">
</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>{$L_Time} : <br />
    <div class="demo">
<p><input name="departureDate" value="{$departureDate}" id="datepicker2"></p></div>
 {if $lng == "en"}Depature Flight time {else} Horario del despegue del vuelo {/if}  - <br><SELECT class="texto_content" name="departure_hour">
									{$V_Departure_Hour}
							</SELECT>
							<SELECT class="texto_content" name="departure_minute">
									{$V_Departure_Minute}
							</SELECT>
							<SELECT class="texto_content" name="departure_DayPart">
									{$V_DepartureDayPart}
							</SELECT></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td>{$L_Flight} <br />
<SELECT class="texto_content" name="pickup_hour">
									{$V_Pickup_Hour}
							</SELECT>
                                  <SELECT class="texto_content" name="pickup_minute">
									{$V_Pickup_Minute}
							</SELECT>
                                <SELECT class="texto_content" name="pickup_DayPart">
									{$V_Pickup_DayPart}
							</SELECT></td>
  </tr>
  
   <tr>
    <td>&nbsp;</td>
    <td>{$L_Airline} <br /><input name="departureAirline" value="{$departureAirline}" type="text" class="texto_content" size="15"> - <input name="departureFlight" value="{$departureFlight}" type="text" class="texto_content" size="15"></td>
  </tr>
                 </table>						
						{/if} 
</td>
               </tr>
			   <tr> <td>&nbsp;</td>
                 <td><input type="image" onclick="javascript: return Form_Submit(frmReserve);" src="{$Templates_Image}{$site_res_img}"></td>
               </tr>
            
		<tr> <td>&nbsp;</td>
		  <td colspan="2" valign="top" align="center">
			<input type="hidden" name="Submit" value="{$SUBMIT}">
			<input type="hidden" name="Action" value="{$ACTION}">
			<input type="hidden" name="status">
			<input type="hidden" name="carttranId" value="{$carttranId}">
			<input type="hidden" name="itemType" value="2">
			<input type="hidden" name="rate" value="{$Rate}">
			<input type="hidden" name="passenger_id" value="{$PassengerId}">
			<input type="hidden" name="triptype_id" value="{$TriptypeId}">
		  </td>
		</tr>
   
     
    </FORM>
<script language="javascript">
  	if(document.frmReserve.singleTrip.value == 1)
	{ldelim}
			if(document.frmReserve.roundTrip.value != 2)
			{ldelim}
				document.frmReserve.departureTotalPerson.disabled	=  true; 
				document.frmReserve.departureHotelName.disabled		=  true;					
				document.frmReserve.departureDate.disabled			=  true; 
				
/*				document.frmReserve.pickup_hour.disabled			=  true; 
				document.frmReserve.pickup_minute.disabled			=  true; 
				document.frmReserve.pickup_DayPart.disabled			=  true; 
*/
				document.frmReserve.departure_hour.disabled			=  true; 
				document.frmReserve.departure_minute.disabled		=  true; 
				document.frmReserve.departure_DayPart.disabled		=  true; 

				document.frmReserve.departureAirline.disabled		=  true; 
				document.frmReserve.departureFlight.disabled		=  true; 				
			{rdelim}
	{rdelim}
  </script>
</table>
