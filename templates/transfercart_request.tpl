<script language="JavaScript">
function checkAll(frm, flag)
{
  with(document.frmTransferCart)
	{
		 
	 if(document.all["chktransferCartId[]"].length)
		 {
		   var len = document.all["chktransferCartId[]"].length;
		  for (var i = 0; i < len; i++) 
			{
				var e = document.all["chktransferCartId[]"][i];
				e.checked = true;
		
			}
		}
	 else
	 	{
		   var e = document.all["chktransferCartId[]"];
		   e.checked = true;
		}		
	}
}
function Form_Delete()
{
	
	with(document.frmTransferCart)
	{
	  if(document.all["chktransferCartId[]"])
	   {	
		  if(document.all["chktransferCartId[]"].length)
		   {
			 if(!IsCheckBoxChecked(document.all["chktransferCartId[]"],'{Valid_Delete}'))
			 {
				return false;
			 }
			 else
			 {
			 	if (confirm('{Confirm_Delete}'))
				{
					Action.value = "Delete";
					submit();
				}
			 }
		  }
		  else if(!(document.all["chktransferCartId[]"].checked))
		  {
		 	alert('{Valid_Delete}');
		 	return false;
		  }	 
	      else
	      {
			if (confirm('{Confirm_Delete}'))
			{
				Action.value = "Delete";
				submit();
			}
		 }
	  }	 
		return true;	
	}
}	
function Form_Request()
{
	
	with(document.frmTransferCart)
	{
	if(document.all["chktransferCartId[]"])
	 {
		 if(document.all["chktransferCartId[]"].length)
		 {
			 if(!IsCheckBoxChecked(document.all["chktransferCartId[]"],'{Valid_Select}'))
			 {
				return false;
			 }
			 else
			 {
				 Action.value = "Send_Request";
				 submit();
				 return true;
			 }
		 }	   
		 else if(!(document.all["chktransferCartId[]"].checked))
		 {
				alert('{Valid_Select}');
				return false;
		 }	 
		 else
		 {
				Action.value = "Send_Request";
				submit();
		 }
	 }	 
		return true;	
	}
}
function Form_Checkout()
{
	
	with(document.frmTransferCart)
	{
	if(document.all["chktransferCartId[]"])
	 {
		 if(document.all["chktransferCartId[]"].length)
		 {
			 if(!IsCheckBoxChecked(document.all["chktransferCartId[]"],'{Valid_Select_Tour}'))
			 {
				return false;
			 }
			 else
			 {
				 Action.value = "Show_All";
				 action = "transferreservation.php";
				 submit();
				 return true;
			 }
		 }	   
		 else if(!(document.all["chktransferCartId[]"].checked))
		 {
				alert('{Valid_Select_Tour}');
				return false;
		 }	 
		 else
		 {
				 Action.value = "Show_All";
				 action = "transferreservation.php";
				 submit();
				 return true;
		 }
	 }	 
		return true;	
	}
}
function View_Cart()
{
	with(document.frmTransferCart)
	{
		Action.value = "Show_Cart";
		submit();
	}
}
function View_Request()
{
	with(document.frmTransferCart)
	{
		Action.value = "Show_Request";
		submit();
	}
}
function Edit_Click(TransferCartId)
{
	with(document.frmTransferCart)
	{
	   
		carttranId.value = TransferCartId;
		Action.value = "Modify";
		submit();
	}
}
function Show_Click(TransferCartId)
{
	with(document.frmTransferCart)
	{
	   
		carttranId.value = TransferCartId;
		Action.value = "Show";
		submit();
	}
}
</script>

<style type="text/css">
<!--
.style2 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #FFFFFF;
}
-->
</style>
<table width="595" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td>
	<table width="595" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="1750A6">
	  <tr> 
          <td width="595" valign="top" bgcolor="#FFFFFF" align="center">
		    <TABLE cellSpacing="1" cellPadding=2 width=100% bgColor="#FFFFFF" >
			<tr>
				<td class="blueHeader" width="615" colspan="8">
					<table width="100%">
					  <tr>
						<td class="blueHeader" width="70%" colspan="5">{L_Transfer_Reservation_Details} ({L_Request})</td>
						<td class="blueHeader" width="30%" colspan="3" align="right"><a href="JavaScript: View_Cart();" class="whiteLink">View Cart</a>&nbsp;|&nbsp;<a href="JavaScript: View_Request();" class="whiteLink">View Request</a></td>
					   </tr>	
					</table>
				</td>		
			</tr>
			<tr><td width="615" colspan="8"class="successMsg">{Message}</td></tr>
			<FORM name="frmTransferCart" action="{A_TransferCart}" method="POST">
			<tr><td width="615" colspan="8"><br>
				<!--input type="button" Value="{Delete}" Name="Submit" class="nrlButton" onClick="javascript : return Form_Delete(frmTourCart);"-->
				<input type="hidden" name="Action" value="{ACTION}">
			</td></tr>
			   <tr>
			   	  <td align='center' width="5%" class="blueHeader">
				  <!--input type="checkbox" name="chkAll" onClick="javascript: return checkAll(document.frmTourCart, this.checked);" -->
				  </td>
				 <td class="blueHeader" align="center" width="20%">{L_transferRequestNo}</td> 
				 <td class="blueHeader" align="center" width="10%">{L_transferStatus}</td> 
			     <td class="blueHeader" align="center" width="38%">{L_Destinations}</td>
  				 <td class="blueHeader" align="center" width="12%">{L_Total}</td>
				 <td class="blueHeader" align="center" width="20%"></td>
			   </tr>
			    <tr><td class="successMsg" colspan="8" align="center">{Notfound_Message}</td></tr>
			    <!-- START BLOCK : CartDetail -->
			    <tr>
					<td align='center' width="5%" class="List_B"><input type='checkbox' name="chktransferCartId[]" value="{carttranId}" {disableCheckBox}></td>
					<td class="List_B" align="center" height="25">{V_transferRequestNo} </td>
					<td class="List_B" align="center" ><b>{V_trStatus}</b></td> 
					<td class="List_B" align="center" height="25">{V_transferOrgin} <img src="{imagepath}/{Transfer_Image}" border="0"> {V_transferDestination}</td>
					<td class="List_B" align="center">$ {V_Total}</td>
					<td class="List_B" align="center"><a href="JavaScript: Show_Click('{carttranId}');" class="lightgreyLink">{View}</a></td>
 			  </tr>	
			  <!-- END BLOCK : CartDetail -->
			  <input type="hidden" name="carttranId"> 
			  </form>
		 	 <tr><td colspan="4"align="right"><b>{L_Total} ($):</b></td>
			   <td  align="right"><b>{V_grandTotal}</b>&nbsp;&nbsp;</td>
			   <td></td>
			 </tr>
			 <tr><td colspan="8">&nbsp;</td></tr>
			 <tr>
				  <td colspan="8" align="center">
					<input type="button" value="{L_ReserveMore}" class="nrlButton" onClick="javascript: location.href('{ReserveMoreLink}');">&nbsp;&nbsp;
					<input type="button" value="{L_Checkout}" class="nrlButton" onClick="javascript : return Form_Checkout(frmTransferCart);">&nbsp;&nbsp;
				  </td>
	         </tr> 
			 <tr>
			 	<td colspan="8" align="center"><br>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr><td colspan="3" class="boldText">{L_transferStatus} {L_Note} :</td></tr>
						<tr><td colspan="3" height="5">&nbsp;</td></tr>
						<tr>
							<td><b>PR</b> - {Note_PR}</td>
							<td><b>AR</b> - {Note_AR}</td>
							<td><b>NA</b> - {Note_NA}</td>
						</tr>
						<tr>
							<td><b>RP</b> - {Note_RP}</td>
							<td><b>PD</b> - {Note_PD}</td>
							<td>&nbsp;</td>
						</tr>
					</table>	
				</td>
			 </tr>
		 </table>
		 </td>
        </tr>
      </table>
    </td>
 </tr>
 <tr>
	   <td colspan="8">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr height="15">
						<td class="paggingLine" width="10%">
							&nbsp;&nbsp;{Page_Link}
						</td>
					</tr>
				</table>
			</td>
		</tr>
</table>
