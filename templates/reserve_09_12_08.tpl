<script>
	var Empty_No_Person		= '{$Empty_No_Person}';
	var Valid_No_Person		= '{$Valid_No_Person}';	
	var Empty_Hotel_Name  	= '{$Empty_Hotel_Name}';
	var Empty_Check_Box 	= '{$Empty_Check_Box}';
	var Empty_Arrival_Date 	= '{$Empty_Arrival_Date}';
	var Valid_Arrival_Date 	= '{$Valid_Arrival_Date}';
	var Empty_Airline_Name 	= '{$Empty_Airline_Name}';
	var Empty_Flight_No 	= '{$Empty_Flight_No}';
	var Empty_Departure_Date= '{$Empty_Departure_Date}';
	var Valid_Departure_Date= '{$Valid_Departure_Date}';
	var Valid_BothDate_Msg 	= '{$Valid_BothDate_Msg}';
</script>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
  <FORM name="frmReserve" action="{$A_Action}" method="post">
    <tr>
      <td width="100%" valign="top" align="center">
	  
<table width="450" height="23" border="0" align="center" cellspacing="0">
       <tr>
         <td width="448" valign="top">
			 {$L_Resv_Msg}
             <table width="270" border="0" align="center" cellspacing="0">
               <tr>
                 <td><table width="268" border="0" cellspacing="0">
                   <tr bgcolor="#007ABD">
						<td colspan="3" class="texto_content">
							<input type="hidden" name="singleTrip" value="1">	
							<span class="Estilo25">
								{$L_Airport_To_Hotel}</span> {foreach name=MaxPassenger from=$MaxPassenger item=Passenger}
								<input type="hidden" name="Passengr_Id[]" value="{$Passenger.passenger_id}">
								<input type="hidden" name="MaxPassenger[]" value="{$Passenger.passenger_ending_range}">
								{/foreach}
							</span>
						</td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td width="112" class="texto_content"><div align="right">{$L_Destinations}:</div></td>
                     <td colspan="2" align="left">
						<input type="text" class="texto_content" value="{$dest_title}" name="dest_title" readonly>
						<input type="hidden" class="texto_content" name="dest_id" value="{$dest_id}" size="10" >					 
					 </td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content" align="left"><div align="right">{$L_No_Person}:</div></td>
                     <td colspan="2" align="left"><input type="text" class="texto_content" size="10" value="{$no_person}" name="no_person" onkeypress="javascript: isNumericKey(this.value)"></td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content" align="left"><div align="right">{$L_Hotel}:</div></td>
                     <td colspan="2" align="left"><input type="text" class="texto_content" size="15" value="{$hotelName}" name="hotelName"></td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_Date}:</div></td>
                     <td width="76" align="left"><input name="arrivalDate" type="text" class="texto_content" size="15" value="{$arrivalDate}" id="arrivalDate" readonly></td>
                     <td width="76">						<img align=absmiddle style='cursor:hand;vertical-align:top;' src='templates/images/popcalendar/calendarsm.gif' onclick='popUpCalendar(this, document.getElementById("arrivalDate"),"yyyy-mm-dd")' alt='select'> 						 </td>

                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_Time}:</div></td>
                     <td colspan="2" align="left"><SELECT class="texto_content" name="arrival_hour">
														  {$V_Arrival_Hour}
                                          </SELECT>
                                          <select class="texto_content" name="arrival_minute">
														  {$V_Arrival_Minute}
                                          </select>
                                          <SELECT class="texto_content" name="arrival_DayPart">
														  {$V_ArrivalDayPart}
                                          </SELECT>
					</td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_Airline}:</div></td>
                     <td colspan="2" align="left"><input name="arrivalAirline" value="{$arrivalAirline}" type="text" class="texto_content" size="15"></td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_Flight} #: </div></td>
                     <td colspan="2" align="left"><input name="arrivalFlight" value="{$arrivalFlight}" type="text" class="texto_content" size="15"></td>
                   </tr>
                 </table></td>
               </tr>
               <tr>
                 <td><table width="268" border="0" cellspacing="0">
                   <tr bgcolor="#007ABD">
                     <td colspan="3" class="texto_content">
						{if $TriptypeId == 2}
							<input type="hidden" name="roundTrip" value="2">	
						{else}
							<input type="hidden" name="roundTrip" value="0">							
						{/if} 

						 <span class="Estilo25">{$L_Hotel_To_Airport}</span>
					 </td>
                   </tr>
				   
										  				   
                   <tr bgcolor="#EBEBEB">
                     <td width="112" class="texto_content"><div align="right">{$L_Origin}:</div></td>
                     <td colspan="2" align="left">
						<input type="text" class="texto_content" value="{$dest_title}" name="dest_title" readonly>
						<input type="hidden" class="texto_content" name="departureDestId" value="{$dest_id}" size="10" >
					 </td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_No_Person}:</div></td>
                     <td colspan="2" align="left"><input type="text" class="texto_content" size="10" value="{$departureTotalPerson}" name="departureTotalPerson" onkeypress="javascript: isNumericKey(this.value)"></td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_Hotel}:</div></td>
                     <td colspan="2" align="left"><input type="text" class="texto_content" size="15" value="{$departureHotelName}" name="departureHotelName"></td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_Date}:</div></td>
                     <td width="76" align="left"><input name="departureDate" value="{$departureDate}" id="departureDate" type="text" class="texto_content" size="15" readonly></td>
                     <td width="76">                                <img align=absmiddle style='cursor:hand;vertical-align:top;' src='templates/images/popcalendar/calendarsm.gif' onclick='popUpCalendar(this, document.getElementById("departureDate"),"yyyy-mm-dd")' alt='select'> </td>

                   </tr>
				   
				   		 <tr bgcolor="#EBEBEB">
                                <td class="texto_content"><div align="right">{$L_Pickup_Time}:</div></td>
                                <td colspan="2" align="left">
                                  <SELECT class="texto_content" name="pickup_hour">
									{$V_Pickup_Hour}
							</SELECT>
                                  <SELECT class="texto_content" name="pickup_minute">
									{$V_Pickup_Minute}
							</SELECT>
                                <SELECT class="texto_content" name="pickup_DayPart">
									{$V_Pickup_DayPart}
							</SELECT>                         </td>
                              </tr>

				   
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_Departure_Time}:</div></td>
                     <td colspan="2" align="left">
							<SELECT class="texto_content" name="departure_hour">
									{$V_Departure_Hour}
							</SELECT>
							<SELECT class="texto_content" name="departure_minute">
									{$V_Departure_Minute}
							</SELECT>
							<SELECT class="texto_content" name="departure_DayPart">
									{$V_DepartureDayPart}
							</SELECT>
					 </td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_Airline}:</div></td>
                     <td colspan="2" align="left"><input name="departureAirline" value="{$departureAirline}" type="text" class="texto_content" size="15"></td>
                   </tr>
                   <tr bgcolor="#EBEBEB">
                     <td class="texto_content"><div align="right">{$L_Flight} #: </div></td>
                     <td colspan="2" align="left"><input name="departureFlight" value="{$departureFlight}" type="text" class="texto_content" size="15"></td>
                   </tr>
                 </table></td>
               </tr>
			   <tr>
                 <td><input type="image" onclick="javascript: return Form_Submit(frmReserve);" src="{$Templates_Image}{$site_res_img}"></td>
               </tr>
            </table>
             </td>
       </tr>
		<tr>
		  <td colspan="2" valign="top" align="center">
			<input type="hidden" name="Submit" value="{$SUBMIT}">
			<input type="hidden" name="Action" value="{$ACTION}">
			<input type="hidden" name="status">
			<input type="hidden" name="carttranId" value="{$carttranId}">
			<input type="hidden" name="itemType" value="2">
			<input type="hidden" name="rate" value="{$Rate}">
			<input type="hidden" name="passenger_id" value="{$PassengerId}">
			<input type="hidden" name="triptype_id" value="{$TriptypeId}">
		  </td>
		</tr>
   
     </table>	  
	  
	  </td>
    </tr>
  </FORM>
  <script language="javascript">
  	if(document.frmReserve.singleTrip.value == 1)
	{ldelim}
			if(document.frmReserve.roundTrip.value != 2)
			{ldelim}
				document.frmReserve.departureTotalPerson.disabled	=  true; 
				document.frmReserve.departureHotelName.disabled		=  true;					
				document.frmReserve.departureDate.disabled			=  true; 
				
/*				document.frmReserve.pickup_hour.disabled			=  true; 
				document.frmReserve.pickup_minute.disabled			=  true; 
				document.frmReserve.pickup_DayPart.disabled			=  true; 
*/
				document.frmReserve.departure_hour.disabled			=  true; 
				document.frmReserve.departure_minute.disabled		=  true; 
				document.frmReserve.departure_DayPart.disabled		=  true; 

				document.frmReserve.departureAirline.disabled		=  true; 
				document.frmReserve.departureFlight.disabled		=  true; 				
			{rdelim}
	{rdelim}
  </script>
</table>
