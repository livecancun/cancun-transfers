<script language="JavaScript">
	var Valid_Delete_Cart	= '{$Valid_Delete_Cart}';
	var Confirm_Delete 		= '{$Confirm_Delete}';
	var Valid_Select_Cart 	= '{$Valid_Select_Cart}';
</script>


<FORM name="frmCart" action="{$A_Cart}" method="POST">
<table width="100%" border="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
	</tr>
    <tr>
      <td valign="top"><div align="justify">
          <table width="92%" border="0" align="center" cellspacing="0">
			<tr>
				<td valign="top" class="blueHeader" width="100%" colspan="3">
					<table width="100%">
		            {foreach name=CartInfo from=$CartInfo item=Cart}	
					{if $smarty.foreach.CartInfo.first}
					  <tr>
						<td class="blueHeader" width="70%"><span class="texto_content"><strong>{$L_Reservation_Details}<br><br>{$L_ConfirmationNo} : </strong>{$Cart.confirmationNo}</span></td>
						<td class="blueHeader" width="30%" align="right"><br><span class="texto_content"><strong>{$L_status} : </strong>{$Cart.cartStatus}</span></td>
					   </tr>	
					{/if}  
					{/foreach}
					
					{foreach name=CartDetails from=$CartDetails item=CartDet}	
					{if $smarty.foreach.CartDetails.first}
					   <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Total Persons : </strong>{$CartDet.total_person}</span></td>
					   </tr>
					   <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Number of Adults : </strong>{$CartDet.tres_no_adult}</span></td>
					   </tr>	
						  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Number of Kids : </strong>{$CartDet.tres_no_kid}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Hotel Name : </strong>{$CartDet.hotelName}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Arrival Date : </strong>{$CartDet.arrivalDate}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Arrival Time : </strong>{$CartDet.arrivalTime}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Arrival Airline : </strong>{$CartDet.arrivalAirline}</span></td>
					   </tr>
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Arrival Flight : </strong>{$CartDet.arrivalFlight}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Total Person: </strong>{$CartDet.departureTotalPerson}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Hotel : </strong>{$CartDet.departureHotelName}</span></td>
					   </tr>
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Pick up time : </strong>{$CartDet.pickupTime}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Date : </strong>{$CartDet.departureDate}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Time : </strong>{$CartDet.departureTime}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Airline : </strong>{$CartDet.departureAirline}</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Flight : </strong>{$CartDet.departureFlight}</span></td>
					   </tr>	
					{/if}  
					{/foreach}
					
					

					</table>
					<br>
				</td>		
			</tr>
					  
            <tr>
              <td colspan="3"class="successMsg" align="center">{$Message}</td>
            <tr bgcolor="#007ABD">
              <td width="35"><div align="center">
                  <input type="checkbox" name="chkAll" onclick="javascript: return checkAll(document.frmCart, this.checked);">
                </div></td>
              <td width="60%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Requested_Service}</span></span></div></td>
<!--              <td width="20%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_ConfirmationNo}</span></span></div></td>
              <td width="20%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_status}</span></span></div></td>
-->              <td width="40%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Rate}</span></span></div></td>
            </tr>
            <tr>
              <td class="successMsg" colspan="3" align="center">{$Notfound_Message}</td>
            </tr>

            {foreach name=CartInfo from=$CartInfo item=Cart}
            {assign var='TotalPrice' value=`$TotalPrice+$Cart.totalCharge`}
            <tr bgcolor="#EBEBEB">
              <td><div align="center"><span class="texto_content">
                  <input type='checkbox' name="chkCartId[]" value="{$Cart.cart_id}">
                  </span></div></td>
              <td><div align="center" class="texto_content">{$Cart.dest_title}</div></td>
<!--              <td><div align="center" class="texto_content">{$Cart.confirmationNo}</div></td>
              <td><div align="center" class="texto_content">{$Cart.cartStatus}</div></td>
-->              <td><div align="center"><span class="texto_content">${$Cart.totalCharge|number_format:2} {$Currency}</span></div></td>
            </tr>
            {/foreach}
            <tr>
              <td><div align="center"><span class="texto_content"> </span></div></td>
              <td><div align="right"><span class="texto_content"><strong>{$L_Total}</strong></span></div></td>
              <td><div align="center"><span class="texto_content"><strong>${$TotalPrice|number_format:2} {$Currency}</strong></span></div>
                <input type="hidden" name="cartId">
                <input type="hidden" name="itemType">
              </td>
            </tr>
            {if $is_airline_empl == 1}
            <tr>
              <td>&nbsp;</td>
              <td><div align="right"><span class="texto_content">
			  	<strong>{$L_Discount} {$discount_rate}% </strong>
				{assign var='discount' value=`$discount_rate`}
				</span></div>
			  </td>
              <td><div align="center"><span class="texto_content"><strong>- $ {math equation="(x * y)/100" x=$TotalPrice y=$discount format="%.2f"}</strong></span></div></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><div align="right"><span class="texto_content"><strong>{$L_Final} {$L_Total}</strong></span></div></td>
              <td><div align="center"><span class="texto_content"><strong>$ {math equation="(x-((x * y)/100))" x=$TotalPrice y=$discount format="%.2f"}</strong></span></div></td>
            </tr>
            {/if}
          </table>
          <table width="92%" border="0" align="center" cellspacing="0">
            <tr>
              <td align="center" colspan="2">
<!--			  	<input type="button" Value="{$Delete}" Name="Submit" class="texto_content" onClick="javascript : return Form_Delete(frmCart);">
-->                <input type="hidden" name="index_id" value="{$index_id}">
                <input type="hidden" name="cart_id" value="{$cart_id}">
                <input type="hidden" name="dest_id" value="{$dest_id}">
				<input type="hidden" name="cartId"> 
				<input type="hidden" name="itemType"> 
                <input type="hidden" name="Action" value="{$ACTION}">
              </td>
            </tr>
			<br>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
			
			 <tr>
			 	<td colspan="2" align="center">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr><td colspan="3" align="left"><span class="texto_content"><b>{$L_status} {$L_Note} :</b></span></td></tr>
						<tr><td colspan="3" align="left">&nbsp;</td></tr>						
						<tr>
							<td align="left"><span class="texto_content"><b>PR</b> - {$Note_PR}</span></td>
							<td align="left"><span class="texto_content"><b>AR</b> - {$Note_AR}</span></td>
							<td align="left"><span class="texto_content"><b>NA</b> - {$Note_NA}</span></td>
						</tr>
						<tr>
							<td align="left"><span class="texto_content"><b>RP</b> - {$Note_RP}</span></td>
							<td align="left"><span class="texto_content"><b>PD</b> - {$Note_PD}</span></td>
							<td>&nbsp;</td>
						</tr>
					</table>	
				</td>
			 </tr>
			<tr>
				<td>
				</td>
			</tr>
          </table>
		  
        </div></td>
    </tr>
       </table>
</form>	   
