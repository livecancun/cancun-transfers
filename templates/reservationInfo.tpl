<script>
	var Valid_Select_Cart	= '{$Valid_Select_Cart}';
	var Valid_Delete_Cart  	= '{$Valid_Delete_Cart}';
	var Confirm_Delete 		= '{$Confirm_Delete}';

	var Empty_First_Name	= '{$Empty_First_Name}';
	var Empty_Last_Name  	= '{$Empty_Last_Name}';
	var Empty_Email 		= '{$Empty_Email}';
	var Valid_Email 		= '{$Valid_Email}';
	var Empty_Address 		= '{$Empty_Address}';
	var Empty_Country 		= '{$Empty_Country}';
	var Empty_City 			= '{$Empty_City}';
	var Empty_State 		= '{$Empty_State}';
	var Empty_Zip 			= '{$Empty_Zip}';
	var Valid_Zip 			= '{$Valid_Zip}';			
	var Empty_Area_Code 	= '{$Empty_Area_Code}';
	var Empty_City_Code 	= '{$Empty_City_Code}';			
	var Empty_Phone_No 		= '{$Empty_Phone_No}';
	var Valid_PhoneNo 		= '{$Valid_PhoneNo}';			
</script>

<br /><br />
<table>
<tr>
<td>
{if $lng == "en"}
<img src="images/step-3.jpg" /></td><td><h2>Checkout form for Payment process</h2></td>
{else}
<img src="images/step-3.jpg" /></td><td><h2>Datos para proceso de pago</h2></td>
{/if}
</tr></table>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" height="100%" border="0" cellspacing="0">
					<tr><td><table width="100%" cellpadding="0" cellspacing="0">

					<tr>
						<td valign="top">
							  <table width="92%" border="0" cellspacing="0">
								<tr>
								  <td valign="top"><div align="justify">
										<p class="Estilo10"><span class="texto_content">{$L_Res_Info}
                                        {if $lng == "en"}
Please fill required data
{else}
Porfavor llene los datos requeridos
{/if}
                                        </span></p>
										<table width="100%" border="0" align="center" cellspacing="0">
										  <FORM name="frmCart" action="{$A_Cart}" method="POST">
											<tr bgcolor="#007ABD">
											  <td width="5%"><div align="center"></div></td>
											  <td width="60%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Requested_Service}</span></span></div></td>
											  <td width="25%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Rate}</span></span></div></td>
											</tr>
											{foreach name=CartInfo from=$CartInfo item=Cart}
											{assign var='TotalPrice' value=`$TotalPrice+$Cart.totalCharge`}
											<tr bgcolor="#EBEBEB">
											  <td><div align="center"><span class="texto_content">
												  <input name="checkbox" type="checkbox" value="checkbox">
												  </span></div></td>
											  <td><div align="center" class="texto_content">{$Cart.dest_title}    </div></td>
											  <td><div align="center"><span class="texto_content">${$Cart.totalCharge} {$Currency}</span></div></td>
											</tr>
											{/foreach}
										  </FORM>
										  <tr>
											<td><div align="center"><span class="texto_content"> </span></div></td>
											<td><div align="right"><span class="texto_content"><strong>{$L_Total}</strong></span></div></td>
											<td><div align="center"><span class="texto_content"><strong>$ {$TotalPrice|string_format:"%.2f"} {$Currency}</strong></span></div></td>
										  </tr>
										  {if $is_airline_empl == 1}
										  <tr>
											<td>&nbsp;</td>
											<td>
												<div align="right"><span class="texto_content"><strong>{$L_Discount} {$discount_rate}% 
												{assign var='discount' value=`$discount_rate`}
												</strong></span></div>
											</td>
											<td><div align="center"><span class="texto_content"><strong>- $ {math equation="(x * y)/100" x=$TotalPrice y=$discount format="%.2f"}</strong></span></div></td>
										  </tr>
										  <tr>
											<td>&nbsp;</td>
											<td><div align="right"><span class="texto_content"><strong>{$L_Final} {$L_Total}</strong></span></div></td>
											<td><div align="center"><span class="texto_content"><strong>$ {math equation="(x-((x * y)/100))" x=$TotalPrice y=$discount format="%.2f"}</strong></span></div></td>
										  </tr>
										  {/if}
										</table>
										<div align="left"><br>
										  <table width="100%" border="0" align="center" cellspacing="0">
											<form name="frmReservation" action="{$A_Reservation}" method="post">
											  <tr bgcolor="#007ABD">
												<td colspan="2"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Personal_Information} </span></span></div></td>
											  </tr>
											  {if $is_airline_empl==1}
											  <tr bgcolor="#EBEBEB">
												<td width="30%"><div align="right"><span class="texto_content"> {$L_Applicable_Discount}:</span></div></td>
												<td width="70%" class="texto_content">{$discount_title}</td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td><div align="right"><span class="texto_content">{$L_Company_Name}:</span></div></td>
												<td><span class="texto_content">{$empl_airline} </span></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td><div align="right"><span class="texto_content">{$L_Company_ID}:</span></div></td>
												<td><span class="texto_content">{$empl_id}</span></td>
											  </tr>
											  {/if}
											  <tr bgcolor="#EBEBEB">
												<td height="20" class="texto_content"><div align="right">{$L_First_Name}:</div></td>
												<td><input type="text" name="firstname" value="{$Firstname}" class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right">{$L_Last_Name}:</div></td>
												<td><input type="text" name="lastname" value="{$Lastname}"class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right">{$L_Email}:</div></td>
												<td><input type="text" name="email" value="{$Email}" class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right">{$L_Address}:</div></td>
												<td><textarea name="address1" rows="3" cols="30" class="texto_content">{$Address1}</textarea>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right">{$L_Country}:</div></td>
												<td><SELECT name="country" class="texto_content"><option value="0">-----------{$L_Select_Country}--------</option>{$CountryList}</SELECT>
												</td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right">{$L_City}:</div></td>
												<td><input name="city" value="{$City}" type="text" class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right">{$L_State}:</div></td>
												<td><input type="text" name="state" value="{$State}" class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right">{$L_Zip}:</div></td>
												<td><input type="text" name="zipcode" value="{$Zip}" class="texto_content" size="10" maxlength="5"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right">{$L_Phone_Number}:</div></td>
												<td><input name="phone_areacode" value="{$Phone_Areacode}" maxlength="3" type="text" class="texto_content" size="10" onKeyPress="javascript: isNumericKey(this.value)">
												  <input name="phone_citycode" value="{$Phone_Citycode}" maxlength="3" type="text" class="texto_content" size="10" onKeyPress="javascript: isNumericKey(this.value)">
												  <input name="phone_no" value="{$Phone_No}" type="text" class="texto_content" maxlength="6" size="10" onKeyPress="javascript: isNumericKey(this.value)"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td height="29" class="texto_content"><div align="right"></div></td>
												<td valign="top"><input type="submit" name="Submit" value="{$Submit}" class="texto_content" onClick="javascript: return Form_ResSubmit(frmReservation);">
												  &nbsp;&nbsp;
												  <input type="hidden" name="Action" value="{$ACTION}">
												  <input type="hidden" name="index_id" value="{$index_id}">
												  <input type="hidden" name="cart_id" value="{$cart_id}">
												  <input type="hidden" name="dest_id" value="{$dest_id}">
												  <input type="hidden" name="V_grandTotal" value="{$V_grandTotal}">				  
												  
												  {section name=Cart loop=$chk_cart_id}
													<input type="hidden" name="chkCartId[]" value="{$chk_cart_id[Cart]}">
												  {/section} 
												  
												  </td>
											  </tr>
											</form>
										  </table>
											</div>
									  </div></td>
								</tr>
							  </table>
						</td>
					</tr>
					
					</table>
					
					</td>
					</tr>

				  </table>
			</td>
		</tr>
</table>
