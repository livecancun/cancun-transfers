
{if $lng == "en"}
                    
<br /><img src="images/cancun-transfers-payment.jpg" alt="Payments methods" /><br/>
<img src="images/pay-online.png"><br><br>
<p>
We offer <strong>transportation service</strong> where travel comfortable clean units, with air conditioning, insured, arrive fast and without making ranks at more accessible prices of the market, <strong>Trip to Cancun, Playa del Carmen or the Riviera maya</strong> in a first transportation service upon arrival we will be waiting at the airport and his departure will go through you to your hotel at the time indicated.<br /><br />
Below you will find a list of prices and destinations, choose your destination and <strong>pay online the easiest way</strong>, or if you wish to <strong>pay directly on arrival. <a href="reserve.html">Please fill this Easy form</a></strong>
<br />
<a href="reserve.html"><img src="images/pay-airport.png" border="0"></a><br />
<br />
Thank you for booking with Cancun-transfers.com, if you can't find the place of destination, please contact us, with pleasure will help you out.<br />

</p>
{else}


<br /><br />
<img src="images/pago-online.png" /><br />
<img src="images/cancun-transfers-payment-es.jpg" alt="Metodos de pago" /><br />
<p>
Ofrecemos <strong>servicio de transportacion</strong> en donde viajara comodo en  unidades limpias, con aire acondicionado, aseguradas, llegue rapido y sin hacer filas a los precios mas accesibles del mercado, Viaje a <strong>Cancun, Playa del Carmen o a la Riviera maya</strong> en un servicio de transportacion de primera, a su llegada estaremos esperandolo en el aeropuerto y a su partida pasaremos por usted a su hotel a la hora indicada.<br /><br />
A continuacion encontrara una lista de precios y destinos, elija su destino y <strong>pague en linea de la manera mas sencilla</strong>, o si lo desea <strong>pague directamente a su llegada <a href="es/reservar.html">llenando este formulario sencillo</strong></a>. <br /><a href="es/reservar.html"><img src="images/pago-aeropuerto.png" border="0" /></a><br /><br />
Gracias por reservar con Cancun-transfers.com, la siguiente lista contiene los destinos y hoteles para reservar en linea, si su hotel no aparece en el listado contactenos que con gusto le ayudaremos.<br />

</p>
{/if}


<table class="table1" border="0" cellspacing="1" cellpadding="0">
<FORM name="frmRates" action="{$A_Action}" method="post">
<thead>
                    <tr>
                        <th scope="col" abbr="Starter">{$L_Destinations}</th>
                        {foreach name=PassengerRange from=$PassengerRange item=Passenger}
									{assign var=PsngrId value=$Passenger.passenger_id}
                        <th scope="col" abbr="Starter"><strong>{$Passenger.passenger_starting_range} {$L_To}
																	{$Passenger.passenger_ending_range} Pax</strong></th>

{/foreach}              <th scope="col" abbr="Deluxe"><img src="images/save-transfers.png" /> Save </th>
                    </tr>
                </thead>
                <tfoot>

                </tfoot>
                <tbody>
                    <tr>
                        {foreach name=DestinationInfo from=$DestinationInfo item=Destination}
                        <th scope="row">{$Destination.dest_title}&nbsp;&nbsp;</th>
                        <!--Loop TD -->
                        {foreach name=R_RecordSet from=$R_RecordSet item=Record}
								{assign var=PriceData value='price_data'|array_value:$Record}
                        <!--Inicio generacion TD -->
                        <td><table class="table1" border="0" cellspacing="0" cellpadding="0">
											<tr>
											 {foreach name=TripTypeInfo from=$TripTypeInfo item=TripType}
													
												<td>
													
														<strong>{$Record.price_data[$Destination.dest_id][$TripType.triptype_id]}</strong>
													
                                                    {if $TripType.triptype_id != 1}
							<br /><input name="reserve" type="image" onclick="JavaScript: Reserve_Click(document.frmRates,{$PsngrId},{$TripType.triptype_id}, {$Destination.dest_id},{$Record.price_data[$Destination.dest_id][$TripType.triptype_id]} );" class="texto_content" src="images/round-trip-transfer.png" value="{$L_Reserve}">
						{else}
						<br /><input name="reserve" type="image" onclick="JavaScript: Reserve_Click(document.frmRates,{$PsngrId},{$TripType.triptype_id}, {$Destination.dest_id},{$Record.price_data[$Destination.dest_id][$TripType.triptype_id]} );" class="texto_content" src="images/one-way-transfer.png" value="{$L_Reserve}">{/if} 
                            
                                                    
                                                    
                                                    
													
												
												</td>
				
											  {/foreach}
													  
											</tr>
									   </table></td>
                        <!--Fin Generacion TD -->
                         {/foreach}
 <!--TD para descuento sin loop -->
                        <td>20%</td>
<!--Fin TD para descuento sin loop -->
                    </tr>
                    {/foreach}<!--Foreach de destinations y geneacion de todos xxxxxx -->
                   
<input type="hidden" name="Action" value="{$ACTION}">
				<input type="hidden" name="rate">
				<input type="hidden" name="passenger_id">
				<input type="hidden" name="triptype_id">
				<input type="hidden" name="dest_id">
				<input type="hidden" name="rate_id">		
                </tbody>
                </FORM>
            </table>