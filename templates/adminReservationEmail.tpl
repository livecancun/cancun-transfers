<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>:::Welcome to Cancun-Transfers.com:::</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
	<link href="{$cssSiteroot}/style.css" rel="stylesheet" type="text/css">
<body>
<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='FFFFFF'>
					      <tr>							  
								 <td>&nbsp;</td>
						  </tr>
						  <tr>
								<td valign='top' width='100%'>
										<font size='2' face='Verdana, Arial, Helvetica,sans-serif'>Dear Administrator,<br></font>
										    <font face='Verdana, Arial, Helvetica, sans-serif' size='2'>
											 <br>
											 <br>
											   Welcome to Cancun-Transfers.com,<br>
											   <br><br>
											   Reservation Details of <font color='#184DA5'><B>{$name}</B></font><br>
											   Confirmation No : <font color='#184DA5'><B>{$Confirmation_No}</B></font>
												<hr><br>
								  </font> 
											   <table border='0' cellpadding='2' cellspacing='2' width='70%' align="center">
												<tr>
												  <td colspan='2' align='left' bgcolor="#007ABD"><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b>&nbsp;{$L_Reservation_Details}</b></font></td>
												</tr>	
												<tr>
													<td  align='center' width='40%' bgcolor='#007ABD'><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b>{$L_cartItem}</b></font></td>
<!--													<td  align='center' width='40%' bgcolor='#184DA5'><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b>{$L_ItemType}</b></font></td>-->
													<td  align='center' width='20%' bgcolor='#007ABD'><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b>{$L_Total}</b></font></td>
												</tr>											
												{foreach name=ShowRequest from=$ShowRequest item=Record}
												{assign var='TotalPrice' value=`$TotalPrice+$Record.totalCharge`}
												<tr>
													<td align='center' height='25' bgcolor='#EEEEEE' ><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'>{$Record.dest_title}</font> </td>
<!--													<td align='center' bgcolor='#EEEEEE'><font size='2' face='Verdana, Arial, Helvetica, sans-serif' color='#000000'>{$Record.itemType}</font> </td>-->
													<td align='center' bgcolor='#EEEEEE'><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'>$ {$Record.totalCharge}</font></td>
											  	</tr>	
											  	{/foreach}
												<tr>
													<td colspan='1' align='right'><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'>{$L_Total} ($):</font></td>
													<td  align='center'>&nbsp;&nbsp;<font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'>$ {$TotalPrice|string_format:"%.2f"}</font></td>
												</tr>
												{if $is_airline_empl == 1}
												<tr>
												  <td><div align="right"><span class="texto_content">
														<font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'>
														<strong>{$L_Discount} {$discount_rate}% </strong>
														</font>
														{assign var='discount' value=`$discount_rate`}
														</span></div>
												  </td>
												  <td><div align="center"><span class="texto_content"><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'><strong>- $ {math equation="(x * y)/100" x=$TotalPrice y=$discount format="%.2f"}</strong></font></span></div></td>
												</tr>
												<tr>
												  <td><div align="right"><span class="texto_content"><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'><strong>{$L_Final} {$L_Total}</strong></font></span></div></td>
												  <td><div align="center"><span class="texto_content"><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'><strong>$ {math equation="(x-((x * y)/100))" x=$TotalPrice y=$discount format="%.2f"}</strong></font></span></div></td>
												</tr>
												{/if}
												
												<tr><td colspan='2'>&nbsp;</td></tr>
											</table>
											<hr><br>								  <font face='Verdana, Arial, Helvetica, sans-serif' size='2'><br>
														  Thank You,<br><br>
														  Regards,<br>
														  Cancun-Transfers Team.
										  </font>
										  <font size='2' face='Verdana, Arial, Helvetica, sans-serif'><br>
										  </font> 
								</td>
						   </tr>
				   <tr><td>&nbsp;</td></tr>
		 </table>
</body>
</html>
