<script>
	var Select_Airline_Employee	= '{$Select_Airline_Employee}';
	var Empty_ID				= '{$Empty_ID}';
	var Empty_Airline_Name  	= '{$Empty_Airline_Name}';
	var Valid_Select_Cart		= '{$Valid_Select_Cart}';
	var Valid_Delete_Cart  		= '{$Valid_Delete_Cart}';
	var Confirm_Delete 			= '{$Confirm_Delete}';
</script>


<table border="0" cellpadding="1" cellspacing="1" width="100%">
<FORM name="frmCart" action="{$A_Cart}" method="POST">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="92%" height="100%" border="0" cellspacing="0">
					<tr>
						<td>  
						<table width="100%" border="0" cellspacing="0">
							<tr>
							  <td valign="top"><div align="justify">
								  {$L_Emp_Msg}
								  <table width="100%" border="0" align="center" cellspacing="0">
									<tr>
									  <td colspan="3"class="successMsg">{$Message}</td>
									<tr bgcolor="#007ABD">
									  <td><div align="center">
										  <input type="checkbox" name="chkAll" onclick="javascript: return checkAll(document.frmCart, this.checked);">
										</div></td>
									  <td width="60%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Requested_Service}</span></span></div></td>
									  <td width="40%"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Rate}</span></span></div></td>
									</tr>
									<tr>
									  <td class="successMsg" colspan="3" align="center">{$Notfound_Message}</td>
									</tr>
									{foreach name=CartInfo from=$CartInfo item=Cart}
									{assign var='TotalPrice' value=`$TotalPrice+$Cart.totalCharge`}
									<tr bgcolor="#EBEBEB">
									  <td><div align="center"><span class="texto_content">
										  <input type='checkbox' name="chkCartId[]" value="{$Cart.cart_id}">
										  </span></div></td>
									  <td><div align="center" class="texto_content">{$Cart.dest_title}</div></td>
									  <td><div align="center"><span class="texto_content">$ {$Cart.totalCharge} {$Currency}</span></div></td>
									</tr>
									{/foreach}
									<tr>
									  <td><div align="center"><span class="texto_content"> </span></div></td>
									  <td><div align="right"><span class="texto_content"><strong>{$L_Total}</strong></span></div></td>
									  <td><div align="center"><span class="texto_content"><strong>$  {$TotalPrice|string_format:"%.2f"}
										 {$Currency}</strong></span></div>
										<input type="hidden" name="cartId">
										<input type="hidden" name="itemType">
									  </td>
									</tr>
									{if $is_airline_empl == 1}
									<tr>
									  <td>&nbsp;</td>
									  <td><div align="right"><span class="texto_content">
										<strong>{$L_Discount} {$discount_rate}% </strong>
										{assign var='discount' value=`$discount_rate`}
										</span></div>
									  </td>
									  <td><div align="center"><span class="texto_content"><strong>- $ {math equation="(x * y)/100" x=$TotalPrice y=$discount format="%.2f"}</strong></span></div></td>
									</tr>
									<tr>
									  <td>&nbsp;</td>
									  <td><div align="right"><span class="texto_content"><strong>{$L_Final} {$L_Total}</strong></span></div></td>
									  <td><div align="center"><span class="texto_content"><strong>$ {math equation="(x-((x * y)/100))" x=$TotalPrice y=$discount format="%.2f"}</strong></span></div></td>
									</tr>
									{/if}
								  </table>
								  <br>
								  <table width="100%" border="0" align="center" cellspacing="0">
									<tr>
									  <td>&nbsp;</td>
									  <td>&nbsp;</td>
									  <td>&nbsp;</td>
									</tr>
									{if $is_airline_empl == 0}
									<tr>
									  <td colspan="3"><!--div id="CHILD0" style="display:block">
										  <p align="center">
										  <div align="center"><span class="texto_content"> <a href="#" onClick="layerSwitch(1,1); return false;">{$L_Is_Airline_Employee}</a> </span></div>
										  </p>
										</div-->
										<div class="pb10" id="CHILD1">
										  <!--p align="center">
										  <div align="center"><span class="texto_content"> <a href="#" onClick="layerSwitch(1,0); return false;">{$L_Is_Airline_Employee}</a> </span></div>
										  </p-->
										  <table width="100%" cellpadding="0" cellspacing="0">
											<tr>
											  <td colspan="2" bgcolor="#007ABD" height="20"><div align="center"><span class="texto_content"><span class="Estilo25">{$L_Additional_Discounts}</span></span></div></td>
											</tr>
											<tr bgcolor="#EBEBEB">
											  <td width="40%"><div align="right"><span class="texto_content">{$L_Airline_Employee} : </span></div></td>
											  <td width="60%"><select name="discount_id" class="texto_content">
												  
																{$Airline_Empl_List}
															
												</select>
											  </td>
											</tr>
											<tr bgcolor="#EBEBEB">
											  <td><div align="right"><span class="texto_content">{$L_ID}#: </span></div></td>
											  <td><input name="empl_id" value="{$empl_id}" type="text" class="texto_content"></td>
											</tr>
											<tr bgcolor="#EBEBEB">
											  <td><div align="right"><span class="texto_content">{$L_Airline_Name}: </span></div></td>
											  <td><input name="empl_airline" value="{$empl_airline}" type="text" class="texto_content"></td>
											</tr>
											<tr bgcolor="#EBEBEB">
												<td><div align="right"><span class="texto_content">{$L_Comments}: </span></div></td>
												<td><textarea name="empl_comments" rows="4" cols="40" style="vertical-align:top" class="texto_content">{$empl_comments}</textarea></td>
											</tr>
											<tr bgcolor="#EBEBEB">
											  <td>&nbsp;</td>
											  <td align="left">
											  <input type="submit" name="Save" value="{$Submit}" class="texto_content" onclick="javascript: return Form_Submit(frmCart);">
												   </td>
											</tr>
											<tr>
											  <td>&nbsp;</td>
											  <td>&nbsp;</td>
											</tr>
										  </table>
										</div></td>
									</tr>
									{/if}
						
									<tr>
									  <td>&nbsp;</td>
									  <td align="center"><input type="button" Value="{$Delete}" Name="Submit" class="texto_content" onclick="javascript : return Form_Delete(frmCart);">
										<input type="hidden" name="index_id" value="{$index_id}">
										<input type="hidden" name="cart_id" value="{$cart_id}">
										<input type="hidden" name="dest_id" value="{$dest_id}">
										<input type="hidden" name="Action" value="{$ACTION}">
										<input type="button" value="{$L_ReserveMore}" class="texto_content" onclick="javascript: location.href('{$ReserveMoreLink}');">
										  
										<input type="button" value="{$L_Checkout}" class="texto_content" onclick="javascript : return Form_Checkout(frmCart,'{$index_id}');">
									  </td>
									</tr>
								  </table>
								</div></td>
							</tr>
						  </table>
					
					</td>
					</tr>

				  </table>
			</td>
		</tr>
	</FORM>
</table>
