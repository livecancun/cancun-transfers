function Form_ResSubmit(frm)
{
	with(frm)
    {
/*		if(frm.airline_emp.checked)
		{
			if(!IsSelected(discount_id, Select_Airline_Employee))
			{
				return false;
			}			
			if(!IsEmpty(empl_id, Empty_ID))
			{
				return false;
			}
			if(!IsEmpty(empl_airline, Empty_Airline_Name))
			{
				return false;
			}
		}
*/
		if(!IsEmpty(firstname, Empty_First_Name))
        {
			return false;
        }
    	if(!IsEmpty(lastname, Empty_Last_Name))
        {
			return false;
        }
		if(!IsEmpty(email, Empty_Email))
        {
	        return false;    
		}
		if(!IsEmail(email, Valid_Email))
        {
			return false;
	    }
    	if(!IsEmpty(address1, Empty_Address))
        {
			return false;
        }
		if(!IsSelected(country, Empty_Country))
        {
			return false;
        }
    	if(!IsEmpty(city, Empty_City))
        {
			return false;
        }
		if(!IsEmpty(state, Empty_State))
        {
			return false;
        }
    	if(!IsEmpty(zipcode, Empty_Zip))
        {
			return false;
        }
/*		else if(!IsZip(zipcode, Valid_Zip))
        {
			return false;
        }
*/    	if(!IsEmpty(phone_areacode, Empty_Area_Code))
		{
				return false;
		}
    	if(!IsEmpty(phone_citycode, Empty_City_Code))
		{
				return false;
		}
    	if(!IsEmpty(phone_no, Empty_Phone_No))
		{
				return false;
		}
		if(!IsPhone(phone_areacode,phone_citycode,phone_no, Valid_PhoneNo))
		{
			return false;
		}
	
	}
	  return true;
}			
