//====================================================================================================
//	File Name		:	validate.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Client side validation in JavaScript.
//====================================================================================================

var ie4=document.all&&navigator.userAgent.indexOf("Opera")==-1;
var ns6=document.getElementById&&navigator.userAgent.indexOf("Opera")==-1;
var ns4=document.layers;
var ffox=!(navigator.userAgent.indexOf("Firefox")==-1);

//====================================================================================================
//	Function Name	:	IsEmpty
//----------------------------------------------------------------------------------------------------
function IsEmpty(fld,msg)
{
	if((fld.value == "" || fld.value.length == 0) && (msg == ''))
	{
		return false;
	}
	if(fld.value == "" || fld.value.length == 0)
	{
		alert(msg);
		try {fld.focus();}catch(e){return true;}
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsSelected
//----------------------------------------------------------------------------------------------------
function IsSelected(fld,msg)
{
	if((fld.value == "" || fld.value == 0))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsEmail
//----------------------------------------------------------------------------------------------------
function IsEmail(fld,msg)
{
	var regex = /^[\w]+(\.[\w]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/ ;
	if(!regex.test(fld.value))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsInt
//----------------------------------------------------------------------------------------------------
function IsInt(fld,msg)
{
	var regex = /^[0-9]*$/;
	if(!regex.test(fld.value))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}


//====================================================================================================
//	Function Name	:	IsFloat
//----------------------------------------------------------------------------------------------------
function IsFloat(fld,msg)
{
	var regex = /^[0-9.]*$/;
	if(!regex.test(fld.value))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsValidString
//----------------------------------------------------------------------------------------------------
function IsValidString(fld,msg)
{
	var regex = /^[_]*[a-zA-Z_]+[a-zA-Z0-9_]*$/;
	if(!regex.test(fld.value))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsPassword
//----------------------------------------------------------------------------------------------------
function IsPassword(fld,msg)
{
	var regex = /^[_]*[a-zA-Z]+[0-9]+[a-zA-Z0-9]*$/;
	if(!regex.test(fld.value))
  	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsLen
//----------------------------------------------------------------------------------------------------
function IsLen(fld, minlen, maxlen, msg)
{
	if(fld.value.length < minlen || fld.value.length > maxlen)
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsCurrency
//----------------------------------------------------------------------------------------------------
function IsCurrency(fld,msg)
{
    val = fld.value.replace(/\s/g, "");

	regex = /^\$?\d{1,3}(,?\d{3})*(\.\d{1,2})?$/;

    if(!regex.test(val)) {
         alert(msg);
		 fld.focus();
		 return false;
    }
	return true;
}
//====================================================================================================
//	Function Name	:	IsPhone
//	Purpose			:	checks if phone field has following characters : 0-9, '-', '+', '(' , ')' .
//						It returns false if there are other than above characters otherwise true .
//	Parameters		:	fld1	-  area code to be checked
//					:	fld2	-  city code to be checked
//					:	fld3	-  actual phone no to be checked
//					    msg -  error message to be displayed
//	Return			:	true or false
//	Author			:	Imtiyaz
//	Creation Date	:	23-Apr-2003
//----------------------------------------------------------------------------------------------------
function IsPhone(fld1,fld2,fld3,msg)
{
	var regex = /^[\d-+()]+$/;

	var phone = "(" + fld1.value + ")" + fld2.value + "-" + fld3.value;
	if(!regex.test(phone))
	{
		alert(msg);
		fld1.focus();
		return false;
	}
	return true;
}


function IsFax(fld,msg)
{
	var num = /^[\d]+$/;

	if(!num.test(fld.value))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}



//====================================================================================================
//	Function Name	:	IsZip
//----------------------------------------------------------------------------------------------------
function IsZip(fld,msg)
{
	var num = /^[\d]+$/;
	if(!num.test(fld.value) || (fld.value.length !=5 && fld.value.length !=6))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsValidFormat
//----------------------------------------------------------------------------------------------------
function IsValidFormat(fld, filelist, msg)
{
	var regex = new RegExp('(' + filelist.toLowerCase() + ')$');
	if(!regex.test(fld.value.toLowerCase()))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsUrl
//----------------------------------------------------------------------------------------------------
function IsUrl(fld,msg)
{
//	var regex = /^(http:\/\/)/;
	var regex = /^(http:\/\/|https:\/\/)/;
	if(!regex.test(fld.value))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	IsValidSize
//----------------------------------------------------------------------------------------------------
function IsValidSize(fld, msg)
{
	var regex = /^[0-9]*x[0-9]*$/i;
	
	if(!regex.test(fld.value))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

//====================================================================================================
//	Function Name	:	isNumericKeyDec
//----------------------------------------------------------------------------------------------------
function isNumericKeyDec()
{
	if (window.event.keyCode < 48 || window.event.keyCode > 57)
	{
		if (window.event.keyCode != 46)
			window.event.keyCode = 0;
	} 
}

//====================================================================================================
//	Function Name	:	isNumericKey
//----------------------------------------------------------------------------------------------------
function isNumericKey()
{
	if (window.event.keyCode < 48 || window.event.keyCode > 57)
	{
		window.event.keyCode = 0;
	} 
}

//====================================================================================================
//	Function Name	:	IsCheckBoxChecked
//----------------------------------------------------------------------------------------------------
function IsCheckBoxChecked(fld,msg)
{
	for(i=0 ; i<fld.length ; i++ )
		if(fld[i].checked)
			return true;
	alert(msg);
	return false;
}

//====================================================================================================
//	Function Name	:	UploadImage_Change
//----------------------------------------------------------------------------------------------------
function UploadImage_Change(obj, imgTag, defaultVal, defaultWidth)
{
	imgTag.width=120;

	if(obj.value == '')
		imgTag.src = defaultVal;
	else
	{
		imgTag.src = obj.value;
		if(defaultWidth != '')
			imgTag.width=defaultWidth;
	}
}

//====================================================================================================
//	Function Name	:	checkImageType
//----------------------------------------------------------------------------------------------------
function checkImageType(fld,msg)
{
	var regex = /(.jpg|.jpeg|.gif)$/;
	if(!regex.test(fld.value))
	{
		alert(msg);
		fld.focus();
		return false;
	}
	return true;
}

