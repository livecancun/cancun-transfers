//====================================================================================================
//	Function Name	:	RadioButtonCheck()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Parameters		:	PersonId  - Person Id whose detail will to be edit
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function  RadioButtonCheck(form) 
  {
    for (var i = 0; i < form.elements.length; i++) 
        {
          if (form.elements[i].type == "radio")
             {
                    if(form.elements[i].checked)
                       {
                         return  true;
                       }
                  }
          }
      return false;
}

//====================================================================================================
//	Function Name	:	Form_Submit()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Parameters		:	PersonId  - Person Id whose detail will to be edit
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Form_Submit(frm)
{
	with(frm)
    {
	if(!RadioButtonCheck(frm))
		{
			alert(Select_Payment_Method); 
			return false;
		}	
	 if(!IsEmpty(firstname, Empty_First_Name))
        {
			return false;
        }
    	if(!IsEmpty(lastname, Empty_Last_Name))
        {
			return false;
        }
		if(!IsEmpty(email,Empty_Email))
        {
	        return false;    
		}
		if(!IsEmail(email, Valid_Email))
        {
			return false;
	    }
    	if(!IsEmpty(address1, Empty_Address))
        {
			return false;
        }
		if(!IsEmpty(country, Empty_Country))
        {
			return false;
        }
    	if(!IsEmpty(city, Empty_City))
        {
			return false;
        }
		if(!IsEmpty(state, Empty_State))
        {
			return false;
        }
    	if(!IsEmpty(zipcode, Empty_Zip))
        {
			return false;
        }
		else if(!IsZip(zipcode, Valid_Zip))
        {
			return false;
        }
    	if(!IsEmpty(phone_areacode, Empty_Area_Code))
		{
				return false;
		}
    	if(!IsEmpty(phone_citycode, Empty_City_Code))
		{
				return false;
		}
    	if(!IsEmpty(phone_no, Empty_Phone_No))
		{
				return false;
		}
		if(!IsPhone(phone_areacode,phone_citycode,phone_no,Valid_PhoneNo))
		{
			return false;
		}
	
	}
	  return true;
}			
