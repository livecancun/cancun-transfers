function Print_Click(CartId)
{
 	with(document.frmTransferCart)
	{
	 popupWindowURL("transfercart.php?Action=Print&cartId="+CartId, 'Print', 600, 350, false, false, true);
	}
}


function Form_Submit(frm)
{
	with(frm)
    {
		
/*		var adult=parseInt(tres_no_adult.value);
		if(adult<'2')
		{
			alert(Valid_Min_Of_Passengers);
			tres_no_adult.focus();
			return false;
		}
*/	
			
		today          = new Date();
		date           = today.getDate();
		month          = today.getMonth() + 1;
		year           = today.getFullYear();
		
		var checkdate = '';
		var arrival_date = '';
		var departure_date   = '';
		
		if(date < 10)
		{
			  date = '0'+date;
	    }
	    if(month < 10)
		{
			  month = '0'+month;
	    }

		  checkdate = year+"-"+month+"-"+date;
	//	  arrival_date = arrival_year.value+"-"+arrival_month.value+"-"+arrival_day.value;
	//    departure_date = departure_year.value+"-"+departure_month.value+"-"+departure_day.value;
		  
		  
			if(!IsEmpty(hotelName,Empty_Hotel_Name))
			{
			    return false;
			}
			if(!(frm.singleTrip.checked || frm.roundTrip.checked))
			{
				alert(Empty_Check_Box);
				return false;
			}
			if(frm.singleTrip.checked)
			{
		
				arrival_date = arrivalDate.value;
				 
				if(!IsEmpty(arrivalDate, Empty_Arrival_Date))
				{
					return false;
				}
				if(frmTransfer.arrivalDate.value < checkdate)
				{
					 alert(Valid_Arrival_Date);
					 frmTransfer.arrivalDate.focus();	
					 return false;
				}
				if(!IsEmpty(arrivalAirline,Empty_Airline_Name))
				{
					return false;
				}
				if(!IsEmpty(arrivalFlight,Empty_Flight_No))
				{
					return false;
				}
		   }
		   if(frm.roundTrip.checked)		
		   {
		  
		  		departure_date = departureDate.value;
		  
				if(!IsEmpty(departureDate, Empty_Departure_Date))
				{
					return false;
				}
				if(frmTransfer.departureDate.value < checkdate)
				{
					 alert(Valid_Departure_Date);
					 frmTransfer.departureDate.focus();	
					 return false;
				}
				if(!IsEmpty(departureAirline,Empty_Airline_Name))
				{
					return false;
				}
				if(!IsEmpty(departureFlight,Empty_Flight_No))
				{
					return false;
				}

		  }	

		if(frm.singleTrip.checked && frm.roundTrip.checked)
		{
			if(frmTransfer.departureDate.value < frmTransfer.arrivalDate.value)
        	{
				alert(Valid_BothDate_Msg);
				frmTransfer.departureDate.focus();
				return false;
    	    }
		}		
	}

	return true;
}	

function Form_Submit1(frm)
{
	with(frm)
    {
	  //status.value = "Reserve";
	  //submit();
//	  alert (status.value);
//		location.href = 
	}
}	
function Form_Submit2(frm)
{
	with(frm)
    {
	//  status.value = "Checkout";
	//  submit();
//	  alert (status.value);
	}
}		
function calendarCallback(date, month, year)
{
	if(month <= 9)
	{ month = '0'+month }
	if(date <= 9)
	{ date = '0'+date }
	date =  year +'-' + month + '-' + date ;
	document.frmTransfer.arrivalDate .value = date;	// name of the form itself is forms[0]
	document.frmTransfer.arrivalDate .focus();
}

function calendarCallback1(date, month, year)
{
	if(month <= 9)
	{ month = '0'+month }
	if(date <= 9)
	{ date = '0'+date }
	date =  year +'-' + month + '-' + date ;
	document.frmTransfer.departureDate.value = date;	// name of the form itself is forms[0]
	document.frmTransfer.departureDate.focus();
}	