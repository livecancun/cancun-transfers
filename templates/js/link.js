//====================================================================================================
//	Function Name	:	Add_Link_Click()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Parameters		:	PersonId  - Person Id whose detail will to be edit
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
	function Add_Link_Click()
	{
		with(document.frmLinkCategory)
		{
			Action.value = "AddLink";
			submit();
		}
	}
	
	function Add_Cat_Click()
	{
		with(document.frmLinkCategory)
		{
			Action.value = "Add";
			submit();
		}
	}

	function Form_Submit(frm)
	{
		with(frm)
		{
			if(!IsEmpty(url_name, msg_Name))
			{
				return false;
			}
			if(!IsEmpty(url_address, msg_Address))
			{
				return false;
			}
			if(!IsEmpty(url_city, msg_City))
			{
				return false;
			}
			if(!IsEmpty(url_state, msg_State))
			{
				return false;
			}
			if(!IsZip(url_zip, msg_Zip))
			{
				return false;
			}
			if(!IsEmpty(url_country, msg_Country))
			{
				return false;
			}
			if(!IsEmpty(url_telno_areacode, msg_Telephone))
			{
					return false;
			}
			if(!IsEmpty(url_telno_citycode, msg_Telephone))
			{
					return false;
			}
			if(!IsEmpty(url_telno_no, msg_Telephone))
			{
					return false;
			}
			if(!IsEmpty(url_title, msg_Title))
			{
				return false;
			}
			if(!IsEmpty(url_desc, msg_Description))
			{
				return false;
			}
			else if(url_desc.value.length	> 300) 
			{
				alert(msg_ValidSize)
				url_desc.focus();
				return false;
			}
			
			if(!IsUrl(url_url,msg_Url))
			{
				return false;
			}
			if(!IsUrl(url_oururl,msg_Our_Url))
			{
				return false;
			}
			if(!IsEmpty(url_email, msg_Email))
			{
				return false;
			}
			if(!IsEmail(url_email, msg_Email))
			{
				return false;
			}

			return true;
		}
	}
	

	function Form_LinkCat_Submit(frm)
	{
		with(frm)
		{
			if(!IsEmpty(cat_name, 'Please, enter the Category name.'))
			{
				return false;
			}
			return true;
		}
	}	
	
