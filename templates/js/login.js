//====================================================================================================
//	Function Name	:	Form_Submit()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Parameters		:	PersonId  - Person Id whose detail will to be edit
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Form_Submit(frm)
{ 
	with(frm)
		{ 
			if(!IsEmpty(confirmationNo,msgEmptyConfirmationNo))
			{
				return false;    
			}
			if(password.value == "")
			{ 
					 alert (msgEmptyUserPassword);
					 frm.password.focus();
					 return false;
			} 
		return true;
	} 
} 
