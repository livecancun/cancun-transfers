//====================================================================================================
//	Function Name	:	Form_Submit()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Parameters		:	PersonId  - Person Id whose detail will to be edit
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Form_Submit(frm)
{
	with(frm)
    {
		if(!IsEmpty(email,Empty_Email))
        {
	        return false;    
		}
		if(!IsEmail(email, Valid_Email))
        {
			return false;
	    }
	
    }
	return true;
}