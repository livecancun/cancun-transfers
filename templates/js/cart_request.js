//====================================================================================================
//	Function Name	:	checkAll(frm, flag)
//	Purpose			:	This function will redirect the page @ reservation form.
//	Parameters		:	PersonId  - Person Id whose detail will to be edit
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function checkAll(frm, flag)
{
  with(document.frmCart)
	{
		 
	 if(document.all["chkCartId[]"].length)
		 {
		   var len = document.all["chkCartId[]"].length;
		  for (var i = 0; i < len; i++) 
			{
				var e = document.all["chkCartId[]"][i];
				e.checked = true;
		
			}
		}
	 else
	 	{
		   var e = document.all["chkCartId[]"];
		   e.checked = true;
		}		
	}
}
function Form_Delete()
{
	
	with(document.frmCart)
	{
	  if(document.all["chkCartId[]"])
	 {	
		 if(document.all["chkCartId[]"].length)
		 {
			 if(!IsCheckBoxChecked(document.all["chkCartId[]"],Valid_Delete_Cart))
			 {
				return false;
			 }
			 else
			 {
			 	if(confirm(Confirm_Delete))
				{
				 Action.value = "Delete";
				 submit();
				} 
			}	 
		 }
		 else if(!(document.all["chkCartId[]"].checked))
		 {
		 	alert(Valid_Delete_Cart);
			return false;
		 }	 
	     else
	     {
			if (confirm(Confirm_Delete))
			{
				Action.value = "Delete";
				submit();
			}
		 }
	  }	 
		return true;	
	}
}	
function Form_Request()
{
	with(document.frmCart)
	{
	 if(document.all["chkCartId[]"])
	 {
		 if(document.all["chkCartId[]"].length)
		 {
		 	
			 if(!IsCheckBoxChecked(document.all["chkCartId[]"],Valid_Select_Cart))
			 {
				return false;
			 }
			 else
			 {
				 Action.value = "Send_Request";
				 submit();
				 return true;
			}	 
		 }	 
		 else if(!(document.all["chkCartId[]"].checked))
		 {
				alert('{Valid_Select_Cart}');
				return false;
		 }	 
		 else
		 {	
		  		Action.value = "Send_Request";
				submit();
				return true;	
		 }
	 }
	 
	 return true;	
	}
}
function Form_Checkout()
{
	
	with(document.frmCart)
	{
	if(document.all["chkCartId[]"])
	 {
		 if(document.all["chkCartId[]"].length)
		 {
			 if(!IsCheckBoxChecked(document.all["chkCartId[]"],Valid_Select_Cart))
			 {
				return false;
			 }
			 else
			 {
				 Action.value = "Show_All";
				 action = "reservation.php";
				 submit();
				 return true;
			 }
		 }	   
		 else if(!(document.all["chkCartId[]"].checked))
		 {
				alert(Valid_Select_Cart);
				return false;
		 }	 
		 else
		 {
				 Action.value = "Show_All";
				 action = "reservation.php";
				 submit();
				 return true;
		 }
	 }	 
		return true;	
	}
}
function View_Cart()
{
	with(document.frmCart)
	{
		Action.value = "Show_Cart";
		submit();
	}
}
function View_Request()
{
	with(document.frmCart)
	{
		Action.value = "Show_Request";
		submit();
	}
}
function Edit_Click(CartId,ItemType)
{
	with(document.frmCart)
	{
		cartId.value = CartId;
		itemType.value = ItemType;
		Action.value = "Modify";

		if(ItemType == 2)
		{
			action = "transfercart.php";
		}	

		submit();
	}
}
function Show_Click(CartId,ItemType)
{
	with(document.frmCart)
	{
	   
		cartId.value = CartId;
		itemType.value = ItemType;
		Action.value = "Show";
		
		if(ItemType == 2)
		{
			action = "transfercart.php";
		}	
		
		submit();
	}
}
