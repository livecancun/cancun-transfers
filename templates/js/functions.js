
//====================================================================================================
//	File Name		:	functions.js
//----------------------------------------------------------------------------------------------------
//	Purpose			:	Javascript Utility functions
//	Author			:	Nirmal Patel
//	Creation Date	:	05-May-2003
//	Copyright		:	Copyrights � 2003 Dot Infosys
//	Email			:	dinesh@dotinfosys.com
//	History			:
//						Date						Author						Remark
//						05-May-2003					Nirmal Patel				Initial Release
//
//====================================================================================================

//====================================================================================================
//	Function Name	:	popupWindowURL
//	Purpose			:	Whenever you wanna open a link into a new window just call this function
//								you need to pass some arguemnts as described below.
//	Parameters		:
//								url  = url to be open in the new window
//								winname = winname is the window name for the reference of that window
//								w is the width
//								h is the height
//								menu is the parameter, if you want menubar to be enabled on the window
//								resize if you wanna resize the window
//								scroll i fyou needed
//	Return			:	true or false
//	Author			:	Nirmal Patel
//	Creation Date	:	05-May-2003
//----------------------------------------------------------------------------------------------------

function popupWindowURL(url, winname,  w, h, menu, resize, scroll) {

    var x = (screen.width-w)/2;
    var y = (screen.height-h)/3;

	if (winname == null) winname = "newWindow";
	if (w == null) w = 800;
	if (h == null) h = 600;
	if (resize == null) resize = 1;

	menutype   = "nomenubar";
	resizetype = "noresizable";
	scrolltype = "noscrollbars";
	if (menu) menutype = "menubar";
	if (resize) resizetype = "resizable";
	if (scroll) scrolltype = "scrollbars";
	//alert(url+","+x+","+winname);
    cwin=window.open(url,winname,"top=" + y + ",left=" + x + ",screenX=" + x + ",screenY=" + y + "," + "status," + menutype + "," + scrolltype + "," + resizetype + ",width=" + w + ",height=" + h);

	if (!cwin.opener) cwin.opener=self;
	cwin.focus();

	return true;
}