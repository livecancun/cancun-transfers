//====================================================================================================
//	Function Name	:	Reserve_Click()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Parameters		:	PersonId  - Person Id whose detail will to be edit
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Reserve_Click(frm, PassengerId, TripTypeId, DestId, Rate)
{
	with(frm)
	{
		passenger_id.value 	= PassengerId;
		triptype_id.value 	= TripTypeId;
		dest_id.value 	= DestId;
		rate.value 		= Rate;
		action		   = "ground_transportation_rates.php";				
		submit();
	}
}



function Form_Submit(frm)
{
	with(frm)
	{
		//alert("hiiiiiii");
		/*var adult=parseInt(tres_no_adult.value);
		var kid=parseInt(tres_no_kid.value);
		noofpersons=adult+kid;
		alert(noofpersons);
		if(adult<'2')
		{
			alert("{Valid_Min_Of_Passengers}");
			tres_no_adult.focus();
			return false;
		}
		*/		
/*		alert("Passengr_Id="+passenger_id.value);			
		alert("MaxPassenger="+MaxPassenger.value);
		return false;
*/		
		today   = new Date();
		date    = today.getDate();
		month   = today.getMonth() + 1;
		year	= today.getFullYear();

		var checkdate = '';
		var arrival_date = '';
		var departure_date   = '';

		if(date<10)
		{
			date = '0'+date;
		}
		
		if(month<10)
		{
			month = '0'+month;
		}
		
		checkdate = year+"-"+month+"-"+date;
			
			//alert(checkdate);
			// arrival_date = arrival_year.value+"-"+arrival_month.value+"-"+arrival_day.value;
			//departure_date = departure_year.value+"-"+departure_month.value+"-"+departure_day.value;
			

/*			if(no_person.value < checkdate)
			{
				 alert(Valid_No_Person);
				 no_person.focus();	
				 return false;
			}
*/

/*			if(!IsEmpty(tres_no_adult, '{Empty_No_Adult_Msg}'))
			{
				return false;
			}
			
			if(isNaN(tres_no_adult.value))
			{
				alert("{Valid_No_Adult_Msg}");
				frmReserve.tres_no_adult.focus();
				return false;
			}

			if(tres_no_kid.value != '')
			{
				if(isNaN(tres_no_kid.value))
				{
				alert("{Valid_No_Child_Msg}");
				frmReserve.tres_no_kid.focus();
				return false;
				}
			}
			
*/
/*
			if(!(frm.singleTrip.checked || frm.roundTrip.checked))
			{
				alert(Empty_Check_Box);
				return false;
			}
*/
			if(frm.singleTrip.value == 1)
			{
				arrival_date = arrivalDate.value;
				
					if(!IsEmpty(no_person, Empty_No_Person))
					{
						return false;
					}
					if(!IsEmpty(hotelName,Empty_Hotel_Name))
					{
						return false;
					}
					if(!IsEmpty(arrivalDate, Empty_Arrival_Date))
					{
						return false;
					}
					if(frmReserve.arrivalDate.value< checkdate)
					{
						alert(Valid_Arrival_Date);
						frmReserve.arrivalDate.focus();
						return false;
					}
					if(!IsEmpty(arrivalAirline,Empty_Airline_Name))
					{
						return false;
					}
					if(!IsEmpty(arrivalFlight,Empty_Flight_No))
					{
						return false;
					}
			}

			if(frm.roundTrip.value == 2)
			{

				departure_date 	= departureDate.value;

					if(!IsEmpty(departureTotalPerson, Empty_No_Person))
					{
						return false;
					}
					if(!IsEmpty(departureHotelName,Empty_Hotel_Name))
					{
						return false;
					}
					if(!IsEmpty(departureDate, Empty_Departure_Date))
					{
						return false;
					}
					if(frmReserve.departureDate.value< checkdate)
					{
						alert(Valid_Departure_Date);
						frmReserve.departureDate.focus();
						return false;
					}
					if(!IsEmpty(departureAirline,Empty_Airline_Name))
					{
						return false;
					}
					if(!IsEmpty(departureFlight,Empty_Flight_No))
					{
						return false;
					}
				}
				if(frm.singleTrip.value == 1 && frm.roundTrip.value == 2)
					{
					if(frmReserve.departureDate.value< frmReserve.arrivalDate.value)
						{
						alert(Valid_BothDate_Msg);
						frmReserve.departureDate.focus();
						return false;
						}
					}
				}
				return true;
			}

			function Form_Submit1(frm)
			{
				with(frm)
				{
				//status.value = "Reserve";
				//submit();
				//	  alert (status.value);
				//		location.href =
				}
			}

			function Form_Submit2(frm)
			{
				with(frm)
				{
				//  status.value = "Checkout";
				//  submit();
				//	  alert (status.value);
				}
			}

			function calendarCallback(date, month, year)
			{
				if(month<= 9)
				{ 
					month = '0'+month 
				}
				
				if(date	<= 9)
				{ 
					date = '0'+date 
				}
				
				date =  year +'-' + month + '-' + date ;
				document.frmReserve.arrivalDate .value = date;	// name of the form itself is forms[0]
				document.frmReserve.arrivalDate .focus();
			}
			
			function calendarCallback1(date, month, year)
			{
				if(month<= 9)
				{ 
					month = '0'+month 
				}
				
				if(date<= 9)
				{ 
					date = '0'+date 
				}
				
				date =  year +'-' + month + '-' + date ;
				document.frmReserve.departureDate.value = date;	// name of the form itself is forms[0]
				document.frmReserve.departureDate.focus();
			}
			
			