//====================================================================================================
//	Function Name	:	Form_Submit()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Form_Submit(frm)
{
	with(frm)
    {
		/*if(frm.airline_emp.checked)
		{*/
			if(!IsSelected(discount_id, Select_Airline_Employee))
			{
				return false;
			}			
			if(!IsEmpty(empl_id, Empty_ID))
			{
				return false;
			}
			if(!IsEmpty(empl_airline, Empty_Airline_Name))
			{
				return false;
			}
		/*}*/
	}
	  return true;
}			


//====================================================================================================
//	Function Name	:	checkAll()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Parameters		:	PersonId  - Person Id whose detail will to be edit
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function checkAll(frm, flag)
{
  with(document.frmCart)
	{
		 
	 if(document.all["chkCartId[]"].length)
		 {
		   var len = document.all["chkCartId[]"].length;
		  for (var i = 0; i < len; i++) 
			{
				var e = document.all["chkCartId[]"][i];
				e.checked = true;
		
			}
		}
	 else
	 	{
		   var e = document.all["chkCartId[]"];
		   e.checked = true;
		}		
	}
}

//====================================================================================================
//	Function Name	:	Form_Checkout()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Form_Checkout(frm,userId)
{
	with(document.frmCart)
	{

	if(document.getElementById("chkCartId[]"))
	 {
		 if(document.getElementById("chkCartId[]").length)
		 {
			 if(!IsCheckBoxChecked(document.getElementById("chkCartId[]"),Valid_Select_Cart))
			 {
				return false;
			 }
			 else
			 {
				 index_id.value = userId;
				 Action.value = "Show_All";
				 action = "ground_transportation_rates.php?Action=ReserveInfo";
				 submit();
				 return true;
			 }
		 }	   
		 else if(!(document.getElementById("chkCartId[]").checked))
		 {
				alert(Valid_Select_Cart);
				return false;
		 }	 
		 else
		 {
				 index_id.value = userId;				 
				 Action.value = "Show_All";
				 action = "ground_transportation_rates.php?Action=ReserveInfo";
				 submit();
				 return true;
		 }
	 }	 
		return true;	
	}
}

//====================================================================================================
//	Function Name	:	Form_Delete()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Form_Delete()
{
	
	with(document.frmCart)
	{
	  if(document.getElementById("chkCartId[]"))
	 {	
		 if(document.getElementById("chkCartId[]").length)
		 {
			 if(!IsCheckBoxChecked(document.getElementById("chkCartId[]"),Valid_Delete_Cart))
			 {
				return false;
			 }
			 else
			 {
			 	if(confirm(Confirm_Delete))
				{
				 Action.value = "Delete";
				 submit();
				} 
			}	 
		 }
		 else if(!(document.getElementById("chkCartId[]").checked))
		 {
		 	alert(Valid_Delete_Cart);
			return false;
		 }	 
	     else
	     {
			if (confirm(Confirm_Delete))
			{     
				Action.value = "Delete";
				//alert(Action.value);
				submit();
			}
		 }
	  }	 
		return true;	
	}
}	

//====================================================================================================
//	Function Name	:	Form_Request()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Form_Request()
{
	with(document.frmCart)
	{
	 if(document.all["chkCartId[]"])
	 {
		 if(document.all["chkCartId[]"].length)
		 {
		 	
			 if(!IsCheckBoxChecked(document.all["chkCartId[]"],Valid_Select_Cart))
			 {
				return false;
			 }
			 else
			 {
				 Action.value = "Send_Request";
				 submit();
				 return true;
			}	 
		 }	 
		 else if(!(document.all["chkCartId[]"].checked))
		 {
				alert(Valid_Select_Cart);
				return false;
		 }	 
		 else
		 {	
		  		Action.value = "Send_Request";
				submit();
				return true;	
		 }
	 }
	 
	 return true;	
	}
}

//====================================================================================================
//	Function Name	:	View_Cart()
//	Purpose			:	This function will redirect the page @ reservation form.
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function View_Cart()
{
	with(document.frmCart)
	{
		Action.value = "Show_Cart";
		submit();
	}
}

//====================================================================================================
//	Function Name	:	View_Request()
//	Purpose			:	This function is used to view the request.
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function View_Request()
{
	with(document.frmCart)
	{
		Action.value = "Show_Request";
		submit();
	}
}

//====================================================================================================
//	Function Name	:	Edit_Click()
//	Purpose			:	This function is used to view the request.
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Edit_Click(CartId,ItemType)
{
	with(document.frmCart)
	{
		cartId.value 	= CartId;
		itemType.value 	= ItemType;
		Action.value 	= "Modify";

		if(ItemType == 2)
		{
			action = "transfercart.php";
		}	

		submit();
	}
}

//====================================================================================================
//	Function Name	:	Show_Click()
//	Purpose			:	This function is used to view the request.
//	Return			:	none
//	Author			:	Urvashi SOlanki
//	Creation Date	:	17-Jan-2006
//----------------------------------------------------------------------------------------------------
function Show_Click(CartId,ItemType)
{
	with(document.frmCart)
	{
	   
		cartId.value = CartId;
		itemType.value = ItemType;
		Action.value = "Show";
		
		if(ItemType == 2)
		{
			action = "transfercart.php";
		}	
		submit();
	}
}



//----------------------------- (Show Airline Employee Details) ----------------------------//
	dom = (document.getElementById)? true : false;
	nn4 = (document.layers)? true : false;
	ie4 = (!dom && document.all)? true : false;
	opera=(navigator.userAgent.indexOf('Opera') >= 0)? true : false;
	
	function layerSwitch(num_all, num_cur) 
	{
		hideAllElement( num_all );
		if (arguments.length==1 ) 
		{
			return;
		}

		Div=dom?document.getElementById('CHILD'+num_cur):document.all['CHILD'+num_cur];
		if (Div && Div.style)Div.style.display="block";
	}
	
	function hideAllElement( num_all ) 
	{
		 for (var i=0; i<=num_all; i++) 
		 {
			 var Div=dom?document.getElementById('CHILD'+i):document.all['CHILD'+i];
			 if (Div && Div.style)Div.style.display="none";
		 }
	}
	
	function layerChange( num_cur ) 
	{
		
		 Div=dom?document.getElementById('CHILD'+num_cur):document.all['CHILD'+num_cur];
		 if (Div && Div.style)
		 Div.style.display = Div.style.display=="none" ? "block" : "none";
	}
//-----------------------------------------------------------------------------------------//

