function Validate_Form(frm)
{
	with(frm)
    {	
		if(!IsEmpty(firstname, 'Please, enter First Name.'))
			return false;

		if(!IsEmpty(lastname, 'Please, enter Last Name.'))
			return false;

		if(!IsEmpty(company, 'Please, enter Company.'))
			return false;

		if(!IsEmpty(email, 'Enter your Email'))
			return false;

		if(!IsEmail(email, 'Enter Valid Email Address'))
			return false;

		return true;
	}
}

function Validate_Email(frm)
{
	with(frm)
    {	
		if(!IsEmpty(newsletter_email, 'Enter your Email'))
			return false;

		if(!IsEmail(newsletter_email, 'Enter Valid Email Address'))
			return false;

		return true;
	}
}
