<?php
/**
 Wstravel v1.0.0 (2018-Sep)
 Copyright (c) 2017-2019 Wstravel.
 */
header('Content-type:application/json;charset=utf-8');
$WST = include '../apiwst/rolwstcfg.php';
if (!empty($_POST['ROL_CLIENT_ID']) && $_POST['ROL_CLIENT_ID'] === $WST['ROL_CLIENT_ID'] && $_SERVER['REQUEST_METHOD'] == 'POST') {
    echo json_encode(['rol_secret_id'=>$WST['ROL_SECRET_ID']]);
	exit();
}
exit();