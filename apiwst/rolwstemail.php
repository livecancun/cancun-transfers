<?php
/**
 Wstravel v1.0.0 (2018-Sep)
 Copyright (c) 2017-2019 Wstravel.
 Envio email usando la libreria PHPMailer : https://github.com/PHPMailer/PHPMailer
 */
header('Content-type:application/json;charset=utf-8');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../PHPMailer/src/Exception.php';
require '../PHPMailer/src/PHPMailer.php';
require '../PHPMailer/src/SMTP.php';

$i18n_lang = 'es';
$data['status'] = false;
$data['log'] = [];
$WST = include '../apiwst/rolwstcfg.php';
// validacion del ID CLIENTE
if (empty($_POST['ROL_CLIENT_ID']) || $_POST['ROL_CLIENT_ID'] != $WST['ROL_CLIENT_ID'] || $_SERVER['REQUEST_METHOD'] != 'POST') {
    echo json_encode($data);
    exit();
}

$mensaje = null;
$mail = new PHPMailer;
try {
    switch ($i18n_lang) {
        case 'es':
            $mensaje  = '<img src="http://www.cancun-transfers.com/images/cancun-shuttle-logo.png" /><br><br><br>';
            $mensaje .= "\r\n";
            $mensaje .= "Estimado {$_POST['nombre_completo']} :<p>cancun-transfers.com, Gracias por su preferencia, ";
            $mensaje .= "hemos enviado un mensaje con la información que nos proporciono, si usted tiene alguna duda o solicita algún cambio, ";
            $mensaje .= "por favor envienos un correo con su solicitud y nos comunicaremos con usted lo mas pronto como nos sea posible, ";
            $mensaje .= "es necesario sea con 24 hrs antes de su llegada, nuestra unidad estará llegando por usted a la hora y fecha de su llegada.</p><p>";
            $mensaje .= "Tambien puede comunicarse a nuestros siguientes números:<br>";
            $mensaje .= "Telefonos de oficina: USA / CAN 888 811 4255 Mex 01 800 161 4957, Cancun 286 15 25, Movil:+529982149012<br>";
            $mensaje .= "Estamos disponibles todos los días, las 24 hrs. </p><br><hr><br> ";
            $mensaje .= " \r\n";
            $mensaje .= "Servicio Solicitado: {$_POST['traslado_nombre']} / {$_POST['servicio_nombre']}<br> \r\n";
            $mensaje .= "Pasajeros: {$_POST['num_pasajeros']}<br> \r\n";
            
            switch ($_POST['traslado_clave']) {
                case 'aero-hotel':
                    $llegada = $_POST['movimientos']['llegada'];
                    $mensaje .= "Hotel de llegada: {$_POST['hotel_nombre']}<br> \r\n";
                    $mensaje .= "Aerolinea: {$llegada['aerolinea']}<br> \r\n";
                    $mensaje .= "Numero vuelo: {$llegada['num_vuelo']}<br> \r\n";
                    $mensaje .= "Fecha llegada: {$llegada['fecha']}<br> \r\n";
                    $mensaje .= "Hora de arrivo: {$llegada['hora']}<br> \r\n";
                    break;
                case 'hotel-aero':
                    $salida = $_POST['movimientos']['salida'];
                    $mensaje .= "Hotel de salida: {$_POST['hotel_nombre']}<br> \r\n";
                    $mensaje .= "Pasamos por usted: {$salida['hora']}<br> \r\n";
                    $mensaje .= "Su vuelo sale: {$salida['fecha']}<br> \r\n";
                    $mensaje .= "Su hora de vuelo es: {$salida['hora_vuelo']}<br> \r\n";
                    break;
                case 'aero-hotel-aero':
                    $llegada = $_POST['movimientos']['llegada'];
                    $salida = $_POST['movimientos']['salida'];
                    
                    $mensaje .= "Hotel de llegada: {$_POST['hotel_nombre']}<br> \r\n";
                    $mensaje .= "Aerolinea: {$llegada['aerolinea']}<br> \r\n";
                    $mensaje .= "Num Vuelo: {$llegada['num_vuelo']}<br> \r\n";
                    $mensaje .= "Fecha llegada: {$llegada['fecha']}<br> \r\n";
                    $mensaje .= "Hora de arrivo: {$llegada['hora']}<br> \r\n";
                    $mensaje .= "<br>\r\n";
                    $mensaje .= "Fecha de salida: {$salida['fecha']}<br> \r\n";
                    $mensaje .= "Pasamos por usted: {$salida['hora']}<br> \r\n";
                    $mensaje .= "Su hora de vuelo es: {$salida['hora_vuelo']}<br> \r\n";
                    break;
            }
            
            $mensaje .= "Su correo es: {$_POST['email']}<br> \r\n";
            $mensaje .= "Su telefono movil es: {$_POST['telefono']}<br> \r\n";
            $mensaje .= "Su pais es: {$_POST['pais_nombre']}<br> \r\n";
            $mensaje .= "Mensaje que nos envio: {$_POST['observaciones']}<br> \r\n";
            $mensaje .= "<br><br>Reservaciones cancun-transfers <br>Tel: USA / CAN 888 811 4255 MEX 01 800 161 4957, Cancun 286 15 25 ,";
            $mensaje .= "Movil: +529982149012<br>email: info@cancun-transfers.com<br> Cancun Quintana Roo Mexico<br>";
            $mensaje .= 'www.cancun-transfers.com <br><br><img src="http://www.cancun-transfers.com/images/cancun-shuttle-logo.png" /><br><br><br>';
            $mensaje .= "<p>El contenido de este correo es informativo, agradecemos su preferencia. </p><br><br> \r\n";
            $mensaje .= "Enviado el " . date('d/m/Y', time());
            //Envio de datos extra a mail principal
            //$mensaje .="<br><br>La casilla de reserva en linea esta %%%pago_online%%%<br> \r\n";
            break;
        case 'en':
            break;
    }
    
    if ($mensaje !== null) { 
        $mail->isSMTP();
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 0; // 2 debug server
        $mail->Host = 'host153.hostmonster.com';
        $mail->Port = 465;
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPAuth = true;
        $mail->Username = "info@cancun-transfers.com";
        $mail->Password = "Kmnpipo3285";
        $mail->setFrom('info@cancun-transfers.com', 'Info cancun-transfers');
        //$mail->addBCC('info@cancun-transfers.com', 'Nueva reserva cancun-transfers');
        $mail->addAddress($_POST['email'], $_POST['nombre_completo']);
        $mail->Subject = 'Mensaje cancun-transfers Español';
        $mail->msgHTML($mensaje);
        if ($mail->send()) {
        //if (false) {
            $data['status'] = true;
        } else {
            $data['log'] = $mail->ErrorInfo;
        }
    }
} catch (Exception $e) {
    $data['log'] = $e->errorMessage();
} catch (\Exception $e) {
    $data['log'] = $e->getMessage();
}
echo json_encode($data);
exit();
?>
