<?php 
// ================================================
// SPAW PHP WYSIWYG editor control
// ================================================
// Image library dialog
// ================================================
// Developed: Alan Mendelevich, alan@solmetra.lt
// Copyright: Solmetra (c)2003 All rights reserved.
// ------------------------------------------------
//                                www.solmetra.com
// ================================================
// $Revision: 1.7 $, $Date: 2003/04/21 15:09:56 $
// ================================================

// include wysiwyg config
if(isset($_POST['editor_root']))
{
	define('EDITOR_ROOT', 	$_POST['editor_root']);
	define('EDITOR_URL', 	$_POST['editor_url']);
}
else
{
	define('EDITOR_ROOT', 	$_GET['editor_root']);
	define('EDITOR_URL', 	$_GET['editor_url']);
}

include EDITOR_ROOT. 'config/spaw_control.config.php';
include EDITOR_ROOT. 'class/lang.class.php';

$theme = empty($HTTP_POST_VARS['theme'])?(empty($HTTP_GET_VARS['theme'])?$spaw_default_theme:$HTTP_GET_VARS['theme']):$HTTP_POST_VARS['theme'];
$theme_path = EDITOR_URL .'libs/themes/'.$theme.'/';

$l = new SPAW_Lang(empty($HTTP_POST_VARS['lang'])?$HTTP_GET_VARS['lang']:$HTTP_POST_VARS['lang']);
$l->setBlock('mergecode');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
  <title><?php echo $l->m('title')?></title>
	<meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Content-Type" content="text/html; charset=<?php echo $l->getCharset()?>">
  <link rel="stylesheet" type="text/css" href="<?php echo $theme_path.'css/'?>dialog.css">
  <script language="javascript" src="utils.js"></script>
  
  <script language="javascript">
  <!--
    function insertClick()
    {
		var selectCode = '';

		with(document.libbrowser)
		{
			if(mergecode.length)
			{
				for(i=0; i < mergecode.length; i++)
				{
					if(mergecode[i].checked)
						selectCode += mergecode[i].value + '&nbsp;';
				}
			}
			else
			{
				if(mergecode.checked)
					selectCode += mergecode.value + '&nbsp;';
			}
		}
		if(selectCode == '')
		{
			alert('<?php echo $l->m('error').': '.$l->m('error_no_code')?>');
		}
		else
		{
			window.returnValue = selectCode;
			window.close();
		}
    }
    
    function Init()
    {
      resizeDialogToContent();
    }
  //-->
  </script>
  <style>
	/* Form elements */
	input,textarea, select {
		font-size 			: 8pt;
		color 				: #000000;
		font-family			: Verdana, Arial, Helvetica, sans-serif;
		font-weight			: normal;
		border-style		: outset;
		border-width		: 1
	}
	td{
		font-family		: Verdana, Arial, Helvetica, sans-serif;
		font-size		: 8pt;
		font-weight		: normal;
		color			: #000000;
	}
  </style>
</head>
<body onLoad="Init()" dir="<?php echo $l->getDir();?>" bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
  <script language="javascript">
  <!--
    window.name = 'codelibrary';
  //-->
  </script>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<form name="libbrowser" method="post" action="mergecode_list.php" target="codelibrary">
	<tr>
		<td>
			<input type="hidden" name="theme" value="<?php echo $theme?>">
			<input type="hidden" name="lang" value="<?php echo $l->lang?>">
			<input type="hidden" name="editor_root" value="<?php echo $editor_root;?>">
			<input type="hidden" name="editor_url" value="<?php echo $editor_url;?>">
			<div style="border: 1px solid #000000; padding: 5 5 5 5; width:100%" align="center">
			<table border="0" cellpadding="2" cellspacing="2" width="95%">
				<tr>
					<td colspan="6" bgcolor="#999999" height="1"></td>
				</tr>
				<tr>
					<td width="2%">&nbsp;</td>
					<td width="23%"><b>Description</b></td>
					<td width="23%"><b>Merge Code</b></td>
					<td width="2%">&nbsp;</td>
					<td width="23%"><b>Description</b></td>
					<td width="23%"><b>Merge Code</b></td>
				</tr>
				<tr>
					<td colspan="6" bgcolor="#999999" height="1"></td>
				</tr>
			<?php
			$i=1;
			foreach($spaw_mergecode_data as $key => $val)
			{
				if($i%2)
					print "<tr>";
				if(preg_match("/sep/", $key))
					print "<td colspan=3>&nbsp;</td>";
				elseif(preg_match("/desc/", $key))
				{
					if($i%2==0)
						print "<td colspan=3>&nbsp;</td></tr><tr>";
					else
						$i++;
					print "<td colspan=6 align=center><b><u>$val</u></b></td></tr><tr>";
				}
				else
				{
			?>
					<td width="2%"><input type="checkbox" name="mergecode" value="<?= $val[1] ?>"></td>
					<td width="23%"><?= $val[0] ?></td>
					<td width="23%"><?= $val[1] ?></td>
			<?php
				}
				if($i%2 == 0)
					print "</tr>";
				$i++;
			}
			?>
				<tr>
					<td colspan="6" bgcolor="#999999" height="1"></td>
				</tr>
				<tr>
					<td valign="top" align="left" colspan="6">
						<input type="button" value="<?php echo $l->m('insert')?>" class="bt" onclick="insertClick();">&nbsp;<input type="button" value="<?php echo $l->m('cancel')?>" class="bt" onclick="window.close();">
					</td>
				</tr>
			</table>
			</div>
		</td>
	</tr>
	</form>
</table>

</body>
</html>