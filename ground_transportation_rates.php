<?php
#====================================================================================================
# File Name : ground_transportation_rates.php 
#----------------------------------------------------------------------------------------------------
# Purpose : This file contains all application configuration details
# Author : PIMSA.COM 
# Copyright : Copyright � 2006 PIMSA.COM 
# Email : info@pimsa.com <mailto:info@pimsa.com>
#
#====================================================================================================

#====================================================================================================
#	Include required files
#----------------------------------------------------------------------------------------------------
define('IN_SITE', 	true);
define('RIGHT_PANEL', 	true);

# include the required file
include_once("includes/common.php");
include_once($physical_path['DB_Access']. 'Cart.php');
include_once($physical_path['DB_Access']. 'Destination.php');
include_once($physical_path['DB_Access']. 'Discount.php');
include_once($physical_path['DB_Access']. 'Reservation.php');
include_once($physical_path['DB_Access']. 'TripRate.php');
include_once($physical_path['DB_Access']. 'TripType.php');
include_once($physical_path['DB_Access']. 'UserMaster.php');
include_once($physical_path['DB_Access']. 'User.php');

global $virtual_path;
#=======================================================================================================================================
# Define the action
#---------------------------------------------------------------------------------------------------------------------------------------
	$Action 		= isset($_GET['Action']) ? $_GET['Action'] : (isset($_POST['Action']) ? $_POST['Action'] : SHOWALL);
	$rate 			= isset($_GET['rate']) ? $_GET['rate'] : $_POST['rate'];
	$dest_id 		= isset($_GET['dest_id']) ? $_GET['dest_id'] : $_POST['dest_id'];	
	$triptype_id 	= isset($_GET['triptype_id']) ? $_GET['triptype_id'] : $_POST['triptype_id'];	
	$passenger_id 	= isset($_GET['passenger_id']) ? $_GET['passenger_id'] : $_POST['passenger_id'];		
	$discount_id	= isset($_GET['discount_id']) ? $_GET['discount_id'] : $_POST['discount_id'];			
	$index_id		= isset($_GET['index_id']) ? $_GET['index_id'] : $_POST['index_id'];				
	$cart_id		= isset($_GET['cart_id']) ? $_GET['cart_id'] : $_POST['cart_id'];					

	# Initialize object with required module
	$objCart 	= new Cart();
	$objDest 	= new Destination();
	$objDisco 	= new Discount();	
	$objRate	= new TripRate();
	$objRes 	= new Reservation();
	$objType	= new TripType();
	$objUser 	= new UserMaster();
	$objAuthUser= new User();	
	
	$redirectUrl = "ground_transportation_rates.php";
//==================================================================================================
// Set the Starting Page, Page Size
//==================================================================================================
	if(!isset($_GET['start']))
		$start_record = 0;
	else
		$start_record = $_GET['start'];
	
	$Page_Size = 15;
	$num_records = '';
#====================================================================================================
#	Add report details into database
#----------------------------------------------------------------------------------------------------
if($Action == RESERVE && $_POST['Submit'] == A_SAVE)
{
  $cartId = 0;


  if(isset($_SESSION['User_Id']) && $_SESSION['User_Perm'] == USER)
  {
			$TotalPerson = 0;
			$TotalPerson = $_POST['no_person'];
			$totalPrice = 0;

			$totalcost	=	$rate;			
			/*****************************************/
			if($_POST['singleTrip'] && $_POST['roundTrip'])
			{
				$totalPrice = $totalcost;
				$trip_status = 2;
			}
			else
			{
				if($_POST['singleTrip'])
				{
					$totalPrice = $totalcost ;
					$trip_status = 0;
				}
				else
				{
					$totalPrice =  $totalcost;
					$trip_status = 1;
				}	
			}
			
			
			
		#========================= TIME portion Start here ============#
			 if(isset($_POST['arrival_hour']))
			 {
				$arrivalTime  	= $_POST['arrival_hour']." : ".$_POST['arrival_minute']." : ".$_POST['arrival_DayPart'];
				$departureTime  = $_POST['departure_hour']." : ".$_POST['departure_minute']." : ".$_POST['departure_DayPart'];
				$pickupTime  		=$_POST['pickup_hour']." : ".$_POST['pickup_minute']." : ".$_POST['pickup_DayPart'];
			 }
	
			$date = date("Y-m-d");
			$cartDateTime = mktime (date('G'),date('i'),date('s'),date('m'),date('d'),date('Y'));
		

			$cartId = $objRes -> InsertCart1($_SESSION['User_Id'],	$_POST['dest_id'], $cartDateTime,	$totalPrice);


			 if(!isset($_SESSION['arrivalDate']))
					$_SESSION['arrivalDate'] = $_POST['arrivalDate'];
	
			 if(!isset($_SESSION['departureDate']))
					$_SESSION['departureDate'] = $_POST['departureDate'];	


			$destId = $objRes -> InsertDest($_POST['dest_id'],				$cartId,						$_POST['no_person'],	
											$_POST['hotelName'],			$_POST['arrivalDate'],			$arrivalTime,			
											$_POST['arrivalAirline'],		$_POST['arrivalFlight'],		$_POST['departureDestId'],	
											$_POST['departureTotalPerson'],	$_POST['departureHotelName'],    $pickupTime,
												$_POST['departureDate'],
											$departureTime,					$_POST['departureAirline'],		$_POST['departureFlight'],
											$trip_status,					$date,							$totalPrice);		

	}

	header("location: cart.php?&cart_id=".$cartId);		
	
}

elseif($Action == RESERVEINFO && ($_POST['Submit'] == A_SUBMIT ||  $_POST['Submit'] == "Enviar") )
{
  	$phoneno = "";

	if($_POST['phone_areacode'] && $_POST['phone_citycode'] && $_POST['phone_no'])
	{
		$phoneno = "(".$_POST['phone_areacode'].")".$_POST['phone_citycode']."-".$_POST['phone_no'];
	}

	$password = rand_pass(6);	

	$userId = $objUser -> UpdatetUserDetails($index_id,	$_POST['firstname'],	$_POST['lastname'],
									$_POST['address1'],		$_POST['address2'],		$_POST['city'],		$_POST['state'],
									$_POST['country'],		$_POST['zipcode'],		$phoneno,			$_POST['email'],		
									$password,				$cart_id,				$dest_id,			$_POST['V_grandTotal']);

	$confirmation_no = date("YmdHi").$userId;



/*print "confirmation_no=".$confirmation_no."<br>";
print "confirmation_no=".md5($confirmation_no)."<br>";
print "password=".$password."<br>";
*/


	$j=0;
	while($j < count($_POST['chkCartId']))
	{		
		$cartId = $_POST['chkCartId'][$j];
		$cartstatus = $objCart->updateAllCart($cartId,$confirmation_no);
		$j++;	
	}		
	
	//$userstatus = updateUserinCart(md5($confirmation_no));


	#==============================Code For Email for admin==============================#
	
	$tpl->assign(array( 'T_Body'		=>	'adminReservationEmail'. $config['tplEx'],
						'DestinationInfo'=>	$objDest->getAll(" GROUP BY dest_id ORDER BY dest_id "),
						'PassengerRange'=>	$objDest->getAllPassenger(),					
						'TripTypeInfo'	=>	$objType->ViewAll(),
						'R_RecordSet'	=>	$objRate->getAll(),
						));

	$tpl->assign(array("L_Reservation_Details" => $lang['L_Reservation_Details'],
						"L_cartItem"			=> $lang['L_cartItem'],
						"L_ItemType"			=> $lang['L_ItemType'],
						"L_Total"   			=> $lang['L_Total'],
						"L_RequestNo"   		=> $lang['RequestNo'],
						"L_Discount"	 		=>	$lang['L_Discount'],
					));
	
		$SortBy = "itemType";
		$start_record = 0;
		$j = 0;
		$grandTotal = 0;

		while($j < count($_POST['chkCartId']))
		{
			$tpl->assign("cartId",$_POST['chkCartId'][$j]);	
			$j++;	
		}
	
		$tpl->assign("chk_cart_id",$_POST['chkCartId']);	
					
		if(count($_POST['chkCartId']))
		{
			$chkCartId = implode(",", $_POST['chkCartId']);
		}	

		$tpl->assign(array( 'ShowRequest'		=>	$objCart -> Show_UserCart($chkCartId),
							));
							

		$sql = $objUser	-> ShowUserInfo($userId);
		$db->next_record();	 

		$name  = stripslashes(ucfirst($db->f('firstname')))." ".stripslashes(ucfirst($db->f('lastname')));
		$email = stripslashes($db->f('email'));
		
		$tpl->assign("name",$name);	
		$tpl->assign("Confirmation_No",$confirmation_no);

		$tpl->assign(array("is_airline_empl"	=> $db->f('is_airline_empl'),
						   "discount_id"     	=> $db->f('discount_id'),
						   "discount_rate"	    => $db->f('discount_rate'),
						   "discount_title"	    => ucfirst($db->f('discount_title')),
						   "empl_id" 	    	=> $db->f('empl_id'),
						   "empl_airline"		=> ucfirst($db->f('empl_airline')),
						   ));

		#===============================Code For Email for admin======================================#
		$support_email = $config[WC_SUPPORT_EMAIL];

		$ccEmail = '';
		
		$File_Content = readFileContent($physical_path['Templates_Root'] . "adminReservationEmail.tpl");

		$Email_Body = $tpl->fetch('adminReservationEmail'. $config['tplEx']);	

		$mimemail->setFrom($name.' <'.$email.'>');
		$mimemail->setSubject($lang['L_Res_ReqDetail']." At Cancun-Transfers.com");
		$mimemail->setHtml(stripslashes($Email_Body));
			
		if($ccEmail != '')
				$mimemail->setCc(explode(',',$ccEmail));
		
		$result = $mimemail->send(explode(',', 'Cancun-Transfers <'. $support_email. '>'));
			
		#===============================Code For Email for User======================================#
		$from = $support_email;
		$subject = $lang['L_Your_ResDetail']. "  At Cancun-Transfers.com";
		$message =	$lang['L_ConfMail_Detail_1'] . $name .
					$lang['L_ConfMail_Detail_2'] . $confirmation_no .
					$lang['L_ConfMail_Detail_3'] . $confirmation_no .
					$lang['L_ConfMail_Detail_4'] . $password		.		
					$lang['L_ConfMail_Detail_5'];

		$headers = "From:" . $from . "\nReply-To: " .$from."\nContent-Type:text/html;charset=iso-8859-1;";
		
		@mail("$email","$subject","$message","$headers");

	
//	header("location: success.php?&add=true&confirmationNo=$confirmation_no");
	
	$confirmation_no = md5($confirmation_no);
	header("location: paymentProcess.php?&confirmationNo=$confirmation_no");
	exit;
	
}

#====================================================================================================
#	List all Destinations
#----------------------------------------------------------------------------------------------------
if($Action	==	SHOWALL)
{
	$tpl->assign(array( 'T_Body'		=>	'rates'. $config['tplEx'],
						'JavaScript'	=>	array('rates.js','popcalendar.js'),
						'A_Action'		=>	$scriptName,
						'DestinationInfo'=>	$objDest->getAll(" GROUP BY dest_id ORDER BY dest_order "),
						'PassengerRange'=>	$objDest->getAllPassenger(),					
						'TripTypeInfo'	=>	$objType->ViewAll(),
						'R_RecordSet'	=>	$objRate->getAll(),
						));


	$tpl->assign(array(	"site_rates_img"	=>	$lang['site_rates_img'],
						"site_paynow_img"	=>	$lang['site_paynow_img'],
						));

	$tpl->assign(array(	"right_menu"		=>	'',
					));

	$tpl->assign(array(	"L_Destinations"   	=> $lang['L_Destinations'],
						"L_Reserve"			=> $lang['L_Reserve'],
						"L_Book"			=> $lang['Book'],
						"L_To"				=> $lang['L_To'],						
						"L_Rate_Text"		=> $lang['L_Rate_Text'],
						"L_Rate_Text2"		=> $lang['L_Rate_Text2'],
						"ACTION"			=> RESERVE,
					   	));

}


elseif($Action == RESERVE)
{
	$tpl->assign(array( 'T_Body'			=>	'reserve'.$config['tplEx'],
						'JavaScript'		=>	array('browserSniffer.js','popcalendar.js',$_SESSION['lng'].'_dynCalendar.js','rates.js'),
						'A_Action'			=>	$scriptName,
						"Header"			=>  "Resv_Rates",
						"L_Resv_Msg"   		=> 	$lang['L_Resv_Msg'],
						'MaxPassenger'		=>	$objDest->getMaxPassenger($passenger_id)
						));

	$tpl->assign(array(	"site_res_img"		=>	$lang['site_res_img'],
						"site_booknow_img"	=>	$lang['site_booknow_img'],
						));

	$dest_title 	= $objDest->Show_Destinations($dest_id);

	$tpl->assign(array(	"dest_id"			=>	$dest_id,
						"dest_title"		=>	$dest_title,
						));

	$tpl->assign(array(	"L_Destinations"   	=> $lang['L_Destinations'],
						"L_Reserve"			=> $lang['L_Reserve'],
						"L_Book"			=> $lang['Book'],
						"L_Origin"			=> $lang['L_Origin'],
						"ACTION"			=> RESERVE,
						"SUBMIT"			=> A_SAVE,			
						"Dest_List"			=> $dest_list,						
					   	));

	$tpl->assign(array( 'Rate'			=>	$rate,
						'PassengerId'	=>	$passenger_id,
						'TriptypeId'	=>	$triptype_id,
						'DestId'		=>	$dest_id,
						));

	$tpl->assign(array("L_Book_Transfer"  		=> $lang['L_Book_Transfer'],
					   "L_Destinations"   		=> $lang['L_Destinations'],
					   "L_No_Person"   			=> $lang['L_No_Person'],					   
					   "L_Adults" 		  		=> $lang['L_Adults'],
					   "L_Childs"		  		=> $lang['L_Childs'],
					   "L_Hotel"          		=> $lang['L_Hotel'],
					   "L_Pickup_Time"			=> $lang['L_Pickup_Time'],
					   "L_Departure_Time"		=> $lang['departuretime'],
					   "L_Date"			  		=> $lang['L_Date'],
					   "L_Time"          		=> $lang['L_Time'],
					   "L_Airline"       		=> $lang['L_Airline'],
					   "L_Flight"        		=> $lang['L_Flight'],
					   "L_Airport_To_Hotel"   	=> $lang['L_Airport_To_Hotel'],
					   "L_Hotel_To_Airport"   	=> $lang['L_Hotel_To_Airport'],
					   "L_Airport_To_Hotel_Msg" => $lang['L_Airport_To_Hotel_Msg'],
					   "L_Hotel_To_Airport_Msg" => $lang['L_Hotel_To_Airport_Msg'],
					   "Empty_No_Person"	 	=> $lang['Empty_No_Person'],
					   "Valid_No_Person"	 	=> $lang['Valid_No_Person'],					   
					   "Empty_No_Adult_Msg"	 	=> $lang['Empty_No_Adult_Msg'],
					   "Valid_No_Adult_Msg" 	=> $lang['Valid_No_Adult_Msg'],
					   "Valid_No_Child_Msg" 	=> $lang['Valid_No_Child_Msg'],
					   "Empty_Hotel_Name" 		=> $lang['Empty_Hotel_Name'],
					   "Valid_Date"       		=> $lang['Valid_Date'],
					   "Empty_Check_Box"  		=> $lang['Empty_Check_Box'],
					   "Valid_Arrival_Date" 	=> $lang['Valid_Arrival_Date'],
					   "Empty_Airline_Name" 	=> $lang['Empty_Airline_Name'],
					   "Empty_Flight_No"    	=> $lang['Empty_Flight_No'],
					   "Empty_Arrival_Date"    	=> $lang['Valid_Arrival_Date'],
					   "Empty_Departure_Date"   => $lang['Valid_Departure_Date'],
					   "Valid_Departure_Date"	=> $lang['Valid_Departure_Date'],
					   "Valid_BothDate_Msg" 	=> $lang['Valid_BothDate_Msg'],
					   "arrivalDate"	  		=> $_SESSION['arrivalDate'],
					   "departureDate"	  		=> $_SESSION['departureDate'],
					   "Valid_Min_Of_Passengers"=> $lang['Valid_Min_Of_Passengers'],
					));


	$tpl->assign(array( "V_Arrival_Hour"	=>	fillArrayCombo($lang['arrHour']),
						"V_Arrival_Minute"	=>	fillArrayCombo($lang['arrMinute']),
						"V_ArrivalDayPart"	=>	fillArrayCombo($lang['arrDayPart']),						
						"V_Departure_Hour"	=>	fillArrayCombo($lang['arrHour']),
						"V_Departure_Minute"=>	fillArrayCombo($lang['arrMinute']),
						"V_DepartureDayPart"=>	fillArrayCombo($lang['arrDayPart']),
						"V_Pickup_Hour"		=>	fillArrayCombo($lang['arrHour']),
						"V_Pickup_Minute"	=>	fillArrayCombo($lang['arrMinute']),
						"V_Pickup_DayPart"	=>	fillArrayCombo($lang['arrDayPart']),						
						));



}					
	
elseif($Action == RESERVEINFO)
{

	$record  		= Country_List();
	$countryList	= fillDbCombo($record, "country_name", "country_name", $country_id);
	
	$tpl->assign(array( 'T_Body'		=>	'reservationInfo'. $config['tplEx'],
						'JavaScript'	=>	array('reservationInfo.js','cart.js','popcalendar.js'),
						'A_Action'		=>	$scriptName,
						"Header"		=>  "Resv_Rates",
						'CountryList'	=>	$countryList,
						'PassengerRange'=>	$objDest->getAllPassenger(),					
						));

	$tpl->assign(array( 'Rate'			=>	$rate,
						'PassengerId'	=>	$passenger_id,
						'TriptypeId'	=>	$triptype_id,
						'dest_id'		=>	$dest_id,
						'index_id'		=>	$index_id,
						));

	$tpl->assign(array(	"site_res_img"  		=> $lang['site_res_img'],
						"L_Personal_Information"=> $lang['L_Personal_Information'],
						"L_Applicable_Discount" => $lang['L_Applicable_Discount'],
						"L_Company_Name" 		=> $lang['L_Company_Name'],						
						"L_Company_ID" 			=> $lang['L_Company_ID'],		
						"L_Phone_Number" 		=> $lang['L_Phone_Number'],								
						));

	$tpl->assign(array(	"L_Reservation_Details"  => $lang['L_Reservation_Details'],
						"L_Res_Info"  			 => $lang['L_Res_Info'],
						"ACTION"				 => RESERVEINFO,
						"SUBMIT"				 => A_SUBMIT,							
						"L_Requested_Service"	 => $lang['L_Requested_Service'],	
						"L_Rate"  				 => $lang['L_Rate'],
						"Send"					 => $lang['L_Send'],	
						"Submit"  				 => $lang['submit'],
					    "Cancel"    	  		 => $lang['cancel'],
						"L_personal_Information" =>	$lang['L_personal_Information'],
						"L_cartItem"			 => $lang['L_cartItem'],
						"L_ItemType"			 => $lang['L_ItemType'],
						"L_RequestNo"   		 => $lang['RequestNo'],
						"L_Total"    			 => $lang['L_Total'],
						"L_HotelInfo"			 =>	$lang['L_HotelInfo'],
						"L_Airline_Employee"	 =>	$lang['L_Airline_Employee'],
						"L_Is_Airline_Employee"	 =>	$lang['L_Is_Airline_Employee'],
						"L_ID"	  		 		 =>	$lang['L_ID'],
						"L_Airline_Name"		 =>	$lang['L_Airline_Name'],
						"L_First_Name"			 =>	$lang['L_First_Name'],
						"L_Last_Name"	  		 =>	$lang['L_Last_Name'],
						"L_Email"				 =>	$lang['L_Email'],
						"L_Address"				 =>	$lang['L_Address'],
						"L_Country"				 =>	$lang['L_Country'],
						"L_City"				 =>	$lang['L_City'],
						"L_State"				 =>	$lang['L_State'],
						"L_Zip"					 =>	$lang['L_Zip'],
						"L_Phone_Number" 		 =>	$lang['L_Phone_Number'],
						"L_Fax"					 =>	$lang['L_Fax'],
						"L_Special_Request"		 => $lang['L_Special_Request'],
						"L_Select_Country" 		 => $lang['L_Select_Country'],
						"Select_Airline_Employee"=> $lang['Select_Airline_Employee'],
						"Empty_ID"		 		 => $lang['Empty_ID'],
						"Empty_Airline_Name"  	 => $lang['Empty_Airline_Name'],
						"Empty_First_Name" 		 => $lang['Empty_First_Name'],
						"Empty_Last_Name"  		 => $lang['Empty_Last_Name'],
						"Empty_Email"	  		 => $lang['Empty_Email'],
						"Valid_Email"	   		 => $lang['Valid_Email'],
						"Empty_Address"	  		 => $lang['Empty_Address'],
						"Empty_Country"	  		 => $lang['Empty_Country'],
						"Empty_City"	  		 => $lang['Empty_City'],
						"Empty_State"	  		 => $lang['Empty_State'],
						"Empty_Zip"		  		 => $lang['Empty_Zip'],
						"Valid_Zip"		  		 => $lang['Valid_Zip'],
						"Valid_Fax"		  		 => $lang['Valid_Fax'],
						"Empty_Area_Code"  		 => $lang['Empty_Area_Code'],
						"Empty_City_Code"  		 => $lang['Empty_City_Code'],
						"Empty_Phone_No"   		 => $lang['Empty_Phone_No'],
						"Valid_PhoneNo"    		 => $lang['Valid_PhoneNo']));

	$tpl->assign(array( "A_Cart"				=> "cart.php?&start=$start_record",
						"L_cartItem"			=> $lang['L_cartItem'],
						"L_ItemType"			=> $lang['L_ItemType'],
						"L_Cart"				=> $lang['L_Cart'],
						"L_Final"	 		 	=>	$lang['L_Final'],
						"L_Discount"	 		=>	$lang['L_Discount'],
						"Modify"   			    => $lang['Modify'],
						"Confirm_Delete"	    => $lang['msgConfirmDelete'],
						"Delete"   			    => $lang['Delete'],
						"Message"				=> $succ_message,
					    "L_Total"   			=> $lang['L_Total'],
						"L_ReserveMore"			=> $lang['L_ReserveMore'],
						"L_Send_Reservation_Request" => $lang['L_Send_Reservation_Request'],
						"Valid_Delete_Cart"		=> $lang['Valid_Delete_Cart'],
						"Valid_Select_Cart"		=> $lang['Valid_Select_Cart'],
						));
	 	
		$SortBy = "itemType";
		$tpl->assign(array( "L_Reservation_Details" => $lang['L_Reservation_Details'],
							"L_Checkout"			=> $lang['L_Checkout'],
							"L_ReserveMore"		    => $lang['L_ReserveMore'],
							"ReserveMoreLink"		=> $url_path['Site_URL']."ground_transportation_rates.php",
						));
		
		$tpl->assign("flag",$_POST['flag']);
		$j=0;
		while($j < count($_POST['chkCartId']))
		{
			$tpl->assign("cartId",$_POST['chkCartId'][$j]);	
			$j++;	
		}
	
		$tpl->assign("chk_cart_id",$_POST['chkCartId']);	
					
		if(count($_POST['chkCartId']))
		{
			$chkCartId = implode(",", $_POST['chkCartId']);
		}	

		$result				=	$objDisco->getAllDisco(" GROUP BY discount_id ORDER BY discount_title ");
		$airline_empl_list 	=	fillDbCombo($result,'discount_id','discount_title',$discount_id);
	
		$tpl->assign(array( "Airline_Empl_List" => $airline_empl_list,
						));

		$sql = $objUser -> ShowUserInfo($index_id); 

		$db->next_record();
		$is_airline_empl	=	$db->f('is_airline_empl');
		$discount_rate		=	$db->f('discount_rate');

		$tpl->assign(array("is_airline_empl"	=> $db->f('is_airline_empl'),
						   "discount_id"     	=> $db->f('discount_id'),
						   "discount_rate"	    => $db->f('discount_rate'),
						   "discount_title"	    => ucfirst($db->f('discount_title')),
						   "empl_id" 	    	=> $db->f('empl_id'),
						   "empl_airline"		=> ucfirst($db->f('empl_airline')),
						   ));
	
		$tpl->assign(array( 'CartInfo'		=>	$objCart -> Show_UserCart($chkCartId),
							));
							
	$sql = $objCart -> Show_CartDest_Detail($SortBy,$start_record,$Page_Size);

	$i = 0;
	$rscnt = $db->num_rows();

	$grandTotal = 0;

	while($i < $rscnt)
	{
		$db->next_record();

		$grandTotal += $db->f('totalCharge');
		$i++;	
	}

	if($is_airline_empl == 1)
	{
		$grandTotal = $grandTotal - (($discount_rate*$grandTotal)/100);
	}

	$tpl->assign("V_grandTotal",number_format($grandTotal,2,'.',','));
			
	if($num_records >= $Page_Size)
		$tpl->assign("Page_Link", generate_pagination($num_records, $Page_Size, $start_record, $add_prevnext_text = TRUE));
}		

	$tpl->display('default_layout'. $config['tplEx']);
	
?>
<?php include_once("analyticstracking.php") ?>
