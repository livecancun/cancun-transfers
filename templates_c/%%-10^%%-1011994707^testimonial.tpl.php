<?php /* Smarty version 2.6.0, created on 2011-06-02 21:02:09
         compiled from testimonial.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'testimonial.tpl', 37, false),array('modifier', 'regex_replace', 'testimonial.tpl', 43, false),array('modifier', 'cat', 'testimonial.tpl', 68, false),array('modifier', 'nl2br', 'testimonial.tpl', 75, false),)), $this); ?>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<table border="0" cellpadding="1" cellspacing="1" width="100%">
<form name="frmTestimonial" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" height="100%" border="0" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<table width="92%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top">
												<h1 align='justify' style="margin-bottom:0px" class='style6 Estilo1 Estilo8 Estilo39'><?php echo $this->_tpl_vars['Testimonials']; ?>

												&nbsp;[ <a href="javascript: Add_Click('<?php echo $this->_tpl_vars['Add_Action']; ?>
');" class="actionLink"><?php echo $this->_tpl_vars['Add']; ?>
</a> ]</h1>
											</td>
										</tr>
										<tr>
											<td valign="top">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
														<?php if ($this->_tpl_vars['SuccMessage']): ?>
														<tr>
															<td class="successMsg"><br /><?php echo $this->_tpl_vars['SuccMessage']; ?>
<br /></td>
														</tr>
														<?php endif; ?>

														<tr>
															<td valign="top" align="center">
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td valign="top" align="center">
																			<table border="0" cellpadding="2" cellspacing="2" width="100%">
																				
																				<tr><td height="1" class="dividerBg"></td></tr>
																				<?php if (count($_from = (array)$this->_tpl_vars['PersonData'])):
    foreach ($_from as $this->_tpl_vars['Person']):
?>    
																				<tr class="<?php echo smarty_function_cycle(array('values' => 'list_A, list_B'), $this);?>
">
																					<td valign="top">
																						<table border="0" cellpadding="0" cellspacing="0" width="100%">
																							
																							<tr>
																								<td align="left">
																								<b><?php echo ((is_array($_tmp=$this->_tpl_vars['Person']->person_name)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")); ?>
</b><br>
																								<?php if ($this->_tpl_vars['Person']->person_city): ?>
																									<?php echo ((is_array($_tmp=$this->_tpl_vars['Person']->person_city)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")); ?>
,&nbsp;

																									<?php if ($this->_tpl_vars['Person']->person_state): ?>
																										<?php echo ((is_array($_tmp=$this->_tpl_vars['Person']->person_state)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")); ?>
<br>
																									<?php endif; ?>	
																									
																									<?php if ($this->_tpl_vars['Person']->person_country): ?>	
																										<?php echo $this->_tpl_vars['Person']->person_country; ?>
<Br />
																									<?php endif; ?>
																									
																								<?php endif; ?>
																								<?php if ($this->_tpl_vars['Person']->person_phone): ?>
																								Tel: <?php echo ((is_array($_tmp=$this->_tpl_vars['Person']->person_phone)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")); ?>
<br>
																								<?php endif; ?>
																								<?php if ($this->_tpl_vars['Person']->person_fax): ?>
																								Fax: <?php echo ((is_array($_tmp=$this->_tpl_vars['Person']->person_fax)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")); ?>
<br>
																								<?php endif; ?>
																								<?php if ($this->_tpl_vars['Person']->person_email): ?>
																								<a href="mailto:<?php echo ((is_array($_tmp=$this->_tpl_vars['Person']->person_email)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")); ?>
" class="siteLink"><?php echo ((is_array($_tmp=$this->_tpl_vars['Person']->person_email)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")); ?>
</a><br>
																								<?php endif; ?>
																								</td>
																								<?php if ($this->_tpl_vars['Person']->person_pic): ?>
																								<td>
																								<img src="<?php if ($this->_tpl_vars['Person']->person_pic): ?> <?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['Testimonial_Path'])) ? $this->_run_mod_handler('cat', true, $_tmp, 'thumb_') : smarty_modifier_cat($_tmp, 'thumb_')))) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['Person']->person_pic) : smarty_modifier_cat($_tmp, $this->_tpl_vars['Person']->person_pic)))) ? $this->_run_mod_handler('regex_replace', true, $_tmp, '/[\\\\]/', '') : smarty_modifier_regex_replace($_tmp, '/[\\\\]/', '')); ?>
 <?php else: ?> <?php echo ((is_array($_tmp=$this->_tpl_vars['Testimonial_Path'])) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['Default_Picture']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['Default_Picture'])); ?>
 <?php endif; ?>" border="0">
																								</td>
																								<?php endif; ?>
																							</tr>
																							<Tr><Td>&nbsp;</Td></Tr>
																							<tr> 
																								<td colspan="2">
																									<p align="justify"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['Person']->person_comment)) ? $this->_run_mod_handler('regex_replace', true, $_tmp, "/[\\\\]/", "") : smarty_modifier_regex_replace($_tmp, "/[\\\\]/", "")))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</p>
																								</td> 
																							</tr>
																							<tr><td height="1" class="dividerBg" colspan="2"></td></tr>
																						</table>
																					</td> 
																				</tr>
																				<?php endforeach; unset($_from); endif; ?>
																				<tr><td colspan="5">&nbsp;</td></tr>
																				<tr>
																					<td colspan="5" class="pageLink" align="right"><?php echo $this->_tpl_vars['Page_Link']; ?>
<!-- Page : 1 2 3 Next --></td>
																				</tr>
																			</table>
																		</td>
																	</tr>
																	<tr><td colspan="2">&nbsp;</td></tr>
																	<tr>
																		<td colspan="2">
																			<input type="hidden" name="person_id">
																			<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
												</table>		
											</td>
										</tr>		

																				
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</form>
</table>