<?php /* Smarty version 2.6.0, created on 2008-04-30 01:56:39
         compiled from links_addedit.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'A_Action', 'links_addedit.tpl', 2, false),array('function', 'L_Action', 'links_addedit.tpl', 8, false),array('function', 'Message', 'links_addedit.tpl', 18, false),array('function', 'url_name', 'links_addedit.tpl', 22, false),array('function', 'url_address', 'links_addedit.tpl', 26, false),array('function', 'url_city', 'links_addedit.tpl', 30, false),array('function', 'url_state', 'links_addedit.tpl', 34, false),array('function', 'url_zip', 'links_addedit.tpl', 38, false),array('function', 'url_country', 'links_addedit.tpl', 42, false),array('function', 'url_telno1', 'links_addedit.tpl', 54, false),array('function', 'url_title', 'links_addedit.tpl', 58, false),array('function', 'Link_Category', 'links_addedit.tpl', 62, false),array('function', 'cat_fill', 'links_addedit.tpl', 66, false),array('function', 'url_desc', 'links_addedit.tpl', 73, false),array('function', 'url_url', 'links_addedit.tpl', 78, false),array('function', 'url_oururl', 'links_addedit.tpl', 82, false),array('function', 'url_email', 'links_addedit.tpl', 86, false),array('function', 'Status_List', 'links_addedit.tpl', 92, false),array('function', 'Save', 'links_addedit.tpl', 105, false),array('function', 'Update', 'links_addedit.tpl', 105, false),array('function', 'Cancel', 'links_addedit.tpl', 106, false),array('function', 'ACTION', 'links_addedit.tpl', 111, false),array('function', 'Start', 'links_addedit.tpl', 112, false),array('function', 'link_id', 'links_addedit.tpl', 113, false),array('function', 'cat_id', 'links_addedit.tpl', 114, false),)), $this); ?>
<table border="0" cellpadding="0" cellspacing="1" width="595">
	<form name="frmLinkDetail"  action="<?php echo smarty_function_A_Action(array(), $this);?>
" method="post">

	<tr>
		<td class="stdSectionHeader">
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="boldHeader">Link Detail [ <?php echo smarty_function_L_Action(array(), $this);?>
 ]</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td valign="top" align="center">
			<table border="0" cellpadding="1" cellspacing="3" width="97%">
				<tr height="20">
					<td colspan="2" align="center" class="successMsg">&nbsp;<?php echo smarty_function_Message(array(), $this);?>
</td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Name :</td>
					<td class="fieldLabel" ><input name="url_name" type="text" id="url_name" value="<?php echo smarty_function_url_name(array(), $this);?>
"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Address :</td>
					<td class="fieldLabel" ><textarea name="url_address" cols="50" rows="3" id="url_address"><?php echo smarty_function_url_address(array(), $this);?>
</textarea></td>
				</tr>	
				<tr>
					<td class="fieldLabelRight" >City :</td>
					<td class="fieldLabel" ><input name="url_city" type="text" id="url_city" value="<?php echo smarty_function_url_city(array(), $this);?>
"  maxlength="255" ></td>
				</tr>	
				<tr>
					<td class="fieldLabelRight" >State  :</td>
					<td class="fieldLabel" ><input name="url_state" type="text" id="url_state" value="<?php echo smarty_function_url_state(array(), $this);?>
"  maxlength="255" ></td>
				</tr>	
				<tr>
					<td class="fieldLabelRight" >Zip :</td>
					<td class="fieldLabel" ><input name="url_zip" type="text" id="url_zip" value="<?php echo smarty_function_url_zip(array(), $this);?>
"  maxlength="5" ></td>
				</tr>	
				<tr>
					<td class="fieldLabelRight" >Country :</td>
					<td class="fieldLabel" ><input name="url_country" type="text" id="url_country" value="<?php echo smarty_function_url_country(array(), $this);?>
"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Telephone :</td>
					<td class="fieldInputStyle">
						<input type="text" name="url_telno_areacode" size="3" maxlength="3" value="<?php echo $this->_tpl_vars['url_telno_areacode']; ?>
" onKeyPress="javascript: isNumericKey(this.value)"> -
						<input type="text" name="url_telno_citycode" size="3" maxlength="3" value="<?php echo $this->_tpl_vars['url_telno_citycode']; ?>
" onKeyPress="javascript: isNumericKey(this.value)"> -
						<input type="text" name="url_telno_no" size="4" maxlength="4" value="<?php echo $this->_tpl_vars['url_telno_no']; ?>
" onKeyPress="javascript: isNumericKey(this.value)">
					</td>	
				</tr>	
				<!--tr>
					<td class="fieldLabelRight" >Telephone :</td>
					<td class="fieldLabel" ><input name="url_telno1" type="text" id="url_telno1" value="<?php echo smarty_function_url_telno1(array(), $this);?>
"  maxlength="255" ></td>
				</tr-->	
				<tr>
					<td class="fieldLabelRight" >Link Title  :</td>
					<td class="fieldLabel" ><input name="url_title" type="text" value="<?php echo smarty_function_url_title(array(), $this);?>
" size="55"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight" valign="top" width="35%" >Link Catagory : </td>
					<td width="75%" class="fieldLabel"> <?php echo smarty_function_Link_Category(array(), $this);?>
	</td>
					<!--td width="75%" class="fieldLabel"> 
					<select name="link_catagory">
					<option value="">Select Catagory</option>
					<?php echo smarty_function_cat_fill(array(), $this);?>

					</select>
					</td-->
				</tr>	
				<tr>
					<td width="35%" class="fieldLabelRight" valign="top">Description : </td>
					<td>
					<textarea name="url_desc" rows="3" cols="50"><?php echo smarty_function_url_desc(array(), $this);?>
</textarea>
					</td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Link URL :</td>
					<td class="fieldLabel" ><input name="url_url" type="text" value="<?php echo smarty_function_url_url(array(), $this);?>
" size="55"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight" >Our URL link in your site :</td>
					<td class="fieldLabel" ><input name="url_oururl" type="text" id="url_oururl" value="<?php echo smarty_function_url_oururl(array(), $this);?>
" size="55"  maxlength="255" ></td>
				</tr>
				<tr>
					<td class="fieldLabelRight">Link Email :</td>
					<td class="fieldLabel" ><input type="text" name="url_email" value="<?php echo smarty_function_url_email(array(), $this);?>
"  maxlength="255" ></td>
				</tr>
				<tr>
					<!--td class="fieldLabelRight">Status :</td>
					<td width="75%" class="fieldLabel"> 
					<select name = "cat_status">
					<?php echo smarty_function_Status_List(array(), $this);?>

					</select>
					</td-->
				</tr>
				<tr><td>&nbsp;</td></tr>
				<!--tr>
					<td colspan="8" align="center" >
						<input  type="submit" name="Submit"  value="Save"   class="nrlButton" onClick="javascript: return Form_Submit(document.frmLinkDetail);">&nbsp;&nbsp;&nbsp;
						<input  type="submit" name="Submit"  value="Cancel" class="nrlButton" >
					</td>
				</tr-->
				<tr>
					<td colspan="8" align="center" >
						<input type="submit" name="Submit" value="<?php echo smarty_function_Save(array(), $this); echo smarty_function_Update(array(), $this);?>
" class="nrlButton" onClick="javascript: return Form_Submit(document.frmLinkDetail);">
						<input type="submit" name="Submit" value="<?php echo smarty_function_Cancel(array(), $this);?>
" class="nrlButton">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="hidden" name="Action" value="<?php echo smarty_function_ACTION(array(), $this);?>
">
						<input type="hidden" name="start" value="<?php echo smarty_function_Start(array(), $this);?>
">
						<input type="hidden" name="link_id" value="<?php echo smarty_function_link_id(array(), $this);?>
">
						<input type="hidden" name="cat_id" value="<?php echo smarty_function_cat_id(array(), $this);?>
">
						<input type="hidden" name="cat_status" value="0">
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</form>
</table>