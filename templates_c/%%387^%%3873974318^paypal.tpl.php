<?php /* Smarty version 2.6.0, created on 2018-09-17 11:09:33
         compiled from paypal.tpl */ ?>
<table border="0" cellpadding="2" cellspacing="2" width="98%">
	<tr>
		<td height="10">&nbsp;</td>
	</tr>
	<tr>
		<td align="center" width="100%"><br><br>
			<p class="Estilo10"><span class="texto_content"><?php echo $this->_tpl_vars['L_Paypal_Msg']; ?>
</span></p>
		</td>
	</tr>						
	<tr>
		<td height="10">&nbsp;</td>
	</tr>
</table>
<form name='frmPayment' 	action='https://www.paypal.com/cgi-bin/webscr' method='post'>
	<input type="hidden" 	name="cmd" 						value="_ext-enter">
	<input type="hidden" 	name="redirect_cmd" 			value="_xclick">
	<input type='hidden' 	name='business' 				value='<?php echo $this->_tpl_vars['paypal_id']; ?>
'>

 	<!-- Order Info -->
	<input type="hidden" 		name="item_name" 				value="<?php echo $this->_tpl_vars['Firstname']; ?>
, <?php echo $this->_tpl_vars['Lastname']; ?>
, <?php echo $this->_tpl_vars['Address1']; ?>
, <?php echo $this->_tpl_vars['City']; ?>
, <?php echo $this->_tpl_vars['State']; ?>
">

	<?php if (isset($this->_sections['Cart'])) unset($this->_sections['Cart']);
$this->_sections['Cart']['name'] = 'Cart';
$this->_sections['Cart']['loop'] = is_array($_loop=$this->_tpl_vars['chkdestTitle']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Cart']['show'] = true;
$this->_sections['Cart']['max'] = $this->_sections['Cart']['loop'];
$this->_sections['Cart']['step'] = 1;
$this->_sections['Cart']['start'] = $this->_sections['Cart']['step'] > 0 ? 0 : $this->_sections['Cart']['loop']-1;
if ($this->_sections['Cart']['show']) {
    $this->_sections['Cart']['total'] = $this->_sections['Cart']['loop'];
    if ($this->_sections['Cart']['total'] == 0)
        $this->_sections['Cart']['show'] = false;
} else
    $this->_sections['Cart']['total'] = 0;
if ($this->_sections['Cart']['show']):

            for ($this->_sections['Cart']['index'] = $this->_sections['Cart']['start'], $this->_sections['Cart']['iteration'] = 1;
                 $this->_sections['Cart']['iteration'] <= $this->_sections['Cart']['total'];
                 $this->_sections['Cart']['index'] += $this->_sections['Cart']['step'], $this->_sections['Cart']['iteration']++):
$this->_sections['Cart']['rownum'] = $this->_sections['Cart']['iteration'];
$this->_sections['Cart']['index_prev'] = $this->_sections['Cart']['index'] - $this->_sections['Cart']['step'];
$this->_sections['Cart']['index_next'] = $this->_sections['Cart']['index'] + $this->_sections['Cart']['step'];
$this->_sections['Cart']['first']      = ($this->_sections['Cart']['iteration'] == 1);
$this->_sections['Cart']['last']       = ($this->_sections['Cart']['iteration'] == $this->_sections['Cart']['total']);
?>
		<input type="hidden" 	name="on<?php echo $this->_sections['Cart']['iteration']-1; ?>
" value="<?php echo $this->_tpl_vars['L_Reservation'];  echo $this->_sections['Cart']['iteration']; ?>
">		
		<input type="hidden" 	name="os<?php echo $this->_sections['Cart']['iteration']-1; ?>
" value="<?php echo $this->_tpl_vars['chkdestTitle'][$this->_sections['Cart']['index']]; ?>
">
	<?php endfor; endif; ?> 

	<input type='hidden' 	name='amount' 					value='<?php echo $this->_tpl_vars['V_paymentTotal']; ?>
'>
	<input type='hidden' 	name='no_shipping' 				value='1'>
	<input type='hidden' 	name='x_invoice_num' 			value='<?php echo $this->_tpl_vars['order_id']; ?>
'>

 	<!-- Billing information  -->
	<input type='hidden' 	name='first_name' 				value='<?php echo $this->_tpl_vars['Firstname']; ?>
'>
	<input type='hidden' 	name='last_name' 				value='<?php echo $this->_tpl_vars['Lastname']; ?>
'>
	<input type='hidden' 	name='address1' 				value='<?php echo $this->_tpl_vars['Address1']; ?>
'>
	<input type='hidden' 	name='city' 					value='<?php echo $this->_tpl_vars['City']; ?>
'>
	<input type='hidden' 	name='state' 					value='<?php echo $this->_tpl_vars['State']; ?>
'>
	<input type='hidden' 	name='zip' 						value='<?php echo $this->_tpl_vars['Zip']; ?>
'>
	<input type='hidden' 	name='country_code' 			value='<?php echo $this->_tpl_vars['Country']; ?>
'>
	<input type='hidden' 	name='night_phone_a' 			value='<?php echo $this->_tpl_vars['Phone_No']; ?>
'>
	<input type='hidden' 	name='email' 					value='<?php echo $this->_tpl_vars['Email']; ?>
'>

 	<!-- Additional Parameters -->
	<input type='hidden' 	name='lc' 						value='en'>
    <input type="hidden" 	name="bn" 						value="PP-BuyNowBF">	
	<input type='hidden' 	name='merchant_order_id' 		value='<?php echo $this->_tpl_vars['order_id']; ?>
'>
	<input type='hidden' 	name='currency_code' 			value='<?php echo $this->_tpl_vars['paypal_currency']; ?>
'>
	<input type='hidden' 	name='no_note' 					value='1'>

 	<!-- Return Parameters -->
	<input type='hidden' 	name='return' 					value='<?php echo $this->_tpl_vars['Site_Root']; ?>
paymentProcess.php?Action=Complete'>
	<input type='hidden' 	name='cancel_return' 			value='<?php echo $this->_tpl_vars['Site_Root']; ?>
paymentProcess.php?Action=Cancel'>

 	<!-- Custom Parameters -->
	<input type='hidden' 	name='customer_id' 				value='<?php echo $this->_tpl_vars['auth_id']; ?>
'>
	<input type="hidden" 	name="custom" 					value="0">
	<input type="hidden" 	name="rm" 						value="2">
	
</form>
<script>setTimeout('document.frmPayment.submit();', 5000);</script>
