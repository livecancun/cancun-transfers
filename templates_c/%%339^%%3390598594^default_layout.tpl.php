<?php /* Smarty version 2.6.0, created on 2018-09-17 15:46:01
         compiled from default_layout.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cancun transfers - Private Cancun Airport Transfers</title>
<meta name="description" content="Cancun transfers and private shuttles to Cancun Airport - Hotel - Cancun Airport" />
<meta name="keywords" content="Cancun tranfers, cancun shuttles, cancun airport tranfers, cancun ground transporttation" />
<meta name="robots" content="index,follow" />

<link href="<?php echo $this->_tpl_vars['Templates_CSS']; ?>
table.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->_tpl_vars['Templates_CSS']; ?>
train.css" rel="stylesheet" type="text/css">
<link href="<?php echo $this->_tpl_vars['Templates_JS']; ?>
jquery.ui.all.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
jquery-1.4.2.min.js"></script>
<script src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
jquery-ui-1.8.custom.min.js"></script>
<script src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
jquery.ui.datepicker-es.js"></script>
<script language="javascript" >
<?php if ($this->_tpl_vars['lng'] == 'en'): ?>
	var vLangue=1;
<?php else: ?>
	var vLangue=0;
<?php endif; ?>
</script>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
validate.js"></script>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
functions.js"></script>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
index.js"></script>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
menu.js"></script>
<?php if (isset($this->_sections['FileName'])) unset($this->_sections['FileName']);
$this->_sections['FileName']['name'] = 'FileName';
$this->_sections['FileName']['loop'] = is_array($_loop=$this->_tpl_vars['JavaScript']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['FileName']['show'] = true;
$this->_sections['FileName']['max'] = $this->_sections['FileName']['loop'];
$this->_sections['FileName']['step'] = 1;
$this->_sections['FileName']['start'] = $this->_sections['FileName']['step'] > 0 ? 0 : $this->_sections['FileName']['loop']-1;
if ($this->_sections['FileName']['show']) {
    $this->_sections['FileName']['total'] = $this->_sections['FileName']['loop'];
    if ($this->_sections['FileName']['total'] == 0)
        $this->_sections['FileName']['show'] = false;
} else
    $this->_sections['FileName']['total'] = 0;
if ($this->_sections['FileName']['show']):

            for ($this->_sections['FileName']['index'] = $this->_sections['FileName']['start'], $this->_sections['FileName']['iteration'] = 1;
                 $this->_sections['FileName']['iteration'] <= $this->_sections['FileName']['total'];
                 $this->_sections['FileName']['index'] += $this->_sections['FileName']['step'], $this->_sections['FileName']['iteration']++):
$this->_sections['FileName']['rownum'] = $this->_sections['FileName']['iteration'];
$this->_sections['FileName']['index_prev'] = $this->_sections['FileName']['index'] - $this->_sections['FileName']['step'];
$this->_sections['FileName']['index_next'] = $this->_sections['FileName']['index'] + $this->_sections['FileName']['step'];
$this->_sections['FileName']['first']      = ($this->_sections['FileName']['iteration'] == 1);
$this->_sections['FileName']['last']       = ($this->_sections['FileName']['iteration'] == $this->_sections['FileName']['total']);
?>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS'];  echo $this->_tpl_vars['JavaScript'][$this->_sections['FileName']['index']]; ?>
"></script>
<?php endfor; endif; ?>
  
</head>

<body onload="MM_preloadImages('images/home-hover.gif','images/about-hover.gif','images/order-hover.gif','images/contact-hover.gif','images/affiliates-hover.gif','images/newsletter-hover.gif')">

			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['T_Header']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> 
		
						<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['T_Menu']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					
						<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['T_Body']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['T_Footer']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


</body>
</html>