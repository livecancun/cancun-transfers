<?php /* Smarty version 2.6.0, created on 2018-09-17 15:46:01
         compiled from rates.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'rates.tpl', 36, false),array('modifier', 'array_value', 'rates.tpl', 52, false),)), $this); ?>

<?php if ($this->_tpl_vars['lng'] == 'en'): ?>
                    
<br /><img src="images/cancun-transfers-payment.jpg" alt="Payments methods" /><br/>
<img src="images/pay-online.png"><br><br>
<p>
We offer <strong>transportation service</strong> where travel comfortable clean units, with air conditioning, insured, arrive fast and without making ranks at more accessible prices of the market, <strong>Trip to Cancun, Playa del Carmen or the Riviera maya</strong> in a first transportation service upon arrival we will be waiting at the airport and his departure will go through you to your hotel at the time indicated.<br /><br />
Below you will find a list of prices and destinations, choose your destination and <strong>pay online the easiest way</strong>, or if you wish to <strong>pay directly on arrival. <a href="reserve.html">Please fill this Easy form</a></strong>
<br />
<a href="reserve.html"><img src="images/pay-airport.png" border="0"></a><br />
<br />
Thank you for booking with Cancun-transfers.com, if you can't find the place of destination, please contact us, with pleasure will help you out.<br />

</p>
<?php else: ?>


<br /><br />
<img src="images/pago-online.png" /><br />
<img src="images/cancun-transfers-payment-es.jpg" alt="Metodos de pago" /><br />
<p>
Ofrecemos <strong>servicio de transportacion</strong> en donde viajara comodo en  unidades limpias, con aire acondicionado, aseguradas, llegue rapido y sin hacer filas a los precios mas accesibles del mercado, Viaje a <strong>Cancun, Playa del Carmen o a la Riviera maya</strong> en un servicio de transportacion de primera, a su llegada estaremos esperandolo en el aeropuerto y a su partida pasaremos por usted a su hotel a la hora indicada.<br /><br />
A continuacion encontrara una lista de precios y destinos, elija su destino y <strong>pague en linea de la manera mas sencilla</strong>, o si lo desea <strong>pague directamente a su llegada <a href="es/reservar.html">llenando este formulario sencillo</strong></a>. <br /><a href="es/reservar.html"><img src="images/pago-aeropuerto.png" border="0" /></a><br /><br />
Gracias por reservar con Cancun-transfers.com, la siguiente lista contiene los destinos y hoteles para reservar en linea, si su hotel no aparece en el listado contactenos que con gusto le ayudaremos.<br />

</p>
<?php endif; ?>


<table class="table1" border="0" cellspacing="1" cellpadding="0">
<FORM name="frmRates" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
<thead>
                    <tr>
                        <th scope="col" abbr="Starter"><?php echo $this->_tpl_vars['L_Destinations']; ?>
</th>
                        <?php if (isset($this->_foreach['PassengerRange'])) unset($this->_foreach['PassengerRange']);
$this->_foreach['PassengerRange']['name'] = 'PassengerRange';
$this->_foreach['PassengerRange']['total'] = count($_from = (array)$this->_tpl_vars['PassengerRange']);
$this->_foreach['PassengerRange']['show'] = $this->_foreach['PassengerRange']['total'] > 0;
if ($this->_foreach['PassengerRange']['show']):
$this->_foreach['PassengerRange']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Passenger']):
        $this->_foreach['PassengerRange']['iteration']++;
        $this->_foreach['PassengerRange']['first'] = ($this->_foreach['PassengerRange']['iteration'] == 1);
        $this->_foreach['PassengerRange']['last']  = ($this->_foreach['PassengerRange']['iteration'] == $this->_foreach['PassengerRange']['total']);
?>
									<?php echo smarty_function_assign(array('var' => 'PsngrId','value' => $this->_tpl_vars['Passenger']['passenger_id']), $this);?>

                        <th scope="col" abbr="Starter"><strong><?php echo $this->_tpl_vars['Passenger']['passenger_starting_range']; ?>
 <?php echo $this->_tpl_vars['L_To']; ?>

																	<?php echo $this->_tpl_vars['Passenger']['passenger_ending_range']; ?>
 Pax</strong></th>

<?php endforeach; unset($_from); endif; ?>              <th scope="col" abbr="Deluxe"><img src="images/save-transfers.png" /> Save </th>
                    </tr>
                </thead>
                <tfoot>

                </tfoot>
                <tbody>
                    <tr>
                        <?php if (isset($this->_foreach['DestinationInfo'])) unset($this->_foreach['DestinationInfo']);
$this->_foreach['DestinationInfo']['name'] = 'DestinationInfo';
$this->_foreach['DestinationInfo']['total'] = count($_from = (array)$this->_tpl_vars['DestinationInfo']);
$this->_foreach['DestinationInfo']['show'] = $this->_foreach['DestinationInfo']['total'] > 0;
if ($this->_foreach['DestinationInfo']['show']):
$this->_foreach['DestinationInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Destination']):
        $this->_foreach['DestinationInfo']['iteration']++;
        $this->_foreach['DestinationInfo']['first'] = ($this->_foreach['DestinationInfo']['iteration'] == 1);
        $this->_foreach['DestinationInfo']['last']  = ($this->_foreach['DestinationInfo']['iteration'] == $this->_foreach['DestinationInfo']['total']);
?>
                        <th scope="row"><?php echo $this->_tpl_vars['Destination']['dest_title']; ?>
&nbsp;&nbsp;</th>
                        <!--Loop TD -->
                        <?php if (isset($this->_foreach['R_RecordSet'])) unset($this->_foreach['R_RecordSet']);
$this->_foreach['R_RecordSet']['name'] = 'R_RecordSet';
$this->_foreach['R_RecordSet']['total'] = count($_from = (array)$this->_tpl_vars['R_RecordSet']);
$this->_foreach['R_RecordSet']['show'] = $this->_foreach['R_RecordSet']['total'] > 0;
if ($this->_foreach['R_RecordSet']['show']):
$this->_foreach['R_RecordSet']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Record']):
        $this->_foreach['R_RecordSet']['iteration']++;
        $this->_foreach['R_RecordSet']['first'] = ($this->_foreach['R_RecordSet']['iteration'] == 1);
        $this->_foreach['R_RecordSet']['last']  = ($this->_foreach['R_RecordSet']['iteration'] == $this->_foreach['R_RecordSet']['total']);
?>
								<?php echo smarty_function_assign(array('var' => 'PriceData','value' => ((is_array($_tmp='price_data')) ? $this->_run_mod_handler('array_value', true, $_tmp, $this->_tpl_vars['Record']) : smarty_modifier_array_value($_tmp, $this->_tpl_vars['Record']))), $this);?>

                        <!--Inicio generacion TD -->
                        <td><table class="table1" border="0" cellspacing="0" cellpadding="0">
											<tr>
											 <?php if (isset($this->_foreach['TripTypeInfo'])) unset($this->_foreach['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['name'] = 'TripTypeInfo';
$this->_foreach['TripTypeInfo']['total'] = count($_from = (array)$this->_tpl_vars['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['show'] = $this->_foreach['TripTypeInfo']['total'] > 0;
if ($this->_foreach['TripTypeInfo']['show']):
$this->_foreach['TripTypeInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['TripType']):
        $this->_foreach['TripTypeInfo']['iteration']++;
        $this->_foreach['TripTypeInfo']['first'] = ($this->_foreach['TripTypeInfo']['iteration'] == 1);
        $this->_foreach['TripTypeInfo']['last']  = ($this->_foreach['TripTypeInfo']['iteration'] == $this->_foreach['TripTypeInfo']['total']);
?>
													
												<td>
													
														<strong><?php echo $this->_tpl_vars['Record']['price_data'][$this->_tpl_vars['Destination']['dest_id']][$this->_tpl_vars['TripType']['triptype_id']]; ?>
</strong>
													
                                                    <?php if ($this->_tpl_vars['TripType']['triptype_id'] != 1): ?>
							<br /><input name="reserve" type="image" onclick="JavaScript: Reserve_Click(document.frmRates,<?php echo $this->_tpl_vars['PsngrId']; ?>
,<?php echo $this->_tpl_vars['TripType']['triptype_id']; ?>
, <?php echo $this->_tpl_vars['Destination']['dest_id']; ?>
,<?php echo $this->_tpl_vars['Record']['price_data'][$this->_tpl_vars['Destination']['dest_id']][$this->_tpl_vars['TripType']['triptype_id']]; ?>
 );" class="texto_content" src="images/round-trip-transfer.png" value="<?php echo $this->_tpl_vars['L_Reserve']; ?>
">
						<?php else: ?>
						<br /><input name="reserve" type="image" onclick="JavaScript: Reserve_Click(document.frmRates,<?php echo $this->_tpl_vars['PsngrId']; ?>
,<?php echo $this->_tpl_vars['TripType']['triptype_id']; ?>
, <?php echo $this->_tpl_vars['Destination']['dest_id']; ?>
,<?php echo $this->_tpl_vars['Record']['price_data'][$this->_tpl_vars['Destination']['dest_id']][$this->_tpl_vars['TripType']['triptype_id']]; ?>
 );" class="texto_content" src="images/one-way-transfer.png" value="<?php echo $this->_tpl_vars['L_Reserve']; ?>
"><?php endif; ?> 
                            
                                                    
                                                    
                                                    
													
												
												</td>
				
											  <?php endforeach; unset($_from); endif; ?>
													  
											</tr>
									   </table></td>
                        <!--Fin Generacion TD -->
                         <?php endforeach; unset($_from); endif; ?>
 <!--TD para descuento sin loop -->
                        <td>20%</td>
<!--Fin TD para descuento sin loop -->
                    </tr>
                    <?php endforeach; unset($_from); endif; ?><!--Foreach de destinations y geneacion de todos xxxxxx -->
                   
<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
				<input type="hidden" name="rate">
				<input type="hidden" name="passenger_id">
				<input type="hidden" name="triptype_id">
				<input type="hidden" name="dest_id">
				<input type="hidden" name="rate_id">		
                </tbody>
                </FORM>
            </table>