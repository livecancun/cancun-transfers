<?php /* Smarty version 2.6.0, created on 2018-09-17 15:46:01
         compiled from default_header.tpl */ ?>
﻿<?php if ($this->_tpl_vars['lng'] == 'en'): ?>

		<center>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="389"><img src="images/header.gif" width="389" height="112" /></td>
        <td><table width="250" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="4">&nbsp;</td>
            <td background="images/call-us.jpg"><table width="242" border="0" cellspacing="0" cellpadding="0" height="62">
              <tr>
                <td>&nbsp;</td>
                <td class="search-text">&nbsp;</td>
              </tr>
            </table></td>
            <td width="4">&nbsp;</td>
          </tr>
        </table></td>
        <td width="20">&nbsp;</td>
      </tr>
    </table>
      <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="97"><a href="http://www.cancun-transfers.com/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Home','','images/home-hover.gif',1)"><img src="images/home.gif" alt="Home" name="Home" width="97" height="39" border="0" id="Home" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="116"><a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=en" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('About Us','','images/about-hover.gif',1)"><img src="images/about.gif" alt="Rates" name="Rates" width="116" height="39" border="0" id="About Us" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="93"><a href="arrivals.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Order','','images/order-hover.gif',1)"><img src="images/order.gif" alt="Arrivals" name="Order" width="93" height="39" border="0" id="Order" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="130"><a href="contact-us.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Contact Us','','images/contact-hover.gif',1)"><img src="images/contact.gif" alt="Contact Us" name="Contact Us" width="130" height="39" border="0" id="Contact Us" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="117"><a href="about-us.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Affiliates','','images/affiliates-hover.gif',1)"><img src="images/affiliates.gif" alt="About US" name="Affiliates" width="117" height="39" border="0" id="Affiliates" /></a></td>
          <td width="2"><img src="images/button-line.gif" width="2" height="39" /></td>
          <td width="131"><a href="http://www.cancun-transfers.com/es/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Newsletter','','images/newsletter-hover.gif',1)"><img src="images/newsletter.gif" alt="Español" name="Newsletter" width="131" height="39" border="0" id="Newsletter" /></a></td>
          <td background="images/button-bg.gif">&nbsp;</td>
          <td width="20"><img src="images/button-right.gif" width="20" height="39" /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="782" valign="top"><img src="images/header-a.jpg" width="782" height="272" hspace="0" vspace="0" /></td>
          <td valign="top"><table width="218" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img src="images/header-b.gif" width="218" height="131" /></td>
            </tr>
            <tr>
              <td height="149" background="images/login-bg.gif"><table width="218" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="user-heading">Private Transfers! </td>
                </tr>
                <tr>
                  <td align="left"><table width="180" border="0" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="user-subheading">&nbsp;</td>
                      <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="user-subheading">&nbsp;</td>
                      <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table></td>
                </tr>
                
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table>

	<?php else: ?>

		<center>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td bgcolor="#FFFFFF"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="389"><img src="es/images/header.gif" width="389" height="112" /></td>
        <td><table width="250" border="0" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td width="4">&nbsp;</td>
            <td background="es/images/call-us.jpg"><table width="242" border="0" cellspacing="0" cellpadding="0" height="62">
              <tr>
                <td>&nbsp;</td>
                <td class="search-text">&nbsp;</td>
              </tr>
            </table></td>
            <td width="4">&nbsp;</td>
          </tr>
        </table></td>
        <td width="20">&nbsp;</td>
      </tr>
    </table>
     <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="20">&nbsp;</td>
          <td width="97"><a href="http://www.cancun-transfers.com/es/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Home','','es/images/home-hover.gif',1)"><img src="es/images/home.gif" alt="Inicio" name="Home" width="97" height="39" border="0" id="Home" /></a></td>
          <td width="2"><img src="es/images/button-line.gif" width="2" height="39" /></td>
          <td width="116"><a href="http://www.cancun-transfers.com/cancun-transfers-rates.php?&lng=sp" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('About Us','','es/images/about-hover.gif',1)"><img src="es/images/about.gif" alt="Tarifas" name="Rates" width="116" height="39" border="0" id="About Us" /></a></td>
          <td width="2"><img src="es/images/button-line.gif" width="2" height="39" /></td>
          <td width="93"><a href="es/llegadas-aeropuerto-cancun.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Order','','es/images/order-hover.gif',1)"><img src="es/images/order.gif" alt="Llegadas" name="Order" width="93" height="39" border="0" id="Order" /></a></td>
          <td width="2"><img src="es/images/button-line.gif" width="2" height="39" /></td>
          <td width="130"><a href="es/contactanos.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Contact Us','','es/images/contact-hover.gif',1)"><img src="es/images/contact.gif" alt="Contactanos" name="Contact Us" width="130" height="39" border="0" id="Contact Us" /></a></td>
          <td width="2"><img src="es/images/button-line.gif" width="2" height="39" /></td>
          <td width="117"><a href="es/nosotros.html" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Affiliates','','es/images/affiliates-hover.gif',1)"><img src="es/images/affiliates.gif" alt="Nosotros" name="Affiliates" width="117" height="39" border="0" id="Affiliates" /></a></td>
          <td width="2"><img src="es/images/button-line.gif" width="2" height="39" /></td>
          <td width="131"><a href="http://www.cancun-transfers.com/" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Newsletter','','es/images/newsletter-hover.gif',1)"><img src="es/images/newsletter.gif" alt="English" name="Newsletter" width="131" height="39" border="0" id="Newsletter" /></a></td>
          <td background="es/images/button-bg.gif">&nbsp;</td>
          <td width="20"><img src="images/button-right.gif" width="20" height="39" /></td>
          <td width="20">&nbsp;</td>
        </tr>
      </table>
      <table width="1000" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="782" valign="top"><img src="es/images/header-a.jpg" width="782" height="272" hspace="0" vspace="0" /></td>
          <td valign="top"><table width="218" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><img src="es/images/header-b.gif" width="218" height="131" /></td>
            </tr>
            <tr>
              <td height="149" background="es/images/login-bg.gif"><table width="218" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="user-heading">Transportacion Privada! </td>
                </tr>
                <tr>
                  <td align="left"><table width="180" border="0" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="user-subheading">&nbsp;</td>
                      <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="user-subheading">&nbsp;</td>
                      <td align="left">&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </table></td>
                </tr>
                
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table>
	<?php endif; ?>