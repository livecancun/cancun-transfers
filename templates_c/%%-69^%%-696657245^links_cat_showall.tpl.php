<?php /* Smarty version 2.6.0, created on 2011-06-02 22:05:34
         compiled from links_cat_showall.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'links_cat_showall.tpl', 67, false),array('modifier', 'mod', 'links_cat_showall.tpl', 69, false),array('modifier', 'capitalize', 'links_cat_showall.tpl', 73, false),)), $this); ?>
<script language="javascript">
	var msg_Name		= '<?php echo $this->_tpl_vars['msg_Name']; ?>
';
	var msg_Address		= '<?php echo $this->_tpl_vars['msg_Address']; ?>
';
	var msg_City		= '<?php echo $this->_tpl_vars['msg_City']; ?>
';
	var msg_State		= '<?php echo $this->_tpl_vars['msg_State']; ?>
';
	var msg_Zip			= '<?php echo $this->_tpl_vars['msg_Zip']; ?>
';
	var msg_Country  	= '<?php echo $this->_tpl_vars['msg_Country']; ?>
';
	var msg_Telephone	= '<?php echo $this->_tpl_vars['msg_Telephone']; ?>
';
	var msg_Title		= '<?php echo $this->_tpl_vars['msg_Title']; ?>
';
	var msg_Description	= '<?php echo $this->_tpl_vars['msg_Description']; ?>
';
	var msg_ValidSize	= '<?php echo $this->_tpl_vars['msg_ValidSize']; ?>
';	
	var msg_Url			= '<?php echo $this->_tpl_vars['msg_Url']; ?>
';
	var msg_Our_Url		= '<?php echo $this->_tpl_vars['msg_Our_Url']; ?>
';
	var msg_Email		= '<?php echo $this->_tpl_vars['msg_Email']; ?>
';
</script>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" height="100%" border="0" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<table width="92%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top">
												<table border="0" cellpadding="0" cellspacing="1" width="100%">
												<form name="frmLinkCategory" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
													<tr>
														<td>
															<table border="0" cellpadding="0" cellspacing="1" width="100%">
																<tr>
																	<td>
																		<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'><?php echo $this->_tpl_vars['L_Link_Heading']; ?>
</h1>
																	</td>
																</tr>
																<tr height="20">
																	<td align="left">
																		<div align="justify">
																			<p align='justify' class='texto_content'>
																				<SPAN class='texto_content Estilo6'>
																					<span class='Estilo24'>
																						<?php echo $this->_tpl_vars['L_Step_Message']; ?>

																					</span>
																				</SPAN>
																			</p>
																		</div>
																	</td>
																</tr>
																<tr><td>&nbsp;</td></tr>
																<tr><td>&nbsp;</td></tr>																
																<tr>
																	<td><div align='justify' class='style6 Estilo1 Estilo8 Estilo39'><font size="2"><b><?php echo $this->_tpl_vars['L_Link_Category']; ?>
</b></font></div></td>
																</tr>
															</table>
														</td>
													</tr>
												
													<tr>
														<td valign="top" align="center">
															<table border="0" cellpadding="0" cellspacing="1" width="100%">
																<tr><td colspan="7" class="successMsg" align="center"><?php echo $this->_tpl_vars['Message']; ?>
</td></tr>
																<tr><td colspan="7" align="center" class="errorMsg"><?php echo $this->_tpl_vars['No_Record_Found']; ?>
</td></tr>

																<?php echo smarty_function_assign(array('var' => 'col','value' => 3), $this);?>

																<?php if (isset($this->_foreach['LinkInfo'])) unset($this->_foreach['LinkInfo']);
$this->_foreach['LinkInfo']['name'] = 'LinkInfo';
$this->_foreach['LinkInfo']['total'] = count($_from = (array)$this->_tpl_vars['LinkInfo']);
$this->_foreach['LinkInfo']['show'] = $this->_foreach['LinkInfo']['total'] > 0;
if ($this->_foreach['LinkInfo']['show']):
$this->_foreach['LinkInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Link']):
        $this->_foreach['LinkInfo']['iteration']++;
        $this->_foreach['LinkInfo']['first'] = ($this->_foreach['LinkInfo']['iteration'] == 1);
        $this->_foreach['LinkInfo']['last']  = ($this->_foreach['LinkInfo']['iteration'] == $this->_foreach['LinkInfo']['total']);
?>
																	<?php if (((is_array($_tmp=$this->_foreach['LinkInfo']['iteration'])) ? $this->_run_mod_handler('mod', true, $_tmp, $this->_tpl_vars['col']) : smarty_modifier_mod($_tmp, $this->_tpl_vars['col'])) == 1): ?>
																	<tr>
																	<?php endif; ?>
																		<td height="18" bgcolor="#EFEBEF" width="30%" align="center">
																			<a href="links.php?&cat_id=<?php echo $this->_tpl_vars['Link']['cat_id']; ?>
" class="titleLink"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['Link']['cat_name'])) ? $this->_run_mod_handler('capitalize', true, $_tmp) : smarty_modifier_capitalize($_tmp)); ?>
</strong></a>
																		</td>
																	<?php if (((is_array($_tmp=$this->_foreach['LinkInfo']['iteration'])) ? $this->_run_mod_handler('mod', true, $_tmp, $this->_tpl_vars['col']) : smarty_modifier_mod($_tmp, $this->_tpl_vars['col'])) == 0): ?>
																	</tr>
																	<?php endif; ?>
																	<?php endforeach; unset($_from); else: ?>
																	<tr><td colspan="7"><?php echo $this->_tpl_vars['no_records']; ?>
</td></tr>	
																<?php endif; ?>

																<tr>
																  <td colspan="7">&nbsp;</td>
																</tr>
																<input type="hidden" name="cat_id" value="<?php echo $this->_tpl_vars['cat_id']; ?>
">
																<input type="hidden" name="Action">
																<input type="hidden" name="start" value="<?php echo $this->_tpl_vars['Start']; ?>
">
															</table>
														</td>
													</tr>
												</form>	
												</table>											
											</td>
										</tr>

										<tr><td height="15">&nbsp;</td></tr>
										<tr>
											<td valign="top">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">
													<form name="frmLinkDetail"  action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
														<tr>
															<td>
																<table border="0" cellpadding="0" cellspacing="0" width="100%">
																	<tr>
																		<td><div align='justify' class='style6 Estilo1 Estilo8 Estilo39'><font size="2"><b><?php echo $this->_tpl_vars['L_Link_Detail']; ?>
 [ <?php echo $this->_tpl_vars['L_Link_Add_Link']; ?>
 ]</b></font></div></td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
														<tr>
															<td valign="top" align="center">
																<table border="0" cellpadding="0" cellspacing="0" width="98%">
																	<tr bgcolor="#007ABD">
																		<td colspan="2"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_Personal_Information']; ?>
 </span></span></div></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td width="30%"><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Name']; ?>
 : </span></div></td>
																		<td width="68%" class="texto_content" align="left"><input name="url_name" type="text" id="url_name" value="<?php echo $this->_tpl_vars['url_name']; ?>
" maxlength="255" size="30"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Address']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><textarea name="url_address" cols="20" rows="3" id="url_address"><?php echo $this->_tpl_vars['url_address']; ?>
</textarea></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_City']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_city" type="text" id="url_city" value="<?php echo $this->_tpl_vars['url_city']; ?>
"  maxlength="255"  size="30"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_State']; ?>
  : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_state" type="text" id="url_state" value="<?php echo $this->_tpl_vars['url_state']; ?>
"  maxlength="255"  size="30"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Zip']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_zip" type="text" id="url_zip" value="<?php echo $this->_tpl_vars['url_zip']; ?>
"  maxlength="5"  size="30"  onKeyPress="javascript: isNumericKey(this.value)"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Country']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_country" type="text" id="url_country" value="<?php echo $this->_tpl_vars['url_country']; ?>
"  maxlength="255"  size="30"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Telephone']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left">
																			<input type="text" name="url_telno_areacode" size="3" maxlength="3" value="<?php echo $this->_tpl_vars['url_telno_areacode']; ?>
" onKeyPress="javascript: isNumericKey(this.value)"> -
																			<input type="text" name="url_telno_citycode" size="3" maxlength="3" value="<?php echo $this->_tpl_vars['url_telno_citycode']; ?>
" onKeyPress="javascript: isNumericKey(this.value)"> -
																			<input type="text" name="url_telno_no" size="4" maxlength="4" value="<?php echo $this->_tpl_vars['url_telno_no']; ?>
" onKeyPress="javascript: isNumericKey(this.value)">
																		</td>	
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Title']; ?>
  : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_title" type="text" value="<?php echo $this->_tpl_vars['url_title']; ?>
" maxlength="255"  size="30"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td valign="top" width="35%"><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Category']; ?>
 : </span></div></td>
																		<td width="75%" class="texto_content" align="left"> 
																			<?php echo $this->_tpl_vars['Link_Category']; ?>
	
																			<select name = "cat_id" class="black" id="cat_id">
																				<?php echo $this->_tpl_vars['Cat_List']; ?>

																			</select>
																		</td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td width="35%" valign="top"><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Description']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><textarea name="url_desc" rows="3" cols="20"><?php echo $this->_tpl_vars['url_desc']; ?>
</textarea><br><font class="validationText"><?php echo $this->_tpl_vars['L_Max300_Chars']; ?>
 </font></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Url']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><input name="url_url" type="text" value="http://" size="30"  maxlength="255" class="texto_content"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Our_Url']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left" ><input name="url_oururl" type="text" id="url_oururl" value="http://" size="30"  maxlength="255" class="texto_content"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Link_Email']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left" ><input type="text" name="url_email" value="<?php echo $this->_tpl_vars['url_email']; ?>
"  maxlength="255"  size="30"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB"><td colspan="2">&nbsp;</td></tr>
																	<tr bgcolor="#EBEBEB">
																		<td colspan="8" align="center" >
																			<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Save']; ?>
" class="nrlButton" onClick="javascript: return Form_Submit(document.frmLinkDetail);">
																			<!--<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Cancel']; ?>
" class="nrlButton">-->
																		</td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td colspan="2">
																			<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
																			<input type="hidden" name="start" value="<?php echo $this->_tpl_vars['Start']; ?>
">
																			<input type="hidden" name="link_id" value="<?php echo $this->_tpl_vars['link_id']; ?>
">
																			<input name="cat_id_" type="hidden" id="cat_id_" value="<?php echo $this->_tpl_vars['cat_id']; ?>
">
																			<input type="hidden" name="cat_status" value="0">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
														<tr>
															<td colspan="2" height="10" class="texto_content"><?php echo $this->_tpl_vars['L_Link_Publish']; ?>
</td>
														</tr>
													</form>
												</table>		
											</td>
										</tr>		

																				
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>