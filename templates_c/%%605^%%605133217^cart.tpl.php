<?php /* Smarty version 2.6.0, created on 2018-09-17 15:45:26
         compiled from cart.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'cart.tpl', 45, false),array('function', 'math', 'cart.tpl', 71, false),array('modifier', 'string_format', 'cart.tpl', 57, false),)), $this); ?>
<script>
	var Select_Airline_Employee	= '<?php echo $this->_tpl_vars['Select_Airline_Employee']; ?>
';
	var Empty_ID				= '<?php echo $this->_tpl_vars['Empty_ID']; ?>
';
	var Empty_Airline_Name  	= '<?php echo $this->_tpl_vars['Empty_Airline_Name']; ?>
';
	var Valid_Select_Cart		= '<?php echo $this->_tpl_vars['Valid_Select_Cart']; ?>
';
	var Valid_Delete_Cart  		= '<?php echo $this->_tpl_vars['Valid_Delete_Cart']; ?>
';
	var Confirm_Delete 			= '<?php echo $this->_tpl_vars['Confirm_Delete']; ?>
';
</script>

<br /><br />
<table>
<tr>
<td>
<?php if ($this->_tpl_vars['lng'] == 'en'): ?>
<img src="images/step-2.jpg" /></td><td><h2>Please confirm services</h2></td>
<?php else: ?>
<img src="images/step-2.jpg" /></td><td><h2>Porfavor confirme servicio(s)</h2></td>
<?php endif; ?>
</tr></table>
<table border="0" cellpadding="1" cellspacing="1" width="100%">
<FORM name="frmCart" action="<?php echo $this->_tpl_vars['A_Cart']; ?>
" method="POST">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="92%" height="100%" border="0" cellspacing="0">
					<tr>
						<td>  
						<table width="100%" border="0" cellspacing="0">
							<tr>
							  <td valign="top"><div align="justify">
								  <?php echo $this->_tpl_vars['L_Emp_Msg']; ?>

								  <table width="100%" border="0" align="center" cellspacing="0">
									<tr>
									  <td colspan="3"class="successMsg"><?php echo $this->_tpl_vars['Message']; ?>
</td>
									<tr bgcolor="#007ABD">
									  <td><div align="center">
										  <input type="checkbox" name="chkAll" onclick="javascript: return checkAll(document.frmCart, this.checked);">
										</div></td>
									  <td width="60%"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_Requested_Service']; ?>
</span></span></div></td>
									  <td width="40%"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_Rate']; ?>
</span></span></div></td>
									</tr>
									<tr>
									  <td class="successMsg" colspan="3" align="center"><?php echo $this->_tpl_vars['Notfound_Message']; ?>
</td>
									</tr>
									<?php if (isset($this->_foreach['CartInfo'])) unset($this->_foreach['CartInfo']);
$this->_foreach['CartInfo']['name'] = 'CartInfo';
$this->_foreach['CartInfo']['total'] = count($_from = (array)$this->_tpl_vars['CartInfo']);
$this->_foreach['CartInfo']['show'] = $this->_foreach['CartInfo']['total'] > 0;
if ($this->_foreach['CartInfo']['show']):
$this->_foreach['CartInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Cart']):
        $this->_foreach['CartInfo']['iteration']++;
        $this->_foreach['CartInfo']['first'] = ($this->_foreach['CartInfo']['iteration'] == 1);
        $this->_foreach['CartInfo']['last']  = ($this->_foreach['CartInfo']['iteration'] == $this->_foreach['CartInfo']['total']);
?>
									<?php echo smarty_function_assign(array('var' => 'TotalPrice','value' => ($this->_tpl_vars['TotalPrice']+$this->_tpl_vars['Cart']['totalCharge'])), $this);?>

									<tr bgcolor="#EBEBEB">
									  <td><div align="center"><span class="texto_content">
										  <input type='checkbox' name="chkCartId[]" id="chkCartId[]" value="<?php echo $this->_tpl_vars['Cart']['cart_id']; ?>
">
										  </span></div></td>
									  <td><div align="center" class="texto_content"><?php echo $this->_tpl_vars['Cart']['dest_title']; ?>
</div></td>
									  <td><div align="center"><span class="texto_content">$ <?php echo $this->_tpl_vars['Cart']['totalCharge']; ?>
 <?php echo $this->_tpl_vars['Currency']; ?>
</span></div></td>
									</tr>
									<?php endforeach; unset($_from); endif; ?>
									<tr>
									  <td><div align="center"><span class="texto_content"> </span></div></td>
									  <td><div align="right"><span class="texto_content"><strong><?php echo $this->_tpl_vars['L_Total']; ?>
</strong></span></div></td>
									  <td><div align="center"><span class="texto_content"><strong>$  <?php echo ((is_array($_tmp=$this->_tpl_vars['TotalPrice'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>

										 <?php echo $this->_tpl_vars['Currency']; ?>
</strong></span></div>
										<input type="hidden" name="cartId">
										<input type="hidden" name="itemType">
									  </td>
									</tr>
									<?php if ($this->_tpl_vars['is_airline_empl'] == 1): ?>
									<tr>
									  <td>&nbsp;</td>
									  <td><div align="right"><span class="texto_content">
										<strong><?php echo $this->_tpl_vars['L_Discount']; ?>
 <?php echo $this->_tpl_vars['discount_rate']; ?>
% </strong>
										<?php echo smarty_function_assign(array('var' => 'discount','value' => ($this->_tpl_vars['discount_rate'])), $this);?>

										</span></div>
									  </td>
									  <td><div align="center"><span class="texto_content"><strong>- $ <?php echo smarty_function_math(array('equation' => "(x * y)/100",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</strong></span></div></td>
									</tr>
									<tr>
									  <td>&nbsp;</td>
									  <td><div align="right"><span class="texto_content"><strong><?php echo $this->_tpl_vars['L_Final']; ?>
 <?php echo $this->_tpl_vars['L_Total']; ?>
</strong></span></div></td>
									  <td><div align="center"><span class="texto_content"><strong>$ <?php echo smarty_function_math(array('equation' => "(x-((x * y)/100))",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</strong></span></div></td>
									</tr>
									<?php endif; ?>
								  </table>
								  <br>
								  <table width="100%" border="0" align="center" cellspacing="0">
									<tr>
									  <td>&nbsp;</td>
									  <td>&nbsp;</td>
									  <td>&nbsp;</td>
									</tr>
									
									<tr>
									  <td>&nbsp;</td>
									  <td align="center"><input type="button" Value="<?php echo $this->_tpl_vars['Delete']; ?>
" Name="Submit" class="texto_content" onclick="javascript : return Form_Delete(frmCart); return false;">
										<input type="hidden" name="index_id" value="<?php echo $this->_tpl_vars['index_id']; ?>
">
										<input type="hidden" name="cart_id" value="<?php echo $this->_tpl_vars['cart_id']; ?>
">
										<input type="hidden" name="dest_id" value="<?php echo $this->_tpl_vars['dest_id']; ?>
">
										<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
										<!--<input type="button" value="<?php echo $this->_tpl_vars['L_ReserveMore']; ?>
" class="texto_content" onclick="javascript: location.href('<?php echo $this->_tpl_vars['ReserveMoreLink']; ?>
');">-->
										 <a href="<?php echo $this->_tpl_vars['ReserveMoreLink']; ?>
" class="texto_content">Reserve more </a>   
										<input type="button" value="<?php echo $this->_tpl_vars['L_Checkout']; ?>
" class="texto_content" onclick="javascript : return Form_Checkout(frmCart,'<?php echo $this->_tpl_vars['index_id']; ?>
'); return false;">
									  </td>
									</tr>
								  </table>
								</div></td>
							</tr>
						  </table>
					
					</td>
					</tr>

				  </table>
			</td>
		</tr>
	</FORM>
</table>