<?php /* Smarty version 2.6.0, created on 2018-09-12 00:35:27
         compiled from login.tpl */ ?>
<script>
	var msgEmptyConfirmationNo		= '<?php echo $this->_tpl_vars['msgEmptyConfirmationNo']; ?>
';
	var msgEmptyUserPassword		= '<?php echo $this->_tpl_vars['msgEmptyUserPassword']; ?>
';	
</script>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
  <form name="frmLogin1" action="<?php echo $this->_tpl_vars['A_Login']; ?>
" method="post">
    <tr>
      <td width="100%" valign="top" align="center"><table width="100%" height="100%" border="0" cellspacing="0">
      <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0" width="92%">
                <tr>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td><table width="100%" height="48" border="0" cellspacing="0">
                      <tr>
                        <td valign="top"><div align="justify">
                            <p class="Estilo10"><span class="texto_content"><?php echo $this->_tpl_vars['Login_Msg']; ?>
</span></p>
                            <table width="60%" border="0" align="center" cellspacing="0">
                              <tr>
                                <td colspan="2" class="errorMsg" align="center"><div align="center"><?php echo $this->_tpl_vars['Error_Message']; ?>
</div>
                                  <br></td>
                              </tr>
                            </table>
                            <table width="60%" height="114" border="0" align="center" cellspacing="0">
                              <tr bgcolor="#007ABD">
                                <td colspan="2"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['LoginHere']; ?>
</span></span></div></td>
                              </tr>
                              <tr bgcolor="#EFEFEF">
                                <td width="125"><div align="right" class="texto_content"><?php echo $this->_tpl_vars['L_ConfirmationNo']; ?>
 </div></td>
                                <td width="157"><input type="text" name="confirmationNo"  value="<?php echo $this->_tpl_vars['confirmationNo']; ?>
"></td>
                              </tr>
                              <tr bgcolor="#EFEFEF">
                                <td height="24"><div align="right" class="texto_content"><?php echo $this->_tpl_vars['L_Password']; ?>
</div></td>
                                <td><input type="password" name="password"></td>
                              </tr>
                              <tr bgcolor="#EFEFEF">
                                <td colspan="2"><div align="center">
                                    <input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['L_Login']; ?>
" class="texto_content" onClick="javascript: return Form_Submit(document.frmLogin1);">
                                    &nbsp;&nbsp;
                                    <input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
                                    <input type="hidden" name="user_id" value="<?php echo $this->_tpl_vars['User_Id']; ?>
">
                                  </div></td>
                              </tr>
                              <tr valign="middle" bgcolor="#EFEFEF">
                                <td height="28" colspan="2"><div align="center" class="texto_content"><br>
                                    <a href="forgotpass.php"><?php echo $this->_tpl_vars['L_Forgot_Password']; ?>
</a><br>
                                    <br>
                                  </div></td>
                              </tr>
                            </table>
                            </div></td>
                      </tr>
                    </table></td>
                </tr>
              </table></td>
          </tr>
		  <tr>
		  	<td>
			&nbsp;<br><br><br><br>
			</td>
		  </tr>
		  
        </table></td>
    </tr>
  </FORM>
</table>