<?php /* Smarty version 2.6.0, created on 2011-06-02 22:05:33
         compiled from default_layout.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $this->_tpl_vars['Site_Title']; ?>
</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
<META NAME="title" 			CONTENT="<?php echo $this->_tpl_vars['Meta_Title']; ?>
">
<META NAME="keywords" 		CONTENT="<?php echo $this->_tpl_vars['Meta_Keyword']; ?>
">
<META NAME="description" 	CONTENT="<?php echo $this->_tpl_vars['Meta_Description']; ?>
">
<META NAME="Robots" CONTENT="INDEX, FOLLOW">
<META NAME="Revisit" CONTENT="15 Days">
<link href="<?php echo $this->_tpl_vars['Templates_CSS']; ?>
style.css" rel="stylesheet" type="text/css">

<?php if ($this->_tpl_vars['lng'] == 'en'): ?>
	<link href="<?php echo $this->_tpl_vars['Templates_CSS']; ?>
cancun.css" rel="stylesheet" type="text/css">
<?php else: ?>
	<link href="<?php echo $this->_tpl_vars['Templates_CSS']; ?>
cancun_esp.css" rel="stylesheet" type="text/css">
<?php endif; ?>
<script language="javascript" >
<?php if ($this->_tpl_vars['lng'] == 'en'): ?>
	var vLangue=1;
<?php else: ?>
	var vLangue=0;
<?php endif; ?>
</script>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
validate.js"></script>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
functions.js"></script>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
index.js"></script>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS']; ?>
menu.js"></script>
<?php if (isset($this->_sections['FileName'])) unset($this->_sections['FileName']);
$this->_sections['FileName']['name'] = 'FileName';
$this->_sections['FileName']['loop'] = is_array($_loop=$this->_tpl_vars['JavaScript']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['FileName']['show'] = true;
$this->_sections['FileName']['max'] = $this->_sections['FileName']['loop'];
$this->_sections['FileName']['step'] = 1;
$this->_sections['FileName']['start'] = $this->_sections['FileName']['step'] > 0 ? 0 : $this->_sections['FileName']['loop']-1;
if ($this->_sections['FileName']['show']) {
    $this->_sections['FileName']['total'] = $this->_sections['FileName']['loop'];
    if ($this->_sections['FileName']['total'] == 0)
        $this->_sections['FileName']['show'] = false;
} else
    $this->_sections['FileName']['total'] = 0;
if ($this->_sections['FileName']['show']):

            for ($this->_sections['FileName']['index'] = $this->_sections['FileName']['start'], $this->_sections['FileName']['iteration'] = 1;
                 $this->_sections['FileName']['iteration'] <= $this->_sections['FileName']['total'];
                 $this->_sections['FileName']['index'] += $this->_sections['FileName']['step'], $this->_sections['FileName']['iteration']++):
$this->_sections['FileName']['rownum'] = $this->_sections['FileName']['iteration'];
$this->_sections['FileName']['index_prev'] = $this->_sections['FileName']['index'] - $this->_sections['FileName']['step'];
$this->_sections['FileName']['index_next'] = $this->_sections['FileName']['index'] + $this->_sections['FileName']['step'];
$this->_sections['FileName']['first']      = ($this->_sections['FileName']['iteration'] == 1);
$this->_sections['FileName']['last']       = ($this->_sections['FileName']['iteration'] == $this->_sections['FileName']['total']);
?>
<script language="javascript" src="<?php echo $this->_tpl_vars['Templates_JS'];  echo $this->_tpl_vars['JavaScript'][$this->_sections['FileName']['index']]; ?>
"></script>
<?php endfor; endif; ?>

</head>

<body bgcolor="#ffffff" bottommargin="0" leftmargin="0" marginheight="0" marginwidth="0" rightmargin="0" topmargin="0" onLoad="MM_preloadImages('<?php echo $this->_tpl_vars['Templates_Image']; ?>
covereng_r2_c1_f2.jpg','<?php echo $this->_tpl_vars['Templates_Image']; ?>
covereng_r2_c2_f2.jpg','<?php echo $this->_tpl_vars['Templates_Image']; ?>
covereng_r2_c3_f2.jpg','<?php echo $this->_tpl_vars['Templates_Image']; ?>
covereng_r2_c4_f2.jpg','<?php echo $this->_tpl_vars['Templates_Image']; ?>
covereng_r2_c5_f2.jpg');">
<table width="770" border="0" align="center" cellpadding="0" cellspacing="0">
<!-- fwtable fwsrc="covereng.png" fwbase="covereng.jpg" fwstyle="Dreamweaver" fwdocid = "1028171462" fwnested="0" -->
	<tr>
		<td width="100%" valign="top">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['T_Header']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> 
		</td>
	</tr>
	<tr>
		<td valign="top">
			<table width="770" cellpadding="0" cellspacing="0">
				<tr>
					<td width="300" align="center" valign="top" background="<?php echo $this->_tpl_vars['Templates_Image']; ?>
vans_left.jpg" height="100%">
						<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['T_Menu']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					</td>
					<td width="464" bgcolor="#FFFFFF" valign="top">
						<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['T_Body']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					</td>
					<td background="<?php echo $this->_tpl_vars['Templates_Image']; ?>
covereng_r4_c9.jpg">&nbsp;</td>
					<!--<td><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
spacer.gif" width="1" height="289" border="0" alt=""></td>-->
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2"><img name="covereng_r5_c1" src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
covereng_r5_c1.jpg" width="770" height="3" border="0" alt=""></td>
		<td><img src="<?php echo $this->_tpl_vars['Templates_Image']; ?>
spacer.gif" width="1" height="3" border="0" alt=""></td>
	</tr>
	<tr>
		<td width="770" background="<?php echo $this->_tpl_vars['Templates_Image']; ?>
covereng_r6_c1.jpg">
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['T_Footer']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		</td>
	</tr>
</table>

<map name="covereng_r1_c1Map">
	<?php if ($this->_tpl_vars['lng'] == 'en'): ?>
		<area shape="rect" coords="655,185,770,209" href="login.php?&lng=en">
	<?php else: ?>
		<area shape="rect" coords="655,185,770,209" href="login.php?&lng=sp">
	<?php endif; ?>
</map>

</body>
</html>