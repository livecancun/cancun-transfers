<?php /* Smarty version 2.6.0, created on 2017-12-22 02:14:08
         compiled from cart_request.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'cart_request.tpl', 101, false),array('function', 'math', 'cart_request.tpl', 128, false),array('modifier', 'number_format', 'cart_request.tpl', 109, false),)), $this); ?>
<script language="JavaScript">
	var Valid_Delete_Cart	= '<?php echo $this->_tpl_vars['Valid_Delete_Cart']; ?>
';
	var Confirm_Delete 		= '<?php echo $this->_tpl_vars['Confirm_Delete']; ?>
';
	var Valid_Select_Cart 	= '<?php echo $this->_tpl_vars['Valid_Select_Cart']; ?>
';
</script>


<FORM name="frmCart" action="<?php echo $this->_tpl_vars['A_Cart']; ?>
" method="POST">
<table width="100%" border="0" cellspacing="0">
	<tr>
		<td>&nbsp;</td>
	</tr>
    <tr>
      <td valign="top"><div align="justify">
          <table width="92%" border="0" align="center" cellspacing="0">
			<tr>
				<td valign="top" class="blueHeader" width="100%" colspan="3">
					<table width="100%">
		            <?php if (isset($this->_foreach['CartInfo'])) unset($this->_foreach['CartInfo']);
$this->_foreach['CartInfo']['name'] = 'CartInfo';
$this->_foreach['CartInfo']['total'] = count($_from = (array)$this->_tpl_vars['CartInfo']);
$this->_foreach['CartInfo']['show'] = $this->_foreach['CartInfo']['total'] > 0;
if ($this->_foreach['CartInfo']['show']):
$this->_foreach['CartInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Cart']):
        $this->_foreach['CartInfo']['iteration']++;
        $this->_foreach['CartInfo']['first'] = ($this->_foreach['CartInfo']['iteration'] == 1);
        $this->_foreach['CartInfo']['last']  = ($this->_foreach['CartInfo']['iteration'] == $this->_foreach['CartInfo']['total']);
?>	
					<?php if ($this->_foreach['CartInfo']['first']): ?>
					  <tr>
						<td class="blueHeader" width="70%"><span class="texto_content"><strong><?php echo $this->_tpl_vars['L_Reservation_Details']; ?>
<br><br><?php echo $this->_tpl_vars['L_ConfirmationNo']; ?>
 : </strong><?php echo $this->_tpl_vars['Cart']['confirmationNo']; ?>
</span></td>
						<td class="blueHeader" width="30%" align="right"><br><span class="texto_content"><strong><?php echo $this->_tpl_vars['L_status']; ?>
 : </strong><?php echo $this->_tpl_vars['Cart']['cartStatus']; ?>
</span></td>
					   </tr>	
					<?php endif; ?>  
					<?php endforeach; unset($_from); endif; ?>
					
					<?php if (isset($this->_foreach['CartDetails'])) unset($this->_foreach['CartDetails']);
$this->_foreach['CartDetails']['name'] = 'CartDetails';
$this->_foreach['CartDetails']['total'] = count($_from = (array)$this->_tpl_vars['CartDetails']);
$this->_foreach['CartDetails']['show'] = $this->_foreach['CartDetails']['total'] > 0;
if ($this->_foreach['CartDetails']['show']):
$this->_foreach['CartDetails']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['CartDet']):
        $this->_foreach['CartDetails']['iteration']++;
        $this->_foreach['CartDetails']['first'] = ($this->_foreach['CartDetails']['iteration'] == 1);
        $this->_foreach['CartDetails']['last']  = ($this->_foreach['CartDetails']['iteration'] == $this->_foreach['CartDetails']['total']);
?>	
					<?php if ($this->_foreach['CartDetails']['first']): ?>
					   <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Total Persons : </strong><?php echo $this->_tpl_vars['CartDet']['total_person']; ?>
</span></td>
					   </tr>
					   <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Number of Adults : </strong><?php echo $this->_tpl_vars['CartDet']['tres_no_adult']; ?>
</span></td>
					   </tr>	
						  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Number of Kids : </strong><?php echo $this->_tpl_vars['CartDet']['tres_no_kid']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Hotel Name : </strong><?php echo $this->_tpl_vars['CartDet']['hotelName']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Arrival Date : </strong><?php echo $this->_tpl_vars['CartDet']['arrivalDate']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Arrival Time : </strong><?php echo $this->_tpl_vars['CartDet']['arrivalTime']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Arrival Airline : </strong><?php echo $this->_tpl_vars['CartDet']['arrivalAirline']; ?>
</span></td>
					   </tr>
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Arrival Flight : </strong><?php echo $this->_tpl_vars['CartDet']['arrivalFlight']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Total Person: </strong><?php echo $this->_tpl_vars['CartDet']['departureTotalPerson']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Hotel : </strong><?php echo $this->_tpl_vars['CartDet']['departureHotelName']; ?>
</span></td>
					   </tr>
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Pick up time : </strong><?php echo $this->_tpl_vars['CartDet']['pickupTime']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Date : </strong><?php echo $this->_tpl_vars['CartDet']['departureDate']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Time : </strong><?php echo $this->_tpl_vars['CartDet']['departureTime']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Airline : </strong><?php echo $this->_tpl_vars['CartDet']['departureAirline']; ?>
</span></td>
					   </tr>	
					  <tr>
						<td class="blueHeader" width="70%" colspan="2"><span class="texto_content"><strong>Departure Flight : </strong><?php echo $this->_tpl_vars['CartDet']['departureFlight']; ?>
</span></td>
					   </tr>	
					<?php endif; ?>  
					<?php endforeach; unset($_from); endif; ?>
					
					

					</table>
					<br>
				</td>		
			</tr>
					  
            <tr>
              <td colspan="3"class="successMsg" align="center"><?php echo $this->_tpl_vars['Message']; ?>
</td>
            <tr bgcolor="#007ABD">
              <td width="35"><div align="center">
                  <input type="checkbox" name="chkAll" onclick="javascript: return checkAll(document.frmCart, this.checked);">
                </div></td>
              <td width="60%"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_Requested_Service']; ?>
</span></span></div></td>
<!--              <td width="20%"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_ConfirmationNo']; ?>
</span></span></div></td>
              <td width="20%"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_status']; ?>
</span></span></div></td>
-->              <td width="40%"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_Rate']; ?>
</span></span></div></td>
            </tr>
            <tr>
              <td class="successMsg" colspan="3" align="center"><?php echo $this->_tpl_vars['Notfound_Message']; ?>
</td>
            </tr>

            <?php if (isset($this->_foreach['CartInfo'])) unset($this->_foreach['CartInfo']);
$this->_foreach['CartInfo']['name'] = 'CartInfo';
$this->_foreach['CartInfo']['total'] = count($_from = (array)$this->_tpl_vars['CartInfo']);
$this->_foreach['CartInfo']['show'] = $this->_foreach['CartInfo']['total'] > 0;
if ($this->_foreach['CartInfo']['show']):
$this->_foreach['CartInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Cart']):
        $this->_foreach['CartInfo']['iteration']++;
        $this->_foreach['CartInfo']['first'] = ($this->_foreach['CartInfo']['iteration'] == 1);
        $this->_foreach['CartInfo']['last']  = ($this->_foreach['CartInfo']['iteration'] == $this->_foreach['CartInfo']['total']);
?>
            <?php echo smarty_function_assign(array('var' => 'TotalPrice','value' => ($this->_tpl_vars['TotalPrice']+$this->_tpl_vars['Cart']['totalCharge'])), $this);?>

            <tr bgcolor="#EBEBEB">
              <td><div align="center"><span class="texto_content">
                  <input type='checkbox' name="chkCartId[]" value="<?php echo $this->_tpl_vars['Cart']['cart_id']; ?>
">
                  </span></div></td>
              <td><div align="center" class="texto_content"><?php echo $this->_tpl_vars['Cart']['dest_title']; ?>
</div></td>
<!--              <td><div align="center" class="texto_content"><?php echo $this->_tpl_vars['Cart']['confirmationNo']; ?>
</div></td>
              <td><div align="center" class="texto_content"><?php echo $this->_tpl_vars['Cart']['cartStatus']; ?>
</div></td>
-->              <td><div align="center"><span class="texto_content">$<?php echo ((is_array($_tmp=$this->_tpl_vars['Cart']['totalCharge'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2) : smarty_modifier_number_format($_tmp, 2)); ?>
 <?php echo $this->_tpl_vars['Currency']; ?>
</span></div></td>
            </tr>
            <?php endforeach; unset($_from); endif; ?>
            <tr>
              <td><div align="center"><span class="texto_content"> </span></div></td>
              <td><div align="right"><span class="texto_content"><strong><?php echo $this->_tpl_vars['L_Total']; ?>
</strong></span></div></td>
              <td><div align="center"><span class="texto_content"><strong>$<?php echo ((is_array($_tmp=$this->_tpl_vars['TotalPrice'])) ? $this->_run_mod_handler('number_format', true, $_tmp, 2) : smarty_modifier_number_format($_tmp, 2)); ?>
 <?php echo $this->_tpl_vars['Currency']; ?>
</strong></span></div>
                <input type="hidden" name="cartId">
                <input type="hidden" name="itemType">
              </td>
            </tr>
            <?php if ($this->_tpl_vars['is_airline_empl'] == 1): ?>
            <tr>
              <td>&nbsp;</td>
              <td><div align="right"><span class="texto_content">
			  	<strong><?php echo $this->_tpl_vars['L_Discount']; ?>
 <?php echo $this->_tpl_vars['discount_rate']; ?>
% </strong>
				<?php echo smarty_function_assign(array('var' => 'discount','value' => ($this->_tpl_vars['discount_rate'])), $this);?>

				</span></div>
			  </td>
              <td><div align="center"><span class="texto_content"><strong>- $ <?php echo smarty_function_math(array('equation' => "(x * y)/100",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</strong></span></div></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><div align="right"><span class="texto_content"><strong><?php echo $this->_tpl_vars['L_Final']; ?>
 <?php echo $this->_tpl_vars['L_Total']; ?>
</strong></span></div></td>
              <td><div align="center"><span class="texto_content"><strong>$ <?php echo smarty_function_math(array('equation' => "(x-((x * y)/100))",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</strong></span></div></td>
            </tr>
            <?php endif; ?>
          </table>
          <table width="92%" border="0" align="center" cellspacing="0">
            <tr>
              <td align="center" colspan="2">
<!--			  	<input type="button" Value="<?php echo $this->_tpl_vars['Delete']; ?>
" Name="Submit" class="texto_content" onClick="javascript : return Form_Delete(frmCart);">
-->                <input type="hidden" name="index_id" value="<?php echo $this->_tpl_vars['index_id']; ?>
">
                <input type="hidden" name="cart_id" value="<?php echo $this->_tpl_vars['cart_id']; ?>
">
                <input type="hidden" name="dest_id" value="<?php echo $this->_tpl_vars['dest_id']; ?>
">
				<input type="hidden" name="cartId"> 
				<input type="hidden" name="itemType"> 
                <input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
              </td>
            </tr>
			<br>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
			
			 <tr>
			 	<td colspan="2" align="center">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr><td colspan="3" align="left"><span class="texto_content"><b><?php echo $this->_tpl_vars['L_status']; ?>
 <?php echo $this->_tpl_vars['L_Note']; ?>
 :</b></span></td></tr>
						<tr><td colspan="3" align="left">&nbsp;</td></tr>						
						<tr>
							<td align="left"><span class="texto_content"><b>PR</b> - <?php echo $this->_tpl_vars['Note_PR']; ?>
</span></td>
							<td align="left"><span class="texto_content"><b>AR</b> - <?php echo $this->_tpl_vars['Note_AR']; ?>
</span></td>
							<td align="left"><span class="texto_content"><b>NA</b> - <?php echo $this->_tpl_vars['Note_NA']; ?>
</span></td>
						</tr>
						<tr>
							<td align="left"><span class="texto_content"><b>RP</b> - <?php echo $this->_tpl_vars['Note_RP']; ?>
</span></td>
							<td align="left"><span class="texto_content"><b>PD</b> - <?php echo $this->_tpl_vars['Note_PD']; ?>
</span></td>
							<td>&nbsp;</td>
						</tr>
					</table>	
				</td>
			 </tr>
			<tr>
				<td>
				</td>
			</tr>
          </table>
		  
        </div></td>
    </tr>
       </table>
</form>	   