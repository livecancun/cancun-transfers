<?php /* Smarty version 2.6.0, created on 2011-02-12 13:08:34
         compiled from testimonial_addedit.tpl */ ?>
<script>
	var Empty_Name		= '<?php echo $this->_tpl_vars['Empty_Name']; ?>
';
	var Empty_Company  	= '<?php echo $this->_tpl_vars['Empty_Company']; ?>
';
	var Valid_Zip 		= '<?php echo $this->_tpl_vars['Valid_Zip']; ?>
';
	var Empty_Phone		= '<?php echo $this->_tpl_vars['Empty_Phone']; ?>
';
	var Valid_Phone  	= '<?php echo $this->_tpl_vars['Valid_Phone']; ?>
';
	var Empty_Fax 		= '<?php echo $this->_tpl_vars['Empty_Fax']; ?>
';
	var Valid_Fax 		= '<?php echo $this->_tpl_vars['Valid_Fax']; ?>
';
	var Empty_TollFree 	= '<?php echo $this->_tpl_vars['Empty_TollFree']; ?>
';
	var Valid_TollFree 	= '<?php echo $this->_tpl_vars['Valid_TollFree']; ?>
';
	var Empty_Email 	= '<?php echo $this->_tpl_vars['Empty_Email']; ?>
';
	var Valid_Email 	= '<?php echo $this->_tpl_vars['Valid_Email']; ?>
';
	var Valid_URL 		= '<?php echo $this->_tpl_vars['Valid_URL']; ?>
';
	var Valid_Photo 	= '<?php echo $this->_tpl_vars['Valid_Photo']; ?>
';			
	var Empty_Description	= '<?php echo $this->_tpl_vars['Empty_Description']; ?>
';
	var Empty_Comment 	= '<?php echo $this->_tpl_vars['Empty_Comment']; ?>
';	

	var Delete_Testimonial 		= '<?php echo $this->_tpl_vars['Delete_Testimonial']; ?>
';	
	var Delete_AllTestimonials 	= '<?php echo $this->_tpl_vars['Delete_AllTestimonials']; ?>
';	
		
</script>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
<form name="frmTestimonial" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post" enctype="multipart/form-data"> 
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" height="100%" border="0" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top">
									<table width="92%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top">
												<h1 align='justify' class='style6 Estilo1 Estilo8 Estilo39'><?php echo $this->_tpl_vars['Testimonial_Header']; ?>
</h1>
											</td>
										</tr>
										<tr>
											<td valign="top">
												<table border="0" cellpadding="0" cellspacing="0" width="100%">

														<tr>
															<td valign="top" align="center">
																<table border="0" cellpadding="0" cellspacing="0" width="98%">
																	<tr bgcolor="#007ABD">
																		<td colspan="2"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_Personal_Information']; ?>
 </span></span></div></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td width="35%"><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Name']; ?>
 : </span></div></td>
																		<td width="63%" class="texto_content" align="left"><input type="text" name="person_name" value="<?php echo $this->_tpl_vars['Person_Name']; ?>
" size="35" maxlength="50"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Address']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><textarea name="person_address" rows="5" cols="40"><?php echo $this->_tpl_vars['Person_Address']; ?>
</textarea></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['City']; ?>
  : </span></div></td>
																		<td class="texto_content" align="left"><input type="text" name="person_city" value="<?php echo $this->_tpl_vars['Person_City']; ?>
" size="20" maxlength="25"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['State']; ?>
 : </span></div></td>

																		<td class="texto_content" align="left">
																			<input type="text" name="person_state" value="<?php echo $this->_tpl_vars['Person_State']; ?>
" size="20" maxlength="25">
																		</td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Country']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><SELECT name="person_country" class="texto_content"><option value="0">-----------<?php echo $this->_tpl_vars['L_Select_Country']; ?>
--------</option><?php echo $this->_tpl_vars['CountryList']; ?>
</SELECT></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Zip']; ?>
  : </span></div></td>
																		<td class="texto_content" align="left"><input type="text" name="person_zip" value="<?php echo $this->_tpl_vars['Person_Zip']; ?>
" size="10" maxlength="5" onKeyPress="javascript: isNumericKey(this.value)"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Phone']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left">
																			<input type="text" name="person_phone_areacode" value="<?php echo $this->_tpl_vars['Person_Phone_Areacode']; ?>
" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
																			<input type="text" name="person_phone_citycode" value="<?php echo $this->_tpl_vars['Person_Phone_Citycode']; ?>
" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
																			<input type="text" name="person_phone_no" value="<?php echo $this->_tpl_vars['Person_Phone_No']; ?>
" size="4" maxlength="4" onKeyPress="javascript: isNumericKey(this.value)">
																		</td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Fax']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left">
																			<input type="text" name="person_fax_areacode" value="<?php echo $this->_tpl_vars['Person_Fax_Areacode']; ?>
" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
																			<input type="text" name="person_fax_citycode" value="<?php echo $this->_tpl_vars['Person_Fax_Citycode']; ?>
" size="3" maxlength="3" onKeyPress="javascript: isNumericKey(this.value)">
																			<input type="text" name="person_fax_no" value="<?php echo $this->_tpl_vars['Person_Fax_No']; ?>
" size="4" maxlength="4" onKeyPress="javascript: isNumericKey(this.value)">
																		</td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Email']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><input type="text" name="person_email" value="<?php echo $this->_tpl_vars['Person_Email']; ?>
" size="35" maxlength="255"></td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">
																		<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Website']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><input type="text" name="person_website" value="<?php echo $this->_tpl_vars['Person_Website']; ?>
" size="35" maxlength="255"></td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td valign="top"><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Photo']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left">
																			<input type="file" name="person_pic" size="25" maxlength="255" <?php if ($this->_tpl_vars['Picture_Filename']): ?> onchange="javascript: UploadImage_Change(this, pic, '<?php echo $this->_tpl_vars['Picture_Filename']; ?>
','');" <?php endif; ?>>
																			<input type="hidden" name="person_pic_edit" value="<?php echo $this->_tpl_vars['Person_Pic']; ?>
"><br>
																			<img src="<?php echo $this->_tpl_vars['Picture_Filename']; ?>
" id="pic" border="0"><br>
																			<?php if ($this->_tpl_vars['Person_Pic'] != ''): ?>
																			<input type="checkbox" name="delete_picture" value="1" class="stdCheckBox"> <?php echo $this->_tpl_vars['Delete_Picture']; ?>

																			<?php endif; ?>
																		</td>
																	</tr>	
																	<tr bgcolor="#EBEBEB">																	
																		<td valign="top"><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['Comment']; ?>
 : </span></div></td>
																		<td class="texto_content" align="left"><textarea name="person_comment" rows="5" cols="40"><?php echo $this->_tpl_vars['Person_Comment']; ?>
</textarea></td>
																	</tr>
																	<tr bgcolor="#EBEBEB"><td colspan="2">&nbsp;</td></tr>
																	<tr bgcolor="#EBEBEB">
																		<td colspan="8" align="center" >
																			<input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Save']; ?>
" class="texto_content" onClick="javascript: return Form_Submit(document.frmTestimonial);"> 
																			&nbsp;&nbsp;<input type="button" name="Submit" value="<?php echo $this->_tpl_vars['Cancel']; ?>
" class="texto_content" onClick="javascript: Cancel_Click('<?php echo $this->_tpl_vars['Cancel_Action']; ?>
');">&nbsp;&nbsp;
																		</td>
																	</tr>
																	<tr bgcolor="#EBEBEB">
																		<td colspan="2">
																			<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
																			<input type="hidden" name="person_id" value="<?php echo $this->_tpl_vars['Person_Id']; ?>
">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
														<tr><td colspan="2">&nbsp;</td></tr>
												</table>		
											</td>
										</tr>		

																				
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</form>
</table>