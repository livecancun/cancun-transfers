<?php /* Smarty version 2.6.0, created on 2018-08-22 00:42:43
         compiled from reserve.tpl */ ?>
<script>
	var Empty_No_Person		= '<?php echo $this->_tpl_vars['Empty_No_Person']; ?>
';
	var Valid_No_Person		= '<?php echo $this->_tpl_vars['Valid_No_Person']; ?>
';	
	var Empty_Hotel_Name  	= '<?php echo $this->_tpl_vars['Empty_Hotel_Name']; ?>
';
	var Empty_Check_Box 	= '<?php echo $this->_tpl_vars['Empty_Check_Box']; ?>
';
	var Empty_Arrival_Date 	= '<?php echo $this->_tpl_vars['Empty_Arrival_Date']; ?>
';
	var Valid_Arrival_Date 	= '<?php echo $this->_tpl_vars['Valid_Arrival_Date']; ?>
';
	var Empty_Airline_Name 	= '<?php echo $this->_tpl_vars['Empty_Airline_Name']; ?>
';
	var Empty_Flight_No 	= '<?php echo $this->_tpl_vars['Empty_Flight_No']; ?>
';
	var Empty_Departure_Date= '<?php echo $this->_tpl_vars['Empty_Departure_Date']; ?>
';
	var Valid_Departure_Date= '<?php echo $this->_tpl_vars['Valid_Departure_Date']; ?>
';
	var Valid_BothDate_Msg 	= '<?php echo $this->_tpl_vars['Valid_BothDate_Msg']; ?>
';
</script>

<br /><br />
<table>
<tr>
<td>
<?php if ($this->_tpl_vars['lng'] == 'en'): ?>
<img src="images/step-1.jpg" /></td><td><h2>Please fill the form</h2>
<?php else: ?>
<img src="images/step-1.jpg" /></td><td><h2>Porfavor llene el formulario</h2>
<?php endif; ?>

</tr></table>

<table>
  <FORM name="frmReserve" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
    <tr>
      <td >
<!--Test de formulario -->
<table>
 <tr>
						<td >
							<input type="hidden" name="singleTrip" value="1">	
							<span >
								<?php echo $this->_tpl_vars['L_Airport_To_Hotel']; ?>
</span> <?php if (isset($this->_foreach['MaxPassenger'])) unset($this->_foreach['MaxPassenger']);
$this->_foreach['MaxPassenger']['name'] = 'MaxPassenger';
$this->_foreach['MaxPassenger']['total'] = count($_from = (array)$this->_tpl_vars['MaxPassenger']);
$this->_foreach['MaxPassenger']['show'] = $this->_foreach['MaxPassenger']['total'] > 0;
if ($this->_foreach['MaxPassenger']['show']):
$this->_foreach['MaxPassenger']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Passenger']):
        $this->_foreach['MaxPassenger']['iteration']++;
        $this->_foreach['MaxPassenger']['first'] = ($this->_foreach['MaxPassenger']['iteration'] == 1);
        $this->_foreach['MaxPassenger']['last']  = ($this->_foreach['MaxPassenger']['iteration'] == $this->_foreach['MaxPassenger']['total']);
?>
								<input type="hidden" name="Passengr_Id[]" value="<?php echo $this->_tpl_vars['Passenger']['passenger_id']; ?>
">
								<input type="hidden" name="MaxPassenger[]" value="<?php echo $this->_tpl_vars['Passenger']['passenger_ending_range']; ?>
">
								<?php endforeach; unset($_from); endif; ?>
							</span>
						</td>
                   </tr>
    
   <tr>
    <td>&nbsp;</td>

    <td><?php echo $this->_tpl_vars['L_Destinations']; ?>
 :<br /><input type="text" class="texto_content" value="<?php echo $this->_tpl_vars['dest_title']; ?>
" name="dest_title" readonly>
						<input type="hidden" class="texto_content" name="dest_id" value="<?php echo $this->_tpl_vars['dest_id']; ?>
" size="35" >
</td>
  </tr>
     <tr>
    <td>&nbsp;</td>
    <td><?php echo $this->_tpl_vars['L_No_Person']; ?>
 :<br /><input type="text" class="texto_content" size="10" value="<?php echo $this->_tpl_vars['no_person']; ?>
" name="no_person" onkeypress="javascript: isNumericKey(this.value)"></td>
  </tr>
   <tr>
    <td>&nbsp;</td>

    <td><?php echo $this->_tpl_vars['L_Hotel']; ?>
 : <br /><input type="text" class="texto_content" size="15" value="<?php echo $this->_tpl_vars['hotelName']; ?>
" name="hotelName">
</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><?php echo $this->_tpl_vars['L_Date']; ?>
 : <br />

<div class="demo">
<p><input name="arrivalDate" value="<?php echo $this->_tpl_vars['arrivalDate']; ?>
" id="datepicker"></p>

</div> - <SELECT class="texto_content" name="arrival_hour">
														  <?php echo $this->_tpl_vars['V_Arrival_Hour']; ?>

                                          </SELECT>
                                          <select class="texto_content" name="arrival_minute">
														  <?php echo $this->_tpl_vars['V_Arrival_Minute']; ?>

                                          </select>
                                          <SELECT class="texto_content" name="arrival_DayPart">
														  <?php echo $this->_tpl_vars['V_ArrivalDayPart']; ?>

                                          </SELECT> &nbsp;</td>
  </tr>

  
    <tr>
    <td>&nbsp;</td>
    <td><?php echo $this->_tpl_vars['L_Airline']; ?>
 : <br /><input name="arrivalAirline" value="<?php echo $this->_tpl_vars['arrivalAirline']; ?>
" type="text" class="texto_content" size="15"> <input name="arrivalFlight" value="<?php echo $this->_tpl_vars['arrivalFlight']; ?>
" type="text" class="texto_content" size="15"></td>
  </tr>  
  
</table>

<!--Fin Test de formulario -->
</td></tr><tr><td>
<?php if ($this->_tpl_vars['TriptypeId'] == 1): ?>
							<br /><br />
						<?php else: ?>
						
                            <!--Inicia segunda tabla -->
<table>
                   <tr >
                     <td >
						
							<input type="hidden" name="roundTrip" value="2">							
					

						 <?php echo $this->_tpl_vars['L_Hotel_To_Airport']; ?>

					 </td>
                   </tr>
				   
                   <tr>
    <td>&nbsp;</td>

    <td><?php echo $this->_tpl_vars['L_Destinations']; ?>
 : <br /><input type="text" class="texto_content" value="<?php echo $this->_tpl_vars['dest_title']; ?>
" name="dest_title" readonly>
						<input type="hidden" class="texto_content" name="departureDestId" value="<?php echo $this->_tpl_vars['dest_id']; ?>
" size="10" >
</td>
  </tr>
   <tr>
    <td>&nbsp;</td>
    <td><?php echo $this->_tpl_vars['L_No_Person']; ?>
 :<br /><input type="text" class="texto_content" size="10" value="<?php echo $this->_tpl_vars['departureTotalPerson']; ?>
" name="departureTotalPerson" onkeypress="javascript: isNumericKey(this.value)"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>

    <td><?php echo $this->_tpl_vars['L_Hotel']; ?>
 : <br /><input type="text" class="texto_content" size="15" value="<?php echo $this->_tpl_vars['departureHotelName']; ?>
" name="departureHotelName">
</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><?php echo $this->_tpl_vars['L_Time']; ?>
 : <br />
    <div class="demo">
<p><input name="departureDate" value="<?php echo $this->_tpl_vars['departureDate']; ?>
" id="datepicker2"></p></div>
 <?php if ($this->_tpl_vars['lng'] == 'en'): ?>Depature Flight time <?php else: ?> Horario del despegue del vuelo <?php endif; ?>  - <br><SELECT class="texto_content" name="departure_hour">
									<?php echo $this->_tpl_vars['V_Departure_Hour']; ?>

							</SELECT>
							<SELECT class="texto_content" name="departure_minute">
									<?php echo $this->_tpl_vars['V_Departure_Minute']; ?>

							</SELECT>
							<SELECT class="texto_content" name="departure_DayPart">
									<?php echo $this->_tpl_vars['V_DepartureDayPart']; ?>

							</SELECT></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td><?php echo $this->_tpl_vars['L_Flight']; ?>
 <br />
<SELECT class="texto_content" name="pickup_hour">
									<?php echo $this->_tpl_vars['V_Pickup_Hour']; ?>

							</SELECT>
                                  <SELECT class="texto_content" name="pickup_minute">
									<?php echo $this->_tpl_vars['V_Pickup_Minute']; ?>

							</SELECT>
                                <SELECT class="texto_content" name="pickup_DayPart">
									<?php echo $this->_tpl_vars['V_Pickup_DayPart']; ?>

							</SELECT></td>
  </tr>
  
   <tr>
    <td>&nbsp;</td>
    <td><?php echo $this->_tpl_vars['L_Airline']; ?>
 <br /><input name="departureAirline" value="<?php echo $this->_tpl_vars['departureAirline']; ?>
" type="text" class="texto_content" size="15"> - <input name="departureFlight" value="<?php echo $this->_tpl_vars['departureFlight']; ?>
" type="text" class="texto_content" size="15"></td>
  </tr>
                 </table>						
						<?php endif; ?> 
</td>
               </tr>
			   <tr> <td>&nbsp;</td>
                 <td><input type="image" onclick="javascript: return Form_Submit(frmReserve);" src="<?php echo $this->_tpl_vars['Templates_Image'];  echo $this->_tpl_vars['site_res_img']; ?>
"></td>
               </tr>
            
		<tr> <td>&nbsp;</td>
		  <td colspan="2" valign="top" align="center">
			<input type="hidden" name="Submit" value="<?php echo $this->_tpl_vars['SUBMIT']; ?>
">
			<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
			<input type="hidden" name="status">
			<input type="hidden" name="carttranId" value="<?php echo $this->_tpl_vars['carttranId']; ?>
">
			<input type="hidden" name="itemType" value="2">
			<input type="hidden" name="rate" value="<?php echo $this->_tpl_vars['Rate']; ?>
">
			<input type="hidden" name="passenger_id" value="<?php echo $this->_tpl_vars['PassengerId']; ?>
">
			<input type="hidden" name="triptype_id" value="<?php echo $this->_tpl_vars['TriptypeId']; ?>
">
		  </td>
		</tr>
   
     
    </FORM>
<script language="javascript">
  	if(document.frmReserve.singleTrip.value == 1)
	{
			if(document.frmReserve.roundTrip.value != 2)
			{
				document.frmReserve.departureTotalPerson.disabled	=  true; 
				document.frmReserve.departureHotelName.disabled		=  true;					
				document.frmReserve.departureDate.disabled			=  true; 
				
/*				document.frmReserve.pickup_hour.disabled			=  true; 
				document.frmReserve.pickup_minute.disabled			=  true; 
				document.frmReserve.pickup_DayPart.disabled			=  true; 
*/
				document.frmReserve.departure_hour.disabled			=  true; 
				document.frmReserve.departure_minute.disabled		=  true; 
				document.frmReserve.departure_DayPart.disabled		=  true; 

				document.frmReserve.departureAirline.disabled		=  true; 
				document.frmReserve.departureFlight.disabled		=  true; 				
			}
	}
  </script>
</table>