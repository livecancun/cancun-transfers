<?php /* Smarty version 2.6.0, created on 2018-07-23 04:43:52
         compiled from reservationInfo.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'reservationInfo.tpl', 59, false),array('function', 'math', 'reservationInfo.tpl', 82, false),array('modifier', 'string_format', 'reservationInfo.tpl', 72, false),)), $this); ?>
<script>
	var Valid_Select_Cart	= '<?php echo $this->_tpl_vars['Valid_Select_Cart']; ?>
';
	var Valid_Delete_Cart  	= '<?php echo $this->_tpl_vars['Valid_Delete_Cart']; ?>
';
	var Confirm_Delete 		= '<?php echo $this->_tpl_vars['Confirm_Delete']; ?>
';

	var Empty_First_Name	= '<?php echo $this->_tpl_vars['Empty_First_Name']; ?>
';
	var Empty_Last_Name  	= '<?php echo $this->_tpl_vars['Empty_Last_Name']; ?>
';
	var Empty_Email 		= '<?php echo $this->_tpl_vars['Empty_Email']; ?>
';
	var Valid_Email 		= '<?php echo $this->_tpl_vars['Valid_Email']; ?>
';
	var Empty_Address 		= '<?php echo $this->_tpl_vars['Empty_Address']; ?>
';
	var Empty_Country 		= '<?php echo $this->_tpl_vars['Empty_Country']; ?>
';
	var Empty_City 			= '<?php echo $this->_tpl_vars['Empty_City']; ?>
';
	var Empty_State 		= '<?php echo $this->_tpl_vars['Empty_State']; ?>
';
	var Empty_Zip 			= '<?php echo $this->_tpl_vars['Empty_Zip']; ?>
';
	var Valid_Zip 			= '<?php echo $this->_tpl_vars['Valid_Zip']; ?>
';			
	var Empty_Area_Code 	= '<?php echo $this->_tpl_vars['Empty_Area_Code']; ?>
';
	var Empty_City_Code 	= '<?php echo $this->_tpl_vars['Empty_City_Code']; ?>
';			
	var Empty_Phone_No 		= '<?php echo $this->_tpl_vars['Empty_Phone_No']; ?>
';
	var Valid_PhoneNo 		= '<?php echo $this->_tpl_vars['Valid_PhoneNo']; ?>
';			
</script>

<br /><br />
<table>
<tr>
<td>
<?php if ($this->_tpl_vars['lng'] == 'en'): ?>
<img src="images/step-3.jpg" /></td><td><h2>Checkout form for Payment process</h2></td>
<?php else: ?>
<img src="images/step-3.jpg" /></td><td><h2>Datos para proceso de pago</h2></td>
<?php endif; ?>
</tr></table>

<table border="0" cellpadding="1" cellspacing="1" width="100%">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" height="100%" border="0" cellspacing="0">
					<tr><td><table width="100%" cellpadding="0" cellspacing="0">

					<tr>
						<td valign="top">
							  <table width="92%" border="0" cellspacing="0">
								<tr>
								  <td valign="top"><div align="justify">
										<p class="Estilo10"><span class="texto_content"><?php echo $this->_tpl_vars['L_Res_Info']; ?>

                                        <?php if ($this->_tpl_vars['lng'] == 'en'): ?>
Please fill required data
<?php else: ?>
Porfavor llene los datos requeridos
<?php endif; ?>
                                        </span></p>
										<table width="100%" border="0" align="center" cellspacing="0">
										  <FORM name="frmCart" action="<?php echo $this->_tpl_vars['A_Cart']; ?>
" method="POST">
											<tr bgcolor="#007ABD">
											  <td width="5%"><div align="center"></div></td>
											  <td width="60%"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_Requested_Service']; ?>
</span></span></div></td>
											  <td width="25%"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_Rate']; ?>
</span></span></div></td>
											</tr>
											<?php if (isset($this->_foreach['CartInfo'])) unset($this->_foreach['CartInfo']);
$this->_foreach['CartInfo']['name'] = 'CartInfo';
$this->_foreach['CartInfo']['total'] = count($_from = (array)$this->_tpl_vars['CartInfo']);
$this->_foreach['CartInfo']['show'] = $this->_foreach['CartInfo']['total'] > 0;
if ($this->_foreach['CartInfo']['show']):
$this->_foreach['CartInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Cart']):
        $this->_foreach['CartInfo']['iteration']++;
        $this->_foreach['CartInfo']['first'] = ($this->_foreach['CartInfo']['iteration'] == 1);
        $this->_foreach['CartInfo']['last']  = ($this->_foreach['CartInfo']['iteration'] == $this->_foreach['CartInfo']['total']);
?>
											<?php echo smarty_function_assign(array('var' => 'TotalPrice','value' => ($this->_tpl_vars['TotalPrice']+$this->_tpl_vars['Cart']['totalCharge'])), $this);?>

											<tr bgcolor="#EBEBEB">
											  <td><div align="center"><span class="texto_content">
												  <input name="checkbox" type="checkbox" value="checkbox">
												  </span></div></td>
											  <td><div align="center" class="texto_content"><?php echo $this->_tpl_vars['Cart']['dest_title']; ?>
    </div></td>
											  <td><div align="center"><span class="texto_content">$<?php echo $this->_tpl_vars['Cart']['totalCharge']; ?>
 <?php echo $this->_tpl_vars['Currency']; ?>
</span></div></td>
											</tr>
											<?php endforeach; unset($_from); endif; ?>
										  </FORM>
										  <tr>
											<td><div align="center"><span class="texto_content"> </span></div></td>
											<td><div align="right"><span class="texto_content"><strong><?php echo $this->_tpl_vars['L_Total']; ?>
</strong></span></div></td>
											<td><div align="center"><span class="texto_content"><strong>$ <?php echo ((is_array($_tmp=$this->_tpl_vars['TotalPrice'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
 <?php echo $this->_tpl_vars['Currency']; ?>
</strong></span></div></td>
										  </tr>
										  <?php if ($this->_tpl_vars['is_airline_empl'] == 1): ?>
										  <tr>
											<td>&nbsp;</td>
											<td>
												<div align="right"><span class="texto_content"><strong><?php echo $this->_tpl_vars['L_Discount']; ?>
 <?php echo $this->_tpl_vars['discount_rate']; ?>
% 
												<?php echo smarty_function_assign(array('var' => 'discount','value' => ($this->_tpl_vars['discount_rate'])), $this);?>

												</strong></span></div>
											</td>
											<td><div align="center"><span class="texto_content"><strong>- $ <?php echo smarty_function_math(array('equation' => "(x * y)/100",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</strong></span></div></td>
										  </tr>
										  <tr>
											<td>&nbsp;</td>
											<td><div align="right"><span class="texto_content"><strong><?php echo $this->_tpl_vars['L_Final']; ?>
 <?php echo $this->_tpl_vars['L_Total']; ?>
</strong></span></div></td>
											<td><div align="center"><span class="texto_content"><strong>$ <?php echo smarty_function_math(array('equation' => "(x-((x * y)/100))",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</strong></span></div></td>
										  </tr>
										  <?php endif; ?>
										</table>
										<div align="left"><br>
										  <table width="100%" border="0" align="center" cellspacing="0">
											<form name="frmReservation" action="<?php echo $this->_tpl_vars['A_Reservation']; ?>
" method="post">
											  <tr bgcolor="#007ABD">
												<td colspan="2"><div align="center"><span class="texto_content"><span class="Estilo25"><?php echo $this->_tpl_vars['L_Personal_Information']; ?>
 </span></span></div></td>
											  </tr>
											  <?php if ($this->_tpl_vars['is_airline_empl'] == 1): ?>
											  <tr bgcolor="#EBEBEB">
												<td width="30%"><div align="right"><span class="texto_content"> <?php echo $this->_tpl_vars['L_Applicable_Discount']; ?>
:</span></div></td>
												<td width="70%" class="texto_content"><?php echo $this->_tpl_vars['discount_title']; ?>
</td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Company_Name']; ?>
:</span></div></td>
												<td><span class="texto_content"><?php echo $this->_tpl_vars['empl_airline']; ?>
 </span></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td><div align="right"><span class="texto_content"><?php echo $this->_tpl_vars['L_Company_ID']; ?>
:</span></div></td>
												<td><span class="texto_content"><?php echo $this->_tpl_vars['empl_id']; ?>
</span></td>
											  </tr>
											  <?php endif; ?>
											  <tr bgcolor="#EBEBEB">
												<td height="20" class="texto_content"><div align="right"><?php echo $this->_tpl_vars['L_First_Name']; ?>
:</div></td>
												<td><input type="text" name="firstname" value="<?php echo $this->_tpl_vars['Firstname']; ?>
" class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right"><?php echo $this->_tpl_vars['L_Last_Name']; ?>
:</div></td>
												<td><input type="text" name="lastname" value="<?php echo $this->_tpl_vars['Lastname']; ?>
"class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right"><?php echo $this->_tpl_vars['L_Email']; ?>
:</div></td>
												<td><input type="text" name="email" value="<?php echo $this->_tpl_vars['Email']; ?>
" class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right"><?php echo $this->_tpl_vars['L_Address']; ?>
:</div></td>
												<td><textarea name="address1" rows="3" cols="30" class="texto_content"><?php echo $this->_tpl_vars['Address1']; ?>
</textarea>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right"><?php echo $this->_tpl_vars['L_Country']; ?>
:</div></td>
												<td><SELECT name="country" class="texto_content"><option value="0">-----------<?php echo $this->_tpl_vars['L_Select_Country']; ?>
--------</option><?php echo $this->_tpl_vars['CountryList']; ?>
</SELECT>
												</td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right"><?php echo $this->_tpl_vars['L_City']; ?>
:</div></td>
												<td><input name="city" value="<?php echo $this->_tpl_vars['City']; ?>
" type="text" class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right"><?php echo $this->_tpl_vars['L_State']; ?>
:</div></td>
												<td><input type="text" name="state" value="<?php echo $this->_tpl_vars['State']; ?>
" class="texto_content" size="40"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right"><?php echo $this->_tpl_vars['L_Zip']; ?>
:</div></td>
												<td><input type="text" name="zipcode" value="<?php echo $this->_tpl_vars['Zip']; ?>
" class="texto_content" size="10" maxlength="5"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td class="texto_content"><div align="right"><?php echo $this->_tpl_vars['L_Phone_Number']; ?>
:</div></td>
												<td><input name="phone_areacode" value="<?php echo $this->_tpl_vars['Phone_Areacode']; ?>
" maxlength="3" type="text" class="texto_content" size="10" onKeyPress="javascript: isNumericKey(this.value)">
												  <input name="phone_citycode" value="<?php echo $this->_tpl_vars['Phone_Citycode']; ?>
" maxlength="3" type="text" class="texto_content" size="10" onKeyPress="javascript: isNumericKey(this.value)">
												  <input name="phone_no" value="<?php echo $this->_tpl_vars['Phone_No']; ?>
" type="text" class="texto_content" maxlength="6" size="10" onKeyPress="javascript: isNumericKey(this.value)"></td>
											  </tr>
											  <tr bgcolor="#EBEBEB">
												<td height="29" class="texto_content"><div align="right"></div></td>
												<td valign="top"><input type="submit" name="Submit" value="<?php echo $this->_tpl_vars['Submit']; ?>
" class="texto_content" onClick="javascript: return Form_ResSubmit(frmReservation);">
												  &nbsp;&nbsp;
												  <input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
												  <input type="hidden" name="index_id" value="<?php echo $this->_tpl_vars['index_id']; ?>
">
												  <input type="hidden" name="cart_id" value="<?php echo $this->_tpl_vars['cart_id']; ?>
">
												  <input type="hidden" name="dest_id" value="<?php echo $this->_tpl_vars['dest_id']; ?>
">
												  <input type="hidden" name="V_grandTotal" value="<?php echo $this->_tpl_vars['V_grandTotal']; ?>
">				  
												  
												  <?php if (isset($this->_sections['Cart'])) unset($this->_sections['Cart']);
$this->_sections['Cart']['name'] = 'Cart';
$this->_sections['Cart']['loop'] = is_array($_loop=$this->_tpl_vars['chk_cart_id']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['Cart']['show'] = true;
$this->_sections['Cart']['max'] = $this->_sections['Cart']['loop'];
$this->_sections['Cart']['step'] = 1;
$this->_sections['Cart']['start'] = $this->_sections['Cart']['step'] > 0 ? 0 : $this->_sections['Cart']['loop']-1;
if ($this->_sections['Cart']['show']) {
    $this->_sections['Cart']['total'] = $this->_sections['Cart']['loop'];
    if ($this->_sections['Cart']['total'] == 0)
        $this->_sections['Cart']['show'] = false;
} else
    $this->_sections['Cart']['total'] = 0;
if ($this->_sections['Cart']['show']):

            for ($this->_sections['Cart']['index'] = $this->_sections['Cart']['start'], $this->_sections['Cart']['iteration'] = 1;
                 $this->_sections['Cart']['iteration'] <= $this->_sections['Cart']['total'];
                 $this->_sections['Cart']['index'] += $this->_sections['Cart']['step'], $this->_sections['Cart']['iteration']++):
$this->_sections['Cart']['rownum'] = $this->_sections['Cart']['iteration'];
$this->_sections['Cart']['index_prev'] = $this->_sections['Cart']['index'] - $this->_sections['Cart']['step'];
$this->_sections['Cart']['index_next'] = $this->_sections['Cart']['index'] + $this->_sections['Cart']['step'];
$this->_sections['Cart']['first']      = ($this->_sections['Cart']['iteration'] == 1);
$this->_sections['Cart']['last']       = ($this->_sections['Cart']['iteration'] == $this->_sections['Cart']['total']);
?>
													<input type="hidden" name="chkCartId[]" value="<?php echo $this->_tpl_vars['chk_cart_id'][$this->_sections['Cart']['index']]; ?>
">
												  <?php endfor; endif; ?> 
												  
												  </td>
											  </tr>
											</form>
										  </table>
											</div>
									  </div></td>
								</tr>
							  </table>
						</td>
					</tr>
					
					</table>
					
					</td>
					</tr>

				  </table>
			</td>
		</tr>
</table>