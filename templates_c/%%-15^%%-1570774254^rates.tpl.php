<?php /* Smarty version 2.6.0, created on 2011-06-02 21:23:36
         compiled from rates.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'rates.tpl', 22, false),array('function', 'cycle', 'rates.tpl', 66, false),array('modifier', 'array_value', 'rates.tpl', 71, false),)), $this); ?>
<table border="0" cellpadding="1" cellspacing="1" width="100%">
<FORM name="frmRates" action="<?php echo $this->_tpl_vars['A_Action']; ?>
" method="post">
	<tr>
		<td width="100%" valign="top" align="center">
			<table width="100%" height="100%" border="0" cellspacing="0">
					<tr><td><table width="100%" cellpadding="0" cellspacing="0">

					<tr>
						<td valign="top">
							<table width="97%" border="0" cellspacing="0">
							  <tr>
								<td><div align="justify"><?php echo $this->_tpl_vars['L_Rate_Text']; ?>
</div></td>
							  </tr>
							</table>
							<br>
						
							<table border="0" cellpadding="0" cellspacing="0" width="97%" bordercolor="#CCCCCC">
								<tr>
									<td width="25%"><div align="center" class="Estilo30"><span class="Estilo27"><?php echo $this->_tpl_vars['L_Destinations']; ?>
</span></div></td>
							
									<?php if (isset($this->_foreach['PassengerRange'])) unset($this->_foreach['PassengerRange']);
$this->_foreach['PassengerRange']['name'] = 'PassengerRange';
$this->_foreach['PassengerRange']['total'] = count($_from = (array)$this->_tpl_vars['PassengerRange']);
$this->_foreach['PassengerRange']['show'] = $this->_foreach['PassengerRange']['total'] > 0;
if ($this->_foreach['PassengerRange']['show']):
$this->_foreach['PassengerRange']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Passenger']):
        $this->_foreach['PassengerRange']['iteration']++;
        $this->_foreach['PassengerRange']['first'] = ($this->_foreach['PassengerRange']['iteration'] == 1);
        $this->_foreach['PassengerRange']['last']  = ($this->_foreach['PassengerRange']['iteration'] == $this->_foreach['PassengerRange']['total']);
?>
									<?php echo smarty_function_assign(array('var' => 'PsngrId','value' => $this->_tpl_vars['Passenger']['passenger_id']), $this);?>

									<td width="20%" align="center">
										<table width="100%" cellpadding="0" cellspacing="0" border="0">
											<tr>
												<td>
													<table width="100%" cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td>
																<div align="center" class="Estilo31"><strong><?php echo $this->_tpl_vars['Passenger']['passenger_starting_range']; ?>
</b> <?php echo $this->_tpl_vars['L_To']; ?>

																	<b><?php echo $this->_tpl_vars['Passenger']['passenger_ending_range']; ?>
</b> Pax</strong>
																</div>
															</td>
														</tr>
													</table>
												</td>
											</tr>

											<tr>
												<td align="center">
													<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
														<tr>
														 <?php if (isset($this->_foreach['TripTypeInfo'])) unset($this->_foreach['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['name'] = 'TripTypeInfo';
$this->_foreach['TripTypeInfo']['total'] = count($_from = (array)$this->_tpl_vars['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['show'] = $this->_foreach['TripTypeInfo']['total'] > 0;
if ($this->_foreach['TripTypeInfo']['show']):
$this->_foreach['TripTypeInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['TripType']):
        $this->_foreach['TripTypeInfo']['iteration']++;
        $this->_foreach['TripTypeInfo']['first'] = ($this->_foreach['TripTypeInfo']['iteration'] == 1);
        $this->_foreach['TripTypeInfo']['last']  = ($this->_foreach['TripTypeInfo']['iteration'] == $this->_foreach['TripTypeInfo']['total']);
?>
															<td>
																<div align="center" class="Estilo31">
																	<?php echo $this->_tpl_vars['TripType']['triptype_title']; ?>

																</div>
															</td>
														 <?php endforeach; unset($_from); endif; ?>
														</tr>
													</table>
												</td>

											</tr>
										</table>

									</td>
									<?php if ($this->_foreach['PassengerRange']['last']):  echo smarty_function_assign(array('var' => 'RangeStart','value' => $this->_tpl_vars['Passenger']['passenger_ending_range']+1), $this); endif; ?>
									<?php endforeach; unset($_from); endif; ?>
							
									
								</tr>
								
								<?php if (isset($this->_foreach['DestinationInfo'])) unset($this->_foreach['DestinationInfo']);
$this->_foreach['DestinationInfo']['name'] = 'DestinationInfo';
$this->_foreach['DestinationInfo']['total'] = count($_from = (array)$this->_tpl_vars['DestinationInfo']);
$this->_foreach['DestinationInfo']['show'] = $this->_foreach['DestinationInfo']['total'] > 0;
if ($this->_foreach['DestinationInfo']['show']):
$this->_foreach['DestinationInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Destination']):
        $this->_foreach['DestinationInfo']['iteration']++;
        $this->_foreach['DestinationInfo']['first'] = ($this->_foreach['DestinationInfo']['iteration'] == 1);
        $this->_foreach['DestinationInfo']['last']  = ($this->_foreach['DestinationInfo']['iteration'] == $this->_foreach['DestinationInfo']['total']);
?>
								
								<tr class="<?php echo smarty_function_cycle(array('values' => 'list_A, list_B'), $this);?>
">
							
									<td><div align="center" class="Estilo32"><?php echo $this->_tpl_vars['Destination']['dest_title']; ?>
&nbsp;&nbsp;</div></td>
							
								<?php if (isset($this->_foreach['R_RecordSet'])) unset($this->_foreach['R_RecordSet']);
$this->_foreach['R_RecordSet']['name'] = 'R_RecordSet';
$this->_foreach['R_RecordSet']['total'] = count($_from = (array)$this->_tpl_vars['R_RecordSet']);
$this->_foreach['R_RecordSet']['show'] = $this->_foreach['R_RecordSet']['total'] > 0;
if ($this->_foreach['R_RecordSet']['show']):
$this->_foreach['R_RecordSet']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Record']):
        $this->_foreach['R_RecordSet']['iteration']++;
        $this->_foreach['R_RecordSet']['first'] = ($this->_foreach['R_RecordSet']['iteration'] == 1);
        $this->_foreach['R_RecordSet']['last']  = ($this->_foreach['R_RecordSet']['iteration'] == $this->_foreach['R_RecordSet']['total']);
?>
								<?php echo smarty_function_assign(array('var' => 'PriceData','value' => ((is_array($_tmp='price_data')) ? $this->_run_mod_handler('array_value', true, $_tmp, $this->_tpl_vars['Record']) : smarty_modifier_array_value($_tmp, $this->_tpl_vars['Record']))), $this);?>

									<td valign="top" width="15%">
										<table border="0" cellspacing="3" cellpadding="0" align="center" width="100%">
											<tr>
											 <?php if (isset($this->_foreach['TripTypeInfo'])) unset($this->_foreach['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['name'] = 'TripTypeInfo';
$this->_foreach['TripTypeInfo']['total'] = count($_from = (array)$this->_tpl_vars['TripTypeInfo']);
$this->_foreach['TripTypeInfo']['show'] = $this->_foreach['TripTypeInfo']['total'] > 0;
if ($this->_foreach['TripTypeInfo']['show']):
$this->_foreach['TripTypeInfo']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['TripType']):
        $this->_foreach['TripTypeInfo']['iteration']++;
        $this->_foreach['TripTypeInfo']['first'] = ($this->_foreach['TripTypeInfo']['iteration'] == 1);
        $this->_foreach['TripTypeInfo']['last']  = ($this->_foreach['TripTypeInfo']['iteration'] == $this->_foreach['TripTypeInfo']['total']);
?>
													
												<td>
													<div align="center" class="Estilo32">
														&nbsp;<?php echo $this->_tpl_vars['Record']['price_data'][$this->_tpl_vars['Destination']['dest_id']][$this->_tpl_vars['TripType']['triptype_id']]; ?>
&nbsp;&nbsp;&nbsp;
													<br>
													<input name="reserve" type="image" onclick="JavaScript: Reserve_Click(document.frmRates,<?php echo $this->_tpl_vars['PsngrId']; ?>
,<?php echo $this->_tpl_vars['TripType']['triptype_id']; ?>
, <?php echo $this->_tpl_vars['Destination']['dest_id']; ?>
,<?php echo $this->_tpl_vars['Record']['price_data'][$this->_tpl_vars['Destination']['dest_id']][$this->_tpl_vars['TripType']['triptype_id']]; ?>
 );" class="texto_content" src="<?php echo $this->_tpl_vars['Templates_Image'];  echo $this->_tpl_vars['site_paynow_img']; ?>
" value="<?php echo $this->_tpl_vars['L_Reserve']; ?>
">
													</div>
												</td>
				
											  <?php endforeach; unset($_from); endif; ?>
													  
											</tr>
									   </table>
									</td>
								<?php endforeach; unset($_from); endif; ?>	
				
							 </tr>
							<?php endforeach; unset($_from); endif; ?>
				
							</table>
						</td>
					</tr>
					
					</table>
					
					<table width="97%" border="0" cellspacing="0">
						<tr>
							<td>
								<div align="justify"><br />
								 	<?php echo $this->_tpl_vars['L_Rate_Text2']; ?>

							  	</div>
							</td>
						</tr>
					</table>
					</td>
					</tr>
				  </table>
			</td>
		</tr>
		<tr>
			<TD>
				<input type="hidden" name="Action" value="<?php echo $this->_tpl_vars['ACTION']; ?>
">
				<input type="hidden" name="rate">
				<input type="hidden" name="passenger_id">
				<input type="hidden" name="triptype_id">
				<input type="hidden" name="dest_id">
				<input type="hidden" name="rate_id">		
			</TD>
		</tr>
	</FORM>
</table>