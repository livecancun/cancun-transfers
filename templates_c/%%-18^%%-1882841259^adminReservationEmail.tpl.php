<?php /* Smarty version 2.6.0, created on 2011-06-02 21:24:49
         compiled from adminReservationEmail.tpl */ ?>
<?php require_once(SMARTY_DIR . 'core' . DIRECTORY_SEPARATOR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'assign', 'adminReservationEmail.tpl', 35, false),array('function', 'math', 'adminReservationEmail.tpl', 55, false),array('modifier', 'string_format', 'adminReservationEmail.tpl', 44, false),)), $this); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>:::Welcome to Cancun-Transfers.com:::</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
	<link href="<?php echo $this->_tpl_vars['cssSiteroot']; ?>
/style.css" rel="stylesheet" type="text/css">
<body>
<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='FFFFFF'>
					      <tr>							  
								 <td>&nbsp;</td>
						  </tr>
						  <tr>
								<td valign='top' width='100%'>
										<font size='2' face='Verdana, Arial, Helvetica,sans-serif'>Dear Administrator,<br></font>
										    <font face='Verdana, Arial, Helvetica, sans-serif' size='2'>
											 <br>
											 <br>
											   Welcome to Cancun-Transfers.com,<br>
											   <br><br>
											   Reservation Details of <font color='#184DA5'><B><?php echo $this->_tpl_vars['name']; ?>
</B></font><br>
											   Confirmation No : <font color='#184DA5'><B><?php echo $this->_tpl_vars['Confirmation_No']; ?>
</B></font>
												<hr><br>
								  </font> 
											   <table border='0' cellpadding='2' cellspacing='2' width='70%' align="center">
												<tr>
												  <td colspan='2' align='left' bgcolor="#007ABD"><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b>&nbsp;<?php echo $this->_tpl_vars['L_Reservation_Details']; ?>
</b></font></td>
												</tr>	
												<tr>
													<td  align='center' width='40%' bgcolor='#007ABD'><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b><?php echo $this->_tpl_vars['L_cartItem']; ?>
</b></font></td>
<!--													<td  align='center' width='40%' bgcolor='#184DA5'><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b><?php echo $this->_tpl_vars['L_ItemType']; ?>
</b></font></td>-->
													<td  align='center' width='20%' bgcolor='#007ABD'><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b><?php echo $this->_tpl_vars['L_Total']; ?>
</b></font></td>
												</tr>											
												<?php if (isset($this->_foreach['ShowRequest'])) unset($this->_foreach['ShowRequest']);
$this->_foreach['ShowRequest']['name'] = 'ShowRequest';
$this->_foreach['ShowRequest']['total'] = count($_from = (array)$this->_tpl_vars['ShowRequest']);
$this->_foreach['ShowRequest']['show'] = $this->_foreach['ShowRequest']['total'] > 0;
if ($this->_foreach['ShowRequest']['show']):
$this->_foreach['ShowRequest']['iteration'] = 0;
    foreach ($_from as $this->_tpl_vars['Record']):
        $this->_foreach['ShowRequest']['iteration']++;
        $this->_foreach['ShowRequest']['first'] = ($this->_foreach['ShowRequest']['iteration'] == 1);
        $this->_foreach['ShowRequest']['last']  = ($this->_foreach['ShowRequest']['iteration'] == $this->_foreach['ShowRequest']['total']);
?>
												<?php echo smarty_function_assign(array('var' => 'TotalPrice','value' => ($this->_tpl_vars['TotalPrice']+$this->_tpl_vars['Record']['totalCharge'])), $this);?>

												<tr>
													<td align='center' height='25' bgcolor='#EEEEEE' ><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'><?php echo $this->_tpl_vars['Record']['dest_title']; ?>
</font> </td>
<!--													<td align='center' bgcolor='#EEEEEE'><font size='2' face='Verdana, Arial, Helvetica, sans-serif' color='#000000'><?php echo $this->_tpl_vars['Record']['itemType']; ?>
</font> </td>-->
													<td align='center' bgcolor='#EEEEEE'><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'>$ <?php echo $this->_tpl_vars['Record']['totalCharge']; ?>
</font></td>
											  	</tr>	
											  	<?php endforeach; unset($_from); endif; ?>
												<tr>
													<td colspan='1' align='right'><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'><?php echo $this->_tpl_vars['L_Total']; ?>
 ($):</font></td>
													<td  align='center'>&nbsp;&nbsp;<font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'>$ <?php echo ((is_array($_tmp=$this->_tpl_vars['TotalPrice'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
</font></td>
												</tr>
												<?php if ($this->_tpl_vars['is_airline_empl'] == 1): ?>
												<tr>
												  <td><div align="right"><span class="texto_content">
														<font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'>
														<strong><?php echo $this->_tpl_vars['L_Discount']; ?>
 <?php echo $this->_tpl_vars['discount_rate']; ?>
% </strong>
														</font>
														<?php echo smarty_function_assign(array('var' => 'discount','value' => ($this->_tpl_vars['discount_rate'])), $this);?>

														</span></div>
												  </td>
												  <td><div align="center"><span class="texto_content"><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'><strong>- $ <?php echo smarty_function_math(array('equation' => "(x * y)/100",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</strong></font></span></div></td>
												</tr>
												<tr>
												  <td><div align="right"><span class="texto_content"><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'><strong><?php echo $this->_tpl_vars['L_Final']; ?>
 <?php echo $this->_tpl_vars['L_Total']; ?>
</strong></font></span></div></td>
												  <td><div align="center"><span class="texto_content"><font size="2" face='Verdana, Arial, Helvetica, sans-serif' color='#000000'><strong>$ <?php echo smarty_function_math(array('equation' => "(x-((x * y)/100))",'x' => $this->_tpl_vars['TotalPrice'],'y' => $this->_tpl_vars['discount'],'format' => "%.2f"), $this);?>
</strong></font></span></div></td>
												</tr>
												<?php endif; ?>
												
												<tr><td colspan='2'>&nbsp;</td></tr>
											</table>
											<hr><br>								  <font face='Verdana, Arial, Helvetica, sans-serif' size='2'><br>
														  Thank You,<br><br>
														  Regards,<br>
														  Cancun-Transfers Team.
										  </font>
										  <font size='2' face='Verdana, Arial, Helvetica, sans-serif'><br>
										  </font> 
								</td>
						   </tr>
				   <tr><td>&nbsp;</td></tr>
		 </table>
</body>
</html>